function ang = AR_Range_MinusPI_Ang_PI(Angle, NormAng)
% Svodenje kuta na [NormAng-pi,NormAng+pi>

ang = AR_Range_MinusPI_PI(Angle-NormAng) + NormAng;