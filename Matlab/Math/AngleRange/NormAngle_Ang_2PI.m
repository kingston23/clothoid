% Svodenje kuta na [NormAng,NormAng+2pi>
function a = NormAngle_Ang_2PI(Angle, NormAng)

a = NormAngle_Zero_2PI(Angle-NormAng) + NormAng;
