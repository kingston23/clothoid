% Svodenje kuta na [-pi,pi>
function [a] = NormAngle_MinusPI_PI(Angle)

PI2 = 2*pi;
Angle = Angle - floor(Angle/PI2)*PI2; % // Svodenje na [0,2pi> (radi i za neg. kutove)
% na [-pi,pi>
ind = find(Angle>=pi);
Angle(ind) = Angle(ind) - PI2;

a = Angle;
