% Svodenje kuta na <-2pi,0]
function a = NormAngle_Minus2PI_Zero(Angle)

PI2 = 2*pi;
Angle = Angle - floor(Angle/PI2)*PI2; % Svodenje na [0,2pi> (radi i za neg. kutove)

%na <-2pi,0]
ind = find(Angle>0);
Angle(ind) = Angle(ind) - PI2;

a = Angle;