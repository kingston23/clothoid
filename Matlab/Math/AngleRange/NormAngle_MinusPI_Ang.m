% Svodenje kuta na [NormAng-pi,NormAng+pi>
function a = NormAngle_MinusPI_Ang(Angle, NormAng)

a = NormAngle_MinusPI_PI(Angle-NormAng) + NormAng;
