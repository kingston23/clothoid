function a = NormAngle_MinusPI_Zero(Angle)
% Svodenje kuta na <-pi,0]

Angle = Angle - floor(Angle/pi)*pi; % Svodenje na [0,pi> (radi i za neg. kutove)

ind = find(Angle>0);
Angle(ind) = Angle(ind) - pi; % na <pi,0]

a = Angle;
