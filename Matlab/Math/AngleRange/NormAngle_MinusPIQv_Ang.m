% Svodenje kuta na [NormAng-pi/4,NormAng+pi/4>
function a = NormAngle_MinusPIQv_Ang(Angle, NormAng)

a = NormAngle_MinusPIQv_PIQv(Angle-NormAng) + NormAng;
