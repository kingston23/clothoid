% Svodenje kuta na [0,pi>
function [a] = NormAngle_Zero_PI(Angle)

a = Angle - floor(Angle/pi)*pi;
