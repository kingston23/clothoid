% Svodenje kuta na [-pi/4,pi/4>. Efekt je isti ko kad bi kutu dodavali ili oduzimali pi/2 tak dugo dok ne upadne u zeljeni interval.
function a = NormAngle_MinusPIQv_PIQv(Angle) % Jebo te ime funkcije :-)

PI_2 = pi/2;
Angle = Angle - floor(Angle/PI_2)*PI_2; % Svodenje na [0,pi/2> (radi i za neg. kutove)

ind = find(Angle>=pi/4);
Angle(ind) = Angle(ind) - PI_2;
a = Angle;