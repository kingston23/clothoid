% Svodenje kuta na [0,2pi>
function a = NormAngle_Zero_2PI(Angle)

PI2 = 2*pi;
Angle = Angle - floor(Angle/PI2)*PI2; % Svodenje na [0,2pi> (radi i za neg. kutove)
a = Angle;
