function bRes = GE_IsEqualApr(x, y)
% Ispituje da li su dva broja priblizno jednaka (eps)
global CGeometry;

if(abs(x-y)<CGeometry.eps)
	bRes = true; return;
else
	bRes = false; return;
end
