function DrawLineImp(Line, xInterval, yInterval, mode)
% Funkcija za crtanje implicitno zadanog pravca
% Pravac je zadan jednadzbom Ax+By+C=0
% Koeficijenti se zadaju u strukturi Line (clanovi su Line.A, Line.B, Line.C)
% 
if(nargin<4)
    mode = 'b';
end

if(abs(Line.B)>abs(Line.A))
    x1 = xInterval(1);
    x2 = xInterval(2);
    y1 = -1/Line.B*(Line.A*x1+Line.C);
    y2 = -1/Line.B*(Line.A*x2+Line.C);
    plot([x1 x2], [y1 y2]);
else
    y1 = yInterval(1);
    y2 = yInterval(2);
    x1 = -1/Line.A*(Line.B*y1+Line.C);
    x2 = -1/Line.A*(Line.B*y2+Line.C);
    plot([x1 x2], [y1 y2], mode);
end
