function [bSuccess DepartPath] = SmoothDepart(GoalState, SmoothedPath)
% Funkcija pronalazi glatki spoj trenutnog stanja sa izgladenom putanjom
%
% GoalState - ciljano stanje, struktura sa clanovima [x y th k], pritom k mora biti 0
% SmoothedPath - izgladena putanja, mora pocinjati linijom na koju se mozemo "prizemljiti"

if(GoalState.k == 0)
    [bSuccess DepartPath] = SmoothDepartLineLine(GoalState, SmoothedPath);
else
    % Zakrivljenost u cilju mora biti nula
    bSuccess = false;
end

return;