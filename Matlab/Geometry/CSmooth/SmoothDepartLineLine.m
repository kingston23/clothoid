function [bSuccess DepartPath] = SmoothDepartLineLine(GoalState, SmoothedPath)
% Pronalazi glatki spoj trenutnog stanja sa krajnjim dijelom (izgladene) putanje
%
% GoalState - zeljeno stanje, struktura sa clanovima [x y th k], pritom k mora biti 0
% SmoothedPath - izgladena putanja, mora zavrsavati linijom na koju se mozemo "prizemljiti"

global CGeometry;

if(isempty(SmoothedPath))
    bSuccess = false;
    return;
end

EndPathSegment = SmoothedPath{end};

if(~strcmp(EndPathSegment.Type, 'LPB'))
    bSuccess = false;
    return;
end

[S.x S.y] = LP_GetPoint(EndPathSegment, 0); % Startna tocka
[M.x M.y] = LP_GetPoint(EndPathSegment, EndPathSegment.m_Length); % Srednja tocka
dMax = 1e100;
eMax = 1e100;
sMax = 1;
G.x = GoalState.x; % Ciljna tocka
G.y = GoalState.y;

% Ako su S, M, G na istom pravcu, onda nam za spajanje treba samo jedan pravac
% Za ispitivanje kolinearnosti se koristi ova formula z = (x2 - x1) * (y3 - y2) - (y2 - y1) * (x3 - x2);
% Ako ona daje ~0, tada su tocke kolinearne
z = (M.x-S.x)*(G.y-M.y) - (M.y-S.y)*(S.x-M.x);
if(z<CGeometry.eps)
    DepartPath{1} = EndPathSegment;
    DepartPath{1}.m_Length = sqrt((G.x-S.x)^2 + (G.y-S.y)^2);
    bSuccess = true;
else
    [bSuccess CL1 CL2 StartLine EndLine] = SmoothSharpTurn(S, M, G, dMax, eMax, sMax);
    nSmoothedPathSegments = 0;

    if(bSuccess)
        if(exist('StartLine', 'var') && isstruct(StartLine))
            nSmoothedPathSegments = nSmoothedPathSegments+1;
            DepartPath{nSmoothedPathSegments} = StartLine;
        end

        nSmoothedPathSegments = nSmoothedPathSegments+1;
        DepartPath{nSmoothedPathSegments} = CL1;

        nSmoothedPathSegments = nSmoothedPathSegments+1;
        DepartPath{nSmoothedPathSegments} = CL2;

        if(exist('EndLine', 'var') && isstruct(EndLine))
            nSmoothedPathSegments = nSmoothedPathSegments+1;
            DepartPath{nSmoothedPathSegments} = EndLine;
        end
    end
end

return;