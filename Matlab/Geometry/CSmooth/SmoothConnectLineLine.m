function [bSuccess ConnectPath] = SmoothConnectLineLine(CurrentState, SmoothedPath)
% Pronalazi glatki spoj trenutnog stanja sa (izgladenom) putanjom
%
% CurrentState - trenutno stanje
% SmoothedPath - izgladena putanja, mora pocinjati linijom na koju se mozemo "prizemljiti"

if(isempty(SmoothedPath))
    bSuccess = false;
    return;
end

BeginPathSegment = SmoothedPath{1};

if(~strcmp(BeginPathSegment.Type, 'LPB'))
    bSuccess = false;
    return;
end

S.x = CurrentState.x;
S.y = CurrentState.y;
[M.x M.y] = LP_GetPoint(BeginPathSegment, 0);
[G.x G.y] = LP_GetPoint(BeginPathSegment, BeginPathSegment.m_Length);
dMax = 1e100;
eMax = 1e100;
sMax = 1;

[bSuccess CL1 CL2 StartLine EndLine] = SmoothSharpTurn(S, M, G, dMax, eMax, sMax);
nSmoothedPathSegments = 0;

if(bSuccess)
    if(exist('StartLine', 'var') && isstruct(StartLine))
        nSmoothedPathSegments = nSmoothedPathSegments+1;
        ConnectPath{nSmoothedPathSegments} = StartLine;
    end

    nSmoothedPathSegments = nSmoothedPathSegments+1;
    ConnectPath{nSmoothedPathSegments} = CL1;

    nSmoothedPathSegments = nSmoothedPathSegments+1;
    ConnectPath{nSmoothedPathSegments} = CL2;

    if(exist('EndLine', 'var') && isstruct(EndLine))
        nSmoothedPathSegments = nSmoothedPathSegments+1;
        ConnectPath{nSmoothedPathSegments} = EndLine;
    end
end

return;