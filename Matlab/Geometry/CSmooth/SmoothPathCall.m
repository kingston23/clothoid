if(~exist('CGeometry'))
    GE_Init;
end
if(~exist('LT'))
    load LT;
end

CurrentState.x = 0.55; CurrentState.y = 0.04; CurrentState.th = pi/4; CurrentState.k = 1;
GoalState.x = 1; GoalState.y = 1.5; GoalState.th = 0; GoalState.k = 0;
Path = [
    CurrentState.x CurrentState.y;
    0.6 0;
    0.5 0.5;
    1 0.5;
    1.6 1;
    GoalState.x GoalState.y
    ];

[bSuccess FinalSmoothedPath] = SmoothPath(CurrentState, GoalState, Path);

clf
plot([CurrentState.x; Path(:,1); GoalState.x], [CurrentState.y; Path(:,2); GoalState.y], 'b');
hold on
CL_DrawClothoidalPath(FinalSmoothedPath)

axis equal
