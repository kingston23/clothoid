function [bSuccess FinalSmoothedPath] = SmoothPath(CurrentState, GoalState, Path)
% Glavna funkcija za izgladivanje putanje
%
% Path - putanja zadana kao matrica tocaka, svaki redak je jedna tocka [x y]
%   - pretpostavka je da je prva tocka putanje jednaka poziciji u CurrentState
% CurrentState - trenutno stanje zadano kao struktura sa [x y th k]
% GoalState - ciljno stanje zadano kao struktura sa [x y th k], k (zakrivljenost) mora biti nula

% Izlazni argumenti
bSuccess = false;
FinalSmoothedPath = [];

bDirectConnectionPossible = DirectConnectionPossible();

if(bDirectConnectionPossible)
    bSuccess = DirectConnect();
else
    % Izbaci prvu i zadnju tocku iz putanje, jer su one na pocetnoj i konacnoj poziciji,
    % a za spajanje pocetne i konacne pozicije i ostatka putanje koristimo funkcije Connect i Depart
    Path2 = Path(2:end-1,:);
    % Izgladi po dijelovima ravni dio putanje klotoidama
    Clearance = 0.2;
    SmoothedPath = GR_SmoothPLPath2(Path2, Clearance);
    
    [bSuccess1 ConnectPath] = SmoothConnect(CurrentState, SmoothedPath);
    [bSuccess2 DepartPath] = SmoothDepart(GoalState, SmoothedPath);
    if(bSuccess1 && bSuccess2)
       % FinalSmoothedPath = ConnectPath + SmoothedPath + DepartPath; % Spajanje svih putanja u jednu konacnu putanju
       % Iz SmoothedPath je izbacena pocetna i krajnja linija,
       % buduci da ce umjesto njih biti novi segmenti u ConnectPath i DepartPath
       FinalSmoothedPath = {ConnectPath{:} SmoothedPath{2:end-1} DepartPath{:}};
       bSuccess = true;
    end
end

if(~bSuccess)
    EmergencyStop();
end

return;
