function [bSuccess ConnectSegment] = SmoothConnect(CurrentState, SmoothedPath)
% Funkcija pronalazi glatki spoj trenutnog stanja sa izgladenom putanjom
%
% CurrentState - trenutno stanje
% SmoothedPath - izgladena putanja, mora pocinjati linijom na koju se mozemo "prizemljiti"

if(CurrentState.k == 0)
    [bSuccess ConnectSegment] = SmoothConnectLineLine(CurrentState, SmoothedPath);
else
    [bSuccess ConnectSegment] = CL_ConnectCircleLine2C_2(CurrentState, SmoothedPath);
    if(bSuccess)
        disp('CL_ConnectCircleLine2C is the funkcija');
        return;
    end
    if(~bSuccess)
        [bSuccess ConnectSegment] = CL_ConnectCircleLine1C_2(CurrentState, SmoothedPath);
    end
    if(bSuccess)
        disp('CL_ConnectCircleLine1C is the funkcija');
        return;
    end
    if(~bSuccess)
        [bSuccess ConnectSegment] = CL_ConnectCircleLine3C_2(CurrentState, SmoothedPath);
    end
    if(bSuccess)
        disp('CL_ConnectCircleLine3C is the funkcija');
        return;
    end
end

return;