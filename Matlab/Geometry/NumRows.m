function n = NumRows(x)

n = size(x);
n = n(1);