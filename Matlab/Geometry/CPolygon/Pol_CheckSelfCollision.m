function [bCollisionFound Polygon IntersectionPoints AllIntersectionPoints] = Pol_CheckSelfCollision(this)
% Provjerava dal poligon sijece sam sebe.
% Ako da, onda ga dijeli na "potpoligone" koji ne sijeku sami sebe
% this - matrica s tockama poligona dimenzije n x 2 (prvi stupac x-ovi, drugi stupac y-oni)
% Polygon - potpoligoni (cell)
% IntersectionPoints - tocke u kojima potpoligon dodiruje druge potpoligone (tj. tocke u kojima je rastavljen) (cell)
% AllIntersectionPoints - sve dodirne tocke svih poligona u nizu u kojem se ne ponavljaju dvije iste (matrica)

bCollisionFound = false;
epsilon = 1e-10;

if(length(this) <= 3)
    return;
end

iPolygon = 1; % Na koliko poligona smo rastavili ulazni poligon, ako nismo nista rastavili izlazni poligon je jednak ulaznom

Polygon{iPolygon} = this; % Ovdje ce na kraju biti niz jednostavnih potpoligona koji se ne sijeku
IntersectionPoints{iPolygon} = [];
AllIntersectionPoints = zeros(0,2);
bPolygonChecked(iPolygon) = false; % Ovaj niz zastavica govori dal je pojedini poligon provjeren za samosijecu

while(iPolygon ~= -1)

    nPoints = length(Polygon{iPolygon});

    % Stavi prvu tocku na kraj (zaokruzivanje poligona)
    Polygon{iPolygon} = [Polygon{iPolygon} ; [Polygon{iPolygon}(1,1) Polygon{iPolygon}(1,2)]];

    i=1;
    while(i<=nPoints-2)

        x1 = Polygon{iPolygon}(i,1);
        y1 = Polygon{iPolygon}(i,2);

        x2 = Polygon{iPolygon}(i+1,1);
        y2 = Polygon{iPolygon}(i+1,2);

        % Provuci implicitni pravac kroz te tocke
        A1 = y1-y2;
        B1 = x2-x1;
        C1 = x1*y2-x2*y1;

        if(i==1)
            jMax = nPoints - 1; % Za prvi brid nejdemo do onog zadnjeg brida jer su prvi i zadnji susjedi, a susjede ne treba ispitivati
        else
            jMax = nPoints;
        end

        for(j=i+2:jMax)

            x3 = Polygon{iPolygon}(j,1);
            y3 = Polygon{iPolygon}(j,2);

            x4 = Polygon{iPolygon}(j+1,1);
            y4 = Polygon{iPolygon}(j+1,2);

            A2 = y3-y4;
            B2 = x4-x3;
            C2 = x3*y4 - x4*y3;

            % Provjeri sjecu
            Temp = A1*B2-A2*B1;

            if(abs(Temp) < epsilon)
                % Ak su pravci priblizno paralelni, onda ih ne razmatramo
                % TUDU: trebalo bi provjeriti dal su jako blizu, ak jesu, onda se moze uzeti kao da se sijeku
                continue;
            end

            % Nadi sjeciste
            xc = (B1*C2-B2*C1)/Temp;
            yc = (A2*C1-A1*C2)/Temp;

            % Provjeri dal je sjeciste nalazi unutar oba omedena pravca
            bCollision = false;
            if(abs(B1)>abs(A1)) % Horizontalan
                % TUDU: ove provjere bi trebalo napraviti sa epsilon
                if((x1<=xc && xc<=x2) || (x2<=xc && xc<=x1))
                    bCollision = true;
                end
            else
                if((y1<=yc && yc<=y2) || (y2<=yc && yc<=y1))
                    bCollision = true;
                end
            end

            if(bCollision) % Provjeri i za drugi pravac
                bCollision = false;

                if(abs(B2)>abs(A2)) % Horizontalan
                    % TUDU: ove provjere bi trebalo napraviti sa epsilon
                    if((x3<=xc && xc<=x4) || (x4<=xc && xc<=x3))
                        bCollision = true;
                    end
                else
                    if((y3<=yc && yc<=y4) || (y4<=yc && yc<=y3))
                        bCollision = true;
                    end
                end
            end

            if(bCollision)
                
                % Razbijamo ga na dva komada
                
                % Spremimo novu presjecnu tocku
                AllIntersectionPoints = [AllIntersectionPoints; [xc yc]];
                
                % Prvi dio - dodamo na kraj
                Polygon{end+1} = [xc yc; Polygon{iPolygon}(i+1:j,:)];
                bPolygonChecked(end+1) = false;
                
                IntersectionPoints{end+1} = [xc yc];

                % Drugi dio - aha, izbacimo sve tocke izmedu (x1,y1) i (x4,y4) i izmedu njih ubacimo samo sjeciste (xc,yc)
                Polygon{iPolygon}(i+2:j,:) = []; % Izbacivanje tocaka izmedu, osim (x2,y2) umjesto kojeg ubacujemo (xc,yc)
                Polygon{iPolygon}(i+1,:) = [xc yc]; % Ubac (xc,yc)
                
                IntersectionPoints{iPolygon} = [IntersectionPoints{iPolygon}; [xc yc]];

                % Apdejtaj brojace itd
                nPoints = length(Polygon{iPolygon})-1;
                i=i-1; % vrati se opet na isti poligon, mozda se on jos negdi sjece sam sa sobom
                break; % izadi iz nutarnje petlje
            end

        end
        i = i+1;

    end

    % Ovaj je ok
    Polygon{iPolygon}(end,:) = []; % Izbaci zadnju tocku (jer je jednaka prvoj)
    bPolygonChecked(iPolygon) = true;
    
    % Provjeri dal ima jos poligona za provjeriti
    bAllChecked = true;
    for(iPolygon=1:length(Polygon))
        if(bPolygonChecked(iPolygon) == false)
            bAllChecked = false;
            break;
        end
    end
    if(bAllChecked)
        iPolygon=-1; % nema ih vec
    end
    
end

iPolygon=1;
while(iPolygon<=length(Polygon))
    if(isempty(Polygon(iPolygon)))
        Polygon(iPolygon) = [];
    else
        iPolygon = iPolygon+1;
    end
end

if(length(Polygon) > 1)
    bCollisionFound = true;
end
