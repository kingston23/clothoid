function Seg_Draw(this, color)
% Ova funkcija crta niz segmenata
%
% this - matrica s kranjnjim tockama linijskih segmenata dimenzije n x 4 (poredak x1 y1 x2 y2)
% color - boja (argument za plot funkciju)

if(isempty(this))
    return;
end

if(nargin<2)
    color = 'b';
end

for(i=1:NumRows(this))
   plot([this(i,1) this(i,3)],  [this(i,2) this(i,4)], color);
   %plot3([this(i,1) this(i,3)],  [this(i,2) this(i,4)], 0.1*[1 1], color);
end
