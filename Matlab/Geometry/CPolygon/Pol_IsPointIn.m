function bIsInside = Pol_IsPointIn(this, Point)
% Testira dal je tocka u poligonu

global CGeometry;

% Provjeri dal je tocka na rubu (eps okolica)

n = NumRows(this);

this = [this; this(1,:)]; % Prosiri privremeno

eps2 = CGeometry.eps^2;


for(i=1:n)
    Line = LIB_LineFromPoints(this(i,1), this(i,2), this(i+1,1), this(i+1,2));
    if(LIB_GetDistance2ToPoint(Line, Point(1), Point(2)) < eps2)
        bIsInside = true;
        return;
    end
end

this(end,:) = [];
bIsInside = inpoly(Point, this);