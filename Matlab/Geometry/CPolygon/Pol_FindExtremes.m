function [xMin xMax yMin yMax] = Pol_FindExtremes(this)
% Nalazi ekstremne tocke poligona (min i max x, min i max y)
% this - poligona zadan kao n x 2 matrica

% Ekstremne tocke
xMin = min(this(:,1));
xMax = max(this(:,1));
yMin = min(this(:,2));
yMax = max(this(:,2));
