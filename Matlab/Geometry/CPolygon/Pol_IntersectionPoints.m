function [IntersectionPoints] = Pol_IntersectionPoints(P1, P2)
% Pronalazi presjecne tocke izmedu 2 poligona. Koristi funkciju curveintersect
%   P1, P2 - poligoni zadani kao n x 2 matrice
%   IntersectionPoints - m x 2 matrica sa sjecistima
%

[x y] = curveintersect([P1(:,1); P1(1,1)], [P1(:,2); P1(1,2)], [P2(:,1); P2(1,1)], [P2(:,2); P2(1,2)]);

IntersectionPoints = [x y];