function [ReversedPolygon] = Pol_Reverse(this)
% Mijenja "smjer" poligona iz clockwise u counterclockwise i obrnuto
%
% this - poligon zadan kao matrica

n = length(this);

ReversedPolygon = zeros(size(this));

for(i=1:n)
    ReversedPolygon(n+1-i, :) = this(i, :);
end