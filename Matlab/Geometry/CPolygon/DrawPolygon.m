function DrawPolygon(this, color, fill)
% this - Poligon zadan kao matrica s tockama dimenzije n x 2 (prvi stupac x-ovi, drugi stupac y-oni), ili cell sa vise takvih matrica, ili polje struktura
% color - boja (argument za plot funkciju)
% fill - ako je 1, onda se crta puni poligon, inace samo rubovi

if(isempty(this))
    return;
end

if(nargin<2)
    color = 'brgymck';
end

if(nargin<3)
    fill = false;
end

bHold = ishold();
hold on;

if(isstruct(this))
    this = Pol_StructToMatrix(this);
end

if(iscell(this))
    
    for(i=1:length(this))
        Polygon = this{i};
        
        if(NumRows(Polygon) < 1)
            continue;
        end
        
        if(ischar(color) && length(color)>1)
            currentcolor = color(rem(i-1,7)+1);
        else
            currentcolor = color;
        end
        
        if(fill)
            patch(Polygon(:,1), Polygon(:,2), currentcolor);
        else
            Polygon = [Polygon; [Polygon(1,1) Polygon(1,2)]]; % Zaokruzi poligon
            plot(Polygon(:,1), Polygon(:,2), currentcolor);
        end
    end
    
    if(bHold)
        hold on;
    else
        hold off;
    end
    
else
    
    if(nargin<2)
        currentcolor = 'b';
    else
        currentcolor = color;
    end
    
    Polygon = this;
    
    if(NumRows(Polygon) < 1)
        return;
    end
    
    if(fill)
        h = patch(Polygon(:,1), Polygon(:,2), currentcolor);
        % set(h, 'FaceColor', currentcolor);
    else
        Polygon = [Polygon; [Polygon(1,1) Polygon(1,2)]]; % Zaokruzi poligon
        plot(Polygon(:,1), Polygon(:,2), 'color', currentcolor);
    end

end
