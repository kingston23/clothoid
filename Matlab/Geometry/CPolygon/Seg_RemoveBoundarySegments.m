function Segments = Seg_RemoveBoundarySegments(this, Polygon, epsilon)
% Funkcija uklanja one segmente iz niza koji imaju pocetnu ili konacnu tocku na rubu poligona
%
% this - matrica s krajnjim tockama linijskih segmenata dimenzije n x 4 (poredak x1 y1 x2 y2)

if(isempty(this))
    return;
end

Segments = [];

for(i=1:NumRows(this))
    x1 = this(i,1);
    y1 = this(i,2);
    x2 = this(i,3);
    y2 = this(i,4);
    
    if(Pol_IsPointOnBoundary(Polygon, [x1 y1], epsilon))
        continue;
    end
    
    if(Pol_IsPointOnBoundary(Polygon, [x2 y2], epsilon))
        continue;
    end
        
    Segments = [Segments; this(i,:)];
end

