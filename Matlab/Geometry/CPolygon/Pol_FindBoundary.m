function [iBoundary] = Pol_FindBoundary(this)
% Pokusava naci poligon koji je boundary svim ostalim poligonima u nizu - to bude poligon koji sadrzi sve ekstremne tocke.
% this - cell poligona zadanih kao n x 2 matrice
%   

% Ekstremne tocke
xMin = min(this{1}(:,1));
yMin = min(this{1}(:,2));
xMax = max(this{1}(:,1));
yMax = max(this{1}(:,2));

% Kojem poligonu pripadaju ekstremne tocke - zasad prvom
ixMin = 1;
iyMin = 1;
ixMax = 1;
iyMax = 1;

n = length(this);

for (i=2:n)
    xMinNew = min(this{i}(:,1));
    if(xMinNew < xMin)
        xMin = xMinNew;
        ixMin = i;
    end
    
    yMinNew = min(this{i}(:,2));
    if(yMinNew < yMin)
        yMin = yMinNew;
        iyMin = i;
    end
    
    xMaxNew = max(this{i}(:,1));
    if(xMaxNew > xMax)
        xMax = xMaxNew;
        ixMax = i;
    end
    
    yMaxNew = max(this{i}(:,2));
    if(yMaxNew > yMax)
        yMax = yMaxNew;
        iyMax = i;
    end
end

if(ixMin==iyMin && iyMin==ixMax && ixMax==iyMax)
    % Sve ekstremne tocke potjecu od istog poligona - taj je vjerojatno boundary,
    % al nemremo to sigurno tvrditi.
    iBoundary = ixMin;
else
    iBoundary = -1;
end
