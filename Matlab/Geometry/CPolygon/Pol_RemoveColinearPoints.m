function [CleanPolygon] = Pol_RemoveColinearPoints(this)
% this - poligon zadan kao n x 2 matrica ili cell takvih matrica
% CleanPolygon - poligon bez kolinearnih vrhova
%
% Funkcija uklanja kolinearne vrhove iz poligona

global CGeometry;

epsilon = CGeometry.eps;

bIsCell = true;
if(~iscell(this))
    this{1} = this;
    bIsCell = false;
end

for(iPolygon=1:length(this))
    n = length(this{iPolygon});

    IsReflex = Pol_IsReflexVertex(this{iPolygon});

    i = 1;
    j = 1;
    while(i<=n)
        if(abs(IsReflex(i)) < epsilon)
            % ukloni ovu tocku - kolinearna je
            this{iPolygon}(j,:) = [];
            j = j-1;
        end
        j = j+1;
        i = i+1;
    end
end

if(bIsCell)
    CleanPolygon = this;
else
    CleanPolygon = this{1};
end
