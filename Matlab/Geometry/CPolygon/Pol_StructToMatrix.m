function [Polygon] = Pol_StructToMatrix(this)
% Pretvara poligon iz formata strukture (kao za PolygonClip biblioteku) u matricu n x 2

for(i=1:length(this))
    
    nPoints = length(this(i).x);
    
    Polygon{i} = zeros(nPoints, 2);
    
    Polygon{i}(:,1) = this(i).x;
    Polygon{i}(:,2) = this(i).y;
    
end