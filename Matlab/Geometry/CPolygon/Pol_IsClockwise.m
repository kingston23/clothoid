function [Output] = Pol_IsClockwise(this)
%   this - poligon zadan kao n x 2 matrica
%   Return the clockwise status of a closed curve (polygon), clockwise or counterclockwise
%   n vertices making up curve p
%   return 0 for incomputables eg: colinear points
%          CLOCKWISE == 1
%          COUNTERCLOCKWISE == -1
%   It is assumed that
%   - the polygon is closed
%   - the last point is not repeated.
%   - the polygon is simple (does not intersect itself or have holes)
%

CLOCKWISE = 1;
COUNTERCLOCKWISE = -1;


n = NumRows(this);

if (n < 3)
  Output = 0;
  return
end

% Zaokruzi poligon dodavanjem prve dvije tocke opet na kraj
this = [this; [this(1,1) this(1,2)]; [this(2,1) this(2,2)]];
    
count = 0;


for (i=1:n)
  j = (i + 1); % n;
  k = (i + 2); % n;
  z  = (this(j,1) - this(i,1)) * (this(k,2) - this(j,2));
  z = z - (this(j,2) - this(i,2)) * (this(k,1) - this(j,1));
  if (z < 0)
     count = count - 1;
  elseif (z > 0)
     count = count + 1;
  end
  
end
      
if (count > 0)
    Output = COUNTERCLOCKWISE;
elseif (count < 0)
    Output = CLOCKWISE;
else
    Output = 0;
end
