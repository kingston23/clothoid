function [BoundingRect] = Pol_GetBoundingRect(this, epsilon)
% Daje pravokutnik koji potpuno sadrzi neki poligon i siri je za epsilon
% Taj pravokutnik bude vracen kao poligon u negativnom smjeru (clockwise)
% this - poligon zadan kao n x 2 matrica
% epsilon - za kolko siri mora biti pravokutnik od poligona

% Ekstremne tocke
[xMin xMax yMin yMax] = Pol_FindExtremes(this);

xMin = xMin-epsilon;
xMax = xMax+epsilon;
yMin = yMin-epsilon;
yMax = yMax+epsilon;

BoundingRect = [xMin yMin; xMin yMax; xMax yMax; xMax yMin];
