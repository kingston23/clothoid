function [IsReflex] = Pol_IsReflexVertex(this)
% this - poligon zadan kao n x 2 matrica
%
% Funkcija ispituje dal je pojedini vertex poligona refleksan (tj. dal je vanjski kut > 180 stupnjeva)
% IsReflex - broj za svaki vrh poligona. Ako je taj broj nula, vrh je kolinearan.
%   - Ako je negativan onda je refleksan vrh

n = NumRows(this);

IsReflex = zeros(n+1, 1); % jedan element vise radi trika, kasnije to ispravljamo

% if(n<3)
%     IsReflex = ones(n, 1);
%     return;
% end

% Zaokruzi poligon dodavanjem prve dvije tocke opet na kraj
this = [this; [this(1,1) this(1,2)]; [this(2,1) this(2,2)]];
    
count = 0;

if (n < 3)
  IsReflex = 0;
  return
end

for (i=1:n)
  j = (i + 1); % n;
  k = (i + 2); % n;
  x1 = this(i,1);
  y1 = this(i,2);
  x2 = this(j,1);
  y2 = this(j,2);
  x3 = this(k,1);
  y3 = this(k,2);
  
  z = (x2 - x1) * (y3 - y2) - (y2 - y1) * (x3 - x2);
  %z  = (this(j,1) - this(i,1)) * (this(k,2) - this(j,2));
  %z = z - (this(j,2) - this(i,2)) * (this(k,1) - this(j,1));
  if (z < 0)
      IsReflex(i+1) = z;
     count = count - 1;
  elseif (z > 0)
      IsReflex(i+1) = z;
     count = count + 1;
  end
  
end

IsReflex(1) = IsReflex(end);

IsReflex(end) = []; % mici ovog zadnjeg