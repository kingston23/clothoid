function bRes = GE_IsPointEqual(P1, P2)
% Ispituje dal je tocka priblizno jednaka drugoj tocki
global CGeometry;

dx = P1(1) - P2(1);
dy = P1(2) - P2(2);

if(abs(dx)<CGeometry.eps && abs(dy)<CGeometry.eps)
	bRes = true; return;
else
	bRes = false; return;
end
