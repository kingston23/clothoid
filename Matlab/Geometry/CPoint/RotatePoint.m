function [x_rot y_rot] = RotatePoint(x,y, angRot)
% Funkcija koja rotira tocku (x,y) za kut angRot (u radijanima) oko ishodista

C = cos(angRot);
S = sin(angRot);
x_rot = C*x - S*y;
y_rot = S*x + C*y;