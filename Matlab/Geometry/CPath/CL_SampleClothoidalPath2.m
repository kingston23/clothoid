function SampledPath = CL_SampleClothoidalPath2(Path, s)
% Uzorkuje putanju koja ima klotoide
%
% Path - cell niz klotoida i linijskih segmenata (krugovi zasad nisu podrzani)
% s - vektor sa udaljenostima na putanji u kojima hocemo dobiti uzorke - pocetak putanje je nula.
% Niz struktura sa clanovima x,y,th,k,dk

nPathSegments = length(Path);

iCurrentPathSegment = 1;
sCurrent = s(1);

iSample = 1;

PathSegmentStart = 0;

finished = false;

while(iCurrentPathSegment <= nPathSegments)
    
    if(finished)
        break;
    end
   
    % Dohvati trenutni segment
    PathSegment = Path{iCurrentPathSegment};
    
    % Duljina trenutnog segmenta
    if(strcmp(PathSegment.Type, 'CL'))
        SegmentLength = PathSegment.length;
    elseif(strcmp(PathSegment.Type, 'LPB'))
        SegmentLength = PathSegment.m_Length;
    end
    
    PathSegmentEnd = PathSegmentStart + SegmentLength;
    
    % Uzorkuj trenutni segment do kraja
    while(sCurrent <= PathSegmentEnd)
        
        sLocal = sCurrent-PathSegmentStart;
        
        SampledPath(iSample).s = sCurrent;
        
        if(strcmp(PathSegment.Type, 'CL'))
            [SampledPath(iSample).x SampledPath(iSample).y] = GetClothoidPoint(PathSegment, sLocal);
            SampledPath(iSample).th = PathSegment.th0 + PathSegment.k0*sLocal + 0.5*PathSegment.c*sLocal^2;
            SampledPath(iSample).k = PathSegment.k0 + PathSegment.c*sLocal;
            SampledPath(iSample).dk = PathSegment.c;
        elseif(strcmp(PathSegment.Type, 'LPB'))
            [SampledPath(iSample).x SampledPath(iSample).y] = LP_GetPoint(PathSegment, sLocal);
            SampledPath(iSample).th = PathSegment.m_Direction;
            SampledPath(iSample).k = 0;
            SampledPath(iSample).dk = 0;
        end
        
        iSample = iSample+1;
        
        if(iSample>length(s))
            finished = true;
            break;
        end
        
        sCurrent = s(iSample);
    end
    
    iCurrentPathSegment = iCurrentPathSegment+1;
    PathSegmentStart = PathSegmentStart + SegmentLength;
end
