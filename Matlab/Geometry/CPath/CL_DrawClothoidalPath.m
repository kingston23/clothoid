function CL_DrawClothoidalPath(Path, Color)
% Crta putanju koja ima klotoide
%
% Path - cell niz klotoida, kruznih lukova i linijskih segmenata

bHold = ishold();
hold on;

if(nargin<2)
    Color = 'r';
end

for(i=1:length(Path))
    
    if(strcmp(Path{i}.Type, 'CL'))
        CL_Draw(Path{i}, Color);
    elseif(strcmp(Path{i}.Type, 'LPB'))
        LPB_DrawLine(Path{i}, Color);
	elseif(strcmp(Path{i}.Type, 'CA'))
		CA_DrawArc(Path{i}, Color);
    end 
    
end

if(bHold)
    hold on;
else
    hold off;
end