function PathLength = PA_GetPathLength(Path)
% Racuna duljinu putanje
%
% Path - cell niz klotoida i linijskih segmenata (krugovi zasad nisu podrzani)

PathLength = 0;

for(i=1:length(Path))
    
    switch(Path{i}.Type)
        case 'CL'
            PathLength = PathLength + Path{i}.length;
            %break;
        case 'LPB'
            PathLength = PathLength + Path{i}.m_Length;
            %break;
        otherwise
            error('Nepodrzani segment putanje');
    end 
    
end
