function SampledPath = CL_SampleClothoidalPath(Path, ds)
% Uzorkuje putanju koja ima klotoide
%
% Path - cell niz klotoida i linijskih segmenata (krugovi zasad nisu podrzani)
% ds - period uzorkovanja, tj. udaljenost izmedu dva uzorka na putanji
% Niz struktura sa clanovima x,y,th,k,dk

nPathSegments = length(Path);

iCurrentPathSegment = 1;
s = 0;

iSample = 1;

PathSegmentStart = 0;

while(iCurrentPathSegment <= nPathSegments)
   
    % Dohvati trenutni segment
    PathSegment = Path{iCurrentPathSegment};
    
    % Duljina trenutnog segmenta
    if(strcmp(PathSegment.Type, 'CL'))
        SegmentLength = PathSegment.length;
    elseif(strcmp(PathSegment.Type, 'LPB'))
        SegmentLength = PathSegment.m_Length;
    elseif(strcmp(PathSegment.Type, 'CA'))
        SegmentLength = PathSegment.m_Length;
    end
    
    PathSegmentEnd = PathSegmentStart + SegmentLength;
    
    % Uzorkuj trenutni segment do kraja
    while(s <= PathSegmentEnd)
        
        sLocal = s-PathSegmentStart;
        
        SampledPath(iSample).s = s;
        
        if(strcmp(PathSegment.Type, 'CL'))
            [SampledPath(iSample).x SampledPath(iSample).y] = GetClothoidPoint(PathSegment, sLocal);
            SampledPath(iSample).th = PathSegment.th0 + PathSegment.k0*sLocal + 0.5*PathSegment.c*sLocal^2;
            SampledPath(iSample).k = PathSegment.k0 + PathSegment.c*sLocal;
            SampledPath(iSample).dk = PathSegment.c;
        elseif(strcmp(PathSegment.Type, 'LPB'))
            [SampledPath(iSample).x SampledPath(iSample).y] = LP_GetPoint(PathSegment, sLocal);
            SampledPath(iSample).th = PathSegment.m_Direction;
            SampledPath(iSample).k = 0;
            SampledPath(iSample).dk = 0;
		elseif(strcmp(PathSegment.Type, 'CA'))
            [SampledPath(iSample).x SampledPath(iSample).y SampledPath(iSample).th] = CA_GetPoint(PathSegment, sLocal);
            SampledPath(iSample).k = 1/ PathSegment.m_r;
            SampledPath(iSample).dk = 0;
        else
            error('Nepodrzani tip');
        end
        s = iSample * ds;
        iSample = iSample+1;
    end
    
    iCurrentPathSegment = iCurrentPathSegment+1;
    PathSegmentStart = PathSegmentStart + SegmentLength;
end
