function SmoothedPath = GR_SmoothPLPath2(PathPoints, Clearance)
% Igladuje piecewise linear putanju klotoidama.
% Kolinearne tocke moraju bit izbacene iz putanje.
%
% PathPoints - niz tocaka putanje, svaki redak je jedna tocka [x y]
% Clearance - minimalni slobodni prostor oko svakog segmenta

% Opcije

% Dal prvi/zadnji segment moze biti koristen cijeli ili samo njegova polovica.
% (Od ostalih segmenata koristi se jedna polovica za ulazni zavoj, i druga za izlazni)
bUseWholeFirstSegment = false;
bUseWholeEndSegment = false;

VeryFirstLine = [];
VeryLastLine = [];

nSmoothedPathSegments = 0;

nTurns = NumRows(PathPoints) - 2;

SmoothedPath = [];

for(iTurn=1:nTurns)
    
    % Start tocka prvog segmenta
    S.x = PathPoints(iTurn, 1);
    S.y = PathPoints(iTurn, 2);
    
    % Srednja tocka
    M.x = PathPoints(iTurn+1, 1);
    M.y = PathPoints(iTurn+1, 2);
    
    % Krajnja tocka drugog segmenta
    G.x = PathPoints(iTurn+2, 1);
    G.y = PathPoints(iTurn+2, 2);
    
    if(iTurn==1 && bUseWholeFirstSegment)
        d1 = sqrt((M.x-S.x)^2 + (M.y-S.y)^2); % Smijemo prvi segment izgladiti po cijeloj duzini
    else
        d1 = sqrt((M.x-S.x)^2 + (M.y-S.y)^2) / 2; % Ostali segmenti - samo polovica
        
        if(iTurn==1)
            % Polovicu prvog segmenta treba dodati kao pocetnu liniju jer ju nismo koristili
            [Tmp VeryFirstLine] = LP_LineFromPoints(S.x, S.y, M.x, M.y);
            VeryFirstLine.m_Length = d1;
            VeryFirstLine.Type = 'LPB';
        end
        
        S.x = (S.x + M.x)/2;
        S.y = (S.y + M.y)/2;
    end
    
    if(iTurn==nTurns && bUseWholeEndSegment)
        d2 = sqrt((M.x-G.x)^2 + (M.y-G.y)^2); % Smijemo zadnji segment izgladiti po cijeloj duzini
    else
        d2 = sqrt((M.x-G.x)^2 + (M.y-G.y)^2) / 2; % Ostali segmenti - samo polovica
            
        if(iTurn==nTurns)
            % Polovicu krajnjeg segmenta treba dodati kao zavrsnu liniju jer ju nismo koristili
            [Tmp VeryLastLine] = LP_LineFromPoints((G.x + M.x)/2, (G.y + M.y)/2, G.x, G.y);
            VeryLastLine.m_Length = d2;
            VeryLastLine.Type = 'LPB';
        end
        
        G.x = (G.x + M.x)/2;
        G.y = (G.y + M.y)/2;
    end
    
    dMax = min(d1,d2);
    
    eMax = Clearance / sqrt(2);
    
    sMax = 1;
    
    [bOK CL1 CL2 StartLine EndLine] = SmoothSharpTurn(S, M, G, dMax, eMax, sMax);
    
    if(bOK)
        if(~isempty(VeryFirstLine))
            if(~isempty(StartLine))
                % Ujediniti ove dvije linije u jednu
                VeryFirstLine.m_Length = VeryFirstLine.m_Length + StartLine.m_Length;
            end
            nSmoothedPathSegments = nSmoothedPathSegments+1;
            SmoothedPath{nSmoothedPathSegments} = VeryFirstLine;
            VeryFirstLine = [];
        elseif(~isempty(StartLine))
            nSmoothedPathSegments = nSmoothedPathSegments+1;
            SmoothedPath{nSmoothedPathSegments} = StartLine;
        end
        
        nSmoothedPathSegments = nSmoothedPathSegments+1;
        SmoothedPath{nSmoothedPathSegments} = CL1;
        
        nSmoothedPathSegments = nSmoothedPathSegments+1;
        SmoothedPath{nSmoothedPathSegments} = CL2;
        
        if(~isempty(EndLine))
            if(~isempty(VeryLastLine))
                % Ujediniti ove dvije linije u jednu
                EndLine.m_Length = EndLine.m_Length + VeryLastLine.m_Length;
            end
            nSmoothedPathSegments = nSmoothedPathSegments+1;
            SmoothedPath{nSmoothedPathSegments} = EndLine;
            VeryLastLine = [];
        elseif(~isempty(VeryLastLine))
            nSmoothedPathSegments = nSmoothedPathSegments+1;
            SmoothedPath{nSmoothedPathSegments} = VeryLastLine;
            VeryLastLine = [];
        end
    end
    
end
