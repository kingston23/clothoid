function [Path] = GR_PathRemoveColinearPoints(Graph, Path)
% Crta graf zadan kao struktura
%
% Path - niz ID-ova cvorova (pretpostavlja se da oni idu od 1)
% this - poligon zadan kao n x 2 matrica ili cell takvih matrica
% CleanPolygon - poligon bez kolinearnih vrhova
%
% Funkcija uklanja kolinearne vrhove iz poligona

global CGeometry;

epsilon = CGeometry.eps;

n = length(Path);

if(n<=2)
    return;
end

DotProduct = zeros(n,1);

for(j=2:n-1)

    i = (j - 1); % n;
    k = (j + 1); % n;
    
    x1 = Graph.Nodes(Path(i), 2);
    y1 = Graph.Nodes(Path(i), 3);
    x2 = Graph.Nodes(Path(j), 2);
    y2 = Graph.Nodes(Path(j), 3);
    x3 = Graph.Nodes(Path(k), 2);
    y3 = Graph.Nodes(Path(k), 3);

    DotProduct(j) = (x2 - x1) * (y3 - y2) - (y2 - y1) * (x3 - x2);
end

i = 2;
j = 2;

while(i<n)
    if(abs(DotProduct(i)) < epsilon)
        % ukloni ovu tocku - kolinearna je
        Path(j) = [];
        j = j-1;
    end
    j = j+1;
    i = i+1;
end
