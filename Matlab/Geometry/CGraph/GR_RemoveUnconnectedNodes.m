function Graph = GR_RemoveUnconnectedNodes(Graph)
% Uklanja nepovezane cvorove iz grafa.
% Uvjet: svi ID-ovi moraju po redu, tj. 1,2,3,...

nNodes = NumRows(Graph.Nodes);

bNodeUsed = zeros(nNodes, 1);

for(i=1:NumRows(Graph.Edges))
    NodeID1 = Graph.Edges(i,2);
    NodeID2 = Graph.Edges(i,3);
    
    bNodeUsed(NodeID1) = 1;
    bNodeUsed(NodeID2) = 1;
end

Graph.Nodes(~bNodeUsed,:) = []; % Izbaci nekoristene

% Sredi da ID-ovi opet budu 1,2,3,...
nNodes = NumRows(Graph.Nodes);

for(i=1:nNodes)
    OldNodeID = Graph.Nodes(i,1);
    if(OldNodeID ~= i)
        Graph.Nodes(i,1) = i;
        
        % Treba apdejtati i rubove
        for(j=1:NumRows(Graph.Edges))
            if(Graph.Edges(j,2) == OldNodeID)
                Graph.Edges(j,2) = i;
            end
            if(Graph.Edges(j,3) == OldNodeID)
                Graph.Edges(j,3) = i;
            end
        end
    end
end
