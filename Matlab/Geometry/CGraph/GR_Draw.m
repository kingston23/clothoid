function GR_Draw(Graph, Color)
% Crta graf zadan kao struktura

bHold = ishold();
hold on;

% Crtaj cvorove
if(nargin<2)
    Color = 'b.';
end

if(~isempty(Graph.Nodes))
    plot(Graph.Nodes(:,2), Graph.Nodes(:,3), Color, 'LineStyle', 'None', 'Marker', 'o');
end

% Crtaj rubove
if(nargin<2)
    Color = 'b';
end

for(i=1:NumRows(Graph.Edges))
    NodeID1 = Graph.Edges(i,2);
    NodeID2 = Graph.Edges(i,3);
    
    i1 = find(Graph.Nodes(:,1) == NodeID1);
    i2 = find(Graph.Nodes(:,1) == NodeID2);
    
    plot([Graph.Nodes(i1,2) Graph.Nodes(i2,2)], [Graph.Nodes(i1,3) Graph.Nodes(i2,3)], Color);
end

if(bHold)
    hold on;
else
    hold off;
end