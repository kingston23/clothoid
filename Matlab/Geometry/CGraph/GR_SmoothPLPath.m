function SmoothedPath = GR_SmoothPLPath(Graph, Path, Clearance)
% Igladuje piecewise linear putanju klotoidama.
% Kolinearne tocke moraju bit izbacene
%
% Graph - roadmapa zadana kao matrica sa recima [ID x y].
% Path - niz ID-ova cvorova (pretpostavlja se da oni idu od 1)
% Clearance - minimalni slobodni prostor oko svakog segmenta

% Konvertiraj putanju u niz tocaka
PathPoints = GR_ConvertPath(Graph, Path);

% Izgladi ju
SmoothedPath = GR_SmoothPLPath2(PathPoints, Clearance);
