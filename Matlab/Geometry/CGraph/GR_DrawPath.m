function GR_DrawPath(Graph, Path, Color)
% Crta graf zadan kao struktura
%
% Path - niz ID-ova cvorova (pretpostavlja se da oni idu od 1)

bHold = ishold();
hold on;

if(nargin<3)
    Color = 'r';
end

plot(Graph.Nodes(Path,2), Graph.Nodes(Path,3), Color);

plot(Graph.Nodes([Path(1) Path(end)],2), Graph.Nodes([Path(1) Path(end)],3), Color, 'LineStyle', 'None', 'Marker', 'o');

if(bHold)
    hold on;
else
    hold off;
end