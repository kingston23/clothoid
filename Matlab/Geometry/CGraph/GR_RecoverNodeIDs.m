function Graph = GR_RecoverNodeIDs(Graph)
% Ensures that node IDs are 1,2,3,...
% E.g. if prevous IDs were 1,3,4,6 this function will set them to 1,2,3,4.
% Precondition: IDs should be sorted beforehand.

nNodes = length(Graph.Nodes);

for(i=1:nNodes)
    OldNodeID = Graph.Nodes(i,1);
    if(OldNodeID ~= i)
        Graph.Nodes(i,1) = i;
        
        % Treba apdejtati i rubove
        for(j=1:length(Graph.Edges))
            if(Graph.Edges(j,2) == OldNodeID)
                Graph.Edges(j,2) = i;
            end
            if(Graph.Edges(j,3) == OldNodeID)
                Graph.Edges(j,3) = i;
            end
        end
    end
end
