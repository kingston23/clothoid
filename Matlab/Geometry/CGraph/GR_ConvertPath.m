function [PathPoints] = GR_ConvertPath(Graph, Path)
% Konvertira putanju iz formata koji je zadan indeksima cvorova u roadmapi -> u niz tocaka [x1 y1; x2 y2; ...]

nPoints = length(Path);

nSmoothedPathSegments = 0;

PathPoints = [];

for(iPoint=1:nPoints)
    
    % Start tocka prvog segmenta
    x = Graph.Nodes(Path(iPoint), 2);
    y = Graph.Nodes(Path(iPoint), 3);
    
    PathPoints(iPoint, 1:2) = [x y];
end
