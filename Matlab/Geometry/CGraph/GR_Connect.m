function GR2 = GR_Connect(GR, ConnectPoints)
% Spaja dva ili vise grafova u jedan ako imaju jednu ili vise zajednickih tocaka (nesto kao zbrajanje grafova)
%
% ConnectPoints - moguce tocke spajanja.
%    Algoritam provjerava svaku od ovih tocaka i ako je zajednicka, koristi ju kao spojnu tocku.
%    Ne smije biti drugih zajednickih tocaka osim te jedne jer se ne radi provjera za to.

GR2 = GR(1);

nNodes = NumRows(GR2.Nodes);
nEdges = NumRows(GR2.Edges);

for(i=2:length(GR))
    
    % Connect all graphs into one, don't care for common points now
    
    if(~isempty(GR(i).Nodes))
        GR(i).Nodes(:,1) = GR(i).Nodes(:,1) + nNodes;
        GR2.Nodes = [GR2.Nodes; GR(i).Nodes];
    end
    
    if(~isempty(GR(i).Edges))
        GR(i).Edges(:,1) = GR(i).Edges(:,1) + nEdges;
        GR(i).Edges(:,2) = GR(i).Edges(:,2) + nNodes;
        GR(i).Edges(:,3) = GR(i).Edges(:,3) + nNodes;
        GR2.Edges = [GR2.Edges; GR(i).Edges];
    end
    
    nNodes = NumRows(GR2.Nodes);
    nEdges = NumRows(GR2.Edges);
    
end

% Now delete redundant points from graph and update adjacent edges.
% We assume that only certain points can be redundant, otherwise we would have to check each point with each other to
% find identical points.

IDsToRemove = [];

for(iPoint=1:NumRows(ConnectPoints))
    
    % Find IDs of all copies of this point
    
    IDs = [];
    
    for(iNode=1:nNodes)
        if(GE_IsPointEqual(ConnectPoints(iPoint,:), GR2.Nodes(iNode,2:3)))
            IDs = [IDs iNode];
        end
    end
    
    if(length(IDs) <= 1)
        continue; % Not found this point at all or found only one copy - no redundant nodes to remove
    end
    
    % Store all IDs other than the first ID. Those nodes will be removed later.
    % For now leave them, but reconnect adjacent edges to the node with the first ID.
    
    IDsToRemove = [IDsToRemove IDs(2:end)];
    
    for(iRemove=2:length(IDs))
        
        IDToRecconect = IDs(iRemove);
        
        for(iEdge=1:nEdges)
            if(GR2.Edges(iEdge,2) == IDToRecconect)
                GR2.Edges(iEdge,2) = IDs(1);
            end
            
            if(GR2.Edges(iEdge,3) == IDToRecconect)
                GR2.Edges(iEdge,3) = IDs(1);
            end
        end
    end 
end

% By now the graph has exact geometric structure, but still has redundant nodes that are now unconnected

GR2.Nodes(IDsToRemove, :) = []; % Remove all redundant

% It now remains to recover nodes IDs so that they are 1,2,3,... and update edges accordingly
GR2 = GR_RecoverNodeIDs(GR2);
