function NewVG = VG_ConnectPoint(VG, Point, VisibilityPolygon)
% Dodaje novu tocku u visibility graph ako je poznat VisibilityPolygon te tocke
%
% VisibilityPolygon - VisibilityPolygon tocke Point

% Idi po svim tockama u visibility grafu i ako je neka u visibility poligonu
% dodaj ju u graf

nNodes = NumRows(VG.Nodes);
for(i=1:nNodes)
    if( Pol_IsPointIn(VisibilityPolygon, VG.Nodes(i,2:3)) )
        VG.Edges(end+1,:) = [length(VG.Edges)+1 i nNodes+1];
    end
end

% Dodaj novi cvor
VG.Nodes(end+1,:) = [NumRows(VG.Nodes)+1 Point(1) Point(2)];

NewVG = VG;