function GR = GR_ConnectPointToClosest(GR, Point)
% Dodaje novu tocku u graph i spaja ju s najblizom tockom u grafu
%

nNodes = NumRows(GR.Nodes);
iClosest = -1;
MinDist2 = 1e300;

for(i=1:nNodes)
    x = GR.Nodes(i,2);
    y = GR.Nodes(i,3);
    
    Dist2 = (Point(1)-x)^2 + (Point(2)-y)^2;
    
    if(Dist2<MinDist2)
        MinDist2 = Dist2;
        iClosest = i;
    end
end

% Dodaj novi cvor
NewNodeID = NumRows(GR.Nodes)+1;
GR.Nodes(end+1,:) = [NewNodeID Point];

% Spoji ga s najblizom tockom
if(iClosest~=-1)
    GR.Edges(end+1,:) = [NumRows(GR.Edges+1) iClosest NewNodeID];
end
