function Corridor = GR_GetCorridor(Graph, Path, Clearance)
% Crta graf zadan kao struktura
%
% Path - niz ID-ova cvorova (pretpostavlja se da oni idu od 1)

PathPolygon = [Graph.Nodes(Path,2) Graph.Nodes(Path,3)];

PathPolygonRev = Pol_Reverse(PathPolygon);

[b Ln1] = LP_LineFromPoints(PathPolygonRev(1,1), PathPolygonRev(1,2), PathPolygonRev(2,1), PathPolygonRev(2,2));

[b Ln2] = LP_LineFromPoints(PathPolygonRev(end,1), PathPolygonRev(end,2), PathPolygonRev(end-1,1), PathPolygonRev(end-1,2));

Ln1 = LP_Rotate(Ln1, -pi/2);
Ln2 = LP_Rotate(Ln2, pi/2);

[PathPolygonRev(1,1), PathPolygonRev(1,2)] = LP_GetPoint(Ln1, Clearance/100);
[PathPolygonRev(end,1), PathPolygonRev(end,2)] = LP_GetPoint(Ln2, Clearance/100);

PathPolygon = [PathPolygon; PathPolygonRev];

PathPolygon = Pol_Reverse(PathPolygon);

Corridor = DilateEnvironment({PathPolygon}, Clearance);
