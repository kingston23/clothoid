function Graph = GR_BuildFromSegments(Segments)
% Izgraduje graf na temelju segmenata
% Kompleksnost n^2

Graph.Nodes = [];
Graph.Edges = [];

nSegs = NumRows(Segments);

for(i=1:nSegs)
    
    IDFirst = -1;
    IDSecond = -1;
    
    % Provjeri dal je koja od tocki vec u grafu
    
    for(j=1:NumRows(Graph.Nodes))
        
        P = Graph.Nodes(j,2:3);
        
        if(GE_IsPointEqual(P, Segments(i,1:2)))
            IDFirst = j;
        end
        
        if(GE_IsPointEqual(P, Segments(i,3:4)))
            IDSecond = j;
        end
        
        if(IDFirst ~= -1 && IDSecond ~= -1)
            break;
        end
    end
    
    % Dodaj tocke ak treba
    
    if(IDFirst == -1)
        IDFirst = NumRows(Graph.Nodes) + 1;
        Graph.Nodes(end+1,:) = [IDFirst Segments(i,1:2)];
        
    end
    
    if(IDSecond == -1)
        IDSecond = NumRows(Graph.Nodes) + 1;
        Graph.Nodes(end+1,:) = [IDSecond Segments(i,3:4)];
    end
    
    % Dodaj rub koji povezuje te dvije tocke
    Graph.Edges(end+1,:) = [NumRows(Graph.Edges)+1 IDFirst IDSecond];
end
