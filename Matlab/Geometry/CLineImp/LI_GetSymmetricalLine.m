function SymLine = GetSymmetricalLine(Line1, Line2)
% Vraca koef. implicitnog zapisa pravca koji je simetrican pravcima Line1 i Line2
% Daje simetralu izmedu dva pravca. Smjer simetrale bit ce srednja vrijednost smjerova dvaju pravaca (uzima se kao da su pravci usmjereni)
% Opcenito svaka 2 pravca imaju dvije simetrale. Koja od te dvije simetrale ce se dobiti, i smjer te simetrale, ovisi o smjerovima 1. i 2. pravca.
% To znaci da ako zelimo dobiti onu drugu simetralu, treba uzeti suprotne predznake A,B,C od jednog od pravaca.

K1 = sqrt(Line1.A*Line1.A + Line1.B*Line1.B);
K2 = sqrt(Line2.A*Line2.A + Line2.B*Line2.B);

K1K2 = K1*K2;

SymLine.A = (Line1.A*K2 + Line2.A*K1) / K1K2;
SymLine.B = (Line1.B*K2 + Line2.B*K1) / K1K2;
SymLine.C = (Line1.C*K2 + Line2.C*K1) / K1K2;