function bIsLeft = LI_IsPointLeft(this, x, y)

if(this.A*x + this.B*y + this.C > 0)
	bIsLeft = true; return;
else
	bIsLeft = false; return;
end
