function [bOK xc yc]= LIB_GetIntersectionPoint(this, OtherLine)
% Finds intersection point between two bounded lines given implicitly.
% bOK - true if intersection is found
% (xc, yc) - intersection

% Prvo nadi presjeciste obicnih pravaca bez da gledamo granicne tocke
[bOK xc yc] = LI_GetIntersectionPoint(this, OtherLine);
if ~bOK
    return
end

% Sad provjeri dal je nadena presjecna tocka unutar granicnih tocaka obaju pravaca

if ~LIB_CheckPoint(this, xc, yc)
    bOK = false;
    return;
end

if ~LIB_CheckPoint(OtherLine, xc, yc)
    bOK = false;
    return;
end
