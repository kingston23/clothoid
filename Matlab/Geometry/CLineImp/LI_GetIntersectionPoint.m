function [bOK x y] = LI_GetIntersectionPoint(this, OtherLine)
% Finds intersection point between two lines given implicitly.
% bOK - true if intersection is found
% (x, y) - intersection

Temp = this.A*OtherLine.B - OtherLine.A*this.B;
if ~Temp
    bOK = false;
    x = [];
    y = [];
    return;
end
    
x = (this.B*OtherLine.C - OtherLine.B*this.C) / Temp;
y = (OtherLine.A*this.C - this.A*OtherLine.C) / Temp;

bOK = true;