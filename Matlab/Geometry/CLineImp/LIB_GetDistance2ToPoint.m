function Distance2 = LIB_GetDistance2ToPoint(this, x, y)
% Vraca udaljenost linijskog segmenta do tocke

% Nadi projekciju tocke na pravac
AA = this.A*this.A;
BB = this.B*this.B;
AB = this.A*this.B;
AABB = AA+BB;

px = (BB*x-AB*y-this.A*this.C)/AABB;
py = (-AB*x+AA*y-this.B*this.C)/AABB;

% Ako se tocka projekcije nalazi na duzini, onda je ta tocka najbliza
if(LIB_CheckPoint(this,px,py))
	dx = px-x;
	dy = py-y;

	Distance2 = (dx*dx+dy*dy);
	return;
end

% Inace je najbliza tocka ili pocetna ili krajnja tocka duzine
dx = this.p1.x-x;
dy = this.p1.y-y;
d1 = dx*dx+dy*dy;

dx = this.p2.x-x;
dy = this.p2.y-y;
d2 = dx*dx+dy*dy;

if(d1<d2)
	Distance2 = d1;
	return;
else
	Distance2 = d2;
	return;
end
