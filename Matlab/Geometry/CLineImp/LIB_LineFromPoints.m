function this = LIB_LineFromPoints(x1,y1,x2,y2)
% Vraca linijski segment (implicitno zadan)

this.A = y1 - y2;
this.B = x2 - x1;
this.C = x1*y2 - x2*y1;

this.p1.x = x1;
this.p1.y = y1;
this.p2.x = x2;
this.p2.y = y2;
