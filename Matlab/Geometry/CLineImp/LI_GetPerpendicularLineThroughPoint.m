function this = LI_GetPerpendicularLineThroughPoint(Line, x, y)
% Pronalazi okomiti pravac koji prolazi kroz zadanu tocku (x,y)
% Smjer pravca bude +90 u odnosu na polazni pravac

this.A = -Line.B;
this.B = Line.A;
this.C = Line.B*x-Line.A*y;
