function ItIs = LIB_CheckPoint(this, xc, yc)
% Testira dal je tocka za koju znamo da je na pravcu, izmedu granicnih tocaka omedenog pravca.
% this - implicitno zadan omedeni pravac
% (xc, yc) - tocka koja se testira
% ItIs - true ako je (xc, yc) priblizno izmedu granicnih tocaka omedenog
% pravca.

global CGeometry;

% Zbog numericke nepreciznosti, ak je linija vise horizontalna, testiramo samo x koordinatu i obrnuto
if(abs(this.B)>abs(this.A))
    % Horizontalan je
    if((this.p1.x-xc<=CGeometry.eps && xc-this.p2.x<=CGeometry.eps) || (this.p2.x-xc<=CGeometry.eps && xc-this.p1.x<=CGeometry.eps))
        ItIs = true;
        return;
    end
else
    if((this.p1.y-yc<=CGeometry.eps && yc-this.p2.y<=CGeometry.eps) || (this.p2.y-yc<=CGeometry.eps && yc-this.p1.y<=CGeometry.eps))
        ItIs = true;
        return;
    end
end
ItIs = false;
