function this = CA_SetArcFromTangent(xStart, yStart, angStart, Radius, DeltaAngle)
% Postavljanje luka na temelju pocetne tocke iz koje pocinje luk
% Luk krece iz tocke (xStart, yStart), tangenta na njega u toj tocki ima smjer angStart i promjena kuta je DeltaAngle (koja moze biti i neg.)
% Radijus luka moze biti pozitivan (tada je luk lijevo od tangente), ili neg (a onda je luk desno od tangente)
% 
% Kombinacijama razlicitih predznaka radijusa i promjene kuta dobijemo cetiri kombinacije:
% 1) r>0, dAng>0 -> luk je lijevo od tangente i ide u smjeru tangente
% 2) r<0, dAng<0 -> luk je desno od tangente i ide u smjeru tangente
% 3) r>0, dAng<0 -> luk je lijevo od tangente i ide u smjeru suprotnom od tangente
% 4) r<0, dAng>0 -> luk je desno od tangente i ide u smjeru suprotnom od tangente

% Racunamo srediste kruznice
this = CI_SetCircleFromTangent(xStart, yStart, angStart, Radius);

% Racunamo pocetni kut i promjenu kuta
if(Radius>=0)
	this.m_BeginAngle = angStart-pi/2;
else
	this.m_BeginAngle = angStart+pi/2;
end

this.m_DeltaAngle = DeltaAngle;
