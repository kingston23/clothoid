function CA_DrawArc(this, color)

th = linspace(this.m_BeginAngle, this.m_BeginAngle+this.m_DeltaAngle, 100);
x = this.m_xc + this.m_r*cos(th);
y = this.m_yc + this.m_r*sin(th);

plot(x,y,color);
