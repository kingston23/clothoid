function [x y Angle] = CA_GetPoint(this, s)

% put = radijus * kut

ang = this.m_BeginAngle + s / this.m_r;
x = this.m_xc + this.m_r * cos(ang);
y = this.m_yc + this.m_r * sin(ang);

if(this.m_r>=0)
	Angle = ang + pi/2;
else
	Angle = ang - pi/2;
end