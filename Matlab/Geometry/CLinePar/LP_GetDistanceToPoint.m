function d = LP_GetDistanceToPoint(this, x, y)
% Daje udaljenost do tocke
% Ako je tocka "desno" od pravca, tada je udaljenost negativna

d = this.S*(this.p0.x-x) + this.C*(y-this.p0.y);
return;
