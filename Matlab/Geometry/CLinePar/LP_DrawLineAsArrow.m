function LP_DrawLineAsArrow(this, parms, color, options)
% Funkcija za crtanje parametarski zadanog pravca - ali kao strelice

if(strfind(options, 'interval'))
    interval = parms;
    if(abs(this.C)>abs(this.S))
        % horizontala - koristimo x interval
        x1 = interval(1);
        x2 = interval(2);
        [y1 d1] = LP_GetY(this, x1);
        [y2 d2] = LP_GetY(this, x2);
    else
        % koristimo y interval
        y1 = interval(3);
        y2 = interval(4);
        [x1 d1] = LP_GetX(this, y1);
        [x2 d2] = LP_GetX(this, y2);
    end
    % Tocke crtamo tak da prva ima manji d
    if(d2<d1)
        swap = x1;
        x1 = x2;
        x2 = swap;
        swap = y1;
        y1 = y2;
        y2 = swap;
    end
elseif(strfind(options, 'length'))
    length = parms;
    x1 = this.p0;
    x2 = this.p0.x+this.C*length;
    y1 = this.p0.y;
    y2 = this.p0.y+this.S*length;
end

ar = arrow([x1 y1], [x2 y2]);
set(ar, 'EdgeColor', color);
set(ar, 'facecolor', color);
