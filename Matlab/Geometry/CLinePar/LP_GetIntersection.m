function [bOk x y] = LP_GetIntersection(this, Line2)
% Daje tocku sjecista dvaju parametarskih pravaca, ili false ako su pravci paralelni
x=0; y=0;

if(~LP_IsParallel(this, Line2))
	d1 = (Line2.C*(Line2.p0.y - this.p0.y) + Line2.S*(this.p0.x - Line2.p0.x)) / (this.S*Line2.C - this.C*Line2.S);

	[x y] = LP_GetPoint(this, d1);

	bOk = true; return;
else
    bOk = false; return;
end