function [x y] = LP_GetPoint(this, d)

x = this.p0.x + this.C*d;
y = this.p0.y + this.S*d;