function [bRes this] = LP_LineFromPoints(x1, y1, x2, y2)
% Zadavanje pravca iz dvije tocke.
% Prva tocka se uzima kao pocetna. Smjer se uzima od prve do druge tocke.
% Tocke moraju biti razlicite (eps), inace se vraca false.

if(x1==x2 && y1==y2)
	bRes = false;
	return;
end
	

Direction = atan2(y2-y1, x2-x1);

this = LP_LineFromDirectionAndPoint(Direction, x1, y1);

bRes = true;
return;
