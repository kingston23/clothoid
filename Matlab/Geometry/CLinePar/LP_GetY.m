function [y d] = LP_GetY(this, x)
% Ako je poznat x, daje y (pravac ne smije biti vertikalan - nema provjere za to)

if(this.C==0); error('vertikalan je'); end
d = (x-this.p0.x)/this.C;
y = this.p0.y + this.S*d;
