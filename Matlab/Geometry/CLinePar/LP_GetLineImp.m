function ImpLine = LP_GetLineImp(this)

ImpLine.A = -this.S;
ImpLine.B = this.C;
ImpLine.C = this.S*this.p0.x - this.C*this.p0.y;