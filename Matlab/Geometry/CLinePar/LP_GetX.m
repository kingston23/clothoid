function [x d] = LP_GetX(this, y)
% Ako je poznat y, daje x (pravac ne smije biti horizontalan - nema provjere za to)

if(this.S==0); error('horizontalan je'); end
d = (y-this.p0.y)/this.S;
x = this.p0.x + this.C*d;