function [this] = LP_Rotate(this, dth)
% Rotiranje pravca oko pocetne tocke za dth [rad] (trenutnom smjeru se dodaje dth)

this.m_Direction = this.m_Direction + dth;

this.C = cos(this.m_Direction);
this.S = sin(this.m_Direction);
