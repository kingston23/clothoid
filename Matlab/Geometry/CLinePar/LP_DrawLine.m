function LP_DrawLine(this, parms, color, options)
% Funkcija za crtanje parametarski zadanog pravca

if(strfind(options, 'interval'))
    interval = parms;
    if(abs(this.C)>abs(this.S))
        % horizontala - koristimo x interval
        x1 = interval(1);
        x2 = interval(2);
        y1 = LP_GetY(this, x1);
        y2 = LP_GetY(this, x2);
    else
        % koristimo y interval
        y1 = interval(3);
        y2 = interval(4);
        x1 = LP_GetX(this, y1);
        x2 = LP_GetX(this, y2);
    end
elseif(strfind(options, 'length'))
    length = parms;
    x1 = this.p0;
    x2 = this.p0.x+this.C*length;
    y1 = this.p0.y;
    y2 = this.p0.y+this.S*length;
end

plot([x1 x2], [y1 y2], color);
