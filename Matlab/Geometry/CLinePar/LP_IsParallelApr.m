function bIsPar = LP_IsParallelApr(this, Line2)
% Ispituje pribliznu (eps) paralelnost

if(GE_IsEqualApr(this.S*Line2.C, this.C*Line2.S))
	bIsPar = true; return;
else
	bIsPar = false; return;
end
