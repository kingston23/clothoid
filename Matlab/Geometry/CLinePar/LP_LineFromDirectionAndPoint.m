function this = LP_LineFromDirectionAndPoint(Direction, x, y)

this.m_Direction = Direction;

this.p0.x = x;
this.p0.y = y;

this.C = cos(Direction);
this.S = sin(Direction);

this.Type = 'LP';