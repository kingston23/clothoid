function [bRes this] = LPB_LineFromPoints(x1, y1, x2, y2)
% Zadavanje omedenog parametarskog pravca iz dvije tocke (LPB = LineParBounded)
% Prva tocka se uzima kao pocetna. Smjer se uzima od prve do druge tocke, a duljina iz razmaka dviju tocaka.
% Tocke moraju biti razlicite (eps), inace se vraca false.

dx = x2-x1;
dy = y2-y1;

if(dx==0 && dy==0)
	bRes = false;
	return;
end

Direction = atan2(dy, dx);

this = LP_LineFromDirectionAndPoint(Direction, x1, y1);
this.m_Length = sqrt(dx*dx+dy*dy);

bRes = true;
return;
