function bIsPar = LP_IsParallel(this, Line2)
% Ispituje paralelnost (tocnu)

if(this.S*Line2.C == this.C*Line2.S)
	bIsPar = true; return;
else
	bIsPar = false; return;
end
