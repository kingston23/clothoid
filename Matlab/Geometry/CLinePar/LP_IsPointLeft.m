function bIsLeft = LP_IsPointLeft(this, x, y)

if(this.S*(this.p0.x-x) + this.C*(y-this.p0.y) > 0)
	bIsLeft = true; return;
else
	bIsLeft = false; return;
end
