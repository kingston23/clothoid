function [this] = LP_SetOppositeDirection(this)
% Postavlja suprotan smjer (dodaje se pi trenutnom smjeru)

this.m_Direction = this.m_Direction + pi;
this.C = -this.C;
this.S = -this.S;
