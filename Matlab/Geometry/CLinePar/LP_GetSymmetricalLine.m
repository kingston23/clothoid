function [SymmetryAxis] = LP_GetSymmetricalLine(this, Line2)
% Daje simetralu izmedu dva pravca. Smjer simetrale bit ce srednja vrijednost smjerova dvaju pravaca.
% Opcenito svaka 2 pravca imaju dvije simetrale. Koja od te dvije simetrale ce se dobiti, i smjer te simetrale, ovisi o smjerovima 1. i 2. pravca.
% To znaci da ako zelimo dobiti onu drugu simetralu, treba uzeti suprotni smjer jednog od pravaca.
% Ak su pravci paralelni, vraca se srednja linija izmedu njih ako su istog smjera, odnosno okomica na oba pravca ako su suprotnog smjera.
% U slucaju da su paralelni i suprotnog smjera, nije sigurno kud bude ispal smjer simetrale (zbog moguce num. pogreske)
% Dakle funkcija uvijek vraca nekakvo rjesenje (bilo ono smisleno ili ne, zato provjeravajmo)


% Odredi novi kut kao sredinu kuteva dvaju pravaca.
% Potrebna je dosta slozena logika jer rezultati inace ne bi bili dobri ako je jedan kut u 2. kvadrantu, a drugi u 3. kvadrantu
NewDirection = NormAngle_MinusPI_PI(this.m_Direction + 0.5*NormAngle_MinusPI_PI(Line2.m_Direction-this.m_Direction));
% MOguci drugi nacin je ispod. Izgleda da daje isto.
%Angle1 = this.m_Direction; Angle2 = Line2.m_Direction;
%Angle2 = AR_Range_MinusPI_Ang_PI(Angle2, Angle1); % Svedi na interval [Angle1-pi, Angle1+pi>
%NewDirection = NormAngle_MinusPI_PI(0.5*(Angle1+Angle2));

[bOk xInter yInter] = LP_GetIntersection(this, Line2);
if(~bOk)
	% Paralelni su pa koristimo ovu logiku ispod. To rjesenje u nekim slucajevima nema previse smisla,
    % zato oprezno s koristenjem donjeg rezultata!
	xInter = 0.5*(this.p0.x + Line2.p0.x);
	yInter = 0.5*(this.p0.y + Line2.p0.y);
end

SymmetryAxis = LP_LineFromDirectionAndPoint(NewDirection, xInter, yInter);
