function LPB_DrawLine(this, color)
% Funkcija za crtanje parametarski zadanog pravca + omedenog

x1 = this.p0.x;
x2 = this.p0.x + this.C*this.m_Length;
y1 = this.p0.y;
y2 = this.p0.y + this.S*this.m_Length;

plot([x1 x2], [y1 y2], color);
