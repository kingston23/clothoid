function bRes = GE_IsLessOrEqualApr(x, y)
% Priblizno ispituje uvjet x<=y (eps)
global CGeometry;

if(x<y || abs(x-y)<CGeometry.eps)
	bRes = true; return;
else
	bRes = false; return;
end
