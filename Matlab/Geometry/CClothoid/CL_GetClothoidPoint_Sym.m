function [x_ y_] = CL_GetClothoidPoint_Sym(CL, s_)
% Odredivanje koordinata klotoide simbolickim proracunom
% CL - struktura koja opisuje klotoidu s parametrima x0, y0, th0, k0, c
%
% U granicnim slucajevima klotoida se aproksimira pravcem, tockom ili kruznicom
% TUDU: dali bi se izbjeci neki sqrt-ovi koristenjem velikog C (skaling faktora)

% Znacenja varijabli
% x0, y0, fi0 - pocetna tocka i orijentacija klotoide
% c - brzina skretanja klotoide
% k0, k - pocetna i trenutna zakrivljenost
% s - prijedeni put - koristi se kao parametar
syms s

fi = CL.th0 + CL.k0*s + CL.c/2*s^2;
x_ = double(CL.x0+int(cos(fi), s, 0, s_));
y_ = double(CL.y0+int(sin(fi), s, 0, s_));

%x_ = subs(x, {x0 y0 fi0 k0 s}, {x0_ y0_ fi0_ k0_ s_});
%y_ = subs(y, {x0 y0 fi0 k0 s}, {x0_ y0_ fi0_ k0_ s_});
