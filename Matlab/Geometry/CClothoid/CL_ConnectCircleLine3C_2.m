function [bSuccess ConnectPath] = CL_ConnectCircleLine3C_2(CurrentState, SmoothedPath, bDraw)
% Izgladivanje uz non-zero pocetnu zakrivljenost
% Problem se svodi na spajanje kruznice i pravca koristenjem triju klotoida
%
% CurrentState - trenutno stanje zadano kao struktura sa [x y th k]
% SmoothedPath - izgladena putanja, mora pocinjati linijom na koju se mozemo "prizemljiti"
% bDraw - zastavica koja kaze dal treba crtati
% ConnectPath - izlazna putanja koja spaja CurrentState i SmoothedPath

if(nargin<3)
    bDraw = false;
end

if(isempty(SmoothedPath))
    bSuccess = false;
    return;
end

BeginPathSegment = SmoothedPath{1};

if(~strcmp(BeginPathSegment.Type, 'LPB'))
    bSuccess = false;
    return;
end

[G1.x G1.y] = LP_GetPoint(BeginPathSegment, 0);
[G2.x G2.y] = LP_GetPoint(BeginPathSegment, BeginPathSegment.m_Length);

[bSuccess C ConnectPath] = CL_ConnectCircleLine3C(CurrentState, G1, G2, bDraw);

return;