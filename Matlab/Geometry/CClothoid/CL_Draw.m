function [xMin xMax yMin yMax x_ y_ th_] = CL_Draw(CL, Color)
% Crtanje klotoide (racuna se simbolicki ili preko lookup tablice)
%
% Parametri klotoide su u strukturi CL

if(nargin<2)
    Color = 'b';
end

bSymbolic = false; % Odabir simbolickog ili LT crtanja

nPoints = 100;
s_plot = linspace(0, CL.length, nPoints);

if(bSymbolic)
    syms th x y s

    th = CL.th0 + CL.k0*s + 0.5*CL.c*s^2;
    x = CL.x0+int(cos(th), s, 0, s);
    y = CL.y0+int(sin(th), s, 0, s);

    x_ = double(subs(x, s, s_plot));
    y_ = double(subs(y, s, s_plot));
    %th_ = double(subs(th, s, s_plot));

else
    x_ = zeros(nPoints,1);
    y_ = zeros(nPoints,1);
    
    for(i=1:nPoints)
        [x_(i) y_(i)] = GetClothoidPoint(CL, s_plot(i));
    end
    
end

xMin = min(x_);
xMax = max(x_);
yMin = min(y_);
yMax = max(y_);

plot(x_, y_, Color);
