function [bOk C Arc CL] = CL_ConnectCircleLine1C(P0, G1, G2, bDraw)
% Izgladivanje uz non-zero pocetnu zakrivljenost
% Problem se svodi na spajanje kruznice i pravca koristenjem jedne klotoide.
%
% P0 - pocetno stanje koje se sastoji od P0.x, P0.y, P0.th, P0.k
% G1, G2 - linija na koju moramo "sletjeti" koristenjem klotoida
% C - scaling factor - rjesenje jednadzbe
% Arc - ulazni kruzni luk
% CL - izlazna klotoida

% TUDU: ogranicenje duljine klotoide
% TUDU: ispitivanje da li se izbjegava puni krug

bOk = true;

C = 0;
Arc = [];
CL = [];

if(nargin<4)
    bDraw = false;
end

%% Ispitivanje argumenata
if(P0.k == 0)
    bOk = false; warning('Ova funkcija zahtijeva poc. zakrivljenost != 0'); return;
end

%% Ispitivanje nuznih uvjeta
% Ovo nisu (barem ne svi) uvjeti postojanja rjesenja vec ovime reduciramo skup mogucih rjesenja.
% U nekim slucajevima rjesenje moze postojati cak i ako ovi uvjeti nisu ispunjeni, ali takva rjesenja ne razmatramo.
% Jedan od razloga je i numericka nestabilnost koja bi se mogla pojaviti u takvim slucajevima.

% Tangenta na putanju kroz tocku P0
P0Tangent = LP_LineFromDirectionAndPoint(P0.th, P0.x, P0.y);

% 1. nuzan uvjet: Ako se pocetno gibamo ulijevo, onda i cilj mora biti s lijeve strane, i obrnuto.
% Uzimamo da G1 ili G2 moraju biti lijevo
if(LP_GetDistanceToPoint(P0Tangent, G1.x, G1.y) * P0.k <= 0 && LP_GetDistanceToPoint(P0Tangent, G2.x, G2.y) * P0.k <= 0)
    bOk = false; warning('Cilj je sa krive strane'); return;
end

% 2. nuzan uvjet: Ako skrecemo ulijevo, onda pocetna tocka mora biti lijevo od vektora G1G2, i obrnuto

% Pravac G1-G2
[tmp G1G2] = LP_LineFromPoints(G1.x, G1.y, G2.x, G2.y);

if(LP_GetDistanceToPoint(G1G2, P0.x, P0.y) * P0.k <= 0)
    bOk = false; warning('Nedozvoljen smjer'); return;
end

% 3. nuzan uvjet: Ako skrecemo ulijevo, pravac na koji slijecemo mora biti desno od kruznice po kojoj se trenutno gibamo

% Pronadi kruznicu po kojoj se trenutno gibamo
Circle = CI_SetCircleFromTangent(P0.x, P0.y, P0.th, 1/P0.k);
d_G1G2_CircleCenter = LP_GetDistanceToPoint(G1G2, Circle.m_xc, Circle.m_yc);
if(GE_IsLessOrEqualApr(d_G1G2_CircleCenter, Circle.m_r))
    bOk = false; warning('Ako skrecemo ulijevo, kruznica po kojoj se gibamo mora biti lijevo od pravca na koji ciljamo');
    return;
end

%% Bracketiranje rjesenja

% Ajmo probati naci donju granicu faktora skaliranja C1

% Zarotirajmo osnovnu klotoidu tako da ima pocetni kut u smjeru vektora G2G1, i da pocinje na pravcu G2G1 (recimo u G1)
CL.x0=G1.x; CL.y0=G1.y;
CL.th0=G1G2.m_Direction+pi;
CL.k0=0;
CL.C=-1; CL.c=-1;

% Potrazimo tocku P1 na toj klotoidi koja ima zakrivljenost kao u P0

s_P1 = abs(P0.k);
[P1.x P1.y] = GetClothoidPoint(CL, s_P1);
% Saznajmo udaljenost pravca G1G2 od pronadene tocke
d_P1_G1G2 = LP_GetDistanceToPoint(G1G2, P1.x, P1.y);

if(GE_IsEqualApr(d_P1_G1G2, 0))
    bOk = false; warning('Trazeni zakret je zanemariv - i moze se aproksimirati pravcem'); return;
end

% Ovo je scaling faktor koji namjesta da se tocka s zeljenom zakrivljenoscu poklopi sa najblizom tockom na kruznici
% Lijevi bracket koji se ovako dobije je u pravilu premali, i trebalo bi vrijediti fMin(C1)<0
C1 = abs((d_G1G2_CircleCenter-Circle.m_r) / d_P1_G1G2);
C1 = min(C1, 1); % Mislim da se za >1 ne bi dobili dobri rezultati. TUDU: provjeriti

% Gornja granica
dth = G1G2.m_Direction - P0.th;
if(P0.k>0)
    % Pozitivni zakret - to znaci da i zahtjevani zakret mora biti pozitivan
    dth = NormAngle_Zero_2PI(dth);
    dth_P1P0_max = min(2*pi-dth, pi);
    C2 = sqrt(2*dth_P1P0_max)/P0.k;
else
    dth = NormAngle_Minus2PI_Zero(dth);
    dth_P1P0_max = max(-2*pi-dth, -pi);
    C2 = sqrt(-2*dth_P1P0_max)/(-P0.k);
end

%CrtajFunkciju(P0, G1G2, -C1, -C2);
%return;

if(P0.k>0)
    C1 = -C1; % Mora biti <0, jer zakrivljenost ide od pozitive prema nuli
    C2 = -C2;
end

if(abs(C1)>abs(C2))
    CrtajFunkciju(P0, G1G2, 0.1*min(C1,C2), 10*max(C1,C2));
    bOk = false; warning('Ne mozemo bracketirati rjesenje'); return;
end

bFoundSolution = false;

[f1 Info] = fMin1(C1, P0, G1G2, true);
%if(Info.bInadmissibleSolution)
%    % Kad bi isli povecavat C, rjesenje bi i dalje ostalo nedopustivo - znaci nema rjesenja
%    warning('Rjesenje je odmah na pocetku nedopustivo - dakle nema rjesenja');
%    bOk = false; return;
%end

f2 = fMin1(C2, P0, G1G2, false);

if(f1 == 0)
    C = C1;
    bFoundSolution = true;
elseif(f2 == 0)
    C = C2;
    bFoundSolution = true;
elseif(f1*f2 > 0)
    warning('velika je vjerojatnost da rjesenje ne postoji (nije bracketed)');
    bOk = false;
else
    if(1)
        % Rijesi metodom bisekcije.
        f = f1;

        if(f<0)
           dC = C2-C1;
           C = C1;
        else
           dC = C1-C2;
           C = C2;
        end

        for(i=1:30)

            dC = 0.5*dC;
            Cmid = C + dC;
            fmid = fMin1(Cmid, P0, G1G2);

            if(fmid<0)
                C = Cmid;
            end
            if(abs(dC)<0.0001 || abs(fmid)<eps)
                fprintf(1, 'Found solution in %d iteration(s)\n', i);
                bFoundSolution = true;
                break;
            end
        end
    elseif(0)
        %% Rijesi false position metodom
        % ova metoda je opcenito sigurna (ne izlazi iz bracketa). Medutim njezin je problem ako funkcija poprima
        % ekstremne vrijednosti (tezi u inf). Drugi problem je kad funkcija ima ravan dio, onda se ova metoda 
        % jako dugo muci, i obicno u tom slucaju potrosi vise iteracije nego bisekcija.
        % U slucaju da je funkcija priblizno linearna, ova metoda je znacajno brza od bisekcije.
        % Medutim, buduci da joj broj iteracija moze znacajno varirati od slucaja do slucaja, ne upotrebljavamo ju.
        fl = f1;
        fh = f2;

        if(fl<0)
           Cl = C1;
           Ch = C2;
        else
           Cl = C2;
           Ch = C1;
           swap = fl;
           fl = fh;
           fh = swap;
        end
        dC = Ch - Cl;

        bFoundSolution = false;
        for(i=1:30)

            C = Cl + dC*fl/(fl-fh);
            f = fMin1(C, P0, G1G2);
            if(f<0)
                del = Cl-C;
                Cl = C;
                fl = f;
            else
                del = Ch-C;
                Ch = C;
                fh = f;
            end

            dC = Ch-Cl;

            if(abs(del)<0.0001 || abs(f)<eps)
                fprintf(1, 'Found solution in %d iteration(s)\n', i);
                bFoundSolution = true;
                break;
            end
        end
    end

    if(~bFoundSolution)
        warning('premasen max. broj iteracija');
        bOk = false;
    end
    
    if(bFoundSolution && ~Info.bAdmissibleSolution)
        warning('pronadeno rjesenje nije dopustivo');
    end
    
end

%% Izracunaj pocetnu i zavrsnu klotoidu
if(bFoundSolution)
    % Moramo ponoviti proracun, jer zadnji poziv funkcije fMin mozda nije onaj koji odgovara rjesenju (tj. odbacen je)
    [f Info] = fMin1(C, P0, G1G2);

    AngleOnCircle = Info.ClothTh0 - sign(P0.k)*pi/2;
    [P1.x P1.y] = CI_GetPointInAngle(Info.Circle, AngleOnCircle);
    CL1.x0 = P1.x;
    CL1.y0 = P1.y;
    CL1.th0 = Info.ClothTh0;
    CL1.k0 = P0.k;
    CL1.C = C;
    CL1.c = sign(C)/(C*C);
    CL1.length = Info.ClothLength;
    
    Arc = Info.Arc;
    CL = CL1;
end

%%
if(bDraw && bFoundSolution)
    figure(1), clf, hold on, grid on
    axis equal
    
    %plot([G1.x G2.x], [G1.y G2.y], 'r');
    CI_DrawCircle(Info.Circle, 'b-.');
    plot(Info.Circle.m_xc, Info.Circle.m_yc, 'r.');
    CA_DrawArc(Info.Arc, 'r');
    %LP_DrawLineAsArrow(G1G2, [G1.x G2.x G1.y G2.y], 'r', 'interval');
    arrow([G1.x G1.y], [G2.x G2.y]);
    plot(P0.x, P0.y, 'r.');
    plot(P1.x, P1.y, 'r.');
    [xmin xmax ymin ymax] = CL_Draw(CL1, 'r');
    %LP_DrawLine(G1G2, [xmin xmax ymin ymax], 'r', 'interval');
    pause;
end

CrtajFunkciju(P0, G1G2, C1, C2);

return;




%% Crtanje funkcije koja se minimizira
function CrtajFunkciju(P0, G1G2, C1, C2)

bDrawPolako = false;
%fMin1(C2, P0, G1G2, true); pause;

if(bDrawPolako)
    C_plot = linspace(C1+(C2-C1)*0.0, C2, 20);
else
    C_plot = linspace(C1, C2*0.99, 500);
end
f_plot = zeros(size(C_plot));

for(i=1:length(C_plot))
    if(bDrawPolako)
        f_plot(i) = fMin1(C_plot(i), P0, G1G2, true);
        fprintf('C=%f, f=%f\n', C_plot(i), f_plot(i));
        pause;
    else
        f_plot(i) = fMin1(C_plot(i), P0, G1G2, false);
    end
end
figure(2), clf, hold on, grid on;
plot(C_plot, f_plot);






%% Funkcija koju minimiziramo
function [out Info] = fMin1(C, P0, G1G2, bDraw)

if(nargin<4)
    bDraw=false;
end
%bDraw=true;
Info.bAdmissibleSolution = true; % Dal je trenutno rjesenje dopustivo

% Pronadi srediste kruznice po kojoj se trenutno gibamo
Circle = CI_SetCircleFromTangent(P0.x, P0.y, P0.th, 1/P0.k);

% Pronadi udaljenost sredista kruznice od pravca G1G2
d_G1G2_Circle = LP_GetDistanceToPoint(G1G2, Circle.m_xc, Circle.m_yc);

% Provuci klotoidu kroz tocku G1 i nadi tocku P1 na njoj koja ima zakrivljenost jednaku zakrivljenosti gibanja robota
CL.x0=G1G2.p0.x;
CL.y0=G1G2.p0.y;
CL.th0=G1G2.m_Direction+pi;
CL.k0=0;
CL.C=C;
CL.c=sign(C)/(C*C);

P1.k = P0.k;
s_kP1 = -P1.k / CL.c;
[P1.x P1.y] = GetClothoidPoint(CL, s_kP1);
P1.th = G1G2.m_Direction + CL.c/2*s_kP1*s_kP1;

% Nadi kruznicu u tocki P1 klotoide
CircleP1 = CI_SetCircleFromTangent(P1.x, P1.y, P1.th, 1/P1.k);

% Pronadi udaljenost kruznice od pravca G1G2
d_G1G2_CircleP1 = LP_GetDistanceToPoint(G1G2, CircleP1.m_xc, CircleP1.m_yc);

out = d_G1G2_Circle - d_G1G2_CircleP1;

if(GE_IsEqualApr(out, 0)) % Zaokruzivanje na nulu
    out = 0;
end

s_PI = abs(C)*sqrt(2*pi); % Kut poluzakreta moze biti <= pi
if(s_kP1>s_PI)
    Info.bAdmissibleSolution = false; %
end

%% Izlazne informacije
if(nargout>=2)
    % Korisne informacije za racunanje putanje
    Info.ClothLength = s_kP1;

    % Za crtanje
    Info.ClothTh0 = CL.th0 + CL.c/2*s_kP1*s_kP1+pi; % Pocetni kut klotoide
    Info.Circle = Circle;
    Info.CircleP1 = CircleP1;
    
    % Kruzni luk
    if(P0.k>0)
        dAng_CircArc = NormAngle_Zero_2PI(Info.ClothTh0 - P0.th);
    else
        dAng_CircArc = NormAngle_Minus2PI_Zero(Info.ClothTh0 - P0.th);
    end
    Info.Arc = CA_SetArcFromTangent(P0.x, P0.y, P0.th, 1/P0.k, dAng_CircArc);
end

%% Crtanje
if(bDraw)
    figure(3), clf, hold on, grid on
    axis equal
    
    CL.length = s_kP1;
    plot(P0.x, P0.y, 'r.');
    plot(P1.x, P1.y, 'g.');
    CI_DrawCircle(Circle, 'r');
    CI_DrawCircle(CircleP1, 'g');
    [xmin xmax ymin ymax] = CL_Draw(CL);
    LP_DrawLineAsArrow(G1G2, [xmin xmax ymin ymax], 'r', 'interval');
end







