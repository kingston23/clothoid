function [x y] = GetClothoidPoint(CL, s)
% Odredivanje koordinata klotoide pomocu lookup tablice
% CL - struktura koja opisuje klotoidu s parametrima x0, y0, th0, k0, c
%
% U granicnim slucajevima klotoida se aproksimira pravcem, tockom ili kruznicom
% TUDU: dali bi se izbjeci neki sqrt-ovi koristenjem velikog C (skaling faktora)

global LT % Lookup tablica

% Parametri od generalne klotoide koju zelimo odrediti
x0_ = CL.x0;
y0_ = CL.y0;
fi0_ = CL.th0;
k0_ = CL.k0;
c_ = CL.c;

% Zadavanje prek faktora skaliranja - nedostatak je da c nemre biti <0
%C_ = C_max; c_ = 1/C_^2;

% Zadavanje prek sharpness-a
C_ = 1/sqrt(abs(c_));

K_ = k0_*C_;

s_L1 = sqrt(abs(c_)/LT.c_L) * (s + k0_/c_); % inputi za lookup tablicu
s_L2 = sqrt(abs(c_)/LT.c_L) * k0_/c_;

if((s_L1>=LT.sL || s_L2>=LT.sL) ) % && abs(C_)<LT.C_min )
    % Aproksimacija tockom
    x = x0_;
    y = y0_;
    warning('Aproksimacija tockom');
elseif(k0_==0 && abs(C_)>LT.C_max && GE_IsEqualApr(1/C_/C_, 0)) % tu se ne bi trebalo desiti nis lose ak idemo iznad C_max
    % Aproksimacija pravcem
    x = x0_ + cos(fi0_)*s;
    y = y0_ + sin(fi0_)*s;
    warning('Aproksimacija pravcem');
elseif(k0_~=0 && abs(K_)>LT.K_max)
    % Aproksimacija kruznicom
    x = x0_ + 1/k0_*(-sin(fi0_)+sin(fi0_+k0_*s));
    y = y0_ + 1/k0_*( cos(fi0_)-cos(fi0_+k0_*s));
    warning('Aproksimacija kruznicom');
else
    % Aproksimacija tablicom

    % Priprema rot matrice
    angRot = -k0_*k0_/2/c_ + fi0_;
    r11 = cos(angRot);
    r12 = -sin(angRot);
    r21 = sin(angRot);
    r22 = cos(angRot);

    % Racunanje outputa lookup tablice (tu je ukljucena i interpolacija)
    [x1, y1] = GetBasicClothoidCoordsLT(s_L1);
    [x2, y2] = GetBasicClothoidCoordsLT(s_L2);

    xz = (x1-x2)*sqrt(LT.c_L/abs(c_)) ;
    yz = (y1-y2)*sqrt(LT.c_L/abs(c_)) * sign(c_);

    x = x0_ + r11*xz + r12*yz;
    y = y0_ + r21*xz + r22*yz;
    
end
