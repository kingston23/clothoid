function output = C_min_Cond(C_)
% Helper function for finding C_min parameter.

global LT;

% In order to avoid negative C
if C_<0
    output=-10;
    return;
end

syms th s x y C

th = 1/(2*C*C)*s^2;
x = (int(cos(th), s, 0, s));
y = (int(sin(th), s, 0, s));

% Maximum errors occurs at this angle: 2.300673465409944 rad.
% This was determined experimentally (todo: prove it)
Delta_Th_emax = 2.300673465409944;
s_ = sqrt(2)*C_*sqrt(Delta_Th_emax);

x_ = double(subs(x, {s,C}, {s_,C_}));
y_ = double(subs(y, {s,C}, {s_,C_}));
e = sqrt(x_^2 + y_^2);

% output is equal to zero if error is equal to e_max
% This actually gives deviation from desired error in milimeters.
% If it would be in meters, fzero has hard time finding a solution.
output = (e - LT.e_max)*1000;
