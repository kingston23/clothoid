% This script computes lookup table with a basic clothoid that is required for fast computation
% of general clothoid coordinates.
% Lookup table is stored to files Clothoid_LT.mat (in Matlab format) and in
% Clothoid_LT.txt (in text format).
%
% Required toolboxes: Optimization, Symbolic
%
% Copyright:
% Misel Brezak, 
% University of Zagreb, Croatia
% 2013
%
% If you use this for your publications, please cite the paper
% "Real-time Approximation of Clothoids with Bounded Error for Path Planning Applications"
% by Misel Brezak and Ivan Petrovic

%% 1) Provide parameters of required set of clothoids
% Edit values according to your application needs!
disp('Initialization')
global LT;

LT.DeltaTh_max = pi/2; % Required maximum orientation change of a single clothoid
LT.s_max = 5; % Maximum length of a signle clothoid
LT.e_max = 1e-3; % Maximum approximation error (in worst case)
LT.e_typical = 1e-9; % Typical approximation error (error at typical value of scaling)
LT.C_typical = 0.5; % Typical scaling factor (at which we want to have typical error)

% Set m=1 if in all queries for clothoid coordinates you will never need clothoids with non-zero initial curvature,
% and m=2 otherwise.
m = 1;

draw = 1; % Set this to 1 if you want to display some figures that illustrate stages of the algorithm, and 0 otherwise.

%% Define symbolic equations that are needed in later stages

% Variables:
% x0, y0, fi0 - initial point and tangent angle of clothoid
% c - sharpness
% k0, k - initial and actual curvature
% s - arc length
syms fi fi0 c k0 k x x0 y y0 s

k = k0 + c*s;
fi = fi0 + int(k); % or: fi = fi0 + k0*s + c/2*s^2;
x = simple(x0+int(cos(fi), s, 0, s));
y = simple(y0+int(sin(fi), s, 0, s));

%% 2) Determining Cmin (minimum scaling factor for given error)
disp('Determining Cmin')
LT.C_min = fzero(@C_min_Cond, 1e-2, optimset('display', 'iter')); % 'TolFun', 1e-6,)

if(draw)
    figure(1), clf, hold on, grid on
    plot(LT.e_max*cos(linspace(0,2*pi,100)), LT.e_max*sin(linspace(0,2*pi,100)),'g')
    axis equal

    x0_ = 0;
    y0_ = 0;
    fi0_ = 0;
    k0_ = 0;

    C_ = LT.C_min;

    c_ = 1/C_^2;

    s_plot = linspace(0, sqrt(2*pi/c_), 100);

    % Uvrsti konkretne parametre, osim s
    x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});

    % Nadi tocke klotoide za zeljeni raspon s
    x_ = double(subs(x_, s, s_plot));
    y_ = double(subs(y_, s, s_plot));

    plot(x_, y_, 'b');
end

%% 3) Determining Cmax (maximum scaling factor for given error)
disp('Determining Cmax')
LT.C_max = fzero(@C_max_Cond, 100, optimset('display', 'iter')); % 'TolFun', 1e-6,)
LT.C_max_initial = LT.C_max; % kasnije ga mozda promijenimo

if(draw)
    figure(1), clf, hold on, grid on

    plot([0 LT.s_max],[0 0],'g')

    x0_ = 0;
    y0_ = 0;
    fi0_ = 0;
    k0_ = 0;

    C_ = LT.C_max;

    c_ = 1/C_^2;

    s_plot = linspace(0, LT.s_max, 100);

    % Uvrsti konkretne parametre, osim s
    x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});

    % Nadi tocke klotoide za zeljeni raspon s
    x_ = double(subs(x_, s, s_plot));
    y_ = double(subs(y_, s, s_plot));

    plot(x_, y_, 'b');
end

%% Determining Kmax (maximum similarity of clothoid to circle)
disp('Determining Kmax')
if(m==1)
    LT.K_max = 0;
else
    LT.K_max = fzero(@K_max_Cond, 100, optimset('display', 'iter')); % 'TolFun', 1e-6,)
end

if(m==2 && draw)
    figure(1), clf, hold on, grid on

    k0_me = LT.DeltaTh_max / LT.s_max;
    plot(1/k0_me*sin(linspace(0,k0_me*LT.s_max,100)), 1/k0_me*(1-cos(linspace(0,k0_me*LT.s_max,100))),'g')
    axis equal

    x0_ = 0;
    y0_ = 0;
    fi0_ = 0;
    k0_ = 0;

    C_ = LT.K_max / k0_me;

    c_ = 1/C_^2;

    s_plot = linspace(0, LT.s_max, 100);

    % Uvrsti konkretne parametre, osim s
    x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_me c_});
    y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_me c_});

    % Nadi tocke klotoide za zeljeni raspon s
    x_ = double(subs(x_, s, s_plot));
    y_ = double(subs(y_, s, s_plot));

    plot(x_, y_, 'b');
    
    clear s_plot;
end

%% Determining of lookup table parameters
disp('Determining lookup table parameters')
% Maximum required basic clothoid length
LT.sL_max = min(sqrt(2*LT.DeltaTh_max), LT.s_max/LT.C_min) + LT.K_max;

% We choose some value that is somewhat greater than LT.sL_max.
% We could also round this value.
LT.sL = LT.sL_max*1.01;

%OLD: LT.Delta_sL_max = min(2.8062*(LT.e_max/LT.C_min)^(1/3), LT.DeltaTh_L_max/LT.sL);
%OLD LT.C_max = 3400; % empirijski

% Maximum allowed distance between two neighbor points
%OLD LT.Delta_sL_max = (2*sqrt(2)*0.084)^(-1/3) *(min(LT.e_max/LT.C_max, LT.e_typical/LT.C_typical))^(1/3);
LT.Delta_sL_max = (0.084*m)^(-1/3) *(min(LT.e_max/LT.C_max, LT.e_typical/LT.C_typical))^(1/3);

% Actual distance between neighbor points (must be <= LT.Delta_sL_max)
LT.Delta_sL = LT.Delta_sL_max;

% We can also manually choose some "nice" value that is lower than LT.Delta_sL_max.
% If you set it manually, do not forget to reset it if input values change!!!!!
%LT.Delta_sL = 0.002;

% Required number of points in lookup table
N_L = ceil(LT.sL / LT.Delta_sL) + 1;

nBytes = N_L * 2 * 8; % Storage requirements for point coordinates in lookup table (in double precision)

LT.sL = (N_L-1) * LT.Delta_sL; % Correction so that sL/Delta_sL is integer

LT.C_L = 1; % Scaling of basic clothoid (we arbitrarily choose value 1 because it does not affect accuracy)


%% Computing of lookup table point coordinates
% In the table we store the basic clothoid with x0_L=y0_L=fi0_L=k0_L=0 and C_L=1
disp('Computing point coordinates (relax)...')
LT.x0_L = 0;
LT.y0_L = 0;
LT.fi0_L = 0;
LT.k0_L = 0;
LT.c_L = 1/LT.C_L^2;

% Uvrsti konkretne parametre, osim s
x_ = subs(x, {x0 y0 fi0 k0 c}, {LT.x0_L LT.y0_L LT.fi0_L LT.k0_L LT.c_L});
y_ = subs(y, {x0 y0 fi0 k0 c}, {LT.x0_L LT.y0_L LT.fi0_L LT.k0_L LT.c_L});

% Nadi tocke klotoide za zeljeni raspon s i spremi ih - to je lookup tablica

s_look = 0 : LT.Delta_sL : LT.sL;
LT.x_L = double(subs(x_, s, s_look));
LT.y_L = double(subs(y_, s, s_look));
clear s_look;

if(draw)
    figure
    plot(LT.x_L, LT.y_L)
    title('The basic clothoid')
    xlabel('x'),ylabel('y')
    axis equal
end
    

% Save to disk
disp('Saving to disk')
save Clothoid_LT LT

% Save also points in text format (easy to load in e.g. C++ program)
% x will be in the first column, y in the second.
points = [LT.x_L' LT.y_L'];
save Clothoid_LT.txt -ASCII -DOUBLE -TABS points
clear points

disp('Done.')
