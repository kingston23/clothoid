function output = K_max_Cond(K_)
% Helper function for finding Kmax parameter.

global LT

e_max = LT.e_max;
s_max = LT.s_max;
DeltaTh_max = LT.DeltaTh_max;

% In order to avoid negative C
if K_<0
    output=500000;
    return;
end

syms th s x y C

% At this initial curvature we have maximum approximation error.
k0_me = DeltaTh_max / s_max;

th = k0_me*s + 1/(2*C*C)*s^2;
x = (int(cos(th), s, 0, s));
y = (int(sin(th), s, 0, s));

% Uvrst konkretne brojke
C_ = K_ / k0_me;

x_ = double(subs(x, {s,C}, {s_max,C_}));
y_ = double(subs(y, {s,C}, {s_max,C_}));

% Endpoint of circular arc of radius 1/k0_me at length s_max
x2 = 1/k0_me*cos(k0_me*s_max-pi/2);
y2 = 1/k0_me*(1+sin(k0_me*s_max-pi/2));

% Error at this point
e = sqrt( (x_-x2)^2 + (y_-y2)^2 );

% output is equal to zero if error is equal to e_max.
% This actually gives deviation from desired error in milimeters.
% If it would be in meters, fzero has hard time finding a solution.
output = (e - e_max)*1000;
