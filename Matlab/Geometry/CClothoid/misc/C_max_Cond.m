function output = C_max_Cond(C_)
% Helper function for finding C_max parameter.

global LT

e_max = LT.e_max;
s_max = LT.s_max;

% In order to avoid negative C
if C_<0
    output=50000;
    return;
end

syms th s x y C

th = 1/(2*C*C)*s^2;
x = (int(cos(th), s, 0, s));
y = (int(sin(th), s, 0, s));

x_ = double(subs(x, {s,C}, {s_max,C_}));
y_ = double(subs(y, {s,C}, {s_max,C_}));
% Approximation error when clothoid is approximated by a line.
% Error is measured at clothoid length s_max.
e = sqrt((s_max-x_)^2 + (y_)^2); 

% output is equal to zero if error is equal to e_max
% This actually gives deviation from desired error in milimeters.
% If it would be in meters, fzero has hard time finding a solution.
output = (e - e_max)*1000;
