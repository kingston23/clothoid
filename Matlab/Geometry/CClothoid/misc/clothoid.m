%% Skripta za isprobavanje klotoida - gleda se samo geometrija bez dimenzije vremena
% Skripta se ne pokrece najedamput nek je podijeljena u cellove pa se pokrece zeljeni cell
% Obicno je preduvjet za pokretanje nekog cella da se prvo pokrene prvi cell


% Znacenja varijabli
% x0, y0, fi0 - pocetna tocka i orijentacija klotoide
% c - brzina skretanja klotoide
% k0, k - pocetna i trenutna zakrivljenost
% s - prijedeni put - koristi se kao parametar
syms fi fi0 c k0 k x x0 y y0 s

k = k0 + c*s;
%fi = fi0 + k0*s + c/2*s^2;
fi = fi0 + int(k);
x = simple(x0+int(cos(fi), s, 0, s));
y = simple(y0+int(sin(fi), s, 0, s));

% boje za crtanje
colors = ['r' 'g' 'b' 'm' 'c' 'y' 'k' 'r' 'g' 'b' 'm' 'k' 'y' 'c' 'r' 'g' 'b' 'm' 'k' 'y' 'c' 'r' 'g' 'b' 'm' 'k' 'y' 'c' ];

%% Racunanje fresnelovog integrala radi usporedbe koliko je matlab precizan
x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0;
c_ = pi;
s_ = 0.01;

x_ = double(subs(x, {x0 y0 fi0 k0 c s}, {x0_ y0_ fi0_ k0_ c_ s_}));
y_ = double(subs(y, {x0 y0 fi0 k0 c s}, {x0_ y0_ fi0_ k0_ c_ s_}));

%% Sad promatramo specijalni slucaj kad su svi pocetni uvjeti jednaki nuli
x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0;

k_ = subs(k, {x0 y0 fi0 k0}, {x0_ y0_ fi0_ k0_});
fi_ = subs(fi, {x0 y0 fi0 k0}, {x0_ y0_ fi0_ k0_});
x_ = subs(x, {x0 y0 fi0 k0}, {x0_ y0_ fi0_ k0_});
y_ = subs(y, {x0 y0 fi0 k0}, {x0_ y0_ fi0_ k0_});

%% Crtanje klotoida sa razlicitim zeljenim parametrima

figure(1), clf, hold on, grid on
%plot(cos([0:0.1:2*pi]), 1+sin([0:0.1:2*pi]))
axis equal

x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0;

s_plot = linspace(0, 5, 100);

%c_ = 1;

C_ = 1;

for i=1:1
    
    c_ = 1/C_^2;
    
    % Eventualno Ogranicavanje kuta
    %s_plot = linspace(0, sqrt(2*(pi)/c_), 100);
    
    % Uvrsti konkretne parametre, osim s
    x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    
    % Nadi tocke klotoide za zeljeni raspon s
    x_ = double(subs(x_, s, s_plot));
    y_ = double(subs(y_, s, s_plot));

    plot(x_, y_, colors(i));

    %c_ = c_ * 4;
    C_ = C_^2;
end

%% Crtanje omjera k0/sqrt(c) za razliciti k0.
% Uzme se neki k0, i trazi se c za taj k0 tak da je udaljenost izmedu krajnje tocke klotoide duge smax i
% iste te klotoide sa c=0 (tj. kruznog luka) jednaka e_max

figure(1), clf, hold on, grid on

global s_max e_max k0_

s_max = 5;
e_max = 0.003;
k0_ = pi/10-0.25;

nIter = 10;
cArray = zeros(1,nIter);
k0Array = zeros(1,nIter);

for i=1:nIter
    
    s_max = min(5, 1/k0_ * pi/2); % Ogranicavamo promjenu kuta na maksimalno pi/2
    
    x2 = 1/k0_*cos(k0_*5-pi/2);
    y2 = 1/k0_*(1+sin(k0_*5-pi/2));
    
    c_ = fzero(@fun, 5e-5, optimset('display', 'iter')); % 'TolFun', 1e-6,

    cArray(i) = c_;
    k0Array(i) = k0_;

    k0_ = 0.05 + k0_;
end

plot(k0Array, k0Array./sqrt(cArray)),shg
xlabel('k0');
ylabel('k0/sqrt(c)');

%% Crtanje klotoida sa razlicitim c koje pocinju sa zadanom poc zakrivljenoscu

figure(2), clf, hold on, grid on
axis equal

x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0.5;

c_ = 0.5;

s_plot = linspace(0, 2, 100);

for i=1:10
    
    % Uvrsti konkretne parametre, osim s
    x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    
    % Nadi tocke klotoide za zeljeni raspon s
    x_ = double(subs(x_, s, s_plot));
    y_ = double(subs(y_, s, s_plot));

    plot(x_, y_, colors(i));

    c_ = c_ * 1.2;
    
end

%% Crtanje klotoida sa razlicitim c koje pocinju sa zadanom poc zakrivljenoscu - al na drugi nacin - urazovoju

figure(3), clf, hold on, grid on
axis equal

x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0;

c_ = 0.5;

for i=1:10
    
    % Uvrsti konkretne parametre, osim s
    x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    
    % Nadi raspon s keri bu dal pravu poc. zakrivljenost
    s0 = 0.5 / c_;
    s_plot = linspace(s0, s0+2, 100);
    
    % Nadi tocke klotoide za zeljeni raspon s
    x_ = double(subs(x_, s, s_plot));
    y_ = double(subs(y_, s, s_plot));

    plot(x_, y_, colors(i));

    c_ = c_ * 1.2;
    
end

%% Crtanje klotoide sa parametrima x0=y0=fi0=k0=0,c!=0 pomocu klotoide koja ima x0=y0=fi0=k0=0 i c=1

figure(1), clf, hold on, grid on
axis equal

% Trazi se klotoida za sljedece parametre
x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0;
c_ = 0.5;

s_plot = linspace(0, 2, 100);

% Uvrsti konkretne parametre
x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
x_t = double(subs(x_, s, s_plot)); % ovo je tocna zeljena klotoida
y_t = double(subs(y_, s, s_plot));

% A sad da dobijemo tu istu pomocu klotoide sa x0=y0=fi0=k0=0 i c=1
x_ = subs(x, {x0 y0 fi0 k0 c}, {0 0 0 0 1});
y_ = subs(y, {x0 y0 fi0 k0 c}, {0 0 0 0 1});
x_a = 1/sqrt(abs(c_))*subs(x_, s, sqrt(abs(c_))*s);
y_a = sign(c_)/sqrt(abs(c_))*subs(y_, s, sqrt(abs(c_))*s);

plot(double(subs(x_t, s, s_plot)), double(subs(y_t, s, s_plot)), 'r');
plot(double(subs(x_a, s, s_plot)), double(subs(y_a, s, s_plot)), 'b');

%% Crtanje klotoide sa parametrima x0,y0,fi0,k0,c pomocu klotoide koja ima xz0=yz0=fiz0=kz0=0 i cz=c

figure(1), clf, hold on, grid on
axis equal

% Trazi se klotoida za sljedece parametre
x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0.5;
c_ = -1.5;

s_plot = linspace(0, 2, 100);

% Uvrsti konkretne parametre
x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
x_t = double(subs(x_, s, s_plot)); % ovo je tocna zeljena klotoida
y_t = double(subs(y_, s, s_plot));

% A sad da dobijemo tu istu pomocu klotoide sa xz0=yz0=fiz0=kz0=0 i cz=c
x_ = subs(x, {x0 y0 fi0 k0 c}, {0 0 0 0 c_});
y_ = subs(y, {x0 y0 fi0 k0 c}, {0 0 0 0 c_});
r11 = cos(-k0_^2/2/c_+fi0_);
r12 = -sin(-k0_^2/2/c_+fi0_);
r21 = sin(-k0_^2/2/c_+fi0_);
r22 = cos(-k0_^2/2/c_+fi0_);
x_zlj = subs(x_, s, s+k0_/c_) - subs(x_, s, k0_/c_);
y_zlj = subs(y_, s, s+k0_/c_) - subs(y_, s, k0_/c_);
x_a = x0_ + r11*x_zlj + r12*y_zlj;
y_a = y0_ + r21*x_zlj + r22*y_zlj;

plot(double(subs(x_t, s, s_plot)), double(subs(y_t, s, s_plot)), 'r');
plot(double(subs(x_a, s, s_plot)), double(subs(y_a, s, s_plot)), 'b');

%% Crtanje bilo koje klotoide pomocu osnovne klotoide koja ima x0=y0=fi0=k0=0 i c=1

figure(1), clf, hold on, grid on
axis equal

% Trazi se klotoida za zeljene parametre
x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = -0.5;
c_ = 1.5;

s_plot = linspace(0, 2, 100);

% Uvrsti konkretne parametre
x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
x_t = double(subs(x_, s, s_plot)); % ovo je tocna zeljena klotoida
y_t = double(subs(y_, s, s_plot));

% A sad da dobijemo tu istu pomocu osnovne klotoide sa x0=y0=fi0=k0=0 i c=1
x_ = subs(x, {x0 y0 fi0 k0 c}, {0 0 0 0 1});
y_ = subs(y, {x0 y0 fi0 k0 c}, {0 0 0 0 1});
r11 = cos(-k0_^2/2/c_+fi0_);
r12 = -sin(-k0_^2/2/c_+fi0_);
r21 = sin(-k0_^2/2/c_+fi0_);
r22 = cos(-k0_^2/2/c_+fi0_);
x_zlj = 1/sqrt(abs(c_))* ...
    ( subs(x_, s, sqrt(abs(c_))*(s+k0_/c_)) - subs(x_, s, sqrt(abs(c_))*k0_/c_) );
y_zlj = sign(c_)/sqrt(abs(c_))* ...
    ( subs(y_, s, sqrt(abs(c_))*(s+k0_/c_)) - subs(y_, s, sqrt(abs(c_))*k0_/c_) );
x_a = x0_ + r11*x_zlj + r12*y_zlj;
y_a = y0_ + r21*x_zlj + r22*y_zlj;

plot(double(subs(x_t, s, s_plot)), double(subs(y_t, s, s_plot)), 'r');
plot(double(subs(x_a, s, s_plot)), double(subs(y_a, s, s_plot)), 'b');


%% Zadavanje parametara za odabir zahtjevanog skupa parametara klotoida.
% Ti parametri nam trebaju kasnije za odredivanje parametara lookup tablice.

global LT % DeltaTh_max s_max e_max 

LT.DeltaTh_max = pi/2; % Maksimalna promjena orijentacije koju radi jedna klotoida
LT.s_max = 5; % Maksimalna duljina jedne klotoide
LT.e_max = 1e-3; % Maksimalna dozvoljena pogreska aproksimacije

LT.e_typical = 1e-9; % Tipicna pogreska aproksimacije (ona koja se dogada na tipicnim vrijednostima C)
LT.C_typical = 0.5; % Tipicni faktor skaliranja na kojem zelimo imati tipicnu pogresku

%LT.DeltaTh_L_max = 10*pi/180; % Maksimalna promjena kuta izmedu dvije tocke klotoide u lookup tablici (vise se ne koristi)

%% Odredivanje Cmin minimalnog scaling faktora za zadanu pogresku

LT.C_min = fzero(@C_min_Cond, 1e-2, optimset('display', 'iter')); % 'TolFun', 1e-6,)

% Za dokaz - nacrtamo tu klotoidu
figure(1), clf, hold on, grid on

plot(LT.e_max*cos(linspace(0,2*pi,100)), LT.e_max*sin(linspace(0,2*pi,100)),'g')
axis equal

x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0;

C_ = LT.C_min;

for i=1:1
    
    c_ = 1/C_^2;
    
    s_plot = linspace(0, sqrt(2*pi/c_), 100);
    
    % Uvrsti konkretne parametre, osim s
    x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    
    % Nadi tocke klotoide za zeljeni raspon s
    x_ = double(subs(x_, s, s_plot));
    y_ = double(subs(y_, s, s_plot));

    plot(x_, y_, colors(i));
end

%% Odredivanje Cmax maksimalnog scaling faktora za zadanu pogresku

LT.C_max = fzero(@C_max_Cond, 100, optimset('display', 'iter')); % 'TolFun', 1e-6,)
LT.C_max_initial = LT.C_max; % kasnije ga mozda promijenimo

% Za dokaz - nacrtamo tu klotoidu
figure(1), clf, hold on, grid on

plot([0 LT.s_max],[0 0],'g')

x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0;

C_ = LT.C_max;

for i=1:1
    
    c_ = 1/C_^2;
    
    s_plot = linspace(0, LT.s_max, 100);
    
    % Uvrsti konkretne parametre, osim s
    x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
    
    % Nadi tocke klotoide za zeljeni raspon s
    x_ = double(subs(x_, s, s_plot));
    y_ = double(subs(y_, s, s_plot));

    plot(x_, y_, colors(i));
end

%% Odredivanje Kmax (maksimalne slicnosti klotoide sa kruznicom)

LT.K_max = fzero(@K_max_Cond, 100, optimset('display', 'iter')); % 'TolFun', 1e-6,)

% Za dokaz - nacrtamo tu klotoidu i kruznicu
figure(1), clf, hold on, grid on

k0_me = LT.DeltaTh_max / LT.s_max;
plot(1/k0_me*sin(linspace(0,k0_me*LT.s_max,100)), 1/k0_me*(1-cos(linspace(0,k0_me*LT.s_max,100))),'g')
axis equal

x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0;

C_ = LT.K_max / k0_me;

for i=1:1
    
    c_ = 1/C_^2;
    
    s_plot = linspace(0, LT.s_max, 100);
    
    % Uvrsti konkretne parametre, osim s
    x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_me c_});
    y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_me c_});
    
    % Nadi tocke klotoide za zeljeni raspon s
    x_ = double(subs(x_, s, s_plot));
    y_ = double(subs(y_, s, s_plot));

    plot(x_, y_, colors(i));
end

%% Odredivanje parametara lookup tablice - prvo treba odvrtiti gornje cellove, a to su:
% inicijalizacijski, za C_min, za C_max i za K_max.

% Maksimalna zahtijevana duljina klotoide u tablici.
LT.sL_max = min(sqrt(2*LT.DeltaTh_max), LT.s_max/LT.C_min) + LT.K_max;

% Treba rucno odabrati neku zaokruzenu vrijednost vecu od ove gore. U ovom slucaju:
%LT.sL = 46.1;
%LT.sL = 1403;
%LT.sL = LT.sL_max*1.01;
LT.sL = 1.78;

%STARO: LT.Delta_sL_max = min(2.8062*(LT.e_max/LT.C_min)^(1/3), LT.DeltaTh_L_max/LT.sL);
LT.Delta_sL_max = (2*sqrt(2)*0.084)^(-1/3) *(min(LT.e_max/LT.C_max, LT.e_typical/LT.C_typical))^(1/3);

LT.C_max = 3400; % empirijski

% Treba rucno odabrati neku zaokruzenu vrijednost manju od ove gore. I po
% mogucnosti da bude visekratnik od sL. Zato:
LT.Delta_sL = 0.002;
%LT.Delta_sL = 0.00095;

% Potreban broj elemenata u lookup tablici:
N_L = ceil(LT.sL / LT.Delta_sL) + 1; % +1 je radi nule
nBytes = N_L * 2 * 8; % Ak se koriste double vrijednosti + svaka koordinata ima x i y

LT.sL = (N_L-1) * LT.Delta_sL; % Opet korekcija da sL i Delta_sL budu "visekratnici"

% Ovo vrijedi uvijek - za sve tablice. Jer smo u disertaciji dobili da je
% ovaj parametar redundantan.
LT.C_L = 1;


%% Odredivanje lookup tablice za klotoide
% Tablica sluzi za odredivanje bilo koje klotoide pomocu osnovne klotoide koja ima x0_L=y0_L=fi0_L=k0_L=0 i c_L>0
% spremljene u tablici
% Tu radimo proracun tablice, a proracun klotoide je u cellu ispod.
% Parametri tablice odreduju se u cellu iznad

LT.x0_L = 0;
LT.y0_L = 0;
LT.fi0_L = 0;
LT.k0_L = 0;
LT.c_L = 1/LT.C_L^2;

% Uvrsti konkretne parametre, osim s
x_ = subs(x, {x0 y0 fi0 k0 c}, {LT.x0_L LT.y0_L LT.fi0_L LT.k0_L LT.c_L});
y_ = subs(y, {x0 y0 fi0 k0 c}, {LT.x0_L LT.y0_L LT.fi0_L LT.k0_L LT.c_L});

% Nadi tocke klotoide za zeljeni raspon s i spremi ih - to je lookup tablica

s_look = 0 : LT.Delta_sL : LT.sL;
LT.x_L = double(subs(x_, s, s_look));
LT.y_L = double(subs(y_, s, s_look));
clear s_look;

%% Odredivanje generalne klotoide pomocu lookup tablice iz gornjeg cell-a

% Parametri od generalne klotoide koju zelimo odrediti
x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0;
% Zadavanje prek faktora skaliranja - nedostatak je da c nemre biti <0
%C_ = LT.C_min; c_ = 1/C_^2;

% Zadavanje prek sharpness-a
c_ = 1; C_ = 1/sqrt(abs(c_));

% Parametar s odrediti tak da se ne prijede maksimalni kut DeltaTh_max
s_end = min(-C_^2 * k0_ + C_*sqrt(C_^2*k0_^2 + 2*LT.DeltaTh_max), LT.s_max); % Paznja ovo ne dela za k0_<0
s_plot = 0 : C_*LT.Delta_sL/10 : s_end; % Interval je vazno "poravnati" s periodom uzorkovanja tablice
s_plot2 = (0 : C_*LT.Delta_sL : s_end) - C_*LT.Delta_sL*0.01; % Tak da imamo tocke malo prije C_*LT.Delta_sL
s_plot = sort([s_plot s_plot2(2:end)]);
%s_plot = s_plot + 0.002;
if(length(s_plot)< 10)
    s_plot = linspace(0, s_end, 1000); % Raspon s za crtanje
    warning('Sumnjiv raspon');
end
%s_plot = 0:0.001:0.01;

% Sad simbolicki najdi tocnu klotoidu radi usporedbe

% Uvrsti konkretne parametre
x_ = subs(x, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
y_ = subs(y, {x0 y0 fi0 k0 c}, {x0_ y0_ fi0_ k0_ c_});
x_t = double(subs(x_, s, s_plot));
y_t = double(subs(y_, s, s_plot));

% A sad probamo dobiti tu istu klotoidu z lookup tablicoj
x_a = ones(1, length(x_t)); % aproksimacija - alociramo nizove
y_a = ones(1, length(y_t));

% Provjeravamo parametre trazene klotoide.
% Ak izlaze izvan raspona, koristiti aproksimaciju tockom, linijom ili
% kruznicom, ovisno koji slucaj nastupi.

K_ = k0_*C_;

if(abs(C_)<LT.C_min)
    % Aproksimacija tockom
    x_a = x0_ * ones(1, length(x_t));
    y_a = y0_ * ones(1, length(x_t));
    warning('Aproksimacija tockom');
elseif(k0_==0 && abs(C_)>LT.C_max) %
    % Aproksimacija pravcem
    x_a = x0_ + cos(fi0_)*s_plot;
    y_a = x0_ + sin(fi0_)*s_plot;
    warning('Aproksimacija pravcem');
elseif(k0_~=0 && abs(K_)>LT.K_max)
    % Aproksimacija kruznicom
    x_a = x0_ + 1/k0_*(-sin(fi0_)+sin(fi0_+k0_*s_plot));
    y_a = y0_ + 1/k0_*( cos(fi0_)-cos(fi0_+k0_*s_plot));
    warning('Aproksimacija kruznicom');
else
    % Aproksimacija tablicom

    % Priprema rot matrice
    angRot = -k0_*k0_/2/c_ + fi0_;
    r11 = cos(angRot);
    r12 = -sin(angRot);
    r21 = sin(angRot);
    r22 = cos(angRot);
    
    sL_max_ovajput = 0;

    for i=1:length(s_plot)

        s_ = s_plot(i);
        s_L1 = sqrt(abs(c_)/LT.c_L) * (s_ + k0_/c_); % inputi za lookup tablicu
        s_L2 = sqrt(abs(c_)/LT.c_L) * k0_/c_;
        
        sL_max_ovajput = max(sL_max_ovajput, s_L1);
        sL_max_ovajput = max(sL_max_ovajput, s_L2);

        % Racunanje outputa lookup tablice (tu je ukljucena i interpolacija)
        [x1, y1] = GetBasicClothoidCoordsLT(s_L1);
        [x2, y2] = GetBasicClothoidCoordsLT(s_L2);

        xz = (x1-x2)*sqrt(LT.c_L/abs(c_)) ;
        yz = (y1-y2)*sqrt(LT.c_L/abs(c_)) * sign(c_);

        x_a(i) = x0_ + r11*xz + r12*yz;
        y_a(i) = y0_ + r21*xz + r22*yz;
    end
    
    fprintf(1, 'sL_max_ovajput = %d\n', sL_max_ovajput);
    
end

figure(1), clf, hold on, grid on
axis equal
plot(x_t, y_t, 'r');
plot(x_a, y_a, 'b');
xlabel('x')
ylabel('y')
shg

figure(2), clf, hold on, grid on
dx = x_t - x_a;
dy = y_t - y_a;
plot(s_plot, sqrt(dx.*dx + dy.*dy));
xlabel('s [m]')
ylabel('e [m]')
title('error');
shg

% Za ispitivanje promjene kuta izmedu dvije tocke u lookup tablici. Ne bi
% smjela biti veca od oko 10 stupnjeva
%figure(3), clf, hold on, grid on
%plot(s_look(1:end-1), diff(c_L*s_look.^2/2)*180/pi);
%xlabel('s')
%ylabel('\Delta \theta_L [ ^\circ]')
%shg

%% Isprobavanje interpolacije
% Simbolicki izrazi za x i y moraju biti vec izracunati
% Paznja - ovo prepisuje LT

% Kreiraj prvo lookup tablicu tako da bude rijetka
LT.C_L = 1;
LT.x0_L = 0;
LT.y0_L = 0;
LT.fi0_L = 0;
LT.k0_L = 0;
LT.c_L = 1/LT.C_L^2;
LT.Delta_sL = 0.25; % Ovo je razmak izmedu tocaka lookup tablice
LT.sL = 10; % Ovo je duljina lookup tablice

% Uvrsti konkretne parametre, osim s
x_ = subs(x, {x0 y0 fi0 k0 c}, {LT.x0_L LT.y0_L LT.fi0_L LT.k0_L LT.c_L});
y_ = subs(y, {x0 y0 fi0 k0 c}, {LT.x0_L LT.y0_L LT.fi0_L LT.k0_L LT.c_L});

% Nadi tocke klotoide za zeljeni raspon s i spremi ih - to je lookup tablica

s_look = 0 : LT.Delta_sL : LT.sL;
LT.x_L = double(subs(x_, s, s_look));
LT.y_L = double(subs(y_, s, s_look));
clear s_look;

global LT

s_Array = 0 : 0.01 : LT.sL;
xL_Array = zeros(1, length(s_Array));
yL_Array = zeros(1, length(s_Array));
xS_Array = zeros(1, length(s_Array));
yS_Array = zeros(1, length(s_Array));

for i=1:length(s_Array)
    s_c = s_Array(i);
    % Izracunaj pomocu tablice i interpolacije
    [xL_Array(i) yL_Array(i)] = GetBasicClothoidCoordsLT(s_c);
    % Izracunaj tocno simbolicki
    xS_Array(i) = double(subs(x_, s, s_c));
    yS_Array(i) = double(subs(y_, s, s_c));
end

% Crtanje

% Prva slika: klotoide
figure(1)
hold off
plot(xS_Array, yS_Array) % tocna simbolicka
axis equal
hold on
plot(xL_Array, yL_Array, 'r') % interpolirana
shg
plot(LT.x_L, LT.y_L, 'g.') % tocke u lookup tablici

% Druga slika: pogreska
figure(2);
hold on
dx = xS_Array - xL_Array;
dy = yS_Array - yL_Array;
error = sqrt(dx.*dx + dy.*dy);
plot(s_Array, error);
grid on

%% Crtanje maksimalne pogreske aproksimacije klotoide za zadani raspon faktora skaliranja

% Zadavanje raspona faktora skaliranja
C1 = LT.C_min * 0.1;
C2 = LT.C_max * 10;

C_Array = logspace(C1, C2, 100);

% Ostali parametri klotoide (osim faktora skaliranja)
x0_ = 0;
y0_ = 0;
fi0_ = 0;
k0_ = 0;

MaxError_Array = zeros(1, length(C_Array));

% Prodi razlicite faktore skaliranja
for i=1:length(C_Array)
    C_ = C_Array(i);
    c_ = 1/C_^2;
    
    % Odredi raspon s tak da ne prijedemo maks duljinu ili kut
    s_end = min(-C_^2 * k0_ + C_*sqrt(C_^2*k0_^2 + 2*LT.DeltaTh_max), LT.s_max); % Paznja ovo ne dela za k0_<0
    s_end = min(s_end, C_*LT.Delta_sL);
    
    s_Array = linspace(0, s_end, 200); % Raspon s za odredivanje maks pogreske
    
    xL_Array = zeros(1, length(s_Array));
    yL_Array = zeros(1, length(s_Array));
    xS_Array = zeros(1, length(s_Array));
    yS_Array = zeros(1, length(s_Array));

    for j=1:length(s_Array)
        s_c = s_Array(j);
        % Izracunaj pomocu tablice i interpolacije
        [xL_Array(j) yL_Array(j)] = GetBasicClothoidCoordsLT(s_c);
        % Izracunaj tocno simbolicki
        xS_Array(j) = double(subs(x_, s, s_c));
        yS_Array(j) = double(subs(y_, s, s_c));
    end
    
    dx = xS_Array - xL_Array;
    dy = yS_Array - yL_Array;
    Error = sqrt(dx.*dx + dy.*dy);
    plot(s_Array, Error);
    grid on;
    
    MaxError_Array(i) = max(Error);
    
    pause
    
end

plot(s_Array, MaxError_Array);
grid on;
