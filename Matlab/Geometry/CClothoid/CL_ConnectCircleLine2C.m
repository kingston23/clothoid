function [bOk C CL1 CL2] = CL_ConnectCircleLine2C(P0, G1, G2, bDraw)
% Izgladivanje uz non-zero pocetnu zakrivljenost
% Problem se svodi na spajanje kruznice i pravca koristenjem dviju klotoida
%
% P0 - pocetno stanje koje se sastoji od P0.x, P0.y, P0.th, P0.k
% G1, G2 - linija na koju moramo "sletjeti" koristenjem klotoida
% C - scaling factor - rjesenje jednadzbe
% CL1 - ulazna klotoida
% CL2 - izlazna klotoida

% TUDU: ogranicenje duljine klotoide

bOk = true;

C = 0;
CL1 = [];
CL2 = [];

if(nargin<4)
    bDraw = false;
end

%% Ispitivanje argumenata
if(P0.k == 0)
    bOk = false; warning('Ova funkcija zahtijeva poc. zakrivljenost != 0'); return;
end

%% Ispitivanje nuznih uvjeta
% Ovo nisu uvjeti postojanja rjesenja vec ovime reduciramo skup mogucih rjesenja.
% U nekim slucajevima rjesenje moze postojati cak i ako ovi uvjeti nisu ispunjeni, ali takva rjesenja ne razmatramo.
% Jedan od razloga je i numericka nestabilnost koja bi se mogla pojaviti u takvim slucajevima.

% Tangenta na putanju kroz tocku P0
P0Tangent = LP_LineFromDirectionAndPoint(P0.th, P0.x, P0.y);

% 1. nuzan uvjet: Ako se pocetno gibamo ulijevo, onda i cilj mora biti s lijeve strane, i obrnuto.
% Uzimamo da G1 ili G2 moraju biti lijevo
if(LP_GetDistanceToPoint(P0Tangent, G1.x, G1.y) * P0.k <= 0 && LP_GetDistanceToPoint(P0Tangent, G2.x, G2.y) * P0.k <= 0)
    bOk = false; warning('Cilj je sa krive strane'); %return;
end

% 2. nuzan uvjet: Ako skrecemo ulijevo, onda pocetna tocka mora biti lijevo od vektora G1G2, i obrnuto

% Pravac G1-G2
[tmp G1G2] = LP_LineFromPoints(G1.x, G1.y, G2.x, G2.y);


if(LP_GetDistanceToPoint(G1G2, P0.x, P0.y) * P0.k < 0)
    bOk = false; warning('Nedozvoljen smjer'); %return;
end

% 3. nuzan uvjet: Zakret mora biti 0<|zakret|2pi
% Ovaj je uvjet 



%% Bracketiranje rjesenja

% Ajmo probati naci donju granicu faktora skaliranja C1 (to zasad radimo kao da je pozitivni zakret)
% Donju granicu nademo tak da izracunamo faktor C uz pretpostavku da je k0=0 (poc. zakrivljenost nula).

% Tangenta kroz tocku P0 (al u suprotnom smjeru od P0.th)
P0Tangent = LP_LineFromDirectionAndPoint(P0.th+pi, P0.x, P0.y);
Bisection = LP_GetSymmetricalLine(P0Tangent, G1G2);

% s1 = sqrt(2*NormAngle_Zero_2PI(Bisection.m_Direction-P0.th)); - stari nacin (losiji, al nekad zna biti i bolji od donjeg)
s1 = sqrt(2*NormAngle_Zero_PI(Bisection.m_Direction-pi/2-P0.th));
[dx dy] = GetBasicClothoidCoordsLT(s1);
%[dx dy] = RotatePoint(dx, dy, P0.th);
D2 = dx*dx + dy*dy;
% Udaljenost pocetne tocke P0 do bisekcije
d_P0_Bisection = LP_GetDistanceToPoint(Bisection, P0.x, P0.y);
d2_P0_Bisection = d_P0_Bisection*d_P0_Bisection;

if(GE_IsEqualApr(D2, 0))
    bOk = false; warning('Trazeni zakret je zanemariv - i moze se aproksimirati pravcem'); return;
end

if(GE_IsEqualApr(d_P0_Bisection, 0))
    bOk = false; warning('Nemoguce zakrenuti na tako maloj udaljenosti - bisekcija je preblizu poc tocki'); return;
end

% Lijevi bracket koji se ovako dobije je u pravilu premali, i trebalo bi biti fMin(C1)<0
C1 = sqrt(d2_P0_Bisection/D2);

% Gornja granica C2 - dobije se ogranicenjem ukupnog zakreta na manje od 2pi
% Ukupni zakret se raspodijeljuje na ulaznu i izlaznu klotoidu.

dth = G1G2.m_Direction - P0.th; % Ukupni zakret
if(P0.k>0)
    % Pozitivni zakret - to znaci da i zahtjevani zakret mora biti pozitivan
    dth = NormAngle_Zero_2PI(dth);
    dth_P1P0_max = min(2*pi-dth, pi); % Ukupni zakret dijela od P1 do P0
    C2 = sqrt(2*dth_P1P0_max)/P0.k;
else
    dth = NormAngle_Minus2PI_Zero(dth);
    dth_P1P0_max = max(-2*pi-dth, -pi);
    C2 = sqrt(-2*dth_P1P0_max)/(-P0.k);
end

if(C1>C2) % Ovo se desilo kod ekstremno brzih zakreta
    bOk = false; warning('Ne mozemo bracketirati rjesenje'); return;
end

if(P0.k<0)
    C1 = -C1;
    C2 = -C2;
end

bFoundSolution = false;

[f1 Info] = fMin1(C1, P0, G1G2, false);
if(Info.bInadmissibleSolution)
    % Kad bi isli povecavat C, rjesenje bi i dalje ostalo nedopustivo - znaci nema rjesenja
    warning('Rjesenje je odmah na pocetku nedopustivo - dakle nema rjesenja');
    %bOk = false; return;
end

f2 = fMin1(C2, P0, G1G2, false);

if(f1 == 0)
    C = C1;
    bFoundSolution = true;
elseif(f2 == 0)
    C = C2;
    bFoundSolution = true;
elseif(f1*f2 > 0)
    warning('velika je vjerojatnost da rjesenje ne postoji (nije bracketed)');
    bOk = false;
elseif(1)
    % Rijesi metodom bisekcije.
    f = f1;
    
    if(f<0)
       dC = C2-C1;
       C = C1;
    else
       dC = C1-C2;
       C = C2;
    end

    for(i=1:30)

        dC = 0.5*dC;
        Cmid = C + dC;
        fmid = fMin1(Cmid, P0, G1G2, false);

        if(fmid<0)
            C = Cmid;
        end
        if(abs(dC)<0.0001 || abs(fmid)<eps)
            fprintf(1, 'Found solution in %d iteration(s)\n', i);
            bFoundSolution = true;
            break;
        end
    end

    if(~bFoundSolution)
        warning('premasen max. broj iteracija');
        bOk = false;
    end
    
    if(bFoundSolution && Info.bInadmissibleSolution)
        warning('pronadeno rjesenje nije dopustivo');
    end    
    
end

%% Rijesi false position metodom
if(0)
    fl = fMin1(C1, P0, G1G2);
    fh = fMin1(C2, P0, G1G2);
    if(fl*fh > 0)
        warning('velika je vjerojatnost da rjesenje ne postoji (nije bracketed)');
        bOk = false; return;
    end
    if(fl<0)
       Cl = C1;
       Ch = C2;
    else
       Cl = C2;
       Ch = C1;
       swap = fl;
       fl = fh;
       fh = swap;
    end
    dC = Ch - Cl;

    bFoundSolution = false;
    for(i=1:30)

        C = Cl + dC*fl/(fl-fh);
        f = fMin1(C, P0, G1G2);
        if(f<0)
            del = Cl-C;
            Cl = C;
            fl = f;
        else
            del = Ch-C;
            Ch = C;
            fh = f;
        end

        dC = Ch-Cl;

        if(abs(del)<0.0001 || abs(f)<eps)
            fprintf(1, 'Found solution in %d iteration(s)\n', i);
            bFoundSolution = true;
            break;
        end
    end
end

%% Izracunaj pocetnu i zavrsnu klotoidu
if(bFoundSolution)
    % Moramo ponoviti proracun, jer zadnji poziv funkcije fMin mozda nije onaj koji odgovara rjesenju (tj. odbacen je)
    [fmid Info] = fMin1(C, P0, G1G2);

    CL1.x0 = P0.x;
    CL1.y0 = P0.y;
    CL1.th0 = P0.th;
    CL1.k0 = P0.k;
    CL1.C = C;
    CL1.c = sign(C)/(C*C);
    CL1.length = Info.FirstClothLength;
    CL1.Type = 'CL';

    [x y] = GetClothoidPoint(CL1, CL1.length);
    CL2.x0 = x;
    CL2.y0 = y;
    CL2.c = -CL1.c;
    CL2.C = -CL1.C;
    CL2.length = Info.SecondClothLength;
    CL2.th0 = CL1.th0 + CL1.k0*CL1.length + 0.5*CL1.c*CL1.length*CL1.length;
    CL2.k0 = CL1.k0 + CL1.c*CL1.length;
    CL2.Type = 'CL';
    
    % CL3 je pocetni dio prve klotoide
    CL3.x0 = Info.P1.x;
    CL3.y0 = Info.P1.y;
    CL3.th0 = Info.P1.th;
    CL3.k0 = 0;
    CL3.C = C;
    CL3.c = sign(C)/(C*C);
    CL3.length = Info.SecondClothLength - Info.FirstClothLength;
    CL3.Type = 'CL';
    
    % Provedi jos dodatni test dopustivosti - klotoida se mora spustiti na pravac G1G2 prije tocke G2
    G1G2_imp = LP_GetLineImp(G1G2);
    G1G2_perp = LI_GetPerpendicularLineThroughPoint(G1G2_imp, G2.x, G2.y);
    [xEnd yEnd] = GetClothoidPoint(CL2, CL2.length);
    if(~LI_IsPointLeft(G1G2_perp, xEnd, yEnd))
        warning('solution is not before G2');
        bOk = false;
    end
    
    
    disp('solved');
else
    warning('no solution found');
    bOk = false;
end

%%
if(bDraw && bFoundSolution)
    figure(1), hold on, grid on
    axis equal
    
    %plot([G1.x G2.x], [G1.y G2.y], 'r');
    
    Circle = CI_SetCircleFromTangent(P0.x, P0.y, P0.th, 1/P0.k);
    CI_DrawCircle(Circle, 'b-.');
    %LP_DrawLineAsArrow(G1G2, [G1.x G2.x G1.y G2.y], 'r', 'interval');
    arrow([G1.x G1.y], [G2.x G2.y]);
    plot(P0.x, P0.y, 'r.');
    plot(Info.P1.x, Info.P1.y, 'r.');
    [xmin1 xmax1 ymin1 ymax1] = CL_Draw(CL1, 'r');
    [xmin2 xmax2 ymin2 ymax2] = CL_Draw(CL2, 'r');
    CL_Draw(CL3, 'r--');
    xmin = min(xmin1, xmin2);
    xmax = max(xmax1, xmax2);
    ymin = min(ymin1, ymin2);
    ymax = max(ymax1, ymax2);
    
    %LP_DrawLineAsArrow(Info.Bisection, [xmin xmax ymin ymax], 'g', 'interval');
    LP_DrawLine(Info.Bisection, [xmin xmax ymin ymax], 'g--', 'interval');
    %LP_DrawLineAsArrow(Info.P1TangentOpp, [xmin xmax ymin ymax], 'r', 'interval');
end

%% Crtanje funkcije koja se minimizira
if(0)
    bDrawPolako = false;
    %fMin1(C2, P0, G1G2, true); pause;
    
    if(bDrawPolako)
        C_plot = linspace(C1+(C2-C1)*0.0, C2, 20);
    else
        C_plot = linspace(C1, C2*0.99, 500);
    end
    f_plot = zeros(size(C_plot));

    for(i=1:length(C_plot))
        if(bDrawPolako)
            f_plot(i) = fMin1(C_plot(i), P0, G1G2, true);
            fprintf('C=%f, f=%f\n', C_plot(i), f_plot(i));
            pause;
        else
            f_plot(i) = fMin1(C_plot(i), P0, G1G2, false);
        end
    end
    figure(2), clf, hold on, grid on;
    plot(C_plot, f_plot);
end

return;




%% Funkcija koju minimiziramo - bolja verzija
function [out Info] = fMin1(C, P0, G1G2, bDraw)

if(nargin<4)
    bDraw=false;
end
Info.bInadmissibleSolution = false; % Dal je trenutno rjesenje dopustivo


%% 1. korak
% Izracunaj tocku P1 - to je pocetna tocka klotoide provucene kroz tocku P0 i u njoj je zakrivljenost =0

CL.x0=0;
CL.y0=0;
CL.th0=0;
CL.k0=0;
CL.C=C;
CL.c=sign(C)/(C*C);

sp0 = P0.k/CL.c; % parametar s u tocki P0
[dx dy] = GetClothoidPoint(CL, sp0); % Promjena x i y od tocke P1 do tocke P0
dth = 0.5*CL.c*sp0*sp0;

% Pomak dx i dy treba rotirati tako da pase orijentacija u tocki P0
angRot = - dth + P0.th;
[dx dy] = RotatePoint(dx, dy, angRot);

P1.x = P0.x - dx;
P1.y = P0.y - dy;
P1.th = P0.th - dth;


%% 2. korak
% Izracunaj pravac bisekcije ili polovista zakreta - to je os simetrije izmedu tangente u tocki P1 i pravca G1G2

% Tangenta kroz P1, ali suprotnog smjera od kuta P1.th - to je radi racunanja osi simetrije
P1TangentOpp = LP_LineFromDirectionAndPoint(P1.th+pi, P1.x, P1.y);
    
if(LP_IsParallelApr(P1TangentOpp, G1G2))
   % Ak su pravci bili paralelni, bisekcija se ne moze pouzdano proracunati jer moze biti u beskonacnosti
   
   dG1G2_P1 = LP_GetDistanceToPoint(G1G2, P1.x, P1.y);
   if(GE_IsEqualApr(dG1G2_P1, 0))
       out = 0; % Ovo nam je prakticki rjesenje
       Bisection = LP_LineFromDirectionAndPoint(G1G2.m_Direction+pi/2, P0.x, P0.y);
       s = 0;
   elseif(dG1G2_P1*P0.k<0)
       s = 0;
       out = inf;
   else
       s = 0;
       out = -inf;
   end
else
    Bisection = LP_GetSymmetricalLine(P1TangentOpp, G1G2);
    thBisection = Bisection.m_Direction;
    
%% 3. korak
    % Nadi mjesto na klotoidi koja ide iz tocke P1 koje je okomito na pravac bisekcije i odredi njegovu udalj.
    % od pravca bisekcije

    %CL.x0 = P0.x;
    %CL.y0 = P0.y;
    %CL.th0 = P0.th;
    %CL.k0 = P0.k;
    %CL.length = abs(C)*sqrt(2*pi) - sp0; % Kut poluzakreta moze biti max pi
    %if(CL.length<0); error('Klotoida vec u startu ima zakret >pi'); end

    CL.x0 = P1.x;
    CL.y0 = P1.y;
    CL.th0 = P1.th;
    CL.k0 = 0;
    CL.length = abs(C)*sqrt((2*pi-eps)); % Kut poluzakreta moze biti max pi

    s1 = CL_GetPointWithOrientation(CL, thBisection-pi/2); 
    s2 = CL_GetPointWithOrientation(CL, thBisection+pi/2);
    s = min(s1, s2);
    
    sFound = false;
    if(GE_IsLessOrEqualApr(s, CL.length))
        sFound = true;
    end

    if(sFound)
        [xLateral yLateral] = GetClothoidPoint(CL, s);
        distToBisection = LP_GetDistanceToPoint(Bisection, xLateral, yLateral);

        if(C>0)
            out = -distToBisection;
        else
            out = distToBisection;
        end
        
        if(s<sp0)
            % Mjesto gdje je klotoida okomita na bisekciju je prije tocke P0.
            % ovakvo rjesenje je mozda matematicki tocno, al je nedopustivo.
            Info.bInadmissibleSolution = false;
        end

        % Za probu - ovo utjece jako lose na metodu false position jer se funkcija izgladuje kod ishodista
        % out = sign(out)*out*out; 
    else
        % Kaj napraviti ak nema - vjerojatno se tocka koja je okomita na bisekciju nalazi na klotoidi na mjestu prije
        % pocetne tocke, tj. izmedu P1 i P0, sto nam pokazuje da je ovo rjesenje nedopustivo.
        warning('Nema tocke okomite na bisekciju');
        out = -inf;
        Info.bInadmissibleSolution = true;
        s = 0;
        %bDraw = 1;
    end
end


%% Izlazne informacije
if(nargout>=2)
    % Korisne informacije za racunanje putanje
    %Info.FirstClothLength = s;
    %Info.SecondClothLength = s+sp0;
    Info.FirstClothLength = s-sp0;
    Info.SecondClothLength = s;

    % Za crtanje
    if(exist('Bisection', 'var'))
        Info.Bisection = Bisection;
    end
    Info.P1 = P1;
    Info.P1TangentOpp = P1TangentOpp;
end

%% Crtanje
if(bDraw)
    figure(3), clf, hold on, grid on
    axis equal
    
    % Produzi klotoidu da pocinje iz P1 prije crtanja - da se bolje vidi od kud zapravo pocinje
    CL.x0 = P1.x;
    CL.y0 = P1.y;
    CL.th0 = P1.th;
    CL.k0 = 0;
    CL.length = abs(C)*sqrt((2*pi)); % Kut poluzakreta moze biti max pi
    [xmin xmax ymin ymax] = CL_Draw(CL);
    if(exist('Bisection', 'var'))
        LP_DrawLineAsArrow(Bisection, [xmin xmax ymin ymax], 'g', 'interval');
    end    
    LP_DrawLineAsArrow(P1TangentOpp, [xmin xmax ymin ymax], 'r', 'interval');
    LP_DrawLineAsArrow(G1G2, [xmin xmax ymin ymax], 'r', 'interval');
    plot(P0.x, P0.y, 'r.');
    plot(P1.x, P1.y, 'r.');
    if(exist('xLateral', 'var'))
        plot(xLateral, yLateral, 'g.');
    end
end







%% Funkcija koju minimiziramo
function [out Info] = fMin2(C, P0, G1G2)
% Vrijednost funkcije je razlika izmedu kuta poluzakreta kojeg zelimo i kojeg dobijemo s trenutnim faktorom skaliranja.
% Trenutni kut dobije se kao kut pod kojim klotoida sijece pravac bisekcije zakreta.
% Problem s ovim pristupom je da se mora traziti sjeciste izmedu klotoide i pravca bisekcije, sto zahtijeva 
% rjesavanje nelinearne jednadzbe unutar svake iteracije.
% Dodatni problem je da sjeciste ne postoji kod velikih i malih vrijednosti C, pa se ne moze dobiti vrijednost funkcije.

eps = 1e-7;

%% 1. korak
% Izracunaj tocku P1 - to je pocetna tocka klotoide provucene kroz tocku P0 i u njoj je zakrivljenost =0

CL.x0=0;
CL.y0=0;
CL.th0=0;
CL.k0=0;
CL.C=C;
CL.c=sign(C)/(C*C);

sp0 = P0.k/CL.c; % parametar s u tocki P0
[dx dy] = GetClothoidPoint(CL, sp0); % Promjena x i y od tocke P1 do tocke P0
dth = 0.5*CL.c*sp0*sp0;

% Pomak dx i dy treba rotirati tako da pase orijentacija u tocki P0
angRot = - dth + P0.th;
[dx dy] = RotatePoint(dx, dy, angRot);

P1.x = P0.x - dx;
P1.y = P0.y - dy;
P1.th = P0.th - dth;

CL.x0 = P1.x;
CL.y0 = P1.y;
CL.th0 = P1.th;

%% 2. korak
% Izracunaj pravac polovista zakreta - to je os simetrije izmedu tangente u tocki P1 i pravca G1G2

% Tangenta kroz P1, ali suprotnog smjera od kuta P1.th - to je radi racunanja osi simetrije
P1TangentOpp = LP_LineFromDirectionAndPoint(P1.th+pi, P1.x, P1.y);
Bisection = LP_GetSymmetricalLine(P1TangentOpp, G1G2);
thBisection = Bisection.m_Direction;


%% 3. korak
% Dobiti ukupni potrebni poluzakret dthRequired - to je zakret od P1.th do kuta okomitog na pravac bisekcije
dthRequired = thBisection-pi/2 - P1.th;
if(C>0)
    % Pozitivni zakret - to znaci da i zahtjevani zakret mora biti pozitivan
    dthRequired = NormAngle_Zero_PI(dthRequired);
else
    dthRequired = NormAngle_MinusPI_Zero(dthRequired);
end

%% 4. korak
% Naci sjeciste pravca bisekcije sa klotoidom.
% Iz tog sjecista dobijemo trenutni kut poluzakreta koji ostvaruje nasa klotoida.

CL.length = sqrt((2*pi-eps)/abs(CL.c));
Bisection_LI = LP_GetLineImp(Bisection);
[sInt xInt yInt] = ClothLineIntersection(CL, Bisection_LI, 0);

if(~isempty(sInt))
    iInt = 1;
    if(length(sInt)==2)
        if(sInt(iInt)<sp0)
            iInt = 2;
        end
    end
    
    dthActual = 0.5*CL.c*sInt(iInt)*sInt(iInt);
    
    % Provjeri dal ovo rjesenje zadovoljava, mora biti s odgovarajuce strane pravca G1G2
    if(C>0)
        if(~LP_IsPointLeft(G1G2, xInt(iInt), yInt(iInt)))
            out = inf;
        else
            out = dthRequired - dthActual;
        end
    else
        if(LP_IsPointLeft(G1G2, xInt(iInt), yInt(iInt)))
            out = inf;
        else
            out = -(dthRequired - dthActual);
        end
    end
    
    CL.length = sInt(iInt); % Za crtanje
    
    if(nargout>=2)
        % Korisne informacije za racunanje putanje
        Info.FirstClothEnd_x = xInt(iInt);
        Info.FirstClothEnd_y = yInt(iInt);
        Info.FirstClothLength = sInt(iInt)-sp0;
        
        Info.SecondCloth_th0 = P1.th + dthActual;
        Info.SecondClothLength = sInt(iInt);
        Info.SecondCloth_k0 = CL.c*sInt(iInt);
        
        % Za crtanje
        Info.Bisection = Bisection;
        Info.P1 = P1;
    end
   
else
    % Nema sjecista - nema rjesenja
    
    % Za skretanje ulijevo, pocetna tocka mora biti lijevo od bisekcije
    % A za skretanja udesno, pocetna tocka mora biti desno od bisekcije
    % Ako to nije ispunjeno C je preveliki i vracamo +inf, a inace C je premali i vracamo -inf
    
    if(C>0)
        if(~LP_IsPointLeft(Bisection, P0.x, P0.y))
            out = inf;
        else
            out = -inf;
        end
    else
        if(LP_IsPointLeft(Bisection, P0.x, P0.y))
            out = inf;
        else
            out = -inf;
        end
    end
end

