function [bSuccess ConnectSegment] = CL_ConnectCircleLine1C_2(CurrentState, SmoothedPath, bDraw)
% Izgladivanje uz non-zero pocetnu zakrivljenost
% Problem se svodi na spajanje kruznice i pravca koristenjem dviju klotoida
%
% CurrentState - trenutno stanje zadano kao struktura sa [x y th k]
% SmoothedPath - izgladena putanja, mora pocinjati linijom na koju se mozemo "prizemljiti"
% bDraw - zastavica koja kaze dal treba crtati

ConnectSegment = [];

if(nargin<3)
    bDraw = false;
end

if(isempty(SmoothedPath))
    bSuccess = false;
    return;
end

BeginPathSegment = SmoothedPath{1};

if(~strcmp(BeginPathSegment.Type, 'LPB'))
    bSuccess = false;
    return;
end

[G1.x G1.y] = LP_GetPoint(BeginPathSegment, 0);
[G2.x G2.y] = LP_GetPoint(BeginPathSegment, BeginPathSegment.m_Length);

[bSuccess C Arc CL] = CL_ConnectCircleLine1C(CurrentState, G1, G2, bDraw);
if(bSuccess)
    ConnectSegment{1} = Arc;
    ConnectSegment{2} = CL;
end

return;