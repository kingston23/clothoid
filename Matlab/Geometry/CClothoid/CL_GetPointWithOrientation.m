function [st] = CL_GetPointWithOrientation(this, Orientation)
% Treba naci st - to je najmanji parametar 's' u kojem klotoida ima tangentu s tocno trazenom orijentacijom
%      (a ne suprotnom)
% Trazimo samo pozitivne parametre st.
%
% this - struktura s parametrima klotoide
% Orientation - trazena orijentacija

st = 0;

if(this.c == 0)
    error('c=0 i nema promjene orijentacije');
end

if(this.k0 == 0)
    % Specijalni slucaj kad je poc. zakrivljenost nula, mnogo laksi za rijesiti

    if(this.c>0)
        dth = NormAngle_Zero_2PI(Orientation - this.th0);
    else
        dth = NormAngle_Minus2PI_Zero(Orientation - this.th0);
    end
       
    % Clan pod korijenom uvijek bi trebal biti pozitivan, jer za c>0 zakret je >=0, a za c<0 je <=0
    st = sqrt(2/this.c * dth);
else
    % Slucaj kad poc. zakrivljenost nije nula, tu imamo rjesavanje kvadratne jed. i dosta razlicitih slucajeva
    
    if(this.k0>0 && this.c>0)
        dth = NormAngle_Zero_2PI(Orientation - this.th0);
        D = this.k0*this.k0 + 2*this.c*dth; % Diskriminanta
        st = (-this.k0 + sqrt(D)) / this.c;
    elseif(this.k0<0 && this.c<0)
        dth = NormAngle_Minus2PI_Zero(Orientation - this.th0);
        D = this.k0*this.k0 + 2*this.c*dth; % Diskriminanta
        st = (-this.k0 - sqrt(D)) / this.c;
    elseif(this.k0>0 && this.c<0)
        % Tu orijentacija prvo raste pa pada.
        % Neke orijentacije se javljaju dvaput na krivulji pa imamo za njih imamo vise rjesenja - tu biramo manje rj.
        % To znaci da neki s-ovi nikad ne mogu biti rjesenja.
        
        % Provjerimo prvo dal je rjesenje u pocetnom dijelu di orijentacija raste
        dth = NormAngle_Zero_2PI(Orientation - this.th0);
        D = this.k0*this.k0 + 2*this.c*dth; % Diskriminanta
        if(D>=0)
            % Da, rjesenje je u dijelu di orijentacija raste
            st = (-this.k0 + sqrt(D)) / this.c; % Kad bi tu stavili -sqrt(D), onda bi dobili ono drugo rjesenje tam di postoji dvoznacnost
        else
            % Ne, rjesenje je u dijelu di orijentacija pada
            dth = NormAngle_Minus2PI_Zero(Orientation - this.th0);
            D = this.k0*this.k0 + 2*this.c*dth; % Diskriminanta
            st = (-this.k0 - sqrt(D)) / this.c;
        end
    else
        % Tu orijentacija prvo pada pa raste.
        % Neke orijentacije se javljaju dvaput na krivulji pa imamo za njih imamo vise rjesenja - tu biramo manje rj.
        % To znaci da neki s-ovi nikad ne mogu biti rjesenja.
        
        % Provjerimo prvo dal je rjesenje u pocetnom dijelu di orijentacija pada
        dth = NormAngle_Minus2PI_Zero(Orientation - this.th0);
        D = this.k0*this.k0 + 2*this.c*dth; % Diskriminanta
        if(D>=0)
            % Da, rjesenje je u dijelu di orijentacija raste
            st = (-this.k0 - sqrt(D)) / this.c; % Kad bi tu stavili -sqrt(D), onda bi dobili ono drugo rjesenje tam di postoji dvoznacnost
        else
            % Ne, rjesenje je u dijelu di orijentacija pada
            dth = NormAngle_Zero_2PI(Orientation - this.th0);
            D = this.k0*this.k0 + 2*this.c*dth; % Diskriminanta
            st = (-this.k0 + sqrt(D)) / this.c;
        end
    end

    if(D<0); error('ocito postoji bug!!!'); end
    if(st<0); error('ocito postoji bug!!!'); end
end
