function [m_x m_y] = CL_ComputeLT_RungeKutta

m_Delta_s = 0.0035;
m_nPoints = 13173;

m_x = zeros(m_nPoints,1);
m_y = zeros(m_nPoints,1);

NumSubSteps = 10000;
SubStep = m_Delta_s / NumSubSteps; % korak integracije za metodu - izgleda da vrijedi: duplo manji korak -> duplo manja greska

% Npr. NumSubSteps 10, max gre�ka = 3.5e-4
% Npr. NumSubSteps 1000, max gre�ka = 3.5e-6
% Npr. NumSubSteps 10000, max gre�ka = 3.5e-7

% Poc. vrijednosti za integrator
x=0; y=0;
c = 1;

% 's' je parametar koji znaci prevaljeni put
iPoint = 0;
for (iSubStep=0:(m_nPoints-1)*NumSubSteps)

	% Spremaj izlazne vrijednosti za zeljene parametre s
	if(mod(iSubStep, NumSubSteps) == 0)
		m_x(iPoint+1) = x;
		m_y(iPoint+1) = y;
		iPoint = iPoint + 1;
    end
    
    s = iSubStep * SubStep;

	% Integracija za x
	k1 = cos(c/2 * s*s);
	k2 = cos(c/2 * (s+SubStep/2*k1)^2);
	k3 = cos(c/2 * (s+SubStep/2*k2)^2);
	k4 = cos(c/2 * (s+SubStep*k3)^2);

	x = x + SubStep/6*(k1+2*k2+2*k3+k4);

	% Integracija za y
	k1 = sin(c/2 * s*s);
	k2 = sin(c/2 * (s+SubStep/2*k1)^2);
	k3 = sin(c/2 * (s+SubStep/2*k2)^2);
	k4 = sin(c/2 * (s+SubStep*k3)^2);

	y = y + SubStep/6*(k1+2*k2+2*k3+k4);
	
end