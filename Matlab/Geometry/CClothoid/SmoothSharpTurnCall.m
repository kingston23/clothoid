% Skripta za zvanje SmoothSharpTurn
global LT

S.x = -1;
S.y = 0;
M.x = 0;
M.y = 0;
G.x = 0;
G.y = 1;
dMax = 1;
eMax = 0.5;
sMax = LT.s_max;

eDesired = 0.1;

%InnerObs(1).x = -0.3;
%InnerObs(1).y = 0.3;
%InnerObs(2).x = -0.5;
%InnerObs(2).y = 0.5;
%InnerObs(3).x = -0.5;
%InnerObs(3).y = -0.5;
%InnerObs(4).x = 0;
%InnerObs(4).y = 0.5;

% Tocke unesene preko:
% Points1 = ginput;
for(i=1:length(Points1))
    InnerObs(i).x = Points1(i,1);
    InnerObs(i).y = Points1(i,2);
end

%OuterObs(1).x1 = -0.1;
%OuterObs(1).y1 = 0.0;
%OuterObs(1).x2 = 0.;
%OuterObs(1).y2 = 0.2;

% Tocke unesene preko:
% Points2 = ginput;
OuterObs = [];
for(i=1:length(Points2)-1)
    OuterObs(i).x1 = Points2(i,1);
    OuterObs(i).y1 = Points2(i,2);
    OuterObs(i).x2 = Points2(i+1,1);
    OuterObs(i).y2 = Points2(i+1,2);
end

[CL1 CL2] = SmoothSharpTurn(S, M, G, dMax, eMax, sMax, eDesired, InnerObs, OuterObs);

