%P0.x = 0; P0.y = 0; P0.th = 0.5; P0.k = 1;

% 3.17209 
%G1.x = -1.2;
%G1.y = 0.1;

%G2.x = -0.1;
%G2.y = 0;

% Ovo daje funckiju kojoj trazimo nul tocku koja je prikazana u disertaciji
%P0.x = 0; P0.y = 0; P0.th = 0; P0.k = 0.1;
%G1.x=6.5; G1.y=-0.6;
%G2.x=7.6; G2.y=-0.7;

% Ovo daje jedan od crteza u disertaciji
P0.x = 0; P0.y = 0; P0.th = 0.5; P0.k = 0.01;
G1.x=0; G1.y=2.5;
G2.x=-3; G2.y=2.5;

%P0.x = 0; P0.y = 0; P0.th = 0; P0.k = -1;
%G1.x=0; G1.y=0;
%G2.x=0; G2.y=3;

[bOk C] = CL_ConnectCircleLine2C(P0, G1, G2, true);
