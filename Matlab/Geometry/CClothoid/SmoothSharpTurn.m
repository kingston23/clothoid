function [bOK CL1 CL2 StartLine EndLine] = SmoothSharpTurn(S, M, G, dMax, eMax, sMax, dObsDesired, InnerObs, OuterObs)
% Funkcija za smoothanje ostrih zakreta parom klotoida.
% Maksimalni zakret koji se moze dobiti ovom funkcijom je <pi.
%
% S - pocetna tocka zakreta (na negativnoj x-osi)
% M - srednja tocka zakreta (ona je u 0,0 pa nije ni potrebna)
% G - ciljna tocka zakreta (mora biti G.y>0)
%
% Podrucje zakreta je zatvoreno vektorima SM i MG
%
% dObsDesired - zeljeni razmak putanje od prepreka. Ovo je soft uvjet, i ako nije moguce, nece se postovati,
%      jer bolje je naci ikakvu putanju nego nikakvu, pa makar ona isla i blizu prepreka (vozi polahke).
%      Zasad algoritam nema mogucnost odredivanja tocne udaljenosti od prepreka, nek to radi ajmo reci priblizno.
%
% TUDU: naprednije stvari kao sto su javljanje udaljenosti putanje od prepreke za svaku prepreku,
% zakreti >= pi itd...
% Isto tak, trenutno racunanje udaljenosti izmedu klotoide i prepreke se vrsi samo duz pravca koji prolazi kroz tocku M,
% tak da se ne moze govoriti o pravoj udaljenosti. To ne radi lose, al nije bas tocno.

global LT; % Lookup tablica

eps = 1e-7;

bOK = true;
StartLine = [];
EndLine = [];

%% Svodenje problema na standardni (pocetna tocka na neg x osi, srednja u ishodistu, zavrsna u y>0

% Zapamti stare tocke
OS = S;
OM = M;
OG = G;

% Translatiram tocke tako da srednja dode u ishodiste
xTranslation = -M.x; yTranslation = -M.y;

S.x = S.x + xTranslation;
S.y = S.y + yTranslation;

M.x = 0;
M.y = 0;

G.x = G.x + xTranslation;
G.y = G.y + yTranslation;

% Rotiram pocetnu i krajnju tocku, tako da pocetna dode na lijevu x-os. Srednja ostaje u ishodistu.

RotAngle = pi - atan2(S.y, S.x);
[S.x S.y] = RotatePoint(S.x, S.y, RotAngle);
[G.x G.y] = RotatePoint(G.x, G.y, RotAngle);

% Proracune radimo za slucaj skretanja klotoide u poz. smjeru. Ako klotoida ipak mora skretati u negativnom smjeru, to pamtimo u ovoj zastavici i
% kasnije radimo inverziju.
bNegativeTurn = false;
if(G.y<0)
    bNegativeTurn = true;
    G.y = -G.y;
end

% Provjeravam udaljenost dMax (ne smije biti veca od duzina definiranih tockama zavoja)
dStart = abs(S.x);
dEnd = sqrt(G.x*G.x + G.y*G.y);

dMax = min(dMax, dStart);
dMax = min(dMax, dEnd);

% Racunam kut tangente klotoide na polovici zavoja. To je polovica ukupne promjene kuta za cijeli zavoj.
ang_g = atan2(G.y, G.x); % ukupni zaokret
angm = ang_g/2; % kut na polovici zavoja

% Simetrala zakreta
Bisection.A = -sin(angm+pi/2);
Bisection.B = cos(angm+pi/2);
Bisection.C = 0;

% U nastavku je BC = basic clothoid (osnovni klotoid sa c=1 spremljen u lookup tablici)
sBC = sqrt(2*angm); % prevaljeni put osnovne klotoide u lookup tablici (sa c=1) u tocki gdje je ona okomita na simetralu
[xBCtmp, yBCtmp] = GetBasicClothoidCoordsLT(sBC); % izlaz iz lookup tablice - treba nam samo kao medurezultat

% Tocka sjecista osnovne klotoide i simetrale
xBCm = -Bisection.B * yBCtmp / Bisection.A;
yBCm = yBCtmp;

% Tocka na x osi na kojoj pocinje osnovna klotoida
xBCs = xBCm - xBCtmp;
yBCs=0;

dBC = -xBCs; % udaljenost pocetka osnovne klotoide do sredista zavoja (tj. ishodista)

% udaljenost sredista zavoja (tj. ishodista) do najblize tocke osnovne klotoide (tj. koliko se klotoida udaljava od linijskog profila staze zadanog tockama)
eBC = sqrt(xBCm*xBCm + yBCm*yBCm);

% Scaling faktori klotoide koji su potrebni da se tocno postigne dMax, eMax, sMax
C_d = dMax/dBC;
C_e = eMax/eBC;
C_s = sMax/sBC;

% Slozi osnovnu klotoidu tako da zavrsava u simetrali. Ovo bu nam trebalo za udaljenosti od prepreka
BC.x0 = xBCs;
BC.y0 = yBCs;
BC.th0 = 0;
BC.k0 = 0;
BC.c = 1;
BC.length = sBC;

% Pravac koji ide od sredista zakreta prema ciljnoj tocki
GoalEdge.A = -sin(ang_g);
GoalEdge.B = cos(ang_g);
GoalEdge.C = 0;


%% Unutarnje prepreke

C_inner = []; % Skaling faktori koji su potrebni da bi putanja tocno dodirivala pojedinu prepreku
C_inner_desired = []; % Skaling faktori koji su potrebni da bi putanja bila na udaljenosti dObsDesired od prepreke

nInnerObs = 0;
if(exist('InnerObs', 'var'))
    nInnerObs = length(InnerObs);
end
for(iObs=1:nInnerObs)
    
    xo = InnerObs(iObs).x;
    yo = InnerObs(iObs).y;
    
    % Preskoci prepreke koje nisu unutar zakreta
    if(yo<0)
        continue;
    end
    
    if(GoalEdge.A*xo + GoalEdge.B*yo < 0)
        continue;
    end
    
    % Dodatno, prepreke koje jesu unutar zavoja, ali su u gornjoj polovici zavoja, zrcalimo u donju polovicu
    if(Bisection.A*xo + Bisection.B*yo < 0)
        
        AA = Bisection.A*Bisection.A;
        BB = Bisection.B*Bisection.B;
        AB = Bisection.A*Bisection.B;
        AABB = AA+BB;
        
        xo_ = ((BB-AA)*xo - 2*AB*yo) / AABB;
		yo_ = (-2*AB*xo + (AA-BB)*yo) / AABB;
        
        xo = xo_; yo = yo_;
        
    end
    
    % Nadi sjeciste osnovne klotoide i pravca koji ide od sredista zakreta i prolazi kroz prepreku
    LN.A = -yo;
    LN.B = xo;
    LN.C = 0;
    
    % Izracunaj scaling faktor klotoide koji je potreban da se tocno postigne dodirivanje prepreke
    [sInt xInt yInt] = ClothLineIntersection(BC, LN);
    
    if(~isempty(sInt))
        % Trebalo bi biti samo jedno sjeciste
        
        d_BCToMidPoint_2 = xInt(1)*xInt(1) + yInt(1)*yInt(1); % Kvadrat udaljenosti sjecista od sredisnje tocke zakreta
        
        d_ObsToMidPoint_2 = xo*xo+yo*yo; % Kvadrat udaljenosti prepreke od sredisnje tocke zakreta
                
        % Skaling faktor racunamo tak da klotoida tocno dodiruje prepreku
        C = sqrt(d_ObsToMidPoint_2/d_BCToMidPoint_2);
        C_inner = [C_inner C];
        
        % Zeljena udaljenost putanje od tocke zakreta - tak da se postuje zeljena udaljenost od prepreka
        d_PathToMidPointDesired_2 = d_ObsToMidPoint_2 - dObsDesired;
        if(d_PathToMidPointDesired_2<0)
            d_PathToMidPointDesired_2 = 0; % Ovo ocito ne mozemo ispuniti, staza bi isla izvan podrucja zakreta
        end
        C_desired = sqrt(d_PathToMidPointDesired_2/d_BCToMidPoint_2);
        
        C_inner_desired = [C_inner_desired C_desired];
    end
    
end

%% Vanjske prepreke
% One su znacajno kompliciranije i mogu biti zadane, osim kao tocke, i kao omedeni pravci (tj. sa dvije tocke).
% To je zato jer je izvana gledano klotoida konveksna, pa tocka dodira moze biti i na bridu poligonalne prepreke,
% a ne samo na vrhu.
% Tu gledamo i prepreke koje nisu unutar pravaca koji definiraju zakret.
% To je zato da mozemo klotoidu provuci tako da je na sredini izmedu nutarnjih i vanjskih prepreka.

C_outer = [];
C_outer_desired = []; % Skaling faktori koji su potrebni da bi putanja bila na udaljenosti dObsDesired od prepreke

nOuterObs = 0;
if(exist('OuterObs', 'var'))
    nOuterObs = length(OuterObs);
end
for(iObs=1:nOuterObs)
    
    xo1 = OuterObs(iObs).x1;
    yo1 = OuterObs(iObs).y1;
    
    xo2 = OuterObs(iObs).x2;
    yo2 = OuterObs(iObs).y2;
    
    % Ovo je pravac koji ide kroz brid prepreke 
    ObstacleEdge.A = yo1 - yo2;
    ObstacleEdge.B = xo2 - xo1;
    ObstacleEdge.C = xo1*yo2 - xo2*yo1;
    
    % Ispitujemo dal postoji presjek podrucja zakreta i ruba prepreke.
    % Ak postoji, to je to, algoritm sam bira dalje kriticnu tocku na prepreki.
    % Ak ne postoji, preskoci ovu prepreku.
    
    % Provjeri dal su vrhovi prepreke unutar podrucja zakreta omedenog sa duzinama S-M i M-G
    bFirstIsOut = yo1<0 || GoalEdge.A*xo1+GoalEdge.B*yo1<0;
    bSecondIsOut = yo2<0 || GoalEdge.A*xo2+GoalEdge.B*yo2<0;
    
    if(bFirstIsOut && bSecondIsOut)
        % Obadvije tocke su vani, ali mozda ipak postoji presjek.
        % Presjek postoji samo ak postoji sjeciste obje duzine S-M i M-G sa pravcem kroz brid prepreke
        
        bFirstIntersection = false;
        if(abs(ObstacleEdge.A)>eps)
            % Sijece pravac kroz S-M (start-sredina)
            xc1 = - ObstacleEdge.C / ObstacleEdge.A;
            yc1 = 0;
            if(xc1<=0) % To sjeciste mora biti u x<=0
                bFirstIntersection = true;
            end
        end
        
        bSecondIntersection = false;
        Temp = -(G.y*ObstacleEdge.B + ObstacleEdge.A*G.x);
        
        if(abs(Temp)>eps) % tj. if(Temp!=0)
            
            % Sijece pravac kroz M-G (sredina-cilj)
            xc2 = G.x*ObstacleEdge.C / Temp;
            yc2 = G.y*ObstacleEdge.C / Temp;
            if(yc2>=0) % To sjeciste mora biti u y>=0
                bSecondIntersection = true;
            end
        end
        
        if(~(bFirstIntersection && bSecondIntersection))
            continue; % Nema sjecista
        end

    end
    
    % Prepreke koje su u gornjoj polovici zavoja zrcalimo u donju polovicu
    
    bIsUp1 = Bisection.A*xo1 + Bisection.B*yo1 < 0; % Dal je prva tocka u gornjoj polovici
    bIsUp2 = Bisection.A*xo2 + Bisection.B*yo2 < 0; % Dal je druga tocka u gornjoj polovici
    
    if(bIsUp1 && bIsUp2) % Obje tocke su u gornjoj polovici, pa ih obje zrcalimo
    
        AA = Bisection.A*Bisection.A;
        BB = Bisection.B*Bisection.B;
        AB = Bisection.A*Bisection.B;
        AABB = AA+BB;
        
        xo_ = ((BB-AA)*xo1 - 2*AB*yo1) / AABB;
		yo_ = (-2*AB*xo1 + (AA-BB)*yo1) / AABB;
        
        xo1 = xo_; yo1 = yo_;
        
        xo_ = ((BB-AA)*xo2 - 2*AB*yo2) / AABB;
		yo_ = (-2*AB*xo2 + (AA-BB)*yo2) / AABB;
        
        xo2 = xo_; yo2 = yo_;
        
    elseif(bIsUp1 || bIsUp2) % Samo jedna tocka je u gornjoj polovici, a druga u donjoj.
        
        % Trazimo sjeciste brida prepreke i simetrale.
        
        Temp = Bisection.A*ObstacleEdge.B - ObstacleEdge.A*Bisection.B; % Ovo bi trebalo biti !=0 jer su tocke s razlicitih strana
        xc = (Bisection.B*ObstacleEdge.C - ObstacleEdge.B*Bisection.C) / Temp;
		yc = (ObstacleEdge.A*Bisection.C - Bisection.A*ObstacleEdge.C) / Temp;
        
        % Sad trazimo koji vrh prepreke je dalje od sredisnje tocke zakreta (M).
        % Tog zadrzavamo, a drugog odbacujemo jer ne utjece na nista.
        if( (xo1*xo1+yo1*yo1) > (xo2*xo2+yo2*yo2) )
            % Prvi je dalji - njega zadrzavamo. Drugog zamjenjujemo sa tockom sjecista.
            xo2 = xc;
            yo2 = yc;
            
            if(bIsUp1) % Dal ovog koji ostaje treba zrcaliti
                AA = Bisection.A*Bisection.A;
                BB = Bisection.B*Bisection.B;
                AB = Bisection.A*Bisection.B;
                AABB = AA+BB;

                xo_ = ((BB-AA)*xo1 - 2*AB*yo1) / AABB;
                yo_ = (-2*AB*xo1 + (AA-BB)*yo1) / AABB;

                xo1 = xo_; yo1 = yo_;
            end
        else
            xo1 = xc;
            yo1 = yc;
            
            if(bIsUp2) % Dal ovog koji ostaje treba zrcaliti
                AA = Bisection.A*Bisection.A;
                BB = Bisection.B*Bisection.B;
                AB = Bisection.A*Bisection.B;
                AABB = AA+BB;

                xo_ = ((BB-AA)*xo2 - 2*AB*yo2) / AABB;
                yo_ = (-2*AB*xo2 + (AA-BB)*yo2) / AABB;

                xo2 = xo_; yo2 = yo_;
            end
        end
        
    end
    
    % Zrcaljenje je gotovo, al ima jos dosta posla.
    % Sad treba naci dal postoji tocka na klotoidi koja ima istu orijentaciju kao i brid prepreke.
    % Ako takva tocka ne postoji, dalje ispitujemo samo vrh prepreke koji je blizi simetrali zakreta.
    % Ako pak takva tocka postoji, provucemo pravac kroz nju i srediste zakreta (tocka M).
    % Onda ispitujemo dal taj pravac sijece brid prepreke.
    % Ako da, to sjeciste predstavlja kriticnu tocku koja je najbliza klotoidi i nju dalje koristimo.
    % Ako ne, kriticna tocka je onaj vrh prepreke koji je blizi nadenom sjecistu.
    
    % Izracunaj pravac kroz brid prepreke - treba ponovo jer su se tocke mozda promijenile
    ObstacleEdge.A = yo1 - yo2;
    ObstacleEdge.B = xo2 - xo1;
    ObstacleEdge.C = xo1*yo2 - xo2*yo1;
    
    st = 0;
    stFound = false;
    % Koeficijenti kvadratne jed
    a = BC.c/2; b = BC.k0; c = BC.th0 - atan2(-ObstacleEdge.A, ObstacleEdge.B);
    diskr = b^2-4*a*c;

    % Postoje 4 mogucnosti - pokusavamo dok ne nademo ispravnu vrijednost
    if(diskr>=0)
        st = (-b + sqrt(diskr)) / (2*a);

        if(st>0 && st<BC.length)
            stFound = true;
        end

        if(~stFound)
            st = (-b - sqrt(diskr)) / (2*a);

            if(st>0 && st<BC.length)
                stFound = true;
            end
        end
    end

    if(~stFound)
        c = BC.th0 - atan2(-ObstacleEdge.A, ObstacleEdge.B) - pi;
        diskr = b^2-4*a*c;

        if(diskr>=0)
            st = (-b + sqrt(diskr)) / (2*a);

            if(st>0 && st<BC.length)
                stFound = true;
            end

            if(~stFound)
                st = (-b - sqrt(diskr)) / (2*a);

                if(st>0 && st<BC.length)
                    stFound = true;
                end
            end
        end
    end
    
    bCriticalPointFound = false; % Dal je pronadena kriticna tocka na prepreki, ona koja bude se najprije dodirnula s klotoidom
    if(stFound)
        [xc yc] = GetClothoidPoint(BC, st); % Tocka osnovne klotoide koja ima istu orijent. kao i rub prepreke
        
        % Provuci pravac kroz gornju tocku i srediste zakreta M(0,0)
        LN.A = -yc;
        LN.B = xc;
        LN.C = 0;
        
        % Ispitujemo dal taj pravac sijece rub prepreke.
        Temp = LN.A*ObstacleEdge.B - ObstacleEdge.A*LN.B;
        
        if(abs(Temp)>eps) % tj. if(Temp!=0)
            
            % Sijece
            x = (LN.B*ObstacleEdge.C - ObstacleEdge.B*LN.C) / Temp;
            y = (ObstacleEdge.A*LN.C - LN.A*ObstacleEdge.C) / Temp;
            
            % Dal je ta tocka stvarno na rubu prepreke?
            if( ((xo1<=x&&x<=xo2) || (xo2<=x&&x<xo1)) && ...
                ((yo1<=y&&y<=yo2) || (yo2<=y&&y<yo1)) )
            
                % Je - uzimamo ju kao kriticnu
                xo = x;
                yo = y;
            else
                % Nije - jedan od vrhova prepreke je kritican, i to onaj koji je blizi (x,y)
                dx1 = xo1-x; dy1 = yo1-y;
                dx2 = xo2-x; dy2 = yo2-y;
                
                if(dx1*dx1+dy1*dy1 < dx2*dx2+dy2*dy2)
                    % Prvi je vrh kritican
                    xo = xo1;
                    yo = yo1;
                else
                    % Drugi je vrh kritican
                    xo = xo2;
                    yo = yo2;
                end
            end
            
            bCriticalPointFound = true; % Nasli smo te
            
        else
            warning('i to je moguce?');
        end
    end

    if(~bCriticalPointFound)
        % Nema takve tocke - ispitujemo samo vrh prepreke koji je blizi simetrali zakreta
        % jer ce on uvijek biti blize klotoidi
        if(Bisection.A*xo1+Bisection.B*yo1 < Bisection.A*xo2+Bisection.B*yo2)
            xo = xo1;
            yo = yo1;
        else
            xo = xo2;
            yo = yo2;
        end
    end
    
    % Dalje je slicno kao i za unutarnje prepreke:
    
    % Nadi sjeciste osnovne klotoide i pravca koji ide od sredista zakreta i prolazi kroz prepreku (tj. kriticnu tocku)
    
    if(xo~=0 || yo~=0)
        LN.A = -yo;
        LN.B = xo;
        LN.C = 0;
    
        % Izracunaj scaling faktor klotoide koji je potreban da se tocno postigne dodirivanje prepreke
        [sInt xInt yInt] = ClothLineIntersection(BC, LN);
    else
        sInt = []; % Nema sjecista naravno
    end
    
    if(~isempty(sInt))
        % Trebalo bi biti samo jedno sjeciste
        
        d_BCToMidPoint_2 = xInt(1)*xInt(1) + yInt(1)*yInt(1); % Kvadrat udaljenosti sjecista od sredisnje tocke zakreta
        
        d_ObsToMidPoint_2 = xo*xo+yo*yo; % Kvadrat udaljenosti prepreke od sredisnje tocke zakreta
        
        % Skaling faktor racunamo tak da klotoida tocno dodiruje tocku prepreke
        C = sqrt(d_ObsToMidPoint_2/d_BCToMidPoint_2);
        C_outer = [C_outer C];
        
        % Zeljena udaljenost putanje od tocke zakreta - tak da se postuje zeljena udaljenost od prepreka
        d_PathToMidPointDesired_2 = d_ObsToMidPoint_2 + dObsDesired; % Ovo je uvijek >0
        C_desired = sqrt(d_PathToMidPointDesired_2/d_BCToMidPoint_2);
        
        C_outer_desired = [C_outer_desired C_desired];
    end
    
end

%% Odredi skaling faktor na temelju svih faktora i pronadi koji uvjet je odlucil

% Prvo odredujemo gornju granicu skaling faktora (trazenjem minimuma)

% Ovo su hard uvjeti
if(C_d<C_e)
    C_upper = C_d;
    status = 'd decided';
else
    C_upper = C_e;
    status = 'e decided';
end
if(C_s < C_upper)
    C_upper = C_s;
    status = 's decided';
end
disp(status);

C_UpperObs = inf; % Gornji skaling faktor zbog unutarnjih prepreka (hard)
C_UpperObsDesired = inf; % Zeljeni gornji skaling faktor zbog unutarnjih prepreka (soft)

bConsiderInnerObstacles = false;

for(i=1:length(C_inner))
    bConsiderInnerObstacles = true;
    if(C_inner(i) < C_UpperObs)
        C_UpperObs = C_inner(i);
    end
    if(C_inner_desired(i) < C_UpperObsDesired)
        C_UpperObsDesired = C_inner_desired(i);
    end
end


% A sada trazimo donju granicu skaling faktora (trazenjem maksimuma za vanjske prepreke)
C_LowerObs = 0; % Gornji skaling faktor zbog unutarnjih prepreka (hard)
C_LowerObsDesired = 0; % Zeljeni gornji skaling faktor zbog unutarnjih prepreka (soft)

bConsiderOuterObstacles = false;

for(i=1:length(C_outer))
    bConsiderOuterObstacles = true;
    if(C_outer(i) > C_LowerObs)
        C_LowerObs = C_outer(i);
    end
    if(C_outer_desired(i) > C_LowerObsDesired)
        C_LowerObsDesired = C_outer_desired(i);
    end
end

if(min(C_upper, C_UpperObs) < C_LowerObs)
    bOK = false; warning('Nema rjesenja koje zadovoljava uvjete, i nutarnje i vanjske prepreke'); return;
end

if(C_UpperObsDesired < C_LowerObsDesired)
    warning('Nema rjesenja koje zadovoljava zeljeni razmak od nutarnjih i vanjskih prepreka');
end

% Konacni C mozemo odrediti na razne nacine.
if(bConsiderInnerObstacles && bConsiderOuterObstacles)
    if(min(C_upper, C_UpperObsDesired) >= C_LowerObsDesired)
        disp('Imamo dovoljan razmak i do nutarnjih i do vanjskih, idemo blize nutarnjima');
        C = min(C_upper, C_UpperObsDesired); % Tak da smo blize nutarnjim preprekama (time se dobije manja zakrivljenost)
    else
        disp('Nemamo dovoljan razmak i do nutarnjih i do vanjskih, idemo u sredinu');
        C = 0.5*(C_UpperObs + C_LowerObs); % kompromis izmedu nutarnjih i vanjskih prepreka.
        C = min(C, C_upper);
    end        
    
elseif(bConsiderInnerObstacles)
    C = min(C_upper, C_UpperObs); % hard uvjet
    C2 = min(C, C_UpperObsDesired);
    if(C2<1) % Preostra klotoida?
        C = C2+(C-C2)*0.5; % Neki kompromis izmedu ostrine klotide i postovanja razmaka od prepreka
    else
        C = C2;
    end
elseif(bConsiderOuterObstacles)
    % C mora biti izmedu C_upper i C_LowerObs, ili ako je moguce C_upper i C_LowerObsDesired
    % Zelimo cim blazi zakret, pa biramo C_upper
    C = C_upper;
else
    % Ne gledaju se prepreke, nek samo uvjeti e_max, s_max itd.
    % Zelimo cim blazi zakret, pa biramo C_upper
    C = C_upper;
end
%C = C_lower;

if(C==0)
    warning('Dobili smo zakret u tocki'); % To u principu i nije greska, al je slucaj koji se mora dodatno obraditi
    c = 1/(LT.C_min*LT.C_min);
else
    c = 1/(C*C); % Sharpness
end

d = dBC * C; % Na kojoj duljini od sredisnje tocke treba poceti klotoida
e = eBC * C; % Koliko klotoida najvise odstupa od sredisnje tocke zakreta
s = sBC * C; % Duljina klotoide

%% Proracunaj parametre pocetnog pravca

bStartLineRequired = false;

if(d < dStart-eps)
    % Pocetni pravac je potreban
    bStartLineRequired = true;
    
    StartLine = LP_LineFromDirectionAndPoint(-RotAngle, OS.x, OS.y);
    StartLine.m_Length = dStart-d;
    StartLine.Type = 'LPB';
else
    % Pocetni pravac je nepotreban
    bStartLineRequired = false;
end


%% Proracunaj parametre klotoida
CL1.x0 = -d;
CL1.y0 = 0;
[CL1.x0, CL1.y0] = RotatePoint(CL1.x0, CL1.y0, -RotAngle);
CL1.x0 = CL1.x0 - xTranslation;
CL1.y0 = CL1.y0 - yTranslation;
CL1.th0 = 0-RotAngle;
CL1.k0 = 0;
CL1.C = C;
CL1.c = c;
if(bNegativeTurn)
    CL1.C = -CL1.C;
    CL1.c = -CL1.c;
end
CL1.length = s;
CL1.Type = 'CL';

CL2.x0 = xBCm*C;
CL2.y0 = yBCm*C;
CL2.th0 = angm;
if(bNegativeTurn)
    CL2.y0 = -CL2.y0;
    CL2.th0 = -CL2.th0;
end
[CL2.x0, CL2.y0] = RotatePoint(CL2.x0, CL2.y0, -RotAngle);
CL2.x0 = CL2.x0 - xTranslation;
CL2.y0 = CL2.y0 - yTranslation;
CL2.th0 = CL2.th0 - RotAngle;
CL2.k0 = CL1.c*s;
CL2.C = -CL1.C;
CL2.c = -CL1.c;
CL2.length = s;
CL2.Type = 'CL';

%% Proracunaj parametre zavrsnog pravca

bEndLineRequired = false;

if(d < dEnd-eps)
    % Potreban je
    bEndLineRequired = true;
    
    % Rotacija i translacija u pocetni koord. sustav
    if(bNegativeTurn)
        ang_g = -ang_g;
    end
    
    EndLine = LP_LineFromDirectionAndPoint(ang_g-RotAngle, OG.x, OG.y);
    EndLine.m_Length = dEnd-d;
    EndLine.Type = 'LPB';
    
    % Ponovo proracunaj pocetnu tocku
    [EndLine.p0.x EndLine.p0.y] = LP_GetPoint(EndLine, -(dEnd-d));
else
    % Pocetni pravac je nepotreban
    bEndLineRequired = false;
end

%% Crtanje
if(0)
    figure(1), hold on, grid on
    axis equal
    %set(gca, 'color', 'k')
    %set(gca, 'xcolor', 'w')
    %set(gca, 'ycolor', 'w')
    
    % Konture okreta
    plot([OS.x OM.x], [OS.y OM.y], 'k:');
    plot([OM.x OG.x], [OM.y OG.y], 'k:');

    CL_Draw(CL1, 'r');
    CL_Draw(CL2, 'r');
    plot(CL1.x0, CL1.y0, 'r.');
    plot(CL2.x0, CL2.y0, 'r.');
    
    if(bStartLineRequired)
        LPB_DrawLine(StartLine, 'r');
        plot(StartLine.p0.x, StartLine.p0.y, 'r.');
    end
    if(bEndLineRequired)
        LPB_DrawLine(EndLine, 'r');
        plot(EndLine.p0.x, EndLine.p0.y, 'r.');
    end

    if(exist('InnerObs', 'var'))
        x = zeros(length(InnerObs),1);
        y = zeros(length(InnerObs),1);
        for(iObs=1:length(InnerObs))
            x(iObs) = InnerObs(iObs).x;
            y(iObs) = InnerObs(iObs).y;
        end
        plot(x, y, 'r');
    end

    if(exist('OuterObs', 'var'))
        for(iObs=1:length(OuterObs))
            plot([OuterObs(iObs).x1 OuterObs(iObs).x2], [OuterObs(iObs).y1 OuterObs(iObs).y2], 'y');
        end
    end
end
