function [x, y bResultReliable] = GetBasicClothoidCoordsLT(s)
% Funkcija vraca koordinate klotoide na temelju trazenja lookup tablice i interpolacije
% LT.x_L, LT.y_L - koordinate u lookup tablici
% LT.c_L - sharpness klotoide u tablici
% LT.Delta_sL - delta s, tj. razmak izmedu tocaka u tablici (>0)
% LT.sL - duljina klotoide u tablici (mora biti nPoints =sLmax/dsL+1)
% s - udaljenost na kojoj hocemo dobiti koordinate,
%     moze biti veca od sL i onda se dela ekstrapolacija, a inace se dela interpolacija

bResultReliable = true;

global LT;

i1 = floor( min(abs(s),LT.sL) / LT.Delta_sL ) + 1; % +1 jer ovo nije C
nPoints = LT.sL/LT.Delta_sL+1;

if(i1+1 > length(LT.x_L))
    warning('Lookup tablica prekratka - ekstrapoliramo');
end

if(s==0)
    x = 0;
    y = 0;
elseif(abs(s)==LT.sL)
    x = LT.x_L(i1);
    y = LT.y_L(i1);    
elseif(abs(s) > LT.sL)
    error('Ekstrapolacija'); % Da upozorimo usera da nekaj ne valja izvana
    bResultReliable = false;
    % Ekstrapolacija
        
    % Radijus u zadnjoj tocki je
    r = 1/(LT.c_L*LT.sL);
    %r = 1/abs(s);
    r = 2/(LT.c_L*(LT.sL+abs(s)));
    % Kut u zeljenoj tocki je 
    % theta = s*s/2; - ne idemo prek kuta nek prek udaljenosti tocke
    
    % Dvije zadnje tocke u tablici koje buju posluzile za ekstrapolaciju
    x1 = LT.x_L(i1-1);
    y1 = LT.y_L(i1-1);
    x2 = LT.x_L(i1);
    y2 = LT.y_L(i1);
    
    % Koeficijenti usmjerenog pravca koji ide od (x1,y1) do (x2,y2) (koef C nam ne treba)
    A = y1 - y2;
    B = x2 - x1;
    
    % Srediste kruznice koja prolazi kroz (x1,y1) do (x2,y2) i ima radijus r (srediste je s gornje lijeve strane usmjerenog pravca)
    xc = (x1+x2+sqrt(4*r*r/(A*A+B*B)-1)*A)/2;
    yc = (y1+y2+sqrt(4*r*r/(A*A+B*B)-1)*B)/2;
    
    theta2 = atan2((y2-yc),(x2-xc)); % Kut vektora od sredista kruznice do 2. tocke
    
    % Ovo je tocka koja je na udaljenosti (s-LT.sL) od druge tocke, iduci po kruznici radijusa r
    x = xc+r*cos(theta2+(s-LT.sL)/r); % (s-LT.sL)/r je prevaljeni kut od druge tocke do ekstrapolirane tocke.
    y = yc+r*sin(theta2+(s-LT.sL)/r);
    
else
    % Interpolacija

    if(0)
        % Interpolacija linijom - samo za usporedbu u clanku
        i2 = i1 + 1;
        p = (abs(s) - (i1-1)*LT.Delta_sL) / LT.Delta_sL;
        x = LT.x_L(i1) + (LT.x_L(i2)-LT.x_L(i1)) * p;
        y = LT.y_L(i1) + (LT.y_L(i2)-LT.y_L(i1)) * p; 
    else
        % Interpolacija kruznim lukom - bolje
        
        % Postupak #2
        s1 = (i1-1)*LT.Delta_sL;
        s_middle = s1+0.5*LT.Delta_sL;
        k_middle = LT.c_L*s_middle;

        ri = 1/k_middle; % radijus u sredini izmedu dvije tocke u lookup tablici
        theta1 = 1/2*LT.c_L*s1^2;
        x1 = LT.x_L(i1);
        y1 = LT.y_L(i1);

        ds = abs(s) - (i1-1)*LT.Delta_sL;

        S = sin(ds/ri/2);
        x = x1+2*ri*cos(theta1+ds/ri/2)*S;
        y = y1+2*ri*sin(theta1+ds/ri/2)*S;
    end
end

if(s<0)
    % Vrati simetricnu tocku
    x = -x;
    y = -y;
end
