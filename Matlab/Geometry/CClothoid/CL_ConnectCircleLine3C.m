function [bOk C ConnectPath] = CL_ConnectCircleLine3C(P0, G1, G2, bDraw)
% Izgladivanje uz non-zero pocetnu zakrivljenost
% Problem se svodi na spajanje kruznice i pravca koristenjem dviju klotoida
%
% P0 - pocetno stanje koje se sastoji od P0.x, P0.y, P0.th, P0.k
% G1, G2 - linija na koju moramo "sletjeti" koristenjem klotoida
%
% TUDU: Odabir scalinga prve klotoide, odabir duljine linije koja slijedi?

if(nargin<4)
    bDraw = false;
end

ConnectPath = [];
bOk = true;

C = 0;

if(bDraw && 0)
    figure(1), clf, hold on, grid on
    axis equal
    
    r1 = rectangle('Position', [0.7 0, 0.1 0.5]);
    r2 = rectangle('Position', [0.1 0.1 0.25 0.2]);
    set(r1, 'FaceColor', [0.8 0.8 0.8]);
    set(r2, 'FaceColor', [0.8 0.8 0.8]);
    plot([0 0.1 0.3], [0 0.3 0.5], 'k:');
    
    Arc = CA_SetArcFromTangent(P0.x, P0.y, P0.th, 1/P0.k, -pi/5);
    CA_DrawArc(Arc, 'k:');
    Arc = CA_SetArcFromTangent(P0.x, P0.y, P0.th, 1/P0.k, pi/10);
    CA_DrawArc(Arc, 'k:');
    ar = arrow([G1.x G1.y], [G2.x G2.y]);
    set(ar, 'LineStyle', ':');
end


%% Ispitivanje argumenata
if(P0.k == 0)
    bOk = false; warning('Ova funkcija zahtijeva poc. zakrivljenost != 0'); return;
end

[bOk G1G2] = LP_LineFromPoints(G1.x, G1.y, G2.x, G2.y);
if(~bOk)
    warning('Tocke G1 i G2 su identicne?');
    return;
end

%% Ispitivanje nuznih uvjeta
% Ovo nisu uvjeti postojanja rjesenja vec ovime reduciramo skup mogucih rjesenja.
% U nekim slucajevima rjesenje moze postojati cak i ako ovi uvjeti nisu ispunjeni, ali takva rjesenja ne razmatramo.
% Jedan od razloga je i numericka nestabilnost koja bi se mogla pojaviti u takvim slucajevima.

%if(LP_GetDistanceToPoint(P0Tangent, G1.x, G1.y) * P0.k <= 0)
%    bOk = false; warning('Cilj je sa krive strane'); return;
%end

c_max = 20;
C1 = 1/sqrt(c_max);
C2 = 1;

CL1.x0 = P0.x;
CL1.y0 = P0.y;
CL1.th0 = P0.th;
CL1.k0 = P0.k;

BestLength = 1e20; % Najbolja pronadena duljina putanje (najkraca)

MaxIters = 3;
for(i=0:MaxIters-1)
    
    % PickScaling
    C = C1 + (C2-C1)*i/(MaxIters-1);
    
    % StraightPath
    CL1.C = C;
    CL1.c = sign(P0.k)/(C*C);
    CL1.length = CL1.k0 / CL1.c;
    CL1.Type = 'CL';
    [P1.x P1.y] = GetClothoidPoint(CL1, CL1.length); % P1 - krajnja tocka klotoide gdje je zakrivljenost = 0
    P1.th = CL1.th0 + CL1.k0*CL1.length + 0.5*CL1.c*CL1.length*CL1.length;
    
    Line = LP_LineFromDirectionAndPoint(P1.th, P1.x, P1.y);
    Line.m_Length = 0.25;
    % TUDU: CheckAdmissibility od ove gore linije - znaci dal sijece prepreke, kakav joj je clearance
    
    % Provjera dal Line sijece pravac G1G2 - ako da, onda radimo jedan hack kojeg sam prisiljen raditi jer nemam
    % funkciju koja dodaje novi cvor u putanju
    [bOK P2.x P2.y] = LP_GetIntersection(Line, G1G2);
    
    if(bOK)
        [bOK CL2 CL3 LN2 LN3] = SmoothSharpTurn(P1, P2, G2, 100, 100, 100, 100, [], []);
        iPathSegment = 1;
        ConnectPath{iPathSegment} = CL1;
        iPathSegment = iPathSegment+1;
        if(~isempty(LN2))
            ConnectPath{iPathSegment} = LN2;
            iPathSegment = iPathSegment+1;
        end
        ConnectPath{iPathSegment} = CL2;
        iPathSegment = iPathSegment+1;
        ConnectPath{iPathSegment} = CL3;
        iPathSegment = iPathSegment+1;
        if(~isempty(LN3))
            ConnectPath{iPathSegment} = LN3;
        end
        
    else
        error('Nedovrseni slucaj - u putanju se dodaju samo klotoide, a ne i linije - sredi to!');
        
        % Tudu: 
        P3 = G2;

        % PathPlanning->UpdateVisibilityGraph (tudu)

        % PathPlanning->GetShortestPath (tudu)
        [P3.x P3.y] = LP_GetPoint(Line, Line.m_Length);

        [tmp P3G1] = LPB_LineFromPoints(P3.x, P3.y, G1.x, G1.y);
        [P4.x P4.y] = LP_GetPoint(P3G1, P3G1.m_Length/2);

        % SmoothPath
        [bOK CL2 CL3 LN2 LN3] = SmoothSharpTurn(P1, P3, P4, 100, 100, 100, 100, [], []);
        [bOK CL4 CL5 LN4 LN5] = SmoothSharpTurn(P4, G1, G2, 100, 100, 100, 100, [], []);

        ConnectPath{1} = CL1;
        ConnectPath{2} = CL2;
        ConnectPath{3} = CL3;
        ConnectPath{4} = CL4;
        ConnectPath{5} = CL5;
    end
    
    Length = PA_GetPathLength(ConnectPath);
    if(Length < BestLength)
        BestLength = Length;
        BestC = C;
        BestConnectPath = ConnectPath;
    end
    
end

C = BestC;
ConnectPath = BestConnectPath;

%%
if(bDraw)
    % Crtanje drugi dio
    figure(1), hold on, grid on
    axis equal
    
%     plot(P0.x, P0.y, 'r.');
%     plot(P1.x, P1.y, 'r.');
%     plot(P3.x, P3.y, 'r.');
%     plot(G1.x, G1.y, 'r.');

    CL_Draw(CL1, 'r');
    plot(CL1.x0, CL1.y0, 'r.');
%     CL_Draw(CL2, 'r');
%     CL_Draw(CL3, 'r');
%     CL_Draw(CL4, 'r');
%     CL_Draw(CL5, 'r');
    
%     if(~isempty(LN2))
%         LPB_DrawLine(LN2, 'b');
%     end
%     if(~isempty(LN3))
%         LPB_DrawLine(LN3, 'b');
%     end
%     if(~isempty(LN4))
%         LPB_DrawLine(LN4, 'b');
%     end
%     if(~isempty(LN5))
%         LPB_DrawLine(LN5, 'b');
%     end
    
    %LPB_DrawLine(Line, 'b');
    
%     plot([P1.x P3.x], [P1.y P3.y], 'k:');
%     plot([P3.x G1.x], [P3.y G1.y], 'k:');
    
end

