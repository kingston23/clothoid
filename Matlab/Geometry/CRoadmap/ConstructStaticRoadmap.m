%function ConstructStaticRoadmap()
%% Inicijalizacija

if(~exist('CGeometry'))
    GE_Init;
end
if(~exist('LT'))
    load LT;
end

% Opcije
bDynObstacle = false; % dal treba dodati din prepreku?
bPioneer = false; % radimo s pioneerom ili s robosoccerom?
bPlanPath = true; % dal ocemo putanju od zadanog starta do cilja

% Read environment geometry from file
if(bPioneer)
    Workspace = read_vertices_from_file('./zavod.environment');
    Workspace = EnvironmentToMeters(Workspace);
else
    Workspace = read_vertices_from_file('./igraliste1.environment');
end

if(bPioneer)
    epsilon = 0.001;
else
    epsilon = 0.0001;
end

if(bDynObstacle)
    % Dinamicka prepreka
    DynWSpaceObstacle = [
    1.55 0.79
    1.551 0.851
    1.67 0.85
    1.66 0.79
        ];
    
    Workspace{end+1} = DynWSpaceObstacle;
end

if(bPioneer)
    RobotRadius = 0.3; % pioneer (mislim da je 0.32 tocnije - duzi je nek siri)
    Clearance = 0.2; % Pioneer
else
    RobotRadius = 0.04; % soccer
    Clearance = 0.06; % Soccer
end

tic

%% Konstruiraj konfiguracijski prostor

% Prosiri za r+epsilon
environment = DilateEnvironment(Workspace, RobotRadius); % prosirujemo za radijus robota

% Nakon prosirivanja moguce je da se neki poligoni sijeku sa samim sobom, i da se razliciti poligoni sijeku.
% Zato prvo rastavljamo svaki poligon na jednostavne poligone.

% Rastavljamo vanjski granicni poligon.
% Kaj se tice vanjskog poligona, prepostavljamo si situaciju da se on ne sijece sa samim sobom na nacin da bude recimo presijecen na pol
% nek da se sijeku neki mali ficleki. Njih jednostavno odbacujemo, iako su neki mozda dosta veliki da u njih stane robot, ali
% nemam vremena s tim se zajebavati.
[b SimplePolygons] = Pol_CheckSelfCollision(environment{1});
% Nadi najveci od ficleka i pretpostavi da je to taj kojeg trebamo (teoretski to uopce ne mora biti taj, al u praksi vecinom je)
iBiggest = -1;
nBiggest = -1;
for(i=1:length(SimplePolygons))
    if(length(SimplePolygons{i}) > nBiggest)
        iBiggest = i;
        nBiggest = length(SimplePolygons{i});
    end
end
environment{1} = SimplePolygons{iBiggest};

% Tu bi trebalo rastaviti i nutarnje poligone
% Medutim oni su vecinom u praksi konveksni, pa to prepustamo buducem radu

% Izracunaj uniju svih prepreka - PolygonClip u akciju!!!

% Pretvori poligone u format za PolygonClip

% Vanjski zidovi
% Obujmica - izgleda da treba staviti obujmicu da bi sve radilo.
% Ta obujmica mora obuhvacati unutarnji poligon zidova.
BoundingRect = Pol_GetBoundingRect(environment{1}, 0.01);
P1 = [];
P1(1).x = BoundingRect(:,1);
P1(1).y = BoundingRect(:,2);
P1(1).hole = 0;
P1(2).x = environment{1}(:,1);
P1(2).y = environment{1}(:,2);
P1(2).hole = 1;

% Otoci - tj. namjestaj ili nutarnji zidovi
P2 = [];
for(i=2:length(environment))
    P2(i-1).x = environment{i}(:,1);
    P2(i-1).y = environment{i}(:,2);
    P2(i-1).hole = 0;
end

ConfigurationSpace = PolygonClip(P1,P2,3); % 3 = unija
% Vrati natrag u matricu - osim zadnjeg poligona koji je samo ovojnica
ConfigurationSpace = Pol_StructToMatrix(ConfigurationSpace(1:end-1));

% Najdi vanjski poligon
iBoundary = Pol_FindBoundary(ConfigurationSpace);

% Put the boundary polygon to be the first
Tmp = ConfigurationSpace(iBoundary);
ConfigurationSpace(iBoundary) = ConfigurationSpace(1);
ConfigurationSpace(1) = Tmp;
iBoundary = 1;

% Ovo iz nekog razloga ne dela dobro ???
%if(Pol_IsClockwise(ConfigurationSpace{iBoundary}) == 1)
    % Poligon koji predstavlja vanjske zidove prostora treba biti anticlockwise, pa ga obracamo. Ili ne? Misterija!
    %ConfigurationSpace{iBoundary} = Pol_Reverse(ConfigurationSpace{iBoundary});
%end

% S ovim dosad dobili smo konfiguracijski prostor.

%% Konstruiraj prosireni konfiguracijski prostor.

% Sad moramo konfiguracijski prostor dodatno prosiriti za c-eps
DilatedCSpaceTmp = DilateEnvironment(ConfigurationSpace, Clearance);
DilatedCSpaceObstacles = [];

% Usput pokupimo chainpointe
% ChainPoints = [
%     0.98 0.9
%     1.22 0.9
%     1.55 1.3
%     1.8 1.2
%     1.8 0.25
%     1.4 0.1
%     1.8 0.1
%     %1.50000407629723         0.950392818611205
%     %1.77636543349448         0.900013045107539
%     ];

ChainPoints = zeros(0,2);

% Nakon prosirivanja moguce da poligoni sijeku sami sebe.
% Stoga ih rastavljamo na jednostavne poligone koji ne sijeku sami sebe i nemaju kolinearnih tocaka.

CSpaceObstacleIntersections = []; % Tu spremamo poligone koji su presjeci prosirenih cspace prepreka

for(i=1:length(DilatedCSpaceTmp))
    if(i==iBoundary)
        % Boundary drugacije tretiramo
        
        [b DilatedCSpaceBoundaries IntersectionPoints AllIntersectionPoints] = Pol_CheckSelfCollision(DilatedCSpaceTmp{i});
        DilatedCSpaceBoundaries = Pol_RemoveColinearPoints(DilatedCSpaceBoundaries);
        
        % Nakon rastavljanja na jednostavne poligone, neke od tih poligona treba odbaciti (nemaju fizikalni smisao)
        % Koje od njih treba odbaciti mozemo skuziti po smjeru obilazenja.

        % U slucaju vanjskih poligona, upotrebljivi ficleki moraju biti counterclockwise.
        % Ostali su na prvi pogled nekorisni, al mogu biti korisni za povezivanje potprostora, zato zapamti njih isto
        nUsefulPolygons = 0;
        nNotUsefulPolygons = 0;
        nBridgePolygons = 0;
        UsefulPolygons = [];
        NotUsefulPolygons = [];
        BridgePolygons = [];

        for(j=1:length(DilatedCSpaceBoundaries))
            if(Pol_IsClockwise(DilatedCSpaceBoundaries{j}) == -1)
                % Ovaj je koristan - pamti ga
                nUsefulPolygons = nUsefulPolygons + 1;
                UsefulPolygons{nUsefulPolygons} = DilatedCSpaceBoundaries{j};
            else
                if(NumRows(IntersectionPoints{j}) == 1)
                    % Ak poligon ima samo jednu poveznu tocku s ostalim poligonima, to je okrnjak kojeg nadam se ne trebamo
                    nNotUsefulPolygons = nNotUsefulPolygons + 1;
                    NotUsefulPolygons{nNotUsefulPolygons} = DilatedCSpaceBoundaries{j};
                else
                    % Ak poligon ima vise poveznih tocaka, to je mosni poligon koji povezuje dva potprostora - tog sigurno trebamo
                    nBridgePolygons = nBridgePolygons + 1;
                    BridgePolygons{nBridgePolygons} = DilatedCSpaceBoundaries{j};
                end
            end
        end
        DilatedCSpaceBoundaries = UsefulPolygons;

        ChainPoints = [ChainPoints; AllIntersectionPoints];
        if(~bPioneer)
            ChainPoints(:,2) = 0.9; % zlocesto
        end
        
    else
        % I unutarnje poligone (prepreke) opet posebno
        [b Tmp IntersectionPoints AllIntersectionPoints] = Pol_CheckSelfCollision(DilatedCSpaceTmp{i});
        % Tmp = Tmp(1); % U slucaju prepreka zadrzi samo prvu
        Tmp = Pol_RemoveColinearPoints(Tmp);
        
        iBiggest = 1;
        if(b)
            % Ako je nutarnji poligon rastavljen na vise poligona, treba zadrzati samo najveceg.
            % Racunamo koji ima najveci bbox i tog uzimamo kao najveceg.
            BBoxArea = 0;
            for(i=1:length(Tmp))
                [xMin xMax yMin yMax] = Pol_FindExtremes(Tmp{i});
                ThisBBoxArea = (xMax-xMin)*(yMax-yMin);
                if(ThisBBoxArea>BBoxArea)
                    BBoxArea = ThisBBoxArea;
                    iBiggest = i;
                end
            end
        end
        DilatedCSpaceObstacles = [DilatedCSpaceObstacles Tmp(iBiggest)];
        
        ChainPoints = [ChainPoints; AllIntersectionPoints];
        
        % Ostali poligoni su presjeci. Spremi ih jer ih kasnije trebamo.
        CSpaceObstacleIntersections = [CSpaceObstacleIntersections Tmp(1:iBiggest-1) Tmp(iBiggest+1:end)];
    end
end

if(bDynObstacle)
    % Ak radimo s "dinamickom" preprekom
    DynChainPoints = [1.50000407629723         0.950392818611205
        1.77636543349448         0.900013045107539];
    
    ChainPoints = [ChainPoints; DynChainPoints];
end

P2 = [];

%% Napravi dekompoziciju na potprostore

clear SubSpace;

% Svaki od ovih korisnih poligona od vanjskih poligona je posebna mini prostorija - environment za sebe
for(iSubSpace=1:length(DilatedCSpaceBoundaries))
    
    % Prvo moramo naci koje prepreke spadaju u ovaj prostor - koristimo operaciju presjeka kao collision check
    % Usput odmah racunamo i uniju
    
    DilatedObstaclesUnion = [];
    
    % Store boundary
    SubSpace(iSubSpace).CSpaceBoundary = DilatedCSpaceBoundaries{iSubSpace};
    
    P1 = [];
    P1(1).x = DilatedCSpaceBoundaries{iSubSpace}(:,1);
    P1(1).y = DilatedCSpaceBoundaries{iSubSpace}(:,2);
    P1(1).hole = 0;
    
    for(j=1:length(DilatedCSpaceObstacles))
        P2.x = DilatedCSpaceObstacles{j}(:,1);
        P2.y = DilatedCSpaceObstacles{j}(:,2);
        P2.hole = 0;
        
        % Presjek prepreke sa poligonom boundarija koji se ovdje ne tretira kao rupa - to je collision check
        Intersection = PolygonClip(P1,P2,1); % 1 = presjek
        
        if(~isempty(Intersection))
            
            if(isempty(DilatedObstaclesUnion))
                DilatedObstaclesUnion = P2;
            else
                % Dodaj novu prepreku u uniju
                DilatedObstaclesUnion = PolygonClip(DilatedObstaclesUnion,P2,3); % 3 = unija
            end
            
            % Ovdje usput radimo i detekciju kolizije prepreka sa svim ostalim preprekama - da bismo nasli presjeke
            % koji nam trebaju za planiranje u uskim prolazima
            for(iOtherObstacle=j+1:length(DilatedCSpaceObstacles))
                P3.x = DilatedCSpaceObstacles{iOtherObstacle}(:,1);
                P3.y = DilatedCSpaceObstacles{iOtherObstacle}(:,2);
                P3.hole = 0;
                
                Tmp = PolygonClip(P2,P3,1); % 1 = presjek
                
                if(~isempty(Tmp))
                    CSpaceObstacleIntersections = [CSpaceObstacleIntersections Pol_StructToMatrix(Tmp)];
                    
                    IntersectionPoints = Pol_IntersectionPoints(DilatedCSpaceObstacles{j}, DilatedCSpaceObstacles{iOtherObstacle});
                    ChainPoints = [ChainPoints; IntersectionPoints];
                end
            end
            
        end
    end
    
    % Store inner obstacles
    SubSpace(iSubSpace).CSpaceObstaclesUnion = DilatedObstaclesUnion;
    
    % Sad jos napravi uniju sa boundarijem
    
    % Unutarnji boundary sad tretiramo kao rupu
    P1(1).hole = 1;
    
    % Stavljamo vanjski bounding box (bez toga ne radi stvar)
    BoundingRect = Pol_GetBoundingRect(DilatedCSpaceBoundaries{iSubSpace}, Clearance); % Prosiri barem za clearance, inace bude problema kod trazenja presjeka
    P1(2).x = BoundingRect(:,1);
    P1(2).y = BoundingRect(:,2);
    P1(2).hole = 0;
    
    DilatedCSpace = [];
    if(~isempty(DilatedObstaclesUnion))
        DilatedCSpace = PolygonClip(P1,DilatedObstaclesUnion,3); % 3 = unija
        DilatedCSpace = Pol_StructToMatrix(DilatedCSpace(1:end-1));
        
        % Ali, trebamo i presjeke. Unutar presjeka koristi se skeletoni kao roadmap.
        
        Tmp = PolygonClip(P1, DilatedObstaclesUnion, 1); % 1 = presjek
        % Dodaj presjek u poligone presjeka
        if(~isempty(Tmp))
            CSpaceObstacleIntersections = [CSpaceObstacleIntersections Pol_StructToMatrix(Tmp)];
            
            for(iObstacle=1:length(DilatedObstaclesUnion))
                P2 = Pol_StructToMatrix( DilatedObstaclesUnion(iObstacle) );
                IntersectionPoints = Pol_IntersectionPoints(DilatedCSpaceBoundaries{iSubSpace}, P2{1});
                ChainPoints = [ChainPoints; IntersectionPoints];
            end
        end
    else
        DilatedCSpace{1} = DilatedCSpaceBoundaries{iSubSpace};
    end
    
    SubSpace(iSubSpace).DilatedCSpace = DilatedCSpace;
    
    %%
    % Izracunaj visibility graph za taj potprostor
    visibility_adjacency_matrix = visibility_graph2([], DilatedCSpace, epsilon); % Misel: prvi argument je guards i ne koristi se, al mora se nekaj zadati
    
    % Poslozi sve tocke u jednu matricu
    AllEnvironmentPoints = DilatedCSpace{1};
    AllIsReflex = Pol_IsReflexVertex(DilatedCSpace{1});
    for(i=2:length(DilatedCSpace))
        AllEnvironmentPoints = [AllEnvironmentPoints; DilatedCSpace{i}];
        AllIsReflex = [AllIsReflex; Pol_IsReflexVertex(DilatedCSpace{i})];
    end

    for i = 1 : size( visibility_adjacency_matrix , 1 )
        for j = 1 : size( visibility_adjacency_matrix , 2 )
            if(visibility_adjacency_matrix(i,j)==1 && AllIsReflex(i)<0 && AllIsReflex(j)<0)
                if(i<j) % Izbjegavanje da se isti rub crta dvaput
                    %plot3( [AllEnvironmentPoints(i,1) AllEnvironmentPoints(j,1)], [AllEnvironmentPoints(i,2) AllEnvironmentPoints(j,2)], 0.3*[1 1], ...
                    %    'm', 'LineWidth', 0.5 , 'LineStyle' , '-' );
                end
            else
                visibility_adjacency_matrix(i,j) = 0; % Iskljuci nepotrebne rubove
            end
        end
    end
    
    % Konvertiraj visibility matricu u graf sa kompaktnom strukturom
    VG = AdjacencyMatrixToConnectedStruct(AllEnvironmentPoints, visibility_adjacency_matrix);
    
    % Izbaci nespojene vrhove - na taj nacin izbacujemo i nerefleksne vrhove (TUDU: refleksan vrh moze takoder biti nepovezan, doduse rijetko, al moze)
    VG = GR_RemoveUnconnectedNodes(VG);
    
    SubSpace(iSubSpace).VG = VG; % store it
    
    % Extend VG by chain points
    for(iChainPoint=1:NumRows(ChainPoints))
        ChainPoint = ChainPoints(iChainPoint,:);
        if(Pol_IsPointIn(DilatedCSpaceBoundaries{iSubSpace}, ChainPoint))
            VisibilityPolygon = visibility_polygon(ChainPoint, DilatedCSpace, epsilon , 0.01 );
            %DrawPolygon(VisibilityPolygon, 'k');
            VG = VG_ConnectPoint(VG, ChainPoint, VisibilityPolygon);
        end
    end
    
    SubSpace(iSubSpace).VGExtended = VG; % store it
    
end

% Izracunaj skeletone unutar svakog presjeka

% Presjecima dodaj presjeke izmedu potprostora
CSpaceObstacleIntersections = [CSpaceObstacleIntersections BridgePolygons NotUsefulPolygons];

clear SK;
for(i=1:length(CSpaceObstacleIntersections))
    [VoronoiSegments n] = voronoi(CSpaceObstacleIntersections(i),1);
    
    if(n<1)
        continue;
    end
    
    % Izbaci segmente koji idu uz rub
    VoronoiSegments = Seg_RemoveBoundarySegments(VoronoiSegments, CSpaceObstacleIntersections{i}, epsilon);
    
    % Napravi graf na temelju skeletona
    SK(i) = GR_BuildFromSegments(VoronoiSegments);
    
    % Pronadi chain tocke koje spadaju u ovaj presjek
    
    ChainPointsHere = [];
    
    for(j=1:NumRows(ChainPoints))
        if(Pol_IsPointOnBoundary(CSpaceObstacleIntersections{i}, ChainPoints(j,:), epsilon))
            ChainPointsHere = [ChainPointsHere; ChainPoints(j,:)];
        end
    end
    
    % Spoji ChainPointe sa skeletonom
    for(j=1:NumRows(ChainPointsHere))
        SK(i) = GR_ConnectPointToClosest(SK(i), ChainPointsHere(j,:));
    end
    
end

if(~exist('SK'))
    SK = [];
end

% Connect all graphs into one. This is a static roadmap.
AllGraphs = [SubSpace(:).VGExtended SK];

StaticRoadmap = GR_Connect(AllGraphs, ChainPoints);

% Connect start and goal point to the graph
if(bPlanPath)
    if(bPioneer)
        StartAndGoal = [2.65379522445561          15.1518837107424
              51.9279158442761          13.8386684450925];
    else
        StartAndGoal = [0.17 0.17;
            %2.24 0.9
            2 1.6
        ];
    end
end

ExtendedRoadmap = StaticRoadmap;

toc

if(bPlanPath)
    for(iPoint = 1:NumRows(StartAndGoal))

        % First determine in which subspace are start and goal
        for(iSubSpace=1:length(SubSpace))

            if(Pol_IsPointIn(SubSpace(iSubSpace).CSpaceBoundary, StartAndGoal(iPoint,:)))

                VisibilityPolygon = visibility_polygon(StartAndGoal(iPoint,:),... 
                    SubSpace(iSubSpace).DilatedCSpace, epsilon , 0.01);

                ExtendedRoadmap = VG_ConnectPoint(ExtendedRoadmap, StartAndGoal(iPoint,:), VisibilityPolygon);

                break;
            end
        end
    end

    % Find shortest path

    [d Path] = dijkstra(ExtendedRoadmap.Nodes, ExtendedRoadmap.Edges, NumRows(ExtendedRoadmap.Nodes)-1, NumRows(ExtendedRoadmap.Nodes));
    [Path] = GR_PathRemoveColinearPoints(ExtendedRoadmap, Path);

    % Find a corridor polygon
    Corridor = GR_GetCorridor(ExtendedRoadmap, Path, Clearance);
end

%return;

%% Add dynamic obstacle

if(bDynObstacle)
    % Construct configuration space obstacle from it
    DynDilatedWSpaceObstacle = DilateEnvironment({DynWSpaceObstacle}, RobotRadius);

    % Compute union with other configuration space obstacles. There is none :)

    % Construct dilated configuration space obstacle
    DynDilatedCSpaceObstacle = DilateEnvironment(DynDilatedWSpaceObstacle, Clearance);

    % Make collision check with other dilated configuration space obstacles. ( I know there is.)
    % If there is collision, compute intersection. Normally, union is also needed to update VG.

    P1 = [];
    P1.x = DynDilatedCSpaceObstacle{1}(:,1);
    P1.y = DynDilatedCSpaceObstacle{1}(:,2);
    P1.hole = 0;

    for(iSubSpace=1:length(SubSpace))

        if(~isempty(SubSpace(iSubSpace).CSpaceObstaclesUnion))
            DynIntersection = PolygonClip(P1,SubSpace(iSubSpace).CSpaceObstaclesUnion,1); % 1 = presjek

            if(~isempty(DynIntersection))
                DynIntersection = Pol_StructToMatrix(DynIntersection);
                [VoronoiSegments] = voronoi(DynIntersection,1); % Ovo nade medial axis

                DynMedialAxis = GR_BuildFromSegments(VoronoiSegments);

                % Izbaci segmente koji idu uz rub
                VoronoiSegments = Seg_RemoveBoundarySegments(VoronoiSegments, DynIntersection{1}, epsilon);

                % Napravi graf na temelju skeletona
                DynCleanMedialAxis = GR_BuildFromSegments(VoronoiSegments);
            end
        end
    end
end


toc


% Drawing

if(bPioneer)
    Axis = [0.5 54 1 16.5];
    AxisPosition = [0 0 1 1]; % Vide se samo objekti bez osi
    FigurePosition = [0 0 1200 400];
else
    Axis = [-0.2 2.4 -0.04 1.84];
    %AxisPosition = [0.04 0.04 0.96 0.95]; % Ovak je bilo u prezentaciji - vide se brojevi uz osi
    AxisPosition = [0 0 1 1]; % Vide se samo objekti bez osi
    FigurePosition = [10 10 700 550];
end

%% Sve u jednoj slici
figure(1);
cla;

ConfSpaceColor = [0.5 0.5 1];
DilatedConfSpaceColor = [0.7 0.7 1];

Box = Pol_GetBoundingRect(Workspace{1}, 0.03);

DrawPolygon(Box, 'b', 1); hold on;
DrawPolygon(Workspace(1), ConfSpaceColor, 1); 

DrawPolygon(ConfigurationSpace(1), DilatedConfSpaceColor, 1);

% Dilated cofiguration space boundary
for(iSubSpace=1:length(SubSpace))
    iBoundary = Pol_FindBoundary(SubSpace(iSubSpace).DilatedCSpace);
    DrawPolygon(SubSpace(iSubSpace).DilatedCSpace(iBoundary), 'w', 1);
end

% Dilated configuration space obstacles
for(iSubSpace=1:length(SubSpace))
    iBoundary = Pol_FindBoundary(SubSpace(iSubSpace).DilatedCSpace);
    DrawPolygon(SubSpace(iSubSpace).DilatedCSpace(1:iBoundary-1), DilatedConfSpaceColor, 1);
    DrawPolygon(SubSpace(iSubSpace).DilatedCSpace(iBoundary+1:end), DilatedConfSpaceColor, 1);
end

% Configuration space obstacles
DrawPolygon(ConfigurationSpace(2:end), ConfSpaceColor, 1);

% Workspace obstacles
DrawPolygon(Workspace(2:end), 'b', 1);

% Draw graphs

for(iSubSpace=1:length(SubSpace))
    GR_Draw(SubSpace(iSubSpace).VGExtended, 'r');
end

% Drew Voronoi (or skeleton)
%[VoronoiSegments n] = voronoi(ConfigurationSpace,1); % 0=voronoi, 1=skeleton
%Seg_Draw(VoronoiSegments, 'k--');

for(i=1:length(SK))
    GR_Draw(SK(i), 'g');
end

DrawPolygon(CSpaceObstacleIntersections, 'g');

if(bPlanPath)
    GR_DrawPath(ExtendedRoadmap, Path, 'b');
end

set(gcf, 'Position', FigurePosition)
set(gca, 'Position', AxisPosition)
SetFig
axis(Axis)

%% Workspace
figure(2);
cla;

DrawPolygon(Box, 'b', 1); hold on;
DrawPolygon(Workspace(1), 'w', 1);
% Workspace obstacles
DrawPolygon(Workspace(2:end), 'b', 1);

%GR_Draw(StaticRoadmap, 'r');
%GR_Draw(ExtendedRoadmap, 'r');
%plot(StartAndGoal(:,1), StartAndGoal(:,2), 'Color', 'b', 'LineStyle', 'None', 'Marker', 'o');
%GR_DrawPath(ExtendedRoadmap, Path, 'b--');

if(bDynObstacle)
    DrawPolygon(DynWSpaceObstacle, 'b', 1);
end

if(bPlanPath)
    DrawPolygon(Corridor, 'g');
    SmoothedPath = GR_SmoothPLPath(ExtendedRoadmap, Path, Clearance);
    CL_DrawClothoidalPath(SmoothedPath, 'b');
    SampledPath = CL_SampleClothoidalPath(SmoothedPath, 0.001);
end

set(gcf, 'Position', FigurePosition)
set(gca, 'Position', AxisPosition)
SetFig
axis(Axis)


%% Configuration space
figure(3);
cla;

DrawPolygon(Box, 'b', 1); hold on;
DrawPolygon(Workspace(1), ConfSpaceColor, 1); 

DrawPolygon(ConfigurationSpace(1), 'w', 1);

% Configuration space obstacles
DrawPolygon(ConfigurationSpace(2:end), ConfSpaceColor, 1);

% Workspace obstacles
DrawPolygon(Workspace(2:end), 'b', 1); 

set(gcf, 'Position', FigurePosition)
set(gca, 'Position', AxisPosition)
SetFig
axis(Axis)

%% Dilated configuration space
figure(4);
cla;

DrawPolygon(Box, 'b', 1); hold on;
DrawPolygon(Workspace(1), ConfSpaceColor, 1); 

DrawPolygon(ConfigurationSpace(1), DilatedConfSpaceColor, 1);

% Dilated cofiguration space boundary
for(iSubSpace=1:length(SubSpace))
    iBoundary = Pol_FindBoundary(SubSpace(iSubSpace).DilatedCSpace);
    DrawPolygon(SubSpace(iSubSpace).DilatedCSpace(iBoundary), 'w', 1);
end

% Dilated configuration space obstacles
for(iSubSpace=1:length(SubSpace))
    iBoundary = Pol_FindBoundary(SubSpace(iSubSpace).DilatedCSpace);
    DrawPolygon(SubSpace(iSubSpace).DilatedCSpace(1:iBoundary-1), DilatedConfSpaceColor, 1);
    DrawPolygon(SubSpace(iSubSpace).DilatedCSpace(iBoundary+1:end), DilatedConfSpaceColor, 1);
end

% Configuration space obstacles
DrawPolygon(ConfigurationSpace(2:end), ConfSpaceColor, 1);

% Workspace obstacles
DrawPolygon(Workspace(2:end), 'b', 1); 

% Now draw other stuff, graphs, paths etc.

DrawPolygon(CSpaceObstacleIntersections, 'r');

if(~isempty(ChainPoints))
    plot(ChainPoints(:,1), ChainPoints(:,2), 'Color', 'r', 'LineStyle', 'None', 'Marker', 'o');
end

% Visibility graphs
for(iSubSpace=1:length(SubSpace))
    %GR_Draw(SubSpace(iSubSpace).VG, 'm');
    %GR_Draw(SubSpace(iSubSpace).VGExtended, 'm');
end

% Voronoi
%[VoronoiSegments n] = voronoi(ConfigurationSpace,1); % 0=voronoi, 1=skeleton
%Seg_Draw(VoronoiSegments, 'k');

% Mini skeletons
for(i=1:length(SK))
    GR_Draw(SK(i), 'r');
end

set(gcf, 'Position', FigurePosition)
set(gca, 'Position', AxisPosition)
SetFig
axis(Axis)

%% Dynamic obstacle
if(~bDynObstacle)
    return;
end

figure(5);
cla;

DrawPolygon(Box, 'b', 1); hold on;
DrawPolygon(Workspace(1), ConfSpaceColor, 1); 

DrawPolygon(ConfigurationSpace(1), DilatedConfSpaceColor, 1);

% Dilated cofiguration space boundary
for(iSubSpace=1:length(SubSpace))
    iBoundary = Pol_FindBoundary(SubSpace(iSubSpace).DilatedCSpace);
    DrawPolygon(SubSpace(iSubSpace).DilatedCSpace(iBoundary), 'w', 1);
end

% Dilated configuration space obstacles
for(iSubSpace=1:length(SubSpace))
    iBoundary = Pol_FindBoundary(SubSpace(iSubSpace).DilatedCSpace);
    DrawPolygon(SubSpace(iSubSpace).DilatedCSpace(1:iBoundary-1), DilatedConfSpaceColor, 1);
    DrawPolygon(SubSpace(iSubSpace).DilatedCSpace(iBoundary+1:end), DilatedConfSpaceColor, 1);
end

% Configuration space obstacles
DrawPolygon(ConfigurationSpace(2:end), ConfSpaceColor, 1);

% Workspace obstacles
DrawPolygon(Workspace(2:end), 'b', 1); 

% Now draw other stuff, graphs, paths etc.

%DrawPolygon(CSpaceObstacleIntersections, 'r');

%plot(ChainPoints(:,1), ChainPoints(:,2), 'Color', 'r', 'LineStyle', 'None', 'Marker', 'o');


% New dynamic obstacle
DrawPolygon(DynDilatedCSpaceObstacle, DilatedConfSpaceColor, 1);

DrawPolygon(DynDilatedWSpaceObstacle, ConfSpaceColor, 1);

DrawPolygon(DynWSpaceObstacle, 'b', 1);

GR_Draw(StaticRoadmap, 'r');

%DrawPolygon(DynIntersection, 'r');

GR_Draw(DynMedialAxis, 'r');

plot(DynChainPoints(:,1), DynChainPoints(:,2), 'Color', 'r', 'LineStyle', 'None', 'Marker', 'o');

set(gcf, 'Position', FigurePosition)
set(gca, 'Position', AxisPosition)
SetFig
axis(Axis)
