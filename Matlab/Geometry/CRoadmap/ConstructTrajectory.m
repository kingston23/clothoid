% Radi mi trajektoriju za doktorat

% Prije ovog treba pozvati:
%GE_Init
%load LT
%ConstructStaticRoadmap

SampledPath = CL_SampleClothoidalPath(SmoothedPath, 0.001);

[Time Velocity TotalTime] = Traj(SampledPath); % ovo duuugo traje (8 min)

disp = Velocity(:,1);
v = Velocity(:,2);

NewSampledPath = CL_SampleClothoidalPath2(SmoothedPath, disp);

%accel = diff(v)./diff(Time);
accel = zeros(length(Time),1);

% Redoslijed podataka u fajlu mora biti: Vrijeme Pomak(od_pocetka) x y kut lin_brzina lin_accel zakrivljenost deriv_zakrivljenosti
DataForFile = [Time disp [NewSampledPath(:).x]' [NewSampledPath(:).y]' [NewSampledPath(:).th]' ...
    v accel [NewSampledPath(:).k]' [NewSampledPath(:).dk]'     ];

save OfflineTraj.txt DataForFile -ASCII