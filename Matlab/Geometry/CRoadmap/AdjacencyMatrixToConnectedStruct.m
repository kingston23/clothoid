function Graph = AdjacencyMatrixToConnectedStruct(Points, AdjMatrix)
% Points - matrica n x 2 sa n tocaka (x,y)
% AdjMatrix - matrica n x n sa jedinicom na mjestu (i,j) ako su doticne tocke spojene, u nulom inace.
%   To je simetricna matrica sa jedinicama na diajgonali.
%
% Graph - struktura sa clanovima nodes i edges
%  NODES should be an Nx3 matrix with the format [ID X Y].
%       where ID is an integer, and X, Y are cartesian position coordinates)
%  SEGMENTS should be an Mx3 matrix with the format [ID N1 N2]
%       where ID is an integer, and N1, N2 correspond to node IDs from NODES list
%       such that there is an [undirected] edge/segment between node N1 and node N2

nPoints = length(Points);

Graph.Nodes(:,2:3) = Points;

% Postavi ID-ove jednake rednom broju tocke pocevsi od 1
for(i=1:nPoints)
    Graph.Nodes(i,1) = i;
end

% Broji kolko ima rubova i rezerviraj memoriju za njih.
% Ima ih kolko i jedinica u matrici, al treba oduzeti jedinice na dijagonali i podijeliti s dva jer je matrica simetricna,
% a za isti rub se dvaput javlja jedinica na mjestu (i,j) i (j,i).
nEdges = (length(find(AdjMatrix==1)) - nPoints) / 2;
Graph.Edges = zeros(nEdges, 3);

% Prodi sve elemente ispod dijagonale
iEdge = 1;
for(i=2:nPoints)
    for(j=1:i-1)
        if(AdjMatrix(i,j) == 1)
            Graph.Edges(iEdge,:) = [iEdge j i];
            iEdge = iEdge+1;
        end
    end
end

return;