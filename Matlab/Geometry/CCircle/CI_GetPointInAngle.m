function [x y] = CI_GetPointInAngle(this, Angle)
% Daje tocku na kruznici koja je pod zeljenim kutem u odnosu na srediste

x = this.m_xc + this.m_r*cos(Angle);
y = this.m_yc + this.m_r*sin(Angle);
