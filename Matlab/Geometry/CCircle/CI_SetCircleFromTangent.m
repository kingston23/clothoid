function [this] = CI_SetCircleFromTangent(xt, yt, TgAngle, Radius)
% Pozitivan radijus oznacava kruznicu lijevo od tangente, a negativan desno.
% Iz tog razloga vazan je i predznak kuta tangente, tj. tangenta je usmjerena.
% Npr. nije isto da li je kut 0 ili pi iako je smjer isti u oba slucaja!

% Vrijedi ista formula i za poz. i za neg. radijus.
this.m_xc = xt - Radius*sin(TgAngle); % cos(TgAngle+pi/2) = -sin(TgAngle) ili za r<0 je -cos(TgAngle-pi/2) = -sin(TgAngle)
this.m_yc = yt + Radius*cos(TgAngle); % sin(TgAngle+pi/2) = cos(TgAngle) ili za r<0 je -sin(TgAngle-pi/2) = cos(TgAngle)
this.m_r = abs(Radius);
