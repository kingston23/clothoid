function [nInters x1 y1 x2 y2] = CI_GetIntersectionWithLine(this, Line)
% Nalazi sjecista kruznice i implicitnog pravca

x1 = 0; y1 = 0; x2 = 0; y2 = 0; % Radi izbjegavanja greske "argument not assigned"

AA = Line.A*Line.A;
BB = Line.B*Line.B;
AB = Line.A*Line.B;
rr = this.m_r*this.m_r;
t = Line.C + Line.A*this.m_xc + Line.B*this.m_yc;
AABB = AA+BB;
D = AABB*rr - t*t;

if(D>0)
	sD = sqrt(D);

	t1 = -Line.A*Line.C + BB*this.m_xc - AB*this.m_yc;
	t2 = -Line.B*Line.C - AB*this.m_xc + AA*this.m_yc;

	x1 = (t1 - Line.B*sD) / AABB;
	y1 = (t2 + Line.A*sD) / AABB;

	x2 = (t1 + Line.B*sD) / AABB;
	y2 = (t2 - Line.A*sD) / AABB;

	nInters = 2; return;
elseif(D==0)
	t1 = -Line.A*Line.C + BB*this.m_xc - AB*this.m_yc;
	t2 = -Line.B*Line.C - AB*this.m_xc + AA*this.m_yc;

	x1 = t1 / AABB;
	y1 = t2 / AABB;

	nInters = 1; return;
else
	nInters = 0; return;
end
