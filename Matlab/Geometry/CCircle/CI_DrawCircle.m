function CI_DrawCircle(this, color)

th = linspace(0, 2*pi, 100);
x = this.m_xc + this.m_r*cos(th);
y = this.m_yc + this.m_r*sin(th);

plot(x,y,color);