opts = detectImportOptions('SampledTraj.txt');
%opts = detectImportOptions('smoothedPath.txt');

opts = setvartype(opts,1:12,'double');
t = readtable('SampledTraj.txt', opts);
figure()
x= table2array(t(:,8));
y= table2array(t(:,9));
th = table2array(t(:,10));
th_ = th*180/pi;
table1 = [x y th_];

plot(x,y, 'b','Linewidth', 1.5);
hold on
%plotanje samo ciljnih tocaka
%primjer 1: jednostavno.txt
%xx= [6.350000, 3.350000,  0.950000];
%yy= [3.150000, 5.450000, 5.95];
%primjer 2: tezi put

% xx= [6.350000, 3.350000, 2.650000, 2.550000, 0.950000];
% yy= [3.150000, 5.450000, 5.450000, 5.350000, 2.950000];

% xx= [6.350000, 3.350000, 2.650000, 2.550000, 0.950000, 0.950000];
% yy= [3.150000, 5.450000, 5.450000, 5.350000, 2.050000, 2.050000];


% primejr 3:
% xx= [8.350000, 9.150000, 9.150000, 9.050000, 8.350000, 8.250000, 8.150000, 7.550000, 7.350000, 6.450000];
% yy= [0.950000, 2.050000, 2.550000, 3.250000, 6.350000, 6.450000, 6.550000, 6.550000, 6.350000,2.650000 ];

%pr 4
% xx= [3.850000, 2.650000, 2.450000, 2.450000, 2.550000, 3.250000, 3.350000, 3.450000, 7.750000];
% yy= [5.750000, 5.450000, 5.250000, 4.850000, 4.150000, 1.050000, 0.950000, 0.850000, 0.750000 ];

%plot(xx,yy, 'r--', 'Linewidth', 0.8);

%%

% xx= [6.398000, 6.396800, 6.27711,6.398000, 5.350000, 2.9500];
% yy= [3.10000,2.950005,  3.1000, 3.100000, 3.45,  6.050000];
%% crtanje tocaka nakon filtiranja i ubacivanja dodatnih zbog orijentacije
opts2 = detectImportOptions('goal_position.txt');
opts2 = setvartype(opts2,1:2,'double');
goaltable = readtable('goal_position.txt', opts2);
xx = table2array(goaltable(:,1));
yy = table2array(goaltable(:,2));
plot(xx,yy, 'r--', 'Linewidth', 0.8);
hold on

% legend('Smoothed Path', 'TWD*', 'start', 'goal');
plot(x(1),y(1),'g*');
hold on
plot(x(end),y(end),'k*');
hold on
% text(x(1)+0.02,y(1)+0.02,'start');
% text(x(end)+0.02,y(end)+0.02,'goal');

xlabel('x');
ylabel('y');

%% crtanje logera
% hold on
% 
% WH_gridmap_x=load('wh_gridmap_x.dat');
% 	WH_gridmap_y=load('wh_gridmap_y.dat');
%      WH_gridmap_static_cell=load('wh_gridmap_static_cell.dat');
%     metric=0.001;
% WH_gridmap_x=WH_gridmap_x*metric;
%     WH_gridmap_y=WH_gridmap_y*metric;
% %     figure(1);
%     hold on;%grid on;
% %     axis equal tight;
% map = [1 1 1; 0.7, 0.7, 0.7];
% colormap(map)
% novex=[];novey=[];starex=[];starey=[];
% for i=1:max(size(WH_gridmap_x))
%                 if(WH_gridmap_static_cell(i)==1)
%                     starex=[starex;WH_gridmap_x(i)];
%                     starey=[starey;WH_gridmap_y(i)];
%                else
%                     novex=[novex;WH_gridmap_x(i)];
%                     novey=[novey;WH_gridmap_y(i)];
%              end
% end
% plot(starex,starey,'g.');
% %  plot(novex,novey,'m.');
% WH_globalna_putanja_x=load('robot_globalna_putanja_x.dat');%odvozena putanja
% WH_globalna_putanja_y=load('robot_globalna_putanja_y.dat');
% WH_globalna_putanja_th=load('robot_globalna_putanja_th.dat');
% fakeloc_x=load('fakelocrobot_globalna_putanja_x.dat');%odvozena putanja iz fakelocalize-a
% fakeloc_y=load('fakelocrobot_globalna_putanja_y.dat');
% fakeloc_th=load('fakelocrobot_globalna_putanja_th.dat');
% goal=load('wh_goal_position.dat');
% 
% %plotanje putanja
% %  plot(WH_globalna_putanja_x*metric,WH_globalna_putanja_y*metric,'r');
%  plot(WH_globalna_putanja_x*metric,WH_globalna_putanja_y*metric,'r.-');
% %  plot(goal(1)*metric,goal(2)*metric,'b*');
% %  plot(fakeloc_x*metric,fakeloc_y*metric,'g.');
% 
%     %plotanje pocetne i konacne koordinate robota
%     %nacrtajmo robota polumjer robota (mm)
% 	rr=260*metric;
% 	fi=linspace(0,2*pi,30);
% 	x_temp=WH_globalna_putanja_x(1)*metric;y_temp=WH_globalna_putanja_y(1)*metric;th_temp=WH_globalna_putanja_th(1);
% 	for i=1:30
% 		x_circle(i)=x_temp+rr*cos(fi(i));
% 		y_circle(i)=y_temp+rr*sin(fi(i));
% 	end
% 	plot(x_circle,y_circle,'k--');
% 	plot([x_temp x_temp+rr*cos(th_temp)], [y_temp y_temp+rr*sin(th_temp)],'k--');
% % 
% 	x_temp=WH_globalna_putanja_x(end)*metric;y_temp=WH_globalna_putanja_y(end)*metric;th_temp=WH_globalna_putanja_th(end);
% 	for i=1:30
% 		x_circle(i)=x_temp+rr*cos(fi(i));
% 		y_circle(i)=y_temp+rr*sin(fi(i));
% 	end
% 	plot(x_circle,y_circle,'k--');
% 	plot([x_temp x_temp+rr*cos(th_temp)], [y_temp y_temp+rr*sin(th_temp)],'k--');
% %     
% 
%     
% % for i=1:length(putic(:,1))
% %     plot(putic(i,1),putic(i,2),'dm')
% %     pause
% % end
%     %figure(1);
%     ylabel('y [m]', 'fontsize',16,'fontname', 'times');
%     %title('Robot environment', 'fontsize', 18,'fontname', 'times');
%     xlabel('x [m]', 'fontsize',16,'fontname', 'times');
%     h=gca;
%     set(h,'fontsize',10,'fontname','times','box', 'on');
%     axminx=min(min(starex),min(novex));
%     axminy=min(min(starey),min(novey));
%     axmaxx=max(max(starex),max(novex));
%     axmaxy=max(max(starey),max(novey));
% %     axis([-26 6 -10 10])   
% % axis([axminx axmaxx axminy axmaxy])
% starttxt=[WH_globalna_putanja_x(1) WH_globalna_putanja_y(1)]*metric;
%     goaltxt=[WH_globalna_putanja_x(end) WH_globalna_putanja_y(end)]*metric;
% text(starttxt(1),starttxt(2),'start','Interpreter','latex')%s h
% text(goaltxt(1),goaltxt(2),'goal','Interpreter','latex')%s h
% axis equal tight
% % print('-depsc2','ime.eps')%radi super bounding boxove