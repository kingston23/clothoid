Source ROS environment
`source /opt/ros/melodic/setup.bash`

Create and build a catkin workspace:

`mkdir catkin_ws`

`cd catkin_ws`

`mkdir src`

`cd src`

`catkin_init_workspace`

`git clone git@bitbucket.org:unizg-fer-lamor/clothoid.git`

`cd ..`

compiling:

`catkin_make`


set up the path:

`cd devel`

`source setup.bash`


### SET INPUT PARAMETERS ###

Set goal, current position and path:
`roslaunch nodelet_talker talker.launch`

