function xn=kinmod(x0,u)
    delaydt=1;
    global u1 u2 u3
    u3=u2;
    u2=u1;
    u1=u;
    dt=0.1;%0.118589495784/10;%0.1;
    sum=0.; %0, bez suma, 0.1 model sa sumom od (-0.5,0.5)*0.1 m/s ili rad/s jos 0.1 puta, isprobano s 0.2
    w=u3(2)+(rand(1)-0.5)*sum*0.5;
    v=u3(1)+(rand(1)-0.5)*sum;
    if (abs(w)>0)
        xn(3)=x0(3)+w*dt;
        xn(1)=x0(1)+v/w*(sin(xn(3))-sin(x0(3)));
        xn(2)=x0(2)-v/w*(cos(xn(3))-cos(x0(3)));
    else
        xn(1)=x0(1)+v*cos(x0(3))*dt;
        xn(2)=x0(2)+v*sin(x0(3))*dt;
        xn(3)=x0(3)+w*dt;
    end
    if (xn(3)>pi)
        xn(3)=xn(3)-2*pi;
    end
    if (xn(3)<-pi)
        xn(3)=xn(3)+2*pi;
    end
end
