# Acado trajectory planning plus driving

This README explains starting and using the acado and driving the robot along the trajectory. Acado calculates the state trajectory of the differential drive mobile robot between start state and the end state and as the output is state in the form [nt x y theta v omega] and parameters in the form [nt tmax], where nt is normalized time, tmax is the final time.

### Getting started

* Put the whole acado_traj folder into your catkin workspace's src folder. 
* install acado for your ROS, for example `sudo apt-get install ros-kinetic-acado`
* Compile the source by typing `catkin_make` in the root folder of your catkin workspace
* Open the terminal to calculate the trajectory by acado:

```
roscd acado_traj
bin/trajacado
```

The output will be saved to `parameters3.csv` and `state3.csv`.


Driving along that trajectory can be done by another binary `bin/drivetraj` which outputs velocity commands to `cmd_vel` topic. Open another terminal to start the prepared simulation from `twdplanner`:

1. `roslaunch twdplanner ushapeamcltest.launch`

2. `bin/drivetraj`

As the output of the actual trajectory is `odvozena` and acado trajectory is rewritten to `state3` and `parameters3` ready to be plotted in Matlab. Open the Matlab and start the script `odvozenatrajros.m`.

For test driving the acado trajectory can be tested in Matlab with the script `trajectorytrackingone.m`.

### Changing start and end states for acado trajectory

Open `prepreke_t.cpp`. The model is defined as:

```
    f << dot(x) == cos(theta)*v;
    f << dot(y) == sin(theta)*v;
    f << dot(theta) == w;
    f << dot(v) == a;
    f << dot(w) == alfa;
```

The start state is defined as follows and needs to be aligned with the ROS simulation.

```
    ocp3.subjectTo( AT_START, x ==  6.4 );
    ocp3.subjectTo( AT_START, y ==  3.1 );
    ocp3.subjectTo( AT_START, theta ==  -PI/2 );
    ocp3.subjectTo( AT_START, v ==  0.0 );
    ocp3.subjectTo( AT_START, w ==  0.0 );
```

The end state is defined as follows and can be chosen arbitrarly:

```
    ocp3.subjectTo( AT_END  , x == 2. );
    ocp3.subjectTo( AT_END  , y ==  7. );
    ocp3.subjectTo( AT_END  , theta == PI );
    ocp3.subjectTo( AT_END  , v ==  0.0 );
    ocp3.subjectTo( AT_END  , w == 0.0 );
```

The constraints are defined as follows and also can be changed:

```
    ocp3.subjectTo(-0.75 <= v <= 0.75);
    ocp3.subjectTo(-100*PI/180 <= w <= 100*PI/180 );
    ocp3.subjectTo(-0.5 <= a <= 0.5);
    ocp3.subjectTo(-100*PI/180 <= alfa <= 100*PI/180 );
```

The constraint on obstacles boundary can be added as follows:

```
    ocp3.subjectTo(3. <= x <= 8.); ocp3.subjectTo(2. <= y <= 6.);
```

The final time influences mainly on the found solution so this needs to be tuned:

```
    ocp3.subjectTo(0 <= T <= 13); 
```

### Changing constraints for driving the trajectory in ROS

Open `main.cpp` and find and change the following section:

```
        double amax=0.5;
        double alfamax=100*M_PI/180;
        double vmax=0.75;
        double wmax=100*M_PI/180;
```

The constraints needs to correspond to the constraints of acado optimization.
