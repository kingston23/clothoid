close all

c=load('odvozena');
opts = detectImportOptions('smoothedtraj23.txt');
opts = setvartype(opts,1:6,'double');
t = readtable('smoothedtraj23.txt', opts);
at = table2array(t(:,1));


s=load('smoothedtraj23.txt');

figure(1)
plot(c(:,1),c(:,2),'gd-')
hold on
plot(s(:,2),s(:,3),'b.-')
xlabel('x [m]')
ylabel('y [m]')
title('trajectory')
axis equal tight



figure(2)
subplot(2,1,1);
dt=0.1;
% plot( linspace(0,dt*length(c(:,1)),length(c(:,1))),c(:,4),'gd-')
plot( c(:,8),c(:,4),'kd-')
hold on
plot( c(:,8),c(:,6),'md-')
plot(at, s(:,5),'b.-')
legend('vset','v','v acado');

subplot(2,1,2);
plot( c(:,8),c(:,5),'kd-')
hold on
plot( c(:,8),c(:,7),'md-')
plot(at, s(:,6),'b.-')
legend('wset','w','w acado')

figure(3)
plot(diff(c(:,8)))
hold on
plot(diff(at))
title('time steps duration')
legend('ros','acado traj')

axis equal tight
