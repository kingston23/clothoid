close all
clear

global u1 u2 u3
u1=[0 0];
u2=u1;
u3=u1;
% load traj %tu su ax [mm] ay [mm] ath [rad] av [mm/s] aw [rad/s]
t=load('parameters3');
at=t(:,1).*t(:,2);
a=load('states3');
ax=a(:,2)'*1000;
ay=a(:,3)'*1000;
ath=a(:,4)';
av=a(:,5)'*1000;
aw=a(:,6)';

figure(10)
plot(diff(at))

%odabir algoritma: trackalg=0 bang bang, trackalg=1 kanayama
trackalg=1;

figure(1),hold on
footprintmm=load('robot_footprint.dat');
rrmm=255.5;%pioneer - the largest x coordinate
scale1=1;
metric=0.001;




        for kk=[1 length(ax)] %kk=1:length(ax)
                tempcolor=[0.8 0.8 0.8];
                tempcolor=[0 0 0];
                                    th_temp=ath(kk);
                        x_temp=ax(kk); y_temp=ay(kk);
                        plot(([x_temp x_temp+rrmm*scale1*cos(th_temp)])*metric, ([y_temp y_temp+rrmm*scale1*sin(th_temp)])*metric,'b','Color',tempcolor,'Linewidth',1);
                        plot(((x_temp+cos(th_temp)*scale1*footprintmm(:,1)-sin(th_temp)*scale1*footprintmm(:,2)))*metric,((y_temp+sin(th_temp)*scale1*footprintmm(:,1)+cos(th_temp)*scale1*footprintmm(:,2)))*metric,'b','Color',tempcolor); 
        end
            
        plot(((ax))*metric,((ay))*metric,'b.-','LineWidth',2);
        %provjera kin modela - super! poklapa se!
        if (0)
        for kk=1:length(ax)
            xn=kinmod([ax(kk)*metric ay(kk)*metric ath(kk)],[av(kk)*metric aw(kk)])
            plot(xn(1),xn(2),'*')
        end
        end

        %tracking
        bv=av(kk)*0;
        bw=aw(kk)*0;
        bx=bv;
        by=bv;
        bth=bv;
        xc=ax(1)*metric;
        yc=ay(1)*metric;
        thc=ath(1);
        vc=0.;%pocetna brzina
        wc=0;
        dt=0.1;
        amax=0.5;
        alfamax=100*pi/180;
        vmax=0.75;
        wmax=100*pi/180;
        %goal coordinates
        xG=ax(end)*metric
        yG=ay(end)*metric
        thG=ath(end);
        if thG>pi
            thG=thG-2*pi;
        end
        if thG<-pi
            thG=thG+2*pi;
        end
        thG
        %drugi alg:
        %some coefficient
        k_x = 0.15;
        k_y = 0.15;
        k_theta = 2 * sqrt(k_y);

        kk=1;
        jj=0;
        %inicijalizacija
        for ii=1:kk
            bx(ii)=xc;
            by(ii)=yc;
            bth(ii)=thc;
            bv(ii)=vc;
            bw(ii)=wc;
        end
        kk=0;
%         dt=0.118589495784/10;
        dt=0.1;
        while (abs(xc-xG)>0.3 || abs(yc-yG)>0.3 || min([abs(thc-thG) abs(thc-thG-2*pi) abs(thc-thG+2*pi)])>25*pi/180 || (abs(vc)>amax*dt) || (abs(wc)>alfamax*dt)) %for kk=5:length(ax)
            jj=jj+1;
            while (kk<length(ax)) && ((jj-1)*dt>=at(kk+1))
                kk=kk+1;
            end
            vt=av(kk)*metric;
            wt=aw(kk);
            xt=ax(kk)*metric;
            yt=ay(kk)*metric;
            tht=ath(kk);
            if trackalg==0
                ex=(xt-xc)*cos(tht)+(yt-yc)*sin(tht); %stari
                ey=(xt-xc)*sin(tht)+(yt-yc)*cos(tht); %stari
            else
                %drugi alg daje druge ex ey, i radi bolje
                ex=(xt-xc)*cos(thc)+(yt-yc)*sin(thc);
                ey=-(xt-xc)*sin(thc)+(yt-yc)*cos(thc);
            end
            eth=tht-thc;
            if (vt<0) %kljucan dio za rikverc
                eth=thc-tht;
            end
            if (eth>pi)
                eth=eth-2*pi;
            end
            if (eth<-pi)
                eth=eth+2*pi;
            end
            if trackalg==0
                ws=wt+sqrt(2*alfamax*abs(eth))*sign(eth); %stari
            else
                %corrected control
                ws = wt + vt * (k_y * ey + k_theta * sin(eth));
            end
            alfac=alfamax*sign(ws-wc);
            if abs((ws-wc)/dt)<abs(alfac)
                alfac=(ws-wc)/dt;
            end
            wc=wc+alfac*dt;
            if wc>wmax
                wc=wmax;
            end
            if wc<-wmax
                wc=-wmax;
            end
            if trackalg==0
                vs=vt-vc*cos(eth)+wt*ey+sqrt(2*amax*abs(ex))*sign(ex); %stari
            else
                %corrected control
                vs = vt * cos(eth) + k_x * ex;
            end
            ac=amax*sign(vs-vc);
            if abs((vs-vc)/dt)<abs(ac)
                ac=(vs-vc)/dt;
            end
            vc=vc+ac*dt;
            if vc>vmax
                vc=vmax;
            end
            if vc<-1*vmax
                vc=-1*vmax;
            end
            bv(jj)=vc;
            bw(jj)=wc;
            xn=kinmod([xc yc thc],[vc wc]);
            xc=xn(1);
            yc=xn(2);
            thc=xn(3);
            bx(jj)=xc;
            by(jj)=yc;
            bth(jj)=thc;
            plot(xn(1),xn(2),'o')
            plot(xt,yt,'mo')
            h=plot([xc xc+rrmm*scale1*metric*cos(thc)], [yc yc+rrmm*scale1*metric*sin(thc)],'b','Color',tempcolor,'Linewidth',1);
            hf=plot(((xc+cos(thc)*scale1*footprintmm(:,1)*metric-sin(thc)*scale1*footprintmm(:,2)*metric)),((yc+sin(thc)*scale1*footprintmm(:,1)*metric+cos(thc)*scale1*footprintmm(:,2)*metric)),'b','Color',tempcolor); 
            pause
            delete(h)
            delete(hf)
        end
            figure(2)
            subplot(2,1,1);
            hold on
%             plot(av*metric,'b')
            plot( at,av*metric,'b')
            plot( linspace(0,dt*length(bv),length(bv)),bv,'r')
            subplot(2,1,2);
            hold on
%             plot(aw,'b')
            plot( at,aw,'b')
            plot( linspace(0,dt*length(bv),length(bv)),bw,'r')
%  plot(bw,'r')
            legend('optimal','tracked')
            figure(1)
            h=plot([xc xc+rrmm*scale1*metric*cos(thc)], [yc yc+rrmm*scale1*metric*sin(thc)],'b','Color',tempcolor,'Linewidth',1);
            hf=plot(((xc+cos(thc)*scale1*footprintmm(:,1)*metric-sin(thc)*scale1*footprintmm(:,2)*metric)),((yc+sin(thc)*scale1*footprintmm(:,1)*metric+cos(thc)*scale1*footprintmm(:,2)*metric)),'b','Color',tempcolor); 

disp('average speed (mm/s) and time (s) to goal')
time_speed=av*ones(size(av))'/length(av);
time_togoal=length(ax)*dt;
speedgoal=[time_speed time_togoal]

disp('tracking: average speed (mm/s) and time (s) to goal')
time_speed=bv*ones(size(bv))'/metric/length(bv);
time_togoal=length(bx)*dt;
speedgoal=[time_speed time_togoal]

goalpostol=sqrt((bx(end)-xG)^2+(by(end)-yG)^2);
goalthtol=abs(bth(end)-thG);
while goalthtol>pi
    goalthtol=abs(goalthtol-2*pi);
end
goalthtol=goalthtol*180/pi;

disp('close to the goal: goalpostol (mm) goalthtol (deg)')
goaltol=[goalpostol goalthtol]
 

    plot(bx,by,'r')
    ylabel('y [m]', 'fontsize',12,'fontname', 'times');
    xlabel('x [m]', 'fontsize',12,'fontname', 'times');
    h=gca;
    set(h,'fontsize',12,'fontname','times','box', 'on');

    