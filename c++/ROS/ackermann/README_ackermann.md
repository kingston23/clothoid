# TWD Planner

This README explains starting and using the TWD Planner.

### Set params

To use package `pathtwdlistener` for patrolling with twd replanning set in CMakeLists.txt `add_executable(pathls src/pointfilter_ackermann.cpp)` as active:

```
add_executable(pathls src/pointfilter_ackermann.cpp)
``` 

In `twdsrc/twdpanner/include` set in `Params.h`  #define LOADMAP 0 if you dont use map.

In `twdsrc/twdpanner/include` set in `moj.h` fixed_frame to `odom`:

```
VisualizationPublisher(ros::NodeHandle n) :
      nh_(n),  fixed_frame_("odom") //map kad koristis stage, odom u gazebu simulacija, minefield gazebo kasnija simulacija
```

In `nodelet_talker/CMakeLists.txt` use:

```
## Declare a C++ library
add_library(nodelet_talker
#src/talker.cpp
#src/90deg_test_talker.cpp
src/twd_talker.cpp
#src/ccpp_talker.cpp
#src/patroling_talker.cpp
#src/patroling_talker_test.cpp
)
```

### Getting started

* Put the whole twdsrc folder into your catkin workspace's src folder. The folder `twdplanner` is the TWD planner, while the folder `pathtwdlistener` is the simple subscriber to the output message of the TWD planner. 
* Compile the source by typing `catkin_make` in the root folder of your catkin workspace
* Open three terminals and start:

1. `roslaunch steer_bot_sim.launch`

2. `roslaunch twdplanner starttwd.launch`

3. `roslaunch pathtwdlistener startpathlistener.launch`


To use smoothing with TWD* run:

4. `roslaunch nodelet_talker test1.launch`

5. `roslaunch nodelet_talker talker.launch`

6. `rosrun ackermann driveackermann`

7. `roslaunch view_steer_bot_robot.launch`



Click on the rviz window on `2D Nav Goal` button and then click somewhere in the free space of the loaded environment. The robot should start to move to the selected goal point. The TWD path should be plotted on the rviz window.

## TEST ONLY TWD*

Launch file starttwd.launch:

```
<?xml version="1.0"?>
<launch>
<!-- running the navigation in gazebo -->   
    <!--node name="twd" pkg="twdplanner" type="twd" output="screen" launch-prefix="gdb -ex run args" ispred arg idu 2 crtice-->
    <node name="twd" pkg="twdplanner" type="twd" output="screen">
        <param name="base_frame" value="base_link" />
        <param name="world_frame" value="odom" />
        <!--param name="odom_topic" value="/odom" /-->
        <param name="odom_topic" value="/odom" />
        <param name="scan_topic" value="scan" />
        <param name="cmd_vel_topic" value="/steer_bot/ackermann_steering_controller/cmd_vel" />
        <param name="vx_min" type="double" value="0" />
        <param name="vx_max" type="double" value="750." />
        <param name="dvx_max" type="double" value="500." />
        <param name="w_min" type="double" value="-100." />
        <param name="w_max" type="double" value="100." />
        <param name="dw_max" type="double" value="100." />
        <param name="robot_radius" type="double" value="400." />
        <param name="drive_flag" type="bool" value="true" />
    </node>

</launch>
```

1. `roslaunch steer_bot_sim.launch`

2. `roslaunch twdplanner starttwd.launch`

3. `roslaunch view_steer_bot_robot.launch`


Click on the rviz window on `2D Nav Goal` button and then click somewhere in the free space of the loaded environment. The robot should start to move to the selected goal point. The TWD path should be plotted on the rviz window.
