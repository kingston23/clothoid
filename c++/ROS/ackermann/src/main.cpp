#include <sys/time.h>
#include <iostream>
#include <string>
#include "ros/ros.h"

//#include <algorithm>

#include <geometry_msgs/Vector3.h>
#include <std_msgs/Float64.h>
#include <std_msgs/UInt8.h>


int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "pathtwdlistener");
  ros::NodeHandle nh;

  ros::Rate rate(10.0);
  //geometry_msgs::Vector3 new_vel;
	std_msgs::Float64 t_cmd, s_cmd;
  std_msgs::UInt8 g_cmd;
  ros::Publisher throttle_pub, steering_pub, gear_pub;
	throttle_pub = nh.advertise<std_msgs::Float64>("/throttle_cmd",10);
	steering_pub = nh.advertise<std_msgs::Float64>("/steering_cmd",10);
  gear_pub = nh.advertise<std_msgs::UInt8>("/gear_cmd",10);

  while (nh.ok()) {
    ros::spinOnce(); 

    t_cmd.data = 3.0;
    s_cmd.data = 1.0;
    g_cmd.data = 0;

    throttle_pub.publish(t_cmd);
    steering_pub.publish(s_cmd);
    gear_pub.publish(g_cmd);

    rate.sleep();
  }
  return 0;
}

