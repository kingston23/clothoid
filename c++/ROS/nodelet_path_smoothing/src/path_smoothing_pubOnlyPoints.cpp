#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>

#include <std_msgs/String.h>
#include <stdio.h>
#include "nodelet_path_smoothing/OutputData.h"
#include "nodelet_path_smoothing/OutputPath.h"

#include "nodelet_talker/InputData.h"

#include "math.h"
#include <vector>
#include <list>
#include <sstream>

#include "Traj/PathSmoothing.h"
#include "Traj/TrajTrackingController.h"
#include "Traj/TrajTrackingRegulator_Nonlinear.h"
//#include "Traj/TimeProfilePlanner.h"


namespace nodelet_path_smoothing
{
class path_smoothing: public nodelet::Nodelet
{
public:
    ros::Publisher pub_result, pub_path_result;
    ros::Subscriber sub_chat;   
    typedef double T;
    double Delta = 0.01;
    int completed = 0;
    virtual void onInit() {  
        ros::NodeHandle& private_nh = getPrivateNodeHandle();
        pub_result = private_nh.advertise<OutputData>("result",10);
        pub_path_result = private_nh.advertise<OutputPath>("SmoothedPathPub",10);
        sub_chat = private_nh.subscribe<nodelet_talker::InputData>("msg_in", 1, &path_smoothing::callback, this);
        //ROS_INFO("Napravio pub i sub");
    }    


    void callback(const nodelet_talker::InputData::ConstPtr& input)
    {
        //ROS_INFO("Usao u callback");

        C2DTrajectoryBase::S2DTrajPoint* goalState = (C2DTrajectoryBase::S2DTrajPoint*)input->GoalStatePtr;
        C2DTrajectoryBase::S2DTrajPoint* currentState = (C2DTrajectoryBase::S2DTrajPoint*)input->CurrentStatePtr;
        std::vector<Geom::C2DPoint >* PWLinearPath= (std::vector<Geom::C2DPoint >*)input->PWLinearPathPtr;

        if(!C2DClothoidApp::CreateLookupTable())
            return;

        CClothoidPathSmoothing PathSmoothing;
        PathSmoothing.SmoothPath((*currentState), (*goalState), (*PWLinearPath));
        CTrajCompositePathProfile& SmoothedPath = PathSmoothing.SmoothedPath();    

        OutputData data;
        OutputPath path_data;
        geometry_msgs::Point p; 

        path_data.points.clear();
        
        data.SmoothedPathPtr=(long)(&SmoothedPath);
        pub_result.publish(data);   //objavljivanje poruke na temu

        C2DTrajectoryBase::S2DTrajPoint TrajPt;
        TrajPt.SetAllZero();
        // spremanje po putu
        T Dist1 = SmoothedPath.GetBeginDist();
        T Dist2 = SmoothedPath.GetEndDist();
        
        ROS_INFO("pocetak trajektorije na %f, kraj trajektorije na %f", Dist1, Dist2);
        for(T Dist = Dist1; Dist <= Dist2; Dist += Delta)
        {
            TrajPt.Dist = Dist;
            SmoothedPath.GetPointByDist(&TrajPt);
            //printf("TRAJEKTORIJA: %f, %f , %f \n", TrajPt.Time, TrajPt.x, TrajPt.y);
            printf("TRAJEKTORIJA: %f \n", TrajPt.Angle);
            p.x = TrajPt.x;
            p.y = TrajPt.y;
            path_data.points.push_back(p);
        }
        pub_path_result.publish(path_data);
        completed = SmoothedPath.LogTrajectoryByDist("smoothedPath.txt", Delta);      //spremanje putanje u txt za analizu u Matlabu
        
            
        //printf("zavrseno %d:", completed);
        ROS_INFO("\n Saving smoothedPath.txt completed!!! Saljemo %d", path_data.points.size());
        if (completed == 1){
            C2DClothoidApp::DestroyLookupTable();
        }
   // }  
    //void main()
    //{
        // CTrajTrackingRegulator_Nonlinear Regulator;
        // CTrajTrackingRegulator_Nonlinear::SParms Parms;
        
        // //parametri ako se ne postave ostaju defaultni koji su definirani u TrajTrackingController.h
        // Parms.pTraj = nullptr;
        // Parms.SamplingPeriod = (1/80);
        // Parms.b = 60;
        // Parms.zeta = 0.7;
        // Parms.vMax = 2;
        // Parms.wMax = 2/(0.067/2);

        // Parms.LinAccelMax = 1;
        // Parms.AngAccelMax = 30;
        // Parms.nSamplesDelay = 0;



        // if(!Regulator.SetParms(&Parms))
        //     return;

        // for(int i=0; i<2400; i++)
        // {
        //     CMobileRobot::SRobotState MeasuredState;
        //     CMobileRobot::SRobotState RefState;

        //     MeasuredState.x = TrajPt.x;
        //     MeasuredState.y = TrajPt.y;
        //     MeasuredState.Angle = TrajPt.Angle;

        //     Regulator.GetRefValues(&MeasuredState, &RefState);

        //     // Posalji reference robotu
        //     float vRef = RefState.Velocity;
        //     float wRef = RefState.AngVelocity;

            
        //}
    //} 
    }

};
}
PLUGINLIB_EXPORT_CLASS(nodelet_path_smoothing::path_smoothing, nodelet::Nodelet);
