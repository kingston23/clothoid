# TWD Planner and Smoothing algorithm

This README explains using the TWD Planner and Smoothing algorithm.


### SET ROS WORKSPACE FOR SMOOTHING ALGORITHM ###

* Source ROS environment
`source /opt/ros/melodic/setup.bash`

* Create and build a catkin workspace:

`mkdir catkin_ws`

`cd catkin_ws`

`mkdir src`

`cd src`

`catkin_init_workspace`

`git clone git@bitbucket.org:unizg-fer-lamor/clothoid.git`

`cd ..`


* Compile the source by typing `catkin_make` in the root folder of your catkin workspace


* set up the path:

`cd devel`

`source setup.bash`


### RUN SMOOTHING ALGORITHM ###

Set goal, current position and path:
`roslaunch nodelet_talker test1.launch`

Run smoothing algoritm:
`roslaunch nodelet_talker talker.launch`

The output will be saved in `~/.ros` folder with name `smoothedPath.txt`. The smoothing algorithm gives a smoothed path as output states of the form [x y] without theta (theta isn't calculated in clothoids segmets but it can be calculated with atan2(y,x) where x and y are output states).


### SMOOTHED PATH VISUALIZATION ###
The package `smoothedpathlistener` is for visaulization of smoothed path in Rviz. 

Change `startpathlistener.launch` form `pathtwdlistener/launch/` like this:

```
<launch>

    <node name="pathls" pkg="pathtwdlistener" type="pathls" output="screen">
    <remap from="fix_path" to="input_talker/fix_path"/>
    </node>

<!--node name="rviz" pkg="rviz" type="rviz" args="-d $(find pathtwdlistener)/twdsim.rviz" /--> 

</launch>

```

Create the `startpathlistener.launch` file and put it into `smoothedpathlistener/launch` folder:

```
<launch>

    <node name="smoothedpathls" pkg="smoothedpathlistener" type="smoothedpathls" output="screen">
    </node>

<node name="rviz" pkg="rviz" type="rviz" args="-d $(find smoothedpathlistener)/twdsim.rviz" /> 

</launch>

```

For the visualization of the smoothed path run:
`roslaunch smoothedpathlistener startpathlistener.launch`


### Getting started -> TWD + Smoothing

* Put the whole twdsrc folder into your catkin workspace's src folder. The folder `twdplanner` is the TWD planner, while the folder `pathtwdlistener` is the simple subscriber to the output message of the TWD planner. 
* Compile the source by typing `catkin_make` in the root folder of your catkin workspace
* Open three terminals and start:

1. `roslaunch twdplanner ushapeamcltest.launch`

2. `roslaunch twdplanner starttwd.launch`

3. `roslaunch pathtwdlistener startpathlistener.launch`

4. `roslaunch nodelet_talker test1.launch`

5. `roslaunch nodelet_talker talker.launch`

6. `roslaunch smoothedpathlistener startpathlistener.launch`

Click on the rviz window on `2D Nav Goal` button and then click somewhere in the free space of the loaded environment. The robot should start to move to the selected goal point. The TWD path should be plotted on the rviz window.



### Examining the log files in Matlab

TWD planner saves all the data to files when the robot reaches the goal. These files are saved in `~/.ros/logger` folder. Copy and paste m-scripts from `twdplanner/logger` folder to that one. There are 3 scripts:

1. crtanjePutanjereplan.m - draws initial path and all replanning path with the optimal area of equal costs D* path

2. putanjeintegeritwd.m - draws D and TWD path paused during execution

3. izracuntwd.m - draws execution times of D, reverse D, and TWD path

4. plotanjeTrajDodatno.m - draws TWD path, Smoothed path and trajectory from logger




### Explanation of plotanjeTrajDodatno.m
Import smoothed path data and save it into vectors x,y,th:

```
%opts = detectImportOptions('SampledTraj.txt'); -> SampledTraj.txt is smoothed trajectory from test example without ROS
opts = detectImportOptions('smoothedPath.txt'); -> smoothedpath.txt is smoothed trajectory form ROS example (output from `roslaunch nodelet_talker talker.launch`)

opts = setvartype(opts,1:12,'double');
t = readtable('smoothedPath.txt', opts);
figure()
x= table2array(t(:,8));
y= table2array(t(:,9));
th = table2array(t(:,10));
th_ = th*180/pi;
table1 = [x y th_];

plot(x,y, 'b','Linewidth', 1.5);
hold on

```

Drawing goal points after filtering and inserting additional ones to align the robot orientation with the orientation of the first segment:

```
%% crtanje tocaka nakon filtiranja i ubacivanja dodatnih zbog orijentacije
opts2 = detectImportOptions('goal_position.txt');
opts2 = setvartype(opts2,1:2,'double');
goaltable = readtable('goal_position.txt', opts2);
xx = table2array(goaltable(:,1));
yy = table2array(goaltable(:,2));
plot(xx,yy, 'r--', 'Linewidth', 0.8);
hold on

% legend('Smoothed Path', 'TWD*', 'start', 'goal');
plot(x(1),y(1),'g*');
hold on
plot(x(end),y(end),'k*');
hold on
% text(x(1)+0.02,y(1)+0.02,'start');
% text(x(end)+0.02,y(end)+0.02,'goal');

xlabel('x');
ylabel('y');

```

Drawing data from logger:

```

%% crtanje logera
hold on

WH_gridmap_x=load('wh_gridmap_x.dat');
	WH_gridmap_y=load('wh_gridmap_y.dat');
     WH_gridmap_static_cell=load('wh_gridmap_static_cell.dat');
    metric=0.001;
WH_gridmap_x=WH_gridmap_x*metric;
    WH_gridmap_y=WH_gridmap_y*metric;
%     figure(1);
    hold on;%grid on;
%     axis equal tight;
map = [1 1 1; 0.7, 0.7, 0.7];
colormap(map)
novex=[];novey=[];starex=[];starey=[];
for i=1:max(size(WH_gridmap_x))
                if(WH_gridmap_static_cell(i)==1)
                    starex=[starex;WH_gridmap_x(i)];
                    starey=[starey;WH_gridmap_y(i)];
               else
                    novex=[novex;WH_gridmap_x(i)];
                    novey=[novey;WH_gridmap_y(i)];
             end
end
plot(starex,starey,'g.');
%  plot(novex,novey,'m.');
WH_globalna_putanja_x=load('robot_globalna_putanja_x.dat');%odvozena putanja
WH_globalna_putanja_y=load('robot_globalna_putanja_y.dat');
WH_globalna_putanja_th=load('robot_globalna_putanja_th.dat');
fakeloc_x=load('fakelocrobot_globalna_putanja_x.dat');%odvozena putanja iz fakelocalize-a
fakeloc_y=load('fakelocrobot_globalna_putanja_y.dat');
fakeloc_th=load('fakelocrobot_globalna_putanja_th.dat');
goal=load('wh_goal_position.dat');

%plotanje putanja
%  plot(WH_globalna_putanja_x*metric,WH_globalna_putanja_y*metric,'r');
 plot(WH_globalna_putanja_x*metric,WH_globalna_putanja_y*metric,'r.-');
%  plot(goal(1)*metric,goal(2)*metric,'b*');
%  plot(fakeloc_x*metric,fakeloc_y*metric,'g.');

    %plotanje pocetne i konacne koordinate robota
    %nacrtajmo robota polumjer robota (mm)
	rr=260*metric;
	fi=linspace(0,2*pi,30);
	x_temp=WH_globalna_putanja_x(1)*metric;y_temp=WH_globalna_putanja_y(1)*metric;th_temp=WH_globalna_putanja_th(1);
	for i=1:30
		x_circle(i)=x_temp+rr*cos(fi(i));
		y_circle(i)=y_temp+rr*sin(fi(i));
	end
	plot(x_circle,y_circle,'k--');
	plot([x_temp x_temp+rr*cos(th_temp)], [y_temp y_temp+rr*sin(th_temp)],'k--');
 
	x_temp=WH_globalna_putanja_x(end)*metric;y_temp=WH_globalna_putanja_y(end)*metric;th_temp=WH_globalna_putanja_th(end);
	for i=1:30
		x_circle(i)=x_temp+rr*cos(fi(i));
		y_circle(i)=y_temp+rr*sin(fi(i));
	end
	plot(x_circle,y_circle,'k--');
	plot([x_temp x_temp+rr*cos(th_temp)], [y_temp y_temp+rr*sin(th_temp)],'k--');


    
% for i=1:length(putic(:,1))
%     plot(putic(i,1),putic(i,2),'dm')
%     pause
% end
    %figure(1);
    ylabel('y [m]', 'fontsize',16,'fontname', 'times');
    %title('Robot environment', 'fontsize', 18,'fontname', 'times');
    xlabel('x [m]', 'fontsize',16,'fontname', 'times');
    h=gca;
    set(h,'fontsize',10,'fontname','times','box', 'on');
    axminx=min(min(starex),min(novex));
    axminy=min(min(starey),min(novey));
    axmaxx=max(max(starex),max(novex));
    axmaxy=max(max(starey),max(novey));
%     axis([-26 6 -10 10])   
% axis([axminx axmaxx axminy axmaxy])
starttxt=[WH_globalna_putanja_x(1) WH_globalna_putanja_y(1)]*metric;
    goaltxt=[WH_globalna_putanja_x(end) WH_globalna_putanja_y(end)]*metric;
text(starttxt(1),starttxt(2),'start','Interpreter','latex')%s h
text(goaltxt(1),goaltxt(2),'goal','Interpreter','latex')%s h
axis equal tight
% print('-depsc2','ime.eps')%radi super bounding boxove

```
