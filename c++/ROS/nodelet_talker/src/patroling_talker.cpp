
#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <std_msgs/String.h>

// basic file operations
#include <iostream>
#include <fstream>
#include <iterator>

#include <stdio.h>
#include "nodelet_talker/InputData.h"
#include "nodelet_talker/Patroling.h"
#include "nodelet_talker/patrolingGoals.h"

#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "tf/tf.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Quaternion.h"

#include <math.h>
#include <vector>
#include <list>
#include <sstream>
#include <inttypes.h>
#include <memory>

#include "Math/Numeric.h"
#include "Geom/GeometryBase.h"
#include "Traj/2DTrajectoryBase.h"
#include "Geom/2DPoint.h"



namespace nodelet_talker
{
class Talker: public nodelet::Nodelet
{
public:
      double robottfx, robottfy, robottfth, goalx, goaly, goalth;
      ros::Publisher pub;
      ros::Publisher pub_laps;
      ros::Subscriber sub_path;
      ros::Subscriber amcl_pose_sub;
      ros::Subscriber goal_pose_sub;
      
    virtual void onInit() {  
      ros::NodeHandle& private_nh = getPrivateNodeHandle();
      NODELET_DEBUG("Initialized the Nodelet");

      pub = private_nh.advertise<InputData>("msg_in", 10);
      pub_laps = private_nh.advertise<Patroling>("msg_laps", 10);
      sub_path = private_nh.subscribe<patrolingGoals>("patroling_path",1,&Talker::PatrolingPathCallback, this); 

      amcl_pose_sub = private_nh.subscribe("/amcl_pose", 1, &Talker::globalposeCallback, this); // current pose 
      goal_pose_sub = private_nh.subscribe("/move_base_simple/goal", 1, &Talker::goal_poseCallback, this); // goal pose

    }
 
      void globalposeCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
      {
        robottfx = msg->pose.pose.position.x;
        robottfy = msg->pose.pose.position.y;
        robottfth = tf::getYaw(msg->pose.pose.orientation);
      }

    void goal_poseCallback(const geometry_msgs::PoseStamped::ConstPtr& simple_g)
    {
      goalx = simple_g->pose.position.x;
      goaly = simple_g->pose.position.y;
      goalth = tf::getYaw(simple_g->pose.orientation);
      ROS_INFO("Goal position: x = %f, y= %f. Goal orientation: %f deg", goalx, goaly, goalth*180/M_PI);
    }

    void PatrolingPathCallback(const nodelet_talker::patrolingGoalsConstPtr& pMsg)
    {
        InputData data; //nova instanca poruke
        Patroling data_laps;
        
        int pathEnd_point = pMsg->points.size(); 
        ROS_INFO("MSG: [%f]", pathEnd_point);
        int i=1;
        while (i<2){
        //postavljanje vrijednosti pokazivača na ulazne parametre
        C2DTrajectoryBase::S2DTrajPoint* goalState = new C2DTrajectoryBase::S2DTrajPoint();
        goalState->x = pMsg->points[pathEnd_point-1].x;
        goalState->y = pMsg->points[pathEnd_point-1].y;
        goalState->Angle = goalth;
        goalState->Curv = 0.0f;
        ROS_INFO("goal msg: [%f %f %f %f]", goalState->x, goalState->y, goalState->Angle, goalState->Curv);
        data.GoalStatePtr=(long)goalState;

        C2DTrajectoryBase::S2DTrajPoint* currentState = new C2DTrajectoryBase::S2DTrajPoint();
        currentState->x = robottfx;
        currentState->y = robottfy;
        currentState->Angle = robottfth;
        //currentState->Angle = 0.0f;
        currentState->Curv = 0.0f;
        ROS_INFO("current state msg: [%f %f %f %f]", currentState->x, currentState->y, currentState->Angle, currentState->Curv);
        data.CurrentStatePtr=(long)(currentState); 
        
        std::vector<Geom::C2DPoint>* PWLinearPath = new std::vector<Geom::C2DPoint >();
        Geom::C2DPoint p;
        if (pMsg->points[0].x != robottfx && pMsg->points[0].y != robottfy){
          p.x=robottfx;
          p.y=robottfy;
          PWLinearPath->push_back(p);
        }
        for (int i=0; i<pMsg->points.size(); i++){
                   p.x=pMsg->points[i].x;
                   p.y=pMsg->points[i].y;
                   ROS_INFO("Tocka na putu talker %d: [%f %f]", i, p.x, p.y);
                   PWLinearPath->push_back(p);
        }
        data.PWLinearPathPtr=(long)(PWLinearPath);
        
        pub.publish(data);//objavljivanje poruke na temu 

        data_laps.lapsPatroling = 2;        
        pub_laps.publish(data_laps); 
        ROS_INFO("Broj krugova za patroling %d", data_laps.lapsPatroling);

        i++;
        }

      };

};
} // namespace nodelet_talker
PLUGINLIB_EXPORT_CLASS(nodelet_talker::Talker,nodelet::Nodelet);

