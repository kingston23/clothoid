
#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <std_msgs/String.h>

#include <stdio.h>
#include "nodelet_talker/InputData.h"
#include "nodelet_talker/Patroling.h"

#include <math.h>
#include <vector>
#include <list>
#include <sstream>
#include <inttypes.h>
#include <memory>

#include "Math/Numeric.h"
#include "Geom/GeometryBase.h"
#include "Traj/2DTrajectoryBase.h"
#include "Geom/2DPoint.h"

// basic file operations
#include <iostream>
#include <fstream>
#include <iterator>

namespace nodelet_talker
{
class Talker: public nodelet::Nodelet
{
public:
    virtual void onInit() {  
        ros::NodeHandle& private_nh = getPrivateNodeHandle();
        NODELET_DEBUG("Initialized the Nodelet");
        ros::Publisher pub = private_nh.advertise<InputData>("msg_in", 10);
        //ros::Publisher pub_laps = private_nh.advertise<Patroling>("msg_laps", 10);

        InputData data; //nova instanca poruke
        //Patroling data_laps;
        // int i=1;
        // while(i<3){
        //     data_laps.lapsPatroling = 2;        
        //     pub_laps.publish(data_laps); 
        //     ROS_INFO("Broj krugova za patroling %d", data_laps.lapsPatroling);
        //     i++;
        // }
        int i=0;
        while (i<3){
        //postavljanje vrijednosti pokazivača na ulazne parametre
        C2DTrajectoryBase::S2DTrajPoint* goalState = new C2DTrajectoryBase::S2DTrajPoint();
        goalState->x=1.0f;
        goalState->y=1.0f;
        goalState->Angle = 0.0f;
        goalState->Curv = 0.0f;
        ROS_INFO("goal msg: [%f %f %f %f]", goalState->x, goalState->y, goalState->Angle, goalState->Curv);
        data.GoalStatePtr=(long)goalState;

        C2DTrajectoryBase::S2DTrajPoint* currentState = new C2DTrajectoryBase::S2DTrajPoint();
        currentState->x = 1.0f;
        currentState->y = 1.0f;
        currentState->Angle = 0.0f;
        currentState->Curv = 0.0f;
        ROS_INFO("current state msg: [%f %f %f %f]", currentState->x, currentState->y, currentState->Angle, currentState->Curv);
        data.CurrentStatePtr=(long)(currentState);

  //vector<item>* myVec = new vector<item>();  
        
        std::vector<Geom::C2DPoint>* PWLinearPath = new std::vector<Geom::C2DPoint >();
        // PWLinearPath->emplace_back(currentState->x, currentState->y);
        // PWLinearPath->emplace_back(1, 0);
        // PWLinearPath->emplace_back(1, 1);
        // PWLinearPath->emplace_back(2, 1);
        // PWLinearPath->emplace_back(3, 0);
        // PWLinearPath->emplace_back(goalState->x, goalState->y);
        //Geom::C2DPoint P0(1.1f,1.0f), P1(1.2f,2.5f), P2(2.2f,3.5f), P3(2.5f,2.7f), P4(4.5f,2.7f), P5(4.1f,2.95f);
        Geom::C2DPoint P0(currentState->x,currentState->y), P1(5.0f,1.0f), P2(3.0f,3.4641f), P3(goalState->x,goalState->y); //, P3(1.0f,1.0f), P4(4.5f,2.7f), P5(4.1f,2.95f);

        PWLinearPath->push_back(P0);
        PWLinearPath->push_back(P1); 
        PWLinearPath->push_back(P2);
        PWLinearPath->push_back(P3);

        //PWLinearPath->push_back(P4);
        //PWLinearPath->push_back(P5);
        data.PWLinearPathPtr=(long)(PWLinearPath);


        pub.publish(data);//objavljivanje poruke na temu 
        i++;
        }

      };

};
} // namespace nodelet_talker
PLUGINLIB_EXPORT_CLASS(nodelet_talker::Talker,nodelet::Nodelet);

