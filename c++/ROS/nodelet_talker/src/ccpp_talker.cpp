
#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <std_msgs/String.h>

#include <stdio.h>
#include "nodelet_talker/InputData.h"

#include <math.h>
#include <vector>
#include <list>
#include <sstream>
#include <inttypes.h>
#include <memory>

#include "Math/Numeric.h"
#include "Geom/GeometryBase.h"
#include "Traj/2DTrajectoryBase.h"
#include "Geom/2DPoint.h"

#include "geometry_msgs/Point.h"
#include "geometry_msgs/Quaternion.h"

#include <twdplanner/Path.h>
#include <pathtwdlistener/FilteredPath.h>
#include <pathtwdlistener/FixOrientation.h>
#include <coverageana/Goals.h>

#include "moj.h"   //DStar.h, Params.h, Planner.h


// basic file operations
#include <iostream>
#include <fstream>
#include <iterator>

namespace nodelet_talker
{
class Talker: public nodelet::Nodelet
{
public:
    //int prolaz =0;
    double robottfx, robottfy, robottfth, goalx, goaly, goalth;
    ros::Subscriber amcl_pose_sub;
    ros::Publisher pub_twd_msg;
    ros::Subscriber sub_pathtwd;
    ros::Subscriber goal_pose_sub;


    virtual void onInit() {  
        ros::NodeHandle& private_nh = getPrivateNodeHandle();
        NODELET_DEBUG("Initialized the Nodelet");
        pub_twd_msg = private_nh.advertise<InputData>("msg_in", 10);
        //sub_pathtwd = private_nh.subscribe<pathtwdlistener::FixOrientation>("fix_path",1,&Talker::FixPathCallback, this); //za TWD+smoothing
        sub_pathtwd = private_nh.subscribe<coverageana::Goals>("filtered_path",1,&Talker::FixPathCallback, this); // za STC+smoothing

        amcl_pose_sub = private_nh.subscribe("/amcl_pose", 1, &Talker::globalposeCallback, this); // current pose 
        goal_pose_sub = private_nh.subscribe("/move_base_simple/goal", 1, &Talker::goal_poseCallback, this); // goal pose

    }
 
      void globalposeCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
      {
        robottfx = msg->pose.pose.position.x;
        robottfy = msg->pose.pose.position.y;
        robottfth = tf::getYaw(msg->pose.pose.orientation);
      }

    void goal_poseCallback(const geometry_msgs::PoseStamped::ConstPtr& simple_g){
      goalx = simple_g->pose.position.x;
      goaly = simple_g->pose.position.y;
      goalth = tf::getYaw(simple_g->pose.orientation);
      ROS_INFO("Goal position: x = %f, y= %f. Goal orientation: %f deg", goalx, goaly, goalth*180/M_PI);
    }

      //void FixPathCallback(const pathtwdlistener::FixOrientationConstPtr& pMsg)  // za TWD
      void FixPathCallback(const coverageana::GoalsConstPtr& pMsg)   // za STC

      {
        int pathEnd_point = pMsg->points.size(); 

        InputData data; //nova instanca poruke
        	ROS_INFO("DOBIO FILTRIRANE TOCKE");

        int i=1;
        while (i<2){
        //postavljanje vrijednosti pokazivača na ulazne parametre
        C2DTrajectoryBase::S2DTrajPoint* goalState = new C2DTrajectoryBase::S2DTrajPoint();
        goalState->x = pMsg->points[pathEnd_point-1].x;
        goalState->y = pMsg->points[pathEnd_point-1].y;
        goalState->Angle = goalth;
        goalState->Curv = 0.0f;
        ROS_INFO("goal msg: [%f %f %f %f]", goalState->x, goalState->y, goalState->Angle, goalState->Curv);
        data.GoalStatePtr=(long)goalState;

        C2DTrajectoryBase::S2DTrajPoint* currentState = new C2DTrajectoryBase::S2DTrajPoint();
        currentState->x = pMsg->points[0].x;
        currentState->y = pMsg->points[0].y;
        currentState->Angle = robottfth;
        //currentState->Angle = 0.0f;
        currentState->Curv = 0.0f;
        ROS_INFO("current state msg: [%f %f %f %f]", currentState->x, currentState->y, currentState->Angle, currentState->Curv);
        data.CurrentStatePtr=(long)(currentState); 
        
        std::vector<Geom::C2DPoint>* PWLinearPath = new std::vector<Geom::C2DPoint >();
        Geom::C2DPoint p;
             for (int i=0; i<pMsg->points.size(); i++){
                   p.x=pMsg->points[i].x;
                   p.y=pMsg->points[i].y;
                   ROS_INFO("Tocka na putu talker %d: [%f %f]", i, p.x, p.y);
                   PWLinearPath->push_back(p);
             }
        data.PWLinearPathPtr=(long)(PWLinearPath);
        //ROS_INFO("goal x,y current x,y: [%f %f %f %f]", goalState->x, goalState->y, currentState->x, currentState->y);

        pub_twd_msg.publish(data);//objavljivanje poruke na temu 
        i++;
        //prolaz++;
        //} //prolaz 
      }
      }

};
} // namespace nodelet_talker
PLUGINLIB_EXPORT_CLASS(nodelet_talker::Talker,nodelet::Nodelet);

