#!/usr/bin/env python
# license removed for brevity
# Simple move_base command publisher which recieves commands via zmq

import rospy

import zmq
import msgpack
from geometry_msgs import *
import tf 
import numpy 
import time

from nodelet_talker.msg import *

class Go():
    def __init__(self):
	# Initializes a rospy node to let the SimpleActionClient publish and subscribe
        rospy.init_node('move_base_client_py')
        rate = rospy.Rate(10) # 10hz
        a = 0
        port = "5555"

            
        def decode(obj):
            if b"goal" in obj: # b is for bytes
                obj = {"goal": obj[b"goal"], "task": obj[b"task"]}
            return obj

        listener = tf.TransformListener()
        pub = rospy.Publisher('patroling_path', patrolingGoals, queue_size=10)  
            
        #while True:       
	          # Socket to talk to server
        context = zmq.Context()
        self.socket = context.socket(zmq.SUB)
        self.socket.connect ("tcp://localhost:%s" % port)
        #self.socket.connect("tcp://10.129.1.191:%s" % port)
        #self.socket.connect("tcp://192.168.1.103:%s" % port)
	# Subscribe to identifier
        topicfilter = "10010"
        self.socket.setsockopt(zmq.SUBSCRIBE, topicfilter)
        print ("setsocktopt")
              
        self.listener = tf.TransformListener()
              
	          # what to do if shut down (e.g. ctrl + C or failure)
        rospy.on_shutdown(self.shutdown)

        goal = patrolingGoals()
        #a = 0
        while True:
          [adress, contents] = self.socket.recv_multipart() # receive
          
          msg = msgpack.unpackb(contents, object_hook=decode) # deserialize
          print msg
	        # Creates a new goal with the MoveBaseGoal constructor


          for i in msg["goal"]:
            p = Point()
            p.x = numpy.longdouble(i[0])
            p.y = numpy.longdouble(i[1])
            goal.points.append(p)
            #print(p.x, p.y)

          #print(goal.points)
          #print a         
          #a=a+1

          pub.publish(goal)
          print("publish msg:", goal.points)
          del goal.points[:]
          rate.sleep() 

    def shutdown(self):
        rospy.loginfo("Stop")
# If the python node is executed as main process (sourced directly)
if __name__ == '__main__':
    try:
        Go()
    except rospy.ROSInterruptException:
        rospy.loginfo("Navigation test finished.") 
