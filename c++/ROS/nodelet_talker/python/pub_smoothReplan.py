#!/usr/bin/env python
# license removed for brevity
# Simple move_base command publisher which recieves commands via zmq

import rospy

import zmq
import msgpack
import tf 
import numpy 
import time

class Patrolling(): 
  def __init__(self):
    port = "5555"
    print (port)
    context = zmq.Context() # initialize context
    self.socket = context.socket(zmq.PUB) # define as publisher
    self.socket.bind("tcp://*:%s" % port) # bind to socket 
    print ("setsockt bind")
    time.sleep(1)
    i=1
    while i<2: 
      message = {                 
                             # "goal": [a[0], a[1], 0,0,0,0,1],
      #"goal": [(5.5,4.2),(6.2,6),(7.5,9)],
      #"goal": [(4.5,4.2),(6.2,6),(7.5,4),(9.5,1)],
      "goal": [(9.5,7.2)],
      "task": [33, "T"],
      }    
      print(message)
      packed_message = msgpack.packb(message) # serialize message
      self.socket.send_multipart([b"10010", packed_message]) # send
      #print(packed_message) 
      #time.sleep(1)
      i= i+1

    def shutdown(self):
        rospy.loginfo("Stop")
# If the python node is executed as main process (sourced directly)
if __name__ == '__main__':
    try:
        Patrolling()
    except rospy.ROSInterruptException:
        rospy.loginfo("Navigation test finished.")
