#!/usr/bin/env python
# license removed for brevity
# Simple move_base command publisher which recieves commands via zmq

import rospy

import zmq
import msgpack
import tf 
import numpy 
import time
from nodelet_talker.msg import *
from geometry_msgs import *

class Patrolling(): 
  def __init__(self):
    port = "5555"
    print (port)
    context = zmq.Context() # initialize context
    self.socket = context.socket(zmq.PUB) # define as publisher
    self.socket.bind("tcp://*:%s" % port) # bind to socket 
    print ("setsockt bind")
    time.sleep(1)
    i=1
    while i<2: 

      message = {                 
                             # "goal": [a[0], a[1], 0,0,0,0,1],
      #"goal": [3, 5,0,0,0,0,1],
      "goal": [4.5, 4.6,0,0,0,0,1],
      "task": [33, "T"],
      }    
      print(message)
      packed_message = msgpack.packb(message) # serialize message
      self.socket.send_multipart([b"10010", packed_message]) # send
      #print(packed_message) 
      #time.sleep(1)

      message = {                 
                             # "goal": [a[0], a[1], 0,0,0,0,1],
      #"goal": [3, 5,0,0,0,0,1],
      "goal": [6, 8,0,0,0,0,1],
      "task": [33, "T"],
      }    
      print(message)
      packed_message = msgpack.packb(message) # serialize message
      self.socket.send_multipart([b"10010", packed_message]) # send
      #print(packed_message) 
      #time.sleep(1)

      message = {                 
                             # "goal": [a[0], a[1], 0,0,0,0,1],
      #"goal": [3, 5,0,0,0,0,1],
      "goal": [7, 8.5,0,0,0,0,1],
      "task": [33, "T"],
      }    
      print(message)
      packed_message = msgpack.packb(message) # serialize message
      self.socket.send_multipart([b"10010", packed_message]) # send
      #print(packed_message) 
      #time.sleep(1)
      message = {                 
                             # "goal": [a[0], a[1], 0,0,0,0,1],
      #"goal": [3, 5,0,0,0,0,1],
      "goal": [10, 9.5,0,0,0,0,1],
      "task": [33, "T"],
      }    
      print(message)
      packed_message = msgpack.packb(message) # serialize message
      self.socket.send_multipart([b"10010", packed_message]) # send
      #print(packed_message) 
      #time.sleep(1)
      i= i+1

    def shutdown(self):
        rospy.loginfo("Stop")
# If the python node is executed as main process (sourced directly)
if __name__ == '__main__':
    try:
        Patrolling()
    except rospy.ROSInterruptException:
        rospy.loginfo("Navigation test finished.")
