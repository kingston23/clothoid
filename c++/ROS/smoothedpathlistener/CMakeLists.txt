cmake_minimum_required(VERSION 2.4.6)
project(smoothedpathlistener)

#For the shared library:
set ( PROJECT_LINK_LIBS libpathSmoothing.so )
link_directories( ~/catkin_ws/src/clothoid/c++/path_lib/build )
 
include_directories(~/catkin_ws/src/clothoid/c++/path_lib/include)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++0x")
#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

find_package(pathtwdlistener REQUIRED)
find_package(twdplanner REQUIRED)

find_package(catkin REQUIRED COMPONENTS
  nodelet
  std_msgs
  nav_msgs
  geometry_msgs
  message_generation
  roscpp
  tf
)

## Generate messages in the 'msg' folder
add_message_files(
   FILES
   OutputPath.msg
  # Point.msg
)

generate_messages(
   DEPENDENCIES
   std_msgs
   nav_msgs
   geometry_msgs
)

catkin_package(
  CATKIN_DEPENDS std_msgs message_runtime nodelet_path_smoothing nodelet
  DEPENDS path
)

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${twdplanner_INCLUDE_DIRS}
  ${pathtwdlistener_INCLUDE_DIRS}
  include
)

add_executable(smoothedpathls src/main.cpp)
#add_executable(smoothedpathls src/main_dobivam_pointer.cpp)
target_link_libraries(smoothedpathls ${catkin_LIBRARIES} ${twdplanner_LIBRARIES} ${pathtwdlistener_LIBRARIES})
add_dependencies(smoothedpathls smoothedpathlistener_generate_messages_cpp)





