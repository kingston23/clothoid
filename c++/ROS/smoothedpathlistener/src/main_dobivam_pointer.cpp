#include <sys/time.h>
#include <iostream>
#include <string>
#include "ros/ros.h"
//#include <algorithm>

//#include <twdplanner/Point.h>
//#include <twdplanner/Path.h>
#include "smoothedpathlistener/OutputPath.h"
#include "nodelet_path_smoothing/OutputData.h"

#include <visualization_msgs/Marker.h>

#include "Traj/PathSmoothing.h"


class VisualizationPublisherTWD
{
protected:
  // Our NodeHandle
  ros::NodeHandle nh_;
  std::string target_frame_;

public:
	ros::Publisher pathvis_pub;
	ros::Subscriber pathtwd_sub;
  
  double Delta = 0.05;

  typedef double T;

  visualization_msgs::Marker pathvis;

  VisualizationPublisherTWD(ros::NodeHandle n) :
      nh_(n),  target_frame_("map") 
  {

 	pathtwd_sub = nh_.subscribe<nodelet_path_smoothing::OutputData>("/output_path/result",1,&VisualizationPublisherTWD::pathCallback, this);
	pathvis_pub = nh_.advertise<visualization_msgs::Marker>("/path_markerListener",10);

    pathvis.header.frame_id = target_frame_;
    pathvis.header.stamp = ros::Time::now();
    pathvis.ns =  "smoothedpathlistener";
    pathvis.action = visualization_msgs::Marker::ADD;
    pathvis.pose.orientation.w  = 1.0;
    pathvis.type = visualization_msgs::Marker::LINE_STRIP;
    pathvis.scale.x = 0.05; 
    pathvis.scale.y = 0.05; 
    pathvis.color.r = 0.6;
    pathvis.color.g = 0.;
    pathvis.color.b = 0.8;
    pathvis.color.a = 1.0;
      

  }

  void visualizationduringmotion();
  void pathCallback(const nodelet_path_smoothing::OutputDataConstPtr& pMsg);

};



int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "smoothedpathlistener");
  ros::NodeHandle nh;
  VisualizationPublisherTWD visualTWD(nh);


  ros::Rate rate(10.0);


  while (nh.ok()) {


    ros::spinOnce(); 

	  visualTWD.visualizationduringmotion();	
    
    
	  rate.sleep();
  }
  return 0;
}


void VisualizationPublisherTWD::visualizationduringmotion(){

			pathvis_pub.publish(pathvis);

}


void VisualizationPublisherTWD::pathCallback(const nodelet_path_smoothing::OutputDataConstPtr& pMsg)
{
  //OutputPath path_data;
  geometry_msgs::Point p; 
  ROS_INFO("usao u callback");
  // path_data.points.clear();

  pathvis.points.clear();
  CTrajCompositePathProfile* pSmoothedPath = (CTrajCompositePathProfile*) pMsg->SmoothedPathPtr;
 
  C2DTrajectoryBase::S2DTrajPoint TrajPt;
  TrajPt.SetAllZero();
  ROS_INFO("1: %f", (&pSmoothedPath));
  T Dist1 = pSmoothedPath->GetBeginDist();
  ROS_INFO("2: ");
  T Dist2 = pSmoothedPath->GetEndDist();
    ROS_INFO("3: ");

  // for(T Dist = Dist1; Dist <= Dist2; Dist += Delta)
  //   {
  //     TrajPt.Dist = Dist;
  //     ROS_INFO("4: ");
  //     pSmoothedPath.GetPointByDist(&TrajPt);
  //     printf("TRAJEKTORIJA: %f, %f \n", TrajPt.x, TrajPt.y);
  //     p.x = TrajPt.x;
  //     p.y = TrajPt.y;
  //     pathvis.points.push_back(p);
  //   }

  //ROS_INFO("CISTIMO SVEEEEEE");

  //geometry_msgs::Point p; 
  //ROS_INFO("Velicina pMsg->points %d: ", pMsg->points.size());

	// for (int i=0; i<path_data.points.size(); i++){
	// 	p.x=path_data->points[i].x;
	// 	p.y=path_data->points[i].y;
  //   //ROS_INFO("Tocka na putu %d: [%f %f]", i, p.x, p.y);

	// 	pathvis.points.push_back(p);
	// }


}



