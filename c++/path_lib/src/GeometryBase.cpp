#include "Geom/GeometryBase.h"

namespace Geom
{
// Definicija za PI
const CGeometryBase::T CGeometryBase::PI = 3.1415926535897932384626433832795;

const CGeometryBase::T CGeometryBase::eps = (T)std::numeric_limits<T>::epsilon() * 100;

}; // namespace Geom
