#include "math.h"
#include "Traj/TrajTrackingController.h"

// Regulatori
#include "Traj/MobileRobotTrackRegulator1.h"
#include "Traj/TrajTrackingController.h"
#include "Traj/TrajTrackingRegulator_Egypt.h"
#include "Traj/TrajTrackingRegulator_Linear.h"
#include "Traj/TrajTrackingRegulator_Nonlinear.h"
#include "Traj/TrajTrackingRegulator_PI.h"
#include "Traj/TrajTrackingRegulator_Toni.h"

//_______________________________________________________________________________________________________________________________________
//
// Konstruira regulator zeljenog tipa
// Ovo radi za float jer ne znam napraviti za opceniti tip :(
//_______________________________________________________________________________________________________________________________________
CTrajTrackingController* ConstructTrajTrackingController(CTrajTrackingController::ETrajTrackingController ControllerType)
{
    switch(ControllerType)
    {
    case CTrajTrackingController::TTC_Linear:
        return new CTrajTrackingRegulator_Linear;

    case CTrajTrackingController::TTC_Nonlinear:
        return new CTrajTrackingRegulator_Nonlinear;

    case CTrajTrackingController::TTC_MPC:
        assert(0);
        return nullptr; // new CTrajTrackingRegulator_MPC; // ovog ne podrzavamo jer trazi dodatne biblioteke

    case CTrajTrackingController::TTC_MPC_IM:
        assert(0);
        return nullptr; // new CTrajTrackingController_MPC_IM; // ovog ne podrzavamo jer se ne kompajlira dobro

    case CTrajTrackingController::TTC_PI:
        return new CTrajTrackingRegulator_PI;

    case CTrajTrackingController::TTC_Moj:
        return new CMobileRobotTrackRegulator1;

    case CTrajTrackingController::TTC_Moj2:
        return new CTrajTrackingRegulator_Moj2;

    case CTrajTrackingController::TTC_Egypt:
        return new CTrajTrackingRegulator_Egypt;

    case CTrajTrackingController::TTC_Toni:
        return new CTrajTrackingRegulator_Toni;

    default:
        assert(0); // Neki nepoznati, nije dodan u gornju listu?
        return nullptr;
    }
}
