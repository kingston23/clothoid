#include "Traj/MyDynamicWindow.h"

#include "Math/AngleRange.h"
#include "Math/Constants.h"
#include "Math/Numeric.h"

#include <algorithm> // min,max
#include <assert.h>
#include <limits>
#include <cmath>

using namespace MathMB;
using namespace std;

const double CMyDynamicWindow::PI = 3.1415926536f;

CMyDynamicWindow::CMyDynamicWindow()
{
    T = 0.1f;

    m_Precision = 0.01f; // Kolko smije fulati poziciju

    RR = 0.07f; // Radijus robota [m]

    // SECURITY DISTANCE za robota
    SC1 = 0;
    SC2 = 0.05f;

    K = 0;

    m_DelayInSamples = 3;

    Reset();
}

// glavna funkcija DW-a
bool CMyDynamicWindow::Sekvenca_izvodjenja()
{
    SP.setpoint_v = 0;
    SP.setpoint_w = 0;

    // Provjera dal je blizu cilja - u cilju gledamo samo s obzirom na orijentaciju
    double delta_x = global_goal_x - RB.x;
    double delta_y = global_goal_y - RB.y;
    double current_goal_distance = sqrt(delta_x * delta_x + delta_y * delta_y);

    if(current_goal_distance < m_Precision * 5)
    {
        m_Line.p1.x = m_Line.p2.x = RB.x;
        m_Line.p1.y = m_Line.p2.y = RB.y;

        if(m_bSniffObstaclesNearGoal &&
           !ObstacleFreeMoving()) // Makar smo blizu cilju moze tu jos biti neka smrdljiva prepreka pa treba pronjusiti prvo
        {
            // Na cilju smrdi, stop!
            SP.setpoint_v = 0;
            SP.setpoint_w = 0;
            return false;
        }

        assert(m_Precision > 0);
        if(current_goal_distance > m_Precision)
        {
            // Orijentiraj se prema cilju (s nosom ili guzicom, ovisno kak je brze) i nakon toga se pozicioniraj prema cilju
            double dx = global_goal_x - RB.x;
            double dy = global_goal_y - RB.y;

            double AngleToRefPoint = atan2(dy, dx);

            double dAng = AngleToRefPoint - RB.th;
            dAng = abs(CGeometryBase::NormAngle_MinusPI2_PI2(dAng));

            CLineImp PLine; // Pravac okomit na smjer robota koji prolazi kroz goal point
            PLine.LineFromDirectionAndPoint(RB.th + PI / 2, global_goal_x, global_goal_y);

            double dError = PLine.GetDistanceToPoint(RB.x, RB.y); // Uzduzna pogreska pozicije

            if(dAng < PI * 5.0f / 180)
            {
                // Pozicioniranje

                SP.setpoint_v = dError * 5.f;
                SP.setpoint_w = 0;
            }
            else
            {
                // Orijentiranje prema referentnom smjeru
                double RefAngle;
                // if(dAng<=PI/2) // Okreni se s nosom
                RefAngle = AngleToRefPoint;
                // else // Okreni se s guzicom
                //	RefAngle = AngleToRefPoint+PI;

                double dAng = RefAngle - RB.th;
                dAng = CGeometryBase::NormAngle_MinusPI_PI(dAng);

                SP.setpoint_v = dError * 2.5f;
                SP.setpoint_w = K * dAng;
            }
        }
        else
        {
            // Fino smo se pozicionirali pa sad okreni robota u smjeru global_goal_fi
            double RefAngle = global_goal_fi;

            double dAng = RefAngle - RB.th;
            dAng = CGeometryBase::NormAngle_MinusPI_PI(dAng);

            SP.setpoint_v = 0;
            SP.setpoint_w = K * dAng;

            if(abs(dAng) < PI * 5 / 180)
                return true;
        }

        return false;
    }

    if(GetFreeLine())
    {
        // Kad smo nasli slobodnu liniju, krecemo se prema njezinoj krajnjoj tocki
        double dx = m_Line.p2.x - RB.x;
        double dy = m_Line.p2.y - RB.y;

        double AngleToRefPoint = atan2(dy, dx);
        double RobAng = CGeometryBase::NormAngle_MinusPI_Ang(RB.th, AngleToRefPoint);
        double dAng = AngleToRefPoint - RobAng;

        SP.setpoint_w = K * dAng;

        if(current_goal_distance > m_StopDistance)
        {
            // Kod malih brzina i uz prepreke robot moze zablokirati ako ima linearnu brzinu.
            // U tom slucaju dajemo mu samo kutnu brzinu.

            if(RB.v < 0.1f && !StaticObstacleFreeMoving())
            {
                SP.setpoint_v = 0;
            }
            else if(RB.v < 0.1f && !DynamicObstacleFreeMoving())
            {
                SP.setpoint_v = 0;
            }
            else
            {
                // Ubrzavaje prema ref brzini
                SP.setpoint_v = CNum<double>::Bound(0, m_vRefPrev + m_dv * T, m_v);
                // SP.setpoint_v = m_v;
                // SP.setpoint_v = Numf::Bound(0, RB.v + m_dv*T, m_v);

                // Ako robot skrece, onda treba usporiti
                if(fabs(dAng) > Const<double>::PI / 15)
                    SP.setpoint_v = CNum<double>::Bound(0.1f, m_vRefPrev - m_dv * T, m_v); // *= std::max((1-3*fabs(dAng)/Const::PI), 0.1f);
            }
        }
        else
        {
            // Kocenje
            SP.setpoint_v = sqrt(2 * m_dv * current_goal_distance);
            SP.setpoint_v -= m_dv * 3 * T; // Malo ubrzavamo kocenje
            SP.setpoint_v = std::max(SP.setpoint_v, 0.);
        }
    }
    else
    {
        SP.setpoint_v = 0;
        SP.setpoint_w = 0;
    }
    return false;
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Njusi prepreke i daje liniju prema cilju koja je slobodna od prepreka
//_____________________________________________________________________________________________________________________________________________________________________
bool CMyDynamicWindow::GetFreeLine()
{
    // Zaustavni put
    assert(m_dv >= 0);
    m_StopDistance = RB.v * RB.v / (2 * m_dv);          // Zaustavni put
    m_StopDistance += m_DelayInSamples * T * abs(RB.v); // za svaki slucaj dodajemo jos pokoji ciklus sa trenutnom brzinom (radi kasnjenja)

    double dx = global_goal_x - RB.x;
    ;
    double dy = global_goal_y - RB.y;

    double current_goal_distance = sqrt(dx * dx + dy * dy);

    double AngleToGoal = atan2(dy, dx);

    // Ispitaj sve moguce pravce koji krecu od trenutne pozicije robota i nadi onog koji je slobodan od prepreka

    double AngleStep = 5.f / 180 * PI;
    double HeadingAngle; // Kut od robota prema kontrolnoj tocki
    int iMax = (int)(2 * PI / AngleStep);
    bool bFoundFreeLine = false;
    for(int i = 0; i < iMax; i++)
    {
        if(!(i & 1)) // parni
            HeadingAngle = AngleToGoal + i / 2 * AngleStep;
        else
            HeadingAngle = AngleToGoal - (i + 1) / 2 * AngleStep;

        m_Line.LineFromDirectionAndPoint(HeadingAngle, RB.x, RB.y);
        m_Line.p1.x = RB.x;
        m_Line.p1.y = RB.y;

        double LookDistance; // Kak daleko gledamo ak ima prepreka

        // ... za svaki slucaj produzujemo horizont gledanja jer ako robot stoji zaustavni put mu je nula pa onda ne bi uopce gledal
        // unaprijed LookDistance = std::max(m_StopDistance, RR*1.5f);
        LookDistance = std::max(m_StopDistance, RR * 2.5f);
        // ... ne trebamo gledati dalje od cilja
        LookDistance = std::min(LookDistance, current_goal_distance);

        m_Line.p2.x = RB.x + cos(HeadingAngle) * LookDistance;
        m_Line.p2.y = RB.y + sin(HeadingAngle) * LookDistance;

        double limit_distance =
            RR + SC1 + fabs(RB.v) / m_v * (SC2 - SC1); // Udaljenost pravca od najblize prepreke mora biti manja od ove granicne
        double DistToClosestObstacle = std::numeric_limits<double>::max(); // Udaljenost od najblize prepreke

        int iMap = 0;
        int nObs = (int)m_pStaticMap->GetNumObjects();
        C2DGeomMap* pMap = m_pStaticMap;

        // Za trenutnu liniju testiraj sve prepreke u mapi i nadi udaljenost linije do najblize prepreke
        for(int iObs = 0; iObs < nObs; iObs++)
        {
            C2DObject* pObs = pMap->GetObject(iObs);

            if(pObs->ID == m_SelfID || pObs->ID == m_IgnoreObs)
                continue;

            // Najdi udaljenost pravca od prepreke
            C2DPoint ObsPt, TrajPt;
            double Dist = pObs->GetDistanceToLine(&m_Line, &ObsPt, &TrajPt);

            if(Dist < limit_distance)
            {
                // ak  je prepreka prije pocetka linije, mozemo ju zanemariti makar je blizu
                CLineImp PLine; // Okomica
                PLine.GetLineFromPerpendicularLine(&m_Line, m_Line.p1.x, m_Line.p1.y);
                if(PLine.IsPointLeft(&ObsPt))
                    Dist = limit_distance * 1.01f;
            }

            if(Dist < DistToClosestObstacle)
            {
                DistToClosestObstacle = Dist;
            }

            if(iObs == nObs - 1)
            {
                if(iMap == 0)
                {
                    // Prebaci se na sljedecu mapu
                    pMap = m_pDynamicMap;
                    iObs = -1;
                    nObs = m_pDynamicMap->GetNumObjects();
                    iMap++;
                }
            }
        }

        if(DistToClosestObstacle > limit_distance)
        {
            // nasli smo slobodnu liniju jeee
            bFoundFreeLine = true;

            // Produzi liniju radi regulacije kuta
            if(m_StopDistance < current_goal_distance / 2)
            {
                m_Line.p2.x = RB.x + cos(HeadingAngle) * current_goal_distance / 2;
                m_Line.p2.y = RB.y + sin(HeadingAngle) * current_goal_distance / 2;
            }

            break;
        }
    }

    return bFoundFreeLine;
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Njusi dal je trenutno gibanje robota (u duljini zaustavnog puta) slobodno od prepreka.
//_____________________________________________________________________________________________________________________________________________________________________
bool CMyDynamicWindow::ObstacleFreeMoving()
{
    return this->StaticObstacleFreeMoving() && this->DynamicObstacleFreeMoving();
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Njusi dal je trenutno gibanje robota (u duljini zaustavnog puta) slobodno od statickih prepreka.
//_____________________________________________________________________________________________________________________________________________________________________
bool CMyDynamicWindow::StaticObstacleFreeMoving()
{
    assert(m_dv >= 0);
    m_StopDistance = RB.v * RB.v / (2 * m_dv);          // Zaustavni put
    m_StopDistance += m_DelayInSamples * T * abs(RB.v); // za svaki slucaj dodajemo jos pokoji ciklus sa trenutnom brzinom (radi kasnjenja)

    // Konstruiraj pravac od trenutne pozicije robota u trenutnom smjeru gibanja robota i duljine koja je potrebna za zaustaviti robota
    m_Line.LineFromDirectionAndPoint(RB.th, RB.x, RB.y);
    m_Line.p1.x = RB.x;
    m_Line.p1.y = RB.y;

    double LookDistance; // Kak daleko gledamo ak ima prepreka
    LookDistance = m_StopDistance;

    m_Line.p2.x = RB.x + cos(RB.th) * LookDistance;
    m_Line.p2.y = RB.y + sin(RB.th) * LookDistance;

    double limit_distance = RR + SC1 + abs(RB.v) / m_v * (SC2 - SC1);

    unsigned int nObs = m_pStaticMap->GetNumObjects();
    C2DGeomMap* pMap = m_pStaticMap;

    for(unsigned int iObs = 0; iObs < nObs; iObs++)
    {
        C2DObject* pObs = pMap->GetObject(iObs);

        if(pObs->ID == m_SelfID)
            continue;

        // Najdi udaljenost pravca od prepreke
        C2DPoint ObsPt, TrajPt;
        double Dist = pObs->GetDistanceToLine(&m_Line, &ObsPt, &TrajPt);

        if(Dist < limit_distance)
        {
            // ak  je prepreka prije pocetka linije, mozemo ju zanemariti makar je blizu
            CLineImp PLine; // Okomica
            PLine.GetLineFromPerpendicularLine(&m_Line, m_Line.p1.x, m_Line.p1.y);
            if(PLine.IsPointLeft(&ObsPt))
                Dist = limit_distance * 1.01f;
        }

        if(Dist < limit_distance)
            return false;
    }

    return true;
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Njusi dal je trenutno gibanje robota (u duljini zaustavnog puta) slobodno od dinamickih prepreka.
//_____________________________________________________________________________________________________________________________________________________________________
bool CMyDynamicWindow::DynamicObstacleFreeMoving()
{
    assert(m_dv >= 0);
    m_StopDistance = RB.v * RB.v / (2 * m_dv);          // Zaustavni put
    m_StopDistance += m_DelayInSamples * T * abs(RB.v); // za svaki slucaj dodajemo jos pokoji ciklus sa trenutnom brzinom (radi kasnjenja)

    // Konstruiraj pravac od trenutne pozicije robota u trenutnom smjeru gibanja robota i duljine koja je potrebna za zaustaviti robota
    m_Line.LineFromDirectionAndPoint(RB.th, RB.x, RB.y);
    m_Line.p1.x = RB.x;
    m_Line.p1.y = RB.y;

    double LookDistance; // Kak daleko gledamo ak ima prepreka
    LookDistance = m_StopDistance;

    m_Line.p2.x = RB.x + cos(RB.th) * LookDistance;
    m_Line.p2.y = RB.y + sin(RB.th) * LookDistance;

    double limit_distance = RR + SC1 + abs(RB.v) / m_v * (SC2 - SC1);

    unsigned int nObs = m_pDynamicMap->GetNumObjects();
    C2DGeomMap* pMap = m_pDynamicMap;

    for(unsigned int iObs = 0; iObs < nObs; iObs++)
    {
        C2DObject* pObs = pMap->GetObject(iObs);

        if(pObs->ID == m_SelfID || pObs->ID == m_IgnoreObs)
            continue;

        // Najdi udaljenost pravca od prepreke
        C2DPoint ObsPt, TrajPt;
        double Dist = pObs->GetDistanceToLine(&m_Line, &ObsPt, &TrajPt);

        if(Dist < limit_distance)
        {
            // ak  je prepreka prije pocetka linije, mozemo ju zanemariti makar je blizu
            CLineImp PLine; // Okomica
            PLine.GetLineFromPerpendicularLine(&m_Line, m_Line.p1.x, m_Line.p1.y);
            if(PLine.IsPointLeft(&ObsPt))
                Dist = limit_distance * 1.01f;
        }

        if(Dist < limit_distance)
            return false;
    }

    return true;
}

//_____________________________________________________________________________________________________________________________________________________________________
//
//
//_____________________________________________________________________________________________________________________________________________________________________
double CMyDynamicWindow::StaticObstacleMinDistance()
{
    // assert(m_dv>=0);
    // m_StopDistance = RB.v*RB.v/(2*m_dv); // Zaustavni put
    // m_StopDistance += m_DelayInSamples*T*abs(RB.v); // za svaki slucaj dodajemo jos pokoji ciklus sa trenutnom brzinom (radi kasnjenja)

    // Konstruiraj pravac od trenutne pozicije robota u trenutnom smjeru gibanja robota i duljine koja je potrebna za zaustaviti robota
    m_Line.LineFromDirectionAndPoint(RB.th, RB.x, RB.y);
    m_Line.p1.x = RB.x;
    m_Line.p1.y = RB.y;

    // double LookDistance; // Kak daleko gledamo ak ima prepreka
    // LookDistance = m_StopDistance;

    m_Line.p2.x = RB.x + cos(RB.th) * 0.01f;
    m_Line.p2.y = RB.y + sin(RB.th) * 0.01f;

    double MinDist = numeric_limits<double>::max();

    unsigned int nObs = m_pStaticMap->GetNumObjects();
    C2DGeomMap* pMap = m_pStaticMap;

    for(unsigned int iObs = 0; iObs < nObs; iObs++)
    {
        C2DObject* pObs = pMap->GetObject(iObs);

        if(pObs->ID == m_SelfID)
            continue;

        // Najdi udaljenost pravca od prepreke
        C2DPoint ObsPt, TrajPt;
        double Dist = pObs->GetDistanceToLine(&m_Line, &ObsPt, &TrajPt);

        if(Dist < MinDist)
        {
            MinDist = Dist;
        }
    }

    return MinDist;
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Provjerava dal je blizu cilja (unutar radijusa d od cilja)
//_____________________________________________________________________________________________________________________________________________________________________
bool CMyDynamicWindow::IsNearGoal(double d)
{
    double delta_x = global_goal_x - RB.x;
    double delta_y = global_goal_y - RB.y;
    double current_goal_distance = sqrt(delta_x * delta_x + delta_y * delta_y);
    return current_goal_distance < d;
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Provjerava dal je blizu cilja (unutar nekog rangea od cilja)
//_____________________________________________________________________________________________________________________________________________________________________
bool CMyDynamicWindow::IsOrientationOK(double AngTol)
{
    double AngleError = fabs(CAngleRange::Range_MinusPI_PI(global_goal_fi - RB.th));

    return AngleError < AngTol;
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Resetira neke varijable na difoltnu vrijednost
//_____________________________________________________________________________________________________________________________________________________________________
void CMyDynamicWindow::Reset()
{
    m_bSniffObstaclesNearGoal = false;
    m_IgnoreObs = -1000;
}
