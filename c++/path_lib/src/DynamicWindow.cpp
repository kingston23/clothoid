#include "Traj/DynamicWindow.h"

#include <algorithm> // min,max
#include <assert.h>
#include <limits>
#include <cmath>

// iznosi za double parametre
const double DynamicWindow::PI = 3.1415926536f;

// Tezine pojedinih doprinosa
const double DynamicWindow::BETA = 1.0f;   // Tezinski koeficijent u kriterijskoj funkciji - prohodnost
const double DynamicWindow::ALPHA = 0.05f; // udio putanje u cijeloj prici - alignment velocity
const double DynamicWindow::DELTA = 2.0f;  // udio putanje u cijeloj prici - alignment heading

template<class T>
inline T bound(T lower, T value, T upper)
{
    return std::max(lower, std::min(value, upper));
}

DynamicWindow::DynamicWindow()
{
    m_vMax = 1;
    m_vMin = 0;
    m_dvMax = 2;

    // velicine vezane za rotaciju
    // ovo je u radijanima
    m_wMax = 32;
    m_wMin = -32;
    m_dwMax = 5 * PI / 18;

    T = 0.1f;

    FK = 100; // Simulira se FK koraka unaprijed

    GOAL_PERIMETER = 0.1f; // malo prije cilja treba uzeti drukcije parametre alpha, beta i delta

    RR = 0.07f; // Radijus robota [m]

    // SECURITY DISTANCE za robota
    SC1 = 0;
    SC2 = 0.05f;
    SC_W = 0.0f;

    // TOLERANCIJA BRZINA - one kaj su manje od ovog se smatraju nulom
    V_TOLERANCE = m_dvMax * T / 10;
    W_TOLERANCE = m_dwMax * T / 10;
}

// glavna funkcija DW-a
void DynamicWindow::Sekvenca_izvodjenja()
{
    SP.setpoint_v = 0;
    SP.setpoint_w = 0;

    // Provjera dal je blizu cilja - u cilju gledamo samo s obzirom na orijentaciju
    double delta_x = global_goal_x - RB.x;
    double delta_y = global_goal_y - RB.y;
    double current_goal_distance = sqrt(delta_x * delta_x + delta_y * delta_y);

    if(GOAL_PERIMETER > current_goal_distance)
    {
        Beta = 0.0;
        Alpha = 0.0;
        Delta = 1.0;
    }
    else
    {
        Beta = BETA;
        Alpha = ALPHA;
        Delta = DELTA;
    }

    // odredjivanje set-a brzina
    // dva su osnovna ogranicenja
    // 1. s obzirom na m_dvMax, m_dwMax odredjujemo trenutni dinamicki prozor
    // 2. s obzirom na m_vMax, m_wMax
    Odredi_dinamicki_prozor();

    // za sve parove brzina ako su dohvatljivi,
    // odredjujemo kruzni luk i minimalni zaustavni put
    for(ni = 0; ni < V_DIM + 1; ni++)
    {
        for(nj = 0; nj < W_DIM + 1; nj++)
        {
            // Nastavak proracuna se vrsi samo ako je par dohvatljiv
            // unutar dinamickog prozora
            if(TB.flag[ni][nj] == MB_TB::INITIAL)
            {
                MinimalniZaustavniPut(); // izracunava se m_StopDistance

                Kruzni_luk();

                // prohodnost odredjene trajektorije & zabrana ako bi doslo do sudara
                Prohodnost();

                // usmjerenost prema cilju
                if((TB.flag[ni][nj] == MB_TB::CLEAR) || (TB.flag[ni][nj] == MB_TB::HAS_OBSTACLE))
                    OrientacijaGoal(global_goal_x, global_goal_y); // globalni cilj

                if((TB.flag[ni][nj] == MB_TB::CLEAR) || (TB.flag[ni][nj] == MB_TB::HAS_OBSTACLE))
                    OcjenaLinearnaBrzina();
            }

        } // po svim w
    }     // po svim v

    // Izracunaj ukupnu ocjenu za svaku trajektoriju
    DoprinosProhodnost();
    DoprinosOrientacijaGoal();
    DoprinosLinearnaBrzina();

    ni = -1;
    nj = -1;

    // PRORACUN OPTIMALNOG PARA
    Optimalni_par();

    // Postavljanje setpointa vektora brzine i azuriranje loggera
    // ako smo uspijeli nesto odabrati!
    if((ni >= 0) && (nj >= 0))
    {
        Kruzni_luk();
        // podesenje setpointa brzina
        SP.setpoint_v = TB.v[ni];

        SP.setpoint_w = TB.w[nj]; // u stupnjevima
    }
    else
    {
        // nismo uspijeli nista odabrati
        // moramo postaviti brzine i set - point u nulu
        SP.setpoint_v = 0;
        SP.setpoint_w = 0;
    }
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Puni strukturu RB sa trenutnim stanjem robota
//_____________________________________________________________________________________________________________________________________________________________________
void DynamicWindow::SetRobotState(double x, double y, double ang, double v, double w)
{
    RB.x = x;
    RB.y = y;
    RB.th = ang;

    while(RB.th < 0.)
        RB.th += 2 * PI;
    while(RB.th >= 2 * PI)
        RB.th -= 2 * PI;

    RB.v = v;
    RB.w = w;
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Funkcija koja odredjuje GRANICE DINAMICKOG PROZORA na temelju trenutnih ocitanih brzina robota
//_____________________________________________________________________________________________________________________________________________________________________
void DynamicWindow::Odredi_dinamicki_prozor()
{
    int i, j;

    double vMax = RB.v + m_dvMax * T;
    vMax = bound(m_vMin, vMax, m_vMax);

    double vMin = RB.v - m_dvMax * T;
    vMin = bound(m_vMin, vMin, m_vMax);

    double dv = vMax - vMin;

    // Za linearne brzine
    // V_DIM je ukupna dimenzija u + i - smjeru brzina
    // U dinamicki prozor sprema se V_DIM+1 brzina u rasponu od minimalno moguce do maksimalno moguce uzevsi u obzir maksimalno
    // usporenje/ubrzanje i vrijeme trajanja ciklusa.
    for(i = 0; i < V_DIM + 1; i++)
        TB.v[i] = vMin + i / V_DIM * dv;

    double wMax = RB.w + m_dwMax * T;
    wMax = bound(m_wMin, wMax, m_wMax);

    double wMin = RB.w - m_dwMax * T;
    wMin = bound(m_wMin, wMin, m_wMax);

    double dw = wMax - wMin;

    // Za kutne brzine.
    for(i = 0; i < W_DIM + 1; i++)
    {
        TB.w[i] = wMin + i / W_DIM * dw;

        if(i > 0 && (TB.w[i - 1] * TB.w[i] < 0))
        {
            // uguravanje w=0 brzine u prostor, bitno za slucaj ravne putanje
            if(fabs(TB.w[i - 1]) > fabs(TB.w[i]))
                TB.w[i] = 0.0f;
            else
                TB.w[i - 1] = 0.0f;
        }
    }

    // Inicijalizacija velicina u tablici brzina i zabrana nedozvoljenih brzina
    for(i = 0; i < V_DIM + 1; i++)
    {
        for(j = 0; j < W_DIM + 1; j++)
        {
            // Obrisi trenutni par
            TB.flag[i][j] = MB_TB::INITIAL;

            TB.ocjena[i][j] = 0.0f;
            TB.ocjena_prohodnost[i][j] = 0.0f;
            TB.ocjena_linearna_brzina[i][j] = 0.0f;
            TB.ocjena_orientacija_goal[i][j] = 0.0f;

            // if((TB.v[i]>m_vMax) || (TB.v[i]<m_vMin) || (TB.w[j]>m_wMax) || (TB.w[j]<m_wMin)) - to se ne moze desiti jer smo gore vec
            // ogranicili 	TB.flag[i][j]=MB_TB::KINEMATIC_CONSTRAINTS;
        }
    }
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Racuna minimalni put koji robotu treba da se zaustavi za odredeni par brzina (v,w)
//_____________________________________________________________________________________________________________________________________________________________________
void DynamicWindow::MinimalniZaustavniPut()
{
    // Usporeduje se koja brzina se moze prije dovesti do nule - kutna ili linearna
    double StopTimeLin = fabs(TB.v[ni] / m_dvMax);
    double StopTimeAng = fabs(TB.w[nj] / m_dwMax);

    if(StopTimeLin > StopTimeAng)
    {
        // zaustavljanje po linearnoj brzini je duze od zaustavljanja po kutnoj brzini
        // onda se koci sa maksimalnom linearnom deceleracijom
        m_StopTime = StopTimeLin;
        m_StopAccel = m_dvMax;
    }
    else
    {
        // zaustavljanje po linearnoj brzini je krace od zaustavljanja po kutnoj brzini
        m_StopTime = StopTimeAng;
        if(m_StopTime == 0)
            m_StopAccel = 0;
        else
            m_StopAccel = fabs(TB.v[ni] / m_StopTime); // linearna deceleracija je manja od maksimalne
    }

    // ovdje je sa Tsecurity opisan broj ciklusa koji mozemo
    //"promasiti" - ovdje je 4
    double Tsecurity = T * 2; // T*4

    m_StopDistance = fabs(TB.v[ni]) * (m_StopTime + Tsecurity) - 0.5f * m_StopAccel * m_StopTime * m_StopTime;
}

/*****************************************************************
 * Funkcija koja nalazi zadani broj tocaka KRUZNOG LUKA / PRAVCA *
 *****************************************************************/
void DynamicWindow::Kruzni_luk()
{
    // Trenutni par brzina
    double v = TB.v[ni];
    double w = TB.w[nj];

    // Ako je w<>0, kruzni luk
    if(fabs(w) > W_TOLERANCE)
    {
        // Radijus luka
        double r = fabs(v / w);
        TB.radius[ni][nj] = r; // mjerimo zakrivljenost krivulje pomocu radiusa

        // Izracunaj srediste kruznog luka.
        double th_c;

        if(w > 0)
            th_c = RB.th + PI / 2;
        else
            th_c = RB.th - PI / 2;

        // Provjera raspona  (sredjivanje na +-180 deg)
        if(th_c > PI)
            th_c -= 2 * PI;
        else if(th_c < -PI)
            th_c += 2 * PI;

        m_Arc.SetCircleFromTangent(RB.x, RB.y, RB.th, v / w);
        // m_Arc.SetCircle(x_c,y_c,r); // Postavi krug - provjeri dal obadvoje daje isto

        // okretanje kuta na poziciju obodne tocke (tj. tocke gdje je robot, ne njegova orijentacija!!!)
        // kut od centra rotacije prema obodnoj tocki
        th_c += PI;

        // Provjera raspona
        if(th_c > PI)
            th_c -= 2 * PI;

        // Promjena kuta za FK koraka unaprijed (ogranicavamo ju na 2PI)
        double dAng = w * T * FK;
        if(dAng > 2 * PI)
            dAng = 2 * PI;
        else if(dAng < -2 * PI)
            dAng = -2 * PI;

        m_Arc.SetArc(th_c, dAng); // Postavi kruzni luk

        // Sad kad znamo parametre kruznog luka, izracunamo gdje bi robot na kruznom luku stao da koci maks deceleracijom
        // Za to koristimo prethodno izracunate parametre od zaustavni puta

        C2DPoint Point;
        m_Arc.GetPointByDistance(m_StopDistance, &Point);

        TB.breakage_point_x[ni][nj] = Point.x;
        TB.breakage_point_y[ni][nj] = Point.y;
        TB.breakage_point_th[ni][nj] = RB.th + w * m_StopTime;

        m_bIsLine = false;
    }
    else
    {
        // Ako je w=0, pravac
        TB.radius[ni][nj] = -1.0; // beskonacni radius

        double S = sin(RB.th);
        double C = cos(RB.th);
        // Mogli bi zadati pravac i preko poc i krajnje tocke, ali to ne bi radilo za v=0 (poc i krajnja bile bi iste)
        m_Line.LineFromDirectionAndPoint(S, C, RB.x, RB.y);
        m_Line.p1.x = RB.x;
        m_Line.p1.y = RB.y;
        m_Line.p2.x = RB.x + v * T * FK * C;
        m_Line.p2.y = RB.y + v * T * FK * S;

        TB.breakage_point_x[ni][nj] = RB.x + m_StopDistance * C;
        TB.breakage_point_y[ni][nj] = RB.y + m_StopDistance * S;
        TB.breakage_point_th[ni][nj] = RB.th;

        m_bIsLine = true;
    }
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Trajektoriji dodjeljuje ocjenu prohodnosti s obzirom na prepreke.
// Racuna lokaciju najblize prepreke.
// Postavlja jedan od ovih flagova za trajektoriju:
// - NON_ADMISSIBLE (doslo bi do sudara)
// - HAS_OBSTACLE (ima prepreku ali moze se zakociti prije nje)
// - CLEAR
//_____________________________________________________________________________________________________________________________________________________________________
void DynamicWindow::Prohodnost()
{
    // Security radius koji je definiran geometrijom robota i trenutnim v,w.
    // Govori koliko sredina robota mora biti minimalno udaljena od prepreke uzevsi u obzir radijus i brzinu robota.
    // Kod vecih brzina robota ta je udaljenost veca.
    double limit_distance = (RR + SC1 + (SC2 - SC1) * fabs(TB.v[ni]) / m_vMax) + SC_W * fabs(TB.w[nj]) / m_wMax;

    for(unsigned int iObs = 0; iObs < m_pMap->GetNumObjects(); iObs++)
    {
        C2DObject* pObs = m_pMap->GetObject(iObs);

        if(pObs->ID == m_SelfID)
            continue;

        // Najdi udaljenost KL od prepreke
        double Dist;

        C2DPoint ObsPt, TrajPt;
        if(m_bIsLine)
            Dist = pObs->GetDistanceToLine(&m_Line, &ObsPt, &TrajPt);
        else
            Dist = pObs->GetDistanceToArc(&m_Arc, &ObsPt, &TrajPt);

        if(Dist < limit_distance)
        {
            // Izracunaj put od pocetka KL do tocke na KL koja je najbliza prepreci
            double DistToCollision;

            if(m_bIsLine)
            {
                double dx = m_Line.p1.x - TrajPt.x;
                double dy = m_Line.p1.y - TrajPt.y;

                DistToCollision = sqrt(dx * dx + dy * dy);
            }
            else
            {
                DistToCollision = m_Arc.GetDistToBeginPoint(TrajPt.x, TrajPt.y);
            }

            if(DistToCollision < m_StopDistance)
            {
                // Ne moze se zaustaviti do prepreke
                TB.flag[ni][nj] = MB_TB::NON_ADMISSIBLE;
                TB.obstacle_point_x[ni][nj] = TrajPt.x;
                TB.obstacle_point_y[ni][nj] = TrajPt.y;
                TB.ocjena_prohodnost[ni][nj] = 0.0;
                return;
            }
            else
            {
                // Trajektorija ima prepreku, al moguce je zaustaviti prije nek najasimo na nju

                // PAZNJA - ovdje ne koristimo udaljenost, vec vrijeme do kolizije
                // kao ocjenu, sto je bolja varijanta, jer uzima indirektno brzinu
                // robota u obzir
                if(abs(TB.v[ni]) > V_TOLERANCE)
                {
                    TB.obstacle_point_x[ni][nj] = TrajPt.x;
                    TB.obstacle_point_y[ni][nj] = TrajPt.x;
                    TB.ocjena_prohodnost[ni][nj] = DistToCollision;
                    TB.ocjena_prohodnost[ni][nj] /= abs(TB.v[ni]); // ja zakomentirala ? - vrijeme
                    TB.flag[ni][nj] = MB_TB::HAS_OBSTACLE;

                    // Sad se jos normalizira vrijeme stizanja do prepreke koje je spremljeno u ocjena_prohodnost od kojeg se oduzme vrijeme
                    // zaustavljanja. Normalizira se u odnosu na neko maksimalno moguce vrijeme.
                    double T_breakage =
                        std::max(fabs(TB.v[ni]) / m_dvMax, fabs(TB.w[nj] / m_dwMax)); // Vrijeme potrebno da se zaustavi robot
                    TB.ocjena_prohodnost[ni][nj] = (TB.ocjena_prohodnost[ni][nj] - T_breakage) / (T * FK - T_breakage);

                    return;
                }
                else
                {
                    // SINGULARNO SLUCAJ kada je translatorna brzina prakticki 0 dakle kada se vrtimo na mjestu!!!
                    // E SADA: to bi u nekim slucajevima mogla biti najbolja varijanta
                    // ali mi smo ovdje pretpostavili da je
                    // TB.ocjena_prohodnost[ni][nj]=0, sto je bolje rijesenje u vecini slucajeva
                    // RAZMISLITI .. vezano za specijalne slucajeve (ali ne treba zanemariti da vec par
                    // brzina sa minimalnom translatornom brzinom ne moze dobiti maksimalnu ocjenu!!!)

                    TB.ocjena_prohodnost[ni][nj] = 0.f;
                    TB.flag[ni][nj] = MB_TB::HAS_OBSTACLE;
                    return;
                }
            }
        }
    }

    // AKO JE PUTANJA CLEAR
    TB.flag[ni][nj] = MB_TB::CLEAR;

    if(abs(TB.v[ni]) > V_TOLERANCE)
        TB.ocjena_prohodnost[ni][nj] = 1.0f;
    else
        // ovdje stavljamo ocjenu 0 na singularitet, jer zapravo tu ne mozemo nista zakljuciti s obzirom na konfiguraciju prepreka
        TB.ocjena_prohodnost[ni][nj] = 0.0f;
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Trajektoriji dodjeljuje ocjenu usmjerenosti prema cilju.
// Usmjerenost se gleda u tocki zaustavljanja za tu trajektoriju, a ne u trenutnoj poziciji robota!!!
//_____________________________________________________________________________________________________________________________________________________________________
void DynamicWindow::OrientacijaGoal(double goal_x, double goal_y)
{
    while(TB.breakage_point_th[ni][nj] >= 2 * PI)
        TB.breakage_point_th[ni][nj] -= 2 * PI;
    while(TB.breakage_point_th[ni][nj] < 0)
        TB.breakage_point_th[ni][nj] += 2 * PI;

    double delta_x = goal_x - TB.breakage_point_x[ni][nj]; // breakage_point cuva poziciju robota na kruznom luku u nekom sljedecem ciklusu,
                                                           // pogledati Kruzni_luk()
    double delta_y = goal_y - TB.breakage_point_y[ni][nj];

    // usmjerenost od pozicije robota u sljedecem ciklusu do cilja
    double orientation_goal = atan2(delta_y, delta_x);
    if(orientation_goal < 0.0)
    {
        orientation_goal += 2 * PI;
    }
    // odstupanje od usmjerenosti prema cilju s obzirom na usmjerenje robota u sljedecem ciklusu
    double error_orientation = orientation_goal - TB.breakage_point_th[ni][nj];
    if(error_orientation < -PI)
    {
        error_orientation += 2 * PI;
    }
    else if(error_orientation > PI)
    {
        error_orientation -= 2 * PI;
    }

    TB.ocjena_orientacija_goal[ni][nj] = 1 - fabs(error_orientation) / PI;
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Trajektoriji dodjeljuje ocjenu s obzirom na brzinu.
//_____________________________________________________________________________________________________________________________________________________________________
void DynamicWindow::OcjenaLinearnaBrzina()
{
    if(TB.v[0] >= 0 && TB.v[V_DIM] >= 0) // Ak su i poc i konacna brzina pozitivne najbolja je najpozitivnija (tj. s najvecim indeksom)
    {
        TB.ocjena_linearna_brzina[ni][nj] = (ni) / V_DIM; // Veci indeks = veca brzina = veca ocjena
    }
    else if(TB.v[0] <= 0 &&
            TB.v[V_DIM] <= 0) // Ak su i poc i konacna brzina negativne najbolja je najnegativnija (tj. s najmanjim indeksom)
    {
        TB.ocjena_linearna_brzina[ni][nj] = (V_DIM - ni) / V_DIM;
    }
    else // Ak poc i konacna brzina imaju suprotne predznake
    {
        double vMax = std::max(abs(TB.v[0]), abs(TB.v[V_DIM]));
        TB.ocjena_linearna_brzina[ni][nj] = fabs(TB.v[ni]) / vMax;
    }
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Bira optimalnu trajektoriju, tj. par v,w sa najboljom ocjenom.
//_____________________________________________________________________________________________________________________________________________________________________
void DynamicWindow::Optimalni_par()
{
    // ocjene koje nesto valjaju su vece od 0.0
    double OcjenaMax = 0.0f;

    // Prolazak po svim parovima (v,w)
    for(int i = 0; i < V_DIM + 1; i++)
    {
        // POTREBNO JOS UBACITI TOLERANCIJU ZA W!!!!!!!!!!!!
        for(int j = 0; j < W_DIM + 1; j++)
        {
            if(TB.flag[i][j] > 0)
            {
                // brzine iz proslog ciklusa su na sredini prozora pa odabiremo njih ako su ocjene iste
                if((fabs(TB.ocjena[i][j] - OcjenaMax) < 0.00001) && ((i == (int)(V_DIM / 2)) || (j == (int)(W_DIM / 2))))
                {
                    ni = i;
                    nj = j;
                    OcjenaMax = TB.ocjena[i][j];
                }

                if(TB.ocjena[i][j] > OcjenaMax)
                {
                    ni = i;
                    nj = j;
                    OcjenaMax = TB.ocjena[i][j];
                }
            }
        }
    }
    // printf("DynamicWindow> Optimalni par(): OcjenaMax=%f, ni=%d, nj=%d", OcjenaMax, ni, nj);

    // pretpostavljamo da ocjena moze biti samo pozitivna!!!
    if(OcjenaMax <= 0.0) // barem neka ocjena mora biti veca od 0.0!!!!!!
    {
        ni = -1;
        nj = -1; // nije omogucen niti jedan par!!!
    }

    return;
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Racuna doprinos ukupnoj ocjeni tako da ocjenu orijentacije mnozi s delta i pribraja ukupnoj ocjeni.
//_____________________________________________________________________________________________________________________________________________________________________
void DynamicWindow::DoprinosOrientacijaGoal()
{
    double max_A = -1e30f;
    double min_A = 1e30f;

    // Trazimo minimume i maksimume ocjena prohodnosti (al samo za one trajektorije koje imaju prohodnost).
    for(int i = 0; i < V_DIM + 1; i++)
    {
        for(int j = 0; j < W_DIM + 1; j++)
        {
            // if(((TB.flag[i][j]==MB_TB::CLEAR)||(TB.flag[i][j]==MB_TB::HAS_OBSTACLE))&&(TB.ocjena_prohodnost[i][j]>0.0f))
            if(TB.flag[i][j] == MB_TB::CLEAR || TB.flag[i][j] == MB_TB::HAS_OBSTACLE)
            {
                if(TB.ocjena_orientacija_goal[i][j] < min_A)
                    min_A = TB.ocjena_orientacija_goal[i][j];
                if(TB.ocjena_orientacija_goal[i][j] > max_A)
                    max_A = TB.ocjena_orientacija_goal[i][j];
            }
        }
    }

    double Factor = 1;
    if(max_A - min_A > 0.001f) // Za slucaj da su min i max priblizno jednaki
        Factor = 1 / (max_A - min_A);
    else
        min_A = 0;

    for(int i = 0; i < V_DIM + 1; i++)
    {
        for(int j = 0; j < W_DIM + 1; j++)
        {
            // if(((TB.flag[i][j]==MB_TB::CLEAR)||(TB.flag[i][j]==MB_TB::HAS_OBSTACLE))&&(TB.ocjena_prohodnost[i][j]>0.0))
            if(TB.flag[i][j] == MB_TB::CLEAR || TB.flag[i][j] == MB_TB::HAS_OBSTACLE)
            {
                TB.ocjena_orientacija_goal[i][j] = (TB.ocjena_orientacija_goal[i][j] - min_A) * Factor;

                TB.ocjena[i][j] += TB.ocjena_orientacija_goal[i][j] * Delta;
            }
        }
    }
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Racuna doprinos ukupnoj ocjeni tako da ocjenu brzine mnozi s alfa i pribraja ukupnoj ocjeni.
//_____________________________________________________________________________________________________________________________________________________________________
void DynamicWindow::DoprinosLinearnaBrzina()
{
    for(int i = 0; i < V_DIM + 1; i++)
    {
        for(int j = 0; j < W_DIM + 1; j++)
        {
            if(TB.flag[i][j] > 0) // Samo za trajektorije koje su sigurne sto se tice prepreka
                TB.ocjena[i][j] += Alpha * TB.ocjena_linearna_brzina[i][j];
        }
    }
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Normalizira izracunate ocjene prohodnosti na [0,1] i nakon toga racuna
// doprinos ukupnoj ocjeni tako da ocjenu prohodnosti mnozi s beta i pribraja ukupnoj ocjeni.
//_____________________________________________________________________________________________________________________________________________________________________
void DynamicWindow::DoprinosProhodnost()
{
    int i, j;

    double max_A = -1e30f;
    double min_A = 1e30f;

    // Trazimo minimume i maksimume ocjena prohodnosti (al samo za one trajektorije koje imaju prohodnost).
    for(i = 0; i < V_DIM + 1; i++)
    {
        for(j = 0; j < W_DIM + 1; j++)
        {
            // if(TB.ocjena_prohodnost[i][j]>0.0f)
            if(((TB.flag[i][j] == MB_TB::CLEAR) || (TB.flag[i][j] == MB_TB::HAS_OBSTACLE)) && (TB.ocjena_prohodnost[i][j] > 0.0f))
            {
                if(TB.ocjena_prohodnost[i][j] < min_A)
                    min_A = TB.ocjena_prohodnost[i][j];
                if(TB.ocjena_prohodnost[i][j] > max_A)
                    max_A = TB.ocjena_prohodnost[i][j];
            }
        }
    }
    // Normiramo koristeci nadene min i max, i mnozimo s beta
    double Factor = 1;
    if(max_A - min_A > 0.001f) // Za slucaj da su min i max priblizno jednaki
        Factor = 1 / (max_A - min_A);
    else
        min_A = 0;

    for(i = 0; i < V_DIM + 1; i++)
    {
        for(j = 0; j < W_DIM + 1; j++)
        {
            // if(TB.ocjena_prohodnost[i][j]>0.0f)
            if(((TB.flag[i][j] == MB_TB::CLEAR) || (TB.flag[i][j] == MB_TB::HAS_OBSTACLE)) && (TB.ocjena_prohodnost[i][j] > 0.0))
            {
                TB.ocjena_prohodnost[i][j] = (TB.ocjena_prohodnost[i][j] - min_A) * Factor;

                TB.ocjena[i][j] += TB.ocjena_prohodnost[i][j] * Beta;
            }
        }
    }
}

//_____________________________________________________________________________________________________________________________________________________________________
//
// Provjerava dal je blizu cilja (unutar radijusa d od cilja)
//_____________________________________________________________________________________________________________________________________________________________________
bool DynamicWindow::IsNearGoal(double d)
{
    double delta_x = global_goal_x - RB.x;
    double delta_y = global_goal_y - RB.y;
    double current_goal_distance = sqrt(delta_x * delta_x + delta_y * delta_y);
    return current_goal_distance < d;
}

// I gotta have some of that brown sugar,
// Gotta try it before I die.
