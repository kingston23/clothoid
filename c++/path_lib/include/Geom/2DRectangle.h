#ifndef _2DRECTANGLE_H_
#define _2DRECTANGLE_H_

#include "Circle.h"
#include "Line.h"

#include <cmath>

// using namespace Geom;
namespace Geom
{
class C2DRectangleBase : public CGeometryBase
{
public:
    // Daje vrhove pravokutnika i jednadzbe pravaca za svaku stranicu pravokutnika
    static inline void ComputeRect1(COORD_GM x, COORD_GM y, ANGLERAD_GM ang, LENGTH_GM a,
                                    LENGTH_GM b, // srediste pravokutnika, kut stranice a i duljine stranica a i b
                                    COORD_GM& x0, COORD_GM& y0, COORD_GM& x1, COORD_GM& y1, COORD_GM& x2, COORD_GM& y2, COORD_GM& x3,
                                    COORD_GM& y3, // vrhovi kvadrata
                                    LNCOEFF_GM& A0, LNCOEFF_GM& B0, LNCOEFF_GM& C0, LNCOEFF_GM& A1, LNCOEFF_GM& B1,
                                    LNCOEFF_GM& C1, // parametri jednadzbe pravca stranica
                                    LNCOEFF_GM& A2, LNCOEFF_GM& B2, LNCOEFF_GM& C2, LNCOEFF_GM& A3, LNCOEFF_GM& B3, LNCOEFF_GM& C3)
    {
        // Tocke
        NUM_GM C = (NUM_GM)cos(ang);
        NUM_GM S = (NUM_GM)sin(ang);
        NUM_GM dx1 = C * a / 2;
        NUM_GM dy1 = S * a / 2;
        NUM_GM dx2 = S * b / 2;
        NUM_GM dy2 = C * b / 2;

        // Ovo bi trebali biti vrhovi kvadrata u smjeru suprotnom od vure. Ako je kut nula, x0,y0 je donja lijeva tocka, x1,y1 je donja
        // desna itd.
        x0 = x - dx1 + dx2;
        y0 = y - dy1 - dy2;

        x1 = x + dx1 + dx2;
        y1 = y + dy1 - dy2;

        x2 = x + dx1 - dx2;
        y2 = y + dy1 + dy2;

        x3 = x - dx1 - dx2;
        y3 = y - dy1 + dy2;

        // Pravce dobijemo iz tocaka
        A0 = y0 - y1; // prvi pravac ide od x0,y0 do x1,y1 itd
        B0 = x1 - x0;
        C0 = -A0 * x0 - B0 * y0;

        A1 = B0;
        B1 = -A0;
        C1 = -A1 * x1 - B1 * y1;

        A2 = B1;
        B2 = -A1;
        C2 = -A2 * x2 - B2 * y2;

        A3 = B2;
        B3 = -A2;
        C3 = -A3 * x3 - B3 * y3;
    }
};

//______________________________________________________________________________________________________________________________________________________
//
// Klasa pravokutnika (reprezentiran je sa 4 duzine)
//______________________________________________________________________________________________________________________________________________________
class C2DRectangle : public CGeometryBase
{
public:
    void ComputeRect(COORD_GM x, COORD_GM y, ANGLERAD_GM ang, LENGTH_GM a, LENGTH_GM b)
    {
        C2DRectangleBase::ComputeRect1(x, y, ang, a, b, m_Lines[0].p1.x, m_Lines[0].p1.y, m_Lines[1].p1.x, m_Lines[1].p1.y, m_Lines[2].p1.x,
                                       m_Lines[2].p1.y, m_Lines[3].p1.x, m_Lines[3].p1.y, m_Lines[0].A, m_Lines[0].B, m_Lines[0].C,
                                       m_Lines[1].A, m_Lines[1].B, m_Lines[1].C, m_Lines[2].A, m_Lines[2].B, m_Lines[2].C, m_Lines[3].A,
                                       m_Lines[3].B, m_Lines[3].C);

        // Pocetne tocke svake linije su postavljene, sad treba postaviti jos i krajnje
        m_Lines[0].p2 = m_Lines[1].p1;
        m_Lines[1].p2 = m_Lines[2].p1;
        m_Lines[2].p2 = m_Lines[3].p1;
        m_Lines[3].p2 = m_Lines[0].p1;
    }

    // Trazi sjecista pravokutnika i kruznog luka.
    // Vraca broj tocaka sjecista (0-8)
    // Niz pPoints mora biti duljine najmanje 8.
    int GetIntersectionWithArc(CCircleArc* pArc, C2DPoint* pPoints) const
    {
        C2DPoint* pPointsTemp = pPoints;
        int nInters = 0;

        // Za svaku stranicu pravokutnika izracunaj sjecista sa kruznim lukom
        for(int i = 0; i < 4; i++)
        {
            int n = pArc->GetIntersectionWithLine(m_Lines[i], pPointsTemp);

            nInters += n;
            pPointsTemp += n;
        }
        assert(nInters <= 8);
        return nInters;
    }

public:
    CBoundLineImp m_Lines[4];
};

}; // namespace Geom

#endif

// You're the sun and as you shine on me
// I feel free, I feel free, I feel free
