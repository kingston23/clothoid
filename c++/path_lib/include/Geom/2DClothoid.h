#ifndef _2D_Clothoid_H_
#define _2D_Clothoid_H_

#include "2DPoint.h"
#include "2DRotation.h"
#include "GeometryBase.h"
#include "Line.h"
#include "Math/AngleRange.h"
#include "Math/Numeric.h"

#include <algorithm>
#include <cmath>
#include <limits>
#include <stdio.h>


using namespace MathMB;

namespace Geom
{
//=================================================================================================================================================================
//
// Klasa za lookup tablicu koja sadrzi koordinate bazne klotoide.
// Bazna klotoida tipicno ima (x0,y0) = (0,0), th0 = 0, k0 = 0, c = 1.
// Na temelju tih koordinata moze se aproksimirati bilo koja druga klotoida.
//
//=================================================================================================================================================================
class C2DClothoidLookupTable
{
public:
    typedef double T;

    C2DClothoidLookupTable()
    {
        m_x = m_y = nullptr;

        m_nPoints = 0;
    }

    ~C2DClothoidLookupTable()
    {
        delete[] m_x;
        m_x = nullptr;
        delete[] m_y;
        m_y = nullptr;

        m_nPoints = 0;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Funkcija vraca koordinate tocke klotoide na temelju trazenja lookup tablice i interpolacije/ekstrapolacije
    // s - udaljenost po klotoidi pocevsi od (0,0) na kojoj hocemo dobiti koordinate,
    //    moze biti veca od m_sEnd i onda se dela ekstrapolacija, a inace interpolacija
    //________________________________________________________________________________________________________________________________________________
    void GetBasicClothoidPoint(T s, T& x, T& y)
    {
        // assert(IsValid());

        // bool bResultReliable = true; // Ovo se zasad ne koristi

        // T as = CNum<T>::Abs(s);

        T as = s;
        T Sign = 1;
        if(s < 0)
        {
            as = -as;
            Sign = (T)-1;
        }

        int i1 = ((int)( std::min(as, m_sEnd) / m_Delta_s))+1; // int ima ulogu floor funkcije
        //int i1 = (int)(as / m_Delta_s); // int ima ulogu floor funkcije
        if(i1 >= m_nPoints - 1)
            i1 = m_nPoints - 1;

        // assert(m_nPoints == (int)(m_sEnd/m_Delta_s)+1);

        /*if(as == 0) // Ovime ne dobijemo znacajnu optimizaciju buduci da se ovo vrlo rijetko desava. U funkciji C2DClothoidApp::GetPoint
        se pazi da se ne poziva ova funkcija kada je k0=0.
        {
            x = y = 0;
        }
        else */
        if(as > m_sEnd)
        {
            // Radi se ekstrapolacija. Ovo je u principu nezeljeno i moze znaciti pogresku u programu ili da je projektirana premala
            // tablica. Rezultat ce vjerojatno biti nepouzdan.
            // bResultReliable = false;

            // Radijus u zadnjoj tocki je (tj. recimo da je)
            //T r = 2/(c * (m_sEnd + as));
            T r = 2 / (m_sEnd + as);

            // Dvije zadnje tocke u tablici koje buju posluzile za ekstrapolaciju
            T x1 = m_x[i1 - 1];
            T y1 = m_y[i1 - 1];
            T x2 = m_x[i1];
            T y2 = m_y[i1];

            // Koeficijenti usmjerenog pravca koji ide od (x1,y1) do (x2,y2) (koef C nam ne treba)
            T A = y1 - y2;
            T B = x2 - x1;

            // Srediste kruznice koja prolazi kroz (x1,y1) do (x2,y2) i ima radijus r (srediste je s gornje lijeve strane usmjerenog pravca)
            T xc = (x1 + x2 + sqrt(4 * r * r / (A * A + B * B) - 1) * A) / 2;
            T yc = (y1 + y2 + sqrt(4 * r * r / (A * A + B * B) - 1) * B) / 2;

            // Kut vektora od sredista kruznice do 2. tocke
            T theta2 = atan2((y2 - yc), (x2 - xc));

            // Ovo je tocka koja je na udaljenosti (s-m_sEnd) od druge tocke, iduci po kruznici radijusa r
            x = Sign * (xc + r * cos(theta2 + (s - m_sEnd) / r)); // (s-m_sEnd)/r je prevaljeni kut od druge tocke do ekstrapolirane tocke.
            y = Sign * (yc + r * sin(theta2 + (s - m_sEnd) / r));
        }
        else
        {
            // Interpolacija

            // Linearna interpolacija
            // int i2 = i1 + 1;
            // T p = (as - (i1-1)*m_Delta_s) / m_Delta_s;
            // x = m_x[i1] + (m_x[i2]-m_x[i1]) * p;
            // y = m_y[i1] + (m_y[i2]-m_y[i1]) * p;

            // Interpolacija kruznim lukom

            T si = (i1-1) * m_Delta_s;

            T ki = si + (T)(0.5) * m_Delta_s; // pretpostavlja se c_L = 1
            T ri = 1 / ki;
            T theta_iL = (T)(0.5) * si * si;

            T ds = as - si;

            T Const = (T)0.5 * ds * ki;
            T S = sin(Const);
            //T Const2 = theta_iL + Const;
            //T Const3 = 2 * ri * sin(Const);
            //T Cos = cos(Const2);
            // T Sin = sin(Const2);
            //T Sin = sqrt(1 - Cos * Cos);

            //x = Sign * (m_x[i1] + Cos * Const3);
            //y = Sign * (m_y[i1] + Sin * Const3);
            
            x = Sign * (m_x[i1] + 2 * ri * cos(theta_iL + Const) * S);
            y = Sign * (m_y[i1] + 2 * ri * sin(theta_iL + Const) * S);
        }

        // if(s<0)
        //{
        //	// U slucaju da je s manje od 0, samo vratimo simetricnu tocku
        //	x = -x;
        //	y = -y;
        //}

        return;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja parametre klotoide koja ce biti sadrzana u lookup tablici, alocira potreban prostor za tocke i racuna tocke.
    // Znacenja parametara - vidi komentare odgovarajucih varijabli clanova klase.
    // Za detaljnija znacenja parametara vidi M. Brezak - doktorska disertacija, 6. poglavlje
    //________________________________________________________________________________________________________________________________________________
    bool SetParms(T DeltaS, T sEnd, T Cmin, T Cmax, T Kmax, T sMax, T DeltaTh_max)
    {
        m_Delta_s = DeltaS;
        m_sEnd = sEnd;
        m_Cmax = Cmax;
        m_Cmin = Cmin;
        m_Kmax = Kmax;
        m_sMax = sMax;
        m_DeltaTh_max = DeltaTh_max;

        m_nPoints = (int)(sEnd / DeltaS) + 1;
        //printf("%d %f", m_nPoints, sEnd);

        if(!Allocate(m_nPoints))
            return false;

        if(!IsValid())
            return false;

        if(!ComputeLookupTable())
            return false;

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Proracun lookup tablice za klotoidu pomocu Runge-Kutta metode.
    // Pretpostavljaju se ovi parametri klotoide: x0=y0=0, k0=0, c=1.
    // TUDU: apdejtati ovu funkciju po uzoru na Matlab funkciju CL_ComputeLT_RungeKutta
    //________________________________________________________________________________________________________________________________________________
    bool ComputeLookupTable()
    {
        // double sStep = 0.001; // korak integracije za metodu - izgleda da vrijedi: duplo manji korak -> duplo manja greska
        const int NumSubSteps = 10;
        // Npr. NumSubSteps 10, max greka = 3.5e-4
        // Npr. NumSubSteps 1000, max greka = 3.5e-6
        // Npr. NumSubSteps 10000, max greka = 3.5e-7
        double SubStep =
            (double)(m_Delta_s) / NumSubSteps; // korak integracije za metodu - izgleda da vrijedi: duplo manji korak -> duplo manja greska
        double HalfSubStep = 0.5 * SubStep;

        int iPoint = 0;

        // Poc. vrijednosti za integrator
        double x = 0, y = 0;
        // double c = 1; ovo je uvijek tako, pa izostavljamo

        const int NumStepsTotal = (m_nPoints - 1) * NumSubSteps;
        for(int iSubStep = 0; iSubStep < NumStepsTotal; ++iSubStep)
        {
            // Spremaj izlazne vrijednosti za zeljene parametre s
            if(iSubStep % NumSubSteps == 0)
            {
                m_x[iPoint] = (T)x;
                m_y[iPoint] = (T)y;
                iPoint++;
            }

            double k1, k2, k3, k4, tmp;

            // 's' je parametar koji znaci prevaljeni put, tj. duljinu klotoide
            double s = (double)iSubStep * SubStep;

            // Integracija za x
            k1 = cos(0.5 * s * s);
            tmp = s + HalfSubStep * k1;
            k2 = cos(0.5 * tmp * tmp);
            tmp = s + HalfSubStep * k2;
            k3 = cos(0.5 * tmp * tmp);
            tmp = s + SubStep * k3;
            k4 = cos(0.5 * tmp * tmp);

            x += SubStep / 6 * (k1 + 2 * k2 + 2 * k3 + k4);

            // Integracija za y
            k1 = sin(0.5 * s * s);
            tmp = s + HalfSubStep * k1;
            k2 = sin(0.5 * tmp * tmp);
            tmp = s + HalfSubStep * k2;
            k3 = sin(0.5 * tmp * tmp);
            tmp = s + SubStep * k3;
            k4 = sin(0.5 * tmp * tmp);

            y += SubStep / 6 * (k1 + 2 * k2 + 2 * k3 + k4);
        }

        return true;
    }

    bool IsValid() const
    {
        if(!m_x || !m_y)
        {
            assert(0);
            return false;
        }

        if(m_Delta_s <= 0)
        {
            assert(0);
            return false;
        }

        if(m_sEnd <= 0)
        {
            assert(0);
            return false;
        }

        if(m_nPoints < 2)
        {
            assert(0);
            return false;
        }

        return true;
    }

    bool Allocate(int _nPoints)
    {
        m_x = new T[_nPoints];
        m_y = new T[_nPoints];

        if(!m_x || !m_y)
        {
            assert(0);

            m_nPoints = 0;

            return false;
        }

        m_nPoints = _nPoints;

        return true;
    }

public:
    // Pokazivaci na nizove tocaka klotoide
    T* m_x;
    T* m_y;

    int m_nPoints; // Koliko je trenutno tocaka u tablici
    T m_Delta_s;   // Udaljenost (putujuci po klotoidi) izmedu dviju susjednih tocaka u tablici (>0)
    T m_sEnd;      // duljina klotoide u tablici (mora biti m_nPoints = sEnd/m_Delta_s+1)

    T m_Cmin; // Maksimalno skaliranje klotoide za koje je tablica projektirana
    T m_Cmax; // Minimalno skaliranje klotoide za koje je tablica projektirana
    T m_Kmax; // Maksimalni parametar K za koji je tablica projektirana (K=k0*C)
    // Maksimalna duljina bilo koje klotoide koju racunamo pomocu ove tablice (uz uvjet da ona zadovoljava sva ostala ogranicenja, npr.
    // ogranicenje C). Vrijedit ce redovito m_sEnd >> m_sMax, jer osnovna klotoida u tablici mora biti znatno dulja da bismo s njom mogli
    // proracunati puno raznih drugih klotoida.
    T m_sMax;
    // Maksimalna dopustena promjena orijentacije bilo koje klotoide
    T m_DeltaTh_max;
};

//=================================================================================================================================================================
//
// Template klasa za aproksimaciju 2D klotoide.
// Tocke klotoide su aproksimirane lookup tablicom i interpolacijom za ograniceno podrucje parametara.
// Moze se zadati zeljeno podrucje parametara za koje se radi lookup tablica.
//
//=================================================================================================================================================================
class C2DClothoidApp : public CGeometryBase
{
public:
    C2DClothoidApp()
    {
        m_c = 0; // Namjerno postavljamo zabranjenu vrijednost
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Testiranje dal je klotoida dobro zadana
    //________________________________________________________________________________________________________________________________________________
    bool IsValid() const
    {
        if(!m_pLookupTable)
            return false;

        if(m_c == 0)
            return false;

        if(fabs(m_k0) / m_sqrtac > m_pLookupTable->m_sEnd)
            return false; // Ova klotoida ima ekstremne parametre i tocke se ne mogu izracunati s postojecom lookup tablicom

        return true;
    }

    void operator=(C2DClothoidApp* pClothoid)
    {
        assert(pClothoid->IsValid());

        *this = *pClothoid;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Oslobadanje lookup tablice.
    // Pozvati npr. na izlasku iz programa, kad vise ne namjeravamo koristiti klotoide.
    //________________________________________________________________________________________________________________________________________________
    static void DestroyLookupTable()
    {
        if(m_pLookupTable)
        {
            delete m_pLookupTable;
            m_pLookupTable = nullptr;
        }
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje parametara klotoide
    //________________________________________________________________________________________________________________________________________________
    bool Set(T x0, T y0, T ang0, T k0, T c, T length)
    {
        if(c == 0)
            return false;

        m_x0 = x0;
        m_y0 = y0;
        m_ang0 = ang0;
        m_k0 = k0;
        m_c = c;
        m_sqrtac = sqrt(fabs(c));
        m_C = 1 / m_sqrtac;
        m_K = k0 * m_C;
        m_Length = length;

        // Priprema clanova matrice rotacije
        T angRot = -k0 * k0 * (T)0.5 / c + ang0;
        T Cos = cos(angRot);
        T Sin = sin(angRot);
        m_r11 = Cos;
        m_r12 = -Sin;
        m_r21 = Sin;
        m_r22 = Cos;

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca sharpness (ostrinu) klotoide
    //________________________________________________________________________________________________________________________________________________
    inline T Get_c() const { return m_c; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca duljinu klotoide
    //________________________________________________________________________________________________________________________________________________
    inline T GetLength() const { return m_Length; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje nove duljine klotoide, uz nediranje ostalih parametara
    //________________________________________________________________________________________________________________________________________________
    inline void Length(T NewLength) { m_Length = NewLength; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje parametara klotoide - preko C (skalirnog faktora)
    // Set2 je nesto malo brzi od Set, jer se izbjegava sqrt
    //________________________________________________________________________________________________________________________________________________
    bool Set2(T x0, T y0, T ang0, T k0,
              T C, // Moze biti i negativno, tada ce i c biti negativan. Na kraju uzima se m_C=abs(C).
              T length)
    {
        if(C == 0)
            return false;

        m_x0 = x0;
        m_y0 = y0;
        m_ang0 = ang0;
        m_k0 = k0;
        m_C = abs(C);
        m_sqrtac = 1 / m_C;
        m_c = 1 / (m_C * m_C);
        if(C < 0)
            m_c = -m_c;
        m_K = k0 * m_C;
        m_Length = length;

        // Priprema clanova matrice rotacije
        T angRot = -k0 * k0 * (T)0.5 / m_c + ang0;
        T Cos = cos(angRot);
        T Sin = sin(angRot);
        // T Sin = sqrt(1 - Cos * Cos);
        m_r11 = Cos;
        m_r12 = -Sin;
        m_r21 = Sin;
        m_r22 = Cos;

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Dobivanje maksimalnog prevaljenog puta za kojeg klasa vraca ispravne vrijednosti (on ovisi o parametrima lookup tablice za x i y)
    // Ispravno podrucje parametra s je [-sEnd/sqrt(|c|)-k0/c, k0/c> U [-k0/c, sEnd/sqrt(|c|)-k0/c], sEnd je krajnji s u lookup tablici
    //________________________________________________________________________________________________________________________________________________
    /*T Get_sMax() const
    {
        assert(IsValid());
    }*/

    //________________________________________________________________________________________________________________________________________________
    //
    // Dobivanje tocke klotoide za zadani parametar s (prevaljeni put po klotoidi pocevsi od pocetne tocke)
    //________________________________________________________________________________________________________________________________________________
    bool GetPoint(T s, T& x, T& y, T& ang, T& k) const
    {
        assert(IsValid());

        if(m_C < m_pLookupTable->m_Cmin)
        {
            // Aproksimacija tockom
            x = m_x0;
            y = m_y0;
        }
        else if(m_k0 == 0 && m_C > m_pLookupTable->m_Cmax)
        {
            // Aproksimacija pravcem
            x = m_x0 + cos(m_ang0) * s;
            y = m_y0 + sin(m_ang0) * s;
        }
        else if(m_k0 != 0 && fabs(m_K) > m_pLookupTable->m_Kmax)
        {
            // Aproksimacija kruznicom
            x = m_x0 + 1 / m_k0 * (-sin(m_ang0) + sin(m_ang0 + m_k0 * s));
            y = m_y0 + 1 / m_k0 * (cos(m_ang0) - cos(m_ang0 + m_k0 * s));
        }
        else
        {
            // Aproksimacija tablicom

            // inputi za lookup tablicu (prema formuli u disertaciji, trebamo dvije tocke iz tablice)
            T s_L1 = m_sqrtac * (s + m_k0 / m_c);

            // Racunanje outputa lookup tablice (ti outputi daju tocke sa interpolacijom/ekstrapolacijom)
            T x1, y1;
            m_pLookupTable->GetBasicClothoidPoint(s_L1, x1, y1);

            T xz, yz;
            if(m_k0 == 0)
            {
                xz = x1 / m_sqrtac;
                yz = CNum<T>::Sign(m_c) * y1 / m_sqrtac;
            }
            else
            {
                T x2, y2;
                T s_L2 = m_sqrtac * m_k0 / m_c;
                m_pLookupTable->GetBasicClothoidPoint(s_L2, x2, y2);
                xz = (x1 - x2) / m_sqrtac;
                yz = CNum<T>::Sign(m_c) * (y1 - y2) / m_sqrtac;
            }

            x = m_x0 + m_r11 * xz + m_r12 * yz;
            y = m_y0 + m_r21 * xz + m_r22 * yz;
        }

        ang = m_ang0 + m_k0*s + m_c*s*s/2;
        k = m_k0 + m_c*s;

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Nalazi najmanji parametar 's' u kojem klotoida ima tangentu s tocno trazenom orijentacijom (a ne suprotnom).
    // Taj parametar predstavlja duljinu krivulje klotoide od pocetne tocke (x0,y0) do prve tocke sa zadanom orijentacijom tangente.
    // Trazimo samo pozitivne parametre st.
    //________________________________________________________________________________________________________________________________________________
    bool GetDistanceAtOrientation(T OrientationAng, // [rad] Trazena orijentacija tangente
                                  T& st             // trazeni parametar
    )
    {
        assert(IsValid());

        if(m_c == 0)
            return false; // c=0 i nema promjene orijentacije

        if(m_k0 == 0)
        {
            // Specijalni slucaj kad je poc. zakrivljenost nula, mnogo laksi za rijesiti

            T dang;
            if(m_c > 0)
                dang = NormAngle_Zero_2PI(OrientationAng - m_ang0);
            else
                dang = NormAngle_Minus2PI_Zero(OrientationAng - m_ang0);

            // Clan pod korijenom uvijek bi trebal biti pozitivan, jer za c>0 zakret je >=0, a za c<0 je <=0
            st = sqrt(2 / m_c * dang);
        }
        else
        {
            // Slucaj kad poc. zakrivljenost nije nula, tu imamo rjesavanje kvadratne jed. i dosta razlicitih slucajeva
            T D; // diskriminanta

            if(m_k0 > 0 && m_c > 0)
            {
                T dang = NormAngle_Zero_2PI(OrientationAng - m_ang0);
                D = m_k0 * m_k0 + 2 * m_c * dang;
                st = (-m_k0 + sqrt(D)) / m_c;
            }
            else if(m_k0 < 0 && m_c < 0)
            {
                T dang = NormAngle_Minus2PI_Zero(OrientationAng - m_ang0);
                D = m_k0 * m_k0 + 2 * m_c * dang;
                st = (-m_k0 - sqrt(D)) / m_c;
            }
            else if(m_k0 > 0 && m_c < 0)
            {
                // Tu orijentacija prvo raste pa pada.
                // Neke orijentacije se javljaju dvaput na krivulji pa imamo za njih imamo vise rjesenja - tu biramo manje rj.
                // To znaci da neki s-ovi nikad ne mogu biti rjesenja.

                // Provjerimo prvo dal je rjesenje u pocetnom dijelu di orijentacija raste
                T dang = NormAngle_Zero_2PI(OrientationAng - m_ang0);
                D = m_k0 * m_k0 + 2 * m_c * dang;
                if(D >= 0)
                {
                    // Da, rjesenje je u dijelu di orijentacija raste
                    st =
                        (-m_k0 + sqrt(D)) / m_c; // Kad bi tu stavili -sqrt(D), onda bi dobili ono drugo rjesenje tam di postoji dvoznacnost
                }
                else
                {
                    // Ne, rjesenje je u dijelu di orijentacija pada
                    dang = NormAngle_Minus2PI_Zero(OrientationAng - m_ang0);
                    D = m_k0 * m_k0 + 2 * m_c * dang;
                    st = (-m_k0 - sqrt(D)) / m_c;
                }
            }
            else
            {
                // Tu orijentacija prvo pada pa raste.
                // Neke orijentacije se javljaju dvaput na krivulji pa imamo za njih imamo vise rjesenja - tu biramo manje rj.
                // To znaci da neki s-ovi nikad ne mogu biti rjesenja.

                // Provjerimo prvo dal je rjesenje u pocetnom dijelu di orijentacija pada
                T dang = NormAngle_Minus2PI_Zero(OrientationAng - m_ang0);
                D = m_k0 * m_k0 + 2 * m_c * dang;
                if(D >= 0)
                {
                    // Da, rjesenje je u dijelu di orijentacija raste
                    st =
                        (-m_k0 - sqrt(D)) / m_c; // Kad bi tu stavili -sqrt(D), onda bi dobili ono drugo rjesenje tam di postoji dvoznacnost
                }
                else
                {
                    // Ne, rjesenje je u dijelu di orijentacija pada
                    T dang = NormAngle_Zero_2PI(OrientationAng - m_ang0);
                    D = m_k0 * m_k0 + 2 * m_c * dang;
                    st = (-m_k0 + sqrt(D)) / m_c;
                }
            }

            assert(st >= 0);
        }
        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Ova funkcija kreira defaultnu lookup tablicu za racunanje klotoida.
    // Treba ju pozvati jednom na pocetku programa. Na kraju programa treba zvati funkciju za brisanje tablice (DestroyLookupTable())
    //________________________________________________________________________________________________________________________________________________
    static bool CreateLookupTable()
    {
        if(m_pLookupTable)
            return true; // Tablica je vec kreirana

        // Defaultni parametri
        T sEnd = (T)46.1055; // (T)46.1055
        T Delta_s = (T)0.0035; //(T)0.0035
        T C_min = (T)0.000594474822523857;
        T C_max = (T)144.337566610772;
        T K_max = (T)44.3084716755815;
        T s_max = (T)5;
        T DeltaTh_max = (T)PI / 2;

        m_pLookupTable = new C2DClothoidLookupTable;
        if(!m_pLookupTable)
            return false;

        if(!m_pLookupTable->SetParms(Delta_s, sEnd, C_min, C_max, K_max, s_max, DeltaTh_max))
            return false;

        return true;
    }

public:
    static C2DClothoidLookupTable* m_pLookupTable; // Lookup tablica za osnovnu klotoidu koja je zajednicka za sve klase klotoide

public:
    T m_x0, m_y0; // pocetna tocka klotoide
    T m_ang0;     // pocetni kut (tangente)
    T m_k0;       // pocetna zakrivljenost
    T m_c;        // brzina zakretanja, ostrina, sharpness (c!=0, smije biti i <0)
    T m_C;        // (>0) faktor skaliranja (=1/sqrt(c))
    T m_K;        // umnozak k0*C
    T m_Length;   // duljina krivulje klotoide

    T m_sqrtac;                   // (>0) korijen od apsolutno c (radi optimizacije nekih proracuna) [ovo nije skrtac :-]
    T m_r11, m_r12, m_r21, m_r22; // elementi matrice rotacije vazni kod aproksimacije pomocu lookup tablice
};

}; // namespace Geom

#endif
