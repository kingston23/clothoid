#ifndef _2DRotation_H
#define _2DRotation_H

#include <assert.h>
#include <cmath>

namespace Geom
{
//=================================================================================================================================================================
//
// 2D rotacijska matrica i rutine za rotiranje tocke.
//
//=================================================================================================================================================================
template<typename T>
class C2DRotationMatrix
{
public:
    C2DRotationMatrix()
    {
        ang = 0;
        C = 1;
        S = 0;
    }

    C2DRotationMatrix(T ang) { SetAngle(ang); }

    void SetAngle(T ang)
    {
        this->ang = ang;
        C = cos(ang);
        S = sin(ang);
    }

    inline void RotatePoint(T& x, T& y) const
    {
        T x_ = x;
        x = C * x_ - S * y;
        y = S * x_ + C * y;
    }

    inline void UnRotatePoint(T& x, T& y) const
    {
        T x_ = x;
        x = C * x_ + S * y;
        y = -S * x_ + C * y;
    }

public:
    T ang;
    T C, S;
};
}; // namespace Geom
#endif