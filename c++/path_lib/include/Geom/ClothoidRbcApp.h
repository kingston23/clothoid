#ifndef _ClothoidRBCApp_H_
#define _ClothoidRBCApp_H_
#include <fstream>
#include <iostream>
#include <cmath>

using namespace std;

namespace Geom
{
//=================================================================================================================================================================
//
// Klasa za lookup aproksimaciju klotoide RBC-om (rational Bezier curve) prema clanku:
// Mont�s, N.; Herraez, A.; Armesto, L. & Tornero, J. Real-time Clothoid Approximation by Rational Bezier Curves Proceedings of
// International Conference on Robotics and Automation, 2008, 2246-2251
//
//=================================================================================================================================================================
template<class T>
class ClothoidRBCApp
{
    // deklaracije privatnih varijabli i metoda
    long Factorial[16]; // sluzi za izracun faktorijela
    int m_order;
    T K;            // homothetial factor as a scaling factor
    bool m_bStatus; // true ako je inicijalizacija OK

    T bernstein(T gamma, long k) const
    {
        T value = Factorial[m_order] / (Factorial[k] * Factorial[m_order - k]);
        T temp = (gamma_e - gamma) / (gamma_e - gamma_i);
        value = value * pow(1 - temp, k) * pow(temp, m_order - k);
        return value;
    }

    T computeRbc_x(T gamma) const
    {
        T up = 0;
        T down = 0;
        for(int k = 0; k <= m_order; k++)
        {
            T temp = m_weightsC[k] * bernstein(gamma, k);
            down += temp;
            up += temp * m_controlPointsC[k] * K;
        }
        return up / down;
    }

    T computeRbc_y(T gamma) const
    {
        T up = 0;
        T down = 0;
        for(int k = 0; k <= m_order; k++)
        {
            T temp = m_weightsS[k] * bernstein(gamma, k);
            down += temp;
            up += temp * m_controlPointsS[k] * K;
        }
        return up / down;
    }

    static const T PI;
    static const T gamma_i; // Pocetak intervala
    static const T gamma_e; // Kraj intervala

    T* m_controlPointsC;
    T* m_controlPointsS;
    T* m_weightsC;
    T* m_weightsS;

public:
    ClothoidRBCApp()
    {
        m_bStatus = false;

        K = 1;

        Factorial[0] = 1L;
        Factorial[1] = 1L;
        Factorial[2] = 2L;
        Factorial[3] = 6L;
        Factorial[4] = 24L;
        Factorial[5] = 120L;
        Factorial[6] = 720L;
        Factorial[7] = 5040L;
        Factorial[8] = 40320L;
        Factorial[9] = 362880L;
        Factorial[10] = 3628800L;
        Factorial[11] = 39916800L;
        Factorial[12] = 479001600L;
        Factorial[13] = 6227020800L;
        Factorial[14] = 87178291200L;
        Factorial[15] = 1307674368000L;

        // citanje binarne datoteke s parametrima RBCa izracunatim pomocu matlaba
        fstream myFile("ClothoidRBCApp_data.bin", ios::in | ios::binary);

        m_order = 0;
        m_controlPointsC = m_controlPointsS = m_weightsC = m_weightsS = nullptr;

        if(myFile)
        {
            myFile.read((char*)&m_order, sizeof(int));

            m_controlPointsC = new T[m_order + 1];
            m_controlPointsS = new T[m_order + 1];
            m_weightsC = new T[m_order + 1];
            m_weightsS = new T[m_order + 1];
        }

        if(myFile)
            myFile.read((char*)m_controlPointsC, sizeof(T) * (m_order + 1));

        if(myFile)
            myFile.read((char*)m_weightsC, sizeof(T) * (m_order + 1));

        if(myFile)
            myFile.read((char*)m_controlPointsS, sizeof(T) * (m_order + 1));

        if(myFile)
            myFile.read((char*)m_weightsS, sizeof(T) * (m_order + 1));

        if(myFile)
            m_bStatus = true;
    }

    // izracun rbc preko zakrivljenosti
    void rbcByCurvature(T k, T& x, T& y) const
    {
        T gamma = (k * K) / PI;
        x = computeRbc_x(gamma);
        y = computeRbc_y(gamma);
    }

    // izracun rbc preko duljine luka
    void rbcByArcLength(T L, T& x, T& y) const
    {
        T gamma = L / K;
        x = computeRbc_x(gamma);
        y = computeRbc_y(gamma);
    }

    // izracun rbc preko kuta tangente
    void rbcByTangentAngle(T t, T& x, T& y) const
    {
        T gamma = sqrt((2 * t) / PI);
        x = computeRbc_x(gamma);
        y = computeRbc_y(gamma);
    }

    ~ClothoidRBCApp(void)
    {
        delete[] m_controlPointsC;
        delete[] m_controlPointsS;
        delete[] m_weightsC;
        delete[] m_weightsS;
    }

    bool IsOK() const { return m_bStatus; }
};

template<class T>
const T ClothoidRBCApp<T>::PI = (T)3.1415926535897932384626433832795;

template<class T>
const T ClothoidRBCApp<T>::gamma_i = (T)0;

template<class T>
const T ClothoidRBCApp<T>::gamma_e = (T)1;

}; // namespace Geom

#endif