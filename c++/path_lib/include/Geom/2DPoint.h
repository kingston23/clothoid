#ifndef _2DPOINT_H_
#define _2DPOINT_H_
#pragma once

#include "GeometryBase.h"

#include <cmath>

namespace Geom
{
//=================================================================================================================================================================
//
// Neki algoritmi za rad s tockama.
//
//=================================================================================================================================================================
class C2DPointBase : public CGeometryBase
{
public:
    typedef double CoordType;

    static inline DIST_GM GetDistanceToPoint(COORD_GM x1, COORD_GM y1, COORD_GM x2, COORD_GM y2)
    {
        DIST_GM dx = x2 - x1;
        DIST_GM dy = y2 - y1;

        return (DIST_GM)sqrt(dx * dx + dy * dy);
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Provjerava kolinearnost triju tocaka.
    // Vraca true ako su kolinearne.
    //________________________________________________________________________________________________________________________________________________
    static bool PointsCollinear(COORD_GM x1, COORD_GM y1, COORD_GM x2, COORD_GM y2, COORD_GM x3, COORD_GM y3)
    {
        T DotProduct = (x2 - x1) * (y3 - y2) - (y2 - y1) * (x3 - x2);

        // Ako je navedeni produkt priblizno jednak nuli, tocke su kolinearne.
        return IsZero(DotProduct);
    }
};

//=================================================================================================================================================================
//
// Klasa za 2D tocku.
//
//=================================================================================================================================================================
class C2DPoint : public C2DPointBase
{
public:
    C2DPoint(){};

    C2DPoint(COORD_GM x_, COORD_GM y_)
    {
        x = x_;
        y = y_;
    }

    /*void operator = (const C2DPoint& Point)
    {
        x = Point.x;
        y = Point.y;
    }*/

    inline void SetPoint(COORD_GM x_, COORD_GM y_)
    {
        x = x_;
        y = y_;
    }

    DIST_GM GetDistanceToPoint(const C2DPoint* pPoint) const
    {
        assert(pPoint);
        return C2DPointBase::GetDistanceToPoint(x, y, pPoint->x, pPoint->y);
    }

    DIST_GM GetDistanceToPoint(const C2DPoint& Point) const { return C2DPointBase::GetDistanceToPoint(x, y, Point.x, Point.y); }

    DIST_GM GetDistanceToPoint(COORD_GM x_, COORD_GM y_) const { return C2DPointBase::GetDistanceToPoint(x, y, x_, y_); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Daje kut vektora od ove tocke do zadane tocke.
    // Tocke ne smiju biti identicne (nema provjere).
    //________________________________________________________________________________________________________________________________________________
    inline ANGLE_GM AngleToPoint(const C2DPoint& Point) const { return atan2(Point.y - y, Point.x - x); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Daje tocku na zadanom djelisnom omjeru izmedu dvije tocke
    //________________________________________________________________________________________________________________________________________________
    C2DPoint
    PointBetween(const C2DPoint& Point2,
                 T DivideRatio // Ako je 0, dobije se prva tocka (this), ako je 1 dobije se tocka Point2, ako je u rasponu <0,1> dobije se
                               // tocka izmedu. Ako je izvan tog raspona, dobije se neka tocka koja nije izmedu zadanih tocaka.
                 ) const
    {
        return C2DPoint(x + (Point2.x - x) * DivideRatio, y + (Point2.y - y) * DivideRatio);
    }

    // Daje kvadrat udaljenosti
    DIST_GM GetDistance2ToPoint(COORD_GM x_, COORD_GM y_) const
    {
        DIST_GM dx = x_ - x;
        DIST_GM dy = y_ - y;
        return (dx * dx + dy * dy);
    }

    // Daje kvadrat udaljenosti
    DIST_GM GetDistance2ToPoint(const C2DPoint* pPoint) const
    {
        DIST_GM dx = pPoint->x - x;
        DIST_GM dy = pPoint->y - y;
        return (dx * dx + dy * dy);
    }

    inline void Translate(DIST_GM dx, DIST_GM dy)
    {
        x += dx;
        y += dy;
    }

    inline bool Equal(const C2DPoint* pPoint) const { return (x == pPoint->x && y == pPoint->y); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja ovu tocku kao sredisnju tocku izmedu dvije zadane tocke
    //________________________________________________________________________________________________________________________________________________
    void MiddlePoint(const C2DPoint& P1, const C2DPoint& P2)
    {
        x = (P1.x + P2.x) * (T)0.5;
        y = (P1.y + P2.y) * (T)0.5;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Provjerava kolinearnost ove tocke sa jos dvije tocke.
    // Vraca true ako su kolinearne.
    //________________________________________________________________________________________________________________________________________________
    bool PointsCollinear(const C2DPoint& P1, const C2DPoint& P2) { return C2DPointBase::PointsCollinear(x, y, P1.x, P1.y, P2.x, P2.y); }

    COORD_GM x, y;
};

}; // namespace Geom

#endif
