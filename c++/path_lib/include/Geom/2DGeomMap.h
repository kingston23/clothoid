#ifndef _2DGEOMMAP_H_
#define _2DGEOMMAP_H_

#include "2DObject.h"

#include <vector>

// using namespace Geom;
namespace Geom
{
//________________________________________________________________________________________________________________________
//
// 2D geometrijska mapa
//________________________________________________________________________________________________________________________
class C2DGeomMap : public CGeometryBase
{
public:
    C2DGeomMap(){};

    bool AddObject(C2DObject* pObject)
    {
        m_Map.push_back(pObject);
        return true;
    }

    void Free() { m_Map.clear(); }

    void Resize(int n) { m_Map.resize(n); }

    bool ReserveObjects(int nObjects)
    {
        try
        {
            m_Map.reserve(nObjects);
        }
        catch(...)
        {
            return false;
        }

        return true;
    };

    unsigned int GetNumObjects() const { return (unsigned int)m_Map.size(); };

    C2DObject* GetObject(unsigned int iObs) const { return m_Map[iObs]; };

public:
    std::vector<C2DObject*> m_Map;
};

}; // namespace Geom

#endif

// �cause I get lost inside my fear
