#ifndef _LINE_H_
#define _LINE_H_

#include "2DPoint.h"
#include "GeometryBase.h"
#include "Math/AngleRange.h"

#include <cmath>
#include <limits>

namespace Geom
{
//=================================================================================================================================================================
//
// Bazna klasa za implicitne pravce s jednadzbom Ax+By+C=0.
//
//=================================================================================================================================================================
class CLineImpBase : public CGeometryBase
{
    // Tipovi su definirani u baznoj klasi
    // Low level operacije sa pravcima
public:
    static inline void GetDirectionVector(LNCOEFF_GM A, LNCOEFF_GM B, VECTORCOMP_GM* i, VECTORCOMP_GM* j)
    {
        *i = B;
        *j = -A;
    }

    static inline void GetNormalVector(LNCOEFF_GM A, LNCOEFF_GM B, VECTORCOMP_GM* i, VECTORCOMP_GM* j)
    {
        *i = A;
        *j = B;
    }

    // Proracunava koef. C pravca ako je poznat smjer (tj. A i B) i tocka kroz koju prolazi pravac
    static inline void GetLineFromDirectionAndPoint(LNCOEFF_GM A, LNCOEFF_GM B, COORD_GM x, COORD_GM y, LNCOEFF_GM* C)
    {
        *C = -(A * x + B * y);
    }

    // Proracunava koeficijente pravca ako je poznat smjer zadan kao kut i tocka kroz koju prolazi pravac
    static inline void GetLineFromDirectionAndPoint(ANGLERAD_GM Direction, COORD_GM x, COORD_GM y, LNCOEFF_GM* A, LNCOEFF_GM* B,
                                                    LNCOEFF_GM* C)
    {
        *A = (LNCOEFF_GM)(-sin(Direction));
        *B = (LNCOEFF_GM)cos(Direction);
        *C = -((*A) * x + (*B) * y);
    }

    static inline ANGLERAD_GM GetLineDirection(LNCOEFF_GM A, LNCOEFF_GM B) { return (ANGLERAD_GM)atan2(-A, B); }

    // Proracunava pravac iz dviju tocaka. Smjer pravca bude od prve do druge tocke.
    static inline void GetLineFromPoints(COORD_GM x1, COORD_GM y1, COORD_GM x2, COORD_GM y2, LNCOEFF_GM* A, LNCOEFF_GM* B, LNCOEFF_GM* C)
    {
        *A = y1 - y2;
        *B = x2 - x1;
        *C = x1 * y2 - x2 * y1;
    }

    // Provjerava da li su dva pravca paralelna
    static inline bool CheckIfParallel(LNCOEFF_GM A1, LNCOEFF_GM B1, LNCOEFF_GM A2, LNCOEFF_GM B2) { return (A1 * B2 == A2 * B1); }

    // Provjerava da li su dva pravca okomita
    static inline bool CheckIfPerpendicular(LNCOEFF_GM A1, LNCOEFF_GM B1, LNCOEFF_GM A2, LNCOEFF_GM B2) { return (A1 * A2 == -B1 * B2); }

    // Pronalazi presjecnu tocku dvaju pravaca. Ako su pravci paralelni vraca fols.
    static inline bool GetIntersectionPoint(LNCOEFF_GM A1, LNCOEFF_GM B1, LNCOEFF_GM C1, LNCOEFF_GM A2, LNCOEFF_GM B2, LNCOEFF_GM C2,
                                            COORD_GM* x, COORD_GM* y)
    {
        COORD_GM Temp = A1 * B2 - A2 * B1;
        if(!Temp)
            return false;
        *x = (B1 * C2 - B2 * C1) / Temp;
        *y = (A2 * C1 - A1 * C2) / Temp;
        return true;
    }

    // Pronalazi okomiti pravac koji prolazi kroz zadanu tocku
    // Smjer pravca bude +90 u odnosu na polazni pravac
    static inline void GetPerpendicularLineThroughPoint(LNCOEFF_GM A, LNCOEFF_GM B, COORD_GM x, COORD_GM y, LNCOEFF_GM* Ap, LNCOEFF_GM* Bp,
                                                        LNCOEFF_GM* Cp)
    {
        *Ap = -B;
        *Bp = A;
        *Cp = B * x - A * y;
    }

    // Pronalazi tocku na pravcu na koju se projicira zadana tocka
    static inline void GetProjectionPointOnLine(LNCOEFF_GM A, LNCOEFF_GM B, LNCOEFF_GM C, COORD_GM x, COORD_GM y, COORD_GM* px,
                                                COORD_GM* py)
    {
        COORD_GM AA = A * A;
        COORD_GM BB = B * B;
        COORD_GM AB = A * B;
        COORD_GM AABB = AA + BB;

        *px = (BB * x - AB * y - A * C) / AABB;
        *py = (-AB * x + AA * y - B * C) / AABB;
    }

    // Pronalazi simetricnu tocku u odnosu na zadani pravac
    static inline void GetSymmetricPoint(LNCOEFF_GM A, LNCOEFF_GM B, LNCOEFF_GM C, COORD_GM x, COORD_GM y, COORD_GM* sx, COORD_GM* sy)
    {
        COORD_GM AA = A * A;
        COORD_GM BB = B * B;
        COORD_GM AB = A * B;
        COORD_GM AABB = AA + BB;

        *sx = ((BB - AA) * x - 2 * AB * y - 2 * A * C) / AABB;
        *sy = (-2 * AB * x + (AA - BB) * y - 2 * B * C) / AABB;
    }

    // Racuna kut izmedu dva pravca u stupnjevima.
    // Kut moze biti i negativan - u tom slucaju drugi pravac se dobije rotacijom prvog za negativni kut.
    static inline ANGLE_GM GetAngleBetweenLinesInDeg(LNCOEFF_GM A1, LNCOEFF_GM B1, LNCOEFF_GM A2, LNCOEFF_GM B2)
    {
        return (ANGLE_GM)((ANGLERAD_GM)atan((A1 * B2 - A2 * B1) / (A1 * A2 + B1 * B2)) * 180.f / PI);
    }

    // Racuna kut izmedu dva pravca u radijanima
    static inline ANGLERAD_GM GetAngleBetweenLines(LNCOEFF_GM A1, LNCOEFF_GM B1, LNCOEFF_GM A2, LNCOEFF_GM B2)
    {
        return (ANGLERAD_GM)atan((A1 * B2 - A2 * B1) / (A1 * A2 + B1 * B2));
    }

    // Racuna A i B koeficijente pravca koji s dobiva rotiranjem originalnog pravca za zadani kut
    // Kut je u stupnjevima.
    static inline void GetRotatedLine(LNCOEFF_GM A, LNCOEFF_GM B, ANGLE_GM Ang, LNCOEFF_GM* Ar, LNCOEFF_GM* Br)
    {
        LNCOEFF_GM TgAng = (LNCOEFF_GM)tan(Ang);
        *Ar = A - B * TgAng;
        *Br = A * TgAng + B;
    }

    // Racuna udaljenost tocke od pravca
    // Ako je tocka "desno" od pravca, tada je udaljenost negativna
    static inline DIST_GM GetDistanceToPoint(LNCOEFF_GM A, LNCOEFF_GM B, LNCOEFF_GM C, COORD_GM x, COORD_GM y)
    {
        return (DIST_GM)(A * x + B * y + C) / (DIST_GM)sqrt(A * A + B * B);
    }

    // Ispituje da li je tocka (strogo) lijevo od pravca
    static inline bool IsPointLeft(LNCOEFF_GM A, LNCOEFF_GM B, LNCOEFF_GM C, COORD_GM x, COORD_GM y)
    {
        return (A * x + B * y + C > 0 ? true : false);
    }

    // Ispituje da li je tocka (strogo) desno od pravca
    static inline bool IsPointRight(LNCOEFF_GM A, LNCOEFF_GM B, LNCOEFF_GM C, COORD_GM x, COORD_GM y)
    {
        return (A * x + B * y + C < 0 ? true : false);
    }

    static inline void GetMiddlePoint(COORD_GM x1, COORD_GM y1, COORD_GM x2, COORD_GM y2, COORD_GM* x, COORD_GM* y)
    {
        *x = (x1 + x2) / (COORD_GM)2.;
        *y = (y1 + y2) / (COORD_GM)2.;
    }

    // Daje tocku na pravcu okomitom na zadani pravac koji prolazi kroz tocku (x,y).
    // Tocka bude udaljena od pravca za d. Udalj. moze biti negativna.
    // Nije testirano.
    static inline void GetPointOnPerpendicularLine(LNCOEFF_GM A, LNCOEFF_GM B, COORD_GM x, COORD_GM y, DIST_GM d, COORD_GM* px,
                                                   COORD_GM* py)
    {
        NUM_GM dAABB = d / (NUM_GM)sqrt(A * A + B * B);
        *px = x + dAABB * A;
        *py = y + dAABB * B;
    }

    // Vraca koeficijent C pravca koji je paralelan sa zadanim pravcem i udaljen od njega za d, koji moze biti
    // pozitivan i negativan. Za pozitivan d, dobiveni pravac je lijevo od zadanog. Koeficijenti A i B naravno
    // ostaju isti.
    // Nije testirano.
    static inline LNCOEFF_GM GetParallelLine(LNCOEFF_GM A, LNCOEFF_GM B, LNCOEFF_GM C, DIST_GM d)
    {
        return (C - d * (NUM_GM)sqrt(A * A + B * B));
    }

    // Daje simetralu izmedu dva pravca. Smjer simetrale bit ce srednja vrijednost smjerova dvaju pravaca.
    // Opcenito svaka 2 pravca imaju dvije simetrale. Koja od te dvije simetrale ce se dobiti, i smjer te simetrale, ovisi o smjerovima 1.
    // i 2. pravca. To znaci da ako zelimo dobiti onu drugu simetralu, treba uzeti suprotne predznake A,B,C od jednog od pravaca. Ak su
    // pravci paralelni (priblizno eps!), vraca se false.
    static inline bool GetSymmetricalLine(LNCOEFF_GM A1, LNCOEFF_GM B1, LNCOEFF_GM C1, LNCOEFF_GM A2, LNCOEFF_GM B2, LNCOEFF_GM C2,
                                          LNCOEFF_GM& A, LNCOEFF_GM& B, LNCOEFF_GM& C)
    {
        NUM_GM K1 = (LNCOEFF_GM)sqrt(A1 * A1 + B1 * B1);
        NUM_GM K2 = (LNCOEFF_GM)sqrt(A2 * A2 + B2 * B2);

        NUM_GM K1K2 = K1 * K2;

        A = (LNCOEFF_GM)((A1 * K2 + A2 * K1) / K1K2);
        B = (LNCOEFF_GM)((B1 * K2 + B2 * K1) / K1K2);
        C = (LNCOEFF_GM)((C1 * K2 + C2 * K1) / K1K2);

        if(IsZero(A) && IsZero(B))
            return false;
        else
            return true;
    }
};

//=================================================================================================================================================================
//
// Klasa za implicitne pravce s jednadzbom Ax+By+C=0.
//
//=================================================================================================================================================================
class CLineImp : public CGeometryBase
{
public:
    LNCOEFF_GM A, B, C;

    //________________________________________________________________________________________________________________________________________________
    //
    // Testiranje dal je pravac dobro zadan (ne smije biti A=B=0)
    //________________________________________________________________________________________________________________________________________________
    bool IsOk() const
    {
        if(A == 0 && B == 0)
            return false;

        return true;
    }

    void Assign(LNCOEFF_GM A_, LNCOEFF_GM B_, LNCOEFF_GM C_)
    {
        A = A_;
        B = B_;
        C = C_;
    }

    void Invert()
    {
        A = -A;
        B = -B;
        C = -C;
    }

    void LineFromDirectionAndPoint(ANGLERAD_GM Direction, COORD_GM x, COORD_GM y)
    {
        CLineImpBase::GetLineFromDirectionAndPoint(Direction, x, y, &A, &B, &C);
        assert(!(A == 0 && B == 0));
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Racuna pravac iz smjera (ako je poznat sin i cos smjera) i tocke
    //________________________________________________________________________________________________________________________________________________
    void LineFromDirectionAndPoint(NUM_GM Sin, NUM_GM Cos, COORD_GM x, COORD_GM y)
    {
        A = -Sin;
        B = Cos;
        CLineImpBase::GetLineFromDirectionAndPoint(A, B, x, y, &C);
        assert(!(A == 0 && B == 0));
    }

    void LineFromPoints(COORD_GM x1, COORD_GM y1, COORD_GM x2, COORD_GM y2)
    {
        CLineImpBase::GetLineFromPoints(x1, y1, x2, y2, &A, &B, &C);
        assert(!(A == 0 && B == 0));
    }

    void LineFromPoints(const C2DPoint* pPoint1, const C2DPoint* pPoint2)
    {
        CLineImpBase::GetLineFromPoints(pPoint1->x, pPoint1->y, pPoint2->x, pPoint2->y, &A, &B, &C);
        assert(!(A == 0 && B == 0));
    }

    DIST_GM GetDistanceToPoint(COORD_GM x, COORD_GM y) const { return CLineImpBase::GetDistanceToPoint(A, B, C, x, y); }

    // Daje kvadrat udaljenosti pravca od tocke
    DIST_GM GetDistance2ToPoint(COORD_GM x, COORD_GM y) const
    {
        DIST_GM dn = A * x + B * y + C;
        assert(A * A + B * B != 0);
        return (dn * dn) / (A * A + B * B);
    }

    // Racuna udaljenost projekcija dviju tocaka na pravac
    // Ak idemo po pravcu u smjeru pravca i prva je p1 a druga p2, onda je udalj pozitivna, a inace je neg
    DIST_GM GetPointProjectionsDist(COORD_GM x1, COORD_GM y1, COORD_GM x2, COORD_GM y2) const
    {
        return (B * (x2 - x1) + A * (y1 - y2) / sqrt(A * A + B * B));
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Daje tocku na okomitom pravcu udaljenu za d od pocetnog pravca.
    // Nije testirano.
    //________________________________________________________________________________________________________________________________________________
    void GetPointOnPerpendicularLine(COORD_GM x1, COORD_GM y1, // tocka na pravcu od koje se mjeri udaljenost
                                     DIST_GM d, // udaljenost trazene tocke od pravca, tj. tocke (x1,y1) na pravcu, moze biti i negativna
                                     COORD_GM& x, COORD_GM& y // (out) trazena tocka
    ) const
    {
        assert(IsOk());

        NUM_GM dAABB = d / (NUM_GM)sqrt(A * A + B * B);
        x = x1 + dAABB * A;
        y = y1 + dAABB * B;
    }

    bool GetIntersectionPoint(const CLineImp& OtherLine, COORD_GM& xc, COORD_GM& yc) const
    {
        return CLineImpBase::GetIntersectionPoint(A, B, C, OtherLine.A, OtherLine.B, OtherLine.C, &xc, &yc);
    }

    bool IsPointLeft(COORD_GM x, COORD_GM y) const { return CLineImpBase::IsPointLeft(A, B, C, x, y); }

    bool IsPointLeft(C2DPoint* pPoint) const { return CLineImpBase::IsPointLeft(A, B, C, pPoint->x, pPoint->y); }

    bool IsPointRight(COORD_GM x, COORD_GM y) const { return CLineImpBase::IsPointRight(A, B, C, x, y); }

    bool IsPointRight(C2DPoint* pPoint) const { return CLineImpBase::IsPointRight(A, B, C, pPoint->x, pPoint->y); }

    // Racuna pravac paralelan pravcu pLine na udaljenosti d od tog pravca
    void GetLineFromParallelLine(CLineImp* pLine, DIST_GM d)
    {
        A = pLine->A;
        B = pLine->B;
        C = CLineImpBase::GetParallelLine(A, B, pLine->C, d);
    }

    // Pomice pravac lijevo ili desno za d (za d>0 pomice ulijevo), pritom zadrzava smjer pravca
    void MoveLine(DIST_GM d) { C = CLineImpBase::GetParallelLine(A, B, C, d); }

    // Racuna pravac okomit na pravac pLine koji prolazi kroz x,y
    // Pravac bu imal smjer kao da se pLine zarotira u pozitivnom smjeru za 90 stupnjeva
    void GetLineFromPerpendicularLine(CLineImp* pLine, COORD_GM x, COORD_GM y)
    {
        CLineImpBase::GetPerpendicularLineThroughPoint(pLine->A, pLine->B, x, y, &A, &B, &C);
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja pravac tako da bude okomit na zadani pravac i da prolazi kroz zadanu tocku.
    // Smjer pravca bude +90 u odnosu na zadani pravac.
    //________________________________________________________________________________________________________________________________________________
    inline void GetPerpendicularLineThroughPoint(const CLineImp& Line, T x, T y)
    {
        A = -Line.B;
        B = Line.A;
        C = Line.B * x - Line.A * y;
    }

    // Vraca kut u rasponu -pi,pi
    ANGLERAD_GM GetAngle() const { return (ANGLERAD_GM)atan2(-A, B); }

    ANGLERAD_GM GetAngleBetweenLines(CLineImp& OtherLine)
    {
        LNCOEFF_GM A2 = OtherLine.A;
        LNCOEFF_GM B2 = OtherLine.B;
        return (ANGLERAD_GM)atan2(A * B2 - A2 * B, A * A2 + B * B2);
    }

    bool IsParallel(CLineImp& OtherLine) const { return (A * OtherLine.B == OtherLine.A * B); }

    COORD_GM GetX(COORD_GM y) const
    {
        assert(A != 0);
        return -(B * y + C) / A;
    }

    COORD_GM GetY(COORD_GM x) const
    {
        assert(B != 0);
        return -(A * x + C) / B;
    }

    // Vraca true ako je pravac vise horizontalan nego okomit. Ako je pod kutem 45 onda je takionak
    // svejedno
    bool IsVertical() const { return abs(A) > abs(B); }

    bool IsHorizontal() const { return abs(B) > abs(A); }

    void GetPointProjection(COORD_GM x, COORD_GM y, COORD_GM& px, COORD_GM& py) const
    {
        CLineImpBase::GetProjectionPointOnLine(A, B, C, x, y, &px, &py);
    }

    // Paznja: funkcije za simetrale nisu provjerene

    // Daje simetralu izmedu dva pravca
    void GetSymmetricalLine(CLineImp& Line1, CLineImp& Line2)
    {
        CLineImpBase::GetSymmetricalLine(Line1.A, Line1.B, Line1.C, Line2.A, Line2.B, Line2.C, A, B, C);
    }

    // Ove funkcije daju simetralu izmedu dva pravca za sve moguce kombinacije smjerova 1. i 2. pravca

    // Isto kao i obican GetSymmetricalLine
    void GetSymmetricalLine1(CLineImp& Line1, CLineImp& Line2)
    {
        CLineImpBase::GetSymmetricalLine(Line1.A, Line1.B, Line1.C, Line2.A, Line2.B, Line2.C, A, B, C);
    }
    // Uzima suprotan smjer od Line1
    void GetSymmetricalLine2(CLineImp& Line1, CLineImp& Line2)
    {
        CLineImpBase::GetSymmetricalLine(-Line1.A, -Line1.B, -Line1.C, Line2.A, Line2.B, Line2.C, A, B, C);
    }
    // Uzima suprotan smjer od Line1 i od Line2. Dobije se isti pravac kao i sa GetSymmetricalLine1 ali suprotnog smjera
    void GetSymmetricalLine3(CLineImp& Line1, CLineImp& Line2)
    {
        CLineImpBase::GetSymmetricalLine(Line1.A, Line1.B, Line1.C, Line2.A, Line2.B, Line2.C, A, B, C);
        A = -A;
        B = -B;
        C = -C;
    }
    // Uzima suprotan smjer od Line2. Dobije se isti pravac kao i sa GetSymmetricalLine2 ali suprotnog smjera
    void GetSymmetricalLine4(CLineImp& Line1, CLineImp& Line2)
    {
        CLineImpBase::GetSymmetricalLine(Line1.A, Line1.B, Line1.C, -Line2.A, -Line2.B, -Line2.C, A, B, C);
    }

    // Racuna smjer od zadane pocetne tocke prema tocki pravca koja je na udaljenosti LookAheadDistance od pocetne tocke.
    // Ak je pocetna tocka dalje od LookAheadDistance, onda vraca smjer kojim se ide okomito prema pravcu.
    ANGLERAD_GM GetDirectionToLookAheadPoint(COORD_GM x, COORD_GM y, DIST_GM LookAheadDistance)
    {
        ANGLERAD_GM Direction;

        DIST_GM DistToPoint = abs(GetDistanceToPoint(x, y));

        bool bIsPointRight = IsPointRight(x, y);

        ANGLERAD_GM LineDirection = GetAngle();

        if(DistToPoint > LookAheadDistance)
        {
            // Udaljenost tocke od pravca je veca od LookAheadDistance, pa picimo okomito prema pravcu
            if(bIsPointRight)
                Direction = LineDirection + PI / 2;
            else
                Direction = LineDirection - PI / 2;
        }
        else
        {
            // Udaljenost tocke od pravca je manja od LookAheadDistance, pa konstruiramo trokut kojem hipotenuza pocinje od tocke
            // i zavrsava u pravcu (u smjeru pravca), jedna kateta mu lezi na pravcu, a jedna ide od tocke prema pravcu okomito.
            // Onda racunamo kut izmedu katete i hipotenuze.
            ANGLERAD_GM dAngle = (ANGLERAD_GM)acos(DistToPoint / LookAheadDistance); // posto je argument poz, rezultat ide od 0 do pi/2

            if(bIsPointRight)
                Direction = LineDirection + PI / 2 - dAngle;
            else
                Direction = LineDirection - PI / 2 + dAngle;
        }

        return Direction;
    }

    // Daje smjer polupravca koji ide prema liniji iz zadane tocke
    ANGLE_GM GetDirectionToLine(COORD_GM x, COORD_GM y)
    {
        if(IsPointRight(x, y))
            return GetAngle() + PI / 2;
        else
            return GetAngle() - PI / 2;
    }
};

//=================================================================================================================================================================
//
// Klasa za pravce koja koristi parametarski oblik jednadzbe pravca:
// x(d) = x0 + C * d
// y(d) = y0 + S * d
//
//=================================================================================================================================================================
class CLinePar : public CGeometryBase
{
public:
    LNCOEFF_GM C, S;         // Koeficijenti smjera
    C2DPoint p0;             // Pocetna tocka
    ANGLERAD_GM m_Direction; // [rad], smjer pravca, jednak je atan2(S,C)

    CLinePar() {}

    CLinePar(T Direction, T x0, T y0) { LineFromDirectionAndPoint(Direction, x0, y0); }

    /*void operator = (CLinePar& Line)
    {
        C = Line.C;
        S = Line.S;
        p0 = Line.p0;
        m_Direction = Line.m_Direction;
    }*/

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje pravca iz smjera i pocetne tocke.
    //________________________________________________________________________________________________________________________________________________
    void LineFromDirectionAndPoint(ANGLERAD_GM Direction, COORD_GM x, COORD_GM y)
    {
        m_Direction = Direction;

        p0.x = x;
        p0.y = y;

        C = (LNCOEFF_GM)cos(Direction);
        S = (LNCOEFF_GM)sin(Direction);
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje pravca iz dvije tocke.
    // Prva tocka se uzima kao pocetna. Smjer se uzima od prve do druge tocke.
    // Tocke moraju biti razlicite (eps), inace se vraca false.
    //________________________________________________________________________________________________________________________________________________
    bool LineFromPoints(T x1, T y1, T x2, T y2)
    {
        T dx = x2 - x1;
        T dy = y2 - y1;
        T Length = sqrt(dx * dx + dy * dy);
        if(IsZero(Length))
            return false;

        T LengthInv = 1 / Length;
        C = dx * LengthInv;
        S = dy * LengthInv;

        p0.x = x1;
        p0.y = y1;

        m_Direction = atan2(dy, dx);

        return true;
    }

    inline bool LineFromPoints(const C2DPoint& Pt1, const C2DPoint& Pt2) { return LineFromPoints(Pt1.x, Pt1.y, Pt2.x, Pt2.y); }

    inline ANGLERAD_GM GetDirection() const { return m_Direction; }

    inline C2DPoint StartPoint() const { return p0; }

    inline void GetPoint(DIST_GM d, COORD_GM& x, COORD_GM& y) const
    {
        x = p0.x + C * d;
        y = p0.y + S * d;
    }

    inline void GetPoint(DIST_GM d, C2DPoint& Point) const
    {
        Point.x = p0.x + C * d;
        Point.y = p0.y + S * d;
    }

    inline void GetLineImp(CLineImp& ImpLine) const
    {
        ImpLine.A = -S;
        ImpLine.B = C;
        ImpLine.C = S * p0.x - C * p0.y;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Testira (pribliznu) paralenost s drugim parametarskim pravcem
    //________________________________________________________________________________________________________________________________________________
    bool IsParallel(const CLinePar& Line2) const
    {
        // Ovo bi se moglo provjeriti i usporedbom kuteva, ali s njima imamo problem pomaka za 360 stupnjeva, i problem ako su smjerovi
        // suprotni. Zato provjeravamo uz pomoc sinusa i kosinusa kuta.
        if(IsEqual(S * Line2.C, C * Line2.S))
            return true;

        return false;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Testira (pribliznu) identicnost dvaju pravaca (uz uvjet da su smjerovi jednaki, a nisu suprotni, tj. pomaknuti za 180 stupnjeva!!!)
    //________________________________________________________________________________________________________________________________________________
    bool IsIdentical(const CLinePar& Line2) const
    {
        return (IsEqual(S, Line2.S) && IsEqual(C, Line2.C) && IsPointAtLine(Line2.p0.x, Line2.p0.y));
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Daje tocku sjecista dvaju parametarskih pravaca, ili false ako su pravci paralelni (priblizno eps).
    //________________________________________________________________________________________________________________________________________________
    bool GetIntersection(const CLinePar& Line2, COORD_GM& x, COORD_GM& y) const
    {
        if(!IsParallel(Line2))
        {
            T d1 = (Line2.C * (Line2.p0.y - p0.y) + Line2.S * (p0.x - Line2.p0.x)) / (S * Line2.C - C * Line2.S);

            GetPoint(d1, x, y);

            return true;
        }
        else
            return false;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Daje simetralu izmedu dva pravca. Smjer simetrale bit ce srednja vrijednost smjerova dvaju pravaca.
    // Opcenito svaka 2 pravca imaju dvije simetrale. Koja od te dvije simetrale ce se dobiti, i smjer te simetrale, ovisi o smjerovima 1.
    // i 2. pravca. To znaci da ako zelimo dobiti onu drugu simetralu, treba uzeti suprotni smjer jednog od pravaca. Ak su pravci paralelni
    // (priblizno eps!), vraca se srednja linija izmedu njih ako su istog smjera, odnosno okomica na oba pravca ako su suprotnog smjera.
    // Dakle funkcija uvijek vraca nekakvo rjesenje!
    //________________________________________________________________________________________________________________________________________________
    void GetSymmetricalLine(CLinePar& Line2, CLinePar& SymmetryAxis) const
    {
        // Odredi novi kut kao sredinu kuteva dvaju pravaca.
        // Potrebna je dosta slozena logika jer rezultati inace ne bi bili dobri ako je jedan kut u 2. kvadrantu, a drugi u 3. kvadrantu
        T NewDirection =
            CAngleRange::Range_MinusPI_PI(m_Direction + (T)0.5 * CAngleRange::Range_MinusPI_PI(Line2.m_Direction - m_Direction));

        T xInter, yInter; // Sjeciste dvaju pravaca

        if(!GetIntersection(Line2, xInter, yInter))
        {
            // Paralelni su
            xInter = (T)0.5 * (p0.x + Line2.p0.x);
            yInter = (T)0.5 * (p0.y + Line2.p0.y);
        }

        SymmetryAxis.LineFromDirectionAndPoint(NewDirection, xInter, yInter);
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Daje udaljenost do tocke.
    // Ako je tocka "desno" od pravca, tada je udaljenost negativna
    //________________________________________________________________________________________________________________________________________________
    inline T GetDistanceToPoint(T x, T y) const { return S * (p0.x - x) + C * (y - p0.y); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Provjerava da li je tocka lijevo od pravca.
    // Ovo moze biti nepouzdano ako je tocka jako blizu pravca!
    //________________________________________________________________________________________________________________________________________________
    inline bool IsPointLeft(COORD_GM x, COORD_GM y) const { return (S * (p0.x - x) + C * (y - p0.y) > 0 ? true : false); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Provjerava da li je tocka na pravcu (priblizno)
    //________________________________________________________________________________________________________________________________________________
    inline bool IsPointAtLine(T x, T y) const { return IsZero(GetDistanceToPoint(x, y)); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Ako je poznat x, daje y (pravac ne smije biti vertikalan - nema provjere za to)
    //________________________________________________________________________________________________________________________________________________
    inline T GetY(COORD_GM x) const
    {
        assert(C != 0);
        T d = (x - p0.x) / C;
        return p0.y + S * d;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Ako je poznat y, daje x (pravac ne smije biti horizontalan - nema provjere za to)
    //________________________________________________________________________________________________________________________________________________
    inline T GetX(COORD_GM y) const
    {
        assert(S != 0);
        T d = (y - p0.y) / S;
        return p0.x + C * d;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Ako je poznata tocka (koja mora biti na pravcu!), ovo daje parametar d
    //________________________________________________________________________________________________________________________________________________
    inline T GetD(COORD_GM x, COORD_GM y) const
    {
        if(abs(C) > abs(S))
            return (x - p0.x) / C;
        else
            return (y - p0.y) / S;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Rotiranje pravca oko pocetne tocke za DeltaAngle [rad] (trenutnom smjeru se dodaje DeltaAngle)
    //________________________________________________________________________________________________________________________________________________
    inline void Rotate(T DeltaAngle)
    {
        m_Direction = m_Direction + DeltaAngle;

        C = cos(m_Direction);
        S = sin(m_Direction);
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Translatiranje pravca za (dx, dy)
    //________________________________________________________________________________________________________________________________________________
    inline void Translate(T dx, T dy)
    {
        p0.x += dx;
        p0.y += dy;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja suprotan smjer (dodaje se pi trenutnom smjeru)
    //________________________________________________________________________________________________________________________________________________
    inline void SetOppositeDirection()
    {
        m_Direction += PI;
        C = -C;
        S = -S;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja ovaj pravac kao os simetrije izmedu dva zadana pravca. Smjer simetrale bit ce srednja vrijednost smjerova dvaju pravaca.
    // Opcenito svaka 2 pravca imaju dvije simetrale. Koja od te dvije simetrale ce se dobiti, i smjer te simetrale, ovisi o smjerovima 1.
    // i 2. pravca. To znaci da ako zelimo dobiti onu drugu simetralu, treba uzeti suprotni smjer jednog od pravaca. Ak su pravci paralelni
    // (priblizno eps!), vraca se srednja linija izmedu njih ako su istog smjera, odnosno okomica na oba pravca ako su suprotnog smjera.
    // Dakle funkcija uvijek vraca nekakvo rjesenje!
    //________________________________________________________________________________________________________________________________________________
    void SymmetryAxis(const CLinePar& Line1, const CLinePar& Line2)
    {
        // Odredi novi kut kao sredinu kuteva dvaju pravaca.
        // Potrebna je dosta slozena logika jer rezultati inace ne bi bili dobri ako je jedan kut u 2. kvadrantu, a drugi u 3. kvadrantu
        T NewDirection = CAngleRange::Range_MinusPI_PI(Line1.m_Direction +
                                                       (T)0.5 * CAngleRange::Range_MinusPI_PI(Line2.m_Direction - Line1.m_Direction));

        T xInter, yInter; // Sjeciste dvaju pravaca

        if(!Line1.GetIntersection(Line2, xInter, yInter))
        {
            // Paralelni su
            xInter = (T)0.5 * (p0.x + Line2.p0.x);
            yInter = (T)0.5 * (p0.y + Line2.p0.y);
        }

        LineFromDirectionAndPoint(NewDirection, xInter, yInter);
    }
};

//=================================================================================================================================================================
//
// Klasa za zraku (pravac omeden s jedne strane) koja koristi parametarski oblik jednadzbe pravca kao bazu:
// x(d) = x0 + C * d
// y(d) = y0 + S * d
// Pritom zraka pocinje u tocki (x0, y0) i ima smjer kao i parametarski pravac
//
//=================================================================================================================================================================
class CLineParRay : public CLinePar
{
public:
    CLineParRay() {}

    //________________________________________________________________________________________________________________________________________________
    //
    // Konstrukcija zrake iz parametarskog pravca trazi samo kopiranje (razlika izmedu pravca i zrake je samo u interpretaciji).
    //________________________________________________________________________________________________________________________________________________
    CLineParRay(const CLinePar& Line)
    {
        C = Line.C;
        S = Line.S;
        p0 = Line.p0;
        m_Direction = Line.m_Direction;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Daje tocku sjecista zrake i parametarskog pravca, ili false ako nema sjecista ili su pravci paralelni (priblizno eps).
    //________________________________________________________________________________________________________________________________________________
    bool GetIntersection(const CLinePar& Line2, COORD_GM& x, COORD_GM& y) const
    {
        if(!IsParallel(Line2))
        {
            T d1 = (Line2.C * (Line2.p0.y - p0.y) + Line2.S * (p0.x - Line2.p0.x)) /
                   (S * Line2.C - C * Line2.S); // d1 mora biti nenegativan (priblizno) da bi zraka sjekla pravac Line2

            if(IsNegative(d1))
                return false;

            GetPoint(d1, x, y);

            return true;
        }
        else
            return false;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Daje tocku sjecista zrake i druge zrake, ili false ako nema sjecista ili su pravci paralelni (priblizno eps).
    //________________________________________________________________________________________________________________________________________________
    bool GetIntersection(const CLineParRay& Ray2, COORD_GM& x, COORD_GM& y) const
    {
        if(!IsParallel(Ray2))
        {
            T d1 = (Ray2.C * (Ray2.p0.y - p0.y) + Ray2.S * (p0.x - Ray2.p0.x)) / (S * Ray2.C - C * Ray2.S); // udaljenost na prvoj zraki

            if(IsNegative(d1)) // d1 mora biti nenegativan (priblizno) da bi se zrake sjekle
                return false;

            GetPoint(d1, x, y);

            T d2 = Ray2.GetD(x, y);

            if(IsNegative(d2)) // d2 mora biti nenegativan (priblizno) da bi se zrake sjekle
                return false;

            return true;
        }
        else
            return false;
    }
};

//=================================================================================================================================================================
//
// Klasa za omedeni parametarski pravac
//
//=================================================================================================================================================================
class CLineParBounded : public CLinePar
{
public:
    T m_Length; // Duljina pravca - mora biti >=0

    //________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje pravca iz dvije tocke.
    // Prva tocka se uzima kao pocetna. Smjer se uzima od prve do druge tocke, a duljina iz razmaka dviju tocaka.
    // Tocke moraju biti razlicite, inace se vraca false.
    //________________________________________________________________________________________________________________________________________________
    bool LineFromPoints(T x1, T y1, T x2, T y2)
    {
        T dx = x2 - x1;
        T dy = y2 - y1;
        m_Length = sqrt(dx * dx + dy * dy);
        if(IsZero(m_Length))
            return false;

        T LengthInv = 1 / m_Length;
        C = dx * LengthInv;
        S = dy * LengthInv;

        p0.x = x1;
        p0.y = y1;

        m_Direction = atan2(dy, dx);

        return true;
    }

    inline bool LineFromPoints(const C2DPoint& Pt1, const C2DPoint& Pt2) { return LineFromPoints(Pt1.x, Pt1.y, Pt2.x, Pt2.y); }

    inline void Length(T NewLength)
    {
        assert(NewLength >= 0);
        m_Length = NewLength;
    }

    inline T Length() const { return m_Length; }

    inline C2DPoint EndPoint() const
    {
        C2DPoint EndPt;
        GetPoint(m_Length, EndPt);
        return EndPt;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Ispituje koja od krajnjih tocaka segmenta je bliza zadanoj tocki.
    // Vraca 0 ako je blize pocetna tocka segmenta, 1 ako je blize zavrsna tocka segmenta. Ako su obje isto blizu, vraca 0.
    //________________________________________________________________________________________________________________________________________________
    int CloserEndPoint(T x, T y) { return (p0.GetDistance2ToPoint(x, y) <= EndPoint().GetDistance2ToPoint(x, y)) ? 0 : 1; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Daje tocku sjecista parametarskog segmenta i parametarske zrake, ili false ako nema sjecista ili su pravci paralelni (priblizno eps).
    // Parametarski pravac (this) se tretira kao zraka s pocetkom u p0 i smjerom m_Direction.
    // Tocka sjecista mora biti (priblizno) i na zraci i na segmentu.
    //________________________________________________________________________________________________________________________________________________
    bool GetIntersection(const CLineParRay& Ray, COORD_GM& x, COORD_GM& y) const
    {
        if(!IsParallel(Ray))
        {
            T d1 = (Ray.C * (Ray.p0.y - p0.y) + Ray.S * (p0.x - Ray.p0.x)) / (S * Ray.C - C * Ray.S);

            if(IsNegative(d1)) // d1 mora biti nenegativan (priblizno) da bi sjeciste bilo na segmentu
                return false;

            if(IsPositive(d1 - m_Length)) // ako je d1 znacajno veci od duljine segmenta, ocito sjeciste nije na segmentu
                return false;

            GetPoint(d1, x, y); // racunanje samog sjecista

            T d2 = Ray.GetD(x, y); // gdje je to na zraci

            if(IsNegative(d2)) // Provjera da li je sjeciste (priblizno) na zraki - tj. d2 mora biti nenegativan
                return false;

            return true;
        }
        else
            return false;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Formira zraku na nacin da zraka pocinje iz pocetne tocke ovog pravca
    //________________________________________________________________________________________________________________________________________________
    CLineParRay FormLineParRay() const { return CLineParRay(*this); }
};

//=================================================================================================================================================================
// Klasa za poluomedeni pravac (ili polupravac).
// Smije se zadavati jedino preko LineFromDirectionAndPoint.
//=================================================================================================================================================================
class CHalfBoundLineImp : public CLineImp
{
public:
    C2DPoint p0;

    void LineFromDirectionAndPoint(ANGLERAD_GM Direction, COORD_GM x, COORD_GM y)
    {
        p0.x = x;
        p0.y = y;

        CLineImp::LineFromDirectionAndPoint(Direction, x, y);
    }

    // Testira dal je tocka na polupravcu
    bool CheckPoint(COORD_GM x, COORD_GM y) const { return (B * (p0.x - x) + A * (y - p0.y) <= 0); }

    bool CheckPoint(const C2DPoint& Pt) const { return CheckPoint(Pt.x, Pt.y); }

    // Trazenje sjecista s pravcem
    bool GetIntersectionPoint(const CLineImp& OtherLine, COORD_GM& xc, COORD_GM& yc) const
    {
        if(!CLineImp::GetIntersectionPoint(OtherLine, xc, yc))
            return false;

        return CheckPoint(xc, yc);
    }
};

//=================================================================================================================================================================
// Klasa za omedeni pravac.
// Smije se zadavati jedino preko LineFromPoints.
//=================================================================================================================================================================
class CBoundLineImp : public CLineImp
{
public:
    C2DPoint p1, p2;

    LENGTH_GM GetLength() const { return p1.GetDistanceToPoint(&p2); }

    void LineFromPoints(COORD_GM x1, COORD_GM y1, COORD_GM x2, COORD_GM y2)
    {
        // Pozove baznu klasu i spremi tocke
        CLineImp::LineFromPoints(x1, y1, x2, y2);

        p1.SetPoint(x1, y1);
        p2.SetPoint(x2, y2);
    }

    void LineFromPoints(const C2DPoint* pPoint1, const C2DPoint* pPoint2)
    {
        CLineImp::LineFromPoints(pPoint1, pPoint2);

        p1 = *pPoint1;
        p2 = *pPoint2;
    }

    void LineFromPoints(const C2DPoint& Point1, const C2DPoint& Point2)
    {
        CLineImp::LineFromPoints(&Point1, &Point2);

        p1 = Point1;
        p2 = Point2;
    }

    // Testira dal je tocka za koju znamo da je na liniji, izmedu granicnih tocaka omedene linije.
    bool CheckPoint(COORD_GM xc, COORD_GM yc) const
    {
        // Zbog numericke nepreciznosti, ak je linija vise horizontalna, testiramo samo x koordinatu i obrnuto
        if(IsHorizontal())
        {
            if((p1.x <= xc && xc <= p2.x) || (p2.x <= xc && xc <= p1.x))
                return true;
        }
        else
        {
            if((p1.y <= yc && yc <= p2.y) || (p2.y <= yc && yc <= p1.y))
                return true;
        }
        return false;
    }

    bool CheckPoint(const C2DPoint& Pt) const { return CheckPoint(Pt.x, Pt.y); }

    // Trazenje sjecista s pravcem
    bool GetIntersectionPoint(const CLineImp& OtherLine, COORD_GM& xc, COORD_GM& yc) const
    {
        if(!CLineImp::GetIntersectionPoint(OtherLine, xc, yc))
            return false;

        return CheckPoint(xc, yc);
    }

    bool GetIntersectionPoint(const CHalfBoundLineImp& OtherLine, COORD_GM& xc, COORD_GM& yc) const
    {
        if(!CLineImp::GetIntersectionPoint(OtherLine, xc, yc))
            return false;

        if(CheckPoint(xc, yc))
            return OtherLine.CheckPoint(xc, yc);

        return false;
    }

    bool GetIntersectionPoint(const CBoundLineImp& OtherLine, COORD_GM& xc, COORD_GM& yc) const
    {
        // Prvo nadi presjeciste obicnih pravaca bez da gledamo granicne tocke
        if(!CLineImp::GetIntersectionPoint(OtherLine, xc, yc))
            return false;

        // Sad provjeri dal je nadena presjecna tocka unutar granicnih tocaka obaju pravaca
        if(!CheckPoint(xc, yc))
            return false;

        if(!OtherLine.CheckPoint(xc, yc))
            return false;

        return true;
    }

    // Nalazi kvadrat udaljenosti duzine i tocke. Usput vraca i tocku na duzini koja je najbliza zadanoj tocki. [ni testirano]
    DIST_GM GetDistance2ToPoint(COORD_GM x, COORD_GM y, COORD_GM& xClosest, COORD_GM& yClosest)
    {
        // Nadi projekciju tocke na pravac
        COORD_GM AA = A * A;
        COORD_GM BB = B * B;
        COORD_GM AB = A * B;
        COORD_GM AABB = AA + BB;

        COORD_GM px = (BB * x - AB * y - A * C) / AABB;
        COORD_GM py = (-AB * x + AA * y - B * C) / AABB;

        // Ako se tocka projekcije nalazi na duzini, onda je ta tocka najbliza
        if(CheckPoint(px, py))
        {
            xClosest = px;
            yClosest = py;

            DIST_GM dx = px - x;
            DIST_GM dy = py - y;

            return (dx * dx + dy * dy);
        }

        // Inace je najbliza tocka ili pocetna ili krajnja tocka duzine
        DIST_GM dx = p1.x - x;
        DIST_GM dy = p1.y - y;
        DIST_GM d1 = dx * dx + dy * dy;

        dx = p2.x - x;
        dy = p2.y - y;
        DIST_GM d2 = dx * dx + dy * dy;

        if(d1 < d2)
        {
            xClosest = p1.x;
            yClosest = p1.y;
            return d1;
        }
        else
        {
            xClosest = p2.x;
            yClosest = p2.y;
            return d2;
        }
    }

    // Nalazi minimalnu (kvadriranu) udaljenost izmedu dvije duzine.
    // Usput daje tocke s prve i druge duzine izmedu kojih je nadena ta minimalna udaljenost.
    // TUDU: Sto ako su duzine paralelne?
    DIST_GM GetDistance2ToLine(const CBoundLineImp& OtherLine, C2DPoint* pPoint1, C2DPoint* pPoint2) const
    {
        // Prvo provjeri dal se sijeku
        COORD_GM xi, yi;

        if(GetIntersectionPoint(OtherLine, xi, yi))
        {
            pPoint1->x = pPoint2->x = xi;
            pPoint1->y = pPoint2->y = yi;
            return 0;
        }

        // Ak se ne sijeku, onda treba provjeriti 8 udaljenosti i izabr. najmanju.
        DIST_GM MinDist2 = std::numeric_limits<DIST_GM>::max(); // Minimalna udalj na kvadrat
        bool bFoundMin = false;

        // Prvo provjeravamo projekcije krajnjih tocaka drugog pravca na pravac this
        COORD_GM AA = A * A;
        COORD_GM BB = B * B;
        COORD_GM AB = A * B;
        COORD_GM AABB = AA + BB;

        // Tocka na drugom pravcu koju projiciramo
        COORD_GM x = OtherLine.p1.x;
        COORD_GM y = OtherLine.p1.y;

        // Tocka projekcije
        COORD_GM px = (BB * x - AB * y - A * C) / AABB;
        COORD_GM py = (-AB * x + AA * y - B * C) / AABB;

        if(CheckPoint(px, py)) // Provjera dal je projekcija na pravcu this
        {
            DIST_GM dx = px - x;
            DIST_GM dy = py - y;
            DIST_GM d2 = dx * dx + dy * dy;
            if(d2 < MinDist2)
            {
                MinDist2 = d2;
                pPoint1->x = px;
                pPoint1->y = py;
                pPoint2->x = x;
                pPoint2->y = y;
                bFoundMin = true;
            }
        }

        // Tocka na drugom pravcu koju projiciramo
        x = OtherLine.p2.x;
        y = OtherLine.p2.y;

        // Tocka projekcije
        px = (BB * x - AB * y - A * C) / AABB;
        py = (-AB * x + AA * y - B * C) / AABB;

        if(CheckPoint(px, py)) // Provjera dal je projekcija na pravcu this
        {
            DIST_GM dx = px - x;
            DIST_GM dy = py - y;
            DIST_GM d2 = dx * dx + dy * dy;
            if(d2 < MinDist2)
            {
                MinDist2 = d2;
                pPoint1->x = px;
                pPoint1->y = py;
                pPoint2->x = x;
                pPoint2->y = y;
                bFoundMin = true;
            }
        }

        // Sad provjeravamo projekcije krajnjih tocaka pravca this na drugi pravac
        AA = OtherLine.A * OtherLine.A;
        BB = OtherLine.B * OtherLine.B;
        AB = OtherLine.A * OtherLine.B;
        AABB = AA + BB;

        // Tocka na pravcu this koju projiciramo
        x = p1.x;
        y = p1.y;

        // Tocka projekcije
        px = (BB * x - AB * y - OtherLine.A * OtherLine.C) / AABB;
        py = (-AB * x + AA * y - OtherLine.B * OtherLine.C) / AABB;

        if(OtherLine.CheckPoint(px, py)) // Provjera dal je projekcija na drugom pravcu
        {
            DIST_GM dx = px - x;
            DIST_GM dy = py - y;
            DIST_GM d2 = dx * dx + dy * dy;
            if(d2 < MinDist2)
            {
                MinDist2 = d2;
                pPoint1->x = x;
                pPoint1->y = y;
                pPoint2->x = px;
                pPoint2->y = py;
                bFoundMin = true;
            }
        }

        // Tocka na pravcu this koju projiciramo
        x = p2.x;
        y = p2.y;

        // Tocka projekcije
        px = (BB * x - AB * y - OtherLine.A * OtherLine.C) / AABB;
        py = (-AB * x + AA * y - OtherLine.B * OtherLine.C) / AABB;

        if(OtherLine.CheckPoint(px, py)) // Provjera dal je projekcija na drugom pravcu
        {
            DIST_GM dx = px - x;
            DIST_GM dy = py - y;
            DIST_GM d2 = dx * dx + dy * dy;
            if(d2 < MinDist2)
            {
                MinDist2 = d2;
                pPoint1->x = x;
                pPoint1->y = y;
                pPoint2->x = px;
                pPoint2->y = py;
                bFoundMin = true;
            }
        }

        // Ukoliko smo na gornji nacin nasli minimum, ne treba dalje trazit jer sigurno se vise ne moze naci manja udaljenost
        if(bFoundMin)
            return MinDist2;

        // Nema nijedne projekcije krajnje tocke jednog pravca na drugi, pa moramo provjeravati udaljenosti izmedu samih krajnjih tocaka
        COORD_GM d2 = p1.GetDistance2ToPoint(&OtherLine.p1);
        if(d2 < MinDist2)
        {
            MinDist2 = d2;
            *pPoint1 = p1;
            *pPoint2 = OtherLine.p1;
        }

        d2 = p1.GetDistance2ToPoint(&OtherLine.p2);
        if(d2 < MinDist2)
        {
            MinDist2 = d2;
            *pPoint1 = p1;
            *pPoint2 = OtherLine.p2;
        }

        d2 = p2.GetDistance2ToPoint(&OtherLine.p1);
        if(d2 < MinDist2)
        {
            MinDist2 = d2;
            *pPoint1 = p2;
            *pPoint2 = OtherLine.p1;
        }

        d2 = p2.GetDistance2ToPoint(&OtherLine.p2);
        if(d2 < MinDist2)
        {
            MinDist2 = d2;
            *pPoint1 = p2;
            *pPoint2 = OtherLine.p2;
        }

        return MinDist2;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Ispituje koja od krajnjih tocaka segmenta je bliza zadanoj tocki.
    // Vraca 0 ako je blize pocetna tocka segmenta, 1 ako je blize zavrsna tocka segmenta. Ako su obje isto blizu, vraca 0.
    //________________________________________________________________________________________________________________________________________________
    int CloserEndPoint(T x, T y) { return (p1.GetDistance2ToPoint(x, y) <= p2.GetDistance2ToPoint(x, y)) ? 0 : 1; }
};

}; // namespace Geom

#endif
