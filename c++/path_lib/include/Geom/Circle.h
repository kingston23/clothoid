
#ifndef CIRCLE_H
#define CIRCLE_H

#include "2DPoint.h"
#include "GeometryBase.h"
#include "Line.h"

namespace Geom
{
// Bazna klasa za krug sa low level (pretezno) inline implementacijama funkcija
class CCircleBase : public CGeometryBase
{
public:
    // Enumeracija vrsta tangenti - znacenje ovisi o funkciji u kojoj se koristi
    enum ETgTwoCircles
    {
        LL_TG = 1,
        RL_TG,
        RR_TG,
        LR_TG,
    };

    //---------------------------------------------------------------------------------------------------------
    // Funkcija nalazi kut tangente na dva kruga.
    // Ako tangenta ne moze biti proracunata (npr. jer je krug unutar kruga), tada funkcija
    // vraca false.
    // TangentType moze biti jedan od tipova ETgTwoCircles.
    // Npr. LL_TG znaci da su oba kruga lijevo od tangente. Smjer tangente uzima se od
    // prvog kruga prema drugom.
    //---------------------------------------------------------------------------------------------------------
    static bool GetTangentAngleToTwoCircles(ETgTwoCircles TangentType, COORD_GM xc1, COORD_GM yc1, RADIUS_GM r1, COORD_GM xc2, COORD_GM yc2,
                                            RADIUS_GM r2,
                                            ANGLERAD_GM* pTgAngle // OUT
    )
    {
        COORD_GM dx = xc2 - xc1;
        COORD_GM dy = yc2 - yc1;
        DIST_GM d2 = dx * dx + dy * dy;

        assert(pTgAngle);

        switch(TangentType)
        {
        case LL_TG:
        case RR_TG: // Vanjske tangente
        {
            DIST_GM dr = r2 - r1;

            DIST_GM Discriminant = d2 - dr * dr;
            if(Discriminant <= 0)
            {
                // Znaci da su kruznice jedna unutar druge i nema vanjske tangente ili
                // su obje kruznice jednake
                assert(0);
                return false;
            }

            DIST_GM sD = (DIST_GM)sqrt(Discriminant); // Korijen diskriminante
            if(TangentType == LL_TG)
                // *pTgAngle = (ANGLERAD_GM)atan2(-dy*dr-dx*sD, -dx*dr+dy*sD) + PI/2;
                *pTgAngle = (ANGLERAD_GM)atan2(dx * dr + dy * sD, -dy * dr + dx * sD);
            else
                // *pTgAngle = (ANGLERAD_GM)atan2(-dy*dr+dx*sD, -dx*dr-dy*sD) - PI/2;
                *pTgAngle = (ANGLERAD_GM)atan2(-dx * dr + dy * sD, dy * dr + dx * sD);
        }
        break;

        case RL_TG:
        case LR_TG: // Unutarnje tangente
        {
            DIST_GM sr = r1 + r2;

            DIST_GM D = d2 - sr * sr; // Diskriminanta
            if(D < 0)                 // Znaci da se kruznice preklapaju (sijeku) i nema unutarnje tangente
            {
                // assert(0);
                return false;
            }

            DIST_GM sD = (DIST_GM)sqrt(D); // Korijen diskriminante
            if(TangentType == LR_TG)
                // *pTgAngle = (ANGLERAD_GM)atan2(-dy*sr+dx*sD, -dx*sr-dy*sD) - PI/2;
                *pTgAngle = (ANGLERAD_GM)atan2(dx * sr + dy * sD, -dy * sr + dx * sD);
            else
                // *pTgAngle = (ANGLERAD_GM)atan2(-dy*sr-dx*sD, -dx*sr+dy*sD) + PI/2;
                *pTgAngle = (ANGLERAD_GM)atan2(-dx * sr + dy * sD, dy * sr + dx * sD);
        }
        break;

        default:
        {
            assert(0);
            return false;
            break;
        }
        }
        return true;
    }

    // Pozitivan radijus oznacava kruznicu lijevo od tangente, a negativan desno.
    // Iz tog razloga vazan je i predznak kuta tangente, tj. tangenta je usmjerena.
    // Npr. nije isto da li je kut 0 ili pi iako je smjer isti u oba slucaja!
    inline static void GetCircleFromTangent(COORD_GM xt, COORD_GM yt, ANGLERAD_GM TgAngle, RADIUS_GM Radius, COORD_GM* pxc, COORD_GM* pyc)
    {
        assert(pxc && pyc);

        // Vrijedi ista formula i za poz. i za neg. radijus.
        *pxc = xt - Radius * (NUM_GM)sin(TgAngle); // cos(TgAngle+pi/2) = -sin(TgAngle) ili za r<0 je -cos(TgAngle-pi/2) = -sin(TgAngle)
        *pyc = yt + Radius * (NUM_GM)cos(TgAngle); // sin(TgAngle+pi/2) = cos(TgAngle) ili za r<0 je -sin(TgAngle-pi/2) = cos(TgAngle)
    };

    // Racuna dva kruga istog radijusa koji se dodiruju uz uvjet da prvi krug dodiruje
    // prvu tangentu u tocki x1,y1, a drugi krug dodiruje drugu tangentu u tocki x2,y2.
    // Tangente se zadaju preko kuteva.
    // Tipovi tangenti mogu biti LL_TG, RR_TG, RL_TG i LR_TG.
    // LR_TG npr. znaci da je prvi krug lijevo od prve tangente, a drugi krug je
    // desno od druge tangente. Pritom je smjer tangente odreden kutem tangente
    // (npr. kutevi 0 i pi znace isti pravac tangente, ali su smjerovi suprotni)
    //
    // U slucaju da nema rjesenja, vraca false.
    inline static bool GetCirclesFromTwoTangents(COORD_GM x1, COORD_GM y1, ANGLERAD_GM TgAng1, // Prva tangenta (IN)
                                                 COORD_GM x2, COORD_GM y2, ANGLERAD_GM TgAng2, // Druga tangenta (IN)
                                                 ETgTwoCircles TgTypes,                        // (IN)
                                                 COORD_GM& xc1, COORD_GM& yc1,                 // Prvi krug (OUT)
                                                 COORD_GM& xc2, COORD_GM& yc2,                 // Drugi krug (OUT)
                                                 RADIUS_GM& r                                  // (OUT)
    )
    {
        DIST_GM dx = x2 - x1;
        DIST_GM dy = y2 - y1;

        NUM_GM S1, S2, C1, C2;

        switch(TgTypes)
        {
        case LL_TG:
            S1 = (NUM_GM)sin(TgAng1);
            C1 = (NUM_GM)cos(TgAng1);

            S2 = (NUM_GM)sin(TgAng2);
            C2 = (NUM_GM)cos(TgAng2);
            break;

        case RR_TG:
            S1 = -(NUM_GM)sin(TgAng1);
            C1 = -(NUM_GM)cos(TgAng1);

            S2 = -(NUM_GM)sin(TgAng2);
            C2 = -(NUM_GM)cos(TgAng2);
            break;

        case LR_TG:
            S1 = (NUM_GM)sin(TgAng1);
            C1 = (NUM_GM)cos(TgAng1);

            S2 = -(NUM_GM)sin(TgAng2);
            C2 = -(NUM_GM)cos(TgAng2);
            break;

        case RL_TG:
            S1 = -(NUM_GM)sin(TgAng1);
            C1 = -(NUM_GM)cos(TgAng1);

            S2 = (NUM_GM)sin(TgAng2);
            C2 = (NUM_GM)cos(TgAng2);
            break;

        default:
            assert(0); // You're not supposed to be here
            return false;
        }

        NUM_GM S = S2 - S1;
        NUM_GM C = C2 - C1;

        // Koef. kvadr. jednadzbe
        NUM_GM a = C * C + S * S - 4;
        NUM_GM b = 2 * (dy * C - dx * S);
        NUM_GM c = dx * dx + dy * dy;

        // Izracunaj radijus
        if(a == 0) // Ako su kutevi tangenti jednaki tada ce biti a=0 (zbog num nepreciz stavimo kriterij <)
        {
            if(b == 0)
                return false; // Nema rjesenja - radijus bi bio beskonacan

            r = -c / b;

            if(r < 0)
                return false; // Nema rjesenja za ovaj tip tangente
        }
        else
        {
            NUM_GM D = b * b - 4 * a * c; // Diskriminanta
            assert(D >= 0);               // Ovo se teoretski ne bi smjelo nikada desiti (izvod)
            NUM_GM sD = (NUM_GM)sqrt(D);

            r = (-b - sD) / (2 * a);

            if(r <= 0)
            {
                if(b == 0)
                    return false; // Nema rjesenja - radijus bi bio beskonacan

                r = -c / b;

                if(r < 0)
                    return false; // Nema rjesenja za ovaj tip tangente
            }

            assert(r >= 0); // Ovo se teoretski ne bi smjelo nikada desiti (izvod)
        }

        // Izracunaj sredista krugova
        xc1 = x1 - r * S1;
        yc1 = y1 + r * C1;

        xc2 = x2 - r * S2;
        yc2 = y2 + r * C2;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________
    //
    // Funkcija za odredivanje sjecista pravca i kruznice. Vraca broj sjecista (0,1 ili 2)
    //____________________________________________________________________________________________________________________________________________
    inline static int GetIntersectionWithLine(COORD_GM xc, COORD_GM yc, RADIUS_GM r, LNCOEFF_GM A, LNCOEFF_GM B, LNCOEFF_GM C, COORD_GM& x1,
                                              COORD_GM& y1, COORD_GM& x2, COORD_GM& y2)
    {
        NUM_GM AA = A * A;
        NUM_GM BB = B * B;
        NUM_GM AB = A * B;
        NUM_GM rr = r * r;
        NUM_GM t = C + A * xc + B * yc;
        NUM_GM AABB = AA + BB;
        NUM_GM D = AABB * rr - t * t;

        if(D > 0)
        {
            NUM_GM sD = (NUM_GM)sqrt(D);

            NUM_GM t1 = -A * C + BB * xc - AB * yc;
            NUM_GM t2 = -B * C - AB * xc + AA * yc;

            x1 = (t1 - B * sD) / AABB;
            y1 = (t2 + A * sD) / AABB;

            x2 = (t1 + B * sD) / AABB;
            y2 = (t2 - A * sD) / AABB;

            return 2;
        }
        else if(D == 0)
        {
            NUM_GM t1 = -A * C + BB * xc - AB * yc;
            NUM_GM t2 = -B * C - AB * xc + AA * yc;

            x1 = t1 / AABB;
            y1 = t2 / AABB;

            return 1;
        }
        else
        {
            return 0;
        }
    }
};

//=====================================================================================================================================
//
// Klasa za kruznicu
//
//=====================================================================================================================================
class CCircle : public CCircleBase
{
public:
    COORD_GM m_xc; // Srediste kruznice
    COORD_GM m_yc;
    COORD_GM m_r; // Radijus (>=0)

public:
    bool IsOk() const
    {
        if(m_r < 0)
            return false;
        return true;
    }

    // Pristupne rutine
    COORD_GM GetRadius() const
    {
        assert(IsValid());
        return m_r;
    }

    COORD_GM GetXc() const
    {
        assert(IsValid());
        return m_xc;
    }

    COORD_GM GetYc() const
    {
        assert(IsValid());
        return m_yc;
    }

    void GetCenter(C2DPoint* pPoint) const
    {
        assert(IsValid() && pPoint);
        pPoint->x = m_xc;
        pPoint->y = m_yc;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Trazi sjecista kruznice i pravca.
    // Vraca broj tocaka sjecista (0,1 ili 2)
    // Niz pPoints mora biti duljine najmanje 2.
    //_________________________________________________________________________________________________________________________________________________________
    int GetIntersectionWithLine(const CLineImp& Line, C2DPoint* pPoints) const
    {
        return CCircleBase::GetIntersectionWithLine(m_xc, m_yc, m_r, Line.A, Line.B, Line.C, pPoints[0].x, pPoints[0].y, pPoints[1].x,
                                                    pPoints[1].y);
    }

    // Trazi sjecista kruznice i duzine.
    // Vraca broj tocaka sjecista (0,1 ili 2)
    // Niz pPoints mora biti duljine najmanje 2.
    int GetIntersectionWithLine(const CBoundLineImp& Line, C2DPoint* pPoints) const
    {
        int n = CCircleBase::GetIntersectionWithLine(m_xc, m_yc, m_r, Line.A, Line.B, Line.C, pPoints[0].x, pPoints[0].y, pPoints[1].x,
                                                     pPoints[1].y);

        int nGood = 0;
        for(int i = 0; i < n; i++)
        {
            if(Line.CheckPoint(pPoints[i]))
            {
                if(nGood != i)
                    pPoints[nGood] = pPoints[i]; // Shiftanje tocke unaprijed ako je ptorebno
                nGood++;
            }
        }
        return nGood;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje kruznice pomocu tangente i jedne tocke.
    // U slucaju neuspjeha (npr. lose zadani parametri) vraca false.
    //_________________________________________________________________________________________________________________________________________________________
    bool CircleFromTangentAndPoint(COORD_GM xt, COORD_GM yt,         // tocka kroz koju prolazi tangenta
                                   ANGLERAD_GM TgAngle,              // kut tangente u toj tocki
                                   COORD_GM xPoint, COORD_GM yPoint, // tocka kroz koju prolazi krug (naravno mora biti razlicita od xt,yt)
                                   bool* pbCircleIsLeftFromTangent = nullptr // (out) ovo se postavi na true ak je izracunati krug lijevo od
                                                                             // smjera tangente ili je radijus nula, a inace na false
    )
    {
        DIST_GM dx = xPoint - xt;
        DIST_GM dy = yPoint - yt;

        NUM_GM Numerator = (dx * dx + dy * dy);
        NUM_GM Denumerator = 2 * (dy * cos(TgAngle) - dx * sin(TgAngle));

        assert(Denumerator != 0);
        if(Denumerator == 0)
            return false;

        m_r = Numerator / Denumerator;
        m_xc = xt - m_r * sin(TgAngle);
        m_yc = yt + m_r * cos(TgAngle);

        if(m_r > 0)
        {
            if(pbCircleIsLeftFromTangent)
                *pbCircleIsLeftFromTangent = true;
        }
        else
        {
            if(pbCircleIsLeftFromTangent)
                *pbCircleIsLeftFromTangent = false;

            m_r = -m_r;
        }

        assert(IsOk());

        return true;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Trazi sjecista kruznice i kruznice.
    // pPoints - niz tocaka u kojem se vracaju sjecista, mora biti duljine najmanje 2.
    // Vraca broj tocaka sjecista (0,1 ili 2). Ako su kruznice identicne, vraca -1.
    //_________________________________________________________________________________________________________________________________________________________
    int GetIntersectionWithCircle(const CCircle& Circ, C2DPoint* pPoints) const
    {
        if(Equal(Circ))
            return -1; // Obje su identicne, beskonacno mnogo sjecista

        // dx and dy are the vertical and horizontal distances between the circle centers
        DIST_GM dx = Circ.m_xc - m_xc;
        DIST_GM dy = Circ.m_yc - m_yc;

        // Determine the straight-line distance between the centers.
        DIST_GM d = sqrt((dy * dy) + (dx * dx));

        // Check for solvability.
        if(d > (m_r + Circ.m_r))
            return 0; // no solution. circles do not intersect.

        DIST_GM rDiff = abs(m_r - Circ.m_r);
        if(d < rDiff)
            return 0; // no solution. one circle is contained in the other

        NUM_GM d_r = 1 / d;
        NUM_GM a = ((m_r * m_r) - (Circ.m_r * Circ.m_r) + (d * d)) * (NUM_GM)0.5 * d_r; // udalj od centra prvog kruga do tocke 2

        // Tocka 2 je tocka u kojoj se sijeku pravac kroz centre kruznica i pravac kroz sjecista
        COORD_GM x2 = m_xc + (dx * a * d_r);
        COORD_GM y2 = m_yc + (dy * a * d_r);

        DIST_GM h2 = (m_r * m_r) - (a * a); // Udalj (na kvadrat) od tocke 2 do bilo kojeg od sjecista

        // Provjeri dal je samo jedno sjeciste
        if(h2 == 0)
        {
            pPoints[0].x = x2;
            pPoints[0].y = y2;
            return 1;
        }

        DIST_GM h = sqrt(h2);

        // Ofset od sjecista do tocke 2
        DIST_GM rx = -dy * (h * d_r);
        DIST_GM ry = dx * (h * d_r);

        // Koordinate sjecista
        pPoints[0].x = x2 + rx;
        pPoints[0].y = y2 + ry;
        pPoints[1].x = x2 - rx;
        pPoints[1].y = y2 - ry;

        return 2;
    }

public:
    void SetCircle(COORD_GM xc, COORD_GM yc, RADIUS_GM r)
    {
        m_xc = xc;
        m_yc = yc;
        m_r = r;

        assert(IsValid());
    }

    void SetCircleFromTangent(COORD_GM xt, COORD_GM yt, ANGLERAD_GM TgAngle, RADIUS_GM Radius)
    {
        GetCircleFromTangent(xt, yt, TgAngle, Radius, &m_xc, &m_yc);
        m_r = abs(Radius);
    }

    bool GetTangentAngleToTwoCircles(ETgTwoCircles TangentType, CCircle* pCircle2,
                                     ANGLERAD_GM* pAngle // OUT
                                     ) const
    {
        return CCircleBase::GetTangentAngleToTwoCircles(TangentType, m_xc, m_yc, m_r, pCircle2->m_xc, pCircle2->m_yc, pCircle2->m_r,
                                                        pAngle);
    }

    bool IsValid() const
    {
        if(m_r < 0)
        {
            assert(0);
            return false;
        }

        return true;
    }

    DIST_GM GetDistanceToLine(CBoundLineImp& Line, C2DPoint* pCircPoint, C2DPoint* pLinePoint) const
    {
        // Provjeri prvo dal ima sjecista, ak ima min udalj je nula
        C2DPoint Inters[2];
        int n = GetIntersectionWithLine(Line, Inters);
        if(n > 0)
        {
            DIST_GM MinDist2 = std::numeric_limits<DIST_GM>::max(); // Minimalna udalj na kvadrat
            int iMinDistPoint = 0;

            // Nadi sjeciste koje je najblize prvoj tocki duzine
            for(int i = 0; i < n; i++)
            {
                DIST_GM dx = Inters[i].x - Line.p1.x;
                DIST_GM dy = Inters[i].y - Line.p1.y;
                DIST_GM d2 = dx * dx + dy * dy;
                if(d2 < MinDist2)
                {
                    MinDist2 = d2;
                    iMinDistPoint = i;
                }
            }
            *pCircPoint = *pLinePoint = Inters[iMinDistPoint];
            return 0;
        }

        // Izracunaj udaljenost kruga od pravca
        DIST_GM d = abs(Line.GetDistanceToPoint(m_xc, m_yc)) - m_r;

        // Izr projekciju sredista kruga na pravac
        COORD_GM px, py;
        Line.GetPointProjection(m_xc, m_yc, px, py);

        // Ak je projekcija na duzini to je min udaljenost
        if(Line.CheckPoint(px, py))
        {
            // Najbliza tocka na krugu je negdje izmedu sredista kruga i projekcijske tocke na pravac
            DIST_GM dx = px - m_xc;
            DIST_GM dy = py - m_yc;
            NUM_GM ratio = m_r / (d + m_r);
            pCircPoint->x = m_xc + ratio * dx;
            pCircPoint->y = m_yc + ratio * dy;
            pLinePoint->x = px;
            pLinePoint->y = py;
            return d;
        }

        // Minimalna udaljenost je jedna od udaljenosti do prvog ili drugog kraja duzine, ovisno koja je manja
        DIST_GM d2_1, d2_2;

        DIST_GM dx = m_xc - Line.p1.x;
        DIST_GM dy = m_yc - Line.p1.y;
        d2_1 = dx * dx + dy * dy;

        dx = m_xc - Line.p2.x;
        dy = m_yc - Line.p2.y;
        d2_2 = dx * dx + dy * dy;

        if(d2_1 < d2_2)
        {
            // Udalj do prve tocke je manja
            DIST_GM dx = Line.p1.x - m_xc;
            DIST_GM dy = Line.p1.y - m_yc;
            d = sqrt(d2_1) - m_r;
            NUM_GM ratio = m_r / (d + m_r);
            pCircPoint->x = m_xc + ratio * dx;
            pCircPoint->y = m_yc + ratio * dy;
            *pLinePoint = Line.p1;
            return d;
        }
        else
        {
            // Udalj do druge tocke je manja
            DIST_GM dx = Line.p2.x - m_xc;
            DIST_GM dy = Line.p2.y - m_yc;
            d = sqrt(d2_2) - m_r;
            NUM_GM ratio = m_r / (d + m_r);
            pCircPoint->x = m_xc + ratio * dx;
            pCircPoint->y = m_yc + ratio * dy;
            *pLinePoint = Line.p2;
            return d;
        }
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Testira dal kruznica this sadrzi (strogo) kruznicu Circ, sto znaci da je Circ unutar this i ne doticu se
    //_________________________________________________________________________________________________________________________________________________________
    bool Contains(const CCircle& Circ) const
    {
        if(Circ.m_r >= m_r)
            return false; // Ne moze sadrzavati kruznicu koja je veca ili jednaka
        DIST_GM rDiff = m_r - Circ.m_r;
        DIST_GM rDiff2 = rDiff * rDiff;
        DIST_GM dx = m_xc - Circ.m_xc;
        DIST_GM dy = m_yc - Circ.m_yc;
        DIST_GM d2 = dx * dx + dy * dy;
        if(d2 < rDiff2)
            return true;
        return false;
    }
    //_________________________________________________________________________________________________________________________________________________________
    //
    // Testira dal su dvije kruznice identicne
    //_________________________________________________________________________________________________________________________________________________________
    bool Equal(const CCircle& Circ) const { return (m_r == Circ.m_r && m_xc == Circ.m_xc && m_yc == Circ.m_yc); }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Trazi smjer u kojem je zadana tocka u odnosu na srediste kruga i x-os.
    // U slucaju da se zada tocka koja je jednaka sredistu, smjer nije definiran.
    //_________________________________________________________________________________________________________________________________________________________
    ANGLERAD_GM GetDirectionToPoint(COORD_GM x, COORD_GM y) const
    {
        assert(IsOk());

        DIST_GM dx = x - m_xc;
        DIST_GM dy = y - m_yc;

        assert(dx != 0 || dy != 0);

        return atan2(dy, dx);
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Daje tocku na kruznici koja je pod zeljenim kutem u odnosu na srediste
    //_________________________________________________________________________________________________________________________________________________________
    void GetPointAtAngle(ANGLERAD_GM Angle, COORD_GM& x, COORD_GM& y) const
    {
        assert(IsOk());

        x = m_xc + m_r * cos(Angle);
        y = m_yc + m_r * sin(Angle);
    }
};

//=====================================================================================================================================
//
// Klasa za kruzni luk
//
//=====================================================================================================================================
class CCircleArc : public CCircle
{
protected:
    ANGLERAD_GM m_BeginAngle; // Pocetni kut [rad]
    ANGLERAD_GM m_DeltaAngle; // Promjena kuta koju opisuje luk [rad] (moze biti i negativan kut, moze biti veci od 2PI)

public:
    // Pristupne rutine
    ANGLERAD_GM GetBeginAngle() const { return m_BeginAngle; }

    ANGLERAD_GM GetDeltaAngle() const { return m_DeltaAngle; }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Vraca true ako je smjer luka u pozitivnom smjeru (tj promjena kuta je pozitivna).
    //_________________________________________________________________________________________________________________________________________________________
    bool IsPositiveDirection() const { return (m_DeltaAngle >= 0 ? true : false); }

    void SetArc(ANGLERAD_GM BeginAngle, ANGLERAD_GM DeltaAngle)
    {
        m_BeginAngle = BeginAngle;
        m_DeltaAngle = DeltaAngle;
    }

    void SetArc(const CCircle* pCircle, ANGLERAD_GM BeginAngle, ANGLERAD_GM DeltaAngle)
    {
        assert(pCircle);
        m_xc = pCircle->GetXc();
        m_yc = pCircle->GetYc();
        m_r = pCircle->GetRadius();

        m_BeginAngle = BeginAngle;
        m_DeltaAngle = DeltaAngle;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje luka na temelju pocetne tocke iz koje pocinje luk
    // Luk krece iz tocke (xStart, yStart), tangenta na njega u toj tocki ima smjer angStart i promjena kuta je DeltaAngle (koja moze biti i
    // neg.) Radijus luka moze biti pozitivan (tada je luk lijevo od tangente), ili neg (a onda je luk desno od tangente)
    //
    // Kombinacijama razlicitih predznaka radijusa i promjene kuta dobijemo cetiri kombinacije:
    // 1) r>0, dAng>0 -> luk je lijevo od tangente i ide u smjeru tangente
    // 2) r<0, dAng<0 -> luk je desno od tangente i ide u smjeru tangente
    // 3) r>0, dAng<0 -> luk je lijevo od tangente i ide u smjeru suprotnom od tangente
    // 4) r<0, dAng>0 -> luk je desno od tangente i ide u smjeru suprotnom od tangente
    //_________________________________________________________________________________________________________________________________________________________
    void SetArc(COORD_GM xStart, COORD_GM yStart, ANGLERAD_GM angStart, RADIUS_GM Radius, ANGLERAD_GM DeltaAngle)
    {
        // Racunamo srediste kruznice
        SetCircleFromTangent(xStart, yStart, angStart, Radius);

        // Racunamo pocetni kut i promjenu kuta
        if(Radius >= 0)
            m_BeginAngle = angStart - PI / 2;
        else
            m_BeginAngle = angStart + PI / 2;

        m_DeltaAngle = DeltaAngle;
    }

public:
    // Vraca duljinu luka (koja moze biti i veca od opsega kruznice ako je promjena kuta veca od 2PI).
    // Duljina luka ce biti >= 0
    LENGTH_GM GetArcLength() const { return abs(m_DeltaAngle) * m_r; }

    void GetBeginPoint(C2DPoint* pPoint) const
    {
        assert(pPoint);
        pPoint->x = m_xc + m_r * (COORD_GM)cos(m_BeginAngle);
        pPoint->y = m_yc + m_r * (COORD_GM)sin(m_BeginAngle);
    }
    void GetBeginPoint(COORD_GM& x, COORD_GM& y) const
    {
        x = m_xc + m_r * (COORD_GM)cos(m_BeginAngle);
        y = m_yc + m_r * (COORD_GM)sin(m_BeginAngle);
    }

    void GetEndPoint(C2DPoint* pPoint) const
    {
        assert(pPoint);
        pPoint->x = m_xc + m_r * (COORD_GM)cos(m_BeginAngle + m_DeltaAngle);
        pPoint->y = m_yc + m_r * (COORD_GM)sin(m_BeginAngle + m_DeltaAngle);
    }

    // Ako krenemo iz pocetne tocke kruznog luka i prijedemo kutnu udaljenost dAng, gdje bi bili?
    void GetPointByAngle(ANGLE_GM dAng, C2DPoint* pPoint) const
    {
        assert(pPoint);
        pPoint->x = m_xc + m_r * (COORD_GM)cos(m_BeginAngle + dAng);
        pPoint->y = m_yc + m_r * (COORD_GM)sin(m_BeginAngle + dAng);
    }

    // Ako krenemo iz pocetne tocke kruznog luka i prijedemo udaljenost d u smjeru luka, gdje bi bili?
    // d moze biti i negativan
    void GetPointByDistance(DIST_GM d, C2DPoint* pPoint) const
    {
        if(m_r == 0)
        {
            pPoint->x = m_xc;
            pPoint->y = m_yc;
            return;
        }
        ANGLE_GM dAng = d / m_r;
        GetPointByAngle(dAng, pPoint);
    }

    // Ak krenemo iz pocetne tocke kruznog luka i prijedemo udaljenost d u smjeru luka, koji bi bio kut u odnosu na srediste luka?
    // d moze biti i negativan
    ANGLERAD_GM GetAngleByDist(DIST_GM d) const
    {
        if(m_r == 0)
        {
            // Nedefinirano, vrati pocetni kut
            return m_BeginAngle;
        }
        return m_BeginAngle + d / m_r;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Trazi sjecista kruznog luka i duzine.
    // pPoints - niz tocaka u kojem se vracaju sjecista, mora biti duljine najmanje 2.
    // Vraca broj tocaka sjecista (0,1 ili 2)
    //_________________________________________________________________________________________________________________________________________________________
    int GetIntersectionWithLine(const CBoundLineImp& Line, C2DPoint* pPoints) const
    {
        int n = CCircle::GetIntersectionWithLine(Line, pPoints);

        int nGood = 0;
        for(int i = 0; i < n; i++)
        {
            if(CheckPoint(pPoints[i]))
            {
                if(nGood != i)
                    pPoints[nGood] = pPoints[i]; // Shiftanje tocke unaprijed ako je ptorebno
                nGood++;
            }
        }
        return nGood;
    }
    //_________________________________________________________________________________________________________________________________________________________
    //
    // Trazi sjecista kruznog luka i kruznice.
    // pPoints - niz tocaka u kojem se vracaju sjecista, mora biti duljine najmanje 2.
    // Vraca broj tocaka sjecista (0,1 ili 2). Ako je luk podskup kruznice, vraca -1 (tj ima beskonacno mnogo sjecista).
    //_________________________________________________________________________________________________________________________________________________________
    int GetIntersectionWithCircle(const CCircle& Circ, C2DPoint* pPoints) const
    {
        int n = Circ.GetIntersectionWithCircle(*this, pPoints);

        if(n == -1)
            return -1;

        // Provjeri dal su vracene tocke na kruznom luku
        int nGood = 0;
        for(int i = 0; i < n; i++)
        {
            if(CheckPoint(pPoints[i]))
            {
                if(nGood != i)
                    pPoints[nGood] = pPoints[i]; // Shiftanje tocke unaprijed ako je ptorebno
                nGood++;
            }
        }
        return nGood;
    }
    //_________________________________________________________________________________________________________________________________________________________
    //
    // Testira dal postoji pravac izmedu zadane tocke i sredista kruznog luka koji sijece sam kruzni luk. (koristi atan!)
    // Ak je zadana tocka na kruznici koja sadrzi luk, onda je to testiranje dal je tocka na luku.
    // Funkcija nema bas smisla ak je zadana tocka jednaka sredistu kruznog luka, u tom slucaju vraca false
    //_________________________________________________________________________________________________________________________________________________________
    bool CheckPoint(COORD_GM x, COORD_GM y) const
    {
        if(y - m_yc == 0 && x - m_xc == 0)
            return false;
        ANGLERAD_GM fi = (ANGLERAD_GM)atan2(y - m_yc, x - m_xc); // Kut pravca od sredista do zadane tocke

        if(m_DeltaAngle >= 0)
        {
            fi = NormAngle_Ang_2PI(fi, m_BeginAngle);
            assert(fi >= m_BeginAngle); // Ovo bi moralo biti ispunjeno nakon normalizacije
            if(fi <= m_BeginAngle + m_DeltaAngle)
                return true;
        }
        else
        {
            fi = NormAngle_Minus2PI_Ang(fi, m_BeginAngle);
            assert(fi <= m_BeginAngle); // Ovo bi moralo biti ispunjeno nakon normalizacije
            if(fi >= m_BeginAngle + m_DeltaAngle)
                return true;
        }
        return false;
    }
    bool CheckPoint(const C2DPoint& Pt) const { return CheckPoint(Pt.x, Pt.y); }
    //_________________________________________________________________________________________________________________________________________________________
    //
    // Racuna udaljenost po KL od pocetne tocke luka do zadane tocke.
    // Zadana tocka bi trebala biti na KL, al i ak nije, onda se racuna njezina projekcija na KL (osim ak je u centru KL, onda se ne moze)
    //_________________________________________________________________________________________________________________________________________________________
    DIST_GM GetDistToBeginPoint(COORD_GM x, COORD_GM y) const
    {
        if(y - m_yc == 0 && x - m_xc == 0)
            return 0;
        ANGLERAD_GM fi = (ANGLERAD_GM)atan2(y - m_yc, x - m_xc); // Kut pravca od sredista do zadane tocke

        if(m_DeltaAngle >= 0)
        {
            fi = NormAngle_Ang_2PI(fi, m_BeginAngle);
            assert(fi >= m_BeginAngle); // Ovo bi moralo biti ispunjeno nakon normalizacije
            ANGLERAD_GM dFi = fi - m_BeginAngle;
            return dFi * m_r;
        }
        else
        {
            fi = NormAngle_Minus2PI_Ang(fi, m_BeginAngle);
            assert(fi <= m_BeginAngle); // Ovo bi moralo biti ispunjeno nakon normalizacije
            ANGLERAD_GM dFi = m_BeginAngle - fi;
            return dFi * m_r;
        }
    }

    bool IsOk()
    {
        if(m_r < 0)
        {
            assert(0);
            return false;
        }
        return true;
    }

    DIST_GM GetDistanceToLine(const CBoundLineImp& Line, C2DPoint* pArcPoint, C2DPoint* pLinePoint) const
    {
        // Provjeri prvo dal ima sjecista, ak ima min udalj je nula
        C2DPoint Inters[2];
        int n = GetIntersectionWithLine(Line, Inters);
        if(n > 0)
        {
            DIST_GM MinDist2 = std::numeric_limits<DIST_GM>::max(); // Minimalna udalj na kvadrat
            int iMinDistPoint = -1;

            // Nadi sjeciste koje je najblize prvoj tocki duzine
            for(int i = 0; i < n; i++)
            {
                DIST_GM dx = Inters[i].x - Line.p1.x;
                DIST_GM dy = Inters[i].y - Line.p1.y;
                DIST_GM d2 = dx * dx + dy * dy;
                if(d2 < MinDist2)
                {
                    MinDist2 = d2;
                    iMinDistPoint = i;
                }
            }
            assert(iMinDistPoint != -1);
            *pArcPoint = *pLinePoint = Inters[iMinDistPoint];
            return 0;
        }

        // Izracunaj udaljenost kruga od pravca
        DIST_GM d = Line.GetDistanceToPoint(m_xc, m_yc) - m_r;

        // Izr projekciju sredista kruga na pravac
        COORD_GM px, py;
        Line.GetPointProjection(m_xc, m_yc, px, py);

        // Ak je projekcija na duzini i kruznom luku to je min udaljenost
        if(Line.CheckPoint(px, py) && CheckPoint(px, py))
        {
            // Najbliza tocka na KL je negdje izmedu sredista i projekcijske tocke na pravac
            DIST_GM dx = px - m_xc;
            DIST_GM dy = py - m_yc;
            NUM_GM ratio = m_r / (d + m_r);
            pArcPoint->x = m_xc + ratio * dx;
            pArcPoint->y = m_yc + ratio * dy;
            pLinePoint->x = px;
            pLinePoint->y = py;
            return d;
        }

        // Provjeri dal u obzir dolaze udaljenosti od krajnjih tocaka duzine i ako da,
        // izracunaj udaljenosti kruga do krajnjih tocaka duzine
        bool b1ok = false, b2ok = false;
        DIST_GM d2_1 = 0, d2_2 = 0;

        if(CheckPoint(Line.p1))
        {
            b1ok = true;
            DIST_GM dx = m_xc - Line.p1.x;
            DIST_GM dy = m_yc - Line.p1.y;
            d2_1 = dx * dx + dy * dy;
        }

        if(CheckPoint(Line.p2))
        {
            b2ok = true;
            DIST_GM dx = m_xc - Line.p2.x;
            DIST_GM dy = m_yc - Line.p2.y;
            d2_2 = dx * dx + dy * dy;
        }

        if(b1ok && b2ok)
        {
            if(d2_1 < d2_2)
            {
                // Udalj do prve tocke je manja
                DIST_GM dx = Line.p1.x - m_xc;
                DIST_GM dy = Line.p1.y - m_yc;
                d = sqrt(d2_1) - m_r;
                NUM_GM ratio = m_r / (d + m_r);
                pArcPoint->x = m_xc + ratio * dx;
                pArcPoint->y = m_yc + ratio * dy;
                *pLinePoint = Line.p1;
                return d;
            }
            else
            {
                // Udalj do druge tocke je manja
                DIST_GM dx = Line.p2.x - m_xc;
                DIST_GM dy = Line.p2.y - m_yc;
                d = sqrt(d2_2) - m_r;
                NUM_GM ratio = m_r / (d + m_r);
                pArcPoint->x = m_xc + ratio * dx;
                pArcPoint->y = m_yc + ratio * dy;
                *pLinePoint = Line.p2;
                return d;
            }
        }
        else if(b1ok)
        {
            DIST_GM dx = Line.p1.x - m_xc;
            DIST_GM dy = Line.p1.y - m_yc;
            d = sqrt(d2_1) - m_r;
            NUM_GM ratio = m_r / (d + m_r);
            pArcPoint->x = m_xc + ratio * dx;
            pArcPoint->y = m_yc + ratio * dy;
            *pLinePoint = Line.p1;
            return d;
        }
        else if(b2ok)
        {
            DIST_GM dx = Line.p2.x - m_xc;
            DIST_GM dy = Line.p2.y - m_yc;
            d = sqrt(d2_2) - m_r;
            NUM_GM ratio = m_r / (d + m_r);
            pArcPoint->x = m_xc + ratio * dx;
            pArcPoint->y = m_yc + ratio * dy;
            *pLinePoint = Line.p2;
            return d;
        }

        // Provjeri udaljenosti izmedu krajnjih tocaka luka i duzine
        DIST_GM MinDist2 = std::numeric_limits<DIST_GM>::max(); // Minimalna udalj na kvadrat

        // Krajnje tocke luka
        C2DPoint p1, p2;
        GetBeginPoint(&p1);
        GetEndPoint(&p2);

        COORD_GM d2 = p1.GetDistance2ToPoint(&Line.p1);
        if(d2 < MinDist2)
        {
            MinDist2 = d2;
            *pArcPoint = p1;
            *pLinePoint = Line.p1;
        }

        d2 = p1.GetDistance2ToPoint(&Line.p2);
        if(d2 < MinDist2)
        {
            MinDist2 = d2;
            *pArcPoint = p1;
            *pLinePoint = Line.p2;
        }

        d2 = p2.GetDistance2ToPoint(&Line.p1);
        if(d2 < MinDist2)
        {
            MinDist2 = d2;
            *pArcPoint = p2;
            *pLinePoint = Line.p1;
        }

        d2 = p2.GetDistance2ToPoint(&Line.p2);
        if(d2 < MinDist2)
        {
            MinDist2 = d2;
            *pArcPoint = p2;
            *pLinePoint = Line.p2;
        }

        return (DIST_GM)sqrt(MinDist2);
    }
    //_________________________________________________________________________________________________________________________________________________________
    //
    // Nalazi najkracu spojnicu od kruznog luka do kruznice, te tocke na KL i kruznici koje odgovaraju toj spojnici.
    // Ukoliko postoji presjeciste, vraca udalj 0. Ako je vise presjecista, vraca ono koje je najblize prvoj tocki KL.
    // TODO: treba rijsiti slucaj kada je jedno unutar drugog
    //_________________________________________________________________________________________________________________________________________________________
    DIST_GM GetDistanceToCircle(const CCircle& Circ, C2DPoint* pArcPoint, C2DPoint* pCircPoint) const
    {
        // Provjeri prvo dal ima sjecista, ak ima min udalj je nula
        C2DPoint Inters[2];
        int n = GetIntersectionWithCircle(Circ, Inters);
        if(n > 0)
        {
            if(n == -1)
            {
                // Ima beskonacno mnogo sjecista, mi vracamo pocetnu tocku luka
                GetBeginPoint(pArcPoint);
                *pCircPoint = *pArcPoint;
                return 0;
            }

            DIST_GM MinDist2 = std::numeric_limits<DIST_GM>::max(); // Minimalna udalj na kvadrat
            int iMinDistPoint = 0;

            COORD_GM x, y; // Pocetna tocka luka
            GetBeginPoint(x, y);

            // Nadi sjeciste koje je najblize pocetnoj tocki luka
            for(int i = 0; i < n; i++)
            {
                DIST_GM dx = Inters[i].x - x;
                DIST_GM dy = Inters[i].y - y;
                DIST_GM d2 = dx * dx + dy * dy;
                if(d2 < MinDist2)
                {
                    MinDist2 = d2;
                    iMinDistPoint = i;
                }
            }
            *pArcPoint = *pCircPoint = Inters[iMinDistPoint];
            return 0;
        }

        // Provjeri dal se spojnica sredista luka i kruznice nalazi na luku
        if(CheckPoint(Circ.m_xc, Circ.m_yc))
        {
            // Min udaljenost je udaljenost dvaju kruznica
            DIST_GM dx = Circ.m_xc - m_xc;
            DIST_GM dy = Circ.m_yc - m_yc;
            DIST_GM d = sqrt(dx * dx + dy * dy); // Udaljenost sredista
            NUM_GM ratio = m_r / d;
            pArcPoint->x = m_xc + ratio * dx;
            pArcPoint->y = m_yc + ratio * dy;
            ratio = (d - Circ.m_r) / d;
            pCircPoint->x = m_xc + ratio * dx;
            pCircPoint->y = m_yc + ratio * dy;
            return (d - m_r - Circ.m_r);
        }

        // Provjeri udaljenosti izmedu krajnjih tocaka luka i kruznice

        C2DPoint p1, p2; // Krajnje tocke luka
        GetBeginPoint(&p1);
        GetEndPoint(&p2);

        DIST_GM d2_1, d2_2; // Udalj krajnjih tocaka luka do centra kruznice na kvadrat

        d2_1 = p1.GetDistance2ToPoint(Circ.m_xc, Circ.m_yc);
        d2_2 = p2.GetDistance2ToPoint(Circ.m_xc, Circ.m_yc);

        if(d2_1 <= d2_2)
        {
            // Pocetna tocka luka je blize
            DIST_GM d_1 = sqrt(d2_1);
            DIST_GM dx = p1.x - Circ.m_xc;
            DIST_GM dy = p1.y - Circ.m_yc;
            NUM_GM ratio = Circ.m_r / d_1;
            pCircPoint->x = Circ.m_xc + ratio * dx;
            pCircPoint->y = Circ.m_yc + ratio * dy;
            *pArcPoint = p1;
            return (d_1 - Circ.m_r);
        }
        else
        {
            // Krajnja tocka luka je blize
            DIST_GM d_2 = sqrt(d2_2);
            DIST_GM dx = p2.x - Circ.m_xc;
            DIST_GM dy = p2.y - Circ.m_yc;
            NUM_GM ratio = Circ.m_r / d_2;
            pCircPoint->x = Circ.m_xc + ratio * dx;
            pCircPoint->y = Circ.m_yc + ratio * dy;
            *pArcPoint = p2;
            return (d_2 - Circ.m_r);
        }
    }
};

}; // namespace Geom

#endif // CIRCLE_H

//
