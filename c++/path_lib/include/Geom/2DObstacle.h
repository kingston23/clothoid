#ifndef _GEOM_2DOBSTACLE_H_
#define _GEOM_2DOBSTACLE_H_

#include "2DPoint.h"
#include "2DSquare.h"
#include "GeometryBase.h"
#include "Line.h"

#include <cmath>

// using namespace Geom;
namespace Geom
{
template<class CoordType = float, class T = CoordType>
class C2DObstacle : public CGeometryBase
{
public:
};

//________________________________________________________________________________________________________________________
//
// Kvadratna prepreka (korisno za robote)
//________________________________________________________________________________________________________________________
template<class CoordType = float, class T = CoordType>
class C2DSquareObstacle : public C2DObstacle
{
public:
};

}; // namespace Geom

#endif _GEOM_2DOBSTACLE_H_

// And when I find the reason, I still can't get used to it
