#ifndef _2D_Clothoid_Algorithms_H_
#define _2D_Clothoid_Algorithms_H_

#include "2DClothoid.h"
#include "2DPoint.h"
#include "2DRotation.h"
#include "Circle.h"
#include "GeometryBase.h"
#include "Line.h"
#include "Math/Numeric.h"

#include <algorithm>
#include <cmath>
#include <limits>

using namespace MathMB;

namespace Geom
{
//=================================================================================================================================================================
//
// Klasa s raznim algoritmima za rad s klotoidama.
//
//=================================================================================================================================================================
class C2DClothoidAlgorithms : public CGeometryBase
{
public:
    C2DClothoidAlgorithms() {}

    ~C2DClothoidAlgorithms() {}

    //________________________________________________________________________________________________________________________________________________
    //
    // Dobivanje klotoide (tj. dviju klotoida i po potrebi dvaju pravaca) koji izgladuju ostri zaokret.
    // Ostri zaokret opisan je sa 2 duzine, razapete triju tockama.
    // Izgladuje se ona strana gdje je kut manji od PI.
    // TUDU: omoguciti zaokret veci od PI
    //________________________________________________________________________________________________________________________________________________
    static bool SmoothSharpTurn(
        const C2DPoint& S, // Startna tocka zakreta
        const C2DPoint& M, // Srednja tocka zakreta.
        const C2DPoint& G, // Ciljna tocka zakreta. Dakle podrucje zakreta je zatvoreno vektorima SM i MG. Tocke S, M, G ne smiju biti
                           // identicne ili kolinearne.
        T d1Fraction,      // <0,1] dio duljine MS koji se smije koristiti za izgladivanje. Npr. ako se stavi 0.5, ulazna klotoida ne smije
                      // poceti prije polovice duzine MS. Za ovo nema smisla postaviti premalu vrijednost (blizu nuli), jer onda ne trebamo
                      // klotoide vec prakticki imamo rotaciju na mjestu.
        T d2Fraction, // <0,1] dio duljine MG koji se smije koristiti za izgladivanje.
        // T dMax, // [>0] Na kojoj najvecoj udaljenosti od srednje tocke smije poceti klotoida. Ova udaljenost mora biti manja od obiju
        // duzina pravaca (ako nije, algoritam ju sam odreze) Vjerojatno nepotrebno!
        T eMax, // [>0] Maksimalna udaljenost klotoide od srednje tocke.
        T sMax, // [>0] Maksimalna duljina jedne klotoide. Ovo ogranicenje je vazno zbog ogranicenja duljine lookup tablice pomocu koje
                // aproksimiramo klotoidu.
        T& d,   // Na kojoj udaljenosti od srednje tocke pocinje vracena klotoida
        T& e,   // Udaljenost vracene klotoide od srednje tocke
        CLineParBounded&
            StartLine, // Pocetni ravni dio, tj. segment od tocke S do pocetka ulazne klotoide - ako je nepotreban duljina ce mu biti nula
        C2DClothoidApp& StartClothoid, // Ulazna klotoida
        C2DClothoidApp& EndClothoid,   // Izlazna klotoida
        CLineParBounded&
            EndLine // Krajnji ravni dio, tj. segment od kraja izlazne klotoide do tocke G - ako je nepotreban duljina ce mu biti nula
    )
    {
        // TODO: omoguciti zadavanje dijela SM i MG segmenata koji se smiju koristiti za zavoj

        // assert(dMax>0);
        assert(d1Fraction > 0 && d2Fraction > 0);
        assert(eMax > 0);
        assert(sMax > 0);

        // Translatiram tocke tako da srednja dode u ishodiste
        T xTranslation = -M.x, yTranslation = -M.y;

        T xs = S.x + xTranslation;
        T ys = S.y + yTranslation;

        // T xm = 0;
        // T ym = 0;

        T xg = G.x + xTranslation;
        T yg = G.y + yTranslation;

        // Rotiram pocetnu i krajnju tocku, tako da pocetna dode na negativnu x-os. Srednja ostaje u ishodistu.

        T RotAngle = PI - atan2(ys, xs);
        C2DRotationMatrix<T> R(RotAngle);

        R.RotatePoint(xs, ys);
        R.RotatePoint(xg, yg);

        // Testiramo kolinearnost i identicnost tocaka.
        if(IsZero(xs))
            return false;

        if(IsZero(yg))
            return false;

        // Proracune radimo za slucaj skretanja klotoide u poz. smjeru. Ako klotoida ipak mora skretati u negativnom smjeru, to pamtimo u
        // ovoj zastavici i kasnije radimo inverziju.
        bool bNegativeTurn = false;
        if(yg < 0)
        {
            bNegativeTurn = true;
            yg = -yg;
        }

        // Provjeravam udaljenost dMax (ne smije biti veca od duzina definiranih tockama zavoja)
        T dStart = abs(xs);               // duljina segmenta MS
        T dEnd = sqrt(xg * xg + yg * yg); // duljina segmenta MG

        d1Fraction = std::min(d1Fraction, (T)1);
        d2Fraction = std::min(d2Fraction, (T)1);

        // dMax = std::min(dMax, dStart);
        T dMax = std::min(dStart * d1Fraction, dEnd * d2Fraction);

        // Racunam kut tangente klotoide na polovici zavoja. To je polovica ukupne promjene kuta za cijeli zavoj.
        T FullAngle = atan2(yg, xg);      // ukupni kut zakreta
        T HalfAngle = (T)0.5 * FullAngle; // kut na polovici zakreta

        // CLineImp<T> Bisector;
        // Bisector.LineFromDirectionAndPoint(HalfAngle+PI/2, 0, 0);

        // simetrala zakreta - koeficijenti impl. jed. pravca
        T Bisector_A = -sin(HalfAngle + PI / 2);
        T Bisector_B = cos(HalfAngle + PI / 2);

        // U nastavku je BC = basic clothoid (osnovni klotoid sa c=1 spremljen u lookup tablici)
        T sBC = sqrt(2 * HalfAngle); // prevaljeni put osnovne klotoide u lookup tablici (sa c=1) u tocki gdje je ona okomita na simetralu
        T xBC, yBC;                  // izlaz iz lookup tablice - treba nam samo kao medurezultat
        C2DClothoidApp::m_pLookupTable->GetBasicClothoidPoint(sBC, xBC, yBC);

        // Tocka sjecista osnovne klotoide i simetrale
        T xBCm = -Bisector_B * yBC / Bisector_A; // Bisector.GetX(yBC);
        T yBCm = yBC;

        // Tocka na x osi na kojoj pocinje osnovna klotoida
        T xBCs = xBCm - xBC;
        // T yBCs = 0;

        T dBC = -xBCs; // udaljenost pocetka osnovne klotoide do sredista zavoja (tj. ishodista)
        assert(dBC > 0);

        T eBC; // udaljenost sredista zavoja (tj. ishodista) do najblize tocke osnovne klotoide (tj. koliko se klotoida udaljava od
               // linijskog profila staze zadanog tockama)
        eBC = sqrt(xBCm * xBCm + yBCm * yBCm);

        // Scaling faktori klotoide koji su potrebni da se tocno postigne dMax, eMax, sMax
        T C_d = dMax / dBC;
        T C_e = eMax / eBC;
        T C_s = sMax / sBC;

        // Odredujemo gornju granicu scaling faktora s kojom su zadovoljeni svi uvjeti. Ona se dobije kao minimum dosad izracunatih C-ova.
        T C_upper;
        if(C_d < C_e)
            C_upper = C_d;
        else
            C_upper = C_e;
        if(C_s < C_upper)
            C_upper = C_s;

        // Ovdje ne gledamo prepreke, pa je C_upper zapravo konacni C
        T C = C_upper;

        d = dBC * C;
        e = eBC * C;
        T ClothoidLength = sBC * C; // duljina jedne klotoide

        //__________________________________________________________________________________________________________________
        // Odredi parametre pocetnog pravca (ak je potreban)

        if(IsPositive(dStart - d))
        {
            // Pocetni pravac je potreban

            // T xg_StartLine = -d;
            // T yg_StartLine = 0;

            //// Rotacija i translacija u pocetni koord. sustav
            // R.UnRotatePoint(xg_StartLine, yg_StartLine);
            // xg_StartLine -= xTranslation;
            // yg_StartLine -= yTranslation;

            // StartLine.LineFromPoints(S.x, S.y, xg_StartLine, yg_StartLine);

            StartLine.LineFromDirectionAndPoint(-RotAngle, S.x, S.y);
            StartLine.Length(dStart - d);
        }
        else
            StartLine.Length(0);

        //__________________________________________________________________________________________________________________
        // Odredi parametre prve (ulazne) klotoide

        {
            T x0 = -d;
            T y0 = 0;
            // ... Rotacija i translacija natrag u pocetni koord. sustav
            R.UnRotatePoint(x0, y0);
            x0 -= xTranslation;
            y0 -= yTranslation;

            T ang0 = -RotAngle;
            T k0 = 0;

            T C1 = C;
            if(bNegativeTurn)
                C1 = -C;

            if(!StartClothoid.Set2(x0, y0, ang0, k0, C1, ClothoidLength))
            {
                assert(0);
            }
        }

        //__________________________________________________________________________________________________________________
        // Odredi parametre druge (izlazne) klotoide

        {
            T x0 = xBCm * C;
            T y0 = yBCm * C;
            // ... Rotacija i translacija natrag u pocetni koord. sustav
            T ang0 = HalfAngle;
            T C2 = -C;
            if(bNegativeTurn)
            {
                y0 = -y0;
                ang0 = -ang0;
                C2 = C;
            }
            R.UnRotatePoint(x0, y0);
            x0 -= xTranslation;
            y0 -= yTranslation;

            ang0 -= RotAngle;

            T k0 = StartClothoid.Get_c() * ClothoidLength;

            if(!EndClothoid.Set2(x0, y0, ang0, k0, C2, ClothoidLength))
            {
                assert(0);
            }
        }

        //__________________________________________________________________________________________________________________
        // Odredi parametre krajnjeg pravca (ak je potreban)

        // ... Ovo je pocetna tocka krajnjeg pravca (u slucaju da je potreban). Krajnja tocka mu je jednaka xg,yg.

        if(IsPositive(dEnd - d))
        {
            // Krajnji pravac je potreban

            // ... Pravac od srednje tocke zavoja (ishodista) prema krajnjoj tocki
            CLinePar EndDirection;
            EndDirection.LineFromDirectionAndPoint(FullAngle, 0, 0);

            T xs_EndLine, ys_EndLine;
            EndDirection.GetPoint(d, xs_EndLine, ys_EndLine);

            if(bNegativeTurn)
                ys_EndLine = -ys_EndLine;

            // Rotacija i translacija u pocetni koord. sustav
            R.UnRotatePoint(xs_EndLine, ys_EndLine);
            xs_EndLine -= xTranslation;
            ys_EndLine -= yTranslation;

            EndLine.LineFromPoints(xs_EndLine, ys_EndLine, G.x, G.y);
        }
        else
            EndLine.Length(0);

        return true;
    }

    // Struktura s ulazno-izlaznim podacima za funkciju ciju nul-tocku trazimo
    struct SIO_ConnectCircleLine1C_fZero
    {
        SIO_ConnectCircleLine1C_fZero(const S2DPathState& P0_, const CLinePar& G1G2_, const CCircle& CircleP0_) :
            P0(P0_), G1G2(G1G2_), CircleP0(CircleP0_)
        {
        }

        T C;                      // [in] Faktor skaliranja klotoide koji je rjesenje naseg problema i kojeg trazimo
        const S2DPathState& P0;   // [in]
        const CLinePar& G1G2;     // [in]
        const CCircle& CircleP0;  // [in] kruznica po kojoj se trenutno gibamo
        bool bAdmissibleSolution; // [out] bit ce false ako je rjesenje matematicki tocno, ali u praksi nedopustivno iz nekog razloga
        T ClothAng0;              // [out]
        T ClothLength;            // [out]
        T dAng_CircArc;           // [out] zakret kruznog luka [rad]
    };

    //________________________________________________________________________________________________________________________________________________
    //
    // Funkcija za spajanje kruznice i pravca pomocu kruznog luka i klotoide.
    // Svrha funkcije je izgladivanje putanje uz  ne-nula pocetnu zakrivljenost
    // TUDU: 1) ogranicenje duljine klotoide
    //________________________________________________________________________________________________________________________________________________
    static bool ConnectCircleLine1C(const S2DPathState& P0, // pocetno stanje koje se sastoji od pocetne pozicije, kuta i zakrivljenosti
                                    const C2DPoint& G1,     // G1G2 je linija na koju moramo "glatko sletjeti"
                                    const C2DPoint& G2,
                                    CCircleArc& Arc,          // kruzni luk - prvi dio vracene putanje
                                    C2DClothoidApp& Clothoid, // klotoida - drugi dio vracene putanje
                                    CLineParBounded& Line // zavrsna linija. ako je potrebna imat ce duljinu >0, a inace bit ce duljine nula
    )
    {
        if(IsZero(P0.k))
            return false; // ova funkcija zahtijeva pocetnu zakrivljenost != 0

        //__________________________________________________________________________________________________________________
        // Ispitivanje nuznih uvjeta

        // Ovo nisu (barem ne svi) uvjeti postojanja rjesenja vec ovime reduciramo skup mogucih rjesenja.
        // U nekim slucajevima rjesenje moze postojati cak i ako ovi uvjeti nisu ispunjeni, ali takva rjesenja ne razmatramo.
        // Jedan od razloga je i numericka nestabilnost koja bi se mogla pojaviti u takvim slucajevima.

        // Tangenta na putanju kroz tocku P0
        CLinePar P0Tangent;
        P0Tangent.LineFromDirectionAndPoint(P0.ang, P0.x, P0.y);

        // 1. nuzan uvjet: Ako se pocetno gibamo ulijevo, onda i cilj mora biti s lijeve strane, i obrnuto.
        // Uzimamo da G1 ili G2 moraju biti lijevo
        if(P0Tangent.GetDistanceToPoint(G1.x, G1.y) * P0.k <= 0 && P0Tangent.GetDistanceToPoint(G2.x, G2.y) * P0.k <= 0)
            return false; // Cilj je sa krive strane

        // 2. nuzan uvjet: Ako skrecemo ulijevo, onda pocetna tocka mora biti lijevo od vektora G1G2, i obrnuto

        // Pravac G1-G2
        CLinePar G1G2;
        if(!G1G2.LineFromPoints(G1.x, G1.y, G2.x, G2.y))
            return false;

        if(G1G2.GetDistanceToPoint(P0.x, P0.y) * P0.k <= 0)
            return false; // Nedozvoljen smjer

        // 3. nuzan uvjet: Ako skrecemo ulijevo, pravac na koji slijecemo mora biti desno od kruznice po kojoj se trenutno gibamo

        // Pronadi kruznicu po kojoj se trenutno gibamo
        CCircle Circle;
        Circle.SetCircleFromTangent(P0.x, P0.y, P0.ang, 1 / P0.k);
        T d_G1G2_CircleCenter = G1G2.GetDistanceToPoint(Circle.m_xc, Circle.m_yc);
        if(IsLessOrEqual(d_G1G2_CircleCenter, Circle.m_r))
            return false; // Ako skrecemo ulijevo, kruznica po kojoj se gibamo mora biti lijevo od pravca na koji ciljamo

        //__________________________________________________________________________________________________________________
        // Ogradivanje rjesenja (tj. trazenje intervala [C1 C2] u kojem je rjesenje)

        // Odredivanje donja granice faktora skaliranja (C1)

        // Zarotirajmo osnovnu klotoidu tako da ima pocetni kut u smjeru vektora G2G1, i da pocinje na pravcu G2G1 (recimo u G1)
        C2DClothoidApp CL;
        if(!CL.Set2(G1.x, G1.y, G1G2.m_Direction + PI, 0, -1, 0))
            return false;

        // Potrazimo tocku P1 na toj klotoidi koja ima zakrivljenost kao u P0

        T s_P1 = abs(P0.k);
        C2DPoint P1;
        T ang, k;
        if(!CL.GetPoint(s_P1, P1.x, P1.y, ang, k))
            return false;
        // Saznajmo udaljenost pravca G1G2 od pronadene tocke
        T d_P1_G1G2 = G1G2.GetDistanceToPoint(P1.x, P1.y);

        if(IsEqual(d_P1_G1G2, 0))
            return false; // Trazeni zakret je zanemariv - i moze se aproksimirati pravcem

        // Ovo je scaling faktor koji namjesta da se tocka s zeljenom zakrivljenoscu poklopi sa najblizom tockom na kruznici
        // Lijevi bracket koji se ovako dobije je u pravilu premali, i trebalo bi vrijediti fMin(C1)<0
        T C1 = abs((d_G1G2_CircleCenter - Circle.GetRadius()) / d_P1_G1G2);
        C1 = std::min(C1, (T)1); // Mislim da se za >1 ne bi dobili dobri rezultati. TUDU: provjeriti

        // Gornja granica
        T C2;
        T dth = G1G2.m_Direction - P0.ang;
        if(P0.k > 0)
        {
            // Pozitivni zakret - to znaci da i zahtjevani zakret mora biti pozitivan
            dth = NormAngle_Zero_2PI(dth);
            T dth_P1P0_max = std::min(2 * PI - dth, PI);
            C2 = sqrt(2 * dth_P1P0_max) / P0.k;
        }
        else
        {
            dth = NormAngle_Minus2PI_Zero(dth);
            T dth_P1P0_max = std::max(-2 * PI - dth, -PI);
            C2 = sqrt(-2 * dth_P1P0_max) / (-P0.k);
        }

        if(C1 > C2)
            return false; // Ne mozemo ograditi rjesenje

        if(P0.k > 0)
        {
            C1 = -C1; // Mora biti <0, jer zakrivljenost ide od pozitive prema nuli
            C2 = -C2;
        }

        //__________________________________________________________________________________________________________________
        // Trazenje rjesenja (iterativnom metodom)

        bool bFoundSolution = false;

        // Ulazno-izlazni podaci za poziv funkcije  ConnectCircleLine1C_fZero ciju nul tocku trazimo

        // Pronadi srediste kruznice po kojoj se trenutno gibamo
        CCircle CircleP0;
        CircleP0.SetCircleFromTangent(P0.x, P0.y, P0.ang, 1 / P0.k);

        SIO_ConnectCircleLine1C_fZero IO(P0, G1G2, CircleP0);

        IO.C = C1;
        T f1 = ConnectCircleLine1C_fZero(IO);

        IO.C = C2;
        T f2 = ConnectCircleLine1C_fZero(IO);

        T C;

        if(IsZero(f1))
        {
            C = C1;
            bFoundSolution = true;
        }
        else if(IsZero(f2))
        {
            C = C2;
            bFoundSolution = true;
        }
        else if(f1 * f2 > 0)
        {
            // velika je vjerojatnost da rjesenje ne postoji (nije uspjelo ogradivanje rjesenja)
            return false;
        }
        else
        {
            // Rijesi metodom bisekcije.
            T f = f1;
            T dC;

            if(f < 0)
            {
                dC = C2 - C1;
                C = C1;
            }
            else
            {
                dC = C1 - C2;
                C = C2;
            }

            for(int i = 1; i <= 30; i++)
            {
                dC = (T)0.5 * dC;
                T Cmid = C + dC;
                IO.C = Cmid;
                T fmid = ConnectCircleLine1C_fZero(IO);

                if(fmid < 0)
                    C = Cmid;

                if(abs(dC) < (T)0.0001 || IsZero(fmid))
                {
                    // Pronadeno rjesenje u 'i' iteracija
                    bFoundSolution = true;
                    break;
                }
            }
        }

        // U matlab verziji funkcije isprobana je jos false position metoda.
        // Ta metoda je opcenito sigurna (ne izlazi iz bracketa). Medutim njezin je problem ako funkcija poprima
        // ekstremne vrijednosti (tezi u inf). Drugi problem je kad funkcija ima ravan dio, onda se ova metoda
        // jako dugo muci, i obicno u tom slucaju potrosi vise iteracije nego bisekcija.
        // U slucaju da je funkcija priblizno linearna, ova metoda je znacajno brza od bisekcije.
        // Medutim, buduci da joj broj iteracija moze znacajno varirati od slucaja do slucaja, ne upotrebljavamo ju.

        if(!bFoundSolution)
        {
            return false; // premasen max. broj iteracija, a nije pronadeno rjesenje
        }
        else
        {
            // Izracunaj parametre kruznog luka i klotoide

            // Moramo ponoviti proracun, jer zadnji poziv funkcije fMin mozda nije onaj koji odgovara rjesenju (tj. odbacen je)
            IO.C = C;
            ConnectCircleLine1C_fZero(IO);

            if(!IO.bAdmissibleSolution)
                return false; // pronadeno rjesenje nije dopustivo

            // Izracunajmo pocetnu tocku klotoide (tj. zavrsnu tocku luka).
            T AngleOnCircle = IO.ClothAng0 - Sign(P0.k) * PI * (T)0.5;
            C2DPoint P1;
            IO.CircleP0.GetPointAtAngle(AngleOnCircle, P1.x, P1.y);
            Clothoid.Set2(P1.x, P1.y, IO.ClothAng0, P0.k, IO.C, IO.ClothLength);

            // Nije jos gotovo - Provedi jos dodatni test dopustivosti pronadenog rjesenja - druga klotoida se mora spustiti na pravac G1G2
            // prije tocke G2
            CLineImp G1G2_imp;
            G1G2.GetLineImp(G1G2_imp); // pretvaramo u imp. oblik jer je malo efikasniji
            CLineImp G1G2_perp;
            G1G2_perp.GetPerpendicularLineThroughPoint(G1G2_imp, G2.x, G2.y);
            T xPathEnd, yPathEnd;
            T ang, k;
            Clothoid.GetPoint(Clothoid.m_Length, xPathEnd, yPathEnd, ang, k);
            if(!G1G2_perp.IsPointLeft(xPathEnd, yPathEnd))
                return false; // solution is not before G2

            // Kruzni luk
            Arc.SetArc(P0.x, P0.y, P0.ang, 1 / P0.k, IO.dAng_CircArc);

            // Zavrsni pravac
            T LineLength = G2.GetDistanceToPoint(xPathEnd, yPathEnd);
            if(!IsZero(LineLength))
            {
                (CLinePar&)Line = G1G2; // sve isto kao i linija G1G2, osim pocetne tocke, i jos treba dodati duljinu linije
                Line.p0.x = xPathEnd;
                Line.p0.y = yPathEnd;
                Line.m_Length = LineLength;
            }
            else
                Line.m_Length = 0;
        }

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Funkcija kojoj trazimo nul-tocku. Nul-tocka te funkcije rezultira rjesenjem problema spajanja kruznice i pravca klotoidom.
    //________________________________________________________________________________________________________________________________________________
    static T ConnectCircleLine1C_fZero(SIO_ConnectCircleLine1C_fZero& IO)
    {
        IO.bAdmissibleSolution = true; // Dal je trenutno rjesenje dopustivo

        const S2DPathState& P0 = IO.P0;
        const CCircle& CircleP0 = IO.CircleP0;

        // Pronadi udaljenost sredista kruznice od pravca G1G2
        T d_G1G2_Circle = IO.G1G2.GetDistanceToPoint(CircleP0.m_xc, CircleP0.m_yc);

        // Provuci klotoidu kroz tocku G1 i nadi tocku P1 na njoj koja ima zakrivljenost jednaku zakrivljenosti gibanja robota
        C2DClothoidApp CL;
        CL.Set2(IO.G1G2.p0.x, IO.G1G2.p0.y, IO.G1G2.m_Direction + PI, 0, IO.C, 0);

        S2DPathState P1;
        P1.k = IO.P0.k;
        T s_kP1 = -P1.k / CL.Get_c();
        T ang, k;
        if(!CL.GetPoint(s_kP1, P1.x, P1.y, ang, k))
            return false;
        P1.ang = IO.G1G2.m_Direction + CL.Get_c() * (T)0.5 * s_kP1 * s_kP1;

        // Nadi kruznicu u tocki P1 klotoide
        CCircle CircleP1;
        CircleP1.SetCircleFromTangent(P1.x, P1.y, P1.ang, 1 / P1.k);

        // Pronadi udaljenost kruznice od pravca G1G2
        T d_G1G2_CircleP1 = IO.G1G2.GetDistanceToPoint(CircleP1.m_xc, CircleP1.m_yc);

        T out = d_G1G2_Circle - d_G1G2_CircleP1;

        if(IsZero(out)) // Zaokruzivanje na nulu
            out = 0;

        // Kut poluzakreta klotoide mora biti <= pi da bi rjesenje bilo dopustivo
        T s_PI = abs(IO.C) * sqrt(2 * PI);
        if(s_kP1 > s_PI)
            IO.bAdmissibleSolution = false;

        // Kut poluzakreta kruznice mora biti <= pi (inace bi imali neprirodne putanje)
        T dAng_CircArc;
        if(P0.k > 0)
            dAng_CircArc = NormAngle_Zero_2PI(IO.ClothAng0 - P0.ang);
        else
            dAng_CircArc = NormAngle_Minus2PI_Zero(IO.ClothAng0 - P0.ang);
        if(abs(dAng_CircArc) > PI)
            IO.bAdmissibleSolution = false;

        // Izlazne informacije korisne za racunanje putanje
        IO.dAng_CircArc = dAng_CircArc;
        IO.ClothLength = s_kP1;
        IO.ClothAng0 = CL.m_ang0 + CL.Get_c() * (T)0.5 * s_kP1 * s_kP1 + PI;

        return out;
    }

    // Struktura s ulazno-izlaznim podacima za funkciju ciju nul-tocku trazimo
    struct SIO_ConnectCircleLine2C_fZero
    {
        SIO_ConnectCircleLine2C_fZero(const S2DPathState& P0_, CLinePar& G1G2_) : P0(P0_), G1G2(G1G2_) {}

        T C;                      // [in] Faktor skaliranja klotoide koji je rjesenje naseg problema i kojeg trazimo
        const S2DPathState& P0;   // [in]
        const CLinePar& G1G2;     // [in]
        bool bAdmissibleSolution; // [out] bit ce false ako je rjesenje matematicki tocno, ali u praksi nedopustivno iz nekog razloga
        T FirstClothLength;       // [out]
        T SecondClothLength;      // [out]
    };

    //________________________________________________________________________________________________________________________________________________
    //
    // Funkcija za spajanje kruznice i pravca pomocu dvije klotoide.
    // Svrha funkcije je izgladivanje putanje uz  ne-nula pocetnu zakrivljenost
    // TUDU: 1) ogranicenje duljine klotoide 2) ispitivanje da li se izbjegava puni krug.
    //________________________________________________________________________________________________________________________________________________
    static bool ConnectCircleLine2C(const S2DPathState& P0, // pocetno stanje koje se sastoji od pocetne pozicije, kuta i zakrivljenosti
                                    const C2DPoint& G1,     // G1G2 je linija na koju moramo "glatko sletjeti"
                                    const C2DPoint& G2,
                                    C2DClothoidApp& Clothoid1, // prva klotoida - prvi dio vracene putanje
                                    C2DClothoidApp& Clothoid2, // druga klotoida - drugi dio vracene putanje
                                    CLineParBounded& Line // zavrsna linija. ako je potrebna imat ce duljinu >0, a inace bit ce duljine nula
    )
    {
        bool bResult = true;

        if(IsZero(P0.k))
            return false; // ova funkcija zahtijeva pocetnu zakrivljenost != 0

        //__________________________________________________________________________________________________________________
        // Ispitivanje nuznih uvjeta

        // Ovo nisu (barem ne svi) uvjeti postojanja rjesenja vec ovime reduciramo skup mogucih rjesenja.
        // U nekim slucajevima rjesenje moze postojati cak i ako ovi uvjeti nisu ispunjeni, ali takva rjesenja ne razmatramo.
        // Jedan od razloga je i numericka nestabilnost koja bi se mogla pojaviti u takvim slucajevima.

        // Tangenta na putanju kroz tocku P0
        CLinePar P0Tangent;
        P0Tangent.LineFromDirectionAndPoint(P0.ang, P0.x, P0.y);

        // 1. nuzan uvjet: Ako se pocetno gibamo ulijevo, onda i cilj mora biti s lijeve strane, i obrnuto.
        // Uzimamo da G1 ili G2 moraju biti lijevo
        if(P0Tangent.GetDistanceToPoint(G1.x, G1.y) * P0.k <= 0 && P0Tangent.GetDistanceToPoint(G2.x, G2.y) * P0.k <= 0)
            return false; // Cilj je sa krive strane

        // 2. nuzan uvjet: Ako skrecemo ulijevo, onda pocetna tocka mora biti lijevo od vektora G1G2, i obrnuto

        // Pravac G1-G2
        CLinePar G1G2;
        if(!G1G2.LineFromPoints(G1.x, G1.y, G2.x, G2.y))
            return false;

        if(G1G2.GetDistanceToPoint(P0.x, P0.y) * P0.k <= 0)
            return false; // Nedozvoljen smjer

        // 3. nuzan uvjet: TUDU: Zakret mora biti 0<|zakret|2pi

        //__________________________________________________________________________________________________________________
        // Ogradivanje rjesenja (tj. trazenje intervala [C1 C2] u kojem je rjesenje)

        // Odredivanje donje ograde faktora skaliranja (C1)

        // Strategija: Donju granicu nademo tak da izracunamo faktor C uz pretpostavku da je k0=0 (poc. zakrivljenost nula - to zasad radimo
        // kao da je pozitivni zakret)

        P0Tangent.SetOppositeDirection(); // Tangenta kroz tocku P0 (al ovaj put u suprotnom smjeru od P0.ang)
        CLinePar Bisection;
        Bisection.SymmetryAxis(P0Tangent, G1G2);

        // T s1 = sqrt(2*NormAngle_Zero_2PI(Bisection.m_Direction-P0.ang)); - stari nacin (opcenito losiji, al nekad zna biti i bolji od
        // donjeg)
        T s1 = sqrt(2 * NormAngle_Zero_PI(Bisection.m_Direction - PI / 2 - P0.ang));
        T dx, dy;
        C2DClothoidApp::m_pLookupTable->GetBasicClothoidPoint(s1, dx, dy);

        //[dx dy] = RotatePoint(dx, dy, P0.th);
        T D2 = dx * dx + dy * dy;
        // Udaljenost pocetne tocke P0 do bisekcije
        T d_P0_Bisection = Bisection.GetDistanceToPoint(P0.x, P0.y);
        T d2_P0_Bisection = d_P0_Bisection * d_P0_Bisection;

        if(IsZero(D2))
            bResult = false; // Trazeni zakret je zanemariv - i moze se aproksimirati pravcem

        if(IsZero(d_P0_Bisection))
            bResult = false; // Nemoguce zakrenuti na tako maloj udaljenosti - bisekcija je preblizu poc tocki

        // Lijevi bracket koji se ovako dobije je u pravilu premali, i trebalo bi biti fMin(C1)<0
        T C1 = sqrt(d2_P0_Bisection / D2);

        // Odredivanje gornje ograde (C2) - dobije se ogranicenjem ukupnog zakreta na manje od 2pi
        // Ukupni zakret se raspodijeljuje na ulaznu i izlaznu klotoidu.
        T C2;

        T dang = G1G2.m_Direction - P0.ang; // Ukupni zakret
        if(P0.k > 0)
        {
            // Pozitivni zakret - to znaci da i zahtjevani zakret mora biti pozitivan
            dang = NormAngle_Zero_2PI(dang);
            T dang_P1P0_max = std::min(2 * PI - dang, PI); // Ukupni zakret dijela od P1 do P0
            C2 = sqrt(2 * dang_P1P0_max) / P0.k;
        }
        else
        {
            dang = NormAngle_Minus2PI_Zero(dang);
            T dang_P1P0_max = std::max(-2 * PI - dang, -PI);
            C2 = sqrt(-2 * dang_P1P0_max) / (-P0.k);
        }

        if(C1 > C2)          // Ovo se desilo kod ekstremno brzih zakreta
            bResult = false; // Ne mozemo bracketirati rjesenje

        if(P0.k < 0)
        {
            C1 = -C1;
            C2 = -C2;
        }

        //__________________________________________________________________________________________________________________
        // Trazenje rjesenja (iterativnom metodom)

        bool bFoundSolution = false;

        // Ulazno-izlazni podaci za poziv funkcije  ConnectCircleLine2C_fZero ciju nul tocku trazimo

        SIO_ConnectCircleLine2C_fZero IO(P0, G1G2);

        IO.C = C1;
        T f1 = ConnectCircleLine2C_fZero(IO);

        // Odmah na pocetku moze se desiti da je rjesenje C1 nedopustivo,
        // Kad bi isli povecavat C, rjesenje bi i dalje ostalo nedopustivo - znaci vjerojatno nema rjesenja, ali ipak pokusajmo cak iako je
        // to slucaj :)

        IO.C = C2;
        T f2 = ConnectCircleLine2C_fZero(IO);

        T C; // trenutno rjesenje

        if(IsZero(f1))
        {
            C = C1;
            bFoundSolution = true;
        }
        else if(IsZero(f2))
        {
            C = C2;
            bFoundSolution = true;
        }
        else if(f1 * f2 > 0)
        {
            // velika je vjerojatnost da rjesenje ne postoji (nije uspjelo ogradivanje)
            return false;
        }
        else
        {
            // Rijesi metodom bisekcije.

            T f = f1;
            T dC;

            if(f < 0)
            {
                dC = C2 - C1;
                C = C1;
            }
            else
            {
                dC = C1 - C2;
                C = C2;
            }

            for(int i = 1; i <= 30; i++)
            {
                dC = (T)0.5 * dC;
                T Cmid = C + dC;
                IO.C = Cmid;
                T fmid = ConnectCircleLine2C_fZero(IO);

                if(fmid < 0)
                    C = Cmid;

                if(abs(dC) < (T)0.0001 || IsZero(fmid))
                {
                    // Found solution in i iteration(s)
                    bFoundSolution = true;
                    break;
                }
            }

            // U matlab verziji funkcije isprobana je jos false position metoda, ali izgleda da je ova ipak bolja u vecini slucajeva
        }

        if(!bFoundSolution)
        {
            return false; // premasen max. broj iteracija, a nije pronadeno rjesenje
        }
        else
        {
            // Izracunaj parametre klotoida
            // Moramo ponoviti proracun, jer zadnji poziv funkcije fMin mozda nije onaj koji odgovara rjesenju (tj. odbacen je)
            IO.C = C;
            ConnectCircleLine2C_fZero(IO);

            if(!IO.bAdmissibleSolution)
                return false; // pronadeno rjesenje nije dopustivo

            Clothoid1.Set2(P0.x, P0.y, P0.ang, P0.k, C, IO.FirstClothLength);

            T xMid, yMid, angMid, kMid;
            Clothoid1.GetPoint(IO.FirstClothLength, xMid, yMid, angMid, kMid);

            Clothoid2.Set2(xMid, yMid, angMid, kMid, -C, IO.SecondClothLength);

            // CL2.th0 = CL1.th0 + CL1.k0*CL1.length + 0.5*CL1.c*CL1.length*CL1.length;
            // CL2.k0 = CL1.k0 + CL1.c*CL1.length;

            // Nije jos gotovo - Provedi jos dodatni test dopustivosti pronadenog rjesenja - druga klotoida se mora spustiti na pravac G1G2
            // prije tocke G2
            CLineImp G1G2_imp;
            G1G2.GetLineImp(G1G2_imp); // pretvaramo u imp. oblik jer je malo efikasniji za ovaj problem
            CLineImp G1G2_perp;
            G1G2_perp.GetPerpendicularLineThroughPoint(G1G2_imp, G2.x, G2.y);
            T xPathEnd, yPathEnd;
            T ang, k;
            Clothoid2.GetPoint(Clothoid2.m_Length, xPathEnd, yPathEnd, ang, k);
            if(!G1G2_perp.IsPointLeft(xPathEnd, yPathEnd))
                return false; // solution is not before G2

            // Zavrsni pravac
            T LineLength = G2.GetDistanceToPoint(xPathEnd, yPathEnd);
            if(IsPositive(LineLength))
            {
                (CLinePar&)Line = G1G2; // sve isto kao i linija G1G2, osim pocetne tocke, i jos treba dodati duljinu linije
                Line.p0.x = xPathEnd;
                Line.p0.y = yPathEnd;
                Line.m_Length = LineLength;
            }
            else
                Line.m_Length = 0;
        }

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Funkcija kojoj trazimo nul-tocku. Nul-tocka te funkcije rezultira rjesenjem problema spajanja kruznice i pravca dvo-klotoidom.
    //________________________________________________________________________________________________________________________________________________
    static T ConnectCircleLine2C_fZero(SIO_ConnectCircleLine2C_fZero& IO)
    {
        assert(IO.C != 0);

        const S2DPathState& P0 = IO.P0;
        const CLinePar& G1G2 = IO.G1G2;

        IO.bAdmissibleSolution = true; // Dal je trenutno rjesenje dopustivo

        // 1. korak
        // Izracunaj tocku P1 - to je pocetna tocka klotoide provucene kroz tocku P0 i u njoj je zakrivljenost =0

        C2DClothoidApp CL;
        CL.Set2(0, 0, 0, 0, IO.C, 0);

        T sp0 = P0.k / CL.m_c; // parametar s u tocki P0
        T dx, dy;              // Promjena x i y od tocke P1 do tocke P0
        T ang, k;
        if(!CL.GetPoint(sp0, dx, dy, ang, k))
            assert(0);
        T dang = (T)0.5 * CL.m_c * sp0 * sp0;

        // Pomak dx i dy treba rotirati tako da pase orijentacija u tocki P0
        T angRot = -dang + P0.ang;
        C2DRotationMatrix<T> R(angRot);

        R.RotatePoint(dx, dy);

        S2DPathState P1;
        P1.x = P0.x - dx;
        P1.y = P0.y - dy;
        P1.ang = P0.ang - dang;

        // 2. korak
        // Izracunaj pravac bisekcije ili polovista zakreta - to je os simetrije izmedu tangente u tocki P1 i pravca G1G2

        // Tangenta kroz P1, ali suprotnog smjera od kuta P1.ang - to je radi racunanja osi simetrije
        CLinePar P1TangentOpp;
        P1TangentOpp.LineFromDirectionAndPoint(P1.ang + PI, P1.x, P1.y);

        T Output;
        T SecondClothLength = 0;

        if(P1TangentOpp.IsParallel(G1G2))
        {
            // Ak su pravci bili paralelni, bisekcija se ne moze pouzdano proracunati jer moze biti u beskonacnosti
            T dG1G2_P1 = G1G2.GetDistanceToPoint(P1.x, P1.y);
            if(IsZero(dG1G2_P1))
            {
                Output = 0; // Ovo nam je prakticki rjesenje
                            // Bisection = LP_LineFromDirectionAndPoint(G1G2.m_Direction+pi/2, P0.x, P0.y);
            }
            else if(dG1G2_P1 * P0.k < 0)
            {
                assert(std::numeric_limits<T>::has_infinity); // uvjerimo se da tip ima infinity
                Output = std::numeric_limits<T>::infinity();  // +beskonacno
            }
            else
            {
                assert(std::numeric_limits<T>::has_infinity); // uvjerimo se da tip ima infinity
                Output = -std::numeric_limits<T>::infinity(); // -beskonacno
            }
        }
        else
        {
            CLinePar Bisection;
            Bisection.SymmetryAxis(P1TangentOpp, G1G2);
            T angBisection = Bisection.m_Direction;

            // 3. korak
            // Nadi mjesto na klotoidi koja ide iz tocke P1 koje je okomito na pravac bisekcije i odredi njegovu udalj. od pravca bisekcije

            // CL.x0 = P0.x;
            // CL.y0 = P0.y;
            // CL.th0 = P0.th;
            // CL.k0 = P0.k;
            // CL.length = abs(C)*sqrt(2*pi) - sp0; % Kut poluzakreta moze biti max pi
            // if(CL.length<0); error('Klotoida vec u startu ima zakret >pi'); end

            T CL_Length = abs(IO.C) * sqrt((2 * PI - eps)); // Kut poluzakreta moze biti max pi
            CL.Set2(P1.x, P1.y, P1.ang, 0, IO.C, CL_Length);

            T s1, s2;

            CL.GetDistanceAtOrientation(angBisection - PI / 2, s1);
            CL.GetDistanceAtOrientation(angBisection + PI / 2, s2);
            T s = std::min(s1, s2);
            SecondClothLength = s;

            bool sFound = false;
            if(IsLessOrEqual(s, CL.GetLength()))
                sFound = true;

            if(sFound)
            {
                T xLateral, yLateral;
                T ang, k;
                CL.GetPoint(s, xLateral, yLateral, ang, k);
                T distToBisection = Bisection.GetDistanceToPoint(xLateral, yLateral);

                if(IO.C > 0)
                    Output = -distToBisection;
                else
                    Output = distToBisection;

                if(s < sp0)
                {
                    // Mjesto gdje je klotoida okomita na bisekciju je prije tocke P0.
                    // ovakvo rjesenje je mozda matematicki tocno, al je nedopustivo.
                    IO.bAdmissibleSolution = false;
                }

                // Za probu - ovo utjece jako lose na metodu false position jer se funkcija izgladuje kod ishodista
                // Output = Sign(Output)*Output*Output;
            }
            else
            {
                // Nema tocke okomite na bisekciju!
                // Kaj napraviti ak nema - vjerojatno se tocka koja je okomita na bisekciju nalazi na klotoidi na mjestu prije
                // pocetne tocke, tj. izmedu P1 i P0, sto nam pokazuje da je ovo rjesenje nedopustivo.

                assert(std::numeric_limits<T>::has_infinity); // uvjerimo se da tip ima infinity
                Output = -std::numeric_limits<T>::infinity(); // -beskonacno

                IO.bAdmissibleSolution = false;
                SecondClothLength = 0;
            }
        }

        // Korisne informacije za racunanje putanje
        IO.FirstClothLength = SecondClothLength - sp0;
        IO.SecondClothLength = SecondClothLength;

        return Output;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Funkcija za spajanje kruznice i pravca pomocu triju klotoida.
    // Svrha funkcije je izgladivanje putanje uz  ne-nula pocetnu zakrivljenost.
    // Ova funkcija sadrzi samo low level dio algoritma (onaj koji je moguce odraditi bez da se poznaje dinamika robota, prepreke,
    // planiranje nove putanje itd). High level dio algoritma potrebno je implementirati izvana. Cijela stvar radi tako da se kruznica
    // izravna pomocu klotoide sa odabranim skaliranjem. Na kraju klotoide se povuce pravac i gleda se dokle se moze povuci taj pravac bez
    // da se udari u prepreku (naravno do neke granice). Ta krajnja tocka pravca se ubaci kao novi cvor u putanju i rekonstruira se putanja
    // (koja je ravna po dijelovima). Zatim se ta po dijelovima ravna putanja jednostavno izgladi (SmoothSharpTurn algoritam). Algoritam se
    // moze probati sa vise razlicitih skaliranja, a na kraju se moze izabrati najbolji rezultat po nekom kriteriju. Low level dio algoritma
    // samo radi konstrukciju izravnavajuce klotoide i pravca.
    //________________________________________________________________________________________________________________________________________________
    static bool
    ConnectCircleLine3C_LowLevel(const S2DPathState& P0, // pocetno stanje koje se sastoji od pocetne pozicije, kuta i zakrivljenosti
                                 T C, // zadani faktor skaliranja izravnavajuce klotoide (ona koja kruznu putanju pretvara u ravnu)
                                 C2DClothoidApp& Clothoid, // izravnavajuca klotoida
                                 CLinePar& Line // tangenta na izravnavajucu klotoidu koja pocinje u tocki di je zakrivljenost jednaka nuli
    )
    {
        if(IsZero(P0.k) || IsZero(C))
            return false; // ova funkcija zahtijeva pocetnu zakrivljenost != 0

        if(P0.k > 0)
            C = -C;

        Clothoid.Set2(P0.x, P0.y, P0.ang, P0.k, C, 0);

        T s = CNum<T>::Abs(P0.k / Clothoid.m_c); // duljina na kojoj je zakrivljenost nula
        Clothoid.Length(s);
        T x, y;
        T ang, k;
        Clothoid.GetPoint(s, x, y, ang, k);

        Line.LineFromDirectionAndPoint(ang, x, y);

        return true;
    }
};

}; // namespace Geom

#endif
