#ifndef GEOMETRY_BASE_H
#define GEOMETRY_BASE_H

#include <assert.h>
#include <cmath>
#include <limits>

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

namespace Geom
{
//=================================================================================================================================================================
//
// Bazna (template) klasa za geometrijske algoritme.
// Za realizaciju templatea smisla imaju jedino realni tipovi (double, float).
// Ak trebamo preciznost, odabiremo double, a ak preciznost nije tolko vazna, a vazna je brzina, onda float.
//
//=================================================================================================================================================================
class CGeometryBase
{
public:
    typedef double CoordType;
    typedef double T;

    // Definicija tipova
    typedef CoordType NUM_GM; // Opceniti numericki tip
    typedef CoordType COORD_GM;
    typedef CoordType LNCOEFF_GM;
    typedef CoordType DIST_GM;
    typedef CoordType VECTORCOMP_GM;
    typedef CoordType RADIUS_GM;
    typedef CoordType LENGTH_GM;

    // Tipovi za kut
    typedef T ANGLE_GM;
    typedef T ANGLERAD_GM;

    // Definira stanje putanje - potrebno za neke algoritme
    struct S2DPathState
    {
        T x, y; // pozicija
        T ang;  // kut [rad]
        T k;    // zakrivljenost
    };

    // Deklaracija za PI
    static const T PI;

    static const NUM_GM eps;

    // Apsolutna vrijednost
    inline static NUM_GM abs(NUM_GM x) { return (x >= 0 ? x : -x); }

    // Signum funkcija
    static T Sign(T x)
    {
        if(x > 0)
            return (T)1;
        if(x < 0)
            return (T)-1;

        return 0;
    }

    // Jednakost koja uzima u obzir epsilon
    inline static bool IsEqual(NUM_GM x, NUM_GM y) { return (abs(x - y) < eps); }

    // Jednakost sa nulom koja uzima u obzir epsilon
    inline static bool IsZero(NUM_GM x) { return (abs(x) < eps); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Ispitivanje ">0" uvjeta (priblizno). Broj mora biti veci od eps da bi se smatrao "jako" pozitivnim.
    //________________________________________________________________________________________________________________________________________________
    inline static bool IsPositive(T x) { return (x > eps); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Ispitivanje "<0" uvjeta (priblizno). Broj mora biti manji od -eps da bi se smatrao "jako" negativnim.
    //________________________________________________________________________________________________________________________________________________
    inline static bool IsNegative(T x) { return (x < (-eps)); }

    // priblizna relacija <= koja uzima u obzir epsilon
    inline static bool IsLessOrEqual(T x, T y) { return (x < y || abs(x - y) < eps); }

    // Funkcije za normiranje kuteva na zeljeni interval
    //__________________________________________________

    // Svodenje kuta na [0,pi/2>
    // Paznja: kod jako malih negativnih kuteva moze dati i pi/2 radi num. nepreciznosti kod oduzimanja
    static inline ANGLERAD_GM NormAngle_Zero_PI2(ANGLERAD_GM Angle)
    {
        const ANGLERAD_GM PI_2 = PI / 2;
        Angle -= (ANGLERAD_GM)floor(Angle / PI_2) * PI_2;
        return Angle;
    }

    // Svodenje kuta na [0,pi>
    // Paznja: kod jako malih negativnih kuteva moze dati i pi radi num. nepreciznosti kod oduzimanja
    static inline ANGLERAD_GM NormAngle_Zero_PI(ANGLERAD_GM Angle)
    {
        Angle -= (ANGLERAD_GM)floor(Angle / PI) * PI;
        return Angle;
    }

    // Svodenje kuta na [0,2pi>
    // Paznja: kod jako malih negativnih kuteva moze dati i 2pi radi num. nepreciznosti kod oduzimanja
    static inline ANGLERAD_GM NormAngle_Zero_2PI(ANGLERAD_GM Angle)
    {
        const ANGLERAD_GM PI2 = 2 * PI;
        Angle -= (ANGLERAD_GM)floor(Angle / PI2) * PI2;
        return Angle;
    }

    // Svodenje kuta na [-pi,pi>
    static inline ANGLERAD_GM NormAngle_MinusPI_PI(ANGLERAD_GM Angle)
    {
        const ANGLERAD_GM PI2 = 2 * PI;
        Angle -= (ANGLERAD_GM)floor(Angle / PI2) * PI2; // Svodenje na [0,2pi>
        if(Angle >= PI)                                 // na [-pi,pi>
            Angle -= PI2;
        return Angle;
    }

    // Svodenje kuta na [-pi/2,pi/2>
    static inline ANGLERAD_GM NormAngle_MinusPI2_PI2(ANGLERAD_GM Angle)
    {
        Angle -= (ANGLERAD_GM)floor(Angle / PI) * PI; // Svodenje na [0,pi>
        if(Angle >= PI / 2)                           // na [-pi/2,pi/2>
            Angle -= PI;
        return Angle;
    }

    // Svodenje kuta na [-pi/4,pi/4>. Efekt je isti ko kad bi kutu dodavali ili oduzimali pi/2 tak dugo dok ne upadne u zeljeni interval.
    static inline ANGLERAD_GM NormAngle_MinusPIQuarter_PIQuarter(ANGLERAD_GM Angle) // Jebo te ime funkcije :)
    {
        const ANGLERAD_GM PI_2 = PI / 2;
        Angle -= (ANGLERAD_GM)floor(Angle / PI_2) * PI_2; // Svodenje na [0,pi/2> (radi i za neg. kutove)
        if(Angle >= PI / 4)                               // na [-pi/4,pi/4>
            Angle -= PI_2;
        return Angle;
    }

    // Svodenje kuta na <-2pi,0]
    static inline ANGLERAD_GM NormAngle_Minus2PI_Zero(ANGLERAD_GM Angle)
    {
        const ANGLERAD_GM PI2 = 2 * PI;
        Angle -= (ANGLERAD_GM)floor(Angle / PI2) * PI2; // Svodenje na [0,2pi> (radi i za neg. kutove)
        if(Angle > 0)
            Angle -= PI2; // na <-2pi,0]
        return Angle;
    }

    // Svodenje kuta na [NormAng,NormAng+2pi>
    static inline ANGLERAD_GM NormAngle_Ang_2PI(ANGLERAD_GM Angle, ANGLERAD_GM NormAng)
    {
        return NormAngle_Zero_2PI(Angle - NormAng) + NormAng;
    }

    // Svodenje kuta na <NormAng-2pi,NormAng]
    static inline ANGLERAD_GM NormAngle_Minus2PI_Ang(ANGLERAD_GM Angle, ANGLERAD_GM NormAng)
    {
        return NormAngle_Minus2PI_Zero(Angle - NormAng) + NormAng;
    }

    // Svodenje kuta na [NormAng-pi,NormAng+pi>
    static inline ANGLERAD_GM NormAngle_MinusPI_Ang(ANGLERAD_GM Angle, ANGLERAD_GM NormAng)
    {
        return NormAngle_MinusPI_PI(Angle - NormAng) + NormAng;
    }

    // Svodenje kuta na [NormAng-pi/4,NormAng+pi/4>
    static inline ANGLERAD_GM NormAngle_MinusPIQv_Ang(ANGLERAD_GM Angle, ANGLERAD_GM NormAng)
    {
        return NormAngle_MinusPIQuarter_PIQuarter(Angle - NormAng) + NormAng;
    }

    // Funkcije za racunanje chain codea kuteva
    //_________________________________________

    // Racuna chain code kuta u rasponu 0-7
    // [-pi/8, pi/8> -> 0
    // [pi/8, 3pi/8> -> 1
    // ...
    static inline unsigned char Angle2ChainCode(ANGLERAD_GM Angle)
    {
        return ((unsigned char)((NormAngle_Zero_2PI(Angle + PI / 8)) / (PI / 4)) &
                7); // Radi num. nepreciznosti bez &7 bi se dobio chain kod 8 za kut 22.5°
    }

    // Racuna chain code kuta u rasponu 0-3
    // [-pi/4, pi/4> -> 0
    // [pi/4, 3pi/4> -> 1
    // ...
    static inline unsigned char Angle2ChainCode4(ANGLERAD_GM Angle)
    {
        return ((unsigned char)((NormAngle_Zero_2PI(Angle + PI / 4)) / (PI / 2)) & 3);
    }
    //_________________________________________

    // Virtualna funkcija za samoprovjeru objekta.
    // Ovdje ne radi nista, a u izvedenim klasama u ovoj funkciji treba napraviti provjere (da li su parametri unutar granica i sl.) i
    // vratiti false u slucaju da provjera zaj.
    virtual bool IsValid() const { return true; }
};

}; // namespace Geom

#endif // GEOMETRY_BASE_H

// And I will stand for my dream if I can,
// Symbol of my faith in who I am ...
