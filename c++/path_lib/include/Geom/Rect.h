#pragma once

#include "2DPoint.h"
#include "GeometryBase.h"
#include "Line.h"

#include <cmath>

// using namespace Geom;
namespace Geom
{
template<class CoordType = float, class T = CoordType>
class CRectangle : public CGeometryBase
{
public:
    C2DPoint p1, p2;

    void SetRect(C2DPoint& p1_, C2DPoint p2_)
    {
        p1 = p1_;
        p2 = p2_;
    }

    void SortPoints()
    {
        // Presleguje tocke tako da je p1.x<p2.x i p1.y<p2.y
        if(p1.x > p2.x)
        {
            COORD_GM tmp = p1.x;
            p1.x = p2.x;
            p2.x = tmp;
        }

        if(p1.y > p2.y)
        {
            COORD_GM tmp = p1.y;
            p1.y = p2.y;
            p2.y = tmp;
        }
    }

    // Provjerava dal je tocka unutar ili na rubu recta
    bool IsPointInRect(COORD_GM x, COORD_GM y)
    {
        SortPoints();
        if(p1.x <= x && x <= p2.x && p1.y <= y && y <= p2.y)
            return true;
        return false;
    }

    bool IsPointInRect(C2DPoint& pt) { return IsPointInRect(pt.x, pt.y); }

    void InflateRect(DIST_GM dx, DIST_GM dy)
    {
        SortPoints();
        p1.x -= dx;
        p2.x += dx;
        p1.y -= dy;
        p2.y += dy;
    }
};

}; // namespace Geom
