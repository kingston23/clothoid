#ifndef _GEOM_2DOBJECT_H_
#define _GEOM_2DOBJECT_H_

#include "2DRectangle.h"
#include "2DSquare.h"

#include <algorithm>
#include <cstring>
#include <cmath>


#ifdef min
#undef min
#endif

namespace Geom
{
//================================================================================================================================================================
// Bazna klasa za 2D geometrijske objekte (pravci, krugovi, kvadrati itd...).
// Klasa definira funkcije za udaljenost objekata, presjecista itd...
//================================================================================================================================================================
class C2DObject : public CGeometryBase
{
public:
    int ID;

public:
    // Daje sjecista ruba objekta sa kruznim lukom.
    // pPoints je niz u koji se spremaju sjecista, nPointsMax je koliko tocaka sjecista mozemo spremiti u niz.
    // Vraca broj sjecista (koji moze biti i veci od nPointsMax)
    virtual int GetIntersectionsWithArc(CCircleArc* pArc, C2DPoint* pPoints, int nPointsMax) const { return 0; }

    // Nalazi najmanju udaljenost izmedu objekta i duzine, te tocku na objektu i tocku na duzini koje odgovaraju toj min. udaljenosti.
    // Ukoliko postoji presjeciste, vraca udalj 0. Ako je vise presjecista, vraca ono koje je najblize prvoj tocki duzine.
    virtual DIST_GM GetDistanceToLine(CBoundLineImp* pLine, C2DPoint* pObjectPoint, C2DPoint* pLinePoint) const { return 0; }

    virtual DIST_GM GetDistanceToArc(CCircleArc* pArc, C2DPoint* pObjectPoint, C2DPoint* pArcPoint) const { return 0; }
};

//================================================================================================================================================================
// Klasa za 2D kvadrat
//================================================================================================================================================================
class C2DObject_Square : public C2DObject
{
public:
    virtual int GetIntersectionsWithArc(CCircleArc* pArc, C2DPoint* pPoints, int nPointsMax) const
    {
        if(nPointsMax < 8)
        {
            C2DPoint pPointsTemp[8];

            int nPoints = m_Sq.GetIntersectionWithArc(pArc, pPointsTemp);

            memcpy(pPoints, pPointsTemp, std::min(nPoints, nPointsMax) * sizeof(C2DPoint));

            return nPoints;
        }
        else
            return m_Sq.GetIntersectionWithArc(pArc, pPoints);
    }

    DIST_GM GetDistanceToLine(CBoundLineImp* pLine, C2DPoint* pObjectPoint, C2DPoint* pLinePoint) const
    {
        return m_Sq.GetDistanceToLine(*pLine, pObjectPoint, pLinePoint);
    }

    void Set(COORD_GM x, COORD_GM y, ANGLERAD_GM ang, LENGTH_GM a) { m_Sq.ComputeSquare(x, y, ang, a); }

public:
    C2DSquare m_Sq;
};

//================================================================================================================================================================
// Klasa za 2D pravokutnik
//================================================================================================================================================================
class C2DObject_Rectangle : public C2DObject
{
public:
    virtual int GetIntersectionsWithArc(CCircleArc* pArc, C2DPoint* pPoints, int nPointsMax) const
    {
        if(nPointsMax < 8)
        {
            C2DPoint pPointsTemp[8];

            int nPoints = m_Rect.GetIntersectionWithArc(pArc, pPointsTemp);

            memcpy(pPoints, pPointsTemp, std::min(nPoints, nPointsMax) * sizeof(C2DPoint));

            return nPoints;
        }
        else
            return m_Rect.GetIntersectionWithArc(pArc, pPoints);
    }

    DIST_GM GetDistanceToLine(CBoundLineImp* pLine, C2DPoint* pObjectPoint, C2DPoint* pLinePoint) const { return 0; }

    void Set(COORD_GM x, COORD_GM y, ANGLERAD_GM ang, LENGTH_GM a, LENGTH_GM b) { m_Rect.ComputeRect(x, y, ang, a, b); }

public:
    C2DRectangle m_Rect;
};

//================================================================================================================================================================
// Klasa za 2D duzinu
//================================================================================================================================================================
class C2DObject_BoundLine : public C2DObject
{
public:
    void Set(COORD_GM x1, COORD_GM y1, COORD_GM x2, COORD_GM y2) { m_Line.LineFromPoints(x1, y1, x2, y2); }

    DIST_GM GetDistanceToArc(CCircleArc* pArc, C2DPoint* pObjectPoint, C2DPoint* pArcPoint) const
    {
        return pArc->GetDistanceToLine(m_Line, pArcPoint, pObjectPoint);
    }

    DIST_GM GetDistanceToLine(CBoundLineImp* pLine, C2DPoint* pObjectPoint, C2DPoint* pLinePoint) const
    {
        DIST_GM Dist = m_Line.GetDistance2ToLine(*pLine, pObjectPoint, pLinePoint);
        Dist = sqrt(Dist);
        return Dist;
    }

public:
    CBoundLineImp m_Line;
};

//================================================================================================================================================================
// Klasa za 2D kruznicu
//================================================================================================================================================================
class C2DObject_Circle : public C2DObject
{
public:
    void Set(COORD_GM xc, COORD_GM yc, RADIUS_GM r) { m_Circle.SetCircle(xc, yc, r); }

    DIST_GM GetDistanceToLine(CBoundLineImp* pLine, C2DPoint* pObjectPoint, C2DPoint* pLinePoint) const
    {
        return m_Circle.GetDistanceToLine(*pLine, pObjectPoint, pLinePoint);
    }

    DIST_GM GetDistanceToArc(CCircleArc* pArc, C2DPoint* pObjectPoint, C2DPoint* pArcPoint) const
    {
        return pArc->GetDistanceToCircle(m_Circle, pArcPoint, pObjectPoint);
    }

public:
    CCircle m_Circle;
};

}; // namespace Geom

#endif
