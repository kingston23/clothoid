#ifndef _3DRotation_H
#define _3DRotation_H

#include <assert.h>
#include <cmath>

namespace Geom
{
//____________________________________________________________________________________________________________________________________________
//
// Racunanje rotacijske matrice iz osi rotacije i kuta rotacije oko te osi.
// Algoritam daje rotacijsku 3x4 matricu. To je varijanta rot. matrice dopunjena nul stupcem sdesna (treba za neke algoritme).
// TUDU: Ovo je jako cudna varijanta rot. matrice, treba li nam stvarno bas takva, pogotovo znajuci da ju treba preslagivati kad se koristi
// u DrawStuff.
//____________________________________________________________________________________________________________________________________________
template<typename T>
static void RotationMatrixFromAxisAndAngle(T* RotMatrix,     // Rotacijska 3x4 matrica. Podaci su poslozeni po recima.
                                           T ax, T ay, T az, // Os rotacije
                                           T angle           // Kut rotacije oko zadane osi [rad]
)
{
    T ax2 = ax * ax;
    T ay2 = ay * ay;
    T az2 = az * az;

    T I2 = ax2 + ay2 + az2;

    T S_2 = sin(angle / 2);
    T K1 = 2 * S_2 * S_2 / I2;
    T K2 = sin(angle) / I2;

    // 1. redak
    RotMatrix[0] = 1 - (K1 * (ay2 + az2));
    RotMatrix[1] = K1 * ax * ay - K2 * az;
    RotMatrix[2] = K1 * ax * az + K2 * ay;
    RotMatrix[3] = (T)(0.0);

    // 2. redak
    RotMatrix[4] = K1 * ax * ay + K2 * az;
    ;
    RotMatrix[5] = 1 - (K1 * (ax2 + az2));
    RotMatrix[6] = K1 * ay * az - K2 * ax;
    RotMatrix[7] = (T)(0.0);

    // 3. redak
    RotMatrix[8] = K1 * ax * az - K2 * ay;
    RotMatrix[9] = K1 * ay * az + K2 * ax;
    RotMatrix[10] = 1 - (K1 * (ax2 + ay2));
    RotMatrix[11] = (T)(0.0);

    // Kod za verifikaciju (uzet iz ODE biblioteke iz funkcije dRFromAxisAndAngle, dQFromAxisAndAngle i dRfromQ)

#define VERIFY_RotationMatrixFromAxisAndAngle
#ifdef VERIFY_RotationMatrixFromAxisAndAngle
    {
        // dRfromQ(), dQfromR() and dDQfromW() are derived from equations in "An Introduction
        // to Physically Based Modeling: Rigid Body Simulation - 1: Unconstrained
        // Rigid Body Dynamics" by David Baraff, Robotics Institute, Carnegie Mellon
        // University, 1997.

        T q[4];
        T l = ax * ax + ay * ay + az * az;
        if(l > T(0.0))
        {
            angle *= T(0.5);
            q[0] = cos(angle);
            l = sin(angle) / sqrt(l);
            q[1] = ax * l;
            q[2] = ay * l;
            q[3] = az * l;
        }
        else
        {
            q[0] = 1;
            q[1] = 0;
            q[2] = 0;
            q[3] = 0;
        }

        // q = (s,vx,vy,vz)
        T qq1 = 2 * q[1] * q[1];
        T qq2 = 2 * q[2] * q[2];
        T qq3 = 2 * q[3] * q[3];
        T _R00 = 1 - qq2 - qq3;
        T _R01 = 2 * (q[1] * q[2] - q[0] * q[3]);
        T _R02 = 2 * (q[1] * q[3] + q[0] * q[2]);
        T _R03 = T(0.0);
        T _R10 = 2 * (q[1] * q[2] + q[0] * q[3]);
        T _R11 = 1 - qq1 - qq3;
        T _R12 = 2 * (q[2] * q[3] - q[0] * q[1]);
        T _R13 = T(0.0);
        T _R20 = 2 * (q[1] * q[3] - q[0] * q[2]);
        T _R21 = 2 * (q[2] * q[3] + q[0] * q[1]);
        T _R22 = 1 - qq1 - qq2;
        T _R23 = T(0.0);
    }
#endif
}

}; // namespace Geom
#endif