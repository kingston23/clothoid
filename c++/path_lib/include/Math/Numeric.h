#ifndef _NUMERIC_H_
#define _NUMERIC_H_

#include <assert.h>
#include <limits>

namespace MathMB
{
//========================================================================================================================================
//
// Template klasa sa raznim vise ili manje korisnim numerickim algoritmima.
//
//========================================================================================================================================
template<class T>
class CNum
{
public:
    static const T PI;

    static const T eps; // Tolerancija za IsNear() i neke druge funkcije [>=0]

    //_____________________________________________________________________________________________
    //
    // Postavlja toleranciju za neke funkcije (IsNear())
    // Tolerancija mora biti >= 0
    //_____________________________________________________________________________________________
    inline static void SetTolerance(T val)
    {
        assert(val >= 0);
        eps = val;
    }

    //_____________________________________________________________________________________________
    //
    // Provjera priblizne jednakosti dva broja.
    //_____________________________________________________________________________________________
    inline static bool IsNear(T x, T y)
    {
        assert(eps >= 0);
        return (Abs(x - y) <= eps);
    }

    //_____________________________________________________________________________________________
    //
    // Jednakost sa nulom koja uzima u obzir epsilon
    //_____________________________________________________________________________________________
    inline static bool IsZero(T x) { return (Abs(x) < eps); }

    //_____________________________________________________________________________________________
    //
    // Ispitivanje ">0" uvjeta (priblizno). Broj mora biti veci od eps da bi se smatrao pozitivnim.
    //_____________________________________________________________________________________________
    inline static bool IsPositive(T x) { return (x > eps); }

    //_____________________________________________________________________________________________
    //
    // Zaokruzivanje
    //_____________________________________________________________________________________________
    static int Round(T x) { return (x >= 0 ? int(x + (T)0.5) : int(x - (T)0.5)); }

    //_____________________________________________________________________________________________
    //
    // Signum funkcija
    //_____________________________________________________________________________________________
    static T Sign(T x)
    {
        if(x > 0)
            return (T)1;
        if(x < 0)
            return (T)-1;

        return 0;
    }

    //_____________________________________________________________________________________________
    //
    // Ogranicavanje vrijednosti na interval [lower, upper]. To je zapravo funkcija zasicenja.
    //_____________________________________________________________________________________________
    static T Bound(T lower, T value, T upper)
    {
        assert(lower <= upper);
        if(value < lower)
            return lower;
        if(value > upper)
            return upper;
        return value;
    }

    //_____________________________________________________________________________________________
    //
    // Apsolutna vr.
    //_____________________________________________________________________________________________
    static T Abs(T x) { return (x >= 0 ? x : -x); }

    //_____________________________________________________________________________________________
    //
    // Gura varijablu x (koja moze biti poz i neg) prema nuli za pozitivnu vrijednost Increment
    //_____________________________________________________________________________________________
    static T PushToZero(T x, T Increment)
    {
        assert(Increment > 0);
        if(x > 0)
        {
            x -= Increment;
            if(x < 0)
                x = 0;
            return x;
        }
        else if(x < 0)
        {
            x += Increment;
            if(x > 0)
                x = 0;
            return x;
        }
        else
            return x;
    }

    //_____________________________________________________________________________________________
    //
    // Svodi broj iz intervala [xMin xMax] na interval [-1 1]
    //_____________________________________________________________________________________________
    static T Normalize_MinusOne_One(T x, T xMin, T xMax)
    {
        assert(xMin < xMax);

        return ((T)2 * (x - xMin) / (xMax - xMin) - (T)1);
    }
};

// Definicija za PI
template<class T>
const T CNum<T>::PI = (T)3.1415926535897932384626433832795;

// Defaultna vrijednost za eps
template<class T>
const T CNum<T>::eps = (T)std::numeric_limits<T>::epsilon() * 100;

}; // namespace MathMB

#endif // _NUMERIC_H_

// And Im never gonna crack
// cause its a good life
// Too good to lose
// But its a hard world
// With the junkie blues
