#ifndef CONSTANTS_H
#define CONSTANTS_H

//_________________________________________________________________________________________________________________________________________________________________________________
//
// Template struktura sa raznim konstantama.
// Za realizaciju templatea smisla imaju jedino realni tipovi (double, float, ?).
//_________________________________________________________________________________________________________________________________________________________________________________
template<class T>
struct Const
{
    typedef T Type;
    // PI - ovi
    //_________
    static const Type PI;
    static const Type PI_2; // pi/2
    static const Type PI_4; // pi/4
    static const Type PI_8; // pi/8
    static const Type _2PI; // 2pi
};

template<class Type>
const Type Const<Type>::PI = (Type)3.1415926535897932384626433832795;

template<class Type>
const Type Const<Type>::PI_2 = (Type)1.5707963267948966192313216916398;

template<class Type>
const Type Const<Type>::PI_4 = (Type)0.78539816339744830961566084581988;

template<class Type>
const Type Const<Type>::PI_8 = (Type)0.39269908169872415480783042290994;

template<class Type>
const Type Const<Type>::_2PI = (Type)6.283185307179586476925286766559;

#endif // CONSTANTS_H

// Primjer:
// float x = Const<float>::PI;
// float y = Const_f::PI_2;
