#ifndef _Functions_H_
#define _Functions_H_

#include <math.h>
#include <limits>

namespace MathMB
{
//========================================================================================================================================
//
// Funkcija pile (koja nema veze s picekima :)
// Tesko ju je rijecima opisati, ali da pokusamo:
// To je periodicka funkcija s vrijednostima koja je za x iz [0,T> definirana kao:
// x=0 -> y=0
// x=T/4 -> y=A
// x=T/2 -> y=0
// x=3T/4 -> y=-A
// x=T -> y=0
// Izmedu tih vrijednosti je linearno raste/pada
// Izvan intervala [0,T> se periodicki ponavlja.
// Moze se definirati da daje vrijednosti samo na zeljenom intervalu [x1,x2], a izvan toga bude nula.
// Moguce ju je i pomaknuti lijevo/desno tako da pocinje u x0.
// Nije testirana i vjerojatno ne radi za integer tipove.
//
//========================================================================================================================================
template<class Type = float>
class CSawFunction
{
public:
    CSawFunction()
    {
        x0 = (Type)0;
        T = (Type)1;
        A = (Type)1;

        x1 = -std::numeric_limits<Type>::max();
        x2 = std::numeric_limits<Type>::max();

        assert(!std::numeric_limits<Type>::is_integer); // ne smije biti cjelobrojni tip
    }

    Type Value(Type x)
    {
        if(x < x1 || x > x2)
            return (Type)0;

        assert(T > 0);
        Type x_ = (Type)4 * fmod((x - x0), T) / T;
        if(x_ < (Type)0)
            x_ += (Type)4; // ovo se desi za negativne brojeve

        if(x_ <= (Type)1)
            return x_ * A;
        else if(x_ <= (Type)3)
            return A * ((Type)2 - x_);
        else
            return A * ((Type)-4 + x_);
    }

public:
    Type x0; // pomak funkcije, ako je x0 pozitivan funkcija je pomaknuta udesno za x0
    Type T;  // Period - u kojem imamo jedan pozitivan i jedan negativan skok
    Type A;  // Amplituda, tj maks. vr. funkcije

    // Interval unutar kojeg je funkcija definirana, a izvan toga bit ce nula.
    // Defaultno je x1 postavljen na min vrijednost za odabrani tip, a x2 na max. vrijednost.
    Type x1, x2;
};

}; // namespace MathMB

#endif