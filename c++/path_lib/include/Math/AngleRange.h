#ifndef ANGLE_RANGE_H
#define ANGLE_RANGE_H

#include "Constants.h"

#include <cmath>

//========================================================================================================================================
// Template klasa za sredivanje kuteva, npr za svodenje kuteva na zeljene intervale.
// Za realizaciju templatea smisla imaju jedino realni tipovi (double, float, ?).
// Ak trebamo preciznost, odabiremo double, a ak preciznost nije tolko vazna, a vazna je brzina, onda float.
//========================================================================================================================================
class CAngleRange
{
public:
    typedef double T;
    typedef T ANGLE;

    //========================================================================================================================================
    // Svodenje kuteva na zadani interval
    //========================================================================================================================================

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na [0,pi/2>
    // Efekt je isti ko kad bi kutu dodavali ili oduzimali pi/2 tak dugo dok ne upadne u zeljeni interval.
    //_____________________________________________________________________________________________
    static inline ANGLE Range_Zero_PI2(ANGLE Angle)
    {
        Angle -= (ANGLE)floor(Angle / Const<T>::PI_2) * Const<T>::PI_2;
        if(Angle == Const<T>::PI_2)
            Angle = 0; // Kontrola radi num. nepreciznosti kod oduzimanja - inace kod jako malih negativnih kuteva moze dati i pi/2
        return Angle;
    }

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na [0,pi>
    // Efekt je isti ko kad bi kutu dodavali ili oduzimali pi tak dugo dok ne upadne u zeljeni interval.
    //_____________________________________________________________________________________________
    static inline ANGLE Range_Zero_PI(ANGLE Angle)
    {
        Angle -= (ANGLE)floor(Angle / Const<T>::PI) * Const<T>::PI;
        if(Angle == Const<T>::PI)
            Angle = 0; // Kontrola radi num. nepreciznosti kod oduzimanja - inace kod jako malih negativnih kuteva moze dati i pi
        return Angle;
    }

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na [0,2pi>
    // Efekt je isti ko kad bi kutu dodavali ili oduzimali 2pi tak dugo dok ne upadne u zeljeni interval.
    //_____________________________________________________________________________________________
    static inline ANGLE Range_Zero_2PI(ANGLE Angle)
    {
        Angle -= (ANGLE)floor(Angle / Const<T>::_2PI) * Const<T>::_2PI;
        if(Angle == Const<T>::_2PI)
            Angle = 0; // Kontrola radi num. nepreciznosti kod oduzimanja - inace kod jako malih negativnih kuteva moze dati i 2pi
        return Angle;
    }

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na [-pi/4,pi/4>
    // Efekt je isti ko kad bi kutu dodavali ili oduzimali pi/2 tak dugo dok ne upadne u zeljeni interval.
    //_____________________________________________________________________________________________
    static inline ANGLE Range_MinusPI4_PI4(ANGLE Angle) // Jebo te ime funkcije :)
    {
        Angle -= (ANGLE)floor(Angle / Const<T>::PI_2) * Const<T>::PI_2; // Svodenje na [0,pi/2> (radi i za neg. kutove)
        if(Angle >= Const<T>::PI / 4)                                   // na [-pi/4,pi/4>
            Angle -= Const<T>::PI_2;
        return Angle;
    }

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na [-pi/2,pi/2>
    // Efekt je isti ko kad bi kutu dodavali ili oduzimali pi tak dugo dok ne upadne u zeljeni interval.
    //_____________________________________________________________________________________________
    static inline ANGLE Range_MinusPI2_PI2(ANGLE Angle)
    {
        Angle -= (ANGLE)floor(Angle / Const<T>::PI) * Const<T>::PI; // Svodenje na [0,pi>
        if(Angle >= Const<T>::PI / 2)                               // na [-pi/2,pi/2>
            Angle -= Const<T>::PI;
        return Angle;
    }

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na [-pi,pi>
    // Efekt je isti ko kad bi kutu dodavali ili oduzimali 2pi tak dugo dok ne upadne u zeljeni interval.
    //_____________________________________________________________________________________________
    static inline ANGLE Range_MinusPI_PI(ANGLE Angle)
    {
        Angle -= (ANGLE)floor(Angle / Const<T>::_2PI) * Const<T>::_2PI; // Svodenje na [0,2pi>
        if(Angle >= Const<T>::PI)                                       // na [-pi,pi>
            Angle -= Const<T>::_2PI;
        return Angle;
    }

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na <-2pi,0]
    //_____________________________________________________________________________________________
    static inline ANGLE Range_Minus2PI_Zero(ANGLE Angle)
    {
        Angle -= (ANGLE)floor(Angle / Const<T>::_2PI) * Const<T>::_2PI; // Svodenje na [0,2pi> (radi i za neg. kutove)
        if(Angle > 0)
            Angle -= Const<T>::_2PI; // na <-2pi,0]
        return Angle;
    }

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na <-pi,0]
    //_____________________________________________________________________________________________
    static inline ANGLE Range_MinusPI_Zero(ANGLE Angle)
    {
        Angle -= (ANGLE)floor(Angle / Const<T>::PI) * Const<T>::PI; // Svodenje na [0,pi> (radi i za neg. kutove)
        if(Angle > 0)
            Angle -= Const<T>::PI; // na <pi,0]
        return Angle;
    }

    //========================================================================================================================================
    // Svodenje kuteva na zadani interval oko nekog drugog kuta
    //========================================================================================================================================

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na [NormAng,NormAng+2pi>
    //_____________________________________________________________________________________________
    static inline ANGLE Range_Ang_2PI(ANGLE Angle, ANGLE NormAng) { return Range_Zero_2PI(Angle - NormAng) + NormAng; }

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na <NormAng-2pi,NormAng]
    //_____________________________________________________________________________________________
    static inline ANGLE Range_Minus2PI_Ang(ANGLE Angle, ANGLE NormAng) { return Range_Minus2PI_Zero(Angle - NormAng) + NormAng; }

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na [NormAng-pi,NormAng+pi>
    //_____________________________________________________________________________________________
    static inline ANGLE Range_MinusPI_Ang_PI(ANGLE Angle, ANGLE NormAng) { return Range_MinusPI_PI(Angle - NormAng) + NormAng; }

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na [NormAng-pi/2, NormAng+pi/2>
    //_____________________________________________________________________________________________
    static inline ANGLE Range_MinusPI2_Ang_PI2(ANGLE Angle, ANGLE NormAng) { return Range_MinusPI2_PI2(Angle - NormAng) + NormAng; }

    //_____________________________________________________________________________________________
    //
    // Svodenje kuta na [NormAng-pi/4,NormAng+pi/4>
    //_____________________________________________________________________________________________
    /*static inline ANGLE Range_MinusPI4_Ang_PI4(ANGLE Angle, ANGLE NormAng)
    {
        return Range_MinusPIQuarter_PIQuarter(Angle-NormAng) + NormAng;
    }*/

    //========================================================================================================================================
    // Funkcije za racunanje chain codea kuteva
    //========================================================================================================================================

    //_____________________________________________________________________________________________
    //
    // Racuna chain code kuta u rasponu 0-7
    // [-pi/8, pi/8> -> 0
    // [pi/8, 3pi/8> -> 1
    // ...
    //_____________________________________________________________________________________________
    static inline unsigned char Angle2ChainCode(ANGLE Angle)
    {
        return ((unsigned char)((Range_Zero_2PI(Angle + Const<T>::PI_8)) / (Const<T>::PI_4)) &
                7); // Radi num. nepreciznosti bez &7 bi se dobio chain kod 8 za kut 22.5�
    }

    //_____________________________________________________________________________________________
    //
    // Racuna chain code kuta u rasponu 0-3
    // [-pi/4, pi/4> -> 0
    // [pi/4, 3pi/4> -> 1
    // ...
    //_____________________________________________________________________________________________
    static inline unsigned char Angle2ChainCode4(ANGLE Angle)
    {
        return ((unsigned char)((Range_Zero_2PI(Angle + Const<T>::PI_4)) / (Const<T>::PI_2)) & 3);
    }

    static inline ANGLE Rad2Deg(ANGLE ang) { return ang * 180 / Const<T>::PI; }

    static inline ANGLE Deg2Rad(ANGLE ang) { return ang * Const<T>::PI / 180; }
};

#endif // ANGLE_RANGE_H

// It wasn't easy
// But nothing is, no
