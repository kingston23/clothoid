#ifndef _PidController_h_
#define _PidController_h_

template <class T>
class CPidController
{
public:
	CPidController()
	{
		// Zelimo da u prvom koraku der i int budu resetirani
		m_bResetDerInt = true;

		m_LastOutput = 0;
	}


	enum EDiscretizationMethod
	{
		DM_Backward,
		DM_Forward,
		DM_Bilin,
	};

	struct SParms
	{
		EDiscretizationMethod DiscretizationMethod;

		T Kp; // Proporc. pojacanje [] <0,inf>
		T Ti; // Integralna vrem. konstanta [s] [0,inf>
		T Td; // Derivacijska vrem. konstanta [s] [0,inf>
		T Ni; // Faktor usporenja derivacijskog clana [] [0,inf>

		T Tdiscr; // Vrijeme diskretizacije [s] [0,inf>
		T StartIntegralTerm; // Pocetna vrijednost za integralni clan
		T MaxOutput; // Ogranicenje izlaza <0,inf>
		T MaxOutputChange;
		
		bool bUseP; // Ovime se moze iskljuciti pojedini clan, ukoliko su svi clanovi iskljuceni, output je 0
		bool bUseI;
		bool bUseD;
	};
	
	bool SetParms(SParms* pPidParms)
	{
		assert(pPidParms);

		m_Kp = pPidParms->Kp;
		m_Ti = pPidParms->Ti;
		m_Td = pPidParms->Td;
		m_Tdiscr = pPidParms->Tdiscr;
		m_Ni = pPidParms->Ni;
		m_DiscretizationMethod = pPidParms->DiscretizationMethod;

		assert(pPidParms->MaxOutput>0);
		m_MaxOutputPos = pPidParms->MaxOutput;
		m_MaxOutputNeg = -pPidParms->MaxOutput;
		m_MaxOutputChange = pPidParms->MaxOutputChange;

		m_bUseP = pPidParms->bUseP;
		m_bUseI = pPidParms->bUseI;
		m_bUseD = pPidParms->bUseD;

		m_LastIntegralTerm = pPidParms->StartIntegralTerm;
		m_LastDerivativeTerm = 0;

		if(!IsValid())
		{
			assert(0);
			return false;
		}

		return true;
	};

	void SetDiscretizationTime(T DiscrTime)
	{
		assert(DiscrTime>=0);
		m_Tdiscr = DiscrTime;
	}

	T GetDiscretizationTime()
	{
		assert(m_Tdiscr>=0);
		return m_Tdiscr;
	}

	T GetControlVal(T RefVal, T MeasuringVal)
	{
		T Output = 0;
		T Error = RefVal - MeasuringVal;

		if(m_bUseP)
			Output = m_Kp*Error;

		if(m_bResetDerInt)
		{
			m_LastIntegralTerm = 0;
			m_LastDerivativeTerm = 0;
			m_LastError = Error;

			m_bResetDerInt = false;
		}

		if(m_bUseI)
		{
			m_LastIntegralTerm += m_Kp/m_Ti*Error*m_Tdiscr;
			Output += m_LastIntegralTerm;
		}

		if(m_bUseD)
		{
			assert(m_Td>0);
			m_LastDerivativeTerm = m_LastDerivativeTerm*m_Td/(m_Td+m_Ni*m_Tdiscr) +
				m_Kp*m_Td*m_Ni*(Error-m_LastError)/(m_Td+m_Ni*m_Tdiscr);
			Output += m_LastDerivativeTerm;
		}

		if(Output > m_MaxOutputPos)
			Output = m_MaxOutputPos;
		else if(Output < m_MaxOutputNeg)
			Output = m_MaxOutputNeg;

		m_LastMeasuringVal = MeasuringVal;
		m_LastError = Error;

		/*
		if(abs(Output-m_LastOutput) > m_MaxOutputChange)
		{
			if(Output>m_LastOutput)
				Output = m_LastOutput + m_MaxOutputChange;
			else
				Output = m_LastOutput - m_MaxOutputChange;
		}*/

		m_LastOutput = Output;

		return Output;

		/*
		T IntegralTerm;

		switch(m_DiscretizationMethod)
		{
		case DM_Backward:
			{
				IntegralTerm = 
			}

		case DM_Forward:
			{
			}

		case DM_Bilin:
			{
			}
		}*/
	};

	//------------------------------------------------------------------------------------
	// Funkcija resetira derivacijski i integralni dio
	//------------------------------------------------------------------------------------
	void ResetDerInt()
	{
		m_bResetDerInt = true;
	}

	bool IsValid() const
	{
		if(m_Kp<=0 || m_Ti<0 || m_Td<0 || m_Tdiscr<0 || m_Ni<0)
			return false;

		return true;
	}

private:
	// Parametri
	EDiscretizationMethod m_DiscretizationMethod; // 0-backward, 1-forward, 2-bilin

	T m_Kp; // Proporcionalno poja�anje
	T m_Ti; // Integralna vremenska konstanta
	T m_Td; // Derivacijska vremenska konstanta
	T m_Tdiscr; // Period diskretizacije
	T m_Ni;
	T m_MaxOutputPos;
	T m_MaxOutputNeg;

	bool m_bUseP;
	bool m_bUseI;
	bool m_bUseD;

	T m_LastIntegralTerm;
	T m_LastMeasuringVal; // Ne koristi se zasad
	T m_LastError;
	T m_LastDerivativeTerm;
	T m_LastOutput;
	T m_MaxOutputChange;

	// Ovo osigurava da u sljedecem koraku der i int budu resetirani
	bool m_bResetDerInt;
};

#endif // _PidController_h_