#ifndef _BoostPTreeHelper_h_
#define _BoostPTreeHelper_h_

#include <boost/property_tree/ptree.hpp>

bool parse_ptree(
				const boost::property_tree::ptree& pt, // Tree in which to search
				std::string key, // Current path inside pt
				const std::string& searchedKey,
				const std::string& searchedSubKey,
				std::string& value // Value of found node
				);

#endif