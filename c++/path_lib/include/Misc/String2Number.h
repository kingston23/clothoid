#ifndef _STRING2NUMBER_H_
#define _STRING2NUMBER_H_

#include <string>
#include <sstream>
#include <iostream>

template <class T>
bool String2Number(const std::string& CppString, T& Number)
{
	std::istringstream IStringStream(CppString);
	return !(IStringStream >> std::dec >> Number).fail();
}

template <class T>
bool String2Number(const char* CString, T& Number)
{
	const std::string CppString(CString);
	std::istringstream IStringStream(CppString);
	return !(IStringStream >> std::dec >> Number).fail();
}

#endif // _STRING2NUMBER_H_
