#ifndef _NUMBER2STRING_H_
#define _NUMBER2STRING_H_

#include <stdio.h>
#include <float.h>

class CNumber2String
{
public:
	static void Number2String(int Value, char* String){ sprintf(String, "%d", Value); }
	static void Number2String(float Value, char* String){ sprintf(String, "%.*g", FLT_DIG, Value); };
	static void Number2String(double Value, char* String){ sprintf(String, "%.*g", DBL_DIG, Value); }

public:
	char Buffer[50];

	template <class T>
	char* Convert(T Value)
	{
		Number2String(Value, Buffer);
		return Buffer;
	}
};

#endif // _NUMBER2STRING_H_
