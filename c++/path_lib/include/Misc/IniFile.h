#ifndef INCLUDED_IniFile_h
#define INCLUDED_IniFile_h

#include <string>
#include <boost/property_tree/ptree.hpp>

class IniFile
{
public:
	IniFile(){};
	IniFile(const std::string& iniFile)
	{
		m_fileName = iniFile;
	};
	~IniFile(){};

	std::string getVal(const std::string& sectionAndKey, const std::string& defaultValue)
	{
		return m_pt.get(sectionAndKey, defaultValue);
	}

	std::string getVal(const std::string& sectionAndKey, const char* defaultValue)
	{
		std::string defString(defaultValue);
		return m_pt.get(sectionAndKey, defString);
	}

	int getVal(const std::string& sectionAndKey, int defaultValue)
	{
		return m_pt.get(sectionAndKey, defaultValue);
	}

	float getVal(const std::string& sectionAndKey, float defaultValue)
	{
		return m_pt.get(sectionAndKey, defaultValue);
	}

	double getVal(const std::string& sectionAndKey, double defaultValue)
	{
		return m_pt.get(sectionAndKey, defaultValue);
	}

	bool getVal(const std::string& sectionAndKey, bool defaultValue)
	{
		return m_pt.get(sectionAndKey, defaultValue);
	}


	bool setVal(const std::string& sectionAndKey, int value)
	{
		m_pt.put(sectionAndKey, value);
		return true;
	}

	bool setVal(const std::string& sectionAndKey, bool value)
	{
		m_pt.put(sectionAndKey, value);
		return true;
	}

	bool setVal(const std::string& sectionAndKey, float value)
	{
		m_pt.put(sectionAndKey, value);
		return true;
	}

	bool setVal(const std::string& sectionAndKey, double value)
	{
		m_pt.put(sectionAndKey, value);
		return true;
	}

	bool setVal(const std::string& sectionAndKey, const std::string& value)
	{
		m_pt.put(sectionAndKey, value);
		return true;
	}

	bool save();
	bool load();

protected:
	boost::property_tree::ptree m_pt;
	std::string m_fileName;
};

#endif
