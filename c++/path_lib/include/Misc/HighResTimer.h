#ifndef _HIGHRESTIMER_H_INCLUDED_
#define _HIGHRESTIMER_H_INCLUDED_

//========================================================================================================================
// Template klasa za precizno mjerenje vremena. Trebalo bi raditi i na win i na linux.
// mb 2006.
//========================================================================================================================
/*
Primjer:

#include "Misc/HighResTimer.h"

{
	//...
	CHighResTimer<double> Timer;

	Timer.Start();

	// Nesto...

	double dTime = Timer.GetDeltaTime();
}
*/

#ifdef _WIN32

#include <windows.h>
#include <assert.h>

//========================================================================================================================
// Template klasa za precizno mjerenje vremena za windowse.
//========================================================================================================================
template <class T>
class CHighResTimer
{
public:

	CHighResTimer()
	{
		// Inicijalizacija - uzmi pocetnu vrijednost i frekvenciju brojaca
		m_ConstructionTime = GetCounterStartValue();
		m_TimerFrequency = GetFrequency();
		m_InitialTime = 0;
	}

	CHighResTimer(double InitialTime)
	{
		// Inicijalizacija - uzmi pocetnu vrijednost i frekvenciju brojaca
		m_ConstructionTime = GetCounterStartValue();
		m_TimerFrequency = GetFrequency();
		m_InitialTime = InitialTime;
	}

	//_____________________________________________________________________________________________________________________________________________________________________
	//
	// Postavlja pocetno vrijeme koje bi timer vratio da ga pozovemo odmah nakon konstrukcije.
	// Ovu funkciju naravno treba pozvati odmah nakon konstrukcije.
	//_____________________________________________________________________________________________________________________________________________________________________
	void SetInitialTime(double InitialTime)
	{
		m_InitialTime = InitialTime;
	}

	//_____________________________________________________________________________________________________________________________________________________________________
	//
	// Funkcija koja vraca trenutno vrijeme (uzima se kao vrijeme koje je proslo od pokretanja konstruktora + zadano pocetno vrijeme)
	//_____________________________________________________________________________________________________________________________________________________________________
	T GetTime()
	{
		assert(m_TimerFrequency != -1);
		LARGE_INTEGER lPerformanceCount;
		::QueryPerformanceCounter(& lPerformanceCount);
		lPerformanceCount.QuadPart -= m_ConstructionTime.QuadPart;

		return T(lPerformanceCount.QuadPart) / m_TimerFrequency + m_InitialTime;
	}

	//_____________________________________________________________________________________________________________________________________________________________________
	//
	// Ova funkcija u kombinaciji sa GetDeltaTime funkcijom sluzi za mjerenje proteklog vremena.
	// Sprema trenutno vrijeme u varijablu pocetnog vremena.
	//_____________________________________________________________________________________________________________________________________________________________________
	void Start()
	{
		::QueryPerformanceCounter(& tStart);
	}

	//_____________________________________________________________________________________________________________________________________________________________________
	//
	// Vraca vrijeme spremljeno kod poziva funkcije Start()
	//_____________________________________________________________________________________________________________________________________________________________________
	T GetStartTime()
	{
		assert(m_TimerFrequency != -1);
		return ((T)(tStart.QuadPart-m_ConstructionTime.QuadPart) / m_TimerFrequency + m_InitialTime);
	}

	//_____________________________________________________________________________________________________________________________________________________________________
	//
	// Vraca vrijeme proteklo od Start()
	//_____________________________________________________________________________________________________________________________________________________________________
	T GetDeltaTime()
	{
		assert(m_TimerFrequency != -1);
		LARGE_INTEGER lPerformanceCount;
		::QueryPerformanceCounter(& lPerformanceCount);
		return ((T)(lPerformanceCount.QuadPart - tStart.QuadPart) / m_TimerFrequency);
	}

	//_____________________________________________________________________________________________________________________________________________________________________
	//
	// Vraca vrijeme proteklo od Start() i resetira pocetno vrijeme (tj. sprema trenutno vrijeme u varijablu pocetnog vremena).
	//_____________________________________________________________________________________________________________________________________________________________________
	T GetDeltaTimeAndRestart()
	{
		assert(m_TimerFrequency != -1);
		LARGE_INTEGER lPerformanceCount;
		::QueryPerformanceCounter(& lPerformanceCount);
		T DeltaTime = ((T)(lPerformanceCount.QuadPart - tStart.QuadPart) / m_TimerFrequency);
		tStart = lPerformanceCount; // resetiraj pocetno vrijeme
		return DeltaTime;
	}

protected:
	LARGE_INTEGER tStart; // pocetno vrijeme za neke funkcije

	// Frekvencija timera.
	// Ako je doslo do greske imat ce vrijednost -1.
	T m_TimerFrequency;

	// Kada je kompjuter dugo ukljucen, 64-bitna vrijednost vremenskog brojaca poprima velike vrijednosti tako da
	// dolazi do gubitka preciznosti kod pretvaranja u double (a pogotovo float). Posto nama nije vazna apsolutna vrijednost
	// vremena, nego samo razlika izmedu nekih dvaju vremenskih trenutaka, mozemo izbjec problem preciznosti tak da
	// kod konstrukcije timera uzmemo pocetnu vrijednost brojaca, i kasnije kod svakog uzimanja vremena, tu vrijednost
	// oduzmemo od trenutnog vremena. Na taj nacin timer uvijek broji vrijeme od proteklo od pocetka konstrukcije +
	// neka konstanta koju mozemo zadati u konstruktoru.
	LARGE_INTEGER m_ConstructionTime;

	double m_InitialTime; // Pocetna vrijednost. To je vrijednost vremena koju bi timer vratio odmah nakon konstrukcije.

protected:
	static T GetFrequency()
	{
		LARGE_INTEGER l_frequency;

		if ( ::QueryPerformanceFrequency (&l_frequency) )
			return T(l_frequency.QuadPart);
		else
			return -1;
	}

	static LARGE_INTEGER GetCounterStartValue()
	{
		LARGE_INTEGER StartValue;
		StartValue.QuadPart = 0;

		::QueryPerformanceCounter(&StartValue);

		return StartValue;
	}
};

#else // _WIN32

 #include <sys/time.h>

//========================================================================================================================
// Template klasa za precizno mjerenje vremena za newindowse.
// TUDU: doraditi ju da se ponasa isto kao i ona za win.
//========================================================================================================================
template <class T>
class CHighResTimer
{
public:

	CHighResTimer()
	{
		// Inicijalizacija - uzmi pocetno vrijeme
		gettimeofday(&tConstructionTime, &tz);
	}

	//_____________________________________________________________________________________________________________________________________________________________________
	//
	// Funkcija koja vraca trenutno vrijeme (uzima se kao vrijeme koje je proslo od pokretanja konstruktora)
	//_____________________________________________________________________________________________________________________________________________________________________
	T GetTime()
	{
		struct timeval tCurrent;
		gettimeofday(&tCurrent, &tz);

		return (T)(tCurrent.tv_sec - tConstructionTime.tvSec) + ((T)(tCurrent.tv_sec - tConstructionTime.tvSec)) / ((T)(1000000));
	}

	//_____________________________________________________________________________________________________________________________________________________________________
	//
	// Ova funkcija u kombinaciji sa End() funkcijom sluzi za mjerenje proteklog vremena.
	// Ovo samo sprema pocetno vrijeme u varijablu pocetnog vremena.
	//_____________________________________________________________________________________________________________________________________________________________________
	void Start()
	{
		gettimeofday(&tStart, &tz);
	}

	//_____________________________________________________________________________________________________________________________________________________________________
	//
	// Sprema trenutno vrijeme u varijablu zavrsnog vremena i vraca vrijeme proteklo od poziva Start().
	//_____________________________________________________________________________________________________________________________________________________________________
	T End()
	{
		gettimeofday(&tEnd, &tz);
		T DeltaTime = (T)(tEnd.tv_sec - tStart.tvSec) + ((T)(tEnd.tv_sec - tStart.tvSec)) / ((T)(1000000));
		return DeltaTime;
	}

	//_____________________________________________________________________________________________________________________________________________________________________
	//
	// Vraca vrijeme izmedu zadnjih poziva Start() i End().
	//_____________________________________________________________________________________________________________________________________________________________________
	T DeltaTime()
	{
		return (T)(tEnd.tv_sec - tStart.tvSec) + ((T)(tEnd.tv_sec - tStart.tvSec)) / ((T)(1000000));
	}

protected:
	struct timeval tConstructionTime;
	struct timeval tStart, tEnd;
	struct timezone tz;
};

#endif // _WIN32

#endif // _HIGHRESTIMER_H_INCLUDED_

