#ifndef _STRINGPARSER_H_
#define _STRINGPARSER_H_

#include "string.h"
#include "ctype.h"

class CStringParser
{
public:

	enum ESeekDir
	{
		BEG = -1,
		CUR,
		END
	};

	CStringParser()
	{
		SetString(NULL, 0);
	}
	CStringParser(char* pString)
	{
		SetString(pString);
	}
	CStringParser(char* pString, unsigned long Len)
	{
		SetString(pString, Len);
	}

	void SetString(char* pString)
	{
		unsigned long Len = pString ? (unsigned long)strlen(pString) : 0;

		SetString(pString, Len);
	}
	void SetString(char* pString, unsigned long Len)
	{
		m_String = pString;
		m_GetPtr = 0;
		m_Len = Len;

		m_bUpdPosOnSuccess = true;
		m_bUpdPosOnFailure = true;
	}

	char* CStringParser::FindPositiveInteger(int& nChars);
	char* CStringParser::FindString(char* String);
	char* CStringParser::FindString(char ** Strings, int nStrings, int& iStringFoundFirst);

	char* CStringParser::GetStringBeforeCharOrBlank(unsigned long& StringLength, char* CharSet);
	char* CStringParser::GetStringBeforeBlank(unsigned long& StringLength);
	char* CStringParser::GetStringBeforeChar(unsigned long& StringLength, char* CharSet);
	char* CStringParser::GetStringContainingChar(unsigned long& StringLength, char* CharSet);

	bool CStringParser::SkipBlanks();
	bool CStringParser::SkipLine();
	int CStringParser::SkipComments();
	bool CStringParser::SkipString(char* String);
	bool CStringParser::SkipChar(char* CharSet);

	char* CStringParser::CheckNextString(char* String);

	char* JumpBeforeBackward(char* Str);
	char* FindStrBackward(char* Str);

	char GetNextChar() { return *(m_String+m_GetPtr++); }

	unsigned long GetChars(char* pBuffer, unsigned long nChars);
	char* GetCurPos() const { return m_String+m_GetPtr; }

	void Seekg(unsigned long Position){ m_GetPtr=Position; };
	void Seekg(long Offset, ESeekDir Direction)
	{
		if(Direction == BEG) m_GetPtr = Offset;
		else if(Direction == CUR) m_GetPtr += Offset;
		else m_GetPtr = m_Len - Offset;
	};

	unsigned long Tellg() const { return m_GetPtr; };

	// Ovo bi moglo vratiti i negativni rezultat!
	unsigned long NumCharsRemain() const { return m_Len-m_GetPtr; }

	bool IsAtEnd() const { return m_GetPtr >= m_Len; };

	// CString1* ExtractQuotedString();

	char* GetBlanks() { return "\x9\xA\xB\xC\xD\x20"; }

	
	char* m_String; // Pokazivac na znakovni niz koji ne mora, ali smije biti NULL terminiran (prav za prav mora jos zasad:)
	unsigned long m_GetPtr;
	unsigned long m_Len; // Broj znakova u stringu - ne ukljucujuci NULL terminator

	// Ako je TRUE, svaka funkcija u klasi bi prilikom trazenja, ekstrakcije itd. trebala
	// updatirati trenutnu poziciju u stringu.
	// Pritom imamo 2 ponasanja, 1. je da funkcija nade ono sto je trazeno, a drugo da ne nade.
	// Sve funkcije se moraju tako ponasati osim ako to nije 
	bool m_bUpdPosOnSuccess;
	bool m_bUpdPosOnFailure;
};

#endif // _STRINGPARSER_H_