#ifndef _C2DCompositeTrajectory_h_
#define _C2DCompositeTrajectory_h_

#include "2DTrajectoryBase.h"

#include <vector>

//=================================================================================================================================================================
//
// Template klasa za sloenu (multikomponentnu) 2D trajektoriju.
// Sadrzi niz pokazivaca na C2DTrajectoryBase klase, koje se moraju nadovezivati jedna
// na drugu (vremenski i prostorno - brzine, x, y, kut, itd. moraju biti kontinuirani).
//
// Trajektorija nakon sto je dodana u niz ne smije vise biti kasnije modificirana, jer klasa
// ne kopira cijelu trajektoriju vec samo pointer na nju.
//
// Kod postavljanja moda ekstrapolacije na kraju trajektorije, potrebno je prvo dodati
// sve trajektorije u niz, pa tek onda postaviti mod ekstrapolacije!
//
//=================================================================================================================================================================
class C2DCompositeTrajectory : virtual public C2DTrajectoryBase
{
public:
    C2DCompositeTrajectory()
    {
        // Poèetna rezervacija memorije za trajektorije
        try
        {
            m_Components.reserve(1000);
        }
        catch(...)
        {
        }
    };
    virtual ~C2DCompositeTrajectory(){

    };

    //________________________________________________________________________________________________________________________________________________
    //
    // Dodavanje trajektorije u niz (na kraj niza).
    //
    // Panja - funkcija modificira sve dodane trajektorije (prilikom dodavanja) osim prve jer
    // poziva Traj.SetBeginTime(), Traj.SetBeginDisp() i Traj.SetBeginDist kako bi se uskladilo
    // s prethodnom trajektorijom u nizu.
    // Trajektorija nakon sto je dodana u niz ne smije vise biti modificirana niti obrisana (npr. bilo bi opasno dodavati lokalne varijable
    // koje se same izbrisu)! Medutim ova klasa ne brise trajektorije kada se one izbacuju iz niza, pa ih korisnik mora sam izbrisati kad
    // nisu vise potrebne (nije bas elegantno rjesenje, al kaj se moze).
    //________________________________________________________________________________________________________________________________________________
    bool AddTrajComponent(C2DTrajectoryBase* pNewTraj)
    {
        if(!pNewTraj->IsValid())
            return false;

        // Postavi ofset tako da se ova trajektorija vremenski i po udaljenosti nastavi na prethodnu
        if(m_Components.empty())
        {
            m_BeginTime = pNewTraj->GetBeginTime();
            m_BeginDisp = pNewTraj->GetBeginDisp();
            m_BeginDist = pNewTraj->GetBeginDist();

            m_DeltaTime = 0;
            m_DeltaDisp = 0;
            m_DeltaDist = 0;
        }
        else
        {
            C2DTrajectoryBase* pLastTraj = m_Components.back();

            pNewTraj->SetBeginTime(pLastTraj->GetEndTime());
            pNewTraj->SetBeginDisp(pLastTraj->GetEndDisp());
            pNewTraj->SetBeginDist(pLastTraj->GetEndDist());
        }

        // Updatiraj ukupnu promjenu vremena i pomaka
        m_DeltaTime += pNewTraj->GetDeltaTime();
        m_DeltaDisp += pNewTraj->GetDeltaDisp();
        m_DeltaDist += pNewTraj->GetDeltaDist();

        // Dodaj trajektoriju u niz
        try
        {
            m_Components.push_back(pNewTraj);
        }
        catch(...)
        {
            return false;
        }

        return true;
    };

    bool ReserveTrajs(int nTrajs)
    {
        try
        {
            m_Components.reserve(nTrajs);
        }
        catch(...)
        {
            return false;
        }

        return true;
    };

    //________________________________________________________________________________________________________________________________________________
    //
    // Ovo treba pozvati radi praznjenja vektora s komponentama prije stvaranja novog niza trajektorija.
    // Buduci da spremamo pointere, ovo nece izbrisati same komponente na koje pointeri pokazuju.
    //________________________________________________________________________________________________________________________________________________
    inline void RemoveAllTrajs() { m_Components.clear(); };

    int GetNumTrajs() const { return (int)m_Components.size(); };

    inline C2DTrajectoryBase* GetTrajComponent(unsigned int iTrajComp) const { return m_Components[iTrajComp]; };

    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        unsigned int nTrajs = GetNumTrajs();
        if(nTrajs < 1)
            return false;

        T Time = pTrPoint->Time;

        for(unsigned int iTraj = 0; iTraj < nTrajs - 1; iTraj++)
        {
            if(Time < m_Components[iTraj]->GetEndTime())
                return m_Components[iTraj]->GetPointByTime(pTrPoint);
        }
        return m_Components[nTrajs - 1]->GetPointByTime(pTrPoint);
    }

    bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        int nTrajs = GetNumTrajs();
        if(nTrajs < 1)
            return false;

        T Disp = pTrPoint->Disp;

        for(int iTraj = 0; iTraj < nTrajs - 1; iTraj++)
        {
            if(Disp < m_Components[iTraj]->GetEndDisp())
                return m_Components[iTraj]->GetPointByDisp(pTrPoint);
        }
        return m_Components[nTrajs - 1]->GetPointByDisp(pTrPoint);
    }

    bool GetPointByDist(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        unsigned int nTrajs = GetNumTrajs();
        if(nTrajs < 1)
            return false;

        T Dist = pTrPoint->Dist;

        for(unsigned int iTraj = 0; iTraj < nTrajs - 1; iTraj++)
        {
            if(Dist < m_Components[iTraj]->GetEndDist())
                return m_Components[iTraj]->GetPointByDist(pTrPoint);
        }
        return m_Components[nTrajs - 1]->GetPointByDist(pTrPoint);
    };

    T GetBeginTime() const { return m_BeginTime; }
    T GetDeltaTime() const { return m_DeltaTime; }
    void SetBeginTime(T BeginTime)
    {
        assert(IsValid());

        m_BeginTime = BeginTime;

        unsigned int nTrajs = GetNumTrajs();

        if(nTrajs >= 1)
            SetBeginTime(0, BeginTime); // Za prvu trajektoriju

        for(unsigned int iTraj = 1; iTraj < nTrajs; iTraj++)
        {
            C2DTrajectoryBase* pLastTraj = m_Components[iTraj - 1];
            C2DTrajectoryBase* pCurTraj = m_Components[iTraj];

            pCurTraj->SetBeginTime(pLastTraj->GetEndTime());
        }
    }

    T GetBeginDisp() const { return m_BeginDisp; }
    T GetDeltaDisp() const { return m_DeltaDisp; }
    void SetBeginDisp(T BeginDisp)
    {
        assert(IsValid());

        m_BeginDisp = BeginDisp;

        unsigned int nTrajs = GetNumTrajs();

        if(nTrajs >= 1)
            SetBeginDisp(0, BeginDisp); // Za prvu trajektoriju

        for(unsigned int iTraj = 1; iTraj < nTrajs; iTraj++)
        {
            C2DTrajectoryBase* pLastTraj = m_Components[iTraj - 1];
            C2DTrajectoryBase* pCurTraj = m_Components[iTraj];

            pCurTraj->SetBeginDisp(pLastTraj->GetEndDisp());
        }
    }

    T GetBeginDist() const { return m_BeginDist; }
    T GetDeltaDist() const { return m_DeltaDist; }
    void SetBeginDist(T BeginDist)
    {
        assert(IsValid());

        m_BeginDist = BeginDist;

        unsigned int nTrajs = GetNumTrajs();

        if(nTrajs >= 1)
            SetBeginDist(0, BeginDist); // Za prvu trajektoriju

        for(unsigned int iTraj = 1; iTraj < nTrajs; iTraj++)
        {
            C2DTrajectoryBase* pLastTraj = m_Components[iTraj - 1];
            C2DTrajectoryBase* pCurTraj = m_Components[iTraj];

            pCurTraj->SetBeginDist(pLastTraj->GetEndDist());
        }
    }

    bool IsValid() const
    {
        if(!C2DTrajectoryBase::IsValid())
            return false;

        if(m_Components.empty())
            return false;

        return true;
    }

    // Kod kompozitne trajektorije, mod ekstrapolacije na pocetku definiran je
    // prvom trajektorijom u nizu
    EPathExtrapolationMode GetPathBeginExtrapolationMode() const
    {
        if(!m_Components.empty())
            return m_Components.front()->GetPathBeginExtrapolationMode();

        return EM_Unknown;
    }

    // Kod kompozitne trajektorije, mod ekstrapolacije na kraju definiran je
    // zadnjom trajektorijom u nizu
    EPathExtrapolationMode GetPathEndExtrapolationMode() const
    {
        if(!m_Components.empty())
            return m_Components.back()->GetPathEndExtrapolationMode();

        return EM_Unknown;
    }

    // Kod kompozitne trajektorije, mod ekstrapolacije na pocetku definiran je
    // prvom trajektorijom u nizu
    bool SetPathBeginExtrapolationMode(EPathExtrapolationMode BeginMode)
    {
        if(!m_Components.empty())
            return m_Components.front()->SetPathBeginExtrapolationMode(BeginMode);

        assert(0); // Nema nijedne trajektorije u nizu
        return false;
    }

    // Kod kompozitne trajektorije, mod ekstrapolacije na kraju definiran je
    // zadnjom trajektorijom u nizu
    bool SetPathEndExtrapolationMode(EPathExtrapolationMode EndMode)
    {
        if(!m_Components.empty())
            return m_Components.back()->SetPathEndExtrapolationMode(EndMode);

        assert(0); // Nema nijedne trajektorije u nizu
        return false;
    }

    std::vector<C2DTrajectoryBase*> m_Components; // vektor koji sprema pokazivace na pojedine dijelove trajektorije

private:
    void SetBeginTime(unsigned int iTraj, T BeginTime) { m_Components[iTraj]->SetBeginTime(BeginTime); }
    void SetBeginDisp(unsigned int iTraj, T BeginDisp) { m_Components[iTraj]->SetBeginDisp(BeginDisp); }
    void SetBeginDist(unsigned int iTraj, T BeginDist) { m_Components[iTraj]->SetBeginDist(BeginDist); }

    T m_DeltaDisp;
    T m_BeginDisp;

    T m_DeltaDist;
    T m_BeginDist;

    T m_DeltaTime;
    T m_BeginTime;
};

#endif
