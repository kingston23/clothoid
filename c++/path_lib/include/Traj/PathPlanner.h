#pragma once

#include "Geom/Circle.h"
#include "ArcPathProfile.h"
#include "LinePathProfile.h"
#include "MobileRobot.h"
#include "TrajPathProfile.h"

#include <float.h>

//--------------------------------------------------------------------------------
// Bazna klasa za planere prostornog dijela trajektorije
//--------------------------------------------------------------------------------
template<class T>
class CPathPlannerBase : public C2DTrajectoryDataTypes<T>
{
public:
    CPathPlannerBase()
    {
        m_bInverted = false;
        m_bAutoInvert = true;
    }

    virtual CTrajPathProfile* PlanPath(typename CMobileRobot::SRobotState* pBeginState, typename CMobileRobot::SRobotState* pEndState,
                                       typename CMobileRobot* pRobot)
    {
        return false;
    };

    virtual bool IsValid() const { return true; }

    // Za invertiranje
    void SetAutoInvert(bool bAutoInvert) { m_bAutoInvert = bAutoInvert; }
    bool GetAutoInvert() const { return m_bAutoInvert; }
    bool IsInverted() const { return m_bInverted; }

protected:
    bool m_bInverted;   // Dal je planirana trajektorija invertirana
    bool m_bAutoInvert; // Dal ga ova klasa smije dati invertiranu trajektoriju (ovisno o situaciji)
};
