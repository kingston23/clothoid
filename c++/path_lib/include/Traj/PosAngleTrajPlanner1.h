#pragma once

#include "LineCirclePathPlanner1.h"
#include "MaxSpeedCircleLineTimeProfile.h"
#include "ProfiledTraj.h"
#include "TrajectoryPlanner.h"

//---------------------------------------------------------------------------------------------------
// Planer trajektorije koji dovodi robota u zadani polozaj "maksimalnom" brzinom.
// Prostorni profil trajektorije sastoji se od linija i kruznih isjecaka.
// Vremenski profil generira se tako da se postigne maksimalna brzina.
//
// Ovaj planer ne daje fizikalno moguce trajektorije jer ima nagle prijelaze iz kruznice u
// pravac i mogu se takoder desiti skokovi u brzini.
//
// Paznja: generirana trajektorija vazeca je samo dok egzistira objekt ove klase. Brisanjem
// ovog objekta brisu se i komponente generirane trajektorije.
//---------------------------------------------------------------------------------------------------
template<typename T>
class CPosAngleTrajPlanner1 : public CTrajectoryPlannerBase<T>
{
public:
    virtual C2DTrajectoryBase* PlanTrajectory(typename CMobileRobot::SRobotState* pBeginState, // In
                                              typename CMobileRobot::SRobotState* pEndState,   // In
                                              CMobileRobot* pRobot                             // In
    )
    {
        assert(pBeginState && pEndState);

        m_PathProfilePlanner.SetAutoInvert(GetAutoInvert());

        CTrajPathProfile* pPathProfile = m_PathProfilePlanner.PlanPath(pBeginState, pEndState, pRobot);

        if(!pPathProfile)
            return nullptr;

        // Ako je path planner vratio invertiranu trajektoriju, onda i time profile planner mora dati invertiranu trajektoriju
        m_TimeProfilePlanner.Invert(m_PathProfilePlanner.IsInverted());

        CTrajTimeProfile* pTimeProfile =
            m_TimeProfilePlanner.PlanTimeProfile((CTrajCompositePathProfile*)pPathProfile, pBeginState, pEndState, pRobot);

        if(!pTimeProfile)
            return nullptr;

        if(!m_Traj.SetPathProfile(pPathProfile))
            return nullptr;

        if(!m_Traj.SetTimeProfile(pTimeProfile))
            return nullptr;

        // Postavi flag ako je trajektorija invertirana
        m_Traj.SetInverted(m_PathProfilePlanner.IsInverted());

        return &m_Traj;
    };

    virtual C2DTrajectoryBase* GetTrajectory() { return &m_Traj; };

    struct SParms
    {
        typename CLineCirclePathPlanner1<T>::SParms PathParms;
        typename CMaxSpeedCircleLineTimeProfile<T>::SParms TimeProfileParms;
    };

    bool SetParms(SParms* pParms)
    {
        // Parametri za path profile
        if(!m_PathProfilePlanner.SetParms(&pParms->PathParms))
            return false;

        // Parametri za time profile
        if(!m_TimeProfilePlanner.SetParms(&pParms->TimeProfileParms))
            return false;

        return true;
    }

    bool IsValid() const
    {
        if(!m_PathProfilePlanner.IsValid())
            return false;

        if(!m_TimeProfilePlanner.IsValid())
            return false;

        if(!m_Traj.IsValid())
            return false;

        if(!CTrajectoryPlannerBase<T>::IsValid())
            return false;

        return true;
    }

    virtual bool IsInverted() const { return m_Traj.IsInverted(); }

protected:
    CLineCirclePathPlanner1<T> m_PathProfilePlanner;
    CMaxSpeedCircleLineTimeProfile<T> m_TimeProfilePlanner;

    CProfiledTraj<T> m_Traj;
};
