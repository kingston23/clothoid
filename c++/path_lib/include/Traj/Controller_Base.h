#ifndef _Controller_Base_h_
#define _Controller_Base_h_

#include "2DTrajectoryBase.h"

//=======================================================================================================================================
//
// Bazna klasa za bilo kakav kontroler (npr. kontroler za navigaciju robota).
//
//=======================================================================================================================================

template<typename T>
class CController_Base : public C2DTrajectoryDataTypes<T>
{
public:
    enum EControllerName
    {
        CN_None,
        CN_CMU98,
        CN_Line1,
        CN_Goto1,
        CN_OpenLoop,
    };

    EControllerName m_Name;
};

#endif // _Controller_Base_h_
