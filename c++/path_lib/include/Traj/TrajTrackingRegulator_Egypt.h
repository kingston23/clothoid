#ifndef _TrajTrackingRegulator_Egypt_h_
#define _TrajTrackingRegulator_Egypt_h_

#include "Math/AngleRange.h"
#include "TrajTrackingController.h"

using namespace std;

//************************************************************************************************************************************************************************************
//
// Nelinearni regulator za slijedenje trajektorije za mobilnog robota - bazira se na clanku za recenziju za RAS
//
//************************************************************************************************************************************************************************************
class CTrajTrackingRegulator_Egypt : public CTrajTrackingController
{
public:
    CTrajTrackingRegulator_Egypt() { m_ControllerType = TTC_Egypt; }

    ~CTrajTrackingRegulator_Egypt() {}

    virtual bool OpenLogFile(const char* FileName)
    {
        m_LogFile.close();
        m_LogFile.clear();
        m_LogFile.open(FileName, ios::out);
        if(!m_LogFile)
            return false;

        m_LogFile << "% Rezultati eksperimenta s nelinearnim (bazira se na clanku za recenziju za RAS od Egipcana) regulatorom za "
                     "slijedenje trajektorije \n";
        m_LogFile << "% Podaci u stupcima imaju poredak: \n";
        m_LogFile << "% Vrijeme | Trajektorija | Mjerenja | Pogreska | Reference \n";
        m_LogFile << "% Detaljnije: \n";
        m_LogFile << "% t xTraj yTraj angTraj vTraj wTraj aTraj xm ym angm vm wm e1 e2 e3 vRef wRef\n";

        m_LogFile << fixed;   // Tak da svaki float broj zauzima isto mjesta
        m_LogFile << showpos; // Tak da pozitivni brojevi imaju +

        // cout.precision(6); // broj znacajnih znamenki (default je 6)
        // cout.width (10); // koliko znamenka zauzima ukupno mjesta (ostatak se nadopunjava razmacima), izgleda da ovo ne dela za float

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Funkcija racuna nove referentne brzine za robota. Poziva se za svaki novi vremenski uzorak.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool GetRefValues(typename CMobileRobot::SRobotState* pState,   // (in), u ovoj strukturi nalazi se trenutno stanje robota
                              typename CMobileRobot::SRobotState* pRefState // (out), u ovu strukturu postavlja se referentna lin. i kutna
                                                                            // brzina robota. Ostali clanovi strukture se ignoriraju
    )
    {
        assert(pState && pRefState);

        if(!IsValid())
        {
            assert(0);
            return false;
        }

        T Time = pState->CurTime; // trenutno vrijeme

        // Uzimamo referentnu tocku iz trajektorije (ref. tocka je funkcija vremena)
        C2DTrajectoryBase::S2DTrajPoint RefPoint;
        RefPoint.Time = Time + (T)0.5 * m_SamplingPeriod; // Uzimamo reference s x koraka unaprijed
        if(!m_pTraj->GetPointByTime(&RefPoint))
            return false;

        // Tocka trajektorije
        T xTraj = RefPoint.x;
        T yTraj = RefPoint.y;
        T angTraj = RefPoint.Angle;
        T vTraj = RefPoint.Velocity;
        T wTraj = RefPoint.GetAngVelocity();
        T aTraj = RefPoint.Accel;

        // Stanje robota
        T xm = pState->x;
        T ym = pState->y;
        T angm = pState->Angle;

        T vm = pState->Velocity;
        T wm = pState->AngVelocity;

        m_LogFile << Time << ' ' << xTraj << ' ' << yTraj << ' ' << angTraj << ' ' << vTraj << ' ' << wTraj << ' ' << aTraj << ' ';
        m_LogFile << xm << ' ' << ym << ' ' << angm << ' ' << vm << ' ' << wm << ' ';

        // Predikcija zbog kasnjenja
        CompensateDelay(pState);

        // Sad uzimamo predvidena stanja robota kao mjerena stanja
        xm = pState->x;
        ym = pState->y;
        angm = pState->Angle;

        vm = pState->Velocity;
        wm = pState->AngVelocity;

        // Proracun izlaza regulatora (tj. ref lin. i kutne brzine robota)

        // Pogreske slijedenja
        T e1 = cos(angm) * (RefPoint.x - xm) + sin(angm) * (RefPoint.y - ym);
        T e2 = -sin(angm) * (RefPoint.x - xm) + cos(angm) * (RefPoint.y - ym);
        T e3 = CAngleRange::Range_MinusPI_PI(RefPoint.Angle - angm);

        // Zbroji pogreske - to je SSE (sum of squared errors)
        m_SSE1 += e1 * e1;
        m_SSE2 += e2 * e2;
        m_SSE3 += e3 * e3;

        // Koeficijenti za algoritam
        T k1 = 15;
        T k2 = 0.25;
        T k_theta = 4;

        // Vektor ub je feedback vektor, a [vRef; wRef] je upravljacka velicina

        // Za kutnu brzinu
        T ub2 = k2 * RefPoint.Velocity * (e2 + 1 * k_theta * e3) + RefPoint.Velocity / k_theta * sin(e3);
        T wRef = 1.f * RefPoint.GetAngVelocity() + ub2; // ref. kutna brzina [rad/s]

        // Za lin brzinu
        T ub1 = k1 * e1 - 1 * k_theta * e3 * wRef;
        T vRef = 1.f * RefPoint.Velocity * cos(e3) + ub1; // ref. linearna brzina [m/s]

        // Napuni strukturu s ref. stanjem
        pRefState->Velocity = vRef;
        pRefState->AngVelocity = wRef;
        pRefState->x = xTraj;
        pRefState->y = yTraj;
        pRefState->Angle = angTraj;

        // Ogranici ref. vrijednosti
        BoundRefValues(pRefState);

        m_LogFile << e1 << ' ' << e2 << ' ' << e3 << ' ' << ' ' << pRefState->Velocity << ' ' << pRefState->AngVelocity << endl;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Struktura za zadavanje parametara regulatora
    //____________________________________________________________________________________________________________________________________________________________________________________
    struct SParms : public CTrajTrackingController::SParms
    {
        // Parametri regulatora
        T b;
        T zeta;
    };

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje parametara regulatora.
    // Ovu funkciju uvijek treba pozvati prije koristenja regulatora.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool SetParms(CTrajTrackingController::SParms* pBaseParms)
    {
        if(!CTrajTrackingController::SetParms(pBaseParms))
            return false;

        SParms* pParms = static_cast<SParms*>(pBaseParms);

        m_b = pParms->b;
        m_zeta = pParms->zeta;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera stanja klase
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValid() const
    {
        if(!CTrajTrackingController::IsValid())
            return false;

        if(!IsValidParms())
            return false;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera dal su dobro postavljeni parametri
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValidParms() const
    {
        if(m_zeta < 0)
            return false;

        if(m_b < 0)
            return false;

        return true;
    }

private:
    // Parametri regulatora

    T m_zeta; // Faktor prigusenja modelske prijenosne funkcije
    T m_b;    // Dodatni faktor za podesavanje

    std::ofstream m_LogFile; // Log datoteka
};

#endif
