#pragma once

#include "2DTrajectoryBase.h"
#include "MobileRobot.h"
// #include "Log/Log.h"

class CTrajTrackingRegulator_PD : public C2DTrajectoryDataTypes<T>
{
public:
    // Zovi za svaki novi sample.
    // U pState zadaju se mjerenja.
    // U pRefState dobiju se ref. lin. i kutna brzina.
    bool GetRefValues(CMobileRobot::SRobotState* pState        // IN
                          CMobileRobot::SRobotState* pRefState // OUT
    )
    {
        assert(pState);

        // LOGSection(TrajTrackingRegulator_PD_GetRefValues);

        if(!IsValid())
        {
            assert(0);
            return false;
        }

        // Trenutno vrijeme
        T Time = pState->CurTime;

        // Referentna tocka uzima se iz trajektorije
        pRefPoint->Time = Time + m_DiscretizationTime; // Uzimamo reference s jednim korakom unaprijed
        if(!m_pTraj->GetPointByTime(pRefPoint))
            return false;

        // Ref. tocka
        T xr = pRefPoint->x;
        T yr = pRefPoint->y;
        T angr = pRefPoint->Angle;
        T cr = pRefPoint->Curv;
        T vr = pRefPoint->Velocity;
        T wr = SpeedRef * CurvRef;

        // Mjerenje
        T xm = pState->x;
        T ym = pState->y;
        T angm = pState->Angle;

        // Proracun izlaza regulatora (tj. lin. i kutne brzine robota)

        // Ovdje ide proracun referenci ... !!!!
        T OutSpeedRef = 0.f;
        T OutAngSpeedRef = 0.f;

        // Napuni strukturu s ref. stanjem
        pRefState->Velocity = OutSpeedRef;
        pRefState->AngVelocity = OutAngSpeedRef;
        pRefState->x = xr;
        pRefState->y = yr;
        pRefState->Angle = angr;

        return true;
    }

    // Inicijalizacija
    struct SParms
    {
        // Reference se dobivaju iz trajektorije:
        C2DTrajectoryBase* pTraj;
    };

    bool SetParms(SParms* pParms)
    {
        m_pTraj = pParms->pTraj;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        return true;
    }

    bool SetTrajectory(C2DTrajectoryBase* pTraj)
    {
        if(!pTraj)
        {
            assert(0);
            return false;
        }
        m_pTraj = pTraj;

        return true;
    }

    bool IsValid() const
    {
        if(!IsValidParms())
            return false;

        if(!m_pTraj)
            return false;

        return true;
    }

    bool IsValidParms() const
    {
        if(m_DiscretizationTime <= 0)
            return false;

        return true;
    }

private:
    // Referentna trajektorija koju slijedimo
    C2DTrajectoryBase* m_pTraj;

    T m_DiscretizationTime;

    // Ovdje idu parametri regulatora (kao npr. zeta, pojacanja itd) !!!!
};
