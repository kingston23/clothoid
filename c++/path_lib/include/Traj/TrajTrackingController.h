#ifndef _TrajTrackingController_h_
#define _TrajTrackingController_h_

#include "Common/Variable.h"
#include "Math/Numeric.h"
#include "2DTrajectoryBase.h"
#include "MobileRobot.h"

#include <fstream>
#include <ios>

using namespace MathMB;

//=================================================================================================================================================================
//
// Baza za regulatore za slijedenje 2D trajektorije za mobilnog robota.
//
// Ispravan redoslijed koristenja funkcija je:
// - Konstrukcija
// - SetParms() - pozvati ako je potrebno, u protivnom koriste se defaultni parametri iz konstruktora
// - Initialize() - uvijek pozvati prije koristenja (i kasnije eventualno nakon SetParms ako naknadno mijenjamo parametre)
// - GetRefValues()
//
//=================================================================================================================================================================
class CTrajTrackingController
{
public:
    typedef double T;

    enum ETrajTrackingController
    {
        TTC_None,
        TTC_Linear,
        TTC_Nonlinear,
        TTC_MPC,
        TTC_MPC_IM,
        TTC_PI,
        TTC_Moj,
        TTC_Moj2,
        TTC_Egypt,
        TTC_Toni,
    };

    CTrajTrackingController* Construct(ETrajTrackingController ControllerType);

    CTrajTrackingController()
    {
        m_SSE1 = m_SSE2 = m_SSE3 = 0;

        m_vp = m_wp = 0;

        m_ControllerType = TTC_None;

        m_pTraj = nullptr;
        m_vMax = (T)1;
        m_wMax = (T)30;
        m_LinAccelMax = (T)1;
        m_AngAccelMax = (T)30;
        m_nSamplesDelay = (T)0;
        m_SamplingPeriod = (T)0.0125;

        m_pTraj = nullptr;
    }

    virtual ~CTrajTrackingController() { m_LogFile.close(); }

    virtual bool OpenLogFile(const char* FileName)
    {
        m_LogFile.close();
        m_LogFile.clear();
        m_LogFile.open(FileName, std::ios::out);
        if(!m_LogFile)
            return false;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Funkcija racuna nove referentne brzine za robota. Poziva se za svaki novi vremenski uzorak.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool GetRefValues(typename CMobileRobot::SRobotState* pState,   // (in), u ovoj strukturi nalazi se trenutno stanje robota
                              typename CMobileRobot::SRobotState* pRefState // (out), u ovu strukturu postavlja se referentna lin. i kutna
                                                                            // brzina robota. Ostali clanovi strukture se ignoriraju
    )
    {
        assert(0); // jer je ovo samo deklaracija
        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Predikciju radi kasnjenja mjernih podataka (ovo je zajednicko za sve regulatore).
    // Funkcija radi predikciju buduceg stanja robota koristenjem kinematickog modela robota.
    // Ogranicenja: zasad funkcija radi samo za kasnjenje koje je cjelobrojni umnozak vremena uzorkovanja (TUDU: poopciti).
    // Da bi ovo radilo, izvedena klasa mora sama postavljati konacne proracunate referentne vrijednosti u kruzni spremnik,
    // ili pozvati funkciju BoundRefValues() koja to sama radi.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool CompensateDelay(typename CMobileRobot::SRobotState* pState // (in), u ovoj strukturi nalazi se trenutno mjereno stanje robota
    )
    {
        assert(pState);

        // Stanje robota (mjereno)
        T xm = pState->x;
        T ym = pState->y;
        T angm = pState->Angle;

        T vm = pState->Velocity;
        T wm = pState->AngVelocity;

        // Predikcija zbog kasnjenja

        // Pocetno predvideno stanje - uzimamo ga kao mjereno stanje, a ne iz modela kao sto se inace radi.
        // Razlog je da bi nam inace x,y i kut sigurno oddriftali jer ovaj sustav ima pogresku u stac stanju.
        // S druge strane, predvidenu brzinu mozemo uzeti direktno iz modela jer ona nema pogresku u stacionarnom stanju.
        // Npr. ako zadamo vRef = 2, robot ce zaista tu brzinu i postici.
        // Bolje bi bilo da koristimo i mjerenje i model - (Kalman)
        T xp = xm;
        T yp = ym;
        T angp = angm;

        // Predvidene brzine
        // 1. nacin - na temelju mjerenja
        m_vp = vm;
        m_wp = wm;

        // 2. nacin - na temelju modela - tj. kao pocetnu brzinu uzmemo onu posljednju predvidenu, pa ne treba nista mijenjati (u tom
        // slucaju ovo gore treba zakomentirati)

        for(int iPredictionStep = 0; iPredictionStep < m_nSamplesDelay; iPredictionStep++)
        {
            // Uzimam prosle reference spremljene u kruznom spremniku.
            // Uzimam od najstarije prema novijima (zato je indeks negativan)
            T vRef = m_vRefHistory.Get(-(iPredictionStep + 1));
            T wRef = m_wRefHistory.Get(-(iPredictionStep + 1));

            // Uzimamo da predvidena brzina robota idealno slijedi referentnu sve dok nije premasena maksimalna akceleracija

            // Zeljene promjene brzine
            T dv = vRef - m_vp;
            T dw = wRef - m_wp;

            // Promjenu brzine ogranicavamo maks akceleracijom
            dv = CNum<T>::Bound(-m_LinAccelMax * m_SamplingPeriod, dv, m_LinAccelMax * m_SamplingPeriod);
            dw = CNum<T>::Bound(-m_AngAccelMax * m_SamplingPeriod, dw, m_AngAccelMax * m_SamplingPeriod);

            // Predvidena brzina robota u ovom koraku predikcije
            m_vp += dv;
            m_wp += dw;

            // Predvidena pozicija robota u ovom koraku predikcije
            xp += m_SamplingPeriod * cos(angp) * m_vp;
            yp += m_SamplingPeriod * sin(angp) * m_vp;
            angp += m_SamplingPeriod * m_wp;
        }

        // Proglasimo predvidena stanja mjerenim stanjima

        pState->x = xp;
        pState->y = yp;
        pState->Angle = angp;

        pState->Velocity = m_vp;
        pState->AngVelocity = m_wp;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Ogranicava referentne vrijednosti, ali tako da zakrivljenost ostane ocuvana.
    // Takoder postavlja izracunate ref. vrijednosti (za koje se smatra da su konacne nakon poziva ove funkcije!!!!)
    // u kruzni spremnik - koji kasnije sluzi za kompenzaciju kasnjenja.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool BoundRefValues(typename CMobileRobot::SRobotState* pRefState // (out), nepotrebno ovdje, moze biti nullptr
    )
    {
        assert(pRefState);

        // Ogranici reference, ali tako da zakrivljenost putanje ostane sacuvana
        assert(m_vMax > 0 && m_wMax > 0);

        T vRef = pRefState->Velocity;
        T wRef = pRefState->AngVelocity;

        T vSigma = CNum<T>::Abs(vRef) / m_vMax;
        T wSigma = CNum<T>::Abs(wRef) / m_wMax;

        if(vSigma > 1 && vSigma > wSigma)
        {
            if(vRef > 0)
                vRef = m_vMax;
            else
                vRef = -m_vMax;

            wRef /= vSigma;
        }
        else if(wSigma > 1 && wSigma > vSigma)
        {
            if(wRef > 0)
                wRef = m_wMax;
            else
                wRef = -m_wMax;

            vRef /= wSigma;
        }

        // Napuni strukturu s ref. stanjem
        pRefState->Velocity = vRef;
        pRefState->AngVelocity = wRef;

        // Zapamti nove reference u kruznom spremniku
        if(m_nSamplesDelay > 0)
        {
            m_vRefHistory.Advance();
            m_vRefHistory.Set(vRef);

            m_wRefHistory.Advance();
            m_wRefHistory.Set(wRef);
        }

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Struktura za zadavanje parametara regulatora
    //____________________________________________________________________________________________________________________________________________________________________________________
    struct SParms
    {
        T SamplingPeriod;

        T vMax;
        T wMax;

        T LinAccelMax;
        T AngAccelMax;

        T nSamplesDelay;

        C2DTrajectoryBase* pTraj; // referentna trajektorija
    };

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje parametara regulatora.
    // Ovu funkciju uvijek treba pozvati prije koristenja regulatora.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool SetParms(SParms* pParms)
    {
        m_pTraj = pParms->pTraj;

        m_SamplingPeriod = pParms->SamplingPeriod;
        m_nSamplesDelay = pParms->nSamplesDelay;

        m_vMax = pParms->vMax;
        m_wMax = pParms->wMax;

        m_LinAccelMax = pParms->LinAccelMax;
        m_AngAccelMax = pParms->AngAccelMax;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Inicijalizacija regulatora - ovo se mora pozvati nakon konstrukcije i/ili SetParms().
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool Initialize()
    {
        if(!m_vRefHistory.BufferSize((int)m_nSamplesDelay))
            return false;

        if(!m_wRefHistory.BufferSize((int)m_nSamplesDelay))
            return false;

        if(!IsValid())
            return false;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera stanja klase
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValid() const
    {
        if(!IsValidParms())
            return false;

        if(!m_pTraj)
            return false;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera dal su dobro postavljeni parametri
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValidParms() const
    {
        if(m_SamplingPeriod <= 0)
            return false;

        if(m_vMax <= 0)
            return false;

        if(m_wMax <= 0)
            return false;

        if(m_LinAccelMax <= 0)
            return false;

        if(m_AngAccelMax <= 0)
            return false;

        if(m_nSamplesDelay < 0)
            return false;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje nove trajektorije
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool SetTrajectory(C2DTrajectoryBase* pTraj)
    {
        if(!pTraj)
        {
            assert(0);
            return false;
        }
        m_pTraj = pTraj;

        return true;
    }

    C2DTrajectoryBase* GetTrajectory() const { return m_pTraj; }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Resetira regulator (tj. resetira memorirana stanja regulatora).
    // Ovo je npr. korisno kad zaustavimo upravljanje, pa ga zelimo kasnije ponovo pokrenuti
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual void Reset()
    {
        m_SSE1 = m_SSE2 = m_SSE3 = 0;

        m_vp = m_wp = 0;

        // Postavi zapamcene referentne vrijednosti brzina na nulu
        m_vRefHistory.SetAllZero();
        m_wRefHistory.SetAllZero();
    }

public:
    T m_SSE1, m_SSE2, m_SSE3; // Suma kvadrata pogreske - korisno kod usporedbe razlicitih regulatora

protected:
    // Referentna trajektorija koju slijedimo
    C2DTrajectoryBase* m_pTraj;

    // Parametri regulatora

    T m_SamplingPeriod; // vrijeme uzorkovanja

    T m_vMax; // [>0] max. linijska brzina (po aps. vrijednosti)
    T m_wMax; // [>0] max. kutna brzina (po aps. vrijednosti)

    T m_LinAccelMax; // [>0] max. linijska akceleracija (po aps. vrijednosti)
    T m_AngAccelMax; // [>0] max. kutna akceleracija (po aps. vrijednosti)

    T m_nSamplesDelay; // Koliko koraka uzorkovanja iznosi kasnjenje

    // Spremnici za prosle vrijednosti upravljackih signala
    CVariableHistoryDynamic<T> m_vRefHistory;
    CVariableHistoryDynamic<T> m_wRefHistory;

    // Posljednja predvidena brzina robota (lin. i kutna). Koristi se kod kompenzacije kasnjenja.
    T m_vp, m_wp;

public:
    ETrajTrackingController m_ControllerType;

    std::ofstream m_LogFile; // Log datoteka
};

// Za konstrukciju regulatora float tipa
CTrajTrackingController* ConstructTrajTrackingController(CTrajTrackingController::ETrajTrackingController ControllerType);

#endif

// These are the rules I make
// Our chains were meant to break
// You'll never change me
