#ifndef _LineTraj1_h_
#define _LineTraj1_h_

#include "2DTrajectoryBase.h"

#include <Assert.h>
#include <cmath>

// Jednostavna trajektorija po pravcu koja ubrzava od v=0 do v=v_stac zadanom akceleracijom
// i natrag do v=0, zadanom deceleracijom.

// Definirati DBG_FILE_ za dijagnostiku (ispis upozorenja u file).

template<class T>
class CLineTraj1 : public C2DTrajectoryBase
{
public:
#ifdef DBG_FILE_
    CLineTraj1() { dbgf = fopen("LineTraj1.debug", "wt"); };
#endif

#ifdef DBG_FILE_
    virtual ~CLineTraj1() { fclose(dbgf); };
#endif

    int GetCapabilities() { return TRCAP_FULL; }

    bool GetPointByTime(T Time, S2DTrajPoint* pTrPoint) const
    {
        T Velocity;
        T Accel;
        T Dist;

        if(Time >= m_t3)
        {
            // Stoji u krajnjoj to�ki
            Velocity = 0;
            Accel = 0;
            Dist = m_Dist3;
        }
        else if(Time >= m_t2)
        {
            // Jednoliko ubrzano akceleracijom m_Deccel brzine m_v do 0
            T dt = Time - m_t2;
            Velocity = m_v - m_Deccel * dt;
            Accel = -m_Deccel;
            Dist = m_Dist2 + (m_v + Velocity) * dt / 2;
        }
        else if(Time >= m_t1)
        {
            // Jednoliko brzinom m_v
            Velocity = m_v;
            Accel = 0;
            Dist = m_Dist1 + m_v * (Time - m_t1);
        }
        else if(Time >= m_t0)
        {
            // Jednoliko ubrzano akceleracijom m_Accel brzine 0 do m_v
            T dt = Time - m_t0;
            Velocity = m_Accel * dt;
            Accel = m_Accel;
            Dist = m_d0 + Velocity * dt / 2;
        }
        else // Vrijeme manje od m_t0
        {
            // Stoji u po�etnoj to�ki
            Velocity = 0;
            Accel = 0;
            Dist = m_d0;
        }

        pTrPoint->x = m_x0 + Dist * m_Cos;
        pTrPoint->y = m_y0 + Dist * m_Sin;
        pTrPoint->Angle = m_Angle0;
        pTrPoint->Velocity = Velocity;
        pTrPoint->Accel = Accel;
        pTrPoint->Time = Time;
        pTrPoint->Dist = Dist;
        pTrPoint->Curv = 0;
        return true;
    }

    bool GetPointByDist(T Dist, S2DTrajPoint* pTrPoint)
    {
        T Time;
        T Velocity;
        T Accel;

        if(Dist >= m_Dist3)
        {
            // Put ne smije biti ve�i od m_Dist3 - vratimo false ili korigiramo
#ifdef DBG_FILE_
            fprintf(dbgf, "Dist>m_Dist3 (udaljenost ve�a od maks mogu�e) u GetPointByDist\n");
#endif
            Dist = m_Dist3;
            Velocity = 0;
            Accel = 0;
            Time = m_t3;
        }
        else if(Dist >= m_Dist2)
        {
            // Jednoliko usporeno akceleracijom m_Deccel od brzine m_v do 0.
            T ds = Dist - m_Dist2;
            T Discr = m_v * m_v - 2 * m_Deccel * ds; // Diskriminanta
#ifdef DBG_FILE_
            if(Discr < 0)
                fprintf(dbgf, "Diskriminanta negativna za Dist1<Dist<Dist2 u GetPointByDist\n");
#endif
            T dt = (m_v - (T)sqrt(Discr)) / m_Deccel;
            Velocity = m_v - m_Deccel * dt;
            Accel = -m_Deccel;
            Time = m_t2 + dt;
        }
        else if(Dist >= m_Dist1)
        {
            // Jednoliko brzinom m_v
            Velocity = m_v;
            Accel = 0;
            Time = m_t1 + (Dist - m_Dist1) / m_v;
        }
        else if(Dist >= m_d0)
        {
            // Jednoliko ubrzano akceleracijom m_Accel od brzine m_v do 0.
            T dt = (T)sqrt(2 * Dist / m_Accel);
            Velocity = m_Accel * dt;
            Accel = m_Accel;
            Time = m_t0 + dt;
        }
        else
        {
            // assert(0); // return false;
            Dist = 0;
            Velocity = 0;
            Accel = 0;
            Time = m_t0;
#ifdef DBG_FILE_
            fprintf(dbgf, "Dist manji od m_d0 u GetPointByDist\n");
#endif
        }

        pTrPoint->x = m_x0 + Dist * m_Cos;
        pTrPoint->y = m_y0 + Dist * m_Sin;
        pTrPoint->Angle = m_Angle0;
        pTrPoint->Curv = 0;
        pTrPoint->Time = Time;
        pTrPoint->Velocity = Velocity;
        pTrPoint->Accel = Accel;
        pTrPoint->Dist = Dist;

        return true;
    }

    struct SParms1
    {
        T StartTime;
        T StartDist;
        T x0;
        T y0;
        T TotalDist;
        T LineAngle;
        T v;
        T Accel;
        T Deccel;
    };

    bool SetParms(SParms1* pParms)
    {
        m_t0 = pParms->StartTime;
        m_d0 = pParms->StartDist;
        m_x0 = pParms->x0;
        m_y0 = pParms->y0;
        m_Distance = pParms->TotalDist;
        m_Angle0 = pParms->LineAngle;
        m_v = pParms->v;
        m_Accel = pParms->Accel;
        m_Deccel = pParms->Deccel;

        return CheckParms();
    };

private:
    bool CheckParms()
    {
        if(m_v12 < 0)
            m_v12 = -m_v12;

        if(m_v0 < 0)
            m_v0 = 0;
        if(m_v3 < 0)
            m_v3 = 0;

        if(m_v0 > m_v12)
            m_Accel = -abs_(m_Accel);
        else
            m_Accel = abs_(m_Accel);

        if(m_v3 < m_v12)
            m_Deccel = -abs_(m_Deccel);
        else
            m_Deccel = abs_(m_Deccel);

        if(m_Distance < 0)
            m_Distance = -m_Distance;

        T dt01 = (m_v12 - m_v0) / m_Accel;
        T Dist01 = (m_v0 + m_v12) * dt01 / 2;

        T dt23 = (m_v3 - m_v12) / m_Deccel;
        T Dist23 = (m_v12 + m_v3) * dt23 / 2;

        T Dist12 = m_Distance - Dist01 - Dist23;
        T dt12;
        if(Dist12 > 0)
        {
            dt12 = Dist12 / m_v12;
        }
        else
        {
            // Ne mo�e se ubrzati do zadane brzine i natrag do v=0 na tako kratkoj udaljenosti
            // Sada su dvije opcije: vratiti false ili se prilagoditi

            /*// Maksimalna brzina koja se mo�e posti�i
            T vmax = (T)sqrt(2*m_Distance*m_Accel*m_Deccel/(m_Accel+m_Deccel));
            m_v12 = vmax;

            dt01 = vmax/m_Accel;
            Dist01 = vmax*dt01/2;

            dt23 = vmax/m_Deccel;
            Dist23 = vmax*dt23/2;

            dt12 = 0;
            Dist12 = 0;*/

#ifdef DBG_FILE_
            fprintf(dbgf, "Ne mo�e se ubrzati do zadane brzine i natrag do v=0 na tako kratkoj udaljenosti!\n");
#endif
            return false;
        }

        m_t1 = m_t0 + dt01;
        m_t2 = m_t1 + dt12;
        m_t3 = m_t2 + dt23;

        m_Dist1 = m_d0 + Dist01;
        m_Dist2 = m_Dist1 + Dist12;
        m_Dist3 = m_Dist2 + Dist23;

        m_Cos = (T)cos(m_Angle0);
        m_Sin = (T)sin(m_Angle0);

        return true;
    };

private:
    static inline T abs_(T x) { return (x > 0 ? x : -x) }

    // Parametri
private:
    // Po�etni uvjeti
    T m_t0; // Po�etno vrijeme (obi�no 0)
    T m_d0; // Po�etna udaljenost (obi�no 0)
    T m_x0; // Po�etna to�ka
    T m_y0;
    T m_Angle0; // Kut pravca

    T m_Distance; // Ukupna duljina prevaljenog puta po trajektoriji
    T m_v0;       // Po�etna brzina
    T m_v12;      // Stacionarna brzina
    T m_v3;       // Kona�na brzina
    T m_Accel;    // Akceleracija od brzine 0 do 1
    T m_Deccel;   // Akceleracija od brzine 1 do 2 - negativna je

private:
    // Private parametri (za ubrzanje ra�unanja da se ne moraju svaki puta ra�unati)
    // Postavlja ih funkcija SetParms() -> CheckParms()
    T m_t1; // Vrijeme u to�ki 1
    T m_t2;
    T m_t3;

    T m_Dist1; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 1
    T m_Dist2; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 2
    T m_Dist3; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 3

    T m_Cos;
    T m_Sin;

#ifdef DBG_FILE_
    FILE* dbgf;
#endif
};

/*
template <class T>
int CLineTraj1<T>::ab()
{
    return 1;
}
*/

/*
Primjer upotrebe:

#define DBG_FILE_ // Opcionalno
#include "LineTraj1.h"

CLineTraj1 Traj;

typedef CLineTraj1::SParms1 SLnParms1;
SLnParms1 Lp1;
Lp1.StartTime = 0;
Lp1.StartDist = 1;
Lp1.x0 = 10;
Lp1.y0 = 20;
Lp1.TotalDist = 3;
Lp1.LineAngle = 3.14f;
Lp1.v = 2;
Lp1.Accel = 1;
Lp1.Deccel = 1;
Traj.SetParms(&Lp1);

typedef CLineTraj1::S2DTrajPoint STrajPt;

STrajPt TrajPt1;
float d = 1;
Traj.GetPointByDist(d, &TrajPt1);

float t = 2;
STrajPt TrajPt2;
Traj.GetPointByTime(t, &TrajPt2);
*/

#endif // _LineTraj1_h_
