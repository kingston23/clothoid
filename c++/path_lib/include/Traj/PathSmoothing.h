#ifndef _Path_Smoothing_H_
#define _Path_Smoothing_H_

#include "Geom/2DClothoidAlgorithms.h"
#include "Geom/2DPoint.h"
#include "Math/Numeric.h"
#include "2DTrajectoryBase.h"
#include "ArcPathProfile.h"
#include "ClothoidPathProfile.h"
#include "LinePathProfile.h"
#include "TrajPathProfile.h"

#include <list>
#include <vector>

using namespace MathMB;

//=================================================================================================================================================================
//
// Klasa za izgladivanje putanje klotoidama.
// TUDU: ova klasa je predvidena da se poziva cesto (puno puta u sekundi) i na slicnim putanjama.
//		Pa bi bilo mozda korisno zapamtiti neke parametre trenutnog rezultata kako bi se ubrzao sljedeci proracun.
// Ako u imenu varijable ne stoji "smoothed", znaci da je to po dijelovima ravna putanja (u pravilu).
//
//=================================================================================================================================================================
class CClothoidPathSmoothing 
{
public:
    typedef double T;

    CClothoidPathSmoothing()
    {
        // prealociranje buffera

        // ocekivani broj zavoja, tj. segmenata u po dijelovima ravnoj putanji
        static const int nCurves = 100;

        m_PWLPathToSmooth.reserve(nCurves);
        m_SmoothedPath.ReserveTrajs(nCurves *
                                    3); // recimo da u prosjeku svaki zavoj generira tri segmenta putanje (dvije klotoide i liniju)

        // ovdje koristimo resize umjesto reserve, jer liste nemaju reserve
        m_ClothoidBuffer.resize(nCurves * 2); // u prosjeku 1 zavoj - 2 klotoide
        m_LineBuffer.resize(nCurves);         // svaki zavoj moze imati i do dva linijska segmenta, al u prosjeku ima jednog
        m_ArcBuffer.resize(1);                // sadasnji algoritam moze generirati maks 1 kruzni luk

        m_LineBuffer_FirstFree = m_LineBuffer.begin();
        m_ArcBuffer_FirstFree = m_ArcBuffer.begin();
        m_ClothoidBuffer_FirstFree = m_ClothoidBuffer.begin();
    }

    ~CClothoidPathSmoothing() {}

    //________________________________________________________________________________________________________________________________________________
    //
    // Izgladivanje po dijelovima ravne putanje klotoidama.
    // Pretpostavke na putanju su:
    // - Izmedu svake dvije susjedne tocke je jedan pravocrtni segment putanje duljine > 0
    // - Ne postoje susjedne tri kolinearne tocke
    // - Prva tocka putanje jednaka je pocetnoj poziciji
    // - Prvi segment ima smjer kao i pocetna orijentacija
    // - Zadnja tocka putanje jednaka je ciljnoj poziciji,
    // - Zadnji segment ima smjer kao i ciljna orijentacija (samo u slucaju ako nam je vazna orijentacija u cilju, ako nije, onda je smjer
    // zadnjeg segmenta proizvoljan)
    //________________________________________________________________________________________________________________________________________________
    bool
    SmoothPath(const typename C2DTrajectoryBase::S2DTrajPoint& CurrentState, // trenutno stanje zadano pozicijom, kutem i zakrivljenoscu
               const typename C2DTrajectoryBase::S2DTrajPoint&
                   GoalState,                            // ciljno stanje zadano pozicijom, kutem i zakrivljenoscu koja mora biti 0
               const std::vector<C2DPoint>& PWLinearPath // Putanja je zadana jednostavno kao niz tocaka. Mora zadovoljavati gornje uvjete.
    )
    {
        m_pOriginalPWPath = &PWLinearPath;

        ClearPath();

        // Glatki spoj trenutnog stanja i po dijelovima linearne putanje
        if(!SmoothConnect(CurrentState, PWLinearPath))
            return false;

        // Sad jos izgladimo po dijelovima linearnu putanju
        if(!SmoothPWLinearPath())
            return false;

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Funkcija pronalazi glatki spoj trenutnog stanja sa po dijelovima ravnom putanjom.
    // Glatki spoj sprema se u odgovarajucu member varijablu.
    //________________________________________________________________________________________________________________________________________________
    bool SmoothConnect(const typename C2DTrajectoryBase::S2DTrajPoint& CurrentState, const std::vector<C2DPoint>& PWLinearPath)
    {
        //printf("duljina niza path: %d \n", PWLinearPath.size());
        if(PWLinearPath.size() < 2)
        {
            assert(0);
            return false;
        }

        bool bSuccess1 = false, bSuccess2 = false, bSuccess3 = false; // za sva tri algoritma

        if(CNum<T>::IsZero(CurrentState.Curv))
        {
            //[bSuccess ConnectSegment] = SmoothConnectLineLine(CurrentState, SmoothedPath);

            // Dovoljno je izgladiti originalnu po dijelovima ravnu putanju standardnim algoritmima
            m_PWLPathToSmooth = PWLinearPath;
            return true; // nista vise za raditi
        }

        // Trenutnu kruznicu po kojoj se robot giba spajamo s drugim linijskim segmentom putanje.
        // Teoretski bi se putanja mogla sastojati i od samo jednog segmenta, pa se u tom slucaju spajamo na prvi segment putanje.
        // Za segment na koji se spajamo, spajamo se samo na njegovu prvu polovicu (pretpostavka je da ce druga polovica biti koristena za
        // izgladivanje ostatka putanje)

        // Priprema ulaznih argumenata
        
        //printf( "ulazni parametri PWLinearPath 0 i 1: [%f %f %f %f]",  PWLinearPath[1].y , PWLinearPath[1].x, PWLinearPath[2].y,  PWLinearPath[2].y );

        // Pocetno stanje
        CGeometryBase::S2DPathState P0;
        P0.x = CurrentState.x;
        P0.y = CurrentState.y;
        P0.ang = CurrentState.Angle;
        P0.k = CurrentState.Curv;

        // Segment G1-G2 kao prvu polovicu segmenta putanje na koji se spajamo
        C2DPoint G1, G2;
        if(PWLinearPath.size() > 2)
        {
            G1 = PWLinearPath[1];
            G2.MiddlePoint(G1, PWLinearPath[2]);

            // m_PWLPathToSmooth.clear(); - valjda ne treba kod assign
            m_PWLPathToSmooth.assign(PWLinearPath.begin() + 1,
                                     PWLinearPath.end()); // nije nam potrebna pocetna tocka putanje - putanja sada pocinje u G1
            // Jos dodatno namjestamo da putanja pocinje u G2. Ovo je samo inicijalno, i kasnije ce se ova tocka vjerojatno zamijeniti s
            // nekom tockom izmedu G1 i G2.
            m_PWLPathToSmooth[0] = G2;
        }
        else
        {
            G1 = PWLinearPath[0];
            G2.MiddlePoint(G1, PWLinearPath[1]);

            m_PWLPathToSmooth.resize(2); // imamo samo dvije tocke u putanji, tj. 1 segment
            m_PWLPathToSmooth[0] = G2;
            m_PWLPathToSmooth[1] = PWLinearPath[1];
        }

        // Izlazni argumenti
        C2DClothoidApp Clothoid1, Clothoid2;
        CLineParBounded Line;
        CCircleArc Arc;

        bSuccess1 = C2DClothoidAlgorithms::ConnectCircleLine1C(P0, G1, G2, Arc, Clothoid2, Line);

        if(bSuccess1)
        {
            // Hura uspjelo - slozi putanju od dobivenih segmenata

            // kruzni luk
            if(CNum<T>::IsPositive(Arc.GetDeltaAngle()))
            {
                CArcPathProfile::SParms0 ArcParms;
                ArcParms.ArcBeginAngle = Arc.GetBeginAngle();
                ArcParms.Radius = Arc.GetRadius();
                ArcParms.xCenter = Arc.m_xc;
                ArcParms.yCenter = Arc.m_yc;
                ArcParms.BeginDist = 0; // nevazno
                ArcParms.DeltaDist = Arc.GetArcLength();
                ArcParms.bPositiveDirection = Arc.IsPositiveDirection();
                CArcPathProfile& ArcSegment = GiveMeCircularArcPathSegment();
                ArcSegment.SetParms(&ArcParms);
                m_SmoothedPath.AddTrajComponent(&ArcSegment);
            }

            // izlazna klotoida
            if(CNum<T>::IsPositive(Clothoid2.GetLength()))
            {
                CClothoidPathProfile::SParms0 ClothoidParms;
                ClothoidParms.pClothoid = &Clothoid2;
                ClothoidParms.BeginDist = 0; // ovo nije vazno, bit ce naknadno postavljeno kod dodavanja u niz
                ClothoidParms.DeltaDist = Clothoid2.GetLength();
                CClothoidPathProfile& ClothoidSegment = GiveMeClothoidPathSegment();
                ClothoidSegment.SetParms(&ClothoidParms);
                m_SmoothedPath.AddTrajComponent(&ClothoidSegment);
            }

            // pravac (ostatak gibanja ide po pravcu)
            if(CNum<T>::IsPositive(Line.m_Length))
            {
                // Pravac ne stavljamo u spojni segment, vec ga pridruzujemo sredisnjem dijelu putanje koji ce kasnije biti izgladen.
                m_PWLPathToSmooth[0] = Line.p0; // Za ovo je dovoljno samo postaviti pocetnu tocku
            }
        }
        else
        {
            // Nije uspjelo - Pokusavamo sa sljedecim algoritmom
            bSuccess2 = C2DClothoidAlgorithms::ConnectCircleLine2C(P0, G1, G2, Clothoid1, Clothoid2, Line);
        }

        if(bSuccess2)
        {
            // Hura uspjelo - slozi putanju od dobivenih segmenata

            CClothoidPathProfile::SParms0 ClothoidParms;

            // ulazna klotoida
            if(CNum<T>::IsPositive(Clothoid1.GetLength()))
            {
                ClothoidParms.pClothoid = &Clothoid1;
                ClothoidParms.BeginDist = 0; // ovo nije vazno, bit ce naknadno postavljeno kod dodavanja u niz
                ClothoidParms.DeltaDist = Clothoid1.GetLength();
                CClothoidPathProfile& ClothoidSegment = GiveMeClothoidPathSegment();
                ClothoidSegment.SetParms(&ClothoidParms);
                m_SmoothedPath.AddTrajComponent(&ClothoidSegment);
            }

            // izlazna klotoida
            if(CNum<T>::IsPositive(Clothoid2.GetLength()))
            {
                ClothoidParms.pClothoid = &Clothoid2;
                ClothoidParms.BeginDist = 0; // ovo nije vazno, bit ce naknadno postavljeno kod dodavanja u niz
                ClothoidParms.DeltaDist = Clothoid2.GetLength();
                CClothoidPathProfile& ClothoidSegment = GiveMeClothoidPathSegment();
                ClothoidSegment.SetParms(&ClothoidParms);
                m_SmoothedPath.AddTrajComponent(&ClothoidSegment);
            }

            // pravac (ostatak gibanja ide po pravcu)
            if(CNum<T>::IsPositive(Line.m_Length))
            {
                // Pravac ne stavljamo u spojni segment, vec ga pridruzujemo sredisnjem dijelu putanje koji ce kasnije biti izgladen.
                m_PWLPathToSmooth[0] = Line.p0; // Za ovo je dovoljno samo postaviti pocetnu tocku
            }
        }

        if(!(bSuccess1 || bSuccess2))
        {
            // Nije uspjelo - Pokusavamo sa sljedecim algoritmom, a to je "ConnectCircleLine3C"
            bSuccess3 = ConnectCircleLine3C(P0, PWLinearPath, Clothoid1, Line);
        }

        if(bSuccess3)
        {
            // Slozi putanju od dobivenih segmenata

            // izravnavajuca klotoida
            if(CNum<T>::IsPositive(Clothoid1.GetLength()))
            {
                CClothoidPathProfile::SParms0 ClothoidParms;
                ClothoidParms.pClothoid = &Clothoid1;
                ClothoidParms.BeginDist = 0; // ovo nije vazno, bit ce naknadno postavljeno kod dodavanja u niz
                ClothoidParms.DeltaDist = Clothoid1.GetLength();
                CClothoidPathProfile& ClothoidSegment = GiveMeClothoidPathSegment();
                ClothoidSegment.SetParms(&ClothoidParms);
                m_SmoothedPath.AddTrajComponent(&ClothoidSegment);
            }

            // pravac je vec ubacen u sredisnji dio putanje (tj. u m_PWLPathToSmooth), pa s njim nemamo vise posla
        }

        return bSuccess1 || bSuccess2 || bSuccess3;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Ovo je high level ConnectCircleLine3C koji zove ConnectCircleLine3C_LowLevel.
    //________________________________________________________________________________________________________________________________________________
    bool ConnectCircleLine3C(
        const typename CGeometryBase::S2DPathState& P0, // pocetno stanje koje se sastoji od pocetne pozicije, kuta i zakrivljenosti
        const std::vector<C2DPoint>& PWLinearPath,      //
        C2DClothoidApp& Clothoid,                       // izravnavajuca klotoida
        CLineParBounded& Line // tangenta na izravnavajucu klotoidu koja pocinje u tocki di je zakrivljenost jednaka nuli
    )
    {
        bool bSuccess = false;

        if(!InitializePickScaling())
            return false;

        // Varijable parametara koji se mijenjaju kod optimiranja
        T C_array[m_PickScalingMaxIters][m_3CLineLengthMaxIters]; // niz koristenih faktora skaliranja
        T L_array[m_PickScalingMaxIters][m_3CLineLengthMaxIters]; // niz duljina linije (one koja dolazi nakon izravnavajuce klotoide)

        // Varijable za racunanje kriterija optimalnosti
        T d_array[m_PickScalingMaxIters][m_3CLineLengthMaxIters]; // niz duljina putanja za razlicite faktore skaliranja
        T AngleSum_array[m_PickScalingMaxIters]
                        [m_3CLineLengthMaxIters]; // niz sa zbrojevima kuteva skretanja za razlicite faktore skaliranja

        const T MaxDistance = (T)0.5; // maks duljina ravnog segmenta - ovo ovisi o velicini robota

        T C; // trenutni faktor skaliranja
        T L; // trenutna duljina linije

        for(int i = 0; i < m_PickScalingMaxIters; i++)
        {
            if(!PickScaling(C))
                break;

            // StraightPath
            if(!C2DClothoidAlgorithms::ConnectCircleLine3C_LowLevel(P0, C, Clothoid, Line))
                continue;

            // CheckAdmissibility() - provjerava se dal je klotoida slobodna od prepreka (nije implementirano)

            // UpdatePath

            // Prvo provjeravamo kolko daleko mozemo ispaliti zraku u smjeru zavrsne linije

            T FreeDistance;
            if(!ShootRay(Line.FormLineParRay(), MaxDistance, FreeDistance))
                continue;

            for(int j = 0; j < m_3CLineLengthMaxIters; j++)
            {
                C_array[i][j] = C;

                // Sad se odmaknemo u smjeru zavrsne linije za dobivenu slobodnu udaljenost (ili njezin dio), i odavde replaniramo po
                // dijelovima ravnu putanju do cilja

                L = FreeDistance * (j + 1) / m_3CLineLengthMaxIters;
                Line.m_Length = L;

                L_array[i][j] = L;

                if(!ReplanPiecewiseLinearPath_NewStartLine(PWLinearPath, Line, m_PWLPathToSmooth))
                    continue;

                T AngleSum;
                T Length = PiecewiseLinearPathLength(m_PWLPathToSmooth, AngleSum);

                d_array[i][j] = Length;
                AngleSum_array[i][j] = AngleSum;

                bSuccess = true; // ako uspije barem jedna iteracija, znaci da je ukupno uspjelo
            }
        }

        // Pronadi maksimume u nizovima radi normalizacije
        T d_Max = std::numeric_limits<T>::min();
        T AngleSum_Max = std::numeric_limits<T>::min();

        for(int i = 0; i < m_PickScalingMaxIters; i++)
        {
            for(int j = 0; j < m_3CLineLengthMaxIters; j++)
            {
                if(d_array[i][j] > d_Max)
                    d_Max = d_array[i][j];

                if(AngleSum_array[i][j] > AngleSum_Max)
                    AngleSum_Max = AngleSum_array[i][j];
            }
        }

        if(d_Max <= 0)
            return false;

        bool bConsiderAngle = true; // dal uzimati u obzir kut
        if(AngleSum_Max <= 0)
            bConsiderAngle = false;

        T d_NormFactor = (T)1 / d_Max;
        T AngleSum_NormFactor = (T)1 / AngleSum_Max;

        // Tezine za razlicite kriterije optimiranja
        T w_d = (T)0.5;        // tezina za duljinu putanje
        T w_AngleSum = (T)0.5; // tezina za sumu kuteva skretanja

        T BestC; // faktor skaliranja koji daje najbolji kriterij
        T BestL; // duljina ravne linije koja daje najbolji kriterij

        // Inicijalizacija kriterija optimalnosti
        T BestCriterion = std::numeric_limits<T>::max();

        for(int i = 0; i < m_PickScalingMaxIters; i++)
        {
            for(int j = 0; j < m_3CLineLengthMaxIters; j++)
            {
                T CurrentCriterion = w_d * d_array[i][j] * d_NormFactor;

                if(bConsiderAngle)
                    CurrentCriterion += w_AngleSum * AngleSum_array[i][j] * AngleSum_NormFactor;

                if(CurrentCriterion < BestCriterion)
                {
                    BestL = L_array[i][j];
                    BestC = C_array[i][j];
                    BestCriterion = CurrentCriterion;
                }
            }
        }

        if(bSuccess)
        {
            // Hura uspjelo
            // Uspjeli smo pronaci neki dobar C

            if(!(C == BestC) || !(L == BestL)) // provjera dal je trenutno izracunata putanja najbolja, ako nije treba ju opet racunati
            {
                // Ponovo pokreni algoritam za najbolji C
                C2DClothoidAlgorithms::ConnectCircleLine3C_LowLevel(P0, BestC, Clothoid, Line);

                T FreeDistance;
                ShootRay(Line.FormLineParRay(), MaxDistance, FreeDistance);

                // Sad se odmaknemo u smjeru zavrsne linije za dobivenu slobodnu udaljenost, i odavde replaniramo po dijelovima ravnu
                // putanju do cilja
                Line.m_Length = BestL;

                ReplanPiecewiseLinearPath_NewStartLine(PWLinearPath, Line, m_PWLPathToSmooth);
            }
        }

        return bSuccess;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Igladuje piecewise linear putanju klotoidama.
    // Kolinearne tocke moraju bit izbacene iz putanje.
    //________________________________________________________________________________________________________________________________________________
    bool SmoothPWLinearPath()
    {
        const int nTurns = (int)m_PWLPathToSmooth.size() - 2;

        if(nTurns < 1)
        {
            // nema nijednog zavoja, nema potrebe za ikakvim izgladivanjem pa samo dodajemo ravni segment
            if(m_PWLPathToSmooth.size() == 2)
            {
                CLinePathProfile::SParms LineParms;
                LineParms.BeginDist = 0; // nevazno
                LineParms.DeltaDist = m_PWLPathToSmooth[0].GetDistanceToPoint(m_PWLPathToSmooth[1]);
                LineParms.LineAngle = m_PWLPathToSmooth[0].AngleToPoint(m_PWLPathToSmooth[1]);
                LineParms.x0 = m_PWLPathToSmooth[0].x;
                LineParms.y0 = m_PWLPathToSmooth[0].y;
                CLinePathProfile& LinePathProfile = GiveMeLinePathSegment();
                LinePathProfile.SetParms(&LineParms);
                m_SmoothedPath.AddTrajComponent(&LinePathProfile);
            }
        }

        // Izgladivanje ako ima zavoja

        // Ovo ce nam posluziti u slucaju da algoritam daje dvije linije koje se nastavljaju jedna na drugu
        CLinePathProfile* pPreviousLinePathProfile = nullptr;

        for(int iTurn = 0; iTurn < nTurns; iTurn++)
        {
            // Start tocka prvog segmenta
            C2DPoint S = m_PWLPathToSmooth[iTurn];

            // Srednja tocka zavoja
            const C2DPoint M = m_PWLPathToSmooth[iTurn + 1];

            // Krajnja tocka drugog segmenta
            C2DPoint G = m_PWLPathToSmooth[iTurn + 2];

            // Najcesce uzimamo cijele segmente SM i MG, ali ima iznimki
            const T d1Fraction = 1;
            T d2Fraction = 1;

            if(nTurns == 1)
            {
                // Ako je samo jedan zakret (tj. to je i prvi i zadnji)
                d2Fraction = (T)0.5; // koristi samo dio segmenta MG, jer ocemo ravno stici u cilj
            }
            else if(iTurn == 0)
            {
                // Prvi zakret
                G = G.PointBetween(M, (T)0.5); // koristi samo polovicu sljedeceg segmenta tak da ostane prostora za sljedeci zakret
            }
            else if(iTurn == nTurns - 1)
            {
                // Zadnji zakret
                S = S.PointBetween(M, (T)0.5); // koristi samo drugu polovicu prvog segmenta, jer je prva vec potrosena u prethodnom zakretu
                d2Fraction = (T)0.5;           // koristi samo dio segmenta MG, jer ocemo ravno stici u cilj
            }
            else
            {
                S = S.PointBetween(M, (T)0.5);
                G = G.PointBetween(M, (T)0.5);
            }

            Geom::CLineParBounded StartLine, EndLine;
            Geom::C2DClothoidApp StartClothoid, EndClothoid;

            const T eMax = 1e10f;
            const T sMax = C2DClothoidApp::m_pLookupTable->m_sMax;
            T d, e;

            if(!C2DClothoidAlgorithms::SmoothSharpTurn(S, M, G, d1Fraction, d2Fraction, eMax, sMax, d, e, StartLine, StartClothoid,
                                                       EndClothoid, EndLine))
                return false;

            // Dodaj dobivene pravce/klotoide u putanju

            if(CNum<T>::IsPositive(StartLine.Length()))
            {
                // Provjeri dal se ovaj novi pravac nastavlja na onaj prethodni, ako da, onda je smisleno ujediniti ih u samo jedan pravac
                // unsigned int nTrajs = m_SmoothedPath.GetNumTrajs();
                // if(nTrajs>0)
                //{
                //	CTrajPathProfile* pLastPathSegment = m_SmoothedPath.GetTrajComponent(nTrajs-1);
                //	if(pLastPathSegment && pLastPathSegment->IsLine())
                //	{
                //		T Angle = ((CLinePathProfile*)pLastPathSegment)->GetAngle();
                //		T x0, y0;
                //		((CLinePathProfile*)pLastPathSegment)->GetStartPoint(x0, y0);

                //		CLinePar PrevLine(Angle, x0, y0);
                //		if( StartLine.IsIdentical(PrevLine) )
                //		{
                //			// Nastavljamo se samo na prethodni pravac, produzujemo ga
                //			T OldDist = ((CLinePathProfile*)pLastPathSegment)->GetDeltaDist();
                //			((CLinePathProfile*)pLastPathSegment)->SetDeltaDist( OldDist+StartLine.Length() ); // ovo je imalo bug, produzilo
                //je pravac, al ne i cijelu kompozitnu putanju 			bPreviousLineContinue = true;
                //		}
                //	}
                //}

                if(pPreviousLinePathProfile)
                {
                    // Samo produzi prethodni pravac, jer ovaj i prethodni su sigurno istog smjera i nastavljaju se jedan na drugog.
                    // Time smo ujedinili prethodni i novi pravac.
                    pPreviousLinePathProfile->SetDeltaDist(pPreviousLinePathProfile->GetDeltaDist() + StartLine.Length());
                    m_SmoothedPath.AddTrajComponent(pPreviousLinePathProfile);
                    pPreviousLinePathProfile = nullptr;
                }
                else
                {
                    // Novi pravac
                    CLinePathProfile::SParms LineParms;
                    LineParms.BeginDist = 0; // nevazno
                    LineParms.DeltaDist = StartLine.Length();
                    LineParms.LineAngle = StartLine.GetDirection();
                    LineParms.x0 = StartLine.p0.x;
                    LineParms.y0 = StartLine.p0.y;
                    CLinePathProfile& LinePathProfile = GiveMeLinePathSegment();
                    LinePathProfile.SetParms(&LineParms);
                    m_SmoothedPath.AddTrajComponent(&LinePathProfile);
                }
            }
            else if(pPreviousLinePathProfile)
            {
                // Nemamo pocetnu liniju, ali imamo nedodanu liniju od prije, pa dodajmo ju sad
                m_SmoothedPath.AddTrajComponent(pPreviousLinePathProfile);
                pPreviousLinePathProfile = nullptr;
            }

            if(CNum<T>::IsPositive(StartClothoid.GetLength()))
            {
                CClothoidPathProfile::SParms0 ClothoidParms;
                ClothoidParms.pClothoid = &StartClothoid;
                ClothoidParms.BeginDist = 0; // ovo nije vazno, bit ce naknadno postavljeno kod dodavanja u niz
                ClothoidParms.DeltaDist = StartClothoid.GetLength();
                CClothoidPathProfile& ClothoidSegment = GiveMeClothoidPathSegment();
                ClothoidSegment.SetParms(&ClothoidParms);
                m_SmoothedPath.AddTrajComponent(&ClothoidSegment);
            }

            if(CNum<T>::IsPositive(EndClothoid.GetLength()))
            {
                CClothoidPathProfile::SParms0 ClothoidParms;
                ClothoidParms.pClothoid = &EndClothoid;
                ClothoidParms.BeginDist = 0; // ovo nije vazno, bit ce naknadno postavljeno kod dodavanja u niz
                ClothoidParms.DeltaDist = EndClothoid.GetLength();
                CClothoidPathProfile& ClothoidSegment = GiveMeClothoidPathSegment();
                ClothoidSegment.SetParms(&ClothoidParms);
                m_SmoothedPath.AddTrajComponent(&ClothoidSegment);
            }

            if(CNum<T>::IsPositive(EndLine.Length()))
            {
                CLinePathProfile::SParms LineParms;
                LineParms.BeginDist = 0; // nevazno
                LineParms.DeltaDist = EndLine.Length();
                LineParms.LineAngle = EndLine.GetDirection();
                LineParms.x0 = EndLine.p0.x;
                LineParms.y0 = EndLine.p0.y;
                CLinePathProfile& LinePathProfile = GiveMeLinePathSegment();
                LinePathProfile.SetParms(&LineParms);
                pPreviousLinePathProfile = &LinePathProfile; // zapamti za kasnije, ali nemoj jos dodavati u ukupnu putanju
            }
        }

        if(pPreviousLinePathProfile)
        {
            // Imamo nedodan linijski profil otprije, dodajmo ga
            m_SmoothedPath.AddTrajComponent(pPreviousLinePathProfile);
            pPreviousLinePathProfile = nullptr;
        }

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Racuna ukupnu duljinu po dijelovima ravne putanje.
    //________________________________________________________________________________________________________________________________________________
    T PiecewiseLinearPathLength(const std::vector<C2DPoint>& PWLinearPath, T& AngleSum) const
    {
        T Length = (T)0;
        AngleSum = (T)0;

        int nSegments = (int)PWLinearPath.size() - 1;
        T PrevSegmentLength;
        for(int i = 0; i < nSegments; i++)
        {
            const C2DPoint& Point1 = PWLinearPath[i];
            const C2DPoint& Point2 = PWLinearPath[i + 1];

            T CurrentSegmentLength = Point1.GetDistanceToPoint(Point2);
            Length += CurrentSegmentLength;

            if(i > 0 && PrevSegmentLength >= 0 && CurrentSegmentLength >= 0)
            {
                // Izracunaj razliku kuteva izmedu dva susjedna segmenta
                const C2DPoint& Point0 = PWLinearPath[i - 1];
                T dx1 = Point1.x - Point0.x;
                T dy1 = Point1.y - Point0.y;
                T dx2 = Point2.x - Point1.x;
                T dy2 = Point2.y - Point1.y;

                T Acos_arg = (dx1 * dx2 + dy1 * dy2) / (PrevSegmentLength * CurrentSegmentLength);
                T Angle = acos(Acos_arg); // rezultat je u intervalu [0-pi], sto nam odgovara
                AngleSum += Angle;
            }

            PrevSegmentLength = CurrentSegmentLength;
        }

        return Length;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Replaniranje po dijelovima ravne putanje - promjena pocetnog segmenta putanje.
    // "Pocetni segment" znaci zapravo da planiranje mora poceti od krajnje tocke tog segmenta.
    // Ova funkcija trazi planer putanje negdje vani (izvan ove klase :).
    //________________________________________________________________________________________________________________________________________________
    bool ReplanPiecewiseLinearPath_NewStartLine(const std::vector<C2DPoint>& OriginalPWLinearPath, // pocetna putanja
                                                CLineParBounded NewStartLine,                      // novi startni segment putanje
                                                std::vector<C2DPoint>& NewPath                     // replanirana putanja
    )
    {
        // tudu
        // todo:

        // Trenutna implementacija pretpostavlja da nema uopce prepreka, jednostavno umjesto prve dvije tocke originalne putanje ubacimo
        // zadani pocetni pravac

        if(OriginalPWLinearPath.size() > 2)
        {
            // u slucaju da su samo tri tocke, ovo bi moglo pokvariti smjer u ciljnoj tocki
            NewPath = OriginalPWLinearPath;
            NewPath[0] = NewStartLine.StartPoint();
            NewPath[1] = NewStartLine.EndPoint();

            // Sad jos provjerimo da nismo unijeli kolinearnost tocaka. Treba provjeriti drugu tocku
            if(NewPath[1].PointsCollinear(NewPath[0], NewPath[2]))
            {
                // Izbacimo srednju tocku
                NewPath.erase(NewPath.begin() + 1);
            }
        }
        else
        {
            // Imamo samo dvije tocke (manje od toga ne moze ni biti) - umjesto prve tocke, stavimo pocetnu tocku zadanog pravca, a zadnju
            // tocku pustimo kak je bila
            NewPath = OriginalPWLinearPath;
            const C2DPoint StartPoint = NewStartLine.StartPoint();
            NewPath.insert(NewPath.begin(), StartPoint);
            NewPath[1] = NewStartLine.EndPoint();
        }

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Inicijalizacija odabira skaliranja izravnavajuce klotoide za algoritam ConnectCircleLine3C
    //________________________________________________________________________________________________________________________________________________
    bool InitializePickScaling()
    {
        m_iPickScalingIteration = 0;

        // neka primitivna inicijalizacija odabira skaliranja - u nedostatku bolje i ova bu ok
        T c_max = (T)70;
        m_C1 = 1 / sqrt(c_max);
        m_C2 = (T)0.5;

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Odabira skaliranja izravnavajuce klotoide za algoritam ConnectCircleLine3C.
    // Prvo treba inicijalizirati odabir sa InitializePickScaling().
    //________________________________________________________________________________________________________________________________________________
    bool PickScaling(T& C)
    {
        if(m_iPickScalingIteration >= m_PickScalingMaxIters)
        {
            assert(0); // premasili smo maks broj iteracija. Mozda nije napravljena inicijalizacija?
            return false;
        }

        C = m_C1 + (m_C2 - m_C1) * m_iPickScalingIteration / (m_PickScalingMaxIters - 1);

        m_iPickScalingIteration++;

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Provjera koliko daleko mozemo ispaliti zraku u smjeru Line i iz tocke koja je spremljena u Line
    //________________________________________________________________________________________________________________________________________________
    bool ShootRay(const CLineParRay& Ray,
                  T MaxDistance,  // ne provjeravamo dalje od ove udaljenosti
                  T& FreeDistance // koliko daleko je slobodan prostor (bez prepreka). Bit ce uvijek Distance<=MaxDistance
                  ) const
    {
        while(1)
        {
            if(!m_pOriginalPWPath)
                break;

            if(m_pOriginalPWPath->size() < 3)
                break;

            // Pronadi sjeciste zrake s drugim segmentom originalne putanje

            CLinePar Line2;
            Line2.LineFromPoints((*m_pOriginalPWPath)[1], (*m_pOriginalPWPath)[2]);

            T x, y;
            if(!Ray.GetIntersection(Line2, x, y))
                break;

            FreeDistance = Ray.p0.GetDistanceToPoint(x, y);

            FreeDistance = std::min(FreeDistance, MaxDistance);

            return true;
        }

        // Tudu
        FreeDistance = MaxDistance; // nije implementirano, pa pretpostavljamo da je sve slobodno zasad

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Treba pozvati prije planiranja putanje - cisti staru putanju i spremnike.
    //________________________________________________________________________________________________________________________________________________
    void ClearPath()
    {
        // izbrisi prvo putanju
        m_SmoothedPath.RemoveAllTrajs();

        // a onda vrati sve iteratore na pocetak (to je kao da smo ih ispraznili)

        m_LineBuffer_FirstFree = m_LineBuffer.begin();
        m_ArcBuffer_FirstFree = m_ArcBuffer.begin();
        m_ClothoidBuffer_FirstFree = m_ClothoidBuffer.begin();
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca novi linijski segment putanje (prvo prosiruje buffer)
    //________________________________________________________________________________________________________________________________________________
    CLinePathProfile& GiveMeLinePathSegment()
    {
        if(m_LineBuffer_FirstFree == m_LineBuffer.end())
        {
            m_LineBuffer_FirstFree--; // treba se vratiti natrag, i onda opet naprijed, jer inace ce biti invalid pointer
            m_LineBuffer.resize(m_LineBuffer.size() * 2); // povecaj duplo za ubuduce
            m_LineBuffer_FirstFree++;
        }

        CLinePathProfile& ReturnSegment = *m_LineBuffer_FirstFree;
        m_LineBuffer_FirstFree++;

        return ReturnSegment;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca novi kruzni segment putanje (prvo prosiruje buffer)
    //________________________________________________________________________________________________________________________________________________
    CArcPathProfile& GiveMeCircularArcPathSegment()
    {
        if(m_ArcBuffer_FirstFree == m_ArcBuffer.end())
        {
            m_ArcBuffer_FirstFree--;                    // treba se vratiti natrag, i onda opet naprijed, jer inace ce biti invalid pointer
            m_ArcBuffer.resize(m_ArcBuffer.size() * 2); // povecaj duplo za ubuduce
            m_ArcBuffer_FirstFree++;
        }

        CArcPathProfile& ReturnSegment = *m_ArcBuffer_FirstFree;
        m_ArcBuffer_FirstFree++;

        return ReturnSegment;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca novi klotoidni segment putanje (prvo prosiruje buffer)
    //________________________________________________________________________________________________________________________________________________
    CClothoidPathProfile& GiveMeClothoidPathSegment()
    {
        if(m_ClothoidBuffer_FirstFree == m_ClothoidBuffer.end())
        {
            m_ClothoidBuffer_FirstFree--; // treba se vratiti natrag, i onda opet naprijed, jer inace ce biti invalid pointer
            m_ClothoidBuffer.resize(m_ClothoidBuffer.size() * 2); // povecaj duplo za ubuduce
            m_ClothoidBuffer_FirstFree++;
        }

        CClothoidPathProfile& ReturnSegment = *m_ClothoidBuffer_FirstFree;
        m_ClothoidBuffer_FirstFree++;

        return ReturnSegment;
    }

    inline CTrajCompositePathProfile& SmoothedPath() { return m_SmoothedPath; }

protected:
    CTrajCompositePathProfile m_SmoothedPath; // glatka putanja koja spaja pocetno stanje i ostatak putanje

    // Originalna po dijelovima ravna putanja (postavljena prilikom poziva algoritma).
    // Varijabla je vazeca samo za vrijeme poziva algoritma.
    const std::vector<C2DPoint>* m_pOriginalPWPath;

    // Neki parametri za biranje skaliranja
    int m_iPickScalingIteration;                 // u tijeku koje iteracije odabira skaliranja se nalazimo (0=prva)
    static const int m_PickScalingMaxIters = 5;  // koliko je iteracija dopusteno kod odabira skaliranja u ConnectCircleLine3C
    static const int m_3CLineLengthMaxIters = 1; // koliko je iteracija dopusteno kod odabira duljine ravne linije u ConnectCircleLine3C
    T m_C1;                                      // pomocna varijabla za odabir skaliranja
    T m_C2;

    // Nakon algoritma za spajanje i odvajanje od putanje, preostat ce po dijelovima ravna putanja u sredini, koju zatim treba izgladiti.
    // A ta putanja ce biti podskup pocetne po dijelovima ravne putanje, osim u slucaju algoritma ConnectCircleLine3C, koji moze izmijeniti
    // originalnu putanju.
    std::vector<C2DPoint> m_PWLPathToSmooth;

    // A ovdje si definiramo buffer s komponentama putanje za koje ne znamo unaprijed koliko cemo ih trebati, pa koristimo list.
    // vector ne valja, jer kod realociranja svi pointeri postaju nevazeci.
    std::list<CLinePathProfile> m_LineBuffer;
    std::list<CArcPathProfile> m_ArcBuffer;
    std::list<CClothoidPathProfile> m_ClothoidBuffer;

    // iteratori na prvi slobodni (neiskoristeni) element liste
    typename std::list<CLinePathProfile>::iterator m_LineBuffer_FirstFree;
    typename std::list<CArcPathProfile>::iterator m_ArcBuffer_FirstFree;
    typename std::list<CClothoidPathProfile>::iterator m_ClothoidBuffer_FirstFree;
};

//=================================================================================================================================================================
//
// Jednostavni prirucni replaner putanje (u nedostatku boljeg). Samo za privremeno koritenje i za napraviti eksperimente.
// Replanira putanju ovisno o tome gdje smo u odnosu na originalnu putanju. Pritom uopce ne gleda prepreke, i ne radi za sve slucajeve, vec
// samo one "lijepe".
//
//=================================================================================================================================================================
template<typename T>
class CPathReplanning
{
public:
    std::vector<C2DPoint> m_OriginalPath;  // originalna po dijelovima ravna putanja
    std::vector<C2DPoint> m_ReplannedPath; // originalna po dijelovima ravna putanja

    //________________________________________________________________________________________________________________________________________________
    //
    // Replanira putanju ovisno o tome gdje smo u odnosu na originalnu putanju. Pritom uopce ne gleda prepreke.
    // Nedostatak ovog algoritma (osim svih ostalih :) je taj da moze imati vrlo kratke segmente, pa cak i one duljine nula
    //________________________________________________________________________________________________________________________________________________
    bool
    ReplanPath(const typename C2DTrajectoryBase::S2DTrajPoint& CurrentState // trenutno stanje zadano pozicijom i kutem (ostalo nije vazno)
    )
    {
        // Utvrdujemo na kojem segmentu originalne putanje smo trenutno

        int iClosestSegment = ClosestSegment(CurrentState.x, CurrentState.y);
        const C2DPoint& Point1 = m_OriginalPath[iClosestSegment];
        const C2DPoint& Point2 = m_OriginalPath[iClosestSegment + 1];
        CLineParBounded CurrentSegment;
        CurrentSegment.LineFromPoints(Point1, Point2);

        // Utvrdimo dal smo na prvoj ili drugoj polovici tog segmenta

        int iCloserPoint = CurrentSegment.CloserEndPoint(CurrentState.x, CurrentState.y);

        // Ako smo na prvoj polovici segmenta, pokusaj naci sjeciste trenutne tangente po kojoj se gibamo s trenutnim segmentom.

        // Tangenta na trenutno gibanje, pocetna tocka je u trenutnoj poziciji
        CLineParRay MotionTangent;
        MotionTangent.LineFromDirectionAndPoint(CurrentState.Angle, CurrentState.x, CurrentState.y);

        bool bClosestSegmentUsed = false;

        if(iCloserPoint == 0)
        {
            T xIntersection, yIntersection;
            if(CurrentSegment.GetIntersection(MotionTangent, xIntersection, yIntersection))
            {
                m_ReplannedPath.clear();
                C2DPoint Point(CurrentState.x, CurrentState.y);
                m_ReplannedPath.push_back(Point);
                Point.SetPoint(xIntersection, yIntersection);
                m_ReplannedPath.push_back(Point);
                m_ReplannedPath.insert(m_ReplannedPath.end(), m_OriginalPath.begin() + iClosestSegment + 1, m_OriginalPath.end());

                bClosestSegmentUsed = true;
            }
        }

        bool bSecondSegmentUsed = false;
        if(!bClosestSegmentUsed)
        {
            // Ne koristi najblizi segment za trazenje sjecista, vec onaj sljedeci

            // Provjerimo dal sljedeci segment uopce postoji (ako ne, onda smo na zadnjem)
            if((int)m_OriginalPath.size() > iClosestSegment + 2)
            {
                const C2DPoint& Point3 = m_OriginalPath[iClosestSegment + 2];

                CLineParRay NextSegment;
                NextSegment.LineFromPoints(Point3, Point2); // smjer zrake postavljamo od krajnje tocke prema pocetnoj

                // Nadi presjeciste zrake iz tangente robota i zrake putanje
                T xIntersection, yIntersection;
                if(MotionTangent.GetIntersection(NextSegment, xIntersection, yIntersection))
                {
                    m_ReplannedPath.clear();
                    C2DPoint Point(CurrentState.x, CurrentState.y);
                    m_ReplannedPath.push_back(Point);
                    Point.SetPoint(xIntersection, yIntersection);
                    m_ReplannedPath.push_back(Point);
                    m_ReplannedPath.insert(m_ReplannedPath.end(), m_OriginalPath.begin() + iClosestSegment + 2, m_OriginalPath.end());

                    bSecondSegmentUsed = true;
                }
            }
        }

        if(!(bClosestSegmentUsed || bSecondSegmentUsed))
        {
            // Nije pronadeno sjeciste jer smo vec bili na zadnjem segmentu, ovaj algoritam tu nije u stanju nis napraviti nek samo koristi
            // originalnu putanju
            m_ReplannedPath.clear();
            C2DPoint Point(CurrentState.x, CurrentState.y);
            m_ReplannedPath.push_back(Point);
            m_ReplannedPath.insert(m_ReplannedPath.end(), m_OriginalPath.begin() + iClosestSegment + 1, m_OriginalPath.end());
        }

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca indeks (pocevsi od nule) na kojem trenutno "stojimo". Tj. vraca segment koji je najblizi zadanoj poziciji (x,y).
    //________________________________________________________________________________________________________________________________________________
    int ClosestSegment(T x, T y) const
    {
        int iClosestSegment = -1;
        T Dist2Min = std::numeric_limits<T>::max();

        int nSegments = (int)m_OriginalPath.size() - 1;
        for(int i = 0; i < nSegments; i++)
        {
            const C2DPoint& Point1 = m_OriginalPath[i];
            const C2DPoint& Point2 = m_OriginalPath[i + 1];
            CBoundLineImp Segment;
            Segment.LineFromPoints(Point1, Point2);

            T xClosest, yClosest; // nepotrebno ovdje
            T Dist2 = Segment.GetDistance2ToPoint(x, y, xClosest, yClosest);

            if(Dist2 < Dist2Min)
            {
                Dist2Min = Dist2;
                iClosestSegment = i;
            }
        }

        return iClosestSegment;
    }
};

#endif
