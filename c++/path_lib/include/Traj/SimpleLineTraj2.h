#ifndef _Simple_Line_Traj2_h_
#define _Simple_Line_Traj2_h_

#include "2DTrajectoryBase.h"

#include <cmath>

//=======================================================================================================================================
//
// Jednostavna trajektorija konstantnom brzinom po pravcu zadanom brzinom bez ubrzanja/usporavanja.
//
//=======================================================================================================================================

template<class T>
class CSimpleLineTraj2 : public C2DTrajectoryBase
{
public:
    CSimpleLineTraj2()
    {
        m_t0 = 0;
        m_x0 = 0;
        m_y0 = 0;
        m_Angle0 = 0;

        m_Distance = 0;
        m_v = 0;
    };

    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        T Time = pTrPoint->Time;

        T Velocity = m_v;
        T Disp = m_Disp0 + Velocity * (Time - m_t0);

        pTrPoint->x = m_x0 + Disp * m_Cos;
        pTrPoint->y = m_y0 + Disp * m_Sin;
        pTrPoint->Angle = m_Angle0;
        pTrPoint->Velocity = Velocity;
        pTrPoint->Accel = 0;
        pTrPoint->Disp = Disp;
        pTrPoint->Curv = 0;
        return true;
    }

    bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        T Disp = pTrPoint->Disp;

        T Velocity = m_v;
        T Time = m_t0 + (Disp - m_Disp0) / Velocity;

        pTrPoint->x = m_x0 + Disp * m_Cos;
        pTrPoint->y = m_y0 + Disp * m_Sin;
        pTrPoint->Angle = m_Angle0;
        pTrPoint->Curv = 0;
        pTrPoint->Time = Time;
        pTrPoint->Velocity = Velocity;
        pTrPoint->Accel = 0;

        return true;
    }

    struct SParms1
    {
        T StartTime;
        T StartDisp;
        T x0;
        T y0;
        T TotalDisp;
        T LineAngle;
        T v;
    };

    bool SetParms(SParms1* pParms)
    {
        m_t0 = pParms->StartTime;
        m_Disp0 = pParms->StartDisp;
        m_x0 = pParms->x0;
        m_y0 = pParms->y0;
        m_Distance = pParms->TotalDisp;
        m_Angle0 = pParms->LineAngle;
        m_v = pParms->v;

        m_Cos = (T)cos(m_Angle0);
        m_Sin = (T)sin(m_Angle0);

        return CheckParms();
    };

private:
    bool CheckParms()
    {
        // Brzina ne moze biti negativna
        if(m_v <= 0)
        {
            assert(0);
            return false;
        }

        // Duljina puta ne moze biti negativna
        if(m_Distance <= 0)
        {
            assert(0);
            return false;
        }

        return true;
    };

    T GetBeginTime() const { return m_t0; };
    T GetDeltaTime() const { return m_t0 + m_Distance / m_v; }
    void SetBeginTime(T BeginTime) { m_t0 = BeginTime; };

    T GetBeginDisp() const { return m_Disp0; };
    T GetEndDisp() const { return m_Disp0 + m_Distance; };
    void SetBeginDisp(T BeginDisp) { m_Disp0 = BeginDisp; };

    // Parametri
private:
    // Po�etni uvjeti
    T m_t0;    // Po�etno vrijeme (obi�no 0)
    T m_Disp0; // Po�etna udaljenost (obi�no 0)
    T m_x0;    // Po�etna to�ka
    T m_y0;
    T m_Angle0; // Kut pravca

    T m_Distance; // Ukupna duljina prevaljenog puta po trajektoriji (>0)
    T m_v;        // Stacionarna brzina (>0)

private:
    // Pomocne varijable za ubrzavanje racunanja
    T m_Cos;
    T m_Sin;
};

/*
Primjer upotrebe:

#include "SimpleLineTraj2.h"

CSimpleLineTraj2 Traj;

typedef CSimpleLineTraj2::SParms1 SLnParms1;
SLnParms1 Lp1;
Lp1.StartTime = 0;
Lp1.StartDisp = 1;
Lp1.x0 = 10;
Lp1.y0 = 20;
Lp1.TotalDisp = 3;
Lp1.LineAngle = 3.14f;
Lp1.v = 2;
if(!Traj.SetParms(&Lp1))
    return false;

typedef CSimpleLineTraj2::S2DTrajPoint STrajPt;

STrajPt TrajPt1;
TrajPt1.Disp = 1;
Traj.GetPointByDisp(&TrajPt1);

STrajPt TrajPt2;
TrajPt2.Time = 2;
Traj.GetPointByTime(&TrajPt2);
*/

#endif // _Simple_Line_Traj_h_
