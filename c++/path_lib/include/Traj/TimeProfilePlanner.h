#ifndef _TimeProfilePlanner_h_
#define _TimeProfilePlanner_h_

#include "MobileRobot.h"
#include "TrajPathProfile.h"
#include "TrajTimeProfile.h"
#include "UniformTimeProfiles.h"

//=================================================================================================================================================================
//
// Bazna klasa za planere vremenskog profila trajektorije
//
//=================================================================================================================================================================
template<typename T>
class CTimeProfilePlannerBase : public C2DTrajectoryDataTypes
{
public:
    CTimeProfilePlannerBase() { m_bInvert = false; }

    virtual CTrajTimeProfile* PlanTimeProfile(const CTrajCompositePathProfile* pPathProfile,         // [in] putanja
                                              const typename CMobileRobot::SRobotState* pBeginState, // [in] pocetno stanje
                                              const typename CMobileRobot::SRobotState* pEndState,   // [in] konacno stanje
                                              const CMobileRobot* pRobot // [in] podaci o robotu (ako su potrebni)
    )
    {
        //return false;
    };

    virtual bool IsValid() const { return true; }

    // Za invertiranje
    void Invert(bool bInvert) { m_bInvert = bInvert; }
    bool IsInverted() const { return m_bInvert; }

protected:
    bool m_bInvert; // Dal planirana trajektorija mora biti invertirana
};

#endif // _TimeProfilePlanner_h_
