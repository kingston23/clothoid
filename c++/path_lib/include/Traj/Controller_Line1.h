#ifndef _Controller_Line1_h_
#define _Controller_Line1_h_

#include "Geom/Line.h"
#include "Math/AngleRange.h"
#include "Math/Numeric.h"
#include "Controller_Base.h"

#include <algorithm>
#include <assert.h>
#include <cmath>

using namespace MathMB;

//=======================================================================================================================================
//
// Klasa za kontroler za vodenje robota po pravcu. Robot ce se prvo priblizavati pravcu, a kad stigne do njega na udaljenost manju od
// LookAheadDistance tada se pocinje orijentirati u smjeru pravca.
//
//=======================================================================================================================================

template<typename T>
class CController_Line1 : public CController_Base<T>
{
public:
    CController_Line1() { m_Name = CN_Line1; }

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Parametri koji se moraju zadavati za svaki novi sempl
    //_____________________________________________________________________________________________________________________________________________________________________
    struct SSampleParms
    {
        T A, B, C; // koeficijenti smjera param jedn. pravca po kojem se robot treba gibati, vektor smjera je i=B, j=-A

        T xRobot, yRobot; // Tu je robot
        T angRobot;
    };

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Daje referentne brzine robota
    //_____________________________________________________________________________________________________________________________________________________________________
    bool GetRefValues(SSampleParms* pSp, // IN
                      T& vRef, T& wRef   // OUT
    )
    {
        assert(pSp);

        if(!IsValid())
        {
            assert(0);
            return false;
        }

        CLineImp<T> Line;
        Line.A = pSp->A;
        Line.B = pSp->B;
        Line.C = pSp->C;

        T DirToLookAheadPoint = Line.GetDirectionToLookAheadPoint(pSp->xRobot, pSp->yRobot, Parms.LookAheadDistance);

        T dAngle = CAngleRange<T>::Range_MinusPI_PI(DirToLookAheadPoint - pSp->angRobot); // pogreska kuta

        T Cos = cos(dAngle);
        T Sin = sin(dAngle);
        T signCos = Cos >= 0 ? (T)1 : (T)-1;
        T signSin = Sin >= 0 ? (T)1 : (T)-1;
        T LinVelocityParam = Cos * Cos * signCos;
        T AngVelocityParam = Sin * Sin * signSin;

        vRef = Parms.vDesired * LinVelocityParam;
        wRef = 2 * Parms.vDesired * AngVelocityParam / Parms.DistWheels;

        return true;
    }

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Parametri kontrolera
    //_____________________________________________________________________________________________________________________________________________________________________
    struct SParms
    {
        T LookAheadDistance;

        T vDesired; // Zeljena brzina kojom robot vozi prema cilju. S tom brzinom bi isal ako mu je trajektorija ravna.

        T DistWheels; // Razmak izmedu kotaca za diferencijalni pogon
    };

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje novih parametara kontrolera
    //_____________________________________________________________________________________________________________________________________________________________________
    bool SetParms(SParms* pNewParms)
    {
        Parms = *pNewParms;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        return true;
    }

    bool IsValid() const
    {
        if(!IsValidParms())
            return false;

        return true;
    }

    bool IsValidParms() const
    {
        if(Parms.LookAheadDistance <= 0)
            return false;

        if(Parms.vDesired <= 0)
            return false;

        if(Parms.DistWheels <= 0)
            return false;

        return true;
    }

    SParms Parms;
};

#endif
