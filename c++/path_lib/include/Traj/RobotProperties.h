#pragma once

template<class T>
class CRobotProperties
{
    // Pogledati tipove
    enum ERobotType
    {
        UndefinedRobot,
        DifferentialDriveRobot,
    };

    ERobotType GetRobotType();

    // Apsolutne velicine - maksimalno moguci iznosi pojedine velicine ovisno o
    virtual T GetMaxSpeed();
    virtual T GetMaxAccel();
    virtual T GetMaxAngSpeed();
    virtual T GetMaxAngAccel();
    virtual T GetMaxCurv();

    // Limiti ovisno o zadanoj tocki trajektorije
    T GetMaxSpeed(STrajPoint* pTrPoint);
    T GetMaxAccel(STrajPoint* pTrPoint);
};
