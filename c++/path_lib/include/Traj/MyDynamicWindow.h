
#ifndef MYDYNAMIC_WINDOW_H
#define MYDYNAMIC_WINDOW_H

#include "Geom/2DGeomMap.h"

using namespace Geom;

class CMyDynamicWindow
{
public:
    // Struktura za mobilnog robota
    struct MB_RB
    {
        // Polozaj, orijentacija i kompas
        double x, y, th, k;

        // Brzine
        double v, w;

        // Zaglavljenja motora
        char lms, rms;
    };

    // Struktura za referentnu brzinu
    struct MB_setpoint
    {
        double setpoint_v;
        double setpoint_w;
    };

    // Definicija tocke - koristi se za prepreke
    struct MB_tocka
    {
        double x, y;
    };

    // Podrska za laser
    struct tocka_T : public MB_tocka
    {
        int valid;
    };

    typedef struct tocka_T tT;

    double m_v;  // Kojom brzinom vozimo
    double m_dv; // Akceleracija
    int m_SelfID;
    int m_IgnoreObs;

    double m_DelayInSamples; // Koliko semplova kasne reference robota

    static const double PI;

    C2DGeomMap* m_pStaticMap;
    C2DGeomMap* m_pDynamicMap;

    double T; // to je ciklus

    double m_Precision; // Preciznost pozicioniranja u metrima

    double m_vRefPrev; // Prethodna referentna brzina robota

    double RR; // Radijus robota [mm]

    // SECURITY DISTANCE za robota
    double SC1; // kod najmanjih brzina smije biti x m od prepreke
    double SC2; // a kod najvecih brzina smije biti x m od prepreke

    MB_RB RB; // instanca stanja robota

    MB_setpoint SP; // instanca setpoint vrijednosti brzina

    double m_StopDistance; // Minimalni zaustavni put

    double global_goal_x, global_goal_y;
    double global_goal_fi;

    double K; // Pojacanje regulatora kuta

    bool m_bSniffObstaclesNearGoal; // Dal da ispituje prepreke blizu cilja ili samo udri

public:
    CBoundLineImp m_Line;

public:
    void Setup(double T_, double v, double dv, double rr, double sc1, double sc2, double K_, double Precision, double vRefPrev)
    {
        m_v = v;
        m_dv = dv;
        RR = rr;
        SC1 = sc1;
        SC2 = sc2;
        K = K_;
        T = T_;
        m_Precision = Precision;
        m_vRefPrev = vRefPrev;
    }

    void SetRobotState(double x, double y, double ang, double v, double w)
    {
        RB.x = x;
        RB.y = y;
        RB.th = ang;
        RB.v = v;
        RB.w = w;
    }

    void SetStaticMap(C2DGeomMap* pMap) { m_pStaticMap = pMap; }

    void SetDynamicMap(C2DGeomMap* pMap) { m_pDynamicMap = pMap; }

    void SetSelfID(int SelfID) { m_SelfID = SelfID; }

    void Reset();

    void SetIgnoreObs(int IgnoreID) { m_IgnoreObs = IgnoreID; }

    void IgnoreObstaclesNearGoal(bool bIgnore) { m_bSniffObstaclesNearGoal = !bIgnore; }

    CMyDynamicWindow();
    ~CMyDynamicWindow(){};

    bool Sekvenca_izvodjenja();

    double GetRefVelocitiesV() { return this->SP.setpoint_v; };
    double GetRefVelocitiesW() { return this->SP.setpoint_w; };

    void GetGoal(double& x, double& y)
    {
        x = global_goal_x;
        y = global_goal_y;
    }

    void SetGlobalGoalX(double global_goal_x) { this->global_goal_x = global_goal_x; };
    void SetGlobalGoalY(double global_goal_y) { this->global_goal_y = global_goal_y; };
    void SetGlobalGoalFi(double fi) { global_goal_fi = fi; }

    bool IsNearGoal(double d);
    bool IsOrientationOK(double AngTol);

    bool ObstacleFreeMoving();
    bool StaticObstacleFreeMoving();
    bool DynamicObstacleFreeMoving();

    double StaticObstacleMinDistance();

protected:
    // ubacivanje trenutnog stanja robota u strukturu
    void Azuriraj_stanje_robota();
    // Definicija dinamickog prozora
    void Odredi_dinamicki_prozor();
    // Funkcija za proracun kruznog luka
    inline void Kruzni_luk();
    void Prohodnost();
    void OrientacijaGoal(double goal_x, double goal_y);
    void OcjenaLinearnaBrzina();

    void DoprinosProhodnost();
    void DoprinosOrientacijaGoal();
    void DoprinosLinearnaBrzina();
    // Trazenje optimalnog para
    void Optimalni_par();

    inline void MinimalniZaustavniPut();

    bool GetFreeLine();
};

#endif
