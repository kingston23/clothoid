#ifndef _Controller_CMU98_h_
#define _Controller_CMU98_h_

#include "Controller_Base.h"
#include "Math/Numeric.h"

#include <algorithm>
#include <assert.h>
#include <cmath>

using namespace MathMB;

#ifdef min
#undef min
#endif

//=======================================================================================================================================
//
// Klasa za kontroler za navigaciju robota opisan u clanku od CMU-a na ICRA99 (Motion Control in Dynamic Mulit-Robot Environtments)
//
//=======================================================================================================================================

template<typename T>
class CController_CMU98 : public CController_Base<T>
{
public:
    CController_CMU98() { m_Name = CN_CMU98; }

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Parametri koji se moraju zadavati za svaki novi sempl
    //_____________________________________________________________________________________________________________________________________________________________________
    struct SSampleParms
    {
        T xTarget, yTarget; // Tu ocemo da robot dojde
        T angTarget;

        T xRobot, yRobot; // Tu je robot
        T angRobot;
    };

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Daje referentne brzine robota
    //_____________________________________________________________________________________________________________________________________________________________________
    bool GetRefValues(SSampleParms* pSp, // IN
                      T& vRef, T& wRef   // OUT
    )
    {
        assert(pSp);

        if(!IsValid())
        {
            assert(0);
            return false;
        }

        T dxToTarget = pSp->xTarget - pSp->xRobot;
        T dyToTarget = pSp->yTarget - pSp->yRobot;

        T DirToTarget = (T)atan2(dyToTarget, dxToTarget);

        T DistToTarget = (T)sqrt(dxToTarget * dxToTarget + dyToTarget * dyToTarget);

        T Ang1 = atan(Parms.Clearance / DistToTarget);       // Uvijek pozitivno, ide od 0 do pi/2 kak se priblizavamo
        T Ang2 = CNum<T>::Abs(DirToTarget - pSp->angTarget); // Pozitivno zbog abs

        T CorrectionAngle = std::min(Ang1, Ang2); // Korekcija kuta kako bi se postiglo zeljenu orijentaciju u cilju
        if(DirToTarget < 0)
            CorrectionAngle = -CorrectionAngle;

        T DirToReachTarget = DirToTarget + CorrectionAngle;

        T dAngle = DirToReachTarget - pSp->angRobot;

        T Cos = cos(dAngle);
        T Sin = sin(dAngle);
        T signCos = Cos >= 0 ? (T)1 : (T)-1;
        T signSin = Sin >= 0 ? (T)1 : (T)-1;
        T LinVelocityParam = Cos * Cos * signCos;
        T AngVelocityParam = Sin * Sin * signSin;

        vRef = Parms.vDesired * LinVelocityParam *
               std::min((T)(DistToTarget / Parms.Clearance), (T)(1)); // usporavamo kak se priblizavamo cilju
        wRef = Parms.vDesired * AngVelocityParam * 20;

        return true;
    }

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Parametri kontrolera
    //_____________________________________________________________________________________________________________________________________________________________________
    struct SParms
    {
        T Clearance; // Kod kruzenja oko cilja radi postizanja orijentacije

        T vDesired; // Zeljena brzina kojom robot vozi prema cilju. S tom brzinom bi isal ako mu je trajektorija ravna.

        T DistWheels; // Razmak izmedu kotaca za diferencijalni pogon
    };

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje novih parametara kontrolera
    //_____________________________________________________________________________________________________________________________________________________________________
    bool SetParms(SParms* pNewParms)
    {
        Parms = *pNewParms;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        return true;
    }

    bool IsValid() const
    {
        if(!IsValidParms())
            return false;

        return true;
    }

    bool IsValidParms() const
    {
        if(Parms.Clearance <= 0)
            return false;

        if(Parms.vDesired <= 0)
            return false;

        if(Parms.DistWheels <= 0)
            return false;

        return true;
    }

private:
    SParms Parms;
};

#endif
