#ifndef _MaxSpeedCircleLineTimeProfile_h_
#define _MaxSpeedCircleLineTimeProfile_h_
#pragma warning(disable : 4786)

#include "ArcPathProfile.h"
//#include "Log/Log.h"
#include "TimeProfilePlanner.h"

#include <algorithm>
#include <list>
#include <malloc.h>
#include <math.h>

//=================================================================================================================================================================
//
// Proracunava najbrzi moguci vremenski profil za dani prostorni profil koji se
// smije sastojati iskljucivo od krugova i pravaca.
//
//=================================================================================================================================================================
template<typename T>
class CMaxSpeedCircleLineTimeProfile : public CTimeProfilePlannerBase<T>
{
protected:
    // Struktura s podacima o maksimalnoj brzini na pojedinom dijelu staze
    struct SMaxSpeedData
    {
        T Dist;     // Duljina pojedinog dijela staze [0,inf>
        T MaxSpeed; // Maks apsolutna brzina na tom dijelu staze [0,inf>
    };

public:
    //---------------------------------------------------------------------------
    // Defaultni konstruktor
    //---------------------------------------------------------------------------
    CMaxSpeedCircleLineTimeProfile()
    {
        // Rezerviramo pocetnu memoriju za stanja
        try
        {
            m_ConstSpeedProfilesBuffer.resize(4);
            m_ConstAccelProfilesBuffer.resize(8);
        }
        catch(...)
        {
        }
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Planiranje vremenskog profila trajektorije
    //____________________________________________________________________________________________________________________________________________________________________________________
    CTrajTimeProfile* PlanTimeProfile(const CTrajCompositePathProfile* pPathProfile,         // in
                                      const typename CMobileRobot::SRobotState* pBeginState, // in
                                      const typename CMobileRobot::SRobotState* pEndState,   // in
                                      const CMobileRobot* pRobot                             // in
    )
    {
        assert(pPathProfile && pBeginState && pEndState);
        assert(IsValidParms());

        m_TimeProfile.RemoveAllTrajs();

        // Postavi iteratore na profile na pocetak
        assert(m_ConstAccelProfilesBuffer.size() > 0);
        assert(m_ConstSpeedProfilesBuffer.size() > 0);

        m_pCurAccelProfile = m_ConstAccelProfilesBuffer.begin();
        m_pCurSpeedProfile = m_ConstSpeedProfilesBuffer.begin();

        // Brojaci koristenih profila
        int nConstSpeedProfiles = 0;
        int nConstAccelProfiles = 0;

        int nPathSegments = pPathProfile->GetNumTrajs();

        // Alociraj na stogu strukture s podacima o maks brzini na pojedinom segmentu staze.
        // Potrebne su maks dvije za svaki krug/pravac staze.
        std::vector<SMaxSpeedData> MaxSpeedData;

        // Proskeniraj stazu i izracunaj maks. dopustene brzine za svaki dio staze
        if(!GetMaxSpeeds(pPathProfile, pEndState->Velocity, MaxSpeedData))
            return nullptr;

        int nMaxSpeedSegments = (int)MaxSpeedData.size();
        assert(nMaxSpeedSegments >= nPathSegments);

        // Ak je trajektorija invertirana, sve sto trebamo napraviti je da promjenimo predznak poc brzine, a sve ostalo ostaje isto
        T CurSpeed;
        if(!IsInverted())
        {
            CurSpeed = pBeginState->Velocity;
            CurSpeed += 0.1f; // Osim toga damo mu malo svunga, jer inace (zbog suma u mjerenju brzine) robot jako lose krece kod svakog
                              // replaniranja trajektorije
        }
        else
            CurSpeed = -pBeginState->Velocity;

        for(int iPathSegment = 0; iPathSegment < nMaxSpeedSegments; iPathSegment++)
        {
            // Pokusavaj ubrzavati/usporavati do trenutne maks brzine
            T SegMaxSpeed = MaxSpeedData[iPathSegment].MaxSpeed;
            assert(SegMaxSpeed >= 0);
            T SegMaxSpeed_2 = SegMaxSpeed * SegMaxSpeed;
            T SegDist = MaxSpeedData[iPathSegment].Dist;
            assert(SegDist >= 0);

            if(SegDist == 0)
            {
                continue; // Ovdje ne mozemo ni ubrzavati ni usporavati ni nista
            }

            // Kroz svaki segment se gibamo sa maksimalno 3 razlicita profila brzine
            // (ubrzanje, konst brzina, usporavanje)

            //-----------------------------
            // Prvi profil.
            // Ubrzavanje prema maks. brzini (ili usporavanje ako je trenutna brzina vec
            // veca od maksimalne).

            T Accel1;
            T BeginSpeed1;
            T dDisp1;
            T dDist1;

            BeginSpeed1 = CurSpeed;
            T BeginSpeed1_2 = BeginSpeed1 * BeginSpeed1;

            if(BeginSpeed1 > SegMaxSpeed)
            {
                // Pocetna brzina je veca od maksimalno dopustene na segmentu (desi se :)
                // Zato usporavamo (ovo je iznimka kada je prvi profil usporavanje, a ne ubrzanje)
                Accel1 = -m_Parms.MaxAccel;

                dDist1 = (SegMaxSpeed_2 - BeginSpeed1_2) * T(0.5) / Accel1;
                assert(dDist1 > 0);
                dDisp1 = dDist1; // Jer je brzina cijelo vrijeme pozitivna
            }
            else if(BeginSpeed1 == SegMaxSpeed)
            {
                // Nije potreban prvi profil (ne treba ubrzavati)
                Accel1 = 0;
                dDist1 = 0;
                dDisp1 = 0;
            }
            else
            {
                // Ubrzavanje
                Accel1 = m_Parms.MaxAccel;

                // Promjena udaljenosti moze biti negativna ako je pocetna brzina negativna
                dDist1 = (SegMaxSpeed_2 - BeginSpeed1_2) * (T)0.5 / Accel1;
                if(BeginSpeed1 >= 0)
                    dDisp1 = dDist1; // Ubrzanje od poz do poz brzine
                else
                    dDisp1 = (BeginSpeed1_2 + SegMaxSpeed_2) * (T)0.5 / Accel1; // Ubrzanje od neg do poz brzine
            }

            //-----------------------------
            // Treci profil - po defaultu je usporavanje od maximalne brzine prema nuli.
            // Drugi (srednji) profil racuna se na kraju.

            T BeginSpeed3 = SegMaxSpeed;
            T Accel3 = -m_Parms.MaxAccel;

            // Provjera da li je moguce usporiti od trenutne brzine do brzine nula bez da nas
            // sjebe ogranicenje brzine.
            T dDist3 = 0;    // Za pocetak pretpostavljamo da uopce ne treba usporavati
            T CheckDist = 0; // Ukupna duljina svih segmenata koje smo dosad provjerili

            for(int iCheckSeg = iPathSegment + 1; iCheckSeg < nMaxSpeedSegments; iCheckSeg++)
            {
                // Nadi maks. brzinu i duljinu segmenta
                T CheckSegMaxSpeed = MaxSpeedData[iCheckSeg].MaxSpeed;
                T CheckSegDist = MaxSpeedData[iCheckSeg].Dist;

                // Dodaj duljinu trenutnog segmenta ukupnoj duljini svih segmenata koji
                // su provjereni.
                CheckDist += CheckSegDist;

                // Izracunaj brzinu koja bi trebala biti na kraju 3. segmenta
                T BeginSpeed3_2 = BeginSpeed3 * BeginSpeed3; // Kvadrat
                T EndSpeed3_2 = BeginSpeed3_2 + 2 * Accel3 * dDist3;

                // Provjeri da li je brzina na kraju 3. segmenta veca od maksimalne na
                // segmentu koji se trenutno provjerava
                T CheckSegMaxSpeed_2 = CheckSegMaxSpeed * CheckSegMaxSpeed;
                if(EndSpeed3_2 > CheckSegMaxSpeed_2)
                {
                    // Imamo prekoracenje!
                    // Izracunaj za koju udaljenost treba treci profil pomaknuti ulijevo kako
                    // se nebi prekoracilo ovo ogranicenje.
                    dDist3 = (CheckSegMaxSpeed_2 - BeginSpeed3_2) * (T)0.5 / Accel3 - CheckDist;
                    // assert(dDist3>0);
                }

                // Sada izracunaj da li brzina (eventualno pomaknutog) 3. profila na kraju
                // ovog segmenta koji se provjerava pasti ispod nule. Ako da, nas posao je
                // gotov.

                T CheckSpeed_2 = BeginSpeed3_2 + 2 * Accel3 * (dDist3 + CheckDist); // Clan ispod korijena
                if(CheckSpeed_2 <= 0)
                {
                    // OK - moze se doci do brzine nula bez da se prekoraci bilo koje ogranicenje
                    break;
                }
            }

            if(dDist1 + dDist3 <= SegDist)
            {
                // Ovo bi trebalo stimati - proracunaj srednji profil sa konst brzinom
                T dDist2 = SegDist - (dDist1 + dDist3);

                // Dodaj sva tri profila
                if(dDisp1 > 0)
                {
                    if(!AddConstAccelTimeProfile(Accel1, BeginSpeed1, dDisp1, nConstAccelProfiles))
                        return nullptr;
                }

                if(dDist2 > 0)
                {
                    // Brzina>0 -> dDisp2=dDist2
                    if(!AddConstSpeedTimeProfile(SegMaxSpeed, dDist2, nConstSpeedProfiles))
                        return nullptr;
                }

                if(dDist3 > 0)
                {
                    CurSpeed = (T)sqrt(SegMaxSpeed_2 + 2 * Accel3 * dDist3);

                    // Brzina>0 -> dDisp3=dDist3
                    if(!AddConstAccelTimeProfile(Accel3, BeginSpeed3, dDist3, nConstAccelProfiles))
                        return nullptr;
                }
                else
                    CurSpeed = SegMaxSpeed;

                continue;
            }
            else
            {
                // Nece biti srednjeg profila.
                // Treba naci sjeciste (d koordinatu) prvog i treceg profila

                T Denum = 2 * (Accel1 - Accel3);
                if(Denum == 0)
                {
                    // Nema sjecista (pusiona).
                    // Sada mozemo jedino usporavati od BeginSpeed1 prema dole i to ce
                    // biti jedini profil.
                    CurSpeed = (T)sqrt(BeginSpeed1_2 + 2 * Accel3 * SegDist);

                    // Dodaj 3. profil
                    if(!AddConstAccelTimeProfile(Accel3, BeginSpeed1, SegDist, nConstAccelProfiles))
                        return nullptr;

                    continue;
                }

                T dInter = (SegMaxSpeed_2 - BeginSpeed1_2 - 2 * Accel3 * (SegDist - dDist3)) / Denum;

                if(dInter < 0)
                {
                    if(BeginSpeed1 < 0)
                    {
                        dDist1 = dInter; // Negativno
                        dDist3 = SegDist - dDist1;
                        BeginSpeed3 = (T)sqrt(BeginSpeed1_2 + 2 * Accel1 * dDist1);

                        CurSpeed = (T)sqrt(BeginSpeed3 * BeginSpeed3 + 2 * Accel3 * dDist3);

                        T dDisp1 = (BeginSpeed1_2 + BeginSpeed3 * BeginSpeed3) * (T)0.5 / Accel1; // Ubrzanje od neg do poz brzine

                        // 1. i 3. profil
                        if(dDisp1 > 0)
                        {
                            if(!AddConstAccelTimeProfile(Accel1, BeginSpeed1, dDisp1, nConstAccelProfiles))
                                return nullptr;
                        }

                        if(dDist3 > 0)
                        {
                            if(!AddConstAccelTimeProfile(Accel3, BeginSpeed3, dDist3, nConstAccelProfiles))
                                return nullptr;
                        }
                    }
                    else
                    {
                        // Opet pusiona - usporavamo od BeginSpeed1 prema dole
                        T CurSpeed_2 = BeginSpeed1_2 + 2 * Accel3 * SegDist;
                        if(CurSpeed_2 >= 0)
                            CurSpeed = (T)sqrt(CurSpeed_2);
                        else
                            CurSpeed = 0; // Moze se desiti radi numericke pogreske

                        // Dodaj 3. profil
                        if(!AddConstAccelTimeProfile(Accel3, BeginSpeed1, SegDist, nConstAccelProfiles))
                            return nullptr;
                    }

                    continue;
                }
                else if(dInter > SegDist)
                {
                    // Nema ogranicenja, pici maks. akceleracijom.
                    // Dodajemo samo prvi profil.
                    if(!AddConstAccelTimeProfile(Accel1, BeginSpeed1, SegDist, nConstAccelProfiles))
                        return nullptr;

                    CurSpeed = (T)sqrt(BeginSpeed1_2 + 2 * Accel1 * SegDist);

                    continue;
                }
                else
                {
                    dDist1 = dInter;
                    dDist3 = SegDist - dDist1;
                    assert(dDist3 >= 0);
                    BeginSpeed3 = (T)sqrt(BeginSpeed1_2 + 2 * Accel1 * dDist1);

                    CurSpeed = (T)sqrt(BeginSpeed3 * BeginSpeed3 + 2 * Accel3 * dDist3);

                    T dDisp1;
                    if(BeginSpeed1 >= 0)
                        dDisp1 = dDist1; // Ubrzanje od poz do poz brzine
                    else
                        dDisp1 = (BeginSpeed1_2 + BeginSpeed3 * BeginSpeed3) * (T)0.5 / Accel1; // Ubrzanje od neg do poz brzine

                    // Dodaj 1. i 3. profil
                    if(dDisp1 > 0)
                    {
                        if(!AddConstAccelTimeProfile(Accel1, BeginSpeed1, dDisp1, nConstAccelProfiles))
                            return nullptr;
                    }

                    if(dDist3 > 0)
                    {
                        if(!AddConstAccelTimeProfile(Accel3, BeginSpeed3, dDist3, nConstAccelProfiles))
                            return nullptr;
                    }
                }
            }
        }
        assert(nConstAccelProfiles + nConstSpeedProfiles > 0);

        if(pEndState->Velocity == (T)0.0)
        {
            // Zavrsni profil je "Miruj bre kad ti kazem!"
            if(!AddConstPosTimeProfile())
                return nullptr;
        }
        else
        {
            // Zavrsni profil je konst brzina jednaka ciljnoj (cak i po cijenu
            // skoka u brzini). Duljina nije bitna jer ako bude prekratak, onda
            // se interpolira.
            if(!AddConstSpeedTimeProfile(pEndState->Velocity, (T)0.01, nConstSpeedProfiles))
                return nullptr;
        }

        return &m_TimeProfile;
    }

protected:
    bool AddConstPosTimeProfile()
    {
        CUniformPositionTimeProfile::SParms Parms;

        // Svi parametri su skroz nevazni
        Parms.DeltaTime = (T)0.1;
        Parms.BeginDist = (T)0.0;
        Parms.BeginTime = (T)0.0;
        Parms.BeginDisp = (T)0.0;

        if(!m_ConstPosProfile.SetParms(&Parms))
            return false;

        if(!m_TimeProfile.AddTrajComponent(&m_ConstPosProfile))
            return false;

        return true;
    }

    bool AddConstSpeedTimeProfile(T Speed, T DeltaDisp, int& nConstSpeedProfiles)
    {
        if((int)m_ConstSpeedProfilesBuffer.size() <= nConstSpeedProfiles + 1)
        {
            // Povecaj buffer - uvijek mora biti barem jedan element vise
            // tako da se moze iterirati na sljedeci element u listi
            try
            {
                m_ConstSpeedProfilesBuffer.resize((nConstSpeedProfiles + 1) * 2);
            }
            catch(...)
            {
                return false;
            }
        }

        CUniformSpeedTimeProfile<T>* pConstSpeedTimeProfile = &(*m_pCurSpeedProfile);

        CUniformSpeedTimeProfile<T>::SParms2 Parms;
        Parms.BeginTime = 0;    // Nevazno
        assert(DeltaDisp >= 0); // Rikverc se ne bi smio desiti za const speed
        Parms.DeltaDisp = DeltaDisp;
        Parms.BeginDisp = 0; // Nevazno
        Parms.BeginDist = 0; // Nevazno
        Parms.Speed = Speed;

        if(!pConstSpeedTimeProfile->SetParms(&Parms))
            return false;

        if(!m_TimeProfile.AddTrajComponent(pConstSpeedTimeProfile))
            return false;

        nConstSpeedProfiles++;
        m_pCurSpeedProfile++;

        return true;
    }

    bool AddConstAccelTimeProfile(T Accel, T StartSpeed, T DeltaDisp, int& nConstAccelProfiles)
    {
        if((int)m_ConstAccelProfilesBuffer.size() <= nConstAccelProfiles + 1)
        {
            // Povecaj buffer - uvijek mora biti barem jedan element vise
            // tako da se moze iterirati na sljedeci element u listi
            try
            {
                m_ConstAccelProfilesBuffer.resize((nConstAccelProfiles + 1) * 2);
            }
            catch(...)
            {
                return false;
            }
        }

        CUniformAccelTimeProfile<T>* pConstAccelTimeProfile = &(*m_pCurAccelProfile);

        CUniformAccelTimeProfile<T>::SParms2 Parms;

        Parms.BeginTime = 0; // Nevazno
        Parms.BeginDisp = 0; // Nevazno
        Parms.BeginDist = 0; // Nevazno
        assert(DeltaDisp >= 0);
        Parms.DeltaDisp = DeltaDisp;
        Parms.BeginSpeed = StartSpeed;
        Parms.Accel = Accel;

        if(!pConstAccelTimeProfile->SetParms(&Parms))
            return false;

        if(!m_TimeProfile.AddTrajComponent(pConstAccelTimeProfile))
            return false;

        nConstAccelProfiles++;
        m_pCurAccelProfile++;

        return true;
    }

public:
    //---------------------------------------------------------------------------
    // Parametri za planer trajektorije
    //---------------------------------------------------------------------------
    struct SParms
    {
        T MaxRobotSpeed;
        T MaxAccel;
        T WheelDistance;
        // Postotak koliko se maksimalno moze usporiti u zavoju. Npr. ako
        // je zakrivljenost zavoja takva da je maks brzina 1m/s, a postotak
        // je 0.25, tada se najvise smije usporiti do 0.25m/s.
        T MaxSlowDown;
    };

    //---------------------------------------------------------------------------
    // Parametri za postavljanje i provjeru parametara
    //---------------------------------------------------------------------------
    bool SetParms(SParms* pParms)
    {
        m_Parms = *pParms;

        if(!IsValidParms())
        {
            return false;
        }

        return true;
    }

    bool IsValidParms() const
    {
        if(!(m_Parms.MaxRobotSpeed > 0 && m_Parms.MaxAccel > 0 && m_Parms.WheelDistance > 0))
        {
            assert(0);
            return false;
        }
        return true;
    }

    bool IsValid() const
    {
        if(!IsValidParms())
            return false;

        if(!m_TimeProfile.IsValid())
            return false;

        return true;
    }

protected:
    //------------------------------------------------------------------------------------------
    // Puni MaxSpeedData niz sa maksimalnim brzinama za svaki segment trajektorije.
    // FinalDesiredSpeed - ref. brzina na kraju staze - ova brzina postavlja se kao
    //		ogranicenje na kraju
    //------------------------------------------------------------------------------------------
    bool GetMaxSpeeds(const CTrajCompositePathProfile* pPathProfile, T FinalDesiredSpeed,
                      std::vector<SMaxSpeedData>& MaxSpeedData ///< [out]
    )
    {
        assert(pPathProfile);
        assert(FinalDesiredSpeed >= 0);

        int nPathSegments = pPathProfile->GetNumTrajs();

        for(int iPathSegment = 0; iPathSegment < nPathSegments; iPathSegment++)
        {
            // Izracunaj trentunu najvecu dopustenu brzinu
            T CurMaxSpeed;
            T CurCurv;

            CTrajPathProfile* pPathSegment = pPathProfile->GetTrajComponent(iPathSegment);
            switch(pPathSegment->GetPathProfileType())
            {
            case CTrajPathProfile::PP_Line:
            {
                CurCurv = 0;
                CurMaxSpeed = m_Parms.MaxRobotSpeed;
            }
            break;

            case CTrajPathProfile::PP_Circle:
            {
                // Saznaj trenutnu zakrivljenost
                CurCurv = ((CArcPathProfile*)pPathSegment)->GetCurvature();
                CurMaxSpeed = m_Parms.MaxRobotSpeed / (1 + m_Parms.WheelDistance * abs(CurCurv) / 2);
            }
            break;

            default:
            {
                assert(0); // Podrzavamo samo pravce i kruznice
                return false;
            }
            break;
            }

            // Ukoliko je promjena zakrivljenosti u odnosu na prethodni path segment veca od praga,
            // umecemo medusegment duljine nula sa usporenjem.
            if(iPathSegment > 0)
            {
                T PrevCurv = 0;

                CTrajPathProfile* pPrevPathSegment = pPathProfile->GetTrajComponent(iPathSegment - 1);
                CTrajPathProfile::EPathProfileType PrevProfileType = pPrevPathSegment->GetPathProfileType();
                if(PrevProfileType == CTrajPathProfile::PP_Line)
                {
                    PrevCurv = 0;
                }
                else if(PrevProfileType == CTrajPathProfile::PP_Circle)
                {
                    PrevCurv = ((CArcPathProfile*)pPrevPathSegment)->GetCurvature();
                }

                double dCurv = abs(CurCurv - PrevCurv);
                double dCurvMax = 10 / m_Parms.WheelDistance; // *** CONST
                dCurv = min(dCurv, dCurvMax);            // Promjenu zakrivljenosti svodimo na interval [0,dCurvMax]

                // Ako je promjena zakrivljenosti manja od praga, to onda zanemarujemo
                if(dCurv > 0.02f / m_Parms.WheelDistance) // *** CONST
                {
                    // Dodajemo medusegment sa usporenjem
                    // Maksimalno usporenje
                    assert(MaxSpeedData.size() > 0);
                    T PrevMaxSpeed = MaxSpeedData.back().MaxSpeed;
                    assert(PrevMaxSpeed > 0);
                    T MaxSpeed = min(PrevMaxSpeed, CurMaxSpeed);

                    SMaxSpeedData maxSpeedData;
                    maxSpeedData.MaxSpeed = MaxSpeed * (m_Parms.MaxSlowDown + dCurv / dCurvMax * (1 - m_Parms.MaxSlowDown));
                    maxSpeedData.Dist = 0;
                    MaxSpeedData.push_back(maxSpeedData);
                }
            }

            // Popuni profil sa max. brzinom
            SMaxSpeedData maxSpeedData;
            maxSpeedData.MaxSpeed = CurMaxSpeed;
            maxSpeedData.Dist = pPathSegment->GetDeltaDist();
            MaxSpeedData.push_back(maxSpeedData);
        }

        // Postavljamo finalnu referentnu brzinu kao konacno ogranicenje
        SMaxSpeedData maxSpeedData;
        maxSpeedData.MaxSpeed = FinalDesiredSpeed;
        maxSpeedData.Dist = 0;
        MaxSpeedData.push_back(maxSpeedData);

        return true;
    }

protected:
    // Bufferi sa vremenskim profilima. Profile drzimo u list bufferu kako bi se izbjeglo
    // dinamicko alociranje memorije za profile prilikom svakog generiranja trajektorije.
    // Koriste se liste kako bi pointeri ostali vazeci prilikom realokacije.
    typedef CUniformSpeedTimeProfile<T> CUniformSpeed;
    typedef CUniformAccelTimeProfile<T> CUniformAccel;

    std::list<CUniformSpeed> m_ConstSpeedProfilesBuffer;
    typename std::list<CUniformSpeed>::iterator m_pCurSpeedProfile;

    std::list<CUniformAccel> m_ConstAccelProfilesBuffer;
    typename std::list<CUniformAccel>::iterator m_pCurAccelProfile;

    CUniformPositionTimeProfile m_ConstPosProfile; // Ovo moze biti potrebno na kraju

    CTrajCompositeTimeProfile<T> m_TimeProfile;

    // Struktura sa parametrima za planer
    SParms m_Parms;
};

#endif // _MaxSpeedCircleLineTimeProfile_h_