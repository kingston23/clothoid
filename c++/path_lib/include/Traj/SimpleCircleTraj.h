// SimpleCircleTraj.h: interface for the CSimpleCircleTraj class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "2DTrajectoryBase.h"

// #include <Assert.h>

/***************************************************************************************************
 Jednostavna trajektorija po kru�nici koja ubrzava od v=0 do v=v_stac zadanom akceleracijom
 i natrag do v=0, zadanom dekceleracijom.
/***************************************************************************************************/
//template<class T>
class CSimpleCircleTraj : public C2DTrajectoryBase
{
public:
    CSimpleCircleTraj()
    {
#ifdef DBG_FILE_
        dbgf = fopen("SimpleCircleTraj.debug", "wt");
#endif
    };

#ifdef DBG_FILE_
    virtual ~CSimpleCircleTraj() { fclose(dbgf); };
#endif

    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        T Velocity;
        T Accel;
        T Disp;

        T Time = pTrPoint->Time;

        if(Time >= m_t3)
        {
            // Stoji u krajnjoj to�ki
            Velocity = 0;
            Accel = 0;
            Disp = m_Disp3;
        }
        else if(Time >= m_t2)
        {
            // Jednoliko ubrzano akceleracijom m_Deccel brzine m_v do 0
            T dt = Time - m_t2;
            Velocity = m_v - m_Deccel * dt;
            Accel = -m_Deccel;
            Disp = m_Disp2 + (m_v + Velocity) * dt / 2;
        }
        else if(Time >= m_t1)
        {
            // Jednoliko brzinom m_v
            Velocity = m_v;
            Accel = 0;
            Disp = m_Disp1 + m_v * (Time - m_t1);
        }
        else if(Time >= m_t0)
        {
            // Jednoliko ubrzano akceleracijom m_Accel brzine 0 do m_v
            T dt = Time - m_t0;
            Velocity = m_Accel * dt;
            Accel = m_Accel;
            Disp = m_Disp0 + Velocity * dt / 2;
        }
        else // Vrijeme manje od m_t0
        {
            // Stoji u po�etnoj to�ki
            Velocity = 0;
            Accel = 0;
            Disp = m_Disp0;
        }
        T CircAng = (Disp - m_Disp0) * m_Curv + m_Angle0; // Kut na kru�nici

        pTrPoint->x = m_xc + m_r * (T)cos(CircAng);
        pTrPoint->y = m_yc + m_r * (T)sin(CircAng);
        if(m_bPositive)
            pTrPoint->Angle = CircAng + PI / 2;
        else
            pTrPoint->Angle = CircAng - PI / 2;
        pTrPoint->Velocity = Velocity;
        pTrPoint->Accel = Accel;
        pTrPoint->Disp = Disp;
        pTrPoint->Curv = m_Curv;
        return true;
    }

    bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        T Time;
        T Velocity;
        T Accel;

        T Disp = pTrPoint->Disp;

        if(Disp >= m_Disp3)
        {
            // Put ne smije biti ve�i od m_Disp3 - vratimo false ili korigiramo
#ifdef DBG_FILE_
            fprintf(dbgf, "Disp>m_Disp3 (udaljenost ve�a od maks mogu�e) u GetPointByDisp\n");
#endif
            Disp = m_Disp3;
            Velocity = 0;
            Accel = 0;
            Time = m_t3;
        }
        else if(Disp >= m_Disp2)
        {
            // Jednoliko usporeno akceleracijom m_Deccel od brzine m_v do 0.
            T ds = Disp - m_Disp2;
            T Discr = m_v * m_v - 2 * m_Deccel * ds; // Diskriminanta
#ifdef DBG_FILE_
            if(Discr < 0)
                fprintf(dbgf, "Diskriminanta negativna za Disp1<Disp<Disp2 u GetPointByDisp\n");
#endif
            T dt = (m_v - (T)sqrt(Discr)) / m_Deccel;
            Velocity = m_v - m_Deccel * dt;
            Accel = -m_Deccel;
            Time = m_t2 + dt;
        }
        else if(Disp >= m_Disp1)
        {
            // Jednoliko brzinom m_v
            Velocity = m_v;
            Accel = 0;
            Time = m_t1 + (Disp - m_Disp1) / m_v;
        }
        else if(Disp >= m_Disp0)
        {
            // Jednoliko ubrzano akceleracijom m_Accel od brzine m_v do 0.
            T dt = (T)sqrt(2 * (Disp - m_Disp0) / m_Accel);
            Velocity = m_Accel * dt;
            Accel = m_Accel;
            Time = m_t0 + dt;
        }
        else
        {
            // assert(0); // return false;
            Disp = m_Disp0;
            Velocity = 0;
            Accel = 0;
            Time = m_t0;
#ifdef DBG_FILE_
            fprintf(dbgf, "Disp manji od m_Disp0 u GetPointByDisp\n");
#endif
        }

        T CircAng = Disp * m_Curv + m_Angle0;

        pTrPoint->x = m_xc + m_r * (T)cos(CircAng);
        pTrPoint->y = m_yc + m_r * (T)sin(CircAng);
        if(m_bPositive)
            pTrPoint->Angle = CircAng + PI / 2;
        else
            pTrPoint->Angle = CircAng - PI / 2;
        pTrPoint->Curv = m_Curv;
        pTrPoint->Time = Time;
        pTrPoint->Velocity = Velocity;
        pTrPoint->Accel = Accel;
        return true;
    }

    // Parametri za zadavanje kruznice preko sredista
    struct SParms1
    {
        T StartTime; // Offseti vremena i pomaka
        T StartDisp;
        T xCenter; // Srediste kruznice [m]
        T yCenter;
        T Radius;
        T TrajAngle0; // Kut po�etne to�ke trajektorije (kut tangente na kru�nicu u po� to�ki)
        T TotalAngle; // Ukupni pomak po kruznici koji treba prijeci [m]
        T v;          // Stacionarna brzina [m/s] (>=0)
        T Accel;      // Ubrzanje na pocetku i usporenje na kraju [m/s2] (>=0)
        T Deccel;
        bool bPositive; // Ako je true idemo u pozitivnom smjeru po kru�nici
    };

    // Direktno zadavanje (preko sredi�ta kru�nice)
    bool SetParms(SParms1* pParms)
    {
        m_t0 = pParms->StartTime;
        m_Disp0 = pParms->StartDisp;
        m_xc = pParms->xCenter;
        m_yc = pParms->yCenter;
        m_r = pParms->Radius;
        m_AngleTotal = pParms->TotalAngle;
        m_bPositive = pParms->bPositive;
        m_v = pParms->v;
        m_Accel = pParms->Accel;
        m_Deccel = pParms->Deccel;
        m_bPositive = pParms->bPositive;
        if(m_bPositive)
            m_Angle0 = pParms->TrajAngle0 - PI / 2;
        else
            m_Angle0 = pParms->TrajAngle0 + PI / 2;

        return CheckParms();
    };

    // Parametri za zadavanje kruznice preko pocetne tocke na kruznici (a ne sredista)
    struct SParms2
    {
        T StartTime; // Offseti vremena i pomaka
        T StartDisp;
        T x0; // Pocetna tocka na kruznici [m]
        T y0;
        T Radius;
        T TrajAngle0; // Kut po�etne to�ke trajektorije (kut tangente na kru�nicu u po� to�ki)
        T TotalAngle; // Ukupni pomak po kruznici koji treba prijeci [m]
        T v;          // Stacionarna brzina [m/s] (>=0)
        T Accel;      // Ubrzanje na pocetku i usporenje na kraju [m/s2] (>=0)
        T Deccel;
        bool bPositive; // Ako je true idemo u pozitivnom smjeru po kru�nici
    };

    // Indirektno zadavanje (preko po�etne to�ke trajektorije)
    bool SetParms(SParms2* pParms)
    {
        m_Disp0 = pParms->StartDisp;
        m_t0 = pParms->StartTime;
        m_r = pParms->Radius;
        m_AngleTotal = pParms->TotalAngle;
        m_bPositive = pParms->bPositive;
        m_v = pParms->v;
        m_Accel = pParms->Accel;
        m_Deccel = pParms->Deccel;
        if(m_bPositive)
            m_Angle0 = pParms->TrajAngle0 - PI / 2;
        else
            m_Angle0 = pParms->TrajAngle0 + PI / 2;
        m_xc = pParms->x0 + pParms->Radius * (T)cos(m_Angle0 + PI);
        m_yc = pParms->y0 + pParms->Radius * (T)sin(m_Angle0 + PI);

        return CheckParms();
    };

    T GetBeginTime() const { return m_t0; };
    T GetDeltaTime() const { return m_t3 - m_t0; }
    void SetBeginTime(T BeginTime)
    {
        T TimeOffset = BeginTime - GetBeginTime();
        m_t0 += TimeOffset;
        m_t1 += TimeOffset;
        m_t2 += TimeOffset;
        m_t3 += TimeOffset;
    };

    T GetBeginDisp() const { return m_Disp0; };
    T GetDeltaDisp() const { return m_Disp3 - m_Disp0; };
    void SetBeginDisp(T BeginDisp)
    {
        T TimeOffset = BeginDisp - GetBeginDisp();
        m_Disp0 += TimeOffset;
        m_Disp1 += TimeOffset;
        m_Disp2 += TimeOffset;
        m_Disp3 += TimeOffset;
    };

private:
    static inline T abs_(T x) { return (x > 0 ? x : -x); }

    bool CheckParms()
    {
        if(m_Accel < 0)
            m_Accel = -m_Accel;

        if(m_Deccel < 0)
            m_Deccel = -m_Deccel;

        if(m_v < 0)
            m_v = -m_v;

        if(m_AngleTotal < 0)
            m_AngleTotal = -m_AngleTotal;

        m_DispTotal = m_AngleTotal * m_r;

        T dt01 = m_v / m_Accel;
        T Disp01 = m_v * dt01 / 2;

        T dt23 = m_v / m_Deccel;
        T Disp23 = m_v * dt23 / 2;

        T Disp12 = m_DispTotal - Disp01 - Disp23;
        T dt12;
        if(Disp12 > 0)
        {
            dt12 = Disp12 / m_v;
        }
        else
        {
            // Ne mo�e se ubrzati do zadane brzine i natrag do v=0 na tako kratkoj udaljenosti
            // Sada su dvije opcije: vratiti false ili se prilagoditi
#ifdef DBG_FILE_
            fprintf(dbgf, "Ne mo�e se ubrzati do zadane brzine i natrag do v=0 na tako kratkoj udaljenosti!\n");
#endif

            // Maksimalna brzina koja se mo�e posti�i
            T vmax = (T)sqrt(2 * m_DispTotal * m_Accel * m_Deccel / (m_Accel + m_Deccel));
            m_v = vmax;

            dt01 = vmax / m_Accel;
            Disp01 = vmax * dt01 / 2;

            dt23 = vmax / m_Deccel;
            Disp23 = vmax * dt23 / 2;

            dt12 = 0;
            Disp12 = 0;
        }

        m_t1 = m_t0 + dt01;
        m_t2 = m_t1 + dt12;
        m_t3 = m_t2 + dt23;

        m_Disp1 = m_Disp0 + Disp01;
        m_Disp2 = m_Disp1 + Disp12;
        m_Disp3 = m_Disp2 + Disp23;

        m_Curv = 1 / m_r;
        if(!m_bPositive)
            m_Curv = -m_Curv;

        return true;
    };

    // Parametri
private:
    // Po�etni uvjeti
    T m_t0; // Po�etno vrijeme (obi�no 0)
    T m_Disp0;
    T m_xc; // Sredi�te kru�nice
    T m_yc;
    T m_r;            // Radijus kru�nice
    T m_Angle0;       // Po�etni kut
    bool m_bPositive; // Kre�emo se u pozitivnom smjeru

    T m_AngleTotal; // Ukupna duljina prevaljenog puta (kuta) po trajektoriji koju treba prije�i
    T m_DispTotal;
    T m_v;      // Stacionarna brzina
    T m_Accel;  // Akceleracija od brzine 0 do m_v
    T m_Deccel; // Akceleracija od brzine m_v do 0

private:
    // Private parametri (za ubrzanje ra�unanja da se ne moraju svaki puta ra�unati)
    // Postavlja ih funkcija SetParms() -> CheckParms()
    T m_t1; // Vrijeme u to�ki 1
    T m_t2;
    T m_t3;

    T m_Disp1; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 1
    T m_Disp2; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 2
    T m_Disp3; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 3

    T m_Curv;

#ifdef DBG_FILE_
    FILE* dbgf;
#endif
};

/* Primjer upotrebe

#define DBG_FILE_ // Opcionalno - ispisuju se upozorenja u "SimpleCircleTraj.debug" file
#include "SimpleCircleTraj.h"

    CSimpleCircleTraj Circ;
    typedef CSimpleCircleTraj::SParms1 SCircParms;
    SCircParms Cp;
    Cp.StartTime = 1;
    Cp.StartDisp = 1;
    Cp.xCenter = 2;
    Cp.yCenter = 2;
    Cp.TrajAngle0 = 0;
    Cp.TotalAngle = 3.14f*5;
    Cp.v = 1;
    Cp.Accel = 1;
    Cp.Deccel = 1;
    Cp.bPositive = true;
    Circ.SetParms(&Cp);

    typedef C2DTrajectoryBase::S2DTrajPoint STrajPt;
    STrajPt TrajPt;
    TrajPt.Time = 1;
    Circ.GetPointByTime(&TrajPt);
    TrajPt.Disp = 2;
    Circ.GetPointByDisp(&TrajPt);

    typedef CSimpleCircleTraj::SParms2 SCircParms2;
    SCircParms2 Cp2;
    Cp2.StartTime = 1;
    Cp2.StartDisp = 2;
    Cp2.x0 = 2;
    Cp2.y0 = 2;
    Cp2.Radius = 1;
    Cp2.TrajAngle0 = PIf/2+PIf*6;
    Cp2.TotalAngle = PIf;
    Cp2.v = 2;
    Cp2.Accel = 1;
    Cp2.Deccel = 1;
    Cp2.bPositive = true;
    Circ.SetParms(&Cp2);
*/
