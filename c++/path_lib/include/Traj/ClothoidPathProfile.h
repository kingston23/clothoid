#ifndef _Clothoid_Path_Profile_h_
#define _Clothoid_Path_Profile_h_

#include "Geom/2DClothoid.h"
#include "TrajPathProfile.h"

using namespace Geom;

class CClothoidPathProfile : public CTrajPathProfile
{
public:
    CClothoidPathProfile()
    {
        m_bParmsSet = false;

        m_BeginExtrapolationMode = EM_Line;
        m_EndExtrapolationMode = EM_Line;
    }

    virtual EPathProfileType GetPathProfileType() const { return PP_Clothoid; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja x, y, Angle i Curv ovisno o Dist parametru iz strukture.
    // Ostali clanovi strukture se ne modificiraju.
    // Ne vrsi se testiranje granica za Dist!!!
    //________________________________________________________________________________________________________________________________________________
    bool GetPointByDist(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        T dd = pTrPoint->Dist - m_BeginDist;

        // ***** Provjera da li je potrebna ekstrapolacija
        bool bExtrapolate = false;
        EPathExtrapolationMode ExtrapolationMode = EM_Unknown;

        if(dd < 0 || dd > m_DeltaDist)
        {
            bExtrapolate = true;

            if(dd < 0)
            {
                dd = 0; // Dobit cemo prvo tocku na pocetku putanje, a onda radimo ekstrapolaciju
                ExtrapolationMode = m_BeginExtrapolationMode;
            }
            else
            {
                dd = m_DeltaDist; // Dobit cemo prvo tocku na kraju putanje, a onda radimo ekstrapolaciju
                ExtrapolationMode = m_EndExtrapolationMode;
            }
        }

        // ***** Racunanje tocke putanje
        T x, y, ang, k;
        if(!m_Clothoid.GetPoint(dd, x, y, ang, k))
        {
            return false;
        }

        pTrPoint->x = x;
        pTrPoint->y = y;
        pTrPoint->Angle = ang;
        pTrPoint->Curv = k;

        // ***** Ekstrapolacija po potrebi
        if(bExtrapolate)
        {
            switch(ExtrapolationMode)
            {
            case EM_Line:
                if(!ExtrapolateLine(pTrPoint))
                    return false;
                break;
            case EM_Circle:
                if(!ExtrapolateArc(pTrPoint))
                    return false;
                break;
            default:
                assert(0); // Ne treba ekstrapolacija? Vracamo pocetnu ili krajnju tocku
                break;
            }
        }

        return true;
    };

    void SetBeginDist(T BeginDist) { m_BeginDist = BeginDist; };
    T GetBeginDist() const { return m_BeginDist; }
    T GetDeltaDist() const { return m_DeltaDist; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Struktura parametara
    //________________________________________________________________________________________________________________________________________________
    struct SParms0
    {
        C2DClothoidApp* pClothoid; // Klotoida cije parametre preuzimamo

        T BeginDist;
        T DeltaDist;
    };

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje parametara
    //________________________________________________________________________________________________________________________________________________
    bool SetParms(SParms0* pParms)
    {
        m_Clothoid = *pParms->pClothoid;

        m_BeginDist = pParms->BeginDist;
        m_DeltaDist = pParms->DeltaDist;

        m_bParmsSet = true;

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Provjera valjanosti parametara
    //________________________________________________________________________________________________________________________________________________
    bool IsValid() const
    {
        if(!m_bParmsSet)
            return false;

        if(!CTrajPathProfile::IsValid())
            return false;

        if(!m_Clothoid.IsValid())
            return false;

        return true;
    }

    // Pristupne rutine

    // Ekstrapolacija
    virtual EPathExtrapolationMode GetPathBeginExtrapolationMode() const { return m_BeginExtrapolationMode; }

    virtual EPathExtrapolationMode GetPathEndExtrapolationMode() const { return m_EndExtrapolationMode; }

    bool SetPathBeginExtrapolationMode(EPathExtrapolationMode BeginMode)
    {
        if(BeginMode != EM_Line && BeginMode != EM_Circle)
            return false;

        m_BeginExtrapolationMode = BeginMode;
        return true;
    }

    bool SetPathEndExtrapolationMode(EPathExtrapolationMode EndMode)
    {
        if(EndMode != EM_Line && EndMode != EM_Circle)
            return false;

        m_EndExtrapolationMode = EndMode;
        return true;
    }

protected:
    // Parametri
    T m_BeginDist; // Pocetni pomak
    T m_DeltaDist; // Promjena pomaka

    C2DClothoidApp m_Clothoid;

    bool m_bParmsSet;

    // Ekstrapolacija
    EPathExtrapolationMode m_BeginExtrapolationMode;
    EPathExtrapolationMode m_EndExtrapolationMode;
};

#endif // _Clothoid_Path_Profile_h_
