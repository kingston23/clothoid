#ifndef _Simple_Line_Traj_h_
#define _Simple_Line_Traj_h_

#include "2DTrajectoryBase.h"

#include <cmath>

//=======================================================================================================================================
//
// Jednostavna trajektorija po pravcu koja ubrzava od v=0 do v=v_stac zadanom akceleracijom
// i natrag do v=0, zadanom deceleracijom.
//
//=======================================================================================================================================

// Definirati DBG_FILE_ za dijagnostiku (ispis upozorenja u file).

template<class T>
class CSimpleLineTraj : public C2DTrajectoryBase
{
public:
    CSimpleLineTraj()
    {
        m_t0 = 0;
        m_x0 = 0;
        m_y0 = 0;
        m_Angle0 = 0;

        m_Distance = 0;
        m_v = 0;
        m_Accel = 0;
        m_Deccel = 0;

#ifdef DBG_FILE_
        dbgf = fopen("SimpleLineTraj.debug", "wt");
#endif
    };

#ifdef DBG_FILE_
    virtual ~CSimpleLineTraj() { fclose(dbgf); };
#endif

    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        T Velocity;
        T Accel;
        T Disp;

        T Time = pTrPoint->Time;

        if(Time >= m_t3)
        {
            // Stoji u krajnjoj to�ki
            Velocity = 0;
            Accel = 0;
            Disp = m_Disp3;
        }
        else if(Time >= m_t2)
        {
            // Jednoliko ubrzano akceleracijom m_Deccel brzine m_v do 0
            T dt = Time - m_t2;
            Velocity = m_v - m_Deccel * dt;
            Accel = -m_Deccel;
            Disp = m_Disp2 + (m_v + Velocity) * dt / 2;
        }
        else if(Time >= m_t1)
        {
            // Jednoliko brzinom m_v
            Velocity = m_v;
            Accel = 0;
            Disp = m_Disp1 + m_v * (Time - m_t1);
        }
        else if(Time >= m_t0)
        {
            // Jednoliko ubrzano akceleracijom m_Accel brzine 0 do m_v
            T dt = Time - m_t0;
            Velocity = m_Accel * dt;
            Accel = m_Accel;
            Disp = m_Disp0 + Velocity * dt / 2;
        }
        else // Vrijeme manje od m_t0
        {
            // Stoji u po�etnoj to�ki
            Velocity = 0;
            Accel = 0;
            Disp = m_Disp0;
        }

        pTrPoint->x = m_x0 + Disp * m_Cos;
        pTrPoint->y = m_y0 + Disp * m_Sin;
        pTrPoint->Angle = m_Angle0;
        pTrPoint->Velocity = Velocity;
        pTrPoint->Accel = Accel;
        pTrPoint->Disp = Disp;
        pTrPoint->Curv = 0;
        return true;
    }

    bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        T Time;
        T Velocity;
        T Accel;

        T Disp = pTrPoint->Disp;

        if(Disp >= m_Disp3)
        {
            // Put ne smije biti ve�i od m_Disp3 - vratimo false ili korigiramo
#ifdef DBG_FILE_
            fprintf(dbgf, "Disp>m_Disp3 (udaljenost ve�a od maks mogu�e) u GetPointByDisp\n");
#endif
            Disp = m_Disp3;
            Velocity = 0;
            Accel = 0;
            Time = m_t3;
        }
        else if(Disp >= m_Disp2)
        {
            // Jednoliko usporeno akceleracijom m_Deccel od brzine m_v do 0.
            T ds = Disp - m_Disp2;
            T Discr = m_v * m_v - 2 * m_Deccel * ds; // Diskriminanta
#ifdef DBG_FILE_
            if(Discr < 0)
                fprintf(dbgf, "Diskriminanta negativna za Disp1<Disp<Disp2 u GetPointByDisp\n");
#endif
            T dt = (m_v - (T)sqrt(Discr)) / m_Deccel;
            Velocity = m_v - m_Deccel * dt;
            Accel = -m_Deccel;
            Time = m_t2 + dt;
        }
        else if(Disp >= m_Disp1)
        {
            // Jednoliko brzinom m_v
            Velocity = m_v;
            Accel = 0;
            Time = m_t1 + (Disp - m_Disp1) / m_v;
        }
        else if(Disp >= m_Disp0)
        {
            // Jednoliko ubrzano akceleracijom m_Accel od brzine m_v do 0.
            T dt = (T)sqrt(2 * (Disp - m_Disp0) / m_Accel);
            Velocity = m_Accel * dt;
            Accel = m_Accel;
            Time = m_t0 + dt;
        }
        else
        {
            // assert(0); // return false;
            Disp = 0;
            Velocity = 0;
            Accel = 0;
            Time = m_t0;
#ifdef DBG_FILE_
            fprintf(dbgf, "Disp manji od m_Disp0 u GetPointByDisp\n");
#endif
        }

        pTrPoint->x = m_x0 + Disp * m_Cos;
        pTrPoint->y = m_y0 + Disp * m_Sin;
        pTrPoint->Angle = m_Angle0;
        pTrPoint->Curv = 0;
        pTrPoint->Time = Time;
        pTrPoint->Velocity = Velocity;
        pTrPoint->Accel = Accel;

        return true;
    }

    struct SParms1
    {
        T StartTime;
        T StartDisp;
        T x0;
        T y0;
        T TotalDisp;
        T LineAngle;
        T v;
        T Accel;
        T Deccel;
    };

    bool SetParms(SParms1* pParms)
    {
        m_t0 = pParms->StartTime;
        m_Disp0 = pParms->StartDisp;
        m_x0 = pParms->x0;
        m_y0 = pParms->y0;
        m_Distance = pParms->TotalDisp;
        m_Angle0 = pParms->LineAngle;
        m_v = pParms->v;
        m_Accel = pParms->Accel;
        m_Deccel = pParms->Deccel;

        return CheckParms();
    };

    virtual T GetBeginTime() const { return m_t0; };
    virtual T GetDeltaTime() const { return m_t3 - m_t0; }
    void SetBeginTime(T BeginTime)
    {
        T TimeOffset = BeginTime - GetBeginTime();
        m_t0 += TimeOffset;
        m_t1 += TimeOffset;
        m_t2 += TimeOffset;
        m_t3 += TimeOffset;
    };

    T GetBeginDisp() const { return m_Disp0; };
    T GetEndDist() const { return m_Disp3; };
    void SetBeginDisp(T BeginDisp)
    {
        T TimeOffset = BeginDisp - GetBeginDisp();
        m_Disp0 += TimeOffset;
        m_Disp1 += TimeOffset;
        m_Disp2 += TimeOffset;
        m_Disp3 += TimeOffset;
    };

private:
    bool CheckParms()
    {
        if(m_Accel < 0)
            m_Accel = -m_Accel;

        if(m_Deccel < 0)
            m_Deccel = -m_Deccel;

        if(m_v < 0)
            m_v = -m_v;

        if(m_Distance < 0)
            m_Distance = -m_Distance;

        T dt01 = m_v / m_Accel;
        T Disp01 = m_v * dt01 / 2;

        T dt23 = m_v / m_Deccel;
        T Disp23 = m_v * dt23 / 2;

        T Disp12 = m_Distance - Disp01 - Disp23;
        T dt12;
        if(Disp12 > 0)
        {
            dt12 = Disp12 / m_v;
        }
        else
        {
            // Ne mo�e se ubrzati do zadane brzine i natrag do v=0 na tako kratkoj udaljenosti
            // Sada su dvije opcije: vratiti false ili se prilagoditi

            // Maksimalna brzina koja se mo�e posti�i
            T vmax = (T)sqrt(2 * m_Distance * m_Accel * m_Deccel / (m_Accel + m_Deccel));
            m_v = vmax;

            dt01 = vmax / m_Accel;
            Disp01 = vmax * dt01 / 2;

            dt23 = vmax / m_Deccel;
            Disp23 = vmax * dt23 / 2;

            dt12 = 0;
            Disp12 = 0;

#ifdef DBG_FILE_
            fprintf(dbgf, "Ne mo�e se ubrzati do zadane brzine i natrag do v=0 na tako kratkoj udaljenosti!\n");
#endif
        }

        m_t1 = m_t0 + dt01;
        m_t2 = m_t1 + dt12;
        m_t3 = m_t2 + dt23;

        m_Disp1 = m_Disp0 + Disp01;
        m_Disp2 = m_Disp1 + Disp12;
        m_Disp3 = m_Disp2 + Disp23;

        m_Cos = (T)cos(m_Angle0);
        m_Sin = (T)sin(m_Angle0);

        return true;
    };

    // Parametri
private:
    // Po�etni uvjeti
    T m_t0;    // Po�etno vrijeme (obi�no 0)
    T m_Disp0; // Po�etna udaljenost (obi�no 0)
    T m_x0;    // Po�etna to�ka
    T m_y0;
    T m_Angle0; // Kut pravca

    T m_Distance; // Ukupna duljina prevaljenog puta po trajektoriji

    // Stacionarna brzina (sekvenca je ubrzanje->stacionarna brzina->usporavanje). Ponekad nece biti uopce moguce dostici tu brzinu ako je
    // prekratka udaljenost. To je u biti najveca dopustena brzina
    T m_v;

    T m_Accel;  // Akceleracija od brzine 0 do 1
    T m_Deccel; // Akceleracija od brzine 1 do 2 - negativna je

private:
    // Private parametri (za ubrzanje ra�unanja da se ne moraju svaki puta ra�unati)
    // Postavlja ih funkcija SetParms() -> CheckParms()
    T m_t1; // Vrijeme u to�ki 1
    T m_t2;
    T m_t3;

    T m_Disp1; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 1
    T m_Disp2; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 2
    T m_Disp3; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 3

    T m_Cos;
    T m_Sin;

#ifdef DBG_FILE_
    FILE* dbgf;
#endif
};

/*
template <class T>
int CSimpleLineTraj<T>::ab()
{
    return 1;
}
*/

/*
Primjer upotrebe:

#define DBG_FILE_ // Opcionalno
#include "SimpleLineTraj.h"

CSimpleLineTraj Traj;

typedef CSimpleLineTraj::SParms1 SLnParms1;
SLnParms1 Lp1;
Lp1.StartTime = 0;
Lp1.StartDisp = 1;
Lp1.x0 = 10;
Lp1.y0 = 20;
Lp1.TotalDisp = 3;
Lp1.LineAngle = 3.14f;
Lp1.v = 2;
Lp1.Accel = 1;
Lp1.Deccel = 1;
Traj.SetParms(&Lp1);

typedef CSimpleLineTraj::S2DTrajPoint STrajPt;

STrajPt TrajPt1;
TrajPt1.Disp = 1;
Traj.GetPointByDisp(&TrajPt1);

STrajPt TrajPt2;
TrajPt2.Time = 2;
Traj.GetPointByTime(&TrajPt2);
*/

#endif // _Simple_Line_Traj_h_
