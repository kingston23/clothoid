#ifndef _Controller_Goto1_h_
#define _Controller_Goto1_h_

#include "Geom/Line.h"
#include "Math/AngleRange.h"
#include "Math/Numeric.h"
#include "Controller_Base.h"
#include "Controller_CMU98.h"
#include "Controller_Line1.h"

#include <algorithm>
#include <assert.h>
#include <cmath>

using namespace MathMB;

//=======================================================================================================================================
//
// Klasa za kontroler za pozicioniranje robota u zeljeni (x,y,ang,v). (nedovrseno, do not koristi!)
//
//=======================================================================================================================================

template<typename T>
class CController_Goto1 : public CController_Base<T>
{
public:
    CController_Goto1() { m_Name = CN_Goto1; }

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Parametri koji se moraju zadavati za svaki novi sempl
    //_____________________________________________________________________________________________________________________________________________________________________
    struct SSampleParms
    {
        T xTarget, yTarget; // Tu ocemo da robot dojde
        T angTarget;
        T vTarget;

        T xRobot, yRobot; // Tu je robot
        T angRobot;
    };

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Daje referentne brzine robota
    //_____________________________________________________________________________________________________________________________________________________________________
    bool GetRefValues(SSampleParms* pSp, // IN
                      T& vRef, T& wRef   // OUT
    )
    {
        assert(pSp);

        if(!IsValid())
        {
            assert(0);
            return false;
        }

        return true;
    }

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Parametri kontrolera
    //_____________________________________________________________________________________________________________________________________________________________________
    struct SParms
    {
        CController_CMU98::SParms CmuParms;
        CController_Line1::SParms Line1Parms;
    };

    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje novih parametara kontrolera
    //_____________________________________________________________________________________________________________________________________________________________________
    bool SetParms(SParms* pNewParms)
    {
        Parms = *pNewParms;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        if(!cCmu.SetParms(pNewParms->CmuParms))
            return false;

        if(!cLine.SetParms(pNewParms->Line1Parms))
            return false;

        return true;
    }

    bool IsValid() const
    {
        if(!cCmu.IsValid())
            return false;

        if(!cLine.IsValid())
            return false;

        if(!IsValidParms())
            return false;

        return true;
    }

    bool IsValidParms() const { return true; }

private:
    CController_CMU98 cCmu;
    CController_Line1 cLine;

    SParms Parms;
};

#endif

/*

Koristenje:

CController_Goto1 cGoto;

// Init parametara
CController_Goto1::SParms Parms;

Parms.CmuParms.xxx = x;
Parms.CmuParms.yyy = y;
...
Parms.Line1Parms.xxx = x;
Parms.Line1Parms.yyy = y;
...

cGoto.SetParms(&Parms);

// Parametri za sempl
CController_Goto1::SSampleParms SampleParms;

SampleParms.xRobot = ;
SampleParms.yRobot = ;
SampleParms.angRobot = ;

SampleParms.xTarget = ;
SampleParms.yRobot = ;
SampleParms.angTarget = ;
SampleParms.vTarget= ;

// Dobivanje ref. brzina
flaot vRef, wRef;
if(!cGoto.GetRefValues(&SampleParms, vRef, wRef))
    return false;

*/
