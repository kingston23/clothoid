#ifndef _TrajTrackingRegulator_Linear_h_
#define _TrajTrackingRegulator_Linear_h_

#include "Math/AngleRange.h"
#include "2DTrajectoryBase.h"
#include "MobileRobot.h"
#include "TrajTrackingController.h"

#include <fstream>

using namespace std;

//************************************************************************************************************************************************************************************
//
// Linearni regulator za slijedenje trajektorije za mobilnog robota.
//
//************************************************************************************************************************************************************************************
class CTrajTrackingRegulator_Linear : public CTrajTrackingController
{
public:
    CTrajTrackingRegulator_Linear()
    {
        m_ControllerType = TTC_Linear;

        m_LogFile.open("Log/Rezultati_linear_reg.logtr", ios::out);

        m_LogFile << "% Rezultati eksperimenta s linearnim regulatorom za slijedenje trajektorije \n";
        m_LogFile << "% Podaci u stupcima imaju poredak: \n";
        m_LogFile << "% Vrijeme | Trajektorija | Mjerenja | Pogreska | Reference \n";
        m_LogFile << "% Detaljnije: \n";
        m_LogFile << "% t xTraj yTraj angTraj vTraj wTraj aTraj xm ym angm vm wm e1 e2 e3 vRef wRef\n";

        m_LogFile << fixed;   // Tak da svaki float broj zauzima isto mjesta
        m_LogFile << showpos; // Tak da pozitivni brojevi imaju +
    }

    ~CTrajTrackingRegulator_Linear() { m_LogFile.close(); }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Funkcija racuna nove referentne brzine za robota. Poziva se za svaki novi vremenski uzorak.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool GetRefValues(typename CMobileRobot::SRobotState* pState,   // (in), u ovoj strukturi nalazi se trenutno stanje robota
                              typename CMobileRobot::SRobotState* pRefState // (out), u ovu strukturu postavlja se referentna lin. i kutna
                                                                            // brzina robota. Ostali clanovi strukture se ignoriraju
    )
    {
        assert(pState && pRefState);

        if(!IsValid())
        {
            assert(0);
            return false;
        }

        T Time = pState->CurTime; // trenutno vrijeme

        // Uzimamo referentnu tocku iz trajektorije (ref. tocka je funkcija vremena)
        C2DTrajectoryBase::S2DTrajPoint RefPoint;
        RefPoint.Time = Time + (T)0.5 * m_SamplingPeriod; // Uzimamo reference s x koraka unaprijed
        if(!m_pTraj->GetPointByTime(&RefPoint))
            return false;

        // Tocka trajektorije
        T xTraj = RefPoint.x;
        T yTraj = RefPoint.y;
        T angTraj = RefPoint.Angle;
        T vTraj = RefPoint.Velocity;
        T wTraj = RefPoint.GetAngVelocity();
        T aTraj = RefPoint.Accel;

        // Stanje robota (mjereno)
        T xm = pState->x;
        T ym = pState->y;
        T angm = pState->Angle;

        T vm = pState->Velocity;
        T wm = pState->AngVelocity;

        m_LogFile << Time << ' ' << xTraj << ' ' << yTraj << ' ' << angTraj << ' ' << vTraj << ' ' << wTraj << ' ' << aTraj << ' ';
        m_LogFile << xm << ' ' << ym << ' ' << angm << ' ' << vm << ' ' << wm << ' ';

        // Predikcija zbog kasnjenja

        CompensateDelay(pState);

        // Sad uzimamo predvidena stanja robota kao mjerena stanja
        xm = pState->x;
        ym = pState->y;
        angm = pState->Angle;

        vm = pState->Velocity;
        wm = pState->AngVelocity;

        // Proracun izlaza regulatora (tj. ref lin. i kutne brzine robota)

        // Pogreske slijedenja
        T e1 = cos(angm) * (xTraj - xm) + sin(angm) * (yTraj - ym);
        T e2 = -sin(angm) * (xTraj - xm) + cos(angm) * (yTraj - ym);
        T e3 = CAngleRange::Range_MinusPI_PI(angTraj - angm);

        // Zbroji pogreske - to je SSE (sum of squared errors)
        m_SSE1 += e1 * e1;
        m_SSE2 += e2 * e2;
        m_SSE3 += e3 * e3;

        // Koeficijenti za algoritam
        T k1 = 2 * m_zeta * sqrt(pow(wTraj, 2) + m_b * pow(vTraj, 2));
        T k3 = k1;

        // Vektor ub (feedback vektor);
        T ub1 = k1 * e1;
        T ub2;
        if(vTraj >= 0)
        {
            T k2 = m_b * vTraj;
            ub2 = k2 * e2 + k3 * e3;
        }
        else
        {
            T k2 = m_b * (-vTraj);
            ub2 = k3 * e3 - k2 * e2;
        }

        // Zbroji feedforward i feedback da bismo dobili upravljacke signale
        T vRef = vTraj * cos(e3) + ub1; // ref. linearna brzina [m/s]
        T wRef = wTraj + ub2;           // ref. kutna brzina [rad/s]

        // Napuni strukturu s ref. stanjem
        pRefState->Velocity = vRef;
        pRefState->AngVelocity = wRef;
        pRefState->x = xTraj;
        pRefState->y = yTraj;
        pRefState->Angle = angTraj;

        // Ogranici ref. vrijednosti
        BoundRefValues(pRefState);

        m_LogFile << e1 << ' ' << e2 << ' ' << e3 << ' ' << ' ' << pRefState->Velocity << ' ' << pRefState->AngVelocity << endl;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Struktura za zadavanje parametara regulatora
    //____________________________________________________________________________________________________________________________________________________________________________________
    struct SParms : public CTrajTrackingController::SParms
    {
        // Parametri regulatora
        T b;
        T zeta;
    };

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje parametara regulatora.
    // Ovu funkciju uvijek treba pozvati prije koristenja regulatora.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool SetParms(CTrajTrackingController::SParms* pBaseParms)
    {
        if(!CTrajTrackingController::SetParms(pBaseParms))
            return false;

        SParms* pParms = static_cast<SParms*>(pBaseParms);

        m_b = pParms->b;
        m_zeta = pParms->zeta;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera stanja klase
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValid() const
    {
        if(!CTrajTrackingController::IsValid())
            return false;

        if(!IsValidParms())
            return false;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera dal su dobro postavljeni parametri
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValidParms() const
    {
        if(m_zeta < 0)
            return false;

        if(m_b < 0)
            return false;

        return true;
    }

private:
    // Parametri regulatora

    T m_zeta; // Faktor prigusenja modelske prijenosne funkcije
    T m_b;    // Dodatni faktor za podesavanje

    std::ofstream m_LogFile; // Log datoteka
};

// Primjer koristenja regulatora
// void main()
//{
//	CTrajTrackingRegulator_Linear Regulator;
//	CTrajTrackingRegulator_Linear::SParms Parms;
//
//
//
//	Parms.pTraj = nullptr;
//	Parms.SamplingPeriod = (1/80);
//	Parms.b = 60;
//	Parms.zeta = 0.7;
//	Parms.vMax = 2;
//	Parms.wMax = 2/(0.067/2);
//  Parms... = ...
//
//	fprintf(Regulator.dat,"xTraj xm yTraj ym angTraj angm CurvTraj vTraj vRef wTraj wRef\n");
//
//	if(!Regulator.SetParms(&Parms))
//		return;
//
//	for(int i=0; i<2400; i++)
//	{
//		CMobileRobot::SRobotState MeasuredState;
//		CMobileRobot::SRobotState RefState;
//
//		MeasuredState.x = ...;
//		MeasuredState.y = ...;
//		...
//
//		Regulator.GetRefValues(&MeasuredState, &RefState);
//
//		// Posalji reference robotu
//		float vRef = RefState.Velocity;
//		float wRef = RefState.AngVelocity;
//
//		...
//	}
//}

#endif // _TrajTrackingRegulator_Linear_h_
