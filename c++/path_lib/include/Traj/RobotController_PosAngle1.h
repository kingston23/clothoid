#ifndef _RobotController_PosAngle1_h_
#define _RobotController_PosAngle1_h_

#include "MobileRobot.h"
#include "MobileRobotTrackRegulator1.h"
#include "PosAngleTrajPlanner1.h"
#include "TrajTrackingController.h"
#include "TrajTrackingRegulator_Linear.h"
#include "TrajTrackingRegulator_MPC.h"
#include "TrajTrackingRegulator_Nonlinear.h"
#include "TrajTrackingRegulator_PI.h"
//#include "Log/Log.h"
#include "Geom/Line.h"

//===============================================================================================================================================================================
//
// Kontroler koji dovodi robota u zeljenu poziciju i kut (a i brzinu)
//
//===============================================================================================================================================================================
template<typename T>
class CRobotController_PosAngle1 : public CMobileRobotDataTypes<T>
{
public:
    CRobotController_PosAngle1()
    {
        m_bAutoInvert = true;
        m_bInverted = false;

        m_bContinueAfterDone = true;
        Reset();

        // Pogreska slijedenja trajektorije koju toleriramo
        m_e1Tol = 0.07f;
        m_e2Tol = 0.07f;
        m_e3Tol = 30.f * PI / 180;
    }

    struct SParms
    {
        // Robot
        DIST_MR WheelDistance;

        T SamplingPeriod; // vrijeme uzorkovanja

        T vMax; // [>0] max. linijska brzina (po aps. vrijednosti)
        T wMax; // [>0] max. kutna brzina (po aps. vrijednosti)

        T LinAccelMax; // [>0] max. linijska akceleracija (po aps. vrijednosti)
        T AngAccelMax; // [>0] max. kutna akceleracija (po aps. vrijednosti)

        T nSamplesDelay; // Koliko koraka uzorkovanja iznosi kasnjenje

        // Tolerancije za kriterij dal smo stigli do cilja
        DIST_MR GoalDistTolerance;
        ANGLERAD_MR GoalAngleTolerance;
        SPEED_MR GoalSpeedTolerance;

        // Tolerancije za kriterij dal se promijenila ciljna tocka
        DIST_MR TrajDistTolerance;
        ANGLERAD_MR TrajAngleTolerance;
        SPEED_MR TrajSpeedTolerance;

        typename CPosAngleTrajPlanner1<T>::SParms TrajParms;
        // typename CMobileRobotTrackRegulator1<T>::SParms RegulatorParms;
    };

    //____________________________________________________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje parametara kontrolera
    //____________________________________________________________________________________________________________________________________________________________________________________________________
    bool SetParms(SParms* pParms)
    {
        // Parametri planera
        if(!m_TrajPlanner.SetParms(&pParms->TrajParms))
            return false;

        m_MaxRobotSpeed = pParms->vMax;

        m_GoalDistTolerance = pParms->GoalDistTolerance;
        m_GoalAngleTolerance = pParms->GoalAngleTolerance;
        m_GoalSpeedTolerance = pParms->GoalSpeedTolerance;

        m_TrajDistTolerance = pParms->TrajDistTolerance;
        m_TrajAngleTolerance = pParms->TrajAngleTolerance;
        m_TrajSpeedTolerance = pParms->TrajSpeedTolerance;

        // Parametri regulatora za slijedenje trajektorije

        CTrajTrackingController::SParms* pControllerParms; // Pokazivac na parametre zajednicke za sve kontrolere

        switch(m_TrackingController.m_ControllerType)
        {
        case CTrajTrackingController::TTC_Linear:
        {
            CTrajTrackingRegulator_Linear<T>::SParms Parms;
            pControllerParms = &Parms;

            Parms.b = 60.f;
            Parms.zeta = 0.7f;

            break;
        }
        case CTrajTrackingController::TTC_Nonlinear:
        {
            CTrajTrackingRegulator_Nonlinear<T>::SParms Parms;
            pControllerParms = &Parms;

            Parms.b = 60.f;
            Parms.zeta = 0.7f;

            break;
        }
        case CTrajTrackingController::TTC_MPC:
        {
            CTrajTrackingRegulator_MPC<T>::SParms Parms;
            pControllerParms = &Parms;

            Parms.ar = 0.8f;
            Parms.q1 = 1;    // 4
            Parms.q2 = 10;   // 40
            Parms.q3 = 0.1f; // 0.1f;
            Parms.r = 1e-3f;

            break;
        }
        case CTrajTrackingController::TTC_PI:
        {
            CTrajTrackingRegulator_PI<T>::SParms Parms;
            pControllerParms = &Parms;

            Parms.b = 0.02f;
            Parms.u1minus = 0;
            Parms.u2minus = 0;
            Parms.y1dminus = 0;
            Parms.y2dminus = 0;
            Parms.kp1 = 0.75;
            Parms.kp2 = 0.75;
            Parms.ti1 = 0;
            Parms.ti2 = 0;

            break;
        }
        case CTrajTrackingController::TTC_Moj:
        {
            CMobileRobotTrackRegulator1<T>::SParms Parms;
            pControllerParms = &Parms;

            Parms.AnglePidParms.DiscretizationMethod = CPidController<T>::DM_Backward;
            Parms.AnglePidParms.Kp = 3.4f; // 1.5f
            Parms.AnglePidParms.Ti = 10.f;
            Parms.AnglePidParms.Td = 0.25f; // 0.1f 0.25f;
            Parms.AnglePidParms.Tdiscr = pParms->SamplingPeriod;
            Parms.AnglePidParms.StartIntegralTerm = 0.f;
            Parms.AnglePidParms.Ni = 15;
            Parms.AnglePidParms.bUseP = true;
            Parms.AnglePidParms.bUseI = false;
            Parms.AnglePidParms.bUseD = true;
            Parms.AnglePidParms.MaxOutput = pParms->wMax;

            Parms.LinearPidParms.DiscretizationMethod = CPidController<T>::DM_Backward;
            Parms.LinearPidParms.Kp = 5.0f; // 8.0f
            Parms.LinearPidParms.Ti = 10.f;
            Parms.LinearPidParms.Td = 0.1f; // 0.25f;
            Parms.LinearPidParms.Tdiscr = pParms->SamplingPeriod;
            Parms.LinearPidParms.StartIntegralTerm = 0.f;
            Parms.LinearPidParms.Ni = 15;
            Parms.LinearPidParms.bUseP = true;
            Parms.LinearPidParms.bUseI = false;
            Parms.LinearPidParms.bUseD = true;
            Parms.LinearPidParms.MaxOutput = pParms->vMax;

            Parms.LookAhead = 0.05f;

            break;
        }
        default:
            assert(0);
            return false;
        }

        // Zajednicki parametri za sve kontrolere
        pControllerParms->vMax = pParms->vMax;
        pControllerParms->wMax = pParms->wMax;
        pControllerParms->LinAccelMax = pParms->LinAccelMax;
        pControllerParms->AngAccelMax = pParms->AngAccelMax;
        pControllerParms->SamplingPeriod = pParms->SamplingPeriod;
        pControllerParms->nSamplesDelay = pParms->nSamplesDelay;

        m_TrackingController.SetParms(pControllerParms);

        m_TrackingController.Initialize();

        Reset();

        if(!IsValidParms())
            return false;

        return true;
    };

    //____________________________________________________________________________________________________________________________________________________________________________________________________
    //
    // Dovodi robota u zeljenu poziciju, kut i brzinu u "minimalnom" vremenu.
    // Pretpostavlja se robot s diferencijalnim pogonom.
    // Referentna brzina mora biti >=0!
    //____________________________________________________________________________________________________________________________________________________________________________________________________
    bool GoTo(TIME_MR CurTime, COORD_MR xCur, COORD_MR yCur, ANGLERAD_MR AngleCur, SPEED_MR SpeedCur, T AngSpeedCur, COORD_MR xRef,
              COORD_MR yRef, ANGLERAD_MR AngleRef, SPEED_MR SpeedRef, SPEED_MR* pSpeedCommand, ANGSPEED_MR* pAngSpeedCommand)
    {
        // LOGSection(CRobotController_PosAngle1_GoTo);

        // Provjera argumenata
        assert(SpeedRef >= 0 && pAngSpeedCommand && pSpeedCommand);

        if(SpeedRef < 0 || !(pSpeedCommand && pAngSpeedCommand))
            return false;

        // Provjeri da li se promijenila ciljna tocka
        bool bGoalPointChanged = GoalPointChanged(xRef, yRef, AngleRef, SpeedRef);

        // Ako trajektorija nije jos konstruirana, ili se promijenila
        // ciljna tocka, konstruiraj novu trajektoriju
        if(!m_bTrajectoryConstructed || bGoalPointChanged)
        {
            if(!ConstructNewTraj(CurTime, xCur, yCur, AngleCur, SpeedCur, xRef, yRef, AngleRef, SpeedRef))
            {
                return false;
            }
        }

        // Provjeri da li je dostignut cilj
        if(!m_bDone)
        {
            CheckIfDone(xCur, yCur, AngleCur, SpeedCur, xRef, yRef, AngleRef, SpeedRef);
        }

        // LOG(m_bDone);

        if(m_bDone && !m_bContinueAfterDone)
        {
            // Misija izvrsena
            *pSpeedCommand = SpeedRef; // Idi dalje ref. brzinom
            *pAngSpeedCommand = 0;
            return true;
        }

        // Upravljacke vrijednosti
        CMobileRobot::SRobotState RefState;
        if(!GetControlVals(CurTime, xCur, yCur, AngleCur, SpeedCur, AngSpeedCur, &RefState))
            return false;

        // Odredi gresku slijedenja trajektorije

        // Greska u odnosu na trenutnu ref. tocku.
        // Ne gledamo brzinu.
        DIST_MR xErr = RefState.x - xCur;
        DIST_MR yErr = RefState.y - yCur;
        DIST_MR ddErr = xErr * xErr + yErr * yErr;
        T e1 = abs(cos(AngleCur) * (xErr) + sin(AngleCur) * (yErr));
        T e2 = abs(-sin(AngleCur) * (xErr) + cos(AngleCur) * (yErr));
        T e3 = abs(NormAngle_MinusPI_PI(RefState.Angle - AngleCur));

        bool bTrackFail = (e1 > m_e1Tol || e2 > m_e2Tol || e3 > m_e3Tol);

        // Odredi koliko smo udaljeni od ciljne tocke
        DIST_MR dxGoal = xRef - xCur;
        DIST_MR dyGoal = yRef - yCur;
        DIST_MR ddGoal_2 = dxGoal * dxGoal + dyGoal * dyGoal;

        // Novu trajektoriju konstruiramo samo ako nismo preblizu cilju
        if(bTrackFail && ddGoal_2 > m_GoalDistTolerance * m_GoalDistTolerance * 4)
        {
            // stLOG("Track fail");

            // Konstruiraj novu trajektoriju
            if(!ConstructNewTraj(CurTime, xCur, yCur, AngleCur, SpeedCur, xRef, yRef, AngleRef, SpeedRef))
            {
                // stLOG("Build new traj");
                return false;
            }

            // Slijedi novu trajektoriju
            if(!GetControlVals(CurTime, xCur, yCur, AngleCur, SpeedCur, AngSpeedCur, &RefState))
                return false;
        }

        // Izlazne ref. vrijednosti
        SPEED_MR SpeedCommand = RefState.Velocity;
        ANGSPEED_MR AngSpeedCommand = RefState.AngVelocity;

        *pSpeedCommand = SpeedCommand;
        *pAngSpeedCommand = AngSpeedCommand;

        // LOG(SpeedCommand);
        // LOG(AngSpeedCommand);

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________________________
    //
    // Vraca regulator
    //____________________________________________________________________________________________________________________________________________________________________________________________________
    CTrajTrackingController& GetTrajTrackingController() const { return m_TrackingController; }

protected:
    bool ConstructNewTraj(TIME_MR CurTime, COORD_MR xCur, COORD_MR yCur, ANGLERAD_MR AngleCur, SPEED_MR SpeedCur, COORD_MR xEnd,
                          COORD_MR yEnd, ANGLERAD_MR AngleEnd, SPEED_MR SpeedEnd)
    {
        // Konstruiraj novu trajektoriju
        CMobileRobot::SRobotState CurState, EndState;

        CurState.x = xCur;
        CurState.y = yCur;
        CurState.Angle = AngleCur;
        CurState.Velocity = SpeedCur;

        EndState.x = xEnd;
        EndState.y = yEnd;
        EndState.Angle = AngleEnd;
        EndState.Velocity = SpeedEnd;

        m_TrajPlanner.SetAutoInvert(GetAutoInvert());

        C2DTrajectoryBase* pTraj = m_TrajPlanner.PlanTrajectory(&CurState, &EndState, nullptr);

        if(!pTraj)
        {
            // assert(0);
            m_bTrajectoryConstructed = false;
            return false;
        }

        pTraj->SetBeginTime(CurTime);

        if(!m_TrackingController.SetTrajectory(pTraj))
        {
            assert(0);
            m_bTrajectoryConstructed = false;
            return false;
        }

        // Zapamti ref. tocku za koju je konstruirana trajektorija
        m_xRefTr = xEnd;
        m_yRefTr = yEnd;
        m_AngleRefTr = AngleEnd;
        m_SpeedRefTr = SpeedEnd;

        Reset();
        m_bTrajectoryConstructed = true;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________________________
    //
    // Racuna referentne vrijednosti brzina pozivanjem regulatora za slijedenje trajektorije
    //____________________________________________________________________________________________________________________________________________________________________________________________________
    bool GetControlVals(TIME_MR CurTime, COORD_MR xCur, COORD_MR yCur, ANGLERAD_MR AngleCur, SPEED_MR SpeedCur, T AngSpeedCur,
                        typename CMobileRobot::SRobotState* pRefState // (out) U ovoj strukturi bit ce ref. brzina robota
    )
    {
        assert(pRefState);

        CMobileRobot::SRobotState MeasuredState;

        // Mjerni podaci za regulator
        MeasuredState.x = xCur;
        MeasuredState.y = yCur;
        MeasuredState.Angle = AngleCur;
        MeasuredState.Velocity = SpeedCur;
        MeasuredState.AngVelocity = AngSpeedCur;
        MeasuredState.CurTime = CurTime;

        if(!m_TrackingController.GetRefValues(&MeasuredState, pRefState))
        {
            assert(0);
            return false;
        }

        return true;
    }

    bool CheckIfDone(COORD_MR xCur, COORD_MR yCur, ANGLERAD_MR AngleCur, SPEED_MR SpeedCur, COORD_MR xRef, COORD_MR yRef,
                     ANGLERAD_MR AngleRef, SPEED_MR SpeedRef)
    {
        // Povjera da li smo stigli u ciljnu tocku
        DIST_MR dx = xRef - xCur;
        DIST_MR dy = yRef - yCur;
        DIST_MR dd2 = dx * dx + dy * dy;

        ANGLERAD_MR dAng = abs(NormAngle_MinusPI_PI(AngleRef - AngleCur));
        SPEED_MR dSpeed = abs(SpeedRef - SpeedCur);

        // Povlacimo pravac kroz ciljnu tocku okomit na ciljni kut.
        // Ako robot prijede s lijeve na desnu stranu tog pravca, uzimamo da
        // je cilj dostignut (princip kaj je tu je)
        CLineImp<T> Line;
        Line.LineFromDirectionAndPoint(AngleRef + PI / 2, xRef, yRef);
        if(Line.IsPointLeft(xCur, yCur) && !m_bRobotWasBehindGoal)
            m_bRobotWasBehindGoal = true;

        DIST_MR GoalDistTolerance_2 = m_GoalDistTolerance * m_GoalDistTolerance;

        m_bDone = (dd2 <= GoalDistTolerance_2 && dAng <= m_GoalAngleTolerance && dSpeed <= m_GoalSpeedTolerance && m_bRobotWasBehindGoal &&
                   Line.IsPointRight(xCur, yCur)) ||
                  (m_bRobotWasBehindGoal && Line.IsPointRight(xCur, yCur) && dd2 <= 4 * GoalDistTolerance_2);

        return true;
    }

    bool GoalPointChanged(COORD_MR xRef, COORD_MR yRef, ANGLERAD_MR AngleRef, SPEED_MR SpeedRef)
    {
        DIST_MR dx = xRef - m_xRefTr;
        DIST_MR dy = yRef - m_yRefTr;

        DIST_MR dd_2 = dx * dx + dy * dy;
        ANGLERAD_MR dAng = abs(NormAngle_MinusPI_PI(AngleRef - m_AngleRefTr));
        SPEED_MR dSpeed = abs(SpeedRef - m_SpeedRefTr);

        return (dd_2 > m_TrajDistTolerance * m_TrajDistTolerance || dAng > m_TrajAngleTolerance || dSpeed > m_TrajSpeedTolerance);
    }

public:
    bool IsValid() const
    {
        if(!IsValidParms())
            return false;

        if(!m_TrajPlanner.IsValid())
            return false;

        if(!m_TrackingController.IsValid())
            return false;

        return true;
    }

    bool IsValidParms() const
    {
        if(!(m_GoalDistTolerance >= 0 && m_GoalAngleTolerance >= 0 && m_GoalSpeedTolerance >= 0 && m_TrajDistTolerance >= 0 &&
             m_TrajAngleTolerance >= 0 && m_TrajSpeedTolerance >= 0 && m_MaxRobotSpeed > 0))
            return false;

        return true;
    }

    void Reset()
    {
        m_bDone = false;
        m_bTrajectoryConstructed = false;
        m_bRobotWasBehindGoal = false;

        m_TrackingController.Reset();
    }

    bool IsDone() const { return m_bDone; }

protected:
    CPosAngleTrajPlanner1<T> m_TrajPlanner; // Planer trajektrorije (krugovi+pravci, maks brzina)

    CTrajTrackingRegulator_Nonlinear<T> m_TrackingController;

    // Referentna tocka za koju je konstruirana trajektorija
    COORD_MR m_xRefTr;
    COORD_MR m_yRefTr;
    ANGLERAD_MR m_AngleRefTr;
    SPEED_MR m_SpeedRefTr;

    // Tolerancije za trajektoriju. Ako se se ref. tocka promijenila vise od tolerancije u odnosu na
    // poc. vrijednost, tada se konstruira nova trajektorija.
    DIST_MR m_TrajDistTolerance;
    ANGLERAD_MR m_TrajAngleTolerance;
    SPEED_MR m_TrajSpeedTolerance;

    // Tolerancije pogreske (uzduzna e1, poprecna e2, kutna e3).
    // Ako je trenutna pogreska slijedenja veca od ove, konstruira se nova trajektorija.
    T m_e1Tol, m_e2Tol, m_e3Tol;

    // Tolerancije za odredivanje da li je dostignut cilj
    DIST_MR m_GoalDistTolerance;
    ANGLERAD_MR m_GoalAngleTolerance;
    SPEED_MR m_GoalSpeedTolerance;

    // Parametri robota
    SPEED_MR m_MaxRobotSpeed;

    bool m_bTrajectoryConstructed; // Flag koji oznacava da li je trajektorija konstruirana
    bool m_bDone;                  // Da li smo stigli u zeljeno stanje (s zadanom tolerancijom)
    bool m_bRobotWasBehindGoal;    // Da li je robot ikada bio iza ciljne tocke

    // Nakon sto done prijede u true, vise se kasnije ne provjerava
    // i ne prelazi u false, osim nakon poziva Reset(). Ukoliko je
    // ova zastavica true, robot ce nastaviti slijediti zadnju konstruiranu
    // trajektoriju.
    bool m_bContinueAfterDone;

    bool m_bInverted;   // Dal je invertiran
    bool m_bAutoInvert; // Dal ga ova klasa smije sama invertirati ovisno o situaciji

public:
    CTrajPathProfile* GetCurrentPath()
    {
        C2DTrajectoryBase* pTraj = m_TrajPlanner.GetTrajectory();

        if(pTraj)
            return pTraj->GetCurrentPath();

        return nullptr;
    }

    CTrajTimeProfile* GetCurrentTimeProfile()
    {
        C2DTrajectoryBase* pTraj = m_TrajPlanner.GetTrajectory();

        if(pTraj)
            return pTraj->GetCurrentTimeProfile();

        return nullptr;
    }

    // Za invertiranje
    void SetAutoInvert(bool bAutoInvert) { m_bAutoInvert = bAutoInvert; }
    bool GetAutoInvert() const { return m_bAutoInvert; }
    bool IsInverted() const { return m_bInverted; }
};

#endif