#pragma once

#include "2DTrajectoryBase.h"
#include "MobileRobot.h"

//--------------------------------------------------------------------------------
// Bazna klasa za planere trajektorije.
//--------------------------------------------------------------------------------
template<class T>
class CTrajectoryPlannerBase : public C2DTrajectoryDataTypes<T>
{
public:
    CTrajectoryPlannerBase() { m_bAutoInvert = true; }

    // Planira trajektoriju
    virtual C2DTrajectoryBase* PlanTrajectory(typename CMobileRobot::SRobotState* pBeginState, // In
                                              typename CMobileRobot::SRobotState* pEndState,   // In
                                              typename CMobileRobot* pRobot                    // In
    )
    {
        return false;
    };

    // Vraca pokazivac na vec isplaniranu trajektoriju (ako je prethodno isplanirana sa PlanTrajectory())
    virtual C2DTrajectoryBase* GetTrajectory() { return nullptr; }

    virtual bool IsValid() const { return true; }

    // Za invertiranje
    void SetAutoInvert(bool bAutoInvert) { m_bAutoInvert = bAutoInvert; }
    bool GetAutoInvert() const { return m_bAutoInvert; }
    virtual bool IsInverted() const
    {
        assert(0); // Ovu funkciju treba overrajdati
        return false;
    }

protected:
    bool m_bAutoInvert; // Dal ga ova klasa smije dati invertiranu trajektoriju (ovisno o situaciji)
};
