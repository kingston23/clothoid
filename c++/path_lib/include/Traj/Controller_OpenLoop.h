#ifndef _Controller_OpenLoop_h_
#define _Controller_OpenLoop_h_

#include "Controller_Base.h"

//=======================================================================================================================================
//
// "Regulator" za slijedenje trajektorije u open-loop nacinu rada (bez povratne veze).
//
//=======================================================================================================================================

template<typename T>
class CController_OpenLoop : public CController_Base<T>
{
public:
    CController_OpenLoop() { m_Name = CN_OpenLoop; }

    //_____________________________________________________________________________________________________________________
    //
    // Parametri sempla
    //_____________________________________________________________________________________________________________________
    struct SSampleParms
    {
        T Time; // Trenutno vrijeme
    };

    //_____________________________________________________________________________________________________________________
    //
    // Dobivanje ref. vrijednosti za svaki novi sample.
    // pParms - struktura s ulaznim parametrima
    // pRefPoint - u ovoj strukturi dobije se ref. brzina i ref. zakrivljenost (iz koje se moze dobiti ref. kutna brzina).
    //_____________________________________________________________________________________________________________________
    bool GetRefValues(SSampleParms* pParms,                               // IN
                      typename C2DTrajectoryBase::S2DTrajPoint* pRefPoint // OUT
    )
    {
        assert(pParms && pRefPoint);

        if(!IsValid())
        {
            assert(0);
            return false;
        }

        // Trenutno vrijeme
        T Time = pParms->Time;

        // Reference
        pRefPoint->Time = Time;
        if(!m_pTraj->GetPointByTime(pRefPoint))
            return false;

        return true;
    }

    //_____________________________________________________________________________________________________________________
    //
    // Parametri za inicijalizaciju regulatora
    //_____________________________________________________________________________________________________________________
    struct SParms
    {
        // Reference se generiraju iz trajektorije:
        C2DTrajectoryBase* pTraj;
    };

    //_____________________________________________________________________________________________________________________
    //
    // Postavljanje parametara regulatora
    //_____________________________________________________________________________________________________________________
    bool SetParms(SParms* pParms)
    {
        m_pTraj = pParms->pTraj;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        return true;
    }

    //_____________________________________________________________________________________________________________________
    //
    // Postavljanje trajektorije iz koje se generiraju reference
    //_____________________________________________________________________________________________________________________
    bool SetTrajectory(C2DTrajectoryBase* pTraj)
    {
        if(!pTraj)
        {
            assert(0);
            return false;
        }
        m_pTraj = pTraj;

        return true;
    }

    bool IsValid() const
    {
        if(!IsValidParms())
            return false;

        if(!m_pTraj)
            return false;

        return true;
    }

    bool IsValidParms() const { return true; }

private:
    // Referentna trajektorija koju slijedimo u open loop petlji
    C2DTrajectoryBase* m_pTraj;
};

#endif // _Controller_OpenLoop_h_