#ifndef _Smooth8Traj_h_
#define _Smooth8Traj_h_

#include "2DTrajectoryBase.h"
#include "Math/Functions.h"

#include <assert.h>
#include <cmath>

using namespace MathMB;

//************************************************************************************************************************************************************************************
//
// Trajektorija po glatkoj osmici (horizontalno polozenoj).
// Ova verzija nema glatki profil brzine (tj. nema ubrzavanja od v=0 do v=v_stac
// zadanom akceleracijom i natrag do v=0 zadanom deceleracijom).
// Takoder ne moze se dobiti trajektorija kao funkcija u ovisnosti o pomaku, nego samo u ovisnosti o vremenu.
// Sluzi za testiranje algoritama za slijedenje trajektorije.
// Parametarska jednadzba je (to je zapravo specijalan slucaj Lissajous-ove krivulje):
// x(t) = x0 + w*sin(alpha*vMax*t)
// y(t) = y0 + h/2*sin(2*alpha*vMax*t),
// gdje je x0,y0 srediste pravokutnika koji opisuje krivulju, a w,h su sirina i visina jedne polovice pravokutnika (lijeve ili desne, a u
// svakoj polovici pravokutnika je pola osmice), alpha je parametar koji skalira stvar tako da maksimalna brzina putovanja bude vMax, a
// iznosi alpha = 1/sqrt(w*w+h*h) t je ulazni parametar, najcesce vrijeme Krivulja pocinje u tocki (x0, y0), tj. u sredini pravokutnika.
//
//************************************************************************************************************************************************************************************
//template<class T>
class CSimpleSmooth8Traj : public C2DTrajectoryBase
{
public:
    CSimpleSmooth8Traj() { m_bParmsOK = false; }

    virtual ~CSimpleSmooth8Traj() {}

    bool IsValid() const
    {
        if(!C2DTrajectoryBase::IsValid())
        {
            assert(0);
            return false;
        }

        assert(m_bParmsOK);
        return m_bParmsOK;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Dohvat tocke trajektorije u zeljenom vremenskom trenutku.
    // Ova funkcija vraca samo x, y, kut, linearnu brzinu, zakrivljenost i akceleraciju -> dakle zasad nema pomaka i prijedenog puta
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());
        if(!m_bParmsOK)
            return false;

        T Time = pTrPoint->Time;
        T DeltaTime = Time - m_t0;

        if(DeltaTime < m_AccelerationDuration)
        {
            // Ubrzavanje po pravcu
            if(DeltaTime < 0)
            {
                // Stajanje u pocetnoj tocki
                DeltaTime = 0;
            }

            T accel = m_vMax * m_vMax / (2 * m_AccelerationLength);

            // proracun
            T Angle0 = atan2(m_Height, m_Width);
            T x0 = m_x0 - m_AccelerationLength * cos(Angle0);
            T y0 = m_y0 - m_AccelerationLength * sin(Angle0);

            T dist = accel / 2 * DeltaTime * DeltaTime;

            pTrPoint->x = x0 + cos(Angle0) * dist;
            pTrPoint->y = y0 + sin(Angle0) * dist;
            pTrPoint->Angle = Angle0;
            pTrPoint->Velocity = accel * DeltaTime; // tudu
            pTrPoint->Curv = 0;
            pTrPoint->Accel = accel;
            pTrPoint->AngVelocity = 0;
            pTrPoint->AngAccel = 0;
        }
        else
        {
            // Osmica blues

            DeltaTime -= m_AccelerationDuration;

            if(DeltaTime > m_EightDuration)
            {
                // Stajanje u krajnjoj tocki
                DeltaTime = m_EightDuration;
            }

            T Alpha =
                1 / sqrt(m_Width * m_Width +
                         m_Height * m_Height); // Faktor za skaliranje koji omogucava da se postigne maksimalna brzina trajektorije m_vMax

            T S = sin(Alpha * m_vMax * DeltaTime);
            T S2 = sin(2 * Alpha * m_vMax * DeltaTime);

            T C = cos(Alpha * m_vMax * DeltaTime);
            T C2 = cos(2 * Alpha * m_vMax * DeltaTime);

            pTrPoint->x = m_x0 + m_Width * S;
            pTrPoint->y = m_y0 + m_Height / 2 * S2;

            T dx = m_Width * Alpha * m_vMax * C;
            T dy = m_Height * Alpha * m_vMax * C2;

            T ddx = -m_Width * Alpha * Alpha * m_vMax * m_vMax * S;
            T ddy = -2 * m_Height * Alpha * Alpha * m_vMax * m_vMax * S2;

            T dddx = -m_Width * Alpha * Alpha * Alpha * m_vMax * m_vMax * m_vMax * C;
            T dddy = -4 * m_Height * Alpha * Alpha * Alpha * m_vMax * m_vMax * m_vMax * C2;

            pTrPoint->Angle = atan2(dy, dx);
            pTrPoint->Velocity = sqrt(dx * dx + dy * dy); // Ne bi nikad trebalo biti nula
            assert(pTrPoint->Velocity != 0);
            pTrPoint->Curv = (dx * ddy - dy * ddx) / ((dx * dx + dy * dy) * pTrPoint->Velocity);

            pTrPoint->Accel = (dx * ddx + dy * ddy) /
                              pTrPoint->Velocity; // Na pocetku (osim ak nema zaletavanja) i na kraju je akceleracija zapravo beskonacna

            // Kutna brzina
            T num = (dx * ddy - dy * ddx);
            T den = (dx * dx + dy * dy);
            T w = num / den;

            // Kutna akceleracija - je malo zajebanija
            T dnum = dx * dddy - dy * dddx;
            T dden = 2 * dx * ddx + 2 * dy * ddy;
            T dw = (dnum * den - num * dden) / (den * den);

            pTrPoint->bAngVelocityFromCurvature = false;
            pTrPoint->bAngAccelFromCurvature = false;

            pTrPoint->AngVelocity = w;
            pTrPoint->AngAccel = dw;
        }

        /*pTrPoint->Disp = Disp;
        pTrPoint->Dist = Disp;*/

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Struktura za zadavanje parametara trajektorije.
    // Za objasnjenje parametara vidi pod objasnjenjem analognih clanova klase.
    //____________________________________________________________________________________________________________________________________________________________________________________
    struct SParms1
    {
        bool bInitialAcceleration;

        T AccelerationLength;

        T StartTime;     // Pocetni offset vremena
        T EightDuration; // (>=0) Koliko dugo vozimo po trajektoriji
        T vMax;          // Maksimalna brzina koju trajektorija dostize

        T x0, y0; // Srediste pravokutnika koji obuhvaca cijelu putanju
        T Width;  // (>=0) Sirina pravokutnika koji obuhvaca cijelu putanju
        T Height; // (>=0) Visina pravokutnika koji obuhvaca cijelu putanju
    };

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje parametara.
    // Na kraju se poziva CheckParms koji vraca false ako parametri ne valjaju ili ih korigira.
    // Ovu funkciju uvijek treba pozvati prije koristenja klase.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool SetParms(SParms1* pParms)
    {
        m_bParmsOK = false;

        m_bInitialAcceleration = pParms->bInitialAcceleration;
        m_AccelerationLength = pParms->AccelerationLength;

        m_t0 = pParms->StartTime;
        m_EightDuration = pParms->EightDuration;

        m_vMax = pParms->vMax;

        m_x0 = pParms->x0;
        m_y0 = pParms->y0;

        m_Width = pParms->Width;
        m_Height = pParms->Height;

        if(!CheckParms())
        {
            return false;
        }

        if(m_bInitialAcceleration)
            m_AccelerationDuration = 2 * m_AccelerationLength / m_vMax;
        else
            m_AccelerationDuration = 0;

        m_bParmsOK = true;

        return true;
    };

    T GetBeginTime() const { return m_t0; };
    T GetDeltaTime() const { return m_AccelerationDuration + m_EightDuration; }
    void SetBeginTime(T BeginTime) { m_t0 = BeginTime; };

protected:
    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera parametara trajektorije.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool CheckParms()
    {
        // Provjeravam predznake parametara

        if(m_vMax < 0)
            return false;

        if(m_EightDuration < 0)
            return false;

        if(m_Width < 0)
            return false;

        if(m_Height < 0)
            return false;

        return true;
    };

    // Parametri
protected:
    // Da li zelimo da na pocetku robot jednoliko ubrzava po ravnoj trajektoriji, i tek onda krene po osmici.
    // Ovo je napravljeno iz razloga jer osmica odmah pocinje maksimalnom brzinom, pa bi imali veliku pogresku brzine na pocetku osmice.
    bool m_bInitialAcceleration;

    T m_AccelerationLength; // Duljina pocetnog ravnog puta za ubrzavanje. Akceleracija ce biti a = vMax^2/(2*AccelerationLength)

    T m_AccelerationDuration;

    // Po�etni uvjeti
    T m_t0;            // Pocetno vrijeme
    T m_EightDuration; // Trajanje voznje po osmici (bez dijela za ubrzavanje)

    T m_vMax;

    T m_x0, m_y0;        // Srediste pravokutnika koji obuhvaca krivulju
    T m_Width, m_Height; // Sirina i visina pravokutnika koji obuhvaca krivulju

protected:
    bool m_bParmsOK; // Ako su parametri zadani i provjereni, ovo je true

#ifdef DBG_FILE_
    FILE* dbgf;
#endif
};

//************************************************************************************************************************************************************************************
//
// Trajektorija po glatkoj osmici - modificirana za demo
//
//************************************************************************************************************************************************************************************
// template<class T>
// class CSmooth8TrajDemo : public CSimpleSmooth8Traj<T>
// {
// public:
//     CSmooth8TrajDemo() {}

// protected:
//     CSawFunction m_Saw;
// };

#endif // _Smooth8Traj_h_
