#ifndef _CTrajTimeProfile_h_
#define _CTrajTimeProfile_h_

#include "2DCompositeTrajectory.h"
#include "2DTrajectoryBase.h"

//=================================================================================================================================================================
//
// Baza za vremenski profil trajektorije - specijalni slucaj trajektorije.
// Koristiti u slucajevima kad je potrebno odvojeno planirati vremenski i prostorni profil
// trajektorije. U tom slucaju vremenski profil definira vrijeme, pomak, brzinu i akceleraciju u
// pojedinom trenutku, a prostorni koordinate u prostoru, kut i zakrivljenost
//
//=================================================================================================================================================================
class CTrajTimeProfile : public C2DTrajectoryBase
{
public:
    //----------------------------------------------------------------------------
    // Tipovi profila staze (odnosi se na geometrijski oblik)
    //----------------------------------------------------------------------------
    enum ETimeProfileType
    {
        TP_None,
        TP_Composite, // Znaci da je objekt klase CTrajCompositeTimeProfile
        TP_UniformPosition,
        TP_UniformSpeed,
        TP_UniformAccel,
    };

    //----------------------------------------------------------------------------------
    // Vraca tip vremenskog profila (da li je jednoliko ubrzano gibanje, jednoliko itd).
    // Obavezno implementirati u izvedenim klasama!
    //----------------------------------------------------------------------------------
    virtual ETimeProfileType GetTimeProfileType() const { return TP_None; }

    virtual bool IsValid() const
    {
        if(!C2DTrajectoryBase::IsValid())
            return false;

        return true;
    }
};

//=================================================================================================================================================================
//
// Klasa za vremenski profil koji se sastoji od vise dijelova.
//
//=================================================================================================================================================================
template<class T>
class CTrajCompositeTimeProfile : public C2DCompositeTrajectory, public CTrajTimeProfile
{
public:
    //----------------------------------------------------------------------------
    // Vraca tip vremenskog profila - u ovom slucaju slozeni profil koji se
    // sastoji od vise podprofila
    //----------------------------------------------------------------------------
    virtual ETimeProfileType GetTimeProfileType() const { return TP_Composite; }

    virtual bool IsValid() const
    {
        if(!C2DCompositeTrajectory::IsValid())
            return false;

        if(!CTrajTimeProfile::IsValid())
            return false;

        return true;
    }

    bool AddTrajComponent(CTrajTimeProfile* pNewTraj) { return C2DCompositeTrajectory::AddTrajComponent(pNewTraj); }

    CTrajTimeProfile* GetTrajComponent(unsigned int iTrajComp) const
    {
        return (CTrajTimeProfile*)(C2DCompositeTrajectory::GetTrajComponent(iTrajComp));
    }

    bool GetPointByTime(S2DTrajPoint* pTrPoint) const { return C2DCompositeTrajectory::GetPointByTime(pTrPoint); }

    bool GetPointByDisp(S2DTrajPoint* pTrPoint) const { return C2DCompositeTrajectory::GetPointByDisp(pTrPoint); };

    bool GetPointByDist(S2DTrajPoint* pTrPoint) const
    {
        assert(0); // Vremenski profil ne moze dati prostornu komponentu!!!
        return C2DCompositeTrajectory::GetPointByDist(pTrPoint);
    };

    // Ovo mora biti inace zove C2DTrajectoryBase!!!
    T GetBeginTime() const { return C2DCompositeTrajectory::GetBeginTime(); }
    T GetDeltaTime() const { return C2DCompositeTrajectory::GetDeltaTime(); }
    void SetBeginTime(T BeginTime) { C2DCompositeTrajectory::SetBeginTime(BeginTime); }

    T GetBeginDist() const { return C2DCompositeTrajectory::GetBeginDist(); }
    T GetDeltaDist() const { return C2DCompositeTrajectory::GetDeltaDist(); }
    void SetBeginDist(T BeginDist) { C2DCompositeTrajectory::SetBeginDist(BeginDist); }

    T GetBeginDisp() const { return C2DCompositeTrajectory::GetBeginDisp(); }
    T GetDeltaDisp() const { return C2DCompositeTrajectory::GetDeltaDisp(); }
    void SetBeginDisp(T BeginDisp) { C2DCompositeTrajectory::SetBeginDisp(BeginDisp); }

    // Mod ekstrapolacije staze nije potreban za vremenski profil (jer on nema veze sa
    // stazom (x,y,...), vec samo sa vremenom, brzinom itd...
};

#endif
