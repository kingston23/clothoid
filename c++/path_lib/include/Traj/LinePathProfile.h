#ifndef _Line_Path_Profile_h_
#define _Line_Path_Profile_h_

#include "TrajPathProfile.h"

class CLinePathProfile : public CTrajPathProfile
{
public:
    CLinePathProfile() { m_bParmsSet = false; }

    virtual EPathProfileType GetPathProfileType() const
    {
        assert(IsValid());
        return PP_Line;
    }

    // Postavlja x, y, Angle i Curv ovisno o Dist parametru iz strukture.
    // Ostali clanovi strukture se ne modificiraju.
    bool GetPointByDist(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!m_bParmsSet)
            return false;

        T dd = pTrPoint->Dist - m_BeginDist;

        pTrPoint->x = m_x0 + dd * m_Cos;
        pTrPoint->y = m_y0 + dd * m_Sin;
        pTrPoint->Angle = m_Angle;
        pTrPoint->Curv = 0;

        return true;
    };

    void SetBeginDist(T BeginDist) { m_BeginDist = BeginDist; };
    T GetBeginDist() const { return m_BeginDist; }
    T GetDeltaDist() const { return m_DeltaDist; }

    inline void SetDeltaDist(T Dist)
    {
        m_DeltaDist = Dist;
        assert(IsValid());
    }

    void GetStartPoint(T& x0, T& y0) const
    {
        assert(IsValid());
        x0 = m_x0;
        y0 = m_y0;
    }
    T GetAngle() const
    {
        assert(IsValid());
        return m_Angle;
    }

    struct SParms
    {
        T BeginDist;
        T DeltaDist;
        T x0;
        T y0;
        T LineAngle;
    };

    bool SetParms(const SParms* pParms)
    {
        m_BeginDist = pParms->BeginDist;
        m_DeltaDist = pParms->DeltaDist;
        m_x0 = pParms->x0;
        m_y0 = pParms->y0;
        m_Angle = pParms->LineAngle;

        m_Cos = (T)cos(m_Angle);
        m_Sin = (T)sin(m_Angle);

        m_bParmsSet = true;

        return true;
    }

    bool IsValid() const
    {
        if(!CTrajPathProfile::IsValid())
            return false;

        return m_bParmsSet;
    }

private:
    // Parametri
    T m_BeginDist; // Pocetna udalj.
    T m_DeltaDist; // Ukupna udalj. tj. duljina
    T m_x0;        // Pocetna to�ka
    T m_y0;
    T m_Angle; // Kut pravca

    // Pomocne varijable
    T m_Cos;
    T m_Sin;

    bool m_bParmsSet;
};

#endif
