#ifndef _Offline_Traj_h_
#define _Offline_Traj_h_

#include "2DTrajectoryBase.h"

#include <cmath>
#include <vector>

//=======================================================================================================================================
//
// Trajektorija napravljena offline koju ucitavamo s diska.
//
//=======================================================================================================================================

// Definirati DBG_FILE_ za dijagnostiku (ispis upozorenja u file).

template<class T>
class COfflineTraj : public C2DTrajectoryBase
{
public:
    COfflineTraj() { Init("OfflineTraj.txt"); }

    COfflineTraj(const char* InputFile) { Init(InputFile); }

    void Init(const char* InputFile)
    {
        m_t0 = 0;
        m_Duration = 0;

        // Ucitaj tocke iz datoteke

        // FILE* TrajFile = fopen("DoktorTraj.txt", "rt"); // trajektorija za doktorat
        FILE* TrajFile = fopen(InputFile, "rt");

        if(TrajFile)
        {
            m_Points.reserve(200);

            // Redoslijed podataka u fajlu mora biti: Vrijeme Pomak(od_pocetka) x y kut lin_brzina lin_accel zakrivljenost
            // deriv_zakrivljenosti
            T time;
            T disp;
            T x, y, ang;
            T v, accel;
            T curv, dcurv;

            while(fscanf(TrajFile, "%f  %f %f %f %f %f %f %f %f\n", &time, &disp, &x, &y, &ang, &v, &accel, &curv, &dcurv) != -1)
            {
                S2DTrajPoint Point;

                Point.Time = time;
                Point.x = x;
                Point.y = y;
                Point.Angle = ang;
                Point.Velocity = v;
                Point.Accel = accel;
                Point.Disp = disp;
                Point.Curv = curv;
                Point.DCurv = dcurv;

                m_Points.push_back(Point);

                m_Duration = time;
                m_Distance = disp;
            }

            fclose(TrajFile);
        }
    }

    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        T Time = pTrPoint->Time - m_t0;

        int iPoint = -1;
        T MinDiff = (T)1e30;

        for(int i = 0; i < (int)m_Points.size(); i++)
        {
            if(fabs(m_Points[i].Time - Time) < MinDiff)
            {
                MinDiff = fabs(m_Points[i].Time - Time);
                iPoint = i;
            }
        }

        if(iPoint != -1)
        {
            pTrPoint->x = m_Points[iPoint].x;
            pTrPoint->y = m_Points[iPoint].y;
            pTrPoint->Angle = m_Points[iPoint].Angle;
            pTrPoint->Velocity = m_Points[iPoint].Velocity;
            pTrPoint->Accel = m_Points[iPoint].Accel;
            pTrPoint->Disp = m_Points[iPoint].Disp;
            pTrPoint->Curv = m_Points[iPoint].Curv;
            pTrPoint->DCurv = m_Points[iPoint].DCurv;
        }

        return true;
    }

    virtual T GetBeginTime() const { return m_t0; };

    virtual T GetDeltaTime() const { return m_Duration; }

    void SetBeginTime(T BeginTime) { m_t0 = BeginTime; };

private:
    bool CheckParms() { return true; };

    // Parametri
private:
    // Po�etni uvjeti
    T m_t0;       // Po�etno vrijeme (obi�no 0)
    T m_Duration; // Kolko traje trajektorija

    T m_Distance; // Ukupna duljina prevaljenog puta po trajektoriji

private:
    std::vector<S2DTrajPoint> m_Points;
};

#endif