#ifndef _AccConstDecTimeProfilePlanner_h_
#define _AccConstDecTimeProfilePlanner_h_
#pragma warning(                                                                                                                           \
    disable : 4786) // iskljucivanje upozorenja u VC++: "identifier was truncated to 'number' characters in the debug information"

#include "ArcPathProfile.h"
#include "Log/Log.h"
#include "Math/Numeric.h"
#include "TimeProfilePlanner.h"

#include <cmath>

using namespace MathMB;

//=================================================================================================================================================================
//
// Proracunava profil brzine za danu putanju: ubrzava (ili usporava) od pocetne brzine do v
// zadanom akceleracijom/deceleracijom i od brzine v do ciljne brzine, zadanom deceleracijom/akceleracijom.
// TUDU: ponasanje klase nije ispitano u slucaju da su pocetna ili ciljna brzina < 0. Moglo bi biti problema pa treba biti na oprezu.
//
//=================================================================================================================================================================
template<typename T>
class CAccConstDecTimeProfilePlanner : public CTimeProfilePlannerBase<T>
{
public:
    CAccConstDecTimeProfilePlanner() {}

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Struktura za zadavanje parametara trajektorije
    //____________________________________________________________________________________________________________________________________________________________________________________
    struct SParms
    {
        T StartTime; // Pocetni offseti vremena i udaljenosti
        T StartDisp;
        T v;      // Stacionarna brzina (>0)
        T Accel1; // akceleracija na pocetku (!=0)
        T Accel2; // akceleracija na kraju (!=0)
    };

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje parametara.
    // Na kraju se poziva CheckParms koji vraca false ako parametri ne valjaju.
    // Ovu funkciju uvijek treba pozvati prije koristenja klase.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool SetParms(SParms* pParms)
    {
        m_t0 = pParms->StartTime;
        m_Disp0 = pParms->StartDisp;

        m_v = pParms->v;
        m_Accel1 = pParms->Accel1;
        m_Accel2 = pParms->Accel2;

        if(CheckParms())
        {
            return true;
        }
        return false;
    };

public:
    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Planiranje vremenskog profila trajektorije
    //____________________________________________________________________________________________________________________________________________________________________________________
    CTrajTimeProfile* PlanTimeProfile(const CTrajCompositePathProfile* pPathProfile,         // [in] putanja
                                      const typename CMobileRobot::SRobotState* pBeginState, // [in] pocetno stanje - vazna je samo brzina
                                      const typename CMobileRobot::SRobotState* pEndState,   // [in] konacno stanje - vazna je samo brzina
                                      const CMobileRobot* pRobot                             // [in] podaci o robotu (ovdje nisu potrebni)
    )
    {
        assert(pBeginState->Velocity >= 0); // moguce da radi i bez ovog uvjeta, ali nije ispitano
        assert(pEndState->Velocity >= 0);   // moguce da radi i bez ovog uvjeta, ali nije ispitano

        m_TimeProfile.RemoveAllTrajs();

        T DispTotal = pPathProfile->GetDeltaDist(); // ukupni pomak jednak je duljini putanje, jer se pretpostavlja da su sve brzine cijelo
                                                    // vrijeme pozitivne, nema rikverc!

        T vBegin = pBeginState->Velocity;
        T dt01 = (m_v - vBegin) / m_Accel1;
        if(dt01 < 0)
        {
            // Vjerojatno je pocetna brzina veca od zeljene, promijenimo predznak akceleracije
            dt01 = -dt01;
            m_Accel1 = -m_Accel1;
        }
        T Disp01 = (T)0.5 * (vBegin + m_v) * dt01;

        T vEnd = pEndState->Velocity;
        T dt23 = (vEnd - m_v) / m_Accel2;
        if(dt23 < 0)
        {
            // Vjerojatno je pocetna brzina veca od zeljene, promijenimo predznak akceleracije
            dt23 = -dt23;
            m_Accel2 = -m_Accel2;
        }
        T Disp23 = (T)0.5 * (m_v + vEnd) * dt23;

        T Disp12 = DispTotal - Disp01 - Disp23;
        T dt12;
        if(Disp12 > 0)
        {
            dt12 = Disp12 / m_v;
        }
        else
        {
            // Ne mo�e se ubrzati do zadane brzine i natrag do ciljne na tako kratkoj udaljenosti
            // Sada su dvije opcije: vratiti false ili se prilagoditi

            // Maksimalna brzina koja se mo�e posti�i
            T vmax = (T)sqrt(2 * DispTotal * abs(m_Accel1) * abs(m_Accel2) / (abs(m_Accel1) + abs(m_Accel2)));
            m_v = vmax;

            dt01 = vmax / abs(m_Accel1);
            // Disp01 = vmax*dt01/2;

            dt23 = vmax / abs(m_Accel2);
            // Disp23 = vmax*dt23/2;

            dt12 = 0;
            // Disp12 = 0;
        }

        /*m_t1 = m_t0 + dt01;
        m_t2 = m_t1 + dt12;
        m_t3 = m_t2 + dt23;

        m_Disp1 = m_Disp0 + Disp01;
        m_Disp2 = m_Disp1 + Disp12;
        m_Disp3 = m_Disp2 + Disp23;*/

        // Profil s ubrzanjem
        if(CNum<T>::IsPositive(dt01))
        {
            CUniformAccelTimeProfile<T>::SParms1 Parms;

            Parms.BeginTime = m_t0;
            Parms.BeginDisp = m_Disp0;
            Parms.BeginDist = pPathProfile->GetBeginDist();
            Parms.DeltaTime = dt01;
            Parms.BeginSpeed = vBegin;
            Parms.Accel = m_Accel1;

            if(!m_AccelProfile.SetParms(&Parms))
                return false;

            if(!m_TimeProfile.AddTrajComponent(&m_AccelProfile))
                return false;
        }

        // Profil s konst brzinom
        if(CNum<T>::IsPositive(dt12))
        {
            CUniformSpeedTimeProfile<T>::SParms1 Parms;
            Parms.BeginTime = m_t0;                         // Nevazno, osim ako je duljina profila ubrzanja jednaka nuli
            Parms.BeginDisp = m_Disp0;                      // Nevazno, osim ako je duljina profila ubrzanja jednaka nuli
            Parms.BeginDist = pPathProfile->GetBeginDist(); // Nevazno, osim ako je duljina profila ubrzanja jednaka nuli
            Parms.DeltaTime = dt12;
            Parms.Speed = m_v;

            if(!m_ConstSpeedProfile.SetParms(&Parms))
                return false;

            if(!m_TimeProfile.AddTrajComponent(&m_ConstSpeedProfile))
                return false;
        }

        // Profil s usporavanjem
        if(CNum<T>::IsPositive(dt23))
        {
            CUniformAccelTimeProfile<T>::SParms1 Parms;

            Parms.BeginTime = m_t0;                         // Nevazno, osim ako je duljina profila ubrzanja jednaka nuli
            Parms.BeginDisp = m_Disp0;                      // Nevazno, osim ako je duljina profila ubrzanja jednaka nuli
            Parms.BeginDist = pPathProfile->GetBeginDist(); // Nevazno, osim ako je duljina profila ubrzanja jednaka nuli
            Parms.DeltaTime = dt23;
            Parms.BeginSpeed = m_v;
            Parms.Accel = m_Accel2;

            if(!m_DeccelProfile.SetParms(&Parms))
                return false;

            if(!m_TimeProfile.AddTrajComponent(&m_DeccelProfile))
                return false;
        }

        return &m_TimeProfile;
    }

protected:
    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera parametara trajektorije i racuna pomocne varijable.
    // U slucaju lose zadanih parametara, vraca false.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool CheckParms() const
    {
        // Provjeravam predznake parametara
        if(m_v <= 0)
            return false;

        if(CNum<T>::IsZero(m_Accel1))
            return false;

        if(CNum<T>::IsZero(m_Accel2))
            return false;

        return true;
    };

    bool AddConstPosTimeProfile()
    {
        CUniformPositionTimeProfile<T>::SParms Parms;

        // Svi parametri su skroz nevazni
        Parms.DeltaTime = (T)0.1;
        Parms.BeginDist = (T)0.0;
        Parms.BeginTime = (T)0.0;
        Parms.BeginDisp = (T)0.0;

        if(!m_ConstPosProfile.SetParms(&Parms))
            return false;

        if(!m_TimeProfile.AddTrajComponent(&m_ConstPosProfile))
            return false;

        return true;
    }

public:
    bool IsValid() const
    {
        if(!CheckParms())
            return false;

        if(!m_TimeProfile.IsValid())
            return false;

        return true;
    }

protected:
protected:
    // Parametri

    T m_v;      // Stacionarna brzina
    T m_Accel1; // Akceleracija od pocetne brzine do brzine m_v
    T m_Accel2; // Akceleracija od brzine m_v do ciljne brzine.
    T m_t0;     // pocetno vrijeme - u tocki 0
    T m_Disp0;  // Pocetni pomak - u tocki 0

    // Profili

    CUniformSpeedTimeProfile<T> m_ConstSpeedProfile;             // za konst brzinu
    CUniformAccelTimeProfile<T> m_AccelProfile, m_DeccelProfile; // za ubrzavanje i usporavanje
    // CUniformPositionTimeProfile<T> m_ConstPosProfile; // Ovo moze biti potrebno na kraju

    CTrajCompositeTimeProfile<T> m_TimeProfile;
};

#endif // _MaxSpeedCircleLineTimeProfile_h_
