#ifndef _TrajPathProfile_h_
#define _TrajPathProfile_h_

#include "Geom/Line.h"
#include "2DCompositeTrajectory.h"
#include "2DTrajectoryBase.h"

//=================================================================================================================================================================
//
// Baza za putanju - tj. prostorni profil trajektorije.
// Koristiti u slucajevima kad je potrebno odvojeno planirati vremenski i prostorni profil
// trajektorije. U tom slucaju vremenski profil definira pomak, brzinu i akceleraciju u
// pojedinom trenutku, a prostorni koordinate u prostoru, kut i zakrivljenost.
//
// Izvedena klasa mora obavezno implementirati funkcije:
// GetPointByDist(), SetBeginDist(), GetBeginDist(), GetTotalDist()
//
//=================================================================================================================================================================
class CTrajPathProfile : virtual public C2DTrajectoryBase
{
public:
    //----------------------------------------------------------------------------
    // Tipovi profila staze (odnosi se na geometrijski oblik)
    //----------------------------------------------------------------------------
    enum EPathProfileType
    {
        PP_None,
        PP_Composite, // Znaci da je objekt klase CTrajCompositePathProfile
        PP_Line,      // klasa CLinePathProfile
        PP_Circle,    // klasa CArcPathProfile
        PP_Eight,
        PP_Clothoid, // klasa CClothoidPathProfile
    };

    //________________________________________________________________________________________________________________________________________________
    //
    // Testiranje tipova putanja (dodati jos funkcija za ostale tipove po potrebi)
    //________________________________________________________________________________________________________________________________________________
    inline bool IsLine() const { return (GetPathProfileType() == PP_Line); }

    //----------------------------------------------------------------------------
    // Vraca tip profila staze (da li je krug, linija itd.).
    // Obavezno implementirati u izvedenim klasama!
    //----------------------------------------------------------------------------
    virtual EPathProfileType GetPathProfileType() const { return PP_None; }

    //----------------------------------------------------------
    // Zahtjevi koji se provjeravaju u IsValid():

    virtual bool IsValid() const
    {
        if(!C2DTrajectoryBase::IsValid())
            return false;

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Pomocna funkcija za ekstrapolaciju putanje pravcem.
    // Koristi se kad se trazi tocka putanja izvan podrucja puta na kojem je ona definirana.
    // Ako se trazi ekstrapolacija prije pocetka putanje, potrebno je dati tocku putanje pTrajPoint izracunatu za pocetnu tocku putanje,
    // a ova funkcija ce modificirati tu tocku ekstrapolacijom.
    // Isto tak, ak se trazi ekstrapolacija nakon kraja putanje, potrebno je dati tocku putanje pTrajPoint izracunatu za krajnju tocku
    // putanje, a ova funkcija ce modificirati tu tocku ekstrapolacijom.
    //________________________________________________________________________________________________________________________________________________
    bool ExtrapolateLine(S2DTrajPoint* pTrajPoint) const
    {
        Geom::CLinePar Line;
        Line.LineFromDirectionAndPoint(pTrajPoint->Angle, pTrajPoint->x, pTrajPoint->y);

        if(pTrajPoint->Dist < GetBeginDist())
            Line.GetPoint(pTrajPoint->Dist - GetBeginDist(), pTrajPoint->x, pTrajPoint->y);
        else
            Line.GetPoint(pTrajPoint->Dist - GetEndDist(), pTrajPoint->x, pTrajPoint->y);

        return true;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Ekstrapolacija putanje kruznim lukom
    //________________________________________________________________________________________________________________________________________________
    bool ExtrapolateArc(S2DTrajPoint* pTrajPoint) const
    {
        assert(0); // neimplementirano
        return true;
    }
};

//=================================================================================================================================================================
//
// Klasa za putanju sastavljenu od vise razlicitih segmenata. Npr. moze se postaviti sekvenca pravac - klotoida - luk - klotoida - pravac
// ...
//
//=================================================================================================================================================================
class CTrajCompositePathProfile : public C2DCompositeTrajectory, public CTrajPathProfile
{
public:
    //----------------------------------------------------------------------------
    // Vraca tip profila staze - u ovom slucaju slozeni profil koji se
    // sastoji od vise podprofila
    //----------------------------------------------------------------------------
    virtual EPathProfileType GetPathProfileType() const { return PP_Composite; }

    virtual bool IsValid() const
    {
        if(!C2DCompositeTrajectory::IsValid())
            return false;

        if(!CTrajPathProfile::IsValid())
            return false;

        return true;
    }

    inline bool LogTrajectoryByDist(const char* FileName, T Delta) const { return C2DTrajectoryBase::LogTrajectoryByDist(FileName, Delta); }

    inline bool AddTrajComponent(CTrajPathProfile* pNewTraj) { return C2DCompositeTrajectory::AddTrajComponent(pNewTraj); }

    CTrajPathProfile* GetTrajComponent(unsigned int iTrajComp) const
    {
        assert(IsValid());
        return dynamic_cast<CTrajPathProfile*>(C2DCompositeTrajectory::GetTrajComponent(iTrajComp));
    }

    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        assert(0); // Prostorni profil ne moze dati vremensku komponentu!!!
        return C2DCompositeTrajectory::GetPointByTime(pTrPoint);
    }

    bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        assert(0); // Prostorni profil ne moze dati vremensku komponentu!!!
        return C2DCompositeTrajectory::GetPointByDisp(pTrPoint);
    };

    bool GetPointByDist(S2DTrajPoint* pTrPoint) const { return C2DCompositeTrajectory::GetPointByDist(pTrPoint); };

    // Ovo mora biti inace zove C2DTrajectoryBase!!!
    T GetBeginTime() const { return C2DCompositeTrajectory::GetBeginTime(); }
    T GetDeltaTime() const { return C2DCompositeTrajectory::GetDeltaTime(); }
    void SetBeginTime(T BeginTime) { C2DCompositeTrajectory::SetBeginTime(BeginTime); }

    T GetBeginDist() const { return C2DCompositeTrajectory::GetBeginDist(); }
    T GetDeltaDist() const { return C2DCompositeTrajectory::GetDeltaDist(); }
    void SetBeginDist(T BeginDist) { C2DCompositeTrajectory::SetBeginDist(BeginDist); }

    T GetBeginDisp() const { return C2DCompositeTrajectory::GetBeginDisp(); }
    T GetDeltaDisp() const { return C2DCompositeTrajectory::GetDeltaDisp(); }
    void SetBeginDisp(T BeginDisp) { C2DCompositeTrajectory::SetBeginDisp(BeginDisp); }

    // Mod ekstrapolacije odreden je baznom C2DCompositeTrajectory klasom
    EPathExtrapolationMode GetPathBeginExtrapolationMode() const { return C2DCompositeTrajectory::GetPathBeginExtrapolationMode(); }

    EPathExtrapolationMode GetPathEndExtrapolationMode() const { return C2DCompositeTrajectory::GetPathEndExtrapolationMode(); }

    bool SetPathBeginExtrapolationMode(EPathExtrapolationMode BeginMode)
    {
        return C2DCompositeTrajectory::SetPathBeginExtrapolationMode(BeginMode);
    }

    bool SetPathEndExtrapolationMode(EPathExtrapolationMode EndMode)
    {
        return C2DCompositeTrajectory::SetPathEndExtrapolationMode(EndMode);
    }
};

#endif // _TrajPathProfile_h_
