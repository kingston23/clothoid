#ifndef _MobileRobot_h_
#define _MobileRobot_h_

#include "Math/Constants.h"

#include <algorithm>
#include <assert.h>

class CTrajPathProfile;

class CTrajTimeProfile;

//=================================================================================================================================================================
//
// Definira podatkovne tipove koje treba koristiti u svim funkcijama radi fleksibilnosti
// i citljivosti programa!
//
//=================================================================================================================================================================
class CMobileRobotDataTypes
{
public:
    typedef double T;

    // Definicije tipova
    typedef T COORD_MR;
    typedef T DIST_MR;
    typedef T DISP_MR;
    typedef T TIME_MR;

    // Brzine i akceleracije
    typedef T VELOCITY_MR;
    typedef VELOCITY_MR SPEED_MR; // Sinonimi
    typedef T ACCEL_MR;

    // Kutne brzine i akceleracije
    typedef T ANGVELOCITY_MR;
    typedef ANGVELOCITY_MR ANGSPEED_MR; // Sinonimi
    typedef T ANGACCEL_MR;

    // Kut
    typedef T ANGLE_MR;
    typedef T ANGLERAD_MR;

    // Zakriveljnost i brzina promjene zakrivljenosti (dc/ds a ne dc/dt!)
    typedef T CURV_MR;
    typedef T DCURV_MR;

    // Opcenito
    typedef T NUM_MR;

    inline static T abs(T x) { return (x >= 0 ? x : -x); }

    // Svodi na [-pi,pi>
    static inline ANGLERAD_MR NormAngle_MinusPI_PI(ANGLERAD_MR Angle)
    {
        ANGLERAD_MR PI2 = 2 * PI;
        Angle -= (ANGLERAD_MR)floor(Angle / PI2) * PI2; // Svodenje na [0,2pi> (radi i za neg. kutove)
        if(Angle > PI)                                  // na [-pi,pi>
            Angle -= PI2;
        return Angle;
    }

    static const T PI;
};

//=================================================================================================================================================================
//
// Opcenita klasa za opcenitog mobilnog robota :)
//
//=================================================================================================================================================================
class CMobileRobot : public CMobileRobotDataTypes
{
public:
    CMobileRobot()
    {
        m_SamplingPeriod = (TIME_MR)0.;
        m_NumSamplesDelay = 0;
    }

    // Ako se ovo promijeni nakon inicijalizacije, dosta toga bi moglo ne funkcionirati!!!!
    bool SetSamplingPeriod(T SamplingPeriod)
    {
        if(SamplingPeriod <= 0)
        {
            assert(0);
            return false;
        }

        m_SamplingPeriod = SamplingPeriod;
        return true;
    }

    T GetSamplingPeriod() const { return m_SamplingPeriod; }

    T GetNumSamplesDelay() const { return m_NumSamplesDelay; }

    bool SetNumSamplesDelay(T NumSamplesDelay)
    {
        if(NumSamplesDelay < 0)
        {
            assert(0);
            return false;
        }
        m_NumSamplesDelay = NumSamplesDelay;
        return true;
    }

    bool SetMaxSpeed(SPEED_MR MaxSpeed)
    {
        if(MaxSpeed <= 0)
        {
            assert(0);
            return false;
        }
        m_MaxSpeed = MaxSpeed;
        return true;
    }

    bool SetMaxAllowedSpeed(SPEED_MR MaxAllowedSpeed)
    {
        if(MaxAllowedSpeed < 0)
        {
            assert(0);
            return false;
        }
        m_MaxAllowedSpeed = MaxAllowedSpeed;
        return true;
    }

    bool SetMaxAngSpeed(T MaxAngSpeed)
    {
        if(MaxAngSpeed < 0)
        {
            assert(0);
            return false;
        }
        m_MaxAngSpeed = MaxAngSpeed;
        return true;
    }

    bool SetMaxAllowedAngSpeed(T MaxAllowedAngSpeed)
    {
        if(MaxAllowedAngSpeed < 0)
        {
            assert(0);
            return false;
        }
        m_MaxAllowedAngSpeed = MaxAllowedAngSpeed;
        return true;
    }

    bool SetMaxAccel(ACCEL_MR MaxAccel)
    {
        if(MaxAccel < 0)
        {
            assert(0);
            return false;
        }
        m_MaxAccel = MaxAccel;
        return true;
    }

    bool SetMaxAllowedAccel(ACCEL_MR MaxAllowedAccel)
    {
        if(MaxAllowedAccel < 0)
        {
            assert(0);
            return false;
        }
        m_MaxAllowedAccel = MaxAllowedAccel;
        return true;
    }

    bool SetMaxAngAccel(T MaxAngAccel)
    {
        if(MaxAngAccel < 0)
        {
            assert(0);
            return false;
        }
        m_MaxAngAccel = MaxAngAccel;
        return true;
    }

    bool SetMaxAllowedAngAccel(T MaxAllowedAngAccel)
    {
        if(MaxAllowedAngAccel < 0)
        {
            assert(0);
            return false;
        }
        m_MaxAllowedAngAccel = MaxAllowedAngAccel;
        return true;
    }

    T GetMaxSpeed() const { return m_MaxSpeed; }

    T GetMaxAllowedSpeed() const { return m_MaxAllowedSpeed; }

    T GetMaxAngSpeed() const { return m_MaxAngSpeed; }

    T GetMaxAllowedAngSpeed() const { return m_MaxAllowedAngSpeed; }

    T GetMaxAccel() const { return m_MaxAccel; }

    T GetMaxAllowedAccel() const { return m_MaxAllowedAccel; }

    T GetMaxAngAccel() const { return m_MaxAngAccel; }

    T GetMaxAllowedAngAccel() const { return m_MaxAllowedAngAccel; }

    // Pogledati tipove
    enum ERobotType
    {
        RT_UndefinedRobot,
        RT_DifferentialDriveRobot,
    };

    virtual ERobotType GetRobotType() const = 0;

    virtual bool IsValid() const = 0;

    virtual bool GoTo1(COORD_MR x, COORD_MR y, bool* pbDone = nullptr)
    {
        assert(0); // You're not supposed to be here
        return false;
    }

    virtual bool GoTo1(COORD_MR x, COORD_MR y, TIME_MR Time, bool* pbDone = nullptr)
    {
        assert(0); // You're not supposed to be here
        return false;
    }

    virtual bool GoTo2(COORD_MR x, COORD_MR y, ANGLERAD_MR Angle, bool* pbDone = nullptr)
    {
        assert(0); // You're not supposed to be here
        return false;
    }

    virtual bool GoTo2(COORD_MR x, COORD_MR y, ANGLERAD_MR Angle, TIME_MR Time, bool* pbDone = nullptr)
    {
        assert(0); // You're not supposed to be here
        return false;
    }

    virtual bool GoTo3(COORD_MR x, COORD_MR y, ANGLERAD_MR Angle, SPEED_MR Speed, bool* pbDone = nullptr)
    {
        assert(0); // You're not supposed to be here
        return false;
    }

    virtual bool GoTo3(COORD_MR x, COORD_MR y, ANGLERAD_MR Angle, SPEED_MR Speed, TIME_MR Time, bool* pbDone = nullptr)
    {
        assert(0); // You're not supposed to be here
        return false;
    }

    //------------------------------------------------------------------------------------------------
    // Definicija trenutnog stanja robota.
    // Prije koristenja drugih funkcija potrebno je postaviti minimalno ona stanja
    // u strukturi koja zahtijeva koristena funkcija (najcese x,y i kut), dakle
    // u mnogim slucajevima nece biti potrebno postaviti sva stanja.
    // Jedinice i dopusteni rasponi za pojedino stanje nisu odredeni nego ovise o
    // implementaciji i parametrima pojedine (virtualne) funkcije.
    //------------------------------------------------------------------------------------------------
    struct SRobotState
    {
        // Trenutno vrijeme.
        // Ovo vrijeme najcesce nece biti stvarno trenutno vrijeme, nego
        // ono vrijeme kada ce se ref. vrijednosti poslati robotu (ako je ono poznato).
        // Ako je delay mali, moze se koristiti i stvarno trenutno vrijeme.
        // Nije vazan apsolutni iznos ovog vremena, nego se racuna dt izmedju dva poziva.
        TIME_MR CurTime;

        // Pozicija robota
        COORD_MR x;
        COORD_MR y;
        ANGLERAD_MR Angle;

        // Linearna brzina i akceleracija
        VELOCITY_MR Velocity;
        ACCEL_MR Accel;

        // Kutna brzina i akceleracija
        ANGVELOCITY_MR AngVelocity;
        ANGACCEL_MR AngAccel;

        CURV_MR GetCurvature() const
        {
            if(Velocity != 0)
                return (CURV_MR)AngVelocity / Velocity;
            else
                return 0; // Ako je brzina 0, zakrivljenost je nedefinirana
        };
    };

    virtual void SetRobotState(const SRobotState* pRobotState)
    {
        // LOGSection(CMobileRobot_SetRobotState);

        assert(pRobotState);
        m_State = *pRobotState;

        // COORD_MR x = m_State.x;
        // COORD_MR y = m_State.y;
        // ANGLERAD_MR Angle = m_State.Angle;
        // TIME_MR CurTime = m_State.CurTime;
        // SPEED_MR Velocity = m_State.Velocity;
        // ANGSPEED_MR AngVelocity = m_State.AngVelocity;

        // LOG(x);
        // LOG(y);
        // LOG(Angle);
        // LOG(CurTime);
        // LOG(Velocity);
        // LOG(AngVelocity);
    }

    inline const SRobotState* GetRobotState() const { return &m_State; }

    virtual CTrajPathProfile* GetCurrentPath() { return nullptr; }
    virtual CTrajTimeProfile* GetCurrentTimeProfile() { return nullptr; }

protected:
    // Stanje robota
    SRobotState m_State;

    // Parametri robota
    T m_MaxSpeed;        // Prava maksimalna brzina <0, inf> (m/s)
    T m_MaxAllowedSpeed; // Maksimalno doputena brzina robota (mora biti <= od maksimalne) <0, inf>

    T m_MaxAngSpeed;        // Maksimalna kutna brzina <0, inf> (rad/s)
    T m_MaxAllowedAngSpeed; // Maks. dopustena kutna brzina <0, inf> (rad/s)

    T m_MaxAccel;        // Maksimalna akceleracija robota <0, inf> (m/s2)
    T m_MaxAllowedAccel; // Maksimalno doputena akcel. robota (mora biti <= od maksimalne) <0, inf> (m/s2)

    T m_MaxAngAccel;        // Maksimalna kutna akceleracija robota <0, inf> (rad/s2)
    T m_MaxAllowedAngAccel; // Maksimalno doputena kutna akcel. robota (mora biti <= od maksimalne) <0, inf> (rad/s2)

    T m_SamplingPeriod;  // Period uzorkovanja
    T m_NumSamplesDelay; // Koliko koraka kasnjenja imamo u sustavu upravljanja
};

//------------------------------------------------------------------------------------------
// Klasa za mobilnog robota sa diferencijalnim pogonom
//------------------------------------------------------------------------------------------
class CDiffDriveRobot : public CMobileRobot
{
public:
    CDiffDriveRobot()
    {
        m_RefSpeed = 0;
        m_RefAngSpeed = 0;

        m_bInverted = false;
        m_bInvertable = true;
    }

    // Identifikacija robota
    ERobotType GetRobotType() const { return RT_DifferentialDriveRobot; }

    bool IsValid() const
    {
        if(m_WheelDistance <= 0)
            return false;

        if(m_WheelRadius <= 0)
            return false;

        return true;
    }

    // Konverzije linearne i kutne brzine u brzinu kotaca i obrnuto.
    // Mora biti tocan parametar m_WheelDistance.
    inline void LinAngToWheelsSpeed(SPEED_MR Speed, ANGSPEED_MR AngSpeed, SPEED_MR& LeftSpeed, SPEED_MR& RightSpeed)
    {
        SPEED_MR dSpeed = AngSpeed * m_WheelDistance / 2;
        LeftSpeed = Speed - dSpeed;
        RightSpeed = Speed + dSpeed;
    }

    inline void WheelsToLinAngSpeed(SPEED_MR LeftSpeed, SPEED_MR RightSpeed, SPEED_MR& Speed, ANGSPEED_MR& AngSpeed)
    {
        Speed = (LeftSpeed + RightSpeed) / 2;
        AngSpeed = (RightSpeed - LeftSpeed) / m_WheelDistance;
    }

    // Korigira ref. brzine tako da ref. brzina niti jednog kotaca ne bude
    // veca od maksimalne, a da se istodobno zadrzi ista zakrivljenost
    // gibanja. (Omjer brzina lijevog i desnog kotaca mora biti isti ko
    // i prije.)
    void CorrectRefSpeeds()
    {
        SPEED_MR SpeedLeftRef, SpeedRightRef;
        LinAngToWheelsSpeed(m_RefSpeed, m_RefAngSpeed, SpeedLeftRef, SpeedRightRef);

        SPEED_MR HigherRefSpeed = std::max(SpeedLeftRef, SpeedRightRef);

        if(HigherRefSpeed > GetMaxSpeed())
        {
            // Radimo korekciju
            if(SpeedLeftRef > SpeedRightRef)
            {
                SpeedRightRef *= GetMaxSpeed() / SpeedLeftRef;
                SpeedLeftRef = GetMaxSpeed();
            }
            else
            {
                SpeedLeftRef *= GetMaxSpeed() / SpeedRightRef;
                SpeedRightRef = GetMaxSpeed();
            }
        }

        WheelsToLinAngSpeed(SpeedLeftRef, SpeedRightRef, m_RefSpeed, m_RefAngSpeed);
    }

    //------------------------------------------------------------------------------------------
    // Pristupne funkcije
    //------------------------------------------------------------------------------------------
    SPEED_MR GetRefSpeed() const { return m_RefSpeed; }

    SPEED_MR GetRefAngSpeed() const { return m_RefAngSpeed; }

    // Ref brzine se obavezno moraju setirati preko ovoga jer se gleda dal je robot invertiran
    void SetRefSpeed(SPEED_MR v, ANGSPEED_MR w)
    {
        m_RefSpeed = v;
        m_RefAngSpeed = w;
    };

    void SetWheelDistance(DIST_MR WheelDistance)
    {
        assert(WheelDistance > 0);
        m_WheelDistance = WheelDistance;
    }

    DIST_MR GetWheelDistance() const
    {
        assert(m_WheelDistance > 0);
        return m_WheelDistance;
    }

    void SetWheelRadius(DIST_MR WheelRadius)
    {
        assert(WheelRadius > 0);
        m_WheelRadius = WheelRadius;
    }

    DIST_MR GetWheelRadius() const
    {
        assert(m_WheelRadius > 0);
        return m_WheelRadius;
    }

    // Trenutne brzine lijevog i desnog kotaca prema podlozi (ne kutne!)
    SPEED_MR GetLeftSpeed() const { return m_State.Velocity - m_WheelDistance * m_State.AngVelocity / 2; }
    SPEED_MR GetRightSpeed() const { return m_State.Velocity + m_WheelDistance * m_State.AngVelocity / 2; }

    virtual void SetRobotState(const SRobotState* pRobotState) { CMobileRobot::SetRobotState(pRobotState); }

    // Postavlja da li se robot smije invertirati (tj. smije mu se izmjeniti prednji i zadnji kraj, tj. smije ici u rikverc, tj. voziti s
    // guzicom prema naprijed)
    virtual void SetInvertable(bool bInvertable) { m_bInvertable = bInvertable; }
    bool IsInvertable() const { return m_bInvertable; }
    bool Invert(bool bInvert)
    {
        if(IsInvertable())
        {
            m_bInverted = bInvert;

            if(bInvert)
                SetRobotState(GetRobotState()); // Ponovno postavi stanje tak da se invertira
            return true;
        }

        return false;
    }
    bool IsInverted() const { return m_bInverted; }

    // Kad robot skrece, to je ko da rotira oko jene tocke rotacije. Ova funkcija daje koord. te tocke,
    // i kut od centra rotacije prema robotu.
    bool GetRotationCenter(COORD_MR& xc, COORD_MR& yc, ANGLERAD_MR& angToRobot, DIST_MR& radius)
    {
        if(!m_State.AngVelocity)
            return false; // nema rotacije pa nema ni centra rotacije

        radius = m_State.Velocity / m_State.AngVelocity;

        ANGLERAD_MR AngToRotationCenter; // kut od robota prema centru rotacije
        if(radius >= 0)
            AngToRotationCenter = m_State.Angle + PI / 2; // rotira u pozitivnom smjeru
        else
            AngToRotationCenter = m_State.Angle - PI / 2; // rotira u neg smjeru

        radius = abs(radius);

        xc = m_State.x + radius * cos(AngToRotationCenter);
        yc = m_State.y + radius * sin(AngToRotationCenter);

        angToRobot = AngToRotationCenter + PI;

        return true;
    }

protected:
    DIST_MR m_WheelDistance; // Udaljenost izmedu kotaca (sirina baze) [m] (>0)
    DIST_MR m_WheelRadius;   // Radijus kotaca [m] (>0)

    // Ref. brzine
    SPEED_MR m_RefSpeed;
    SPEED_MR m_RefAngSpeed;

    bool m_bInverted;   // Dal je invertiran
    bool m_bInvertable; // Dal se smije invertirati
};

#endif
