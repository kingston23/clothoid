#ifndef _Simple8Traj_h_
#define _Simple8Traj_h_

#include "Traj/2DTrajectoryBase.h"

//#include <Assert.h>
#include <cmath>

//************************************************************************************************************************************************************************************
//
// Jednostavna trajektorija po osmici (kombinacija dvije kru�nice) koja ubrzava od v=0 do v=v_stac
// zadanom akceleracijom i natrag do v=0, zadanom deceleracijom.
// Sluzi za testiranje algoritama za slijedenje trajektorije.
// Glavni problem ove trajektorije da ima diskontinuitet zakrivljenosti na prijelazu sa jedne kruznice na drugu.
//
//************************************************************************************************************************************************************************************
//template<class T>
class CSimple8Traj : public C2DTrajectoryBase
{
public:
    CSimple8Traj()
    {
        m_bParmsOK = false;
#ifdef DBG_FILE_
        dbgf = fopen("Simple8Traj.debug", "wt");
#endif
    };

#ifdef DBG_FILE_
    virtual ~CSimple8Traj() { fclose(dbgf); };
#endif

    bool IsValid() const
    {
        if(!C2DTrajectoryBase::IsValid())
        {
            //assert(0);
            return false;
        }

        //assert(m_bParmsOK);
        return m_bParmsOK;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Dohvat tocke trajektorije u zeljenom vremenskom trenutku
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        //assert(IsValid());
        if(!m_bParmsOK)
            return false;

        T Velocity;
        T Accel;
        T Disp;

        T Time = pTrPoint->Time;

        if(Time >= m_t3)
        {
            // Stoji u krajnjoj to�ki
            Velocity = 0;
            Accel = 0;
            Disp = m_Disp3;
        }
        else if(Time >= m_t2)
        {
            // Jednoliko ubrzano akceleracijom m_Deccel brzine m_v do 0
            T dt = Time - m_t2;
            Velocity = m_v - m_Deccel * dt;
            Accel = -m_Deccel;
            Disp = m_Disp2 + (m_v + Velocity) * dt / 2;
        }
        else if(Time >= m_t1)
        {
            // Jednoliko brzinom m_v
            Velocity = m_v;
            Accel = 0;
            Disp = m_Disp1 + m_v * (Time - m_t1);
        }
        else if(Time >= m_t0)
        {
            // Jednoliko ubrzano akceleracijom m_Accel brzine 0 do m_v
            T dt = Time - m_t0;
            Velocity = m_Accel * dt;
            Accel = m_Accel;
            Disp = m_Disp0 + Velocity * dt / 2;
        }
        else // Vrijeme manje od m_t0
        {
            // Stoji u po�etnoj to�ki
            Velocity = 0;
            Accel = m_Accel;
            Disp = m_Disp0;
        }

        T Rem = (T)fmod(Disp - m_dv, m_o_a + m_o_b);
        if(Rem <= m_o_a)
        {
            // 1. krug
            T d_ = Rem;
            T CircAng;
            if(m_bPositive)
            {
                T Ang = m_MinAng + d_ * m_Curv_a;
                CircAng = Ang - PI / 2;
                pTrPoint->Angle = Ang;
            }
            else
            {
                T Ang = m_MaxAng + d_ * m_Curv_a;
                CircAng = Ang + PI / 2;
                pTrPoint->Angle = Ang;
            }

            pTrPoint->x = m_xc_a + m_r_a * (T)cos(CircAng);
            pTrPoint->y = m_yc_a + m_r_a * (T)sin(CircAng);
            pTrPoint->Curv = m_Curv_a;
        }
        else
        {
            // 2. krug
            T d_ = Rem - m_o_a;
            T CircAng;
            if(m_bPositive) // Prvi pozitivan -> drugi negativan
            {
                T Ang = m_MaxAng + d_ * m_Curv_b;
                CircAng = Ang + PI / 2;
                pTrPoint->Angle = Ang;
            }
            else
            {
                T Ang = m_MinAng + d_ * m_Curv_b;
                CircAng = Ang - PI / 2;
                pTrPoint->Angle = Ang;
            }

            pTrPoint->x = m_xc_b + m_r_b * (T)cos(CircAng);
            pTrPoint->y = m_yc_b + m_r_b * (T)sin(CircAng);
            pTrPoint->Curv = m_Curv_b;
        }

        pTrPoint->Velocity = Velocity;
        pTrPoint->Accel = Accel;
        pTrPoint->Disp = Disp;
        pTrPoint->Dist = Disp;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Dohvat tocke trajektorije za zadani ukupni pomak od pocetka gibanja.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        //assert(IsValid());
        if(!m_bParmsOK)
            return false;

        T Time;
        T Velocity;
        T Accel;

        T Disp = pTrPoint->Disp;

        if(Disp >= m_Disp3)
        {
            // Put ne smije biti ve�i od m_Disp3 - vratimo false ili korigiramo
#ifdef DBG_FILE_
            fprintf(dbgf, "Disp>m_Disp3 (udaljenost ve�a od maks mogu�e) u GetPointByDisp\n");
#endif
            Disp = m_Disp3;
            Velocity = 0;
            Accel = 0;
            Time = m_t3;
        }
        else if(Disp >= m_Disp2)
        {
            // Jednoliko usporeno akceleracijom m_Deccel od brzine m_v do 0.
            T ds = Disp - m_Disp2;
            T Discr = m_v * m_v - 2 * m_Deccel * ds; // Diskriminanta
#ifdef DBG_FILE_
            if(Discr < 0)
                fprintf(dbgf, "Diskriminanta negativna za Disp1<Disp<Disp2 u GetPointByDisp\n");
#endif
            T dt = (m_v - (T)sqrt(Discr)) / m_Deccel;
            Velocity = m_v - m_Deccel * dt;
            Accel = -m_Deccel;
            Time = m_t2 + dt;
        }
        else if(Disp >= m_Disp1)
        {
            // Jednoliko brzinom m_v
            Velocity = m_v;
            Accel = 0;
            Time = m_t1 + (Disp - m_Disp1) / m_v;
        }
        else if(Disp >= m_Disp0)
        {
            // Jednoliko ubrzano akceleracijom m_Accel od brzine m_v do 0.
            T dt = (T)sqrt(2 * (Disp - m_Disp0) / m_Accel);
            Velocity = m_Accel * dt;
            Accel = m_Accel;
            Time = m_t0 + dt;
        }
        else
        {
            // assert(0); // return false;
            Disp = m_Disp0;
            Velocity = 0;
            Accel = 0;
            Time = m_t0;
#ifdef DBG_FILE_
            fprintf(dbgf, "Disp manji od m_Disp0 u GetPointByDisp\n");
#endif
        }

        T Rem = (T)fmod(Disp - m_dv, m_o_a + m_o_b);
        if(Rem <= m_o_a)
        {
            // 1. krug
            T d_ = Rem;
            T CircAng;
            if(m_bPositive)
            {
                T Ang = m_MinAng + d_ * m_Curv_a;
                CircAng = Ang - PI / 2;
                pTrPoint->Angle = Ang;
            }
            else
            {
                T Ang = m_MaxAng + d_ * m_Curv_a;
                CircAng = Ang + PI / 2;
                pTrPoint->Angle = Ang;
            }

            pTrPoint->x = m_xc_a + m_r_a * (T)cos(CircAng);
            pTrPoint->y = m_yc_a + m_r_a * (T)sin(CircAng);
            pTrPoint->Curv = m_Curv_a;
        }
        else
        {
            // 2. krug
            T d_ = Rem - m_o_a;
            T CircAng;
            if(m_bPositive) // Prvi pozitivan -> drugi negativan
            {
                T Ang = m_MaxAng + d_ * m_Curv_b;
                CircAng = Ang + PI / 2;
                pTrPoint->Angle = Ang;
            }
            else
            {
                T Ang = m_MinAng + d_ * m_Curv_b;
                CircAng = Ang - PI / 2;
                pTrPoint->Angle = Ang;
            }

            pTrPoint->x = m_xc_b + m_r_b * (T)cos(CircAng);
            pTrPoint->y = m_yc_b + m_r_b * (T)sin(CircAng);
            pTrPoint->Curv = m_Curv_b;
        }

        pTrPoint->Time = Time;
        pTrPoint->Velocity = Velocity;
        pTrPoint->Accel = Accel;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Struktura za zadavanje parametara trajektorije
    //____________________________________________________________________________________________________________________________________________________________________________________
    struct SParms1
    {
        T StartTime; // Pocetni offseti vremena i udaljenosti
        T StartDisp;
        T xCenter1; // Sredi�te kru�nice 1 - to je kru�nica na kojoj po�inje traj.
        T yCenter1;
        T xCenter2; // Sredi�te kru�nice 2
        T yCenter2;
        T RadiusRatio;  // Omjer radijusa prve i druge (ra/rb)
        T Angle0;       // Kut po�etne to�ke trajektorije (kut tangente na kru�nicu u po� to�ki)
        T TotalDisp;    // Ukupni pomak koji ce se prijeci po trajektoriji [m]
        T v;            // Stacionarna brzina (>=0)
        T Accel;        // Pocetna akceleracija (>=0)
        T Deccel;       // Deceleracija na kraju (>=0)
        bool bPositive; // Ako je true idemo u pozitivnom smjeru po kru�nici 1, a u negativnom po kruznici 2
    };

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje parametara.
    // Na kraju se poziva CheckParms koji vraca false ako parametri ne valjaju ili ih korigira.
    // Ovu funkciju uvijek treba pozvati prije koristenja klase.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool SetParms(SParms1* pParms)
    {
        m_bParmsOK = false;

        m_t0 = pParms->StartTime;
        m_Disp0 = pParms->StartDisp;

        m_xc_a = pParms->xCenter1;
        m_yc_a = pParms->yCenter1;

        m_xc_b = pParms->xCenter2;
        m_yc_b = pParms->yCenter2;

        T dx = pParms->xCenter2 - pParms->xCenter1;
        T dy = pParms->yCenter2 - pParms->yCenter1;
        T d = (T)sqrt(dx * dx + dy * dy);

        m_r_b = d / (1 + pParms->RadiusRatio);
        m_r_a = d - m_r_b;

        m_Angle0 = pParms->Angle0;
        m_DispTotal = pParms->TotalDisp;
        m_bPositive = pParms->bPositive;
        m_v = pParms->v;
        m_Accel = pParms->Accel;
        m_Deccel = pParms->Deccel;

        if(CheckParms())
        {
            m_bParmsOK = true;
            return true;
        }
        return false;
    };

private:
    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera parametara trajektorije i racuna pomocne varijable.
    // U slucaju lose zadanih parametara, ova funkcija ih pokusava korigirati, a ako ne uspije vraca false.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool CheckParms()
    {
        // Provjeravam predznake parametara
        if(m_Accel < 0)
            return false;

        if(m_Deccel < 0)
            return false;

        if(m_v < 0)
            return false;

        if(m_DispTotal < 0)
            return false;

        T BridgeAng = (T)atan2(m_yc_b - m_yc_a, m_xc_b - m_xc_a);

        if(m_bPositive)
        {
            BridgeAng += PI / 2;
            if(BridgeAng < 0)
                BridgeAng += 2 * PI; // Svodimo na 0-2PI
            m_MaxAng = BridgeAng + ((int)(m_Angle0 / (2 * PI))) * 2 * PI;
            m_MinAng = m_MaxAng - 2 * PI;
            m_dv = m_Disp0 - m_r_a * (m_Angle0 - m_MinAng);
        }
        else
        {
            BridgeAng -= PI / 2;
            if(BridgeAng < 0)
                BridgeAng += 2 * PI; // Svodimo na 0-2PI
            m_MinAng = BridgeAng + ((int)(m_Angle0 / (2 * PI)) - 2) * 2 * PI;
            m_MaxAng = m_MinAng + 2 * PI;
            m_dv = m_Disp0 - m_r_a * (m_MaxAng - m_Angle0);
        }

        m_o_a = 2 * m_r_a * PI;
        m_o_b = 2 * m_r_b * PI;

        // m_dbr = m_r_a*dAng;

        T dt01 = m_v / m_Accel;
        T Disp01 = m_v * dt01 / 2;

        T dt23 = m_v / m_Deccel;
        T Disp23 = m_v * dt23 / 2;

        T Disp12 = m_DispTotal - Disp01 - Disp23;
        T dt12;
        if(Disp12 > 0)
        {
            dt12 = Disp12 / m_v;
        }
        else
        {
            // Ne mo�e se ubrzati do zadane brzine i natrag do v=0 na tako kratkoj udaljenosti
            // Sada su dvije opcije: vratiti false ili se prilagoditi
#ifdef DBG_FILE_
            fprintf(dbgf, "Ne mo�e se ubrzati do zadane brzine i natrag do v=0 na tako kratkoj udaljenosti!\n");
#endif

            // Maksimalna brzina koja se mo�e posti�i
            T vmax = (T)sqrt(2 * m_DispTotal * m_Accel * m_Deccel / (m_Accel + m_Deccel));
            m_v = vmax;

            dt01 = vmax / m_Accel;
            Disp01 = vmax * dt01 / 2;

            dt23 = vmax / m_Deccel;
            Disp23 = vmax * dt23 / 2;

            dt12 = 0;
            Disp12 = 0;
        }

        m_t1 = m_t0 + dt01;
        m_t2 = m_t1 + dt12;
        m_t3 = m_t2 + dt23;

        m_Disp1 = m_Disp0 + Disp01;
        m_Disp2 = m_Disp1 + Disp12;
        m_Disp3 = m_Disp2 + Disp23;

        m_Curv_a = 1 / m_r_a;
        m_Curv_b = 1 / m_r_b;
        if(!m_bPositive)
        {
            m_Curv_a = -m_Curv_a;
        }
        else
        {
            m_Curv_b = -m_Curv_b;
        }

        return true;
    };

    T GetBeginTime() const { return m_t0; };
    T GetDeltaTime() const { return m_t3 - m_t0; }
    void SetBeginTime(T BeginTime)
    {
        T TimeOffset = BeginTime - GetBeginTime();
        m_t0 += TimeOffset;
        m_t1 += TimeOffset;
        m_t2 += TimeOffset;
        m_t3 += TimeOffset;
    };

    T GetBeginDisp() const { return m_Disp0; };
    T GetDeltaDisp() const { return m_Disp3 - m_Disp0; };
    void SetBeginDisp(T BeginDisp)
    {
        T TimeOffset = BeginDisp - GetBeginDisp();
        m_Disp0 += TimeOffset;
        m_Disp1 += TimeOffset;
        m_Disp2 += TimeOffset;
        m_Disp3 += TimeOffset;
    };

    // Parametri
private:
    // Po�etni uvjeti
    T m_t0;    // Po�etno vrijeme (obi�no 0)
    T m_Disp0; // Po�etna udaljenost
    T m_xc_a;  // Sredi�te kru�nice 1
    T m_yc_a;
    T m_r_a;  // Radijus kru�nice 1
    T m_xc_b; // Sredi�te kru�nice 2
    T m_yc_b;
    T m_r_b;          // Radijus kru�nice 2
    T m_Angle0;       // Po�etni kut trajektorije na kru�nici 1
    bool m_bPositive; // Kre�emo se u pozitivnom smjeru ako je true (za kru�nicu 1)

    T m_DispTotal;
    T m_v;      // Stacionarna brzina
    T m_Accel;  // Akceleracija od brzine 0 do m_v
    T m_Deccel; // Akceleracija od brzine m_v do 0

private:
    // Private parametri (za ubrzanje ra�unanja da se ne moraju svaki puta ra�unati)
    // Postavlja ih funkcija CheckParms()
    T m_t1; // Vrijeme u to�ki 1
    T m_t2;
    T m_t3;

    T m_Disp1; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 1
    T m_Disp2; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 2
    T m_Disp3; // Ukupna prije�ena udaljenost od to�ke 0 do to�ke 3

    T m_dv;  // Udaljenost po trajektoriji koja se prvi puta mora proci do prijelaza na drugu kruznicu
    T m_o_a; // Opseg prve kru�nice
    T m_o_b; // Opseg druge kru�nice

    T m_MaxAng; // Maksimalni i minimalni kut trajektorije - kut se ovdje mijenja periodicki tako da ima min i maks vrijednosti
    T m_MinAng;

    T m_Curv_a, m_Curv_b; // Zakrivljenost prvog i drugog kruga, moze biti i negativna idemo u negativnom smjeru po tom krugu

    bool m_bParmsOK; // Ako su parametri provjereni

#ifdef DBG_FILE_
    FILE* dbgf;
#endif
};

/* Primjer upotrebe klase:

#define DBG_FILE_ // Opcionalno
#include "Simple8Traj.h"

CSimple8Traj T8;

typedef CSimple8Traj::SParms1 S8Parms1;
S8Parms1 T8Parms;
T8Parms.StartTime = 1;
T8Parms.StartDisp = 10;
T8Parms.xCenter1 = 1;
T8Parms.yCenter1 = 1;
T8Parms.xCenter2 = -1;
T8Parms.yCenter2 = -1;
T8Parms.RadiusRatio = 2;
T8Parms.Angle0 = -20;
T8Parms.TotalDisp = 19;
T8Parms.v = 1;
T8Parms.Accel = 1;
T8Parms.Deccel = 1;
T8Parms.bPositive = true;
T8.SetParms(&T8Parms);

typedef C2DTrajectoryBase::S2DTrajPoint STrajPt;
STrajPt TrajPt;
TrajPt.Time = 2;
T8.GetPointByTime(&TrajPt);

*/

#endif // _Simple8Traj_h_
