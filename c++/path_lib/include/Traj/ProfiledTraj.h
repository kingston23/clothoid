#ifndef _ProfiledTraj_h_
#define _ProfiledTraj_h_

#include "2DTrajectoryBase.h"
#include "TrajPathProfile.h"
#include "TrajTimeProfile.h"

//=================================================================================================================================================================
// Koristiti u slucajevima kad je potrebno odvojeno planirati vremenski i prostorni profil
// trajektorije. U tom slucaju vremenski profil definira pomak, brzinu i akceleraciju u
// pojedinom trenutku, a prostorni koordinate u prostoru, kut i zakrivljenost.
//
// Nakon sto je profil dodan u klasu ne smije se vise modificirati!
//
// Za podesavanje moda ekstrapolacije, mora prvo biti postavljen vazeci path profil.
//=================================================================================================================================================================
template<class T>
class CProfiledTraj : public C2DTrajectoryBase
{
public:
    CProfiledTraj()
    {
        m_pTimeProfile = nullptr;
        m_pPathProfile = nullptr;
    };

    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!(m_pTimeProfile && m_pPathProfile))
            return false;

        // Popuni vremenski dio trajektorije - ovo obavezno mora vratiti Dist da bi se dobio i prostorni dio
        if(!m_pTimeProfile->GetPointByTime(pTrPoint))
        {
            assert(0);
            return false;
        }

        // Popuni prostorni dio trajektorije
        if(!m_pPathProfile->GetPointByDist(pTrPoint))
        {
            assert(0);
            return false;
        }

        return C2DTrajectoryBase::GetPointByTime(pTrPoint);
    }

    bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!(m_pTimeProfile && m_pPathProfile))
            return false;

        // Popuni vremenski dio trajektorije - ovo obavezno mora vratiti Dist da bi se dobio i prostorni dio
        if(!m_pTimeProfile->GetPointByDisp(pTrPoint))
        {
            assert(0);
            return false;
        }

        // Popuni prostorni dio trajektorije
        if(!m_pPathProfile->GetPointByDist(pTrPoint))
        {
            assert(0);
            return false;
        }

        return C2DTrajectoryBase::GetPointByDisp(pTrPoint);
    }

    // Ovo moze vratiti samo prostorni dio trajektorije
    bool GetPointByDist(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!m_pPathProfile)
            return false;

        // Popuni prostorni dio trajektorije
        if(!m_pPathProfile->GetPointByDist(pTrPoint))
        {
            assert(0);
            return false;
        }

        return C2DTrajectoryBase::GetPointByDist(pTrPoint);
    }

    //---------------------------------------------------------
    // Postavlja prostorni profil
    //---------------------------------------------------------
    bool SetPathProfile(CTrajPathProfile* pPathProfile)
    {
        assert(pPathProfile);

        if(!pPathProfile->IsValid())
        {
            assert(0); // Shit path profile
            return false;
        }

        m_pPathProfile = pPathProfile;

        // Uskladivanje pocetnih tocaka obaju profila
        if(m_pTimeProfile)
        {
            m_pPathProfile->SetBeginDisp(m_pTimeProfile->GetBeginDisp());
            m_pPathProfile->SetBeginDist(m_pTimeProfile->GetBeginDist());
            m_pPathProfile->SetBeginTime(m_pTimeProfile->GetBeginTime());
        }

        return true;
    };

    //---------------------------------------------------------
    // Postavlja vremenski profil
    //---------------------------------------------------------
    bool SetTimeProfile(CTrajTimeProfile* pTimeProfile)
    {
        assert(pTimeProfile);

        if(!pTimeProfile->IsValid())
        {
            assert(0); // Shit time profile
            return false;
        }

        m_pTimeProfile = pTimeProfile;

        // Uskladivanje pocetnih tocaka obaju profila
        if(m_pPathProfile)
        {
            m_pPathProfile->SetBeginDisp(m_pTimeProfile->GetBeginDisp());
            m_pPathProfile->SetBeginDist(m_pTimeProfile->GetBeginDist());
            m_pPathProfile->SetBeginTime(m_pTimeProfile->GetBeginTime());
        }

        return true;
    };

    bool IsValid() const
    {
        if(!C2DTrajectoryBase::IsValid())
            return false;

        if(!(m_pTimeProfile && m_pPathProfile))
            return false;

        return true;
    }

    T GetBeginTime() const
    {
        assert(IsValid());
        if(m_pTimeProfile)
            return m_pTimeProfile->GetBeginTime();
        else
            return 0;
    }
    T GetDeltaTime() const
    {
        if(m_pTimeProfile)
            return m_pTimeProfile->GetDeltaTime();
        else
            return 0;
    }
    void SetBeginTime(T BeginTime)
    {
        assert(IsValid());
        if(m_pTimeProfile && m_pPathProfile)
        {
            m_pTimeProfile->SetBeginTime(BeginTime);
            m_pPathProfile->SetBeginTime(BeginTime);
        }
    }

    T GetBeginDisp() const
    {
        assert(IsValid());
        if(m_pTimeProfile)
            return m_pTimeProfile->GetBeginDisp();
        else
            return 0;
    }
    T GetDeltaDisp() const
    {
        if(m_pTimeProfile)
            return m_pTimeProfile->GetDeltaDisp();
        else
            return 0;
    }
    void SetBeginDisp(T BeginDisp)
    {
        assert(IsValid());
        if(m_pTimeProfile && m_pPathProfile)
        {
            m_pTimeProfile->SetBeginDisp(BeginDisp);
            m_pPathProfile->SetBeginDisp(BeginDisp);
        }
    }

    T GetBeginDist() const
    {
        assert(IsValid());
        if(m_pTimeProfile)
            return m_pTimeProfile->GetBeginDist();
        else
            return 0;
    }
    T GetDeltaDist() const
    {
        assert(IsValid());
        if(m_pTimeProfile)
            return m_pTimeProfile->GetDeltaDist();
        else
            return 0;
    }
    void SetBeginDist(T BeginDist)
    {
        assert(IsValid());
        if(m_pTimeProfile && m_pPathProfile)
        {
            m_pTimeProfile->SetBeginDist(BeginDist);
            m_pPathProfile->SetBeginDist(BeginDist);
        }
    }

    CTrajTimeProfile* GetTimeProfile() const
    {
        assert(IsValid());
        return m_pTimeProfile;
    }

    CTrajPathProfile* GetPathProfile() const
    {
        assert(IsValid());
        return m_pPathProfile;
    }

    // Kod profilirane trajektorije, mod ekstrapolacije staze definiran je
    // path profilom

    EPathExtrapolationMode GetPathBeginExtrapolationMode() const
    {
        if(m_pPathProfile)
            return m_pPathProfile->GetPathBeginExtrapolationMode();

        return EM_Unknown;
    }

    EPathExtrapolationMode GetPathEndExtrapolationMode() const
    {
        if(m_pPathProfile)
            return m_pPathProfile->GetPathEndExtrapolationMode();

        return EM_Unknown;
    }

    bool SetPathBeginExtrapolationMode(EPathExtrapolationMode BeginMode)
    {
        if(m_pPathProfile)
            return m_pPathProfile->SetPathBeginExtrapolationMode(BeginMode);

        return false;
    }

    bool SetPathEndExtrapolationMode(EPathExtrapolationMode EndMode)
    {
        if(m_pPathProfile)
            return m_pPathProfile->SetPathEndExtrapolationMode(EndMode);

        return false;
    }

    virtual CTrajPathProfile* GetCurrentPath() { return m_pPathProfile; }

    virtual CTrajTimeProfile* GetCurrentTimeProfile() { return m_pTimeProfile; }

private:
    CTrajTimeProfile* m_pTimeProfile;
    CTrajPathProfile* m_pPathProfile;
};

#endif _ProfiledTraj_h_
