
#ifndef _DYNAMIC_WINDOW_BASE_H_
#define _DYNAMIC_WINDOW_BASE_H_

#include "Geom/2DGeomMap.h"

using namespace Geom;

class CDynamicWindowBase
{
public:
    //_____________________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje parametara dinamickog prozora
    //_____________________________________________________________________________________________________________________________________________________________________
    void SetDW(double SampleTime, double vMin, double vMax, double dvMax, double wMin, double wMax, double dwMax, double GoalPerimeter,
               double RobotRadius, double sc1, double sc2, double vTolerance, double wTolerance)
    {
        T = SampleTime;

        V_MAX = vMax;
        V_MIN = vMin;
        DV_MAX = dvMax;

        W_MAX = wMax;
        W_MIN = wMin;
        DW_MAX = dwMax;

        // Gledamo toliko ciklusa unaprijed kolko mu treba da zastopa od max brzine do 0
        // FK = vMax/dvMax/SampleTime;

        GOAL_PERIMETER = GoalPerimeter;

        RR = RobotRadius;

        SC1 = sc1;
        SC2 = sc2;

        V_TOLERANCE = vTolerance;
        W_TOLERANCE = wTolerance;
    }

    void SetRobotState(double x, double y, double ang, double v, double w);

    C2DGeomMap* m_pMap;

    void SetMap(C2DGeomMap* pMap) { m_pMap = pMap; }

    void SetSelfID(int SelfID) { m_SelfID = SelfID; }

    int m_SelfID;

protected:
    // Parametri - deklaracije

    // PAZNJA!!! -ovo je u milimetrima!!!
    static const double PI;

    double V_MAX;
    double V_MIN;
    double DV_MAX;

    // velicine vezane za rotaciju
    // ovo je u radijanima
    double W_MAX;
    double W_MIN;
    double DW_MAX;

    // DIMENZIJE DINAMICKOG PROZORA (ukupno je dimenzija +1)
    static const int V_DIM = 6; // 2 // (+1)Dimenzija dinamickog prozora todo
    static const int W_DIM = 6; // (+1)Dimenzija dinamickog prozora, MORA biti PARAN broj za simetriju!!!

    // VREMENSKI PARAMETRI ALGORITMA
    double T; // to je ciklus

    // koliko ciklusa gledamo unaprijed, broj 50 je odabran s obzirom na max brzinu od 600mm/s i max domet lasera 3000mm
    double FK;

    // PARAMETRI KRUZNOG LUKA
    static const int N_KL = 30;
    // ovo je za racunanje minimalne udaljenosti po svim preprekama do tocke na kruznom luku (ne do svih po kruznom luku)-uzmimo slucaj da
    // trajektorija mora biti nedozvoljena jer smo nasli prvu prepreku koja je u safety distance-u ako je 0 onda se racuna do prve prepreke
    // koja je u safety distance od robota LASER

    // Tezine pojedinih doprinosa
    static const double BETA;  // Tezinski koeficijent u kriterijskoj funkciji - prohodnost
    static const double ALPHA; // udio putanje u cijeloj prici - alignment velocity
    static const double DELTA; // udio putanje u cijeloj prici - alignment heading

    double GOAL_PERIMETER; // malo prije cilja treba uzeti drukcije parametre alpha, beta i delta

    double RR; // Radijus robota [mm]

    // SECURITY DISTANCE za robota
    double SC1; // kod najmanjih brzina smije biti x m od prepreke
    double SC2; // a kod najvecih brzina smije biti x m od prepreke
    double SC_W;

    // TOLERANCIJA BRZINA - znaci brzina 0 znaci i od 5mm/s
    double V_TOLERANCE;
    double W_TOLERANCE;

public:
    DynamicWindow();
    ~DynamicWindow(){};

    void Sekvenca_izvodjenja();

    double GetRefVelocitiesV() { return this->SP.setpoint_v; };
    double GetRefVelocitiesW() { return this->SP.setpoint_w; };

    void SetGlobalGoalX(double global_goal_x) { this->global_goal_x = global_goal_x; };
    void SetGlobalGoalY(double global_goal_y) { this->global_goal_y = global_goal_y; };

    bool IsNearGoal(double d);

protected:
    // Definicija struktura
    //_____________________

    // Struktura za mobilnog robota
    struct MB_RB
    {
        // Polozaj, orijentacija i kompas
        double x, y, th, k;

        // Brzine
        double v, w;

        // Zaglavljenja motora
        char lms, rms;
    };

    // Struktura za kruzni luk
    struct MB_KL
    {
        // Koordinate
        double x[N_KL];
        double y[N_KL];
    };

    // Struktura za referentnu brzinu
    struct MB_setpoint
    {
        double setpoint_v;
        double setpoint_w;
    };

    // Definicija tocke - koristi se za prepreke
    struct MB_tocka
    {
        double x, y;
    };

    // Podrska za laser
    struct tocka_T : public MB_tocka
    {
        int valid;
    };

    typedef struct tocka_T tT;

    // Struktura tablica brzina
    struct MB_TB
    {
        // Moguce stanje flagova za parove brzina
        enum EFlag
        {
            INITIAL = 0,
            DYNAMIC_CONSTRAINTS = -1,
            NON_ADMISSIBLE = -2,
            KINEMATIC_CONSTRAINTS = -3,
            HAS_OBSTACLE = 1,
            CLEAR = 2
        };

        // Tablica - ovdje se zbrajaju doprinosi za kriterijsku funkciju
        EFlag flag[V_DIM + 1][W_DIM + 1];

        // Tablica - ukupna ocjena odredjene trajektorije
        double ocjena[V_DIM + 1][W_DIM + 1];

        // tablica prohodnosti glede na prepreke
        double ocjena_prohodnost[V_DIM + 1][W_DIM + 1]; // [0-1]

        double ocjena_linearna_brzina[V_DIM + 1][W_DIM + 1]; // [0-1]

        double ocjena_orientacija_goal[V_DIM + 1][W_DIM + 1]; // [0-1]

        // Iznosi - ova polja govore koje su to brzine u tablici
        double v[V_DIM + 1];
        double w[W_DIM + 1];

        double radius[V_DIM + 1][W_DIM + 1];

        // Gdje bi robot na kruznom luku stao da koci maks deceleracijom i koja bi mu bila orijentacija
        double breakage_point_x[V_DIM + 1][W_DIM + 1];
        double breakage_point_y[V_DIM + 1][W_DIM + 1];
        double breakage_point_th[V_DIM + 1][W_DIM + 1];

        double obstacle_point_x[V_DIM + 1][W_DIM + 1];
        double obstacle_point_y[V_DIM + 1][W_DIM + 1];
    };

protected:
    // Deklaracije struktura i varijabli
    //__________________________________

    MB_TB TB; // Tablica brzina dinamickog prozora

    // Kruzni luk ili ako ide pravocrtno, onda pravac
public:
    CCircleArc m_Arc;
    CBoundLineImp m_Line;
    bool m_bIsLine; // Ako je linija, ovo je true

protected:
    MB_RB RB; // instanca stanja robota

    MB_setpoint SP; // instanca setpoint vrijednosti brzina

    // (UNIVERZALNI) Indexi za polja
    int ni, nj;

    // Varijable minimalnog zaustavnog puta za par (v,w) koji se trenutno ispituje
    double m_StopTime;     // Vrijeme zaustavnog puta
    double m_StopAccel;    // Akceleracija kojom zaustavljamo
    double m_StopDistance; // Minimalni zaustavni put

    double global_goal_x, global_goal_y;

    double Alpha, Beta, Delta;

protected:
    // ubacivanje trenutnog stanja robota u strukturu
    void Azuriraj_stanje_robota();
    // Definicija dinamickog prozora
    void Odredi_dinamicki_prozor();
    // Funkcija za proracun kruznog luka
    inline void Kruzni_luk();
    void Prohodnost();
    void OrientacijaGoal(double goal_x, double goal_y);
    void OcjenaLinearnaBrzina();

    void DoprinosProhodnost();
    void DoprinosOrientacijaGoal();
    void DoprinosLinearnaBrzina();
    // Trazenje optimalnog para
    void Optimalni_par();

    inline void MinimalniZaustavniPut();
};

#endif // _DYNAMIC_WINDOW_BASE_H_
