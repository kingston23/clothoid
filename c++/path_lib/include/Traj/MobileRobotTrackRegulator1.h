#ifndef _Traj_Track_Regulator_1_h_
#define _Traj_Track_Regulator_1_h_

#include "Math/AngleRange.h"
#include "Misc/PidController.h"
#include "2DTrajectoryBase.h"
#include "TrajTrackingController.h"

#include <fstream>

class CMobileRobotTrackRegulator1 : public C2DTrajectoryDataTypes, public CTrajTrackingController
{
public:
    typedef double T;

    CMobileRobotTrackRegulator1()
    {
        m_ControllerType = TTC_Moj;

        m_LogFile.open("Log/Rezultati_moj_reg.logtr", std::ios::out);

        m_LogFile << "% Rezultati eksperimenta s mojim regulatorom za slijedenje trajektorije \n";
        m_LogFile << "% Podaci u stupcima imaju poredak: \n";
        m_LogFile << "% Vrijeme | Trajektorija | Mjerenja | Pogreska | Reference \n";
        m_LogFile << "% Detaljnije: \n";
        m_LogFile << "% t xTraj yTraj angTraj vTraj wTraj aTraj xm ym angm vm wm e1 e2 e3 vRef wRef\n";

        m_LogFile << std::fixed;   // Tak da svaki float broj zauzima isto mjesta
        m_LogFile << std::showpos; // Tak da pozitivni brojevi imaju +
    }

    ~CMobileRobotTrackRegulator1() { m_LogFile.close(); }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Funkcija racuna nove referentne brzine za robota. Poziva se za svaki novi vremenski uzorak.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool GetRefValues(typename CMobileRobot::SRobotState* pState,   // (in), u ovoj strukturi nalazi se trenutno stanje robota
                              typename CMobileRobot::SRobotState* pRefState // (out), u ovu strukturu postavlja se referentna lin. i kutna
                                                                            // brzina robota. Ostali clanovi strukture se ignoriraju
    )
    {
        assert(pState && pRefState);

        if(!IsValid())
        {
            assert(0);
            return false;
        }

        // Trenutno vrijeme
        T Time = pState->CurTime;

        // Reference
        C2DTrajectoryBase::S2DTrajPoint RefPoint;
        RefPoint.Time = Time + (T)0.5 * m_SamplingPeriod; // Uzimamo reference s x koraka unaprijed
        if(!m_pTraj->GetPointByTime(&RefPoint))
            return false;

        T xTraj = RefPoint.x;
        T yTraj = RefPoint.y;
        T angTraj = RefPoint.Angle;
        T vTraj = RefPoint.Velocity;
        T wTraj = RefPoint.GetAngVelocity();
        T aTraj = RefPoint.Accel;

        T DispRef = RefPoint.Disp;

        m_LogFile << Time << ' ' << xTraj << ' ' << yTraj << ' ' << angTraj << ' ' << vTraj << ' ' << wTraj << ' ' << aTraj << ' ';

        // Mjerenje
        T xm = pState->x;
        T ym = pState->y;
        T angm = pState->Angle;

        T vm = pState->Velocity;
        T wm = pState->AngVelocity;

        m_LogFile << xm << ' ' << ym << ' ' << angm << ' ' << vm << ' ' << wm << ' ';

        // Predikcija zbog kasnjenja
        CompensateDelay(pState);

        // Sad uzimamo predvidena stanja robota kao mjerena stanja
        xm = pState->x;
        ym = pState->y;
        angm = pState->Angle;

        // Proracun izlaza regulatora (tj. ref lin. i kutne brzine robota)

        // Pogreske slijedenja
        T e1 = cos(angm) * (xTraj - xm) + sin(angm) * (yTraj - ym);
        T e2 = -sin(angm) * (xTraj - xm) + cos(angm) * (yTraj - ym);
        T e3 = CAngleRange::Range_MinusPI_PI(angTraj - angm);

        // Zbroji pogreske - to je SSE (sum of squared errors)
        m_SSE1 += e1 * e1;
        m_SSE2 += e2 * e2;
        m_SSE3 += e3 * e3;

        // Greske
        T xError = xTraj - xm;
        T yError = yTraj - ym;

        // Izracunaj feedforward komponente (nekoliko koraka unaprijed radi kasnjenja)
        C2DTrajectoryBase::S2DTrajPoint FeedForward;
        // FeedForward.Time = Time + m_nSamplesDelay*m_SamplingPeriod;
        FeedForward.Time = Time + (T)0.5 * m_SamplingPeriod;
        if(!m_pTraj->GetPointByTime(&FeedForward))
            return false;
        T SpeedIdeal = FeedForward.Velocity;
        T AngSpeedIdeal = SpeedIdeal * FeedForward.Curv;

        // Korekcija da ga skrenemo prema trajektoriji (gresnika na pravi put) - koristi se look ahead point.
        T xLookAhead, yLookAhead;

        T LookAhead = m_LookAhead; // Udaljenost look ahead tocke od referentne pozicije
        // LookAhead = 3.5*DiscretizationTime*abs(pParms->SpeedMsr);
        assert(LookAhead > 0);

        // Look ahead tocka se racuna kao tocka na tangenti putanje udaljena za LookAhead udaljenost od ref. pozicije
        xLookAhead = xTraj + cos(angTraj) * LookAhead;
        yLookAhead = yTraj + sin(angTraj) * LookAhead;

        // Drugi nacin racunanja look ahead tocke je preko udaljenosti

        // Ovo sa look ahead pointom je zajebano na kraju trajektorije pa se koristi malo slozenija logika
        T RemainDisp = m_pTraj->GetEndDisp() - DispRef; // Koliki pomak je ostao do kraja trajektorije
        if(RemainDisp >= 0)
        {
            // Zasad je nasa pretpostavka tocna (sto ne mora tako i ostati u sljedecim pozivima f-ije)
            if(RemainDisp >= LookAhead)
            {
                // Nismo jos blizu kraja trajektorije
                C2DTrajectoryBase::S2DTrajPoint LookAheadPoint;
                LookAheadPoint.Disp = DispRef + LookAhead;
                if(!m_pTraj->GetPointByDisp(&LookAheadPoint))
                    return false;
                xLookAhead = LookAheadPoint.x;
                yLookAhead = LookAheadPoint.y;
            }
            else
            {
                // Blizu smo kraja trajektorije pa radimo malo interpolacije
                C2DTrajectoryBase::S2DTrajPoint LookAheadPoint;

                LookAheadPoint.Disp = m_pTraj->GetEndDisp();
                if(!m_pTraj->GetPointByDisp(&LookAheadPoint))
                    return false;

                T InterpolationLen = LookAhead - RemainDisp;
                xLookAhead = LookAheadPoint.x + InterpolationLen * (T)cos(LookAheadPoint.Angle);
                yLookAhead = LookAheadPoint.y + InterpolationLen * (T)sin(LookAheadPoint.Angle);
            }
        }
        else
        {
            // Pretpostavka nije tocna, i trajektorija ga fercera dalje od endpointova.
            // U tom slucaju i mi bezobzirno premasujemo endpointe kako bismo dobili look ahead point.
            C2DTrajectoryBase::S2DTrajPoint LookAheadPoint;

            LookAheadPoint.Disp = DispRef + LookAhead;
            if(!m_pTraj->GetPointByDisp(&LookAheadPoint))
                return false;
            xLookAhead = LookAheadPoint.x;
            yLookAhead = LookAheadPoint.y;
        }

        // T LongError = (T)((T)cos(angTraj)*xError + (T)sin(angTraj)*yError);
        T LongError = (T)((T)cos(angm) * xError + (T)sin(angm) * yError);

        T OutSpeedRef = SpeedIdeal + m_LinearPid.GetControlVal(LongError, 0);

        // T OutSpeedRef = SpeedIdeal + 	1.4f*sqrt(vTraj*vTraj + 60.f*wTraj*wTraj)*LongError;

        T AngError = CAngleRange::Range_MinusPI_PI((T)atan2(yLookAhead - ym, xLookAhead - xm) - angm);

        T OutAngSpeedRef = AngSpeedIdeal + m_AnglePid.GetControlVal(AngError, 0);

        // T OutAngSpeedRef = AngSpeedIdeal + 1.4f*sqrt(vTraj*vTraj + 60.f*wTraj*wTraj)*AngError;

        // Napuni strukturu s ref. stanjem
        pRefState->Velocity = OutSpeedRef;
        pRefState->AngVelocity = OutAngSpeedRef;

        // Ogranici ref. vrijednosti
        BoundRefValues(pRefState);

        m_LogFile << e1 << ' ' << e2 << ' ' << e3 << ' ' << ' ' << pRefState->Velocity << ' ' << pRefState->AngVelocity << std::endl;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Struktura za zadavanje parametara regulatora
    //____________________________________________________________________________________________________________________________________________________________________________________
    struct SParms : public CTrajTrackingController::SParms
    {
        // Parametri regulatora

        T LookAhead;

        // Parametri pid-a za kut
        typename CPidController<T>::SParms AnglePidParms;

        // Parametri pid-a za longitudinalnu pogresku
        typename CPidController<T>::SParms LinearPidParms;
    };

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje parametara regulatora.
    // Ovu funkciju uvijek treba pozvati prije koristenja regulatora.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool SetParms(CTrajTrackingController::SParms* pBaseParms)
    {
        if(!CTrajTrackingController::SetParms(pBaseParms))
            return false;

        SParms* pParms = static_cast<SParms*>(pBaseParms);

        if(!m_AnglePid.SetParms(&pParms->AnglePidParms))
            return false;

        if(!m_LinearPid.SetParms(&pParms->LinearPidParms))
            return false;

        m_LookAhead = pParms->LookAhead;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Resetira regulator (tj. resetira memorirana stanja regulatora).
    // Ovo je npr. korisno kad zaustavimo upravljanje, pa ga zelimo kasnije ponovo pokrenuti
    //____________________________________________________________________________________________________________________________________________________________________________________
    void Reset()
    {
        CTrajTrackingController::Reset();

        // Zdravo je resetirati derivatore i integratore
        m_LinearPid.ResetDerInt();
        m_AnglePid.ResetDerInt();
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera stanja klase
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValid() const
    {
        if(!CTrajTrackingController::IsValid())
            return false;

        if(!IsValidParms())
            return false;

        if(!m_AnglePid.IsValid())
            return false;

        if(!m_LinearPid.IsValid())
            return false;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera dal su dobro postavljeni parametri
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValidParms() const
    {
        if(m_LookAhead <= 0)
            return false;

        return true;
    }

protected:
    CPidController<T> m_AnglePid;
    CPidController<T> m_LinearPid;

    T m_LookAhead;

    std::ofstream m_LogFile; // Log datoteka
};

#endif
