#ifndef _Soccer_Robot_h_
#define _Soccer_Robot_h_

#include "MobileRobot.h"
#include "RobotController_PosAngle1.h"

template<class T>
class CSoccerRobot : public CDiffDriveRobot<T>
{
public:
    CSoccerRobot()
    {
        m_pController = nullptr;

        m_bUpdateParms = false;
    }

    ~CSoccerRobot() { delete m_pController; }

    bool GoTo3(COORD_MR x, COORD_MR y, ANGLERAD_MR Angle, SPEED_MR Speed, bool* pbDone = nullptr)
    {
        if(!m_pController)
        {
            m_pController = new CRobotController_PosAngle1<T>;
            if(!m_pController)
            {
                assert(0);
                return false;
            }
            m_bUpdateParms = true;
        }

        if(m_bUpdateParms)
        {
            m_bUpdateParms = false;

            // Postavljanje parametara kontrolera
            CRobotController_PosAngle1<T>::SParms Parms;

            Parms.vMax = GetMaxAllowedSpeed();
            Parms.wMax = GetMaxAllowedAngSpeed();
            Parms.LinAccelMax = GetMaxAllowedAccel();
            Parms.AngAccelMax = GetMaxAllowedAngAccel();

            Parms.SamplingPeriod = GetSamplingPeriod();
            Parms.nSamplesDelay = GetNumSamplesDelay();

            Parms.WheelDistance = GetWheelDistance();
            Parms.GoalDistTolerance = 0.05f;
            Parms.GoalAngleTolerance = PI * 15 / 180;
            Parms.GoalSpeedTolerance = 0.3f;
            Parms.TrajDistTolerance = 0.025f;
            Parms.TrajAngleTolerance = PI * 10 / 180;
            Parms.TrajSpeedTolerance = 0.3f;

            // Parms.TrajParms.PathParms.DefaultRadius = m_Parms.DefaultRadius;
            // Parms.TrajParms.PathParms.EndLineLength = 0.f;
            // Parms.TrajParms.PathParms.EndLineLength = 0.05f;

            Parms.TrajParms.TimeProfileParms.MaxRobotSpeed = GetMaxAllowedSpeed();
            Parms.TrajParms.TimeProfileParms.MaxAccel = GetMaxAllowedAccel();
            Parms.TrajParms.TimeProfileParms.WheelDistance = GetWheelDistance();
            Parms.TrajParms.TimeProfileParms.MaxSlowDown = (T)0.5;

            // Parms.RegulatorParms.pTraj = nullptr;
            // Parms.RegulatorParms.LookAhead = 0.15f; // 0.15
            // Parms.RegulatorParms.nSamplesDelay = 3.f;

            // Parms.RegulatorParms.AnglePidParms.DiscretizationMethod = CPidController<T>::DM_Backward;
            // Parms.RegulatorParms.AnglePidParms.Kp = 5.0f; // 1.5f
            // Parms.RegulatorParms.AnglePidParms.Ti = 10.f;
            // Parms.RegulatorParms.AnglePidParms.Td = (T)0.05;  // 0.1f 0.25f;
            // Parms.RegulatorParms.AnglePidParms.Tdiscr = m_SamplingPeriod;
            // Parms.RegulatorParms.AnglePidParms.StartIntegralTerm = 0.f;
            // Parms.RegulatorParms.AnglePidParms.Ni = 15;
            // Parms.RegulatorParms.AnglePidParms.bUseP = true;
            // Parms.RegulatorParms.AnglePidParms.bUseI = false;
            // Parms.RegulatorParms.AnglePidParms.bUseD = true;
            // Parms.RegulatorParms.AnglePidParms.MaxOutput = GetMaxSpeed()*2/GetWheelDistance();
            // Parms.RegulatorParms.AnglePidParms.MaxOutputChange = GetMaxSpeed()*2/GetWheelDistance(); // Nema ogranicenja

            // Parms.RegulatorParms.LinearPidParms.DiscretizationMethod = CPidController<T>::DM_Backward;
            // Parms.RegulatorParms.LinearPidParms.Kp = 8.0f; // 8.0f
            // Parms.RegulatorParms.LinearPidParms.Ti = 10.f;
            // Parms.RegulatorParms.LinearPidParms.Td = (T)0.03; // 0.25f;
            // Parms.RegulatorParms.LinearPidParms.Tdiscr = m_SamplingPeriod;
            // Parms.RegulatorParms.LinearPidParms.StartIntegralTerm = 0.f;
            // Parms.RegulatorParms.LinearPidParms.Ni = 15;
            // Parms.RegulatorParms.LinearPidParms.bUseP = true;
            // Parms.RegulatorParms.LinearPidParms.bUseI = false;
            // Parms.RegulatorParms.LinearPidParms.bUseD = true;
            // Parms.RegulatorParms.LinearPidParms.MaxOutput = GetMaxSpeed();
            // Parms.RegulatorParms.LinearPidParms.MaxOutputChange = GetMaxSpeed(); // Nema ogranicenja

            // Parms.RegulatorParms.vMax = GetMaxSpeed();
            // Parms.RegulatorParms.wMax = GetMaxSpeed()*2/GetWheelDistance();
            // Parms.RegulatorParms.LinAccelMax = GetMaxAllowedAccel();
            // Parms.RegulatorParms.AngAccelMax = GetMaxAllowedAccel()*2/GetWheelDistance();
            // Parms.RegulatorParms.SamplingPeriod = m_SamplingPeriod;
            // Parms.RegulatorParms.nSamplesDelay = 0;

            if(!m_pController->SetParms(&Parms))
            {
                assert(0);
                return false;
            }
        }

        m_pController->SetAutoInvert(IsInvertable());

        TIME_MR CurTime = m_State.CurTime;

        SPEED_MR SpeedCommand;
        ANGSPEED_MR AngSpeedCommand;

        if(!m_pController->GoTo(CurTime, m_State.x, m_State.y, m_State.Angle, m_State.Velocity, m_State.AngVelocity, x, y, Angle, Speed,
                                &SpeedCommand, &AngSpeedCommand))
        {
            // assert(0);
            return false;
        }

        SetRefSpeed(SpeedCommand, AngSpeedCommand);

        CorrectRefSpeeds();

        if(pbDone)
            *pbDone = m_pController->IsDone();

        return true;
    }

    virtual CTrajPathProfile* GetCurrentPath()
    {
        if(m_pController)
            return m_pController->GetCurrentPath();

        return false;
    }

    virtual CTrajTimeProfile* GetCurrentTimeProfile()
    {
        if(m_pController)
            return m_pController->GetCurrentTimeProfile();

        return false;
    }

    //------------------------------------------------------------------------------------------------------
    // Pristupne funkcije
    //------------------------------------------------------------------------------------------------------

    int GetRobotID() const { return m_RobotID; }

    void SetRobotID(int RobotID)
    {
        assert(RobotID >= 0 && RobotID <= 4);
        m_RobotID = RobotID;
    }

protected:
    CRobotController_PosAngle1<T>* m_pController; // Kontroler za GoTo3() u min vremenu

    int m_RobotID;

    bool m_bUpdateParms; // Dal treba apdejtati parametre u sljedecem pozivu
};

#endif // _Soccer_Robot_h_