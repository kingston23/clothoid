#ifndef _TrajTrackingRegulator_PI_h_
#define _TrajTrackingRegulator_PI_h_

#include "Math/AngleRange.h"
#include "TrajTrackingController.h"

#include <stdio.h>

//************************************************************************************************************************************************************************************
//
// Linearni regulator za slijedenje trajektorije za mobilnog robota.
//
//************************************************************************************************************************************************************************************
class CTrajTrackingRegulator_PI : public CTrajTrackingController
{
public:
    CTrajTrackingRegulator_PI()
    {
        m_ControllerType = TTC_PI;

        dat = fopen("Rezultati_PI.da", "wt");
    }

    ~CTrajTrackingRegulator_PI() { fclose(dat); }
    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Funkcija racuna nove referentne brzine za robota. Poziva se za svaki novi vremenski uzorak.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool GetRefValues(typename CMobileRobot::SRobotState* pState,   // (in), u ovoj strukturi nalazi se trenutno stanje robota
                      typename CMobileRobot::SRobotState* pRefState // (out), u ovu strukturu postavlja se referentna lin. i kutna brzina
                                                                    // robota. Ostali clanovi strukture se ignoriraju
    )
    {
        assert(pState && pRefState);

        if(!IsValid())
        {
            assert(0);
            return false;
        }

        T Time = pState->CurTime; // trenutno vrijeme

        // Uzimamo referentnu tocku iz trajektorije (ref. tocka je funkcija vremena)
        C2DTrajectoryBase::S2DTrajPoint RefPoint;
        RefPoint.Time = Time + m_SamplingPeriod; // Uzimamo reference s jednim korakom unaprijed - dobije se bolje ponasanje zbog kasnjenja
        if(!m_pTraj->GetPointByTime(&RefPoint))
            return false;

        // Tocka trajektorije
        T xTraj = RefPoint.x;
        T yTraj = RefPoint.y;
        T angTraj = RefPoint.Angle;
        T vTraj = RefPoint.Velocity;
        T wTraj = RefPoint.GetAngVelocity();

        // Predikcija zbog kasnjenja

        CTrajTrackingController::GetRefValues(pState, nullptr);

        // Sad uzimamo predvidena stanja robota kao mjerena stanja
        T xm = pState->x;
        T ym = pState->y;
        T angm = pState->Angle;

        fprintf(dat, "%f ", xTraj);
        fprintf(dat, "%f ", xm);
        fprintf(dat, "%f ", yTraj);
        fprintf(dat, "%f ", ym);
        fprintf(dat, "%f ", angTraj);
        fprintf(dat, "%f ", angm);

        // Predikcija zbog kasnjenja

        CompensateDelay(pState);

        // Sad uzimamo predvidena stanja robota kao mjerena stanja
        xm = pState->x;
        ym = pState->y;
        angm = pState->Angle;

        // Proracun izlaza regulatora (tj. ref lin. i kutne brzine robota)

        // algoritam ...

        T ym1 = xm + m_b * cos(angm);
        T ym2 = ym + m_b * sin(angm);
        T y1d = xTraj + m_b * cos(angTraj);
        T y2d = yTraj + m_b * sin(angTraj);

        T u1 = (m_SamplingPeriod * m_ti1) * (y1d - ym1) + m_u1minus;
        T u2 = (m_SamplingPeriod * m_ti2) * (y2d - ym2) + m_u2minus;

        m_u1minus = u1;
        m_u2minus = u2;

        u1 = u1 + m_kp1 * (y1d - ym1);
        u2 = u2 + m_kp2 * (y2d - ym2);

        T vRef = cos(angm) * u1 + sin(angm) * u2 + vTraj; // ref. linearna brzina [m/s]
        T wRef = -sin(angm) * u1 / m_b + cos(angm) * u2 / m_b + wTraj;
        ; // ref. kutna brzina [rad/s]

        // Napuni strukturu s ref. stanjem
        pRefState->Velocity = vRef;
        pRefState->AngVelocity = wRef;
        pRefState->x = xTraj;
        pRefState->y = yTraj;
        pRefState->Angle = angTraj;

        // Ogranici ref. vrijednosti
        BoundRefValues(pRefState);

        fprintf(dat, "%f ", vTraj);
        fprintf(dat, "%f ", pRefState->Velocity);
        fprintf(dat, "%f ", wTraj);
        fprintf(dat, "%f ", pRefState->AngVelocity);

        fprintf(dat, "\n");
        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Struktura za zadavanje parametara regulatora
    //____________________________________________________________________________________________________________________________________________________________________________________
    struct SParms : public CTrajTrackingController::SParms
    {
        // Ovdje se deklariraju parametri, kao npr. pojacanja regulatora, vrijeme diskretizacije itd, npr:

        T b;
        T u1minus;
        T u2minus;
        T y1dminus;
        T y2dminus;
        T kp1;
        T kp2;
        T ti1;
        T ti2;
    };

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje parametara regulatora.
    // Ovu funkciju uvijek treba pozvati prije koristenja regulatora.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool SetParms(SParms* pParms)
    {
        if(!CTrajTrackingController::SetParms(pParms))
            return false;

        m_b = pParms->b;
        m_u1minus = pParms->u1minus;
        m_u2minus = pParms->u2minus;
        m_y1dminus = pParms->y1dminus;
        m_y2dminus = pParms->y2dminus;
        m_kp1 = pParms->kp1;
        m_kp2 = pParms->kp2;
        m_ti1 = pParms->ti1;
        m_ti2 = pParms->ti2;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera stanja klase
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValid() const
    {
        if(!CTrajTrackingController::IsValid())
            return false;

        if(!IsValidParms())
            return false;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera dal su dobro postavljeni parametri
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValidParms() const
    {
        // TUDU - provjera
        /*if(m_SamplingPeriod<=0)
            return false;

        if(m_vMax<=0)
            return false;

        if(m_wMax<=0)
            return false;*/

        return true;
    }

private:
    // Parametri regulatora
    T m_b;
    T m_u1minus;
    T m_u2minus;
    T m_y1dminus;
    T m_y2dminus;
    T m_kp1;
    T m_kp2;
    T m_ti1;
    T m_ti2;

    FILE* dat;
};

// Primjer koristenja regulatora - vjerojatno nije updatiran s promjenom koda :)
// void main()
//{
//	CTrajTrackingRegulator_PI Regulator;
//	CTrajTrackingRegulator_PI::SParms Parms;

//	Parms.b=0.02;
//  Parms.u1minus=0;
//  Parms.u2minus=0;
//  Parms.y1dminus=0;
//  Parms.y2dminus=0;
//  Parms.kp1=10;
//  Parms.kp2=10;
//  Parms.ti1=6.25;
//  Parms.ti2=6.25;
//
//
//	Parms.pTraj = nullptr;
//	Parms.SamplingPeriod = (1/80);
//
//	Parms.vMax = 2;
//	Parms.wMax = 2/(0.067/2);
//
//	fprintf(Regulator.dat,"xTraj xm yTraj ym angTraj angm CurvTraj vTraj vRef wTraj wRef\n");
//
//	if(!Regulator.SetParms(&Parms))
//		return;
//
//	for(int i=0; i<2400; i++)
//	{
//		CMobileRobot::SRobotState MeasuredState;
//		CMobileRobot::SRobotState RefState;
//
//		MeasuredState.x = ...;
//		MeasuredState.y = ...;
//		...
//
//		Regulator.GetRefValues(&MeasuredState, &RefState);
//
//		// Posalji reference robotu
//		float vRef = RefState.Velocity;
//		float wRef = RefState.AngVelocity;
//
//		...
//	}
//}

#endif // _TrajTrackingRegulator_Linear_h_
