#ifndef _2DTrajectoryBase_h_
#define _2DTrajectoryBase_h_

#include <assert.h>
#include <fstream>

class CTrajPathProfile;

class CTrajTimeProfile;

//=================================================================================================================================================================
//
// Definicija tipova za trajektorije
//
//=================================================================================================================================================================
class C2DTrajectoryDataTypes
{
public:
    // Opceniti brojcani tip, moze se definirati kao float ili double
    typedef double T;

    inline static T abs(T x) { return (x >= 0 ? x : -x); }

    static const T PI;
};

//=================================================================================================================================================================
//
// Bazna (apstraktna) klasa za trajektorije
//
//=================================================================================================================================================================
class C2DTrajectoryBase : public C2DTrajectoryDataTypes
{
public:
    C2DTrajectoryBase() { m_bInverted = false; }
    // virtual ~C2DTrajectoryBase(){};

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Sempliranje trajektorije i zapisivanje stanja trajektorije u datoteku.
    // Trajektorija se semplira po koordinati udaljenosti.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool LogTrajectoryByDist(const char* Filename, // Datoteka u koju zapisujemo, format zapisa je jedno stanje - jedan redak
                             T Delta               // Udaljenost izmedu dvije tocke
                             ) const
    {
        std::ofstream LogFile(Filename);
        if(LogFile.fail())
            return false;

        LogFile << std::fixed;   // Tak da svaki float broj zauzima isto mjesta
        //LogFile << std::showpos; // Tak da pozitivni brojevi imaju +

        //LogFile << "% Ispis trajektorije u datoteku \n";
        //LogFile << "% Podaci u stupcima imaju poredak: \n";
        //LogFile << "% Time Disp Velocity Accel AngVelocity AngAccel Dist x y Angle Curv dCurv\n";

        S2DTrajPoint TrajPt;
        TrajPt.SetAllZero();
        T Dist1 = GetBeginDist();
        T Dist2 = GetEndDist();

        for(T Dist = Dist1; Dist <= Dist2; Dist += Delta)
        {            
            TrajPt.Dist = Dist;
            GetPointByDist(&TrajPt);
            //LogFile << TrajPt.Time << ',' << TrajPt.x << ',' << TrajPt.y << ','  << TrajPt.Angle << ',' << TrajPt.Velocity << ',' << TrajPt.GetAngVelocity() << std::endl;

            LogFile << TrajPt.Time << ',' << ' ' << TrajPt.x << ',' << ' ' << TrajPt.y << ',' << ' ' << TrajPt.Angle << ',' << ' ' << TrajPt.Velocity << ',' << ' ' << TrajPt.GetAngVelocity() << ',' << ' ' << TrajPt.Curv << std::endl;
            // LogFile << TrajPt.Time << ' ' << TrajPt.Disp << ' ' << TrajPt.Velocity << ' ' << TrajPt.Accel << ' ' << TrajPt.GetAngVelocity()
            //         << ' ' << TrajPt.GetAngAccel() << ' ' << TrajPt.Dist << ' ' << TrajPt.x << ' ' << TrajPt.y << ' ' << TrajPt.Angle << ' '
            //         << TrajPt.Curv << ' ' << TrajPt.DCurv << std::endl;
        }
        LogFile.close();
        return true;
    
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Sempliranje trajektorije i zapisivanje stanja trajektorije u datoteku.
    // Trajektorija se semplira po koordinati vremena.
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool LogTrajectoryByTime(const char* Filename, // Datoteka u koju zapisujemo, format zapisa je jedno stanje - jedan redak
                             T Delta               // Vremenska udaljenost izmedu dvije tocke
                             ) const
    {
        std::ofstream LogFile(Filename);
        if(LogFile.fail())
            return false;

        LogFile << std::fixed;   // Tak da svaki float broj zauzima isto mjesta
        LogFile << std::showpos; // Tak da pozitivni brojevi imaju +

        LogFile << "% Ispis trajektorije u datoteku \n";
        LogFile << "% Podaci u stupcima imaju poredak: \n";
        LogFile << "% Time Disp Velocity Accel AngVelocity AngAccel Dist x y Angle Curv dCurv\n";

        S2DTrajPoint TrajPt;
        TrajPt.SetAllZero();
        T Time1 = GetBeginTime();
        T Time2 = GetEndTime();
        for(T Time = Time1; Time <= Time2; Time += Delta)
        {
            TrajPt.Time = Time;
            GetPointByTime(&TrajPt);

            LogFile << TrajPt.Time << ' ' << TrajPt.Disp << ' ' << TrajPt.Velocity << ' ' << TrajPt.Accel << ' ' << TrajPt.GetAngVelocity()
                    << ' ' << TrajPt.GetAngAccel() << ' ' << TrajPt.Dist << ' ' << TrajPt.x << ' ' << TrajPt.y << ' ' << TrajPt.Angle << ' '
                    << TrajPt.Curv << ' ' << TrajPt.DCurv << std::endl;
        }

        LogFile.close();

        return true;
    }

    // Mod koji se koristi kad se izade izvan granice trajektorije
    enum EPathExtrapolationMode
    {
        EM_Unknown,
        EM_Line,   // Trajektorija se nastavlja linijom (tangenta na zadnju tocku)
        EM_Circle, // Trajektorija se nastavlja kontinuiranom zakrivljenoscu (npr. ako je krug, nastavlja se i dalje krug)
    };

    // Definira tocku trajektorije. Pojedini algoritam ce postaviti samo neke od clanova strukture, pa je potrebno prvo prouciti algoritam
    // prije upotrebe ove strukture.
    struct S2DTrajPoint
    {
        S2DTrajPoint()
        {
            // Radi kompatibilnosti
            bAngVelocityFromCurvature = true;
            bAngAccelFromCurvature = true;
        }

        // Vremenski dio
        T Time;
        T Disp; // Pomak koji se ucini po trajektoriji, moze samo rasti bez obzira na smjer u kojem se gibamo
        T Velocity;
        T Accel;

        // Kutna brzina i akceleracija mogu se zadati direktno, ili se mogu izracunati iz zakrivljenosti, ovisi od slucaja do slucaja, pa
        // treba prouciti dobro prije upotrebe :) Ako robot rotira na mjestu, treba ih zadati direktno, jer je tada zakrivljenost
        // beskonacna.
        T AngVelocity; // Kutna brzina
        T AngAccel;    // Kutna akceleracija

        // Prostorni dio
        T Dist; // Udaljenost prijedena po trajektoriji (ali ne euklidska), moze rasti i padati ovisno o smjeru u kojem se gibamo
        T x;
        T y;
        T Angle;
        T Curv;  // dAngle/dDist - brzina promjene kuta
        T DCurv; // dCurv/dDist - brzina promjene zakrivljenosti

        // DWORD Mask; // Rezervirano

        T GetAngVelocity() const
        {
            if(bAngVelocityFromCurvature)
                return (T)Velocity * Curv;
            else
                return AngVelocity;
        };

        T GetAngAccel() const
        {
            if(bAngAccelFromCurvature)
                return (T)Accel * Curv + (T)DCurv * Velocity * Velocity; // Nadam se da je ova formula tocna :)
            else
                return AngAccel;
        }

        void SetAllZero() { Time = Disp = Velocity = Accel = AngVelocity = AngAccel = Dist = x = y = Angle = Curv = DCurv = 0; }

        bool bAngVelocityFromCurvature; // dal se kutna brzina racuna iz zakrivljenosti
        bool bAngAccelFromCurvature;    // dal se kutna akceleracija racuna iz zakrivljenosti (tj. iz dCurv)
    };

    // Invertirana trajektorija hmm.
    static void InvertTrajPoint(S2DTrajPoint& TrajPt)
    {
        TrajPt.Velocity = -TrajPt.Velocity;
        TrajPt.Accel = -TrajPt.Accel;
        // TrajPt.Dist = -TrajPt.Dist; // Ovo se ne moze invertirati, trebalo bi zapravo invertirati promjenu puta, a ne sam put
        TrajPt.Angle = TrajPt.Angle + PI;
        TrajPt.Curv = -TrajPt.Curv;
        TrajPt.DCurv = -TrajPt.DCurv;
    };

    // Moze (ali ne mora, ovisno o implementaciji) vratiti sve varijable iz strukture S2DTrajPoint,
    // buduci da su sve jednoznacne funkcije vremena.
    // Funkcija ocekuje da je vec zadan Time clan strukture.
    // Ispravna strategija implementacije bila bi na temelju vremena izracunati vremenski dio
    // tocke trajektorije, te onda pozvati GetPointByDist kako bi se izracunao prostorni dio.
    virtual bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        if(IsInverted())
            InvertTrajPoint(*pTrPoint);
        return true;
    }

    // Moze (ali ne mora, ovisno o implementaciji) vratiti sve varijable iz strukture S2DTrajPoint,
    // buduci da su sve jednoznacne funkcije pomaka.
    // Opcenito, ovo ne vrijedi ako je brzina u bilo kojem intervalu dt>0 jednaka nuli, jer u tom
    // slucaju varijable nisu jednoznacne funkcije pomaka!!!!!!!!!
    // Funkcija ocekuje da je vec zadan Disp clan strukture.
    // Ispravna strategija implementacije bila bi na temelju pomaka izracunati vremenski dio
    // tocke trajektorije, te onda pozvati GetPointByDist kako bi se izracunao prostorni dio.
    virtual bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        if(IsInverted())
            InvertTrajPoint(*pTrPoint);
        return true;
    };

    // Moze (ali ne mora, ovisno o implementaciji) vratiti sve varijable iz strukture S2DTrajPoint
    // koje se odnose na prostorni dio trajektorije (x,y,Angle,Curv)
    // buduci da su sve jednoznacne funkcije udaljenosti.
    // Funkcija ne smije modificirati niti jedan drugi clan strukture!!!!!!!!
    // Funkcija ocekuje da je vec zadan Dist clan strukture.
    virtual bool GetPointByDist(S2DTrajPoint* pTrPoint) const
    {
        if(IsInverted())
            InvertTrajPoint(*pTrPoint);
        return true;
    };

    // Funkcije za manipuliranje ofsetima itd...
    // Glavna svrha je omoguciti nadovezivanje trajektorija jedne na drugu

    // Vrijeme
    virtual T GetBeginTime() const
    { /*assert(0);*/
        return 0;
    };
    virtual T GetDeltaTime() const
    { /*assert(0);*/
        return 0;
    };
    virtual void SetBeginTime(T BeginTime){};

    T GetEndTime() const { return GetBeginTime() + GetDeltaTime(); }

    // Pomak
    virtual T GetBeginDisp() const
    { /*assert(0);*/
        return 0;
    };
    virtual T GetDeltaDisp() const
    { /*assert(0);*/
        return 0;
    };
    virtual void SetBeginDisp(T BeginDisp){};

    T GetEndDisp() const { return GetBeginDisp() + GetDeltaDisp(); }

    // Udaljenost
    virtual T GetBeginDist() const { return 0; };
    virtual T GetDeltaDist() const { return 0; };
    virtual void SetBeginDist(T BeginDist){};

    T GetEndDist() const { return GetBeginDist() + GetDeltaDist(); }

    // Validacija i dijagnostika objekta.
    // Ova funkcija bi trebala prvo pozvati IsValid() od bazne klase i ako on vrati false, takoder
    // vratiti false.
    // Nakon toga treba obaviti provjeru parametara, tj internog stanja objekta.
    // The definition of �validity� depends on the object�s class.
    // As a rule, the function should perform a �shallow check.�
    // That is, if an object contains pointers to other objects,
    // it should check to see whether the pointers are not null,
    // but it should not perform validity testing on the objects referred to by the pointers.
    virtual bool IsValid() const
    {
        if(GetDeltaDisp() < 0) // Promjena pomaka uvijek mora biti >= 0
            return false;

        if(GetDeltaTime() < 0) // Promjena vremena uvijek mora biti >= 0
            return false;

        // Da li je ispravan mod ekstrapolacije?
        if(GetPathBeginExtrapolationMode() < EM_Unknown || GetPathBeginExtrapolationMode() > EM_Circle)
            return false;

        return true;
    }

    // ExtrapolationMode

    // Ako trajetkorija podrzava ekstrapolaciju, treba implementirati ove funkcije i
    // vratiti trenutni mod ekstrapolacije

    // Ekstrapolacija prije pocetka trajektorije
    virtual EPathExtrapolationMode GetPathBeginExtrapolationMode() const { return EM_Unknown; }

    // Ekstrapolacija poslije kraja trajektorije
    virtual EPathExtrapolationMode GetPathEndExtrapolationMode() const { return EM_Unknown; }

    // Ako trajetkorija podrzava ekstrapolaciju, treba implementirati sljedece dvije funkcije
    // i postaviti trenutni mod ekstrapolacije

    // Ekstrapolacija prije pocetka trajektorije
    virtual bool SetPathBeginExtrapolationMode(EPathExtrapolationMode BeginMode)
    {
        assert(0); // Ako se izvrsi ovaj kod, onda trajektorija ne podrzava ekstrapolaciju
        return false;
    }

    // Ekstrapolacija poslije kraja trajektorije
    virtual bool SetPathEndExtrapolationMode(EPathExtrapolationMode EndMode)
    {
        assert(0); // Ako se izvrsi ovaj kod, onda trajektorija ne podrzava ekstrapolaciju
        return false;
    }

    // Pomocna funkcija koja ce raditi za sve vrste pathova i ekstrapolirati path prije
    // pocetka. Buduci da je opcenita, funkcija ce raditi dosta sporije nego da se
    // implementira specijalni kod za svaku vrstu patha.
    // bool GetPointByDist_BeforeBegin(S2DTrajPoint* pTrPoint, EPathExtrapolationMode BeginExtrapolationMode)
    //{
    //	T WantedDist = pTrPoint->Dist; // Distance za tocku koja se trazi

    //	// Dobij pocetnu tocku trajektorije
    //	T BeginDist = GetBeginDist();
    //	pTrPoint->Dist = BeginDist; // Ovo radimo samo privremeno
    //	if(!GetPointByDist(pTrPoint))
    //	{
    //		assert(0);
    //		return false;
    //	}

    //	T dd = WantedDist - BeginDist;
    //	assert(dd<0); // Ovu funkciju ima smisla zvati samo ako smo "prije pocetka"

    //	pTrPoint->Dist = WantedDist; // Ponovo vracamo staru vrijednost

    //	switch(BeginExtrapolationMode)
    //	{
    //	case EM_Line:
    //		{
    //			// Ekstrapolacija pravcem
    //			T BeginAng = pTrPoint->Angle;
    //			pTrPoint->x += dd*(T)cos(Ang);
    //			pTrPoint->y += dd*(T)sin(Ang);
    //			pTrPoint->Curv = 0;
    //			pTrPoint->DCurv = 0;
    //		}
    //		break;

    //	case EM_KeepCurvature:
    //		{
    //			// Ekstrapolacija kruznicom

    //			// Kut i zakrivljenost u pocetnoj tocki trajektorije
    //			T BeginAng = pTrPoint->Angle;
    //			T BeginCurv = pTrPoint->Curv;

    //			if(BeginCurv!=0)
    //			{
    //				// Ovo ce raditi i za pozitivan i za negativan BeginCurv

    //				// Racunanje sredista kruznice
    //				T Ang = BeginAng + PI/2; // Kut od pocetne tocke prema sredistu kruznice
    //				T r = 1/BeginCurv; // Moze biti i negativan
    //				T xc = pTrPoint->x + r*(T)cos(Ang);
    //				T yc = pTrPoint->y + r*(T)sin(Ang);

    //				T DeltaAng = dd*BeginCurv; // Promjena kuta u odnosu na poc. tocku trajektorije
    //				T CircAng = DeltaAng+Ang+PI; // Kut od sredista prema trazenoj tocki trajektorije

    //				pTrPoint->x = xc + r*(T)cos(CircAng);
    //				pTrPoint->y = yc + r*(T)sin(CircAng);

    //				pTrPoint->Angle = BeginAng+DeltaAng;

    //				// Curv ostaje isti
    //				pTrPoint->DCurv = 0;
    //			}
    //			else
    //			{
    //				// Isto kao i za pravac
    //				T Ang = pTrPoint->Angle;
    //				pTrPoint->x += dd*(T)cos(Ang);
    //				pTrPoint->y += dd*(T)sin(Ang);
    //				pTrPoint->Curv = 0;
    //				pTrPoint->DCurv = 0;
    //			}
    //		}
    //		break;

    //	default:
    //		assert(0);
    //		return false;
    //	}

    //	return true;
    //}

    virtual CTrajPathProfile* GetCurrentPath() { return nullptr; }

    virtual CTrajTimeProfile* GetCurrentTimeProfile() { return nullptr; }

    bool IsInverted() const { return m_bInverted; }
    void SetInverted(bool bInverted) { m_bInverted = bInverted; }

protected:
    bool m_bInverted; // Dal je trajektorija invertirana
};

#endif // _2DTrajectoryBase_h_
