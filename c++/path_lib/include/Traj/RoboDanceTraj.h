#ifndef _RoboDanceTrajectory_h_
#define _RoboDanceTrajectory_h_

#include "Math/AngleRange.h"
#include "Modules/RoboDance.h"

#include <assert.h>
#include <stdio.h>
#include <vector>

//=================================================================================================================================================================
//
// Klasa koja racuna trajektoriju za pojedinog robota za robotski ples.
//
//=================================================================================================================================================================
template<typename T>
class CRoboDanceTrajectory : public C2DTrajectoryBase
{
public:
    CRoboDanceTrajectory()
    {
        m_pRoboDanceModule = nullptr;
        m_BeginTime = 0;
    }

    // virtual ~CRoboDanceTrajectory(){};

    virtual bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!m_pRoboDanceModule)
            return false;

        pTrPoint->Time -= GetBeginTime(); // treba oduzeti pocetno vrijeme da bi radil poziv sljedece funkcije
        if(!m_pRoboDanceModule->GetTrajectoryPoint(m_iRobot, pTrPoint))
            return false;

        pTrPoint->Time += GetBeginTime(); // vracam vrijeme na staro

        return true;
    }

    virtual T GetBeginTime() const { return m_BeginTime; };
    virtual T GetDeltaTime() const { return (T)m_pRoboDanceModule->GetDuration(); }
    virtual void SetBeginTime(T BeginTime) { m_BeginTime = BeginTime; }

    virtual bool IsValid() const
    {
        if(!C2DTrajectoryBase::IsValid())
            return false;

        if(!m_pRoboDanceModule)
            return false;

        if(m_iRobot < 0)
            return false;

        return true;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje pokazivaca na modul za robotski ples
    //_________________________________________________________________________________________________________________________________________________________
    void SetRoboDanceModule(RoboDance* pRoboDanceModule)
    {
        assert(pRoboDanceModule);
        m_pRoboDanceModule = pRoboDanceModule;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje indeksa robota
    //_________________________________________________________________________________________________________________________________________________________
    void SetRobot(int iRobot)
    {
        assert(iRobot >= 0);
        m_iRobot = iRobot;
    }

protected:
    RoboDance* m_pRoboDanceModule; // Pokazivac na modul za robotski ples

    int m_iRobot; // Na kojeg po redu robota (iz RoboDance modula) se odnosi ova trajektorija. Indeksi pocinju od nule.

    T m_BeginTime; // Pocetno vrijeme trajekotrije
};

#endif
