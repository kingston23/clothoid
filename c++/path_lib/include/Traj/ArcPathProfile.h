#ifndef _Arc_Path_Profile_h_
#define _Arc_Path_Profile_h_

#include "TrajPathProfile.h"

class CArcPathProfile : public CTrajPathProfile
{
public:
    CArcPathProfile()
    {
        m_bParmsSet = false;

        m_BeginExtrapolationMode = EM_Line;
        m_EndExtrapolationMode = EM_Line;
    }

    virtual EPathProfileType GetPathProfileType() const { return PP_Circle; }

    // Postavlja x, y, Angle i Curv ovisno o Dist parametru iz strukture.
    // Ostali clanovi strukture se ne modificiraju.
    // Ne vrsi se testiranje granica za Dist!!! (osim preko assert)
    bool GetPointByDist(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        T dd = pTrPoint->Dist - m_BeginDist;

        // assert(dd>=0 && dd<=m_DeltaDist); // Provjera granica

        T Angle0 = 0; // Kut za ekstrapolaciju
        T ddExtr = 0;
        bool bExtrapolateLine = false;
        if(m_BeginExtrapolationMode == EM_Line && dd < 0) // Ako je EM_Circle, nije potreban poseban kod
        {
            // Ekstrapolacija prije pocetka trajektorije
            Angle0 = m_ArcBeginAngle;
            ddExtr = dd;
            bExtrapolateLine = true;
        }

        if(m_EndExtrapolationMode == EM_Line && dd > m_DeltaDist)
        {
            // Ekstrapolacija poslije kraja trajektorije
            Angle0 = m_ArcBeginAngle + m_DeltaDist * m_Curv;
            ddExtr = dd - m_DeltaDist;
            bExtrapolateLine = true;
        }

        if(bExtrapolateLine)
        {
            // Uzmemo pocetnu tocku i kut kruznog luka
            T C = (T)cos(Angle0);
            T S = (T)sin(Angle0);
            T x0 = m_xc + m_r * C;
            T y0 = m_yc + m_r * S;

            if(m_Curv > 0)
                pTrPoint->Angle = Angle0 + PI / 2;
            else
                pTrPoint->Angle = Angle0 - PI / 2;
            pTrPoint->x = x0 + cos(pTrPoint->Angle) * ddExtr;
            pTrPoint->y = y0 + sin(pTrPoint->Angle) * ddExtr;
            pTrPoint->Curv = 0;
            pTrPoint->DCurv = 0;

            return true;
        }

        T CircAng = dd * m_Curv + m_ArcBeginAngle;

        pTrPoint->x = m_xc + m_r * (T)cos(CircAng);
        pTrPoint->y = m_yc + m_r * (T)sin(CircAng);
        if(m_Curv > 0)
            pTrPoint->Angle = CircAng + PI / 2;
        else
            pTrPoint->Angle = CircAng - PI / 2;
        pTrPoint->Curv = m_Curv;

        return true;
    };

    void SetBeginDist(T BeginDist) { m_BeginDist = BeginDist; };
    T GetBeginDist() const { return m_BeginDist; }
    T GetDeltaDist() const { return m_DeltaDist; }

    // Zadavanje luka preko sredista, radijusa, i pocetnog kuta luka (a ne trajektorije!)
    struct SParms0
    {
        T BeginDist;
        T DeltaDist;
        T xCenter; // Srediste kruznice [m]
        T yCenter;
        T Radius;                // >0
        T ArcBeginAngle;         // Kut po�etne to�ke trajektorije (kut tangente na kru�nicu u po� to�ki)
        bool bPositiveDirection; // za true je pozitivan smjer obilazenja po luku
    };

    // Direktno zadavanje (preko sredista kruznice)
    bool SetParms(SParms0* pParms)
    {
        if(pParms->Radius <= 0)
        {
            assert(0);
            return false;
        }
        m_r = pParms->Radius;
        m_BeginDist = pParms->BeginDist;
        m_DeltaDist = pParms->DeltaDist;
        m_xc = pParms->xCenter;
        m_yc = pParms->yCenter;
        if(pParms->bPositiveDirection)
        {
            m_Curv = 1 / m_r;
        }
        else
        {
            m_Curv = -1 / m_r;
        }
        m_ArcBeginAngle = pParms->ArcBeginAngle;

        m_bParmsSet = true;
        return true;
    };

    // Zadavanje luka preko sredista, radijusa, i kuta trajektorije (a ne luka!) u poc. tocki
    struct SParms1
    {
        T BeginDist;
        T DeltaDist;
        T xCenter; // Srediste kruznice [m]
        T yCenter;
        T Radius;                // >0
        T TrajAngle0;            // Kut po�etne to�ke trajektorije (kut tangente na kru�nicu u po� to�ki)
        bool bPositiveDirection; // za true je pozitivan smjer obilazenja po luku
    };

    bool SetParms(SParms1* pParms)
    {
        if(pParms->Radius <= 0)
        {
            assert(0);
            return false;
        }
        m_r = pParms->Radius;
        m_BeginDist = pParms->BeginDist;
        m_DeltaDist = pParms->DeltaDist;
        m_xc = pParms->xCenter;
        m_yc = pParms->yCenter;
        if(pParms->bPositiveDirection)
        {
            m_ArcBeginAngle = pParms->TrajAngle0 - PI / 2;
            m_Curv = 1 / m_r;
        }
        else
        {
            m_ArcBeginAngle = pParms->TrajAngle0 + PI / 2;
            m_Curv = -1 / m_r;
        }

        m_bParmsSet = true;
        return true;
    };

    // Parametri za zadavanje kruznice preko pocetne tocke na luku (a ne sredista)
    struct SParms2
    {
        T BeginDist;
        T DeltaDist;
        T x0; // Pocetna tocka na kruznici [m]
        T y0;
        T Radius;                // >0
        T TrajAngle0;            // Kut po�etne to�ke trajektorije (kut tangente na kru�nicu u po� to�ki)
        bool bPositiveDirection; // za true je pozitivan smjer obilazenja po luku
    };

    // Indirektno zadavanje (preko po�etne to�ke trajektorije)
    bool SetParms(SParms2* pParms)
    {
        if(pParms->Radius <= 0)
        {
            assert(0);
            return false;
        }
        m_BeginDist = pParms->BeginDist;
        m_DeltaDist = pParms->DeltaDist;
        m_r = pParms->Radius;
        if(pParms->bPositiveDirection)
        {
            m_ArcBeginAngle = pParms->TrajAngle0 - PI / 2;
            m_Curv = 1 / m_r;
        }
        else
        {
            m_ArcBeginAngle = pParms->TrajAngle0 + PI / 2;
            m_Curv = -1 / m_r;
        }

        // Izracunaj srediste kruznice iz pocetne tocke na kruznici
        m_xc = pParms->x0 + m_r * (T)cos(m_ArcBeginAngle + PI);
        m_yc = pParms->y0 + m_r * (T)sin(m_ArcBeginAngle + PI);
        m_bParmsSet = true;

        return true;
    };

    bool IsValid() const
    {
        if(!CTrajPathProfile::IsValid())
            return false;

        if(m_r <= 0)
            return false;

        if(m_Curv == 0)
            return false;

        if(m_BeginExtrapolationMode != EM_Line && m_BeginExtrapolationMode != EM_Circle)
            return false;

        if(m_EndExtrapolationMode != EM_Line && m_EndExtrapolationMode != EM_Circle)
            return false;

        return m_bParmsSet;
    }

    // Pristupne rutine

    // Paznja - moze biti negativan radijus
    T GetRadius() const
    {
        assert(IsValid());
        return m_r;
    }

    T GetCurvature() const
    {
        assert(IsValid());
        return m_Curv;
    }

    void GetCenter(T& xc, T& yc) const
    {
        assert(IsValid());
        xc = m_xc;
        yc = m_yc;
    }

    T GetAngle0() const
    {
        assert(IsValid());
        return m_ArcBeginAngle;
    }

    // Ekstrapolacija
    virtual EPathExtrapolationMode GetPathBeginExtrapolationMode() const { return m_BeginExtrapolationMode; }

    virtual EPathExtrapolationMode GetPathEndExtrapolationMode() const { return m_EndExtrapolationMode; }

    bool SetPathBeginExtrapolationMode(EPathExtrapolationMode BeginMode)
    {
        if(BeginMode != EM_Line && BeginMode != EM_Circle)
            return false;

        m_BeginExtrapolationMode = BeginMode;
        return true;
    }

    bool SetPathEndExtrapolationMode(EPathExtrapolationMode EndMode)
    {
        if(EndMode != EM_Line && EndMode != EM_Circle)
            return false;

        m_EndExtrapolationMode = EndMode;
        return true;
    }

private:
    // Parametri
    T m_BeginDist; // Pocetni pomak
    T m_DeltaDist; // Promjena pomaka
    T m_xc;        // Srediste kruznice
    T m_yc;
    T m_r; // (>0) Radijus kruznice

    // Pomocne varijable
    T m_Curv; // (!=0) zakrivljenost, tj. inverz radijusa, ali sa predznakom, negativno za negativan smjer obilazenja po luku

    T m_ArcBeginAngle; // Pocetni kut (ne tangente nego u odnosu na srediste)

    bool m_bParmsSet;

    // Ekstrapolacija
    EPathExtrapolationMode m_BeginExtrapolationMode;
    EPathExtrapolationMode m_EndExtrapolationMode;
};

/*
    Primer:

    CArcPathProfile::SParms2 CircleParms;
    CircleParms.BeginDist = 0;
    CircleParms.DeltaDist = 2;
    CircleParms.Radius = -1;
    CircleParms.x0 = 0;
    CircleParms.y0 = 0;
    CircleParms.TrajAngle0 = 0;
    CArcPathProfile CirclePath;
    CirclePath.SetParms(&CircleParms);

    CirclePath.SetEndExtrapolationMode(C2DTrajectoryBase.EM_Line);
*/

#endif // _Arc_Path_Profile_h_
