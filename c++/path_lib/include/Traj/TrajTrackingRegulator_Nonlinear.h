#ifndef _TrajTrackingRegulator_NONlinear_h_
#define _TrajTrackingRegulator_NONlinear_h_

#include "Geom/Line.h"
#include "Math/AngleRange.h"
#include "TrajTrackingController.h"

using namespace std;

//************************************************************************************************************************************************************************************
//
// Nelinearni regulator za slijedenje trajektorije za mobilnog robota.
//
//************************************************************************************************************************************************************************************
class CTrajTrackingRegulator_Nonlinear : public CTrajTrackingController
{
public:
    CTrajTrackingRegulator_Nonlinear() { m_ControllerType = TTC_Nonlinear; }

    virtual bool OpenLogFile(const char* FileName)
    {
        m_LogFile.close();
        m_LogFile.clear();
        m_LogFile.open(FileName, ios::out);
        if(!m_LogFile)
            return false;

        m_LogFile << "% Rezultati eksperimenta s nelinearnim regulatorom za slijedenje trajektorije \n";
        m_LogFile << "% Podaci u stupcima imaju poredak: \n";
        m_LogFile << "% Vrijeme | Trajektorija | Mjerenja | Pogreska | Reference \n";
        m_LogFile << "% Detaljnije: \n";
        m_LogFile << "% t xTraj yTraj angTraj vTraj wTraj aTraj xm ym angm vm wm e1 e2 e3 vRef wRef\n";

        m_LogFile << fixed;   // Tak da svaki float broj zauzima isto mjesta
        m_LogFile << showpos; // Tak da pozitivni brojevi imaju +

        // cout.precision(6); // broj znacajnih znamenki (default je 6)
        // cout.width (10); // koliko znamenka zauzima ukupno mjesta (ostatak se nadopunjava razmacima), izgleda da ovo ne dela za float

        return true;
    }

    ~CTrajTrackingRegulator_Nonlinear() {}

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Funkcija racuna nove referentne brzine za robota. Poziva se za svaki novi vremenski uzorak.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool GetRefValues(typename CMobileRobot::SRobotState* pState,   // (in), u ovoj strukturi nalazi se trenutno stanje robota
                              typename CMobileRobot::SRobotState* pRefState // (out), u ovu strukturu postavlja se referentna lin. i kutna
                                                                            // brzina robota. Ostali clanovi strukture se ignoriraju
    )
    {
        assert(pState && pRefState);

        if(!IsValid())
        {
            assert(0);
            return false;
        }

        T Time = pState->CurTime; // trenutno vrijeme

        // Uzimamo referentnu tocku iz trajektorije (ref. tocka je funkcija vremena)
        C2DTrajectoryBase::S2DTrajPoint RefPoint;
        RefPoint.Time = Time + (T)0.5 * m_SamplingPeriod; // Uzimamo reference s x koraka unaprijed
        if(!m_pTraj->GetPointByTime(&RefPoint))
            return false;

        // Tocka trajektorije
        T xTraj = RefPoint.x;
        T yTraj = RefPoint.y;
        T angTraj = RefPoint.Angle;
        T vTraj = RefPoint.Velocity;
        T wTraj = RefPoint.GetAngVelocity();
        T aTraj = RefPoint.Accel;

        // Stanje robota
        T xm = pState->x;
        T ym = pState->y;
        T angm = pState->Angle;

        T vm = pState->Velocity;
        T wm = pState->AngVelocity;

        m_LogFile << Time << ' ' << xTraj << ' ' << yTraj << ' ' << angTraj << ' ' << vTraj << ' ' << wTraj << ' ' << aTraj << ' ';
        m_LogFile << xm << ' ' << ym << ' ' << angm << ' ' << vm << ' ' << wm << ' ';

        // Predikcija zbog kasnjenja
        CompensateDelay(pState);

        // Sad uzimamo predvidena stanja robota kao mjerena stanja
        xm = pState->x;
        ym = pState->y;
        angm = pState->Angle;

        vm = pState->Velocity;
        wm = pState->AngVelocity;

        // Proracun izlaza regulatora (tj. ref lin. i kutne brzine robota)

        // Pogreske slijedenja
        T e1 = cos(angm) * (RefPoint.x - xm) + sin(angm) * (RefPoint.y - ym);
        T e2 = -sin(angm) * (RefPoint.x - xm) + cos(angm) * (RefPoint.y - ym);
        T e3 = CAngleRange::Range_MinusPI_PI(RefPoint.Angle - angm);

        // Zbroji pogreske - to je SSE (sum of squared errors)
        m_SSE1 += e1 * e1;
        m_SSE2 += e2 * e2;
        m_SSE3 += e3 * e3;

        // Koeficijenti za algoritam
        T k1 = 2 * m_zeta * sqrt(pow(RefPoint.GetAngVelocity(), 2) + m_b * pow(RefPoint.Velocity, 2));
        T k3 = k1;
        T k2 = m_b;

        // Vektor ub (feedback vektor);
        T ub1 = k1 * e1;
        T ub2;
        if(e3 > (T)0.001)
            ub2 = k2 * RefPoint.Velocity * sin(e3) / e3 * e2 + k3 * e3;
        else
            // Ispustamo sin(e3)/e3 jer je za mali e3 to jednako 1 (bez ovoga imali bi moguce dijeljenje s nulom kad je e3=0).
            // Za ovaj slucaj je nelinearni regulator zapravo skroz jednak linearnom, jer je jedina razlika medu njima zapravo taj
            // sin(e3)/e3.
            ub2 = k2 * RefPoint.Velocity * e2 + k3 * e3;

        // Zbroji feedforward i feedback da bismo dobili upravljacke signale
        T vRef = 1.f * RefPoint.Velocity * cos(e3) + ub1; // ref. linearna brzina [m/s]
        T wRef = 1.f * RefPoint.GetAngVelocity() + ub2;   // ref. kutna brzina [rad/s]

        // Napuni strukturu s ref. stanjem
        pRefState->Velocity = vRef;
        pRefState->AngVelocity = wRef;
        pRefState->x = xTraj;
        pRefState->y = yTraj;
        pRefState->Angle = angTraj;

        // Ogranici ref. vrijednosti
        BoundRefValues(pRefState);

        m_LogFile << e1 << ' ' << e2 << ' ' << e3 << ' ' << ' ' << pRefState->Velocity << ' ' << pRefState->AngVelocity << endl;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Struktura za zadavanje parametara regulatora
    //____________________________________________________________________________________________________________________________________________________________________________________
    struct SParms : public CTrajTrackingController::SParms
    {
        // Parametri regulatora
        T b;
        T zeta;
    };

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje parametara regulatora.
    // Ovu funkciju uvijek treba pozvati prije koristenja regulatora.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool SetParms(CTrajTrackingController::SParms* pBaseParms)
    {
        if(!CTrajTrackingController::SetParms(pBaseParms))
            return false;

        SParms* pParms = static_cast<SParms*>(pBaseParms);

        m_b = pParms->b;
        m_zeta = pParms->zeta;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera stanja klase
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValid() const
    {
        if(!CTrajTrackingController::IsValid())
            return false;

        if(!IsValidParms())
            return false;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera dal su dobro postavljeni parametri
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValidParms() const
    {
        if(m_zeta < 0)
            return false;

        if(m_b < 0)
            return false;

        return true;
    }

private:
    // Parametri regulatora

    T m_zeta; // Faktor prigusenja modelske prijenosne funkcije
    T m_b;    // Dodatni faktor za podesavanje

    std::ofstream m_LogFile; // Log datoteka
};

//************************************************************************************************************************************************************************************
//
// Moj regulator za slijedenje trajektorije za mobilnog robota koji se bazira na nelinearnom
//
//************************************************************************************************************************************************************************************
class CTrajTrackingRegulator_Moj2 : public CTrajTrackingController
{
public:
    CTrajTrackingRegulator_Moj2()
    {
        m_ControllerType = TTC_Moj2;

        m_LogFile.open("Log/Rezultati_moj2_reg.logtr", ios::out);

        m_LogFile << "% Rezultati eksperimenta s mojim regulatorom za slijedenje trajektorije \n";
        m_LogFile << "% Podaci u stupcima imaju poredak: \n";
        m_LogFile << "% Vrijeme | Trajektorija | Mjerenja | Pogreska | Reference \n";
        m_LogFile << "% Detaljnije: \n";
        m_LogFile << "% t xTraj yTraj angTraj vTraj wTraj aTraj xm ym angm vm wm e1 e2 e3 vRef wRef\n";

        m_LogFile << fixed;   // Tak da svaki float broj zauzima isto mjesta
        m_LogFile << showpos; // Tak da pozitivni brojevi imaju +

        // cout.precision(6); // broj znacajnih znamenki (default je 6)
        // cout.width (10); // koliko znamenka zauzima ukupno mjesta (ostatak se nadopunjava razmacima), izgleda da ovo ne dela za float

        // Defaultne vrijednosti parametara
        m_zeta = (T)0.7; // Faktor prigusenja modelske prijenosne funkcije
        m_b = (T)60;   // Dodatni faktor za podesavanje
    }

    ~CTrajTrackingRegulator_Moj2() { m_LogFile.close(); }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Funkcija racuna nove referentne brzine za robota. Poziva se za svaki novi vremenski uzorak.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool GetRefValues(typename CMobileRobot::SRobotState* pState,   // (in), u ovoj strukturi nalazi se trenutno stanje robota
                              typename CMobileRobot::SRobotState* pRefState // (out), u ovu strukturu postavlja se referentna lin. i kutna
                                                                            // brzina robota. Ostali clanovi strukture se ignoriraju
    )
    {
        assert(pState && pRefState);

        if(!IsValid())
        {
            assert(0);
            return false;
        }

        T Time = pState->CurTime; // trenutno vrijeme

        // Uzimamo referentnu tocku iz trajektorije (ref. tocka je funkcija vremena)
        C2DTrajectoryBase::S2DTrajPoint RefPoint;
        RefPoint.Time = Time + (T)0.5 * m_SamplingPeriod; // Uzimamo reference s x koraka unaprijed
        if(!m_pTraj->GetPointByTime(&RefPoint))
            return false;

        // Tocka trajektorije
        T xTraj = RefPoint.x;
        T yTraj = RefPoint.y;
        T angTraj = RefPoint.Angle;
        T cTraj = RefPoint.Curv;
        T vTraj = RefPoint.Velocity;
        T wTraj = vTraj * cTraj;
        T aTraj = RefPoint.Accel;

        // Stanje robota
        T xm = pState->x;
        T ym = pState->y;
        T angm = pState->Angle;

        T vm = pState->Velocity;
        T wm = pState->AngVelocity;

        m_LogFile << Time << ' ' << xTraj << ' ' << yTraj << ' ' << angTraj << ' ' << vTraj << ' ' << wTraj << ' ' << aTraj << ' ';
        m_LogFile << xm << ' ' << ym << ' ' << angm << ' ' << vm << ' ' << wm << ' ';

        // Predikcija zbog kasnjenja
        CompensateDelay(pState);

        // Sad uzimamo predvidena stanja robota kao mjerena stanja
        xm = pState->x;
        ym = pState->y;
        angm = pState->Angle;

        vm = pState->Velocity;
        wm = pState->AngVelocity;

        // Trazimo tocku koja je na zeljenoj udaljenosti ispred robota
        Geom::CLinePar RobotLine;
        RobotLine.LineFromDirectionAndPoint(angm, xm, ym);

        // Proracun izlaza regulatora (tj. ref lin. i kutne brzine robota)
        T e1 = cos(angm) * (RefPoint.x - xm) + sin(angm) * (RefPoint.y - ym);
        T e2 = -sin(angm) * (RefPoint.x - xm) + cos(angm) * (RefPoint.y - ym);

        T e3 = CAngleRange::Range_MinusPI_PI(RefPoint.Angle - angm);

        // Zbroji pogreske - to je SSE (sum of squared errors)
        m_SSE1 += e1 * e1;
        m_SSE2 += e2 * e2;
        m_SSE3 += e3 * e3;

        // Koeficijenti za algoritam
        T k1 = 2 * m_zeta * sqrt(pow(RefPoint.GetAngVelocity(), 2) + m_b * pow(RefPoint.Velocity, 2));
        T k3 = k1;
        T k2 = m_b;

        // Vektor ub (feedback vektor);
        T ub1 = k1 * e1;
        T ub2;
        if(e3 > (T)0.001)
            ub2 = k2 * RefPoint.Velocity * sin(e3) / e3 * e2 + k3 * e3;
        else
            // Ispustamo sin(e3)/e3 jer je za mali e3 to jednako 1 (bez ovoga imali bi moguce dijeljenje s nulom kad je e3=0).
            // Za ovaj slucaj je nelinearni regulator zapravo skroz jednak linearnom, jer je jedina razlika medu njima zapravo taj
            // sin(e3)/e3.
            ub2 = k2 * RefPoint.Velocity * e2 + k3 * e3;

        // Zbroji feedforward i feedback da bismo dobili upravljacke signale
        T vRef = RefPoint.Velocity * cos(e3) + ub1; // ref. linearna brzina [m/s]
        T wRef = RefPoint.GetAngVelocity() + ub2;   // ref. kutna brzina [rad/s]

        // Napuni strukturu s ref. stanjem
        pRefState->Velocity = vRef;
        pRefState->AngVelocity = wRef;
        pRefState->x = xTraj;
        pRefState->y = yTraj;
        pRefState->Angle = angTraj;

        // Ogranici ref. vrijednosti
        BoundRefValues(pRefState);

        m_LogFile << e1 << ' ' << e2 << ' ' << e3 << ' ' << ' ' << pRefState->Velocity << ' ' << pRefState->AngVelocity << endl;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Struktura za zadavanje parametara regulatora
    //____________________________________________________________________________________________________________________________________________________________________________________
    struct SParms : public CTrajTrackingController::SParms
    {
        // Parametri regulatora
        T b;
        T zeta;
    };

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Zadavanje parametara regulatora.
    // Ovu funkciju uvijek treba pozvati prije koristenja regulatora.
    //____________________________________________________________________________________________________________________________________________________________________________________
    virtual bool SetParms(CTrajTrackingController::SParms* pBaseParms)
    {
        if(!CTrajTrackingController::SetParms(pBaseParms))
            return false;

        SParms* pParms = static_cast<SParms*>(pBaseParms);

        m_b = pParms->b;
        m_zeta = pParms->zeta;

        if(!IsValidParms())
        {
            assert(0);
            return false;
        }

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera stanja klase
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValid() const
    {
        if(!CTrajTrackingController::IsValid())
            return false;

        if(!IsValidParms())
            return false;

        return true;
    }

    //____________________________________________________________________________________________________________________________________________________________________________________
    //
    // Provjera dal su dobro postavljeni parametri
    //____________________________________________________________________________________________________________________________________________________________________________________
    bool IsValidParms() const
    {
        if(m_zeta < 0)
            return false;

        if(m_b < 0)
            return false;

        return true;
    }

private:
    // Parametri regulatora

    T m_zeta; // Faktor prigusenja modelske prijenosne funkcije
    T m_b;    // Dodatni faktor za podesavanje
};

/*
// Primjer koristenja regulatora

void main()
{
    CTrajTrackingRegulator_Nonlinear Regulator;
    CTrajTrackingRegulator_Nonlinear::SParms Parms;
    
    //parametri ako se ne postave ostaju defaultni koji su definirani u TrajTrackingController.h
    Parms.pTraj = nullptr;
    Parms.SamplingPeriod = (1/80);
    Parms.b = 60;
    Parms.zeta = 0.7;
    Parms.vMax = 2;
    Parms.wMax = 2/(0.067/2);

    Parms.LinAccelMax = 1;
    Parms.AngAccelMax = 30;
    Parms.nSamplesDelay = 0;



    if(!Regulator.SetParms(&Parms))
        return;

    for(int i=0; i<2400; i++)
    {
        CMobileRobot::SRobotState MeasuredState;
        CMobileRobot::SRobotState RefState;

        MeasuredState.x = ...;
        MeasuredState.y = ...;
        ...

        Regulator.GetRefValues(&MeasuredState, &RefState);

        // Posalji reference robotu
        float vRef = RefState.Velocity;
        float wRef = RefState.AngVelocity;

        ...
    }
}

*/

#endif
