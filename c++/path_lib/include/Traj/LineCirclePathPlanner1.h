#ifndef LINECIRCLEPATHPLANNER1_H
#define LINECIRCLEPATHPLANNER1_H

#include "PathPlanner.h"
//#include "Log/Log.h"
#include "Math/AngleRange.h"
//#include "Obstacle.h"

#include <limits>
#include <vector>

extern CObstacle Obstacle; // TUDU - ovo je privremeno radi nogometa

#ifdef max
#undef max
#endif

//------------------------------------------------------------------------------------------------------------------------
// Path Planner koji planira putanju jednostavno kao kombinaciju krugova i pravaca (to je bad zbog inercije).
// Trajektorija je odredena pocetnom tockom i smjerom, te konacnom tockom i smjerom.
//
// Opcenito, uvijek je moguce pronaci stazu oblika kruzni luk - linija - kruzni luk koji
// ce povezati bilo koje dvije tocke s proizvoljnim smjerovima. Kod ove metode zagarantirano
// je da radijus zakretanja nikad nece biti manji od zadanog, medutim to moze rezultirati
// "sirokim" obilazenjima ako su dvije tocke blizu. Da bi se izbjegla ta obilazenja, u
// ovoj izvedbi dopusteno je smanjenje radijusa ako su tocke toliko blizu da bi to
// rezultiralo obilazenjem.
//
// Osim toga dodana je mogucnost da se staza zavrsi pravcem zadane duljine kako bi
// se dobilo bolje pozicioniranje u ciljnoj tocki.
//------------------------------------------------------------------------------------------------------------------------
template<typename T = float>
class CLineCirclePathPlanner1 : public CPathPlannerBase<T>
{
public:
    virtual CTrajPathProfile* PlanPath(typename CMobileRobot::SRobotState* pBeginState, typename CMobileRobot::SRobotState* pEndState,
                                       typename CMobileRobot* pRobot)
    {
        // LOGSection(CLineCirclePathPlanner);

        assert(pBeginState && pEndState);

        // Iz struktura za pocetno i konacno stanje jedino se u obzir uzimaju x, y i Angle

        m_Path.RemoveAllTrajs();

        T x1 = pBeginState->x, y1 = pBeginState->y, x2 = pEndState->x, y2 = pEndState->y;

        T BeginRobotAngle = pBeginState->Angle, EndRobotAngle = pEndState->Angle;

        int numTries = (int)m_Parms.PossibleRadii.size() * (int)m_Parms.PossibleEndLineLengths.size();

        for(int iTry = 0; iTry < numTries; iTry++)
        {
            T DefaultRadius;
            T EndLineLength;

            int iRadius = iTry % (int)m_Parms.PossibleRadii.size();
            int iEndLength = iTry / (int)m_Parms.PossibleRadii.size();

            DefaultRadius = m_Parms.PossibleRadii[iRadius];
            EndLineLength = m_Parms.PossibleEndLineLengths[iEndLength];

            if(EndLineLength > 0)
            {
                // Koristi se zavrsna linija - izracunaj novi x2, y2
                CLinePathProfile::SParms LineParms;
                LineParms.BeginDist = 0;
                LineParms.DeltaDist = EndLineLength;
                x2 -= EndLineLength * (T)cos(EndRobotAngle);
                y2 -= EndLineLength * (T)sin(EndRobotAngle);
                LineParms.x0 = x2;
                LineParms.y0 = y2;
                LineParms.LineAngle = EndRobotAngle;
                if(!m_EndLine.SetParms(&LineParms))
                    return nullptr;

                // iLOG(200, x2);
                // iLOG(201, y2);
                // iLOG(202, EndRobotAngle);
                // iLOG(203, EndLineLength);
            }

            // LOG(x1);
            // LOG(y1);
            // LOG(x2);
            // LOG(y2);

            // Pocetna dva kruga postavljam lijevo i desno od tangente na gibanje u pocetnoj tocki,
            // a zavrsna dva kruga postavljam lijevo i desno od tangente na gibanje u ciljnoj tocki
            CCircle BeginCircleLeft;
            CCircle EndCircleLeft;

            BeginCircleLeft.SetCircleFromTangent(x1, y1, BeginRobotAngle, DefaultRadius);
            EndCircleLeft.SetCircleFromTangent(x2, y2, EndRobotAngle, DefaultRadius);

            CCircle BeginCircleRight;
            CCircle EndCircleRight;

            BeginCircleRight.SetCircleFromTangent(x1, y1, BeginRobotAngle, -DefaultRadius);
            EndCircleRight.SetCircleFromTangent(x2, y2, EndRobotAngle, -DefaultRadius);

            // Sada trazimo moguce cetiri tangente koje povezuju svaku kombinaciju jednog pocetnog i jednog zavrsnog kruga.
            // Za svaku od pronadjenih putanja trazimo iznos kriterija.
            // Uzima se trajektorija sa najmanjim iznosom kriterija.
            T Kriterions[8];
            bool bValidTraj[8];

            enum ETrajType
            {
                CircleLineCircle,
                CircleCircle,
                Arc,
            };
            ETrajType TrajTypes[8];

            for(int i = 0; i < 8; i++)
            {
                Kriterions[i] = numeric_limits<T>::max();
                bValidTraj[i] = false;
            }

            T BeginRobotAngles[8];
            BeginRobotAngles[0] = BeginRobotAngles[1] = BeginRobotAngles[2] = BeginRobotAngles[3] = BeginRobotAngle;

            // Niz sa tipovima tangenti
            CCircle::ETgTwoCircles TgType[8];
            TgType[0] = CCircle::LL_TG;
            TgType[1] = CCircle::LR_TG;
            TgType[2] = CCircle::RR_TG;
            TgType[3] = CCircle::RL_TG;

            // Niz sa pokazivacima na pocetne krugove koji odgovaraju gornjem nizu sa tipovima tangenti
            CCircle* BeginCircles[8];
            BeginCircles[0] = &BeginCircleLeft;
            BeginCircles[1] = &BeginCircleLeft;
            BeginCircles[2] = &BeginCircleRight;
            BeginCircles[3] = &BeginCircleRight;

            // Niz sa pokazivacima na krajnje krugove koji odgovaraju gornjem nizu sa tipovima tangenti
            CCircle* EndCircles[8];
            EndCircles[0] = &EndCircleLeft;
            EndCircles[1] = &EndCircleRight;
            EndCircles[2] = &EndCircleRight;
            EndCircles[3] = &EndCircleLeft;

            CCircle BeginCircleLeftInv;
            CCircle BeginCircleRightInv;

            if(GetAutoInvert())
            {
                // Ako je omugucen autoinvert, ovdje inicijalizujemo varijable za to.
                // To znaci da bumo ispitivali dal je povoljnije da robot dode u ciljnu tocku na rikverc,
                // pa nam to donosi dodatne 4 moguce staze koje treba ispitati.

                BeginCircleLeftInv = BeginCircleRight;
                BeginCircleRightInv = BeginCircleLeft;

                TgType[4] = TgType[0];
                TgType[5] = TgType[1];
                TgType[6] = TgType[2];
                TgType[7] = TgType[3];

                BeginCircles[4] = &BeginCircleLeftInv;
                BeginCircles[5] = &BeginCircleLeftInv;
                BeginCircles[6] = &BeginCircleRightInv;
                BeginCircles[7] = &BeginCircleRightInv;

                EndCircles[4] = &EndCircleLeft;
                EndCircles[5] = &EndCircleRight;
                EndCircles[6] = &EndCircleRight;
                EndCircles[7] = &EndCircleLeft;

                BeginRobotAngles[4] = BeginRobotAngles[5] = BeginRobotAngles[6] = BeginRobotAngles[7] = BeginRobotAngle + PI;
            }

            int nCases;
            if(GetAutoInvert())
                nCases = 8;
            else
                nCases = 4;

            // Ispitujem sve staze (dal su dopustene s obzirom na prepreke i kolika je duljina pojedine staze)

            for(int i = 0; i < nCases; i++)
            {
                CCircleArc<T> BeginArc;
                CCircleArc<T> EndArc;

                C2DPoint LineBeginPoint; // Pocetna tocka linije koja spaja arcove
                C2DPoint LineEndPoint;   // Krajnja tocka linije

                if(GetTgToTwoCircles(BeginCircles[i], EndCircles[i], TgType[i], &BeginArc, &EndArc, &LineBeginPoint, &LineEndPoint,
                                     BeginRobotAngles[i], EndRobotAngle))
                {
                    CBoundLineImp Line;
                    Line.LineFromPoints(&LineBeginPoint, &LineEndPoint);

                    // Provjeri prepreke
                    if(Obstacle.CheckCollision(BeginArc) || Obstacle.CheckCollision(EndArc) || Obstacle.CheckCollision(Line))
                        bValidTraj[i] = false;
                    else
                        bValidTraj[i] = true;

                    // Nadi iznos kriterija

                    // Duljina linije koja povezuje arcove (tangenta)
                    T LineLen = LineBeginPoint.GetDistanceToPoint(&LineEndPoint);

                    Kriterions[i] = BeginArc.GetArcLength() + LineLen + EndArc.GetArcLength();

                    TrajTypes[i] = CircleLineCircle;
                }
                else
                {
                    // if(TgType[i]==CCircle::LR_TG || TgType[i]==CCircle::RL_TG)

                    // Ako se radi o unutarnjim tangentama i funkcija GetTgToTwoCircles
                    // je vratila false, najvjerojatnije se radi o tome da se krugovi djelomicno
                    // preklapaju i ne postoje unutarnje tangente.
                    // Stoga planiramo samo jedan krajnji luk i zanemarujemo pocetnu orijentaciju robota
                    if(GetArcPath(x1, y1, x2, y2, EndRobotAngle, BeginArc))
                    {
                        // Provjeri prepreke
                        if(Obstacle.CheckCollision(BeginArc))
                            bValidTraj[i] = false;
                        else
                            bValidTraj[i] = true;

                        //??
                        // Duljinu normaliziramo kao da krugovi imaju defaultni radijus
                        // kako bi dobili ravnopravan kriterij
                        // Kriterions[i] = abs(Arc.GetDeltaAngle()) * DefaultRadius;

                        Kriterions[i] = BeginArc.GetArcLength();

                        TrajTypes[i] = Arc;
                    }
                }
            }

            // Pronadji trajektoriju s najnizim kriterijem
            int iChosenTraj = -1;
            T MinKriterion = FLT_MAX;
            for(int i = 0; i < nCases; i++)
            {
                if(bValidTraj[i] && Kriterions[i] < MinKriterion)
                {
                    iChosenTraj = i;
                    MinKriterion = Kriterions[i];
                }
            }

            // LOG(MinKriterion); // Duljina odabrane trajektorije

            // Ponovo izracunaj tangentu i arcove za optimalni slucaj
            if(iChosenTraj != -1)
            {
                CCircleArc<T> BeginArc;
                CCircleArc<T> EndArc;

                C2DPoint LineBeginPoint; // Pocetna tocka linije koja spaja arcove
                C2DPoint LineEndPoint;   // Krajnja tocka linije

                if(TrajTypes[iChosenTraj] == CircleLineCircle)
                {
                    if(GetTgToTwoCircles(BeginCircles[iChosenTraj], EndCircles[iChosenTraj], TgType[iChosenTraj], &BeginArc, &EndArc,
                                         &LineBeginPoint, &LineEndPoint, BeginRobotAngles[iChosenTraj], EndRobotAngle))
                    {
                        // Konacno postavi trajektoriju:

                        // Pocetni kruzni luk
                        CArcPathProfile::SParms0 CircParms;
                        CircParms.BeginDist = 0;
                        CircParms.DeltaDist = BeginArc.GetArcLength();
                        CircParms.xCenter = BeginArc.GetXc();
                        CircParms.yCenter = BeginArc.GetYc();
                        CircParms.Radius = BeginArc.GetRadius();
                        CircParms.ArcBeginAngle = BeginArc.GetBeginAngle();
                        CircParms.bPositiveDirection = (BeginArc.GetDeltaAngle() >= 0 ? true : false);

                        if(!m_BeginArc.SetParms(&CircParms))
                            return nullptr;

                        // Zavrsni kruzni luk
                        CircParms.BeginDist = 0;
                        CircParms.DeltaDist = EndArc.GetArcLength();
                        CircParms.xCenter = EndArc.GetXc();
                        CircParms.yCenter = EndArc.GetYc();
                        CircParms.ArcBeginAngle = EndArc.GetBeginAngle();
                        CircParms.bPositiveDirection = (EndArc.GetDeltaAngle() >= 0 ? true : false);
                        CircParms.Radius = EndArc.GetRadius();

                        // iLOG(210, EndArc.GetXc());
                        // iLOG(211, EndArc.GetYc());
                        // iLOG(212, EndArc.GetBeginAngle());
                        // iLOG(213, EndArc.GetDeltaAngle());
                        // iLOG(214, EndArc.GetRadius());

                        if(!m_EndArc.SetParms(&CircParms))
                            return nullptr;

                        // Linija koja povezuje te kruzne lukove
                        CLinePathProfile::SParms LineParms;
                        LineParms.BeginDist = 0;
                        LineParms.DeltaDist = LineBeginPoint.GetDistanceToPoint(&LineEndPoint);
                        ;
                        LineParms.x0 = LineBeginPoint.x;
                        LineParms.y0 = LineBeginPoint.y;
                        LineParms.LineAngle = (T)atan2(LineEndPoint.y - LineBeginPoint.y, LineEndPoint.x - LineBeginPoint.x);

                        // iLOG(215, LineParms.x0);
                        // iLOG(216, LineParms.y0);
                        // iLOG(217, LineParms.LineAngle);
                        // iLOG(218, LineParms.DeltaDist);

                        if(!m_Line.SetParms(&LineParms))
                            return nullptr;

                        // Dodaj konstruirane segmente u trajektoriju
                        if(!m_Path.AddTrajComponent(&m_BeginArc))
                            return nullptr;

                        if(!m_Path.AddTrajComponent(&m_Line))
                            return nullptr;

                        if(!m_Path.AddTrajComponent(&m_EndArc))
                            return nullptr;

                        if(EndLineLength > 0)
                        {
                            if(!m_Path.AddTrajComponent(&m_EndLine))
                                return nullptr;
                        }
                    }
                }
                else if(TrajTypes[iChosenTraj] == CircleCircle)
                {
                    if(GetTwoArcsPath(x1, y1, BeginRobotAngles[iChosenTraj], x2, y2, EndRobotAngle, TgType[iChosenTraj], BeginArc, EndArc))
                    {
                        // Konacno postavi trajektoriju:

                        // Pocetni kruzni luk
                        CArcPathProfile::SParms0 CircParms;
                        CircParms.BeginDist = 0;
                        CircParms.DeltaDist = BeginArc.GetArcLength();
                        CircParms.xCenter = BeginArc.GetXc();
                        CircParms.yCenter = BeginArc.GetYc();
                        CircParms.Radius = BeginArc.GetRadius();
                        CircParms.ArcBeginAngle = BeginArc.GetBeginAngle();
                        CircParms.bPositiveDirection = (BeginArc.GetDeltaAngle() >= 0 ? true : false);

                        // iLOG(205, BeginArc.GetXc());
                        // iLOG(206, BeginArc.GetYc());
                        // iLOG(207, BeginArc.GetBeginAngle());
                        // iLOG(208, BeginArc.GetDeltaAngle());
                        // iLOG(209, BeginArc.GetRadius());

                        if(!m_BeginArc.SetParms(&CircParms))
                            return nullptr;

                        // Zavrsni kruzni luk
                        CircParms.BeginDist = 0;
                        CircParms.DeltaDist = EndArc.GetArcLength();
                        CircParms.xCenter = EndArc.GetXc();
                        CircParms.yCenter = EndArc.GetYc();
                        CircParms.Radius = EndArc.GetRadius();
                        CircParms.ArcBeginAngle = EndArc.GetBeginAngle();
                        CircParms.bPositiveDirection = (EndArc.GetDeltaAngle() >= 0 ? true : false);

                        // iLOG(210, EndArc.GetXc());
                        // iLOG(211, EndArc.GetYc());
                        // iLOG(212, EndArc.GetBeginAngle());
                        // iLOG(213, EndArc.GetDeltaAngle());
                        // iLOG(214, EndArc.GetRadius());

                        if(!m_EndArc.SetParms(&CircParms))
                            return nullptr;

                        // Dodaj konstruirane segmente u trajektoriju
                        if(!m_Path.AddTrajComponent(&m_BeginArc))
                            return nullptr;

                        if(!m_Path.AddTrajComponent(&m_EndArc))
                            return nullptr;

                        if(EndLineLength > 0)
                        {
                            if(!m_Path.AddTrajComponent(&m_EndLine))
                                return nullptr;
                        }
                    }
                }
                else if(TrajTypes[iChosenTraj] == Arc)
                {
                    if(GetArcPath(x1, y1, x2, y2, EndRobotAngle, BeginArc))
                    {
                        // Pocetni kruzni luk
                        CArcPathProfile::SParms0 CircParms;
                        CircParms.BeginDist = 0;
                        CircParms.DeltaDist = BeginArc.GetArcLength();
                        CircParms.xCenter = BeginArc.GetXc();
                        CircParms.yCenter = BeginArc.GetYc();
                        CircParms.Radius = BeginArc.GetRadius();
                        CircParms.ArcBeginAngle = BeginArc.GetBeginAngle();
                        CircParms.bPositiveDirection = (BeginArc.GetDeltaAngle() >= 0 ? true : false);

                        if(!m_BeginArc.SetParms(&CircParms))
                            return nullptr;

                        // Dodaj konstruirane segmente u trajektoriju
                        if(!m_Path.AddTrajComponent(&m_BeginArc))
                            return nullptr;

                        if(EndLineLength > 0)
                        {
                            if(!m_Path.AddTrajComponent(&m_EndLine))
                                return nullptr;
                        }
                    }
                }
                else
                {
                    assert(0);
                }

                // Zapamti dal je invertirana staza
                if(iChosenTraj > 3)
                    m_bInverted = true;
                else
                    m_bInverted = false;

                assert(IsValid());

                return &m_Path;
            }
        }

        return nullptr;
    }

protected:
    // Svodi na [0,2PI>
    static inline T NormAngle_Zero_2PI(T Angle)
    {
        while(Angle >= 2 * PI)
            Angle -= 2 * PI;
        while(Angle < 0)
            Angle += 2 * PI;
        return Angle;
    }

    // Svodi na <-2PI,0]
    static inline T NormAngle_Minus2PI_Zero(T Angle)
    {
        while(Angle > 0)
            Angle -= 2 * PI;
        while(Angle <= -2 * PI)
            Angle += 2 * PI;
        return Angle;
    }

    bool GetTgToTwoCircles(CCircle* pBeginCircle, CCircle* pEndCircle, typename CCircle::ETgTwoCircles TgType, CCircleArc<T>* pBeginArc,
                           CCircleArc<T>* pEndArc, C2DPoint* pLineBeginPoint, C2DPoint* pLineEndPoint, T BeginRobotAngle, T EndRobotAngle)
    {
        assert(pBeginCircle && pEndCircle && pBeginArc && pEndArc && pLineBeginPoint && pLineEndPoint);

        T TgAngle;

        if(pBeginCircle->GetTangentAngleToTwoCircles(TgType, pEndCircle, &TgAngle))
        {
            // Pocetni arc
            T DeltaAngle;
            if(TgType == CCircle::RR_TG || TgType == CCircle::RL_TG)
            {
                DeltaAngle = NormAngle_Minus2PI_Zero(TgAngle - BeginRobotAngle); // Negativan smjer

                pBeginArc->SetArc(pBeginCircle, BeginRobotAngle + PI / 2, DeltaAngle);
            }
            else
            {
                DeltaAngle = NormAngle_Zero_2PI(TgAngle - BeginRobotAngle); // Pozitivan smjer

                pBeginArc->SetArc(pBeginCircle, BeginRobotAngle - PI / 2, DeltaAngle);
            }

            // Pocetna tocka linije
            pBeginArc->GetEndPoint(pLineBeginPoint);

            // Zavrsni arc
            if(TgType == CCircle::LR_TG || TgType == CCircle::RR_TG)
            {
                DeltaAngle = NormAngle_Minus2PI_Zero(EndRobotAngle - TgAngle); // Negativan smjer

                pEndArc->SetArc(pEndCircle,
                                (EndRobotAngle + PI / 2) - DeltaAngle, // Pocetni kut = konacni - promjena kuta
                                DeltaAngle);
            }
            else
            {
                DeltaAngle = NormAngle_Zero_2PI(EndRobotAngle - TgAngle); // Pozitivan smjer

                pEndArc->SetArc(pEndCircle,
                                (EndRobotAngle - PI / 2) - DeltaAngle, // Pocetni kut = konacni - promjena kuta
                                DeltaAngle);
            }

            // Krajnja tocka linije
            pEndArc->GetBeginPoint(pLineEndPoint);
        }
        else
        {
            return false;
        }

        return true;
    }

    //_______________________________________________________________________________________________________________________________________________________
    //
    // Planira trajektoriju od jednog kruznog luka koji ide iz pocetne tocke (ali zanemaruje pocetnu orijentaciju),
    // do ciljne tocke u kojoj mu je smjer tangente jednak ciljnom smjeru.
    // Dakle uzima u obzir ciljnu orijentaciju, ali ne i pocetnu.
    // Ovo je korisno kad je robot vec blizu cilju.
    //_______________________________________________________________________________________________________________________________________________________
    bool GetArcPath(T x1, T y1,        // pocetna tocka
                    T x2, T y2,        // ciljna tocka
                    T EndRobotAngle,   // ciljna orijentacija
                    CCircleArc<T>& Arc // (out)
    )
    {
        bool bPositiveDirection; // dal je smjer obilazenja po krugu pozitivan ili negativan

        // Treba naci jednadzbu kruznice sa zadanom tangentom i jednom tockom.
        if(!Arc.CircleFromTangentAndPoint(x2, y2, EndRobotAngle, x1, y1, &bPositiveDirection))
            return false;

        // Jos izracunamo pocetni i konacni kut luka
        T ArcBeginAngle = Arc.GetDirectionToPoint(x1, y1);
        T ArcEndAngle = Arc.GetDirectionToPoint(x2, y2);

        // ... dal je smjer obilazenja pozitivan il negativan
        bool bPositiveDirection2 = (CAngleRange<T>::Range_MinusPI_PI(ArcEndAngle - EndRobotAngle) < 0 ? true : false);

        if(bPositiveDirection)
        {
            // Svodi krajnji kut na interval [pocetni,pocetni+2pi>
            ArcEndAngle = CAngleRange<T>::Range_Ang_2PI(ArcEndAngle, ArcBeginAngle);
        }
        else
        {
            // Svodi krajnji kut na interval <pocetni-2pi,pocetni>
            ArcEndAngle = CAngleRange<T>::Range_Minus2PI_Ang(ArcEndAngle, ArcBeginAngle);
        }

        T ArcDeltaAngle = ArcEndAngle - ArcBeginAngle;
        Arc.SetArc(ArcBeginAngle, ArcDeltaAngle);

        assert(Arc.IsOk());

        return true;
    }

    //_______________________________________________________________________________________________________________________________________________________
    //
    // Planira trajektoriju od dva kruzna luka jednakih radijusa tako da se lukovi spajaju u zajednickoj tangenti.
    // Ovo je nepovoljno jer imamo prijelaz iz lijevog kruga u desni krug, sto robot tesko moze savladati
    //_______________________________________________________________________________________________________________________________________________________
    bool GetTwoArcsPath(T x1, T y1,                              // pocetna tocka
                        T BeginRobotAngle,                       // pocetna orijentacija
                        T x2, T y2,                              // ciljna tocka
                        T EndRobotAngle,                         // ciljna orijentacija
                        typename CCircle::ETgTwoCircles TgTypes, // moze biti RL_TG ili LR_TG (npr. RL_TG znaci da je prvi krug desno od
                                                                 // tangente u poc. tocki, a drugi desno)
                        CCircleArc<T>& BeginArc,                 // (OUT)
                        CCircleArc<T>& EndArc                    // (OUT)
    )
    {
        // Sredista krugova
        T xc1, yc1, xc2, yc2;

        // Radijus
        T r;

        if(!CCircleBase<T>::GetCirclesFromTwoTangents(x1, y1, BeginRobotAngle, x2, y2, EndRobotAngle, TgTypes, xc1, yc1, xc2, yc2, r))
            return false;

        // Izracunaj kut pravca od sredista prvog do sredista drugog kruga
        T Ang_c1c2 = (T)atan2(yc2 - yc1, xc2 - xc1);

        // Slozi arcove

        BeginArc.SetCircle(xc1, yc1, r);
        EndArc.SetCircle(xc2, yc2, r);

        if(TgTypes == CCircle::RL_TG)
        {
            T Beg_ArcBeginAngle = BeginRobotAngle + PI / 2;
            T Beg_ArcDeltaAngle = NormAngle_Minus2PI_Zero(Ang_c1c2 - Beg_ArcBeginAngle); // Negativan smjer

            BeginArc.SetArc(Beg_ArcBeginAngle, Beg_ArcDeltaAngle);

            T End_ArcBeginAngle = Ang_c1c2 + PI;
            T End_ArcDeltaAngle = NormAngle_Zero_2PI(EndRobotAngle - PI / 2 - End_ArcBeginAngle); // Pozitivan smjer

            EndArc.SetArc(End_ArcBeginAngle, End_ArcDeltaAngle);
        }
        else if(TgTypes == CCircle::LR_TG)
        {
            T Beg_ArcBeginAngle = BeginRobotAngle - PI / 2;
            T Beg_ArcDeltaAngle = NormAngle_Zero_2PI(Ang_c1c2 - Beg_ArcBeginAngle); // Pozitivan smjer

            BeginArc.SetArc(Beg_ArcBeginAngle, Beg_ArcDeltaAngle);

            T End_ArcBeginAngle = Ang_c1c2 + PI;
            T End_ArcDeltaAngle = NormAngle_Minus2PI_Zero(EndRobotAngle + PI / 2 - End_ArcBeginAngle); // Negativan smjer

            EndArc.SetArc(End_ArcBeginAngle, End_ArcDeltaAngle);
        }
        else
        {
            assert(0);
            return false; // Ne moze se konstruirati trajektorija za druge tipove tangenti
        }

        return true;
    }

public:
    // Parametri - zadaju se preko strukture
    struct SParms
    {
        SParms()
        {
            PossibleRadii.push_back((T)0.2);
            PossibleRadii.push_back((T)0.15);
            PossibleRadii.push_back((T)0.1);
            PossibleRadii.push_back((T)0.05);
            PossibleRadii.push_back((T)0.25);

            PossibleEndLineLengths.push_back((T)0.05);
            PossibleEndLineLengths.push_back((T)0.0);
        }

        // Ovo je dodano specijalno radi nogometa tako da pokusava sa vise razlitih parametara.
        std::vector<T> PossibleRadii;          ///< Radiuses to try. (0,inf)
        std::vector<T> PossibleEndLineLengths; ///< Moguce zeljene duljine zavrsne linije [0,inf)
    };

    bool SetParms(SParms* pParms)
    {
        m_Parms = *pParms;

        if(!IsValidParms())
            return false;

        return true;
    }

    bool IsValid() const
    {
        if(!IsValidParms())
            return false;

        // Morao bi biti postavljen barem pocetni luk, zavrsni se ne koristi u nekim slucajevima
        if(!(m_BeginArc.IsValid() && m_Path.IsValid()))
            return false;

        return true;
    }

    bool IsValidParms() const { return true; }

    // Podaci
protected:
    // T m_DefaultRadius;

    // Dijelovi trajektorije - ovisno o slucaju koriste se svi ili samo neki od njih
    CArcPathProfile m_BeginArc, m_EndArc; // Pocetni i konacni luk
    CLinePathProfile m_Line;              // Linija izmedu njih

    // Zavrsna linija za bolje pozicioniranje
    CLinePathProfile m_EndLine;

    CTrajCompositePathProfile m_Path;

    SParms m_Parms;
};

#endif