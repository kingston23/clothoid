#ifndef _TimeProfile_h_
#define _TimeProfile_h_

#include "2DTrajectoryBase.h"

/********************************************************************************************************
    Baza za vremenski profil trajektorije - specijalni slucaj trajektorije.
    Koristiti u slucajevima kad je potrebno odvojeno planirati vremenski i prostorni profil
    trajektorije. U tom slucaju vremenski profil definira pomak, brzinu i akceleraciju u
    pojedinom trenutku, a prostorni koordinate u prostoru, kut i zakrivljenost
*********************************************************************************************************/
//template<class T>
class CTrajTimeProfile : public C2DTrajectoryBase
{
    // Prazna klasa - to je zapravo samo preimenovana bazna klasa, a podrzava
    // sve funkcije koje i bazna klasa
};

#endif // _TimeProfile_h_