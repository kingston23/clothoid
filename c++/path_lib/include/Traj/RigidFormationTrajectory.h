#ifndef _RigidFormationTrajectory_h_
#define _RigidFormationTrajectory_h_

#include "Math/AngleRange.h"

#include <assert.h>
#include <stdio.h>
#include <vector>

//=================================================================================================================================================================
//
// Klasa koja racuna trajektoriju za robota u rigidnoj formaciji.
// U rigidnoj formaciji se racuna kao da postoji neki (virtualni) robot lider, a ostali roboti pratitelji "pricvrsceni" za lidera na fiksnoj
// udaljenosti i kutu. Medutim, pratiteljeva orijentacija ne mora biti jednaka orijentaciji lidera, a racuna se tako da svi roboti u
// formaciji imaju istu kutnu brzinu.
//
//=================================================================================================================================================================
template<typename T>
class CRigidFormationTrajectory : public C2DTrajectoryBase
{
public:
    CRigidFormationTrajectory()
    {
        m_pLeaderTrajectory = nullptr;

        m_DistanceToLeader = 0;
        m_AngleToLeader = 0;
    }

    // virtual ~CRigidFormationTrajectory(){};

    virtual bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!m_pLeaderTrajectory)
            return false;

        S2DTrajPoint LeaderPoint; // Trenutna trajektorija lidera
        LeaderPoint.Time = pTrPoint->Time;

        if(!m_pLeaderTrajectory->GetPointByTime(&LeaderPoint))
            return false;

        // Indeks r odnosi se na robota pratitelja
        T xr, yr, angr, kr, vr, wr;

        if(!GeometricTransform(LeaderPoint.x, LeaderPoint.y, LeaderPoint.Angle, LeaderPoint.Curv, LeaderPoint.Velocity,
                               LeaderPoint.GetAngVelocity(), xr, yr, angr, kr, vr))
            return false;

        // Posto kutnu brzinu ne znam izracunati analiticki, izracunat cu ju prljavim trikom - diferenciranjem kuta :)
        // TUDU: naci analiticki izraz
        T DerivativePeriod = 0.001f;
        if(pTrPoint->Time - DerivativePeriod < GetBeginTime())
        {
            // Uzmi da je kutna brzina pratitelja jednaka kutnoj brzini lidera
            wr = LeaderPoint.GetAngVelocity();
        }
        else if(LeaderPoint.GetAngVelocity() == 0)
        {
            wr = 0;
        }
        else
        {
            LeaderPoint.Time = pTrPoint->Time - DerivativePeriod;
            if(!m_pLeaderTrajectory->GetPointByTime(&LeaderPoint))
                return false;

            T xr_p, yr_p, angr_p, kr_p, vr_p; // Od ovog treba nam samo kut
            if(!GeometricTransform(LeaderPoint.x, LeaderPoint.y, LeaderPoint.Angle, LeaderPoint.Curv, LeaderPoint.Velocity,
                                   LeaderPoint.GetAngVelocity(), xr_p, yr_p, angr_p, kr_p, vr_p))
                return false;

            wr = (angr - angr_p) / DerivativePeriod;
        }

        // Pakiraj vrijednosti u strukturu
        pTrPoint->x = xr;
        pTrPoint->y = yr;
        pTrPoint->Angle = angr;
        pTrPoint->Curv = kr; // vjerojatno ni ovo ne valja
        pTrPoint->Velocity = vr;
        pTrPoint->Accel = 0; // ?
        pTrPoint->DCurv = 0; // ?
        pTrPoint->bAngVelocityFromCurvature = false;
        pTrPoint->AngVelocity = wr;

        // TUDU: Ovo nije sasvim tocno buduci da pratitelji prevaljuju razlicit put u odnosu na lidera, ali nemam pojma kak izracunati prave
        // vrijednosti
        pTrPoint->Disp = LeaderPoint.Disp;
        pTrPoint->Dist = LeaderPoint.Dist;

        return true;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Geometrijska transformacija da se iz stanja lidera dobije stanje pratitelja (ali bez kutne brzine)
    //_________________________________________________________________________________________________________________________________________________________
    bool GeometricTransform(T xl, T yl, T angl, T kl, T vl, T wl, T& xr, T& yr, T& angr, T& kr, T& vr) const
    {
        xr = xl + m_DistanceToLeader * cos(angl + m_AngleToLeader);
        yr = yl + m_DistanceToLeader * sin(angl + m_AngleToLeader);

        if(vl == 0 && wl == 0)
        {
            // Lider stoji

            angr = angl;
            kr = kl;
            vr = 0;
        }
        else if(vl != 0 && wl == 0)
        {
            // Lider ide ravno

            angr = angl;
            kr = 0;
            vr = vl;
        }
        else if(vl != 0 && vl != 0)
        {
            // Lider se giba istodobno skrece

            // ... proracunavam centar kruznice po kojoj se krece lider
            T xc, yc;

            assert(kl != 0);
            T rl = fabs(1 / kl); // radijus kruznice po kojoj se krece lider

            if(kl > 0)
            {
                xc = xl + rl * cos(angl + PI / 2);
                yc = yl + rl * sin(angl + PI / 2);
            }
            else
            {
                xc = xl + rl * cos(angl - PI / 2);
                yc = yl + rl * sin(angl - PI / 2);
            }

            T dcx = xr - xc;
            T dcy = yr - yc;
            angr = atan2(dcy, dcx) + PI / 2; // ovo je kut pratitelja - moze biti fulan za pi. Zato ga svodimo na kut lidera

            angr = CAngleRange::Range_MinusPI2_Ang_PI2(angr, angl);

            kr = 1 / sqrt(dcx * dcx + dcy * dcy);
            if(kl < 0)
                kr = -kr;

            vr = wl / kr;
        }
        else if(vl == 0 && wl != 0)
        {
            assert(0); // tudu: implementirati
        }
        else
        {
            assert(0); // kaj ima jos slucaja?
        }

        // Tudu: racunanje akceleracije?

        return true;
    }

    virtual bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        // tudu: implementirati
        assert(0);

        return false;
    };

    virtual bool GetPointByDist(S2DTrajPoint* pTrPoint) const
    {
        // tudu: implementirati
        assert(0);

        return false;
    };

    // Funkcije za manipuliranje ofsetima itd...
    // Glavna svrha je omoguciti nadovezivanje trajektorija jedne na drugu

    virtual T GetBeginTime() const { return m_pLeaderTrajectory->GetBeginTime(); };
    virtual T GetDeltaTime() const { return m_pLeaderTrajectory->GetDeltaTime(); }
    virtual void SetBeginTime(T BeginTime) { m_pLeaderTrajectory->SetBeginTime(BeginTime); }
    T GetEndTime() const { return m_pLeaderTrajectory->GetEndTime(); }

    // PAZNJA
    // Pomaci se jednostavno prosljeduju u trajektoriju lidera - sto ne mora biti tocno jer pratitelji ostvaruju razlicit pomak, ali barem
    // daje neke orijentacijske vrijednosti
    virtual T GetBeginDisp() const { return m_pLeaderTrajectory->GetBeginDisp(); }
    virtual T GetDeltaDisp() const { return m_pLeaderTrajectory->GetDeltaDisp(); }
    virtual void SetBeginDisp(T BeginDisp) { m_pLeaderTrajectory->SetBeginDisp(BeginDisp); }
    T GetEndDisp() const { return m_pLeaderTrajectory->GetEndDisp(); }

    // PAZNJA
    // Udaljenosti se jednostavno prosljeduju u trajektoriju lidera - sto ne mora biti tocno jer pratitelji ostvaruju razlicite prevaljene
    // putove u odnosu na lidera, ali barem daje neke orijentacijske vrijednosti.
    virtual T GetBeginDist() const { return m_pLeaderTrajectory->GetBeginDist(); }
    virtual T GetDeltaDist() const { return m_pLeaderTrajectory->GetDeltaDist(); };
    virtual void SetBeginDist(T BeginDist) { m_pLeaderTrajectory->SetBeginDist(BeginDist); }
    T GetEndDist() const { return m_pLeaderTrajectory->GetEndDist(); }

    virtual bool IsValid() const
    {
        if(!C2DTrajectoryBase::IsValid())
            return false;

        if(!m_pLeaderTrajectory)
            return false;

        if(m_DistanceToLeader < 0)
            return false;

        return true;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje pokazivaca na trajektoriju lidera.
    //_________________________________________________________________________________________________________________________________________________________
    void SetLeaderTrajectory(C2DTrajectoryBase* pLeaderTrajectory) { m_pLeaderTrajectory = pLeaderTrajectory; }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje polozaja u odnosu na lidera.
    //_________________________________________________________________________________________________________________________________________________________
    void SetPoseToLeader(T DistanceToLeader, T AngleToLeader)
    {
        assert(m_DistanceToLeader >= 0);

        m_DistanceToLeader = DistanceToLeader;
        m_AngleToLeader = AngleToLeader;
    }

protected:
    C2DTrajectoryBase* m_pLeaderTrajectory; // Trajektorija (virtualnog) robota lidera

    T m_DistanceToLeader; // [>=0] Kolko je udaljenost robota pratitelja do lidera
    T m_AngleToLeader;    // [rad] Kut robota pratitelja prema lideru. Npr. ak je kut nula, pratitelj je uvijek ispred lidera.
};

//=================================================================================================================================================================
//
// Klasa koja racuna sluzi kao kontejner za vise trajektorije svih robota u rigidnoj formaciji.
//
//=================================================================================================================================================================
template<typename T>
class CRigidFormationTrajectoryContainer
{
public:
    CRigidFormationTrajectoryContainer()
    {
        m_pLeaderTrajectory = nullptr;

        m_bAutoDeleteLeaderTrajectory = false;
    }

    ~CRigidFormationTrajectoryContainer()
    {
        ClearFollowers();

        if(m_bAutoDeleteLeaderTrajectory)
            DeleteLeaderTrajectory();
    }

    inline int GetNumFollowers() const { return (int)m_pFollowerTrajectories.size(); }

    C2DTrajectoryBase* GetFollowerTrajectory(int iFollower)
    {
        if(iFollower < 0 || iFollower >= GetNumFollowers())
            return false;

        return m_pFollowerTrajectories[iFollower];
    }

    C2DTrajectoryBase* GetLeaderTrajectory() const { return m_pLeaderTrajectory; }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Postavljanje pokazivaca na trajektoriju lidera.
    // Vraca prijasnju trajektoriju lidera, koju pozivatelj moze izbrisati nakon toga
    //_________________________________________________________________________________________________________________________________________________________
    C2DTrajectoryBase* SetLeaderTrajectory(C2DTrajectoryBase* pLeaderTrajectory)
    {
        C2DTrajectoryBase* pPreviousLeaderTrajectory = m_pLeaderTrajectory;
        m_pLeaderTrajectory = pLeaderTrajectory;

        // Postavi novu trajektoriju za sve pratitelje
        for(int i = 0; i < (int)m_pFollowerTrajectories.size(); i++)
            m_pFollowerTrajectories[i]->SetLeaderTrajectory(pLeaderTrajectory);

        return pPreviousLeaderTrajectory;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Brise trajektoriju lidera
    //_________________________________________________________________________________________________________________________________________________________
    void DeleteLeaderTrajectory()
    {
        delete m_pLeaderTrajectory;
        m_pLeaderTrajectory = nullptr;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Brisanje svih pratitelja
    //_________________________________________________________________________________________________________________________________________________________
    bool ClearFollowers()
    {
        // Izbrisi sve trajektorije pratitelja
        for(int i = 0; i < (int)m_pFollowerTrajectories.size(); i++)
        {
            delete m_pFollowerTrajectories[i];
            m_pFollowerTrajectories[i] = nullptr;
        }

        m_pFollowerTrajectories.clear();

        return true;
    }

    //_________________________________________________________________________________________________________________________________________________________
    //
    // Dodaje novog pratitelja
    //_________________________________________________________________________________________________________________________________________________________
    bool AddFollowerTraj(T DistanceToLeader, T AngleToLeader)
    {
        assert(DistanceToLeader >= 0);

        CRigidFormationTrajectory<T>* pFollowerTraj = new CRigidFormationTrajectory<T>;
        if(!pFollowerTraj)
            return false;

        pFollowerTraj->SetPoseToLeader(DistanceToLeader, AngleToLeader);
        pFollowerTraj->SetLeaderTrajectory(m_pLeaderTrajectory);

        m_pFollowerTrajectories.push_back(pFollowerTraj);

        return true;
    }

    void SetAutoDeleteLeaderTrajectory(bool bDelete) { m_bAutoDeleteLeaderTrajectory = bDelete; }

protected:
    vector<CRigidFormationTrajectory<T>*> m_pFollowerTrajectories; // Trajektorije za robote pratitelje

    C2DTrajectoryBase* m_pLeaderTrajectory; // Trajektorija (virtualnog) robota lidera

    bool m_bAutoDeleteLeaderTrajectory; // dal treba automatski izbrisati trajektoriju lidera kod destrukcije
};

#endif
