#ifndef _UniformTimeProfiles_h_
#define _UniformTimeProfiles_h_

#include "TrajTimeProfile.h"

//=================================================================================================================================================================
//
// Najblesaviji vremenski profil - za stajanje na mjestu :)
//
//=================================================================================================================================================================
class CUniformPositionTimeProfile : public CTrajTimeProfile
{
public:
    CUniformPositionTimeProfile() { m_bParmsSet = false; }

    virtual ETimeProfileType GetTimeProfileType() const { return TP_UniformPosition; }

    // Postavlja Dist, Speed i Accel, ovisno o Disp parametru iz strukture
    // (ne postavlja Time jer u ovom slucaju nije jednoznacno (brzina = 0)).
    // Ostali clanovi strukture se ne modificiraju.
    // Ne vrsi se testiranje granica za disp (osim assert)!
    bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!m_bParmsSet)
            return false;

        // assert(pTrPoint->Disp-GetBeginDisp() >= 0);
        // assert(pTrPoint->Disp-GetBeginDisp() <= GetDeltaDisp());

        pTrPoint->Dist = m_BeginDist;
        pTrPoint->Velocity = 0;
        pTrPoint->Accel = 0;

        return true;
    };

    // Postavlja Disp, Dist, Speed i Accel, ovisno o Time parametru iz strukture
    // Ostali clanovi strukture se ne modificiraju.
    // Ne vrsi se testiranje da li je Time unutar postavljenih granica (osim assert)!
    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!m_bParmsSet)
            return false;

        // assert(pTrPoint->Time-GetBeginTime() > 0);
        // assert(pTrPoint->Time-GetBeginTime() < GetDeltaTime());

        pTrPoint->Disp = m_BeginDisp;
        pTrPoint->Dist = m_BeginDist;
        pTrPoint->Velocity = 0;
        pTrPoint->Accel = 0;

        return true;
    };

    struct SParms
    {
        T BeginTime;
        T DeltaTime;
        T BeginDisp;
        T BeginDist;
    };

    bool SetParms(SParms* pParms)
    {
        m_BeginTime = pParms->BeginTime;
        m_DeltaTime = pParms->DeltaTime;
        m_BeginDisp = pParms->BeginDisp;
        m_BeginDist = pParms->BeginDist;

        assert(m_DeltaTime >= 0);

        m_bParmsSet = true;

        return true;
    }

    void SetBeginDisp(T BeginDisp) { m_BeginDisp = BeginDisp; };
    T GetBeginDisp() const { return m_BeginDisp; }
    T GetDeltaDisp() const { return 0; }

    void SetBeginDist(T BeginDist) { m_BeginDist = BeginDist; };
    T GetBeginDist() const { return m_BeginDist; }
    T GetDeltaDist() const { return 0; }

    void SetBeginTime(T BeginTime) { m_BeginTime = BeginTime; };
    T GetBeginTime() const { return m_BeginTime; }
    T GetDeltaTime() const { return m_DeltaTime; }

    bool IsValid() const
    {
        if(!CTrajTimeProfile::IsValid())
            return false;

        return m_bParmsSet;
    }

private:
    // Parametri
    T m_BeginDisp; // Pocetni pomak
    T m_BeginDist; // Pocetna udalj.
    T m_BeginTime; // Pocetno vrijeme
    T m_DeltaTime; // Ukupno vrijeme

    // Pomocne varijable
    bool m_bParmsSet;
};

//=================================================================================================================================================================
//
// Vremenski profil za gibanje jednolikom brzinom
//
//=================================================================================================================================================================
template<class T>
class CUniformSpeedTimeProfile : public CTrajTimeProfile
{
public:
    CUniformSpeedTimeProfile() { m_bParmsSet = false; }

    virtual ETimeProfileType GetTimeProfileType() const { return TP_UniformSpeed; }

    // Postavlja Time, Dist, Speed i Accel, ovisno o Disp parametru iz strukture;
    // Ostali clanovi strukture se ne modificiraju;
    // Ne vrsi se testiranje da li je Disp unutar granica (osim assert);
    // Vraca Time=m_BeginTime ako je brzina jednaka nuli.
    bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!m_bParmsSet)
            return false;

        T dDisp = pTrPoint->Disp - m_BeginDisp;

        // assert(dDisp >= 0 && dDisp <= GetDeltaDisp());
        assert(m_Speed != 0);

        T dt;
        if(m_Speed != 0)
            dt = dDisp / m_Speed;
        else
            dt = 0;

        pTrPoint->Time = m_BeginTime + dt;
        pTrPoint->Dist = m_BeginDist + m_Speed * dt;
        pTrPoint->Velocity = m_Speed;
        pTrPoint->Accel = 0;

        return true;
    };

    // Postavlja Disp, Dist, Speed i Accel, ovisno o Time parametru iz strukture.
    // Ostali clanovi strukture se ne modificiraju.
    // Ne vrsi se testiranje da li je Time unutar postavljenih granica (osim assert).
    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!m_bParmsSet)
            return false;

        T dt = pTrPoint->Time - m_BeginTime;

        // assert(dt >= 0 && dt <= GetDeltaTime());

        pTrPoint->Disp = m_BeginDisp + abs(m_Speed) * dt;
        pTrPoint->Dist = m_BeginDist + m_Speed * dt;
        pTrPoint->Velocity = m_Speed;
        pTrPoint->Accel = 0;

        return true;
    };

    struct SParms1
    {
        T BeginTime;
        T DeltaTime;
        T BeginDisp;
        T BeginDist;
        T Speed;
    };

    // Zadaje se ukupno vrijeme iz kojeg se racuna ukupni pomak
    bool SetParms(SParms1* pParms)
    {
        m_BeginTime = pParms->BeginTime;
        m_DeltaTime = pParms->DeltaTime;
        m_BeginDisp = pParms->BeginDisp;
        m_BeginDist = pParms->BeginDist;
        m_Speed = pParms->Speed;

        m_DeltaDist = m_Speed * m_DeltaTime;
        m_DeltaDisp = abs(m_DeltaDist);

        m_bParmsSet = true;

        return true;
    }

    struct SParms2
    {
        T BeginTime;
        T BeginDisp;
        T DeltaDisp;
        T BeginDist;
        T Speed;
    };

    // Zadaje se ukupni pomak iz kojeg se onda racuna ukupno vrijeme.
    // U ovom slucaju brzina ne smije biti 0.
    bool SetParms(SParms2* pParms)
    {
        m_bParmsSet = false;

        m_BeginTime = pParms->BeginTime;
        m_BeginDisp = pParms->BeginDisp;
        m_DeltaDisp = pParms->DeltaDisp;
        assert(m_DeltaDisp >= 0);
        m_Speed = pParms->Speed;
        m_BeginDist = pParms->BeginDist;
        m_DeltaDist = (m_Speed >= 0 ? m_DeltaDisp : -m_DeltaDisp);
        if(m_Speed == 0)
        {
            assert(0); // Brzina ne smije biti nula
            return false;
        }

        m_DeltaTime = m_DeltaDist / m_Speed;
        assert(m_DeltaTime >= 0);

        m_bParmsSet = true;

        return true;
    }

    void SetBeginDisp(T BeginDisp)
    {
        assert(IsValid());
        m_BeginDisp = BeginDisp;
    };
    T GetBeginDisp() const
    {
        assert(IsValid());
        return m_BeginDisp;
    }
    T GetDeltaDisp() const { return m_DeltaDisp; }

    void SetBeginDist(T BeginDist)
    {
        assert(IsValid());
        m_BeginDist = BeginDist;
    };
    T GetBeginDist() const
    {
        assert(IsValid());
        return m_BeginDist;
    }
    T GetDeltaDist() const
    {
        assert(IsValid());
        return m_DeltaDist;
    }

    void SetBeginTime(T BeginTime)
    {
        assert(IsValid());
        m_BeginTime = BeginTime;
    };
    T GetBeginTime() const
    {
        assert(IsValid());
        return m_BeginTime;
    }
    T GetDeltaTime() const { return m_DeltaTime; }

    bool IsValid() const
    {
        if(!CTrajTimeProfile::IsValid())
            return false;

        if(m_Speed == 0)
            return false;

        return m_bParmsSet;
    }

private:
    // Parametri
    T m_BeginDisp; // Pocetni pomak
    T m_DeltaDisp; // Ukupni pomak

    T m_BeginDist; // Pocetni udalj
    T m_DeltaDist; // Ukupni udalj tj duljina

    T m_BeginTime; // Pocetno vrijeme
    T m_DeltaTime; // Ukupno vrijeme

    T m_Speed; // Brzina kojom se gibamo

    // Pomocne varijable
    bool m_bParmsSet;

    static T abs(T x) { return (x > 0 ? x : -x); }
};

//=================================================================================================================================================================
//
// Vremenski profil za jednoliko ubrzano gibanje.
//
//=================================================================================================================================================================
template<class T>
class CUniformAccelTimeProfile : public CTrajTimeProfile
{
public:
    CUniformAccelTimeProfile() { m_bParmsSet = false; }

    virtual ETimeProfileType GetTimeProfileType() const { return TP_UniformAccel; }

    // Postavlja Time, Speed i Accel, ovisno o Disp parametru iz strukture.
    // Ostali clanovi strukture se ne modificiraju.
    // Ne vrsi se testiranje da li je Disp unutar granica (osim assert)!
    bool GetPointByDisp(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!m_bParmsSet)
            return false;

        T dDisp = pTrPoint->Disp - m_BeginDisp;

        // assert(dDisp >= 0 && dDisp <= GetDeltaDisp());

        pTrPoint->Accel = m_Accel;

        // Izracunaj DeltaTime i DeltaDist
        T dt1 = -m_BeginSpeed / m_Accel;

        T dDist1 = dt1 * m_BeginSpeed / 2;
        T dDisp1 = abs(dDist1);

        if(dt1 > 0 && dDisp >= dDisp1)
        {
            // Brzina je imala zero crossing
            T dDisp2 = dDisp - dDisp1;
            T dDist2 = (dDist1 >= 0 ? -dDisp2 : dDisp2);

            T dt2 = (T)sqrt(abs(2 * dDisp2 / m_Accel));

            assert(dDisp1 >= 0 && dDisp2 >= 0);
            assert(dt1 >= 0 && dt2 >= 0);

            T dTime = dt1 + dt2;
            pTrPoint->Time = m_BeginTime + dTime;
            pTrPoint->Dist = m_BeginDist + dDist1 + dDist2;
            pTrPoint->Velocity = m_BeginSpeed + m_Accel * dTime;
        }
        else if(m_BeginSpeed == 0)
        {
            T dDist;
            if(m_Accel > 0)
                dDist = dDisp;
            else
                dDist = -dDisp;

            pTrPoint->Dist = m_BeginDist + dDist;

            T dTime = (T)sqrt(2 * m_Accel * dDist);

            pTrPoint->Time = m_BeginTime + dTime;
            pTrPoint->Velocity = m_Accel * dTime;
        }
        else
        {
            // Brzina nije imala zero crossing
            T dDist;
            if(m_BeginSpeed >= 0)
                dDist = dDisp;
            else
                dDist = -dDisp;

            pTrPoint->Dist = m_BeginDist + dDist;

            T Discriminant = m_BeginSpeed * m_BeginSpeed + 2 * m_Accel * dDist;
            assert(Discriminant >= 0);

            T dTime;
            if(m_BeginSpeed > 0)
                dTime = dt1 + (T)sqrt(Discriminant) / m_Accel;
            else
                dTime = dt1 - (T)sqrt(Discriminant) / m_Accel;

            assert(dTime >= 0);

            pTrPoint->Time = m_BeginTime + dTime;
            pTrPoint->Velocity = m_BeginSpeed + m_Accel * dTime;
        }
        return true;
    };

    // Postavlja Disp, Dist, Speed i Accel, ovisno o Disp parametru iz strukture
    // Ostali clanovi strukture se ne modificiraju.
    // Ne vrsi se testiranje da li je Time unutar postavljenih granica (osim assert)!
    bool GetPointByTime(S2DTrajPoint* pTrPoint) const
    {
        assert(IsValid());

        if(!m_bParmsSet)
            return false;

        T dt = pTrPoint->Time - m_BeginTime;

        // assert(dt>=0 && dt <= GetDeltaTime());

        T Velocity = m_BeginSpeed + m_Accel * dt;
        pTrPoint->Velocity = Velocity;
        T dDist = (m_BeginSpeed + Velocity) * dt / 2;
        pTrPoint->Dist = m_BeginDist + dDist;
        pTrPoint->Accel = m_Accel;

        // Disp
        T dt1 = -m_BeginSpeed / m_Accel; // Vrijeme kada brzina ima vrijednost 0 - ako je negativno tada brzina nikad nema vrijednost nula
        T Disp = m_BeginDisp;
        if(dt1 > 0 && dt > dt1)
        {
            // Brzina je imala zero crossing u dt1
            Disp += abs(m_BeginSpeed * m_BeginSpeed / 2 / m_Accel); // Pomak od m_BeginSpeed do brzine 0
            T dt2 = dt - dt1;
            assert(dt2 > 0);
            Disp += abs(m_Accel) * dt2 * dt2 / 2;
        }
        else
        {
            // Brzina nije imala zero crossing
            Disp += abs(dDist);
        }
        pTrPoint->Disp = Disp;

        return true;
    };

    // Zadavanje preko ukupnog vremena - ukupni pomak se racuna iz ukupnog vremena
    struct SParms1
    {
        T BeginTime;
        T DeltaTime;
        T BeginDisp;
        T BeginDist;
        T BeginSpeed;
        T Accel;
    };

    // Postavljanje parametara preko ukupnog vremena - povoljnija verzija
    // jer ne treba racunati korijen
    bool SetParms(SParms1* pParms)
    {
        m_bParmsSet = false;

        m_BeginTime = pParms->BeginTime;
        m_DeltaTime = pParms->DeltaTime;
        m_BeginDisp = pParms->BeginDisp;
        m_BeginDist = pParms->BeginDist;
        m_BeginSpeed = pParms->BeginSpeed;
        m_Accel = pParms->Accel;

        if(m_Accel == 0 || m_DeltaTime < 0)
        {
            assert(0);
            return false;
        }

        // Dist
        m_DeltaDist = m_BeginSpeed * m_DeltaTime + m_Accel / 2 * (m_DeltaTime * m_DeltaTime);

        // Disp
        T dt1 = -m_BeginSpeed /
                m_Accel; // Vrijeme kada brzina ima vrijednost 0 - ako je negativno tada brzina nikad ne moze imati vrijednost nula
        if(dt1 > 0 && dt1 < m_DeltaTime)
        {
            // Brzina dostize vrijednost 0 za dt1
            m_DeltaDisp = abs(m_BeginSpeed * m_BeginSpeed / 2 / m_Accel); // Pomak od m_BeginSpeed do brzine 0
            T dt2 = m_DeltaTime - dt1;
            assert(dt2 >= 0);
            m_DeltaDisp += abs(m_Accel) * dt2 * dt2 / 2;
        }
        else
        {
            // Brzina nema zero crossing u vrijednosti 0
            m_DeltaDisp = abs(m_DeltaDist);
        }

        m_bParmsSet = true;

        return true;
    }

    // Zadavanje preko ukupnog pomaka - ukupno vrijeme se racuna iz ukupnog pomaka
    struct SParms2
    {
        T BeginTime;
        T BeginDisp;
        T BeginDist;
        T DeltaDisp;
        T BeginSpeed;
        T Accel;
    };

    // Postavljanje parametara preko ukupnog pomaka - nepovoljnija verzija
    // jer se treba racunati korijen
    bool SetParms(SParms2* pParms)
    {
        m_bParmsSet = false;

        m_BeginTime = pParms->BeginTime;
        m_BeginDisp = pParms->BeginDisp;
        m_BeginDist = pParms->BeginDist;
        m_DeltaDisp = pParms->DeltaDisp;
        m_BeginSpeed = pParms->BeginSpeed;
        m_Accel = pParms->Accel;

        if(m_Accel == 0 || m_DeltaDisp < 0)
        {
            assert(0); // Mora biti Accel != 0 i DeltaDisp >= 0
            return false;
        }

        // Izracunaj DeltaTime i DeltaDist
        T dt1 = -m_BeginSpeed / m_Accel; // Zero crossing brzine

        T dDist1 = dt1 * m_BeginSpeed / 2;
        T dDisp1 = abs(dDist1);

        if(dt1 > 0 && dDisp1 < m_DeltaDisp)
        {
            // Brzina ima zero crossing i Disp != Dist
            T dDisp2 = m_DeltaDisp - dDisp1;
            T dDist2 = (dDist1 >= 0 ? -dDisp2 : dDisp2);

            T dt2 = (T)sqrt(abs(2 * dDisp2 / m_Accel));

            assert(dDisp1 >= 0 && dDisp2 >= 0);
            assert(dt1 >= 0 && dt2 >= 0);

            m_DeltaTime = dt1 + dt2;
            m_DeltaDist = dDist1 + dDist2;
        }
        else if(m_BeginSpeed == 0)
        {
            if(m_Accel > 0)
                m_DeltaDist = m_DeltaDisp;
            else
                m_DeltaDist = -m_DeltaDisp;

            m_DeltaTime = (T)sqrt(2 * m_DeltaDist / m_Accel);
        }
        else
        {
            // Brzina nema zero crossing i Disp = |Dist|
            T EndSpeed;

            if(m_BeginSpeed > 0)
            {
                m_DeltaDist = m_DeltaDisp;
                T EndSpeed_2 = m_BeginSpeed * m_BeginSpeed + 2 * m_Accel * m_DeltaDist;
                assert(EndSpeed_2 + ((T)0.0001) >= 0); // Radi num neprecizn
                if(EndSpeed_2 < 0)
                    EndSpeed_2 = 0;
                EndSpeed = (T)sqrt(EndSpeed_2);
            }
            else
            {
                m_DeltaDist = -m_DeltaDisp;
                T EndSpeed_2 = m_BeginSpeed * m_BeginSpeed + 2 * m_Accel * m_DeltaDist;
                assert(EndSpeed_2 >= 0);
                EndSpeed = -(T)sqrt(EndSpeed_2);
            }

            m_DeltaTime = (EndSpeed - m_BeginSpeed) / m_Accel;
        }
        assert(m_DeltaTime >= 0);

        m_bParmsSet = true;

        return true;
    }

    // Struktura za zadavanje parametara za jednoliko ubrzanje od nula do zeljene brzine uz zadani pomak.
    // Racuna se potrebna akceleracija (paznja! moze ispasti prevelika!)
    struct SParms3
    {
        T BeginTime;
        T BeginDisp;
        T BeginDist;

        T DeltaDisp;       // [>0] Zeljeni pomak
        T DesiredVelocity; // [!=0] Zeljena brzina
    };

    bool SetParms(SParms3* pParms)
    {
        m_bParmsSet = false;

        m_BeginTime = pParms->BeginTime;
        m_BeginDisp = pParms->BeginDisp;
        m_BeginDist = pParms->BeginDist;

        m_DeltaDisp = pParms->DeltaDisp;

        if(m_DeltaDisp <= 0 || pParms->DesiredVelocity == 0)
        {
            assert(0);
            return false;
        }

        m_BeginSpeed = 0;

        // Izracunaj DeltaTime i DeltaDist
        if(pParms->DesiredVelocity > 0)
        {
            m_DeltaDist = m_DeltaDisp;
            m_Accel = pParms->DesiredVelocity * pParms->DesiredVelocity / (2 * m_DeltaDisp);
        }
        else
        {
            m_DeltaDist = -m_DeltaDisp;
            m_Accel = -pParms->DesiredVelocity * pParms->DesiredVelocity / (2 * m_DeltaDisp);
        }

        m_DeltaTime = pParms->DesiredVelocity / m_Accel;

        assert(m_DeltaTime >= 0);

        m_bParmsSet = true;

        return true;
    }

    // Struktura za zadavanje parametara za jednoliko usporavanje od zadane brzine do nule uz zadani pomak.
    // Racuna se potrebna deceleracija (paznja! moze ispasti prevelika!)
    struct SParms4
    {
        T BeginTime;
        T BeginDisp;
        T BeginDist;

        T DeltaDisp;     // [>0] Zeljeni pomak
        T StartVelocity; // [!=0] Pocetna brzina
    };

    bool SetParms(SParms4* pParms)
    {
        m_bParmsSet = false;

        m_BeginTime = pParms->BeginTime;
        m_BeginDisp = pParms->BeginDisp;
        m_BeginDist = pParms->BeginDist;

        m_DeltaDisp = pParms->DeltaDisp;

        if(m_DeltaDisp <= 0 || pParms->StartVelocity == 0)
        {
            assert(0);
            return false;
        }

        m_BeginSpeed = pParms->StartVelocity;

        // Izracunaj DeltaTime i DeltaDist
        if(m_BeginSpeed > 0)
        {
            m_DeltaDist = m_DeltaDisp;
            m_Accel = -m_BeginSpeed * m_BeginSpeed / (2 * m_DeltaDisp);
        }
        else
        {
            m_DeltaDist = -m_DeltaDisp;
            m_Accel = m_BeginSpeed * m_BeginSpeed / (2 * m_DeltaDisp);
        }

        m_DeltaTime = -m_BeginSpeed / m_Accel;

        assert(m_DeltaTime >= 0);

        m_bParmsSet = true;

        return true;
    }

    void SetBeginDisp(T BeginDisp) { m_BeginDisp = BeginDisp; };
    T GetBeginDisp() const { return m_BeginDisp; }
    T GetDeltaDisp() const { return m_DeltaDisp; }

    void SetBeginDist(T BeginDist) { m_BeginDist = BeginDist; };
    T GetBeginDist() const { return m_BeginDist; }
    T GetDeltaDist() const { return m_DeltaDist; }

    void SetBeginTime(T BeginTime) { m_BeginTime = BeginTime; };
    T GetBeginTime() const { return m_BeginTime; }
    T GetDeltaTime() const { return m_DeltaTime; }

    bool IsValid() const
    {
        if(!CTrajTimeProfile::IsValid())
            return false;

        if(m_Accel == 0)
            return false;

        return m_bParmsSet;
    }

    T GetEndSpeed()
    {
        return m_BeginSpeed + m_Accel * m_DeltaTime;
        ;
    }

private:
    // Parametri
    T m_BeginDisp; // Pocetni pomak
    T m_DeltaDisp; // Ukupni pomak

    T m_BeginDist; // Pocetna udalj
    T m_DeltaDist; // Duljina

    T m_BeginTime; // Pocetno vrijeme
    T m_DeltaTime; // Ukupno vrijeme

    T m_BeginSpeed; // Pocetna brzina
    T m_Accel;      // Ubrzanje

    // Pomocne varijable
    bool m_bParmsSet;
};

#endif // _UniformTimeProfiles_h_
