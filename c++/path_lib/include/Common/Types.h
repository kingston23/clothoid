#ifndef _Types_h_
#define _Types_h_

struct s_uint24
{
    unsigned char b1;
    unsigned char b2;
    unsigned char b3;
};

typedef unsigned int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef s_uint24 uint24;

//#ifndef BYTE
//#define BYTE unsigned char
//#endif

typedef unsigned char BYTE;

#endif