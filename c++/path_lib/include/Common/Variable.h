#ifndef _Variable_h_
#define _Variable_h_

#include <assert.h>
#include <cstdlib>
#include <cstring>

//=================================================================================================================================================================
//
// Klasa za prosirenu varijablu, koja osim vrijednosti sadrzi vrijeme i pouzdanost.
//
//=================================================================================================================================================================
template<class Type>
class CVariableEx
{
public:
    Type val;
    float conf; // pouzdanost [0,1]
    double time;

    CVariableEx()
    {
        conf = 0;
        val = 0;
        time = 0;
    }

    void Set(Type v, double t, float c = 1.f)
    {
        assert(c >= 0 && c <= 1);
        val = v;
        conf = c;
        time = t;
    }
    void Get(Type& v, double& t, float& c) const
    {
        v = val;
        t = time;
        c = conf;
    }

    Type GetV() const { return val; }

    operator Type() const { return GetV(); }
};

//=================================================================================================================================================================
//
// Varijabla sa cuvanjem proslih vrijednosti u kruznom spremniku.
// Velicina spremnika moze se dinamicki postaviti.
//
//=================================================================================================================================================================
template<class Type>
class CVariableHistoryDynamic
{
public:
    CVariableHistoryDynamic()
    {
        iHistory = 0;
        HistorySize = 0;

        pVar = NULL;
    }

    ~CVariableHistoryDynamic() { free(pVar); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja vrijednost svih varijabli u spremniku na nula.
    //________________________________________________________________________________________________________________________________________________
    void SetAllZero() { memset(pVar, 0, sizeof(Type) * HistorySize); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja sljedecu varijablu u kruznom spremniku kao trenutnu.
    //________________________________________________________________________________________________________________________________________________
    void Advance() { iHistory = (iHistory + 1) % HistorySize; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja varijablu.
    //
    // Postavlja se trenutna varijabla u kruznom spremniku.
    //________________________________________________________________________________________________________________________________________________
    void Set(Type v)
    {
        assert(pVar);
        pVar[iHistory] = v;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca vrijednost trenutne varijable.
    // Ovo ima smisla samo ak je varijabla prethodno postavljena.
    //________________________________________________________________________________________________________________________________________________
    Type Get() const
    {
        assert(pVar);
        return pVar[iHistory];
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca vrijednost trazene prosle prosirene varijable iz kruznog spremnika.
    // Ovo ima smisla samo ak je trazena varijabla prethodno postavljena.
    // Treba paziti da indeks trazene varijable ne izlazi izvan velicine spremnika
    // (iako se u tom slucaju nece srusiti program zbog koristenja operatora %)
    //
    // Ind - indeks koji odreduje koja varijabla se vraca (za i=0 vraca trenutnu, i=1 vraca prvu proslu itd. ukrug)
    //		Ako je indeks negativan, npr. ide od -1 pa nanize, dobi se prvo najstarija varijabla, pa ona manje stara itd.
    //		Raspon je [-BufferSize, BufferSize-1]
    //________________________________________________________________________________________________________________________________________________
    Type Get(int Ind) const
    {
        assert(pVar);
        assert(Ind >= -HistorySize && Ind < HistorySize);
        int i = (iHistory - Ind + HistorySize) % HistorySize;
        if(i < 0)
            i = 0;

        return pVar[i];
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Operator koji vraca vrijednost trenutne varijable.
    // Ovo ima smisla samo ak je varijabla prethodno postavljena.
    //________________________________________________________________________________________________________________________________________________
    operator Type() const
    {
        assert(pVar);
        return pVar[iHistory];
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca velicinu kruznog spremnika
    //________________________________________________________________________________________________________________________________________________
    int BufferSize() const { return HistorySize; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja velicinu kruznog spremnika (uz realokaciju memorije).
    // Prijasnje vrijednosti u spremniku ostaju sacuvane, a eventualno novoalocirane se postavljaju na nulu.
    // Kod smanjivanja spremnika, resetira se indeks trenutne vrijednosti na nulu ako je izvan granica spremnika
    //________________________________________________________________________________________________________________________________________________
    bool BufferSize(int NewHistorySize)
    {
        if(NewHistorySize == HistorySize)
            return true;

        Type* pNewVar = (Type*)realloc(pVar, NewHistorySize * sizeof(Type));

        if(!pNewVar)
            return false;

        // Provjeri da pokazivac na trenutnu vrijednost nije izvan granica
        if(iHistory >= NewHistorySize)
            iHistory = 0;

        // Postavi novoalocirane vrijednosti na nulu
        if(NewHistorySize > HistorySize)
            memset(pNewVar + HistorySize, 0, sizeof(Type) * (NewHistorySize - HistorySize));

        pVar = pNewVar;
        HistorySize = NewHistorySize;

        return true;
    }

public:
    Type* pVar; // kruzni spremnik varijabli

protected:
    int iHistory; // Indeks trenutne varijable u kruznom spremniku
    int HistorySize;
};

//=================================================================================================================================================================
//
// Prosirena varijabla sa cuvanjem proslih vrijednosti u kruznom spremniku.
//
// Sve vrijednosti varijabli, vremena i pouzdanosti su u konstruktoru postavljaju na nulu.
//
//=================================================================================================================================================================
template<class Type, int HistorySize>
class CVariableHistoryEx
{
public:
    CVariableHistoryEx()
    {
        iHistory = 0;

        SetAllZero();

        bAutoAdvanceOnNewTime = true; // U ovom slucaju ne moramo zvati Advance()
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja poudanost svih varijabli u spremniku na nula.
    //________________________________________________________________________________________________________________________________________________
    void SetConfidenceZero()
    {
        for(int i = 0; i < HistorySize; i++)
            Var[i].conf = (Type)0;
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja vrijednost, pouzdanost i vrijeme svih varijabli u spremniku na nula.
    //________________________________________________________________________________________________________________________________________________
    void SetAllZero() { memset(Var, 0, sizeof(Var)); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja sljedecu varijablu u kruznom spremniku kao trenutnu.
    //________________________________________________________________________________________________________________________________________________
    void Advance() { iHistory = (iHistory + 1) % HistorySize; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Postavlja prosirenu varijablu.
    //
    // U slucaju da je postavljena opcija bAutoAdvanceOnNewTime, i ak je novo vrijeme, premjestamo se na sljedecu varijablu.
    // U protivnom postavlja se trenutna varijabla.
    // Paziti u slucaju prvog postavljanja kad je prosla varijabla nedefinirana pa joj se ne moze provjeriti vrijeme!!!
    // U tom slucaju korisno je osigurati da je vrijeme uvijek >0, pa posto je u konstruktoru vrijeme svih varijabli postavljeno na 0,
    // nova varijabla s vremenom >0 bude automatski dodana na sljedece mjesto.
    //________________________________________________________________________________________________________________________________________________
    void Set(Type v, double t, float c = 1.f)
    {
        if(bAutoAdvanceOnNewTime && t > Var[iHistory].time)
            Advance(); // Premjestamo se na sljedecu varijablu ak je novo vrijeme

        Var[iHistory].Set(v, t, c);
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca vrijednost trenutne prosirene varijable.
    // Ovo ima smisla samo ak je varijabla prethodno postavljena.
    //________________________________________________________________________________________________________________________________________________
    void Get(Type& v, double& t, float& c) const { Var[iHistory].Get(v, t, c); }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca vrijednost trazene prosle prosirene varijable iz kruznog spremnika.
    // Ovo ima smisla samo ak je trazena varijabla prethodno postavljena.
    // Treba paziti da indeks trazene varijable ne izlazi izvan velicine spremnika
    // (iako se u tom slucaju nece srusiti program zbog koristenja operatora %)
    //
    // Ind - indeks koji odreduje koja varijabla se vraca (za i=0 vraca trenutnu, i=1 vraca prvu proslu itd. ukrug)
    //		Raspon je [0, BufferSize-1]
    //________________________________________________________________________________________________________________________________________________
    void Get(int Ind, Type& v, double& t, float& c) const
    {
        assert(Ind >= 0 && Ind < HistorySize);
        int i = (iHistory - Ind + HistorySize) % HistorySize;
        if(i < 0)
            i = 0;
        Var[i].Get(v, t, c);
    }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca vrijednost trenutne varijable.
    // Ovo ima smisla samo ak je varijabla prethodno postavljena.
    //________________________________________________________________________________________________________________________________________________
    Type GetV() const { return Var[iHistory].val; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Operator koji vraca vrijednost trenutne varijable.
    // Ovo ima smisla samo ak je varijabla prethodno postavljena.
    //________________________________________________________________________________________________________________________________________________
    operator Type() const { return Var[iHistory].val; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca vrijednost vremena trenutne varijable.
    // Ovo ima smisla samo ak je varijabla prethodno postavljena.
    //________________________________________________________________________________________________________________________________________________
    double GetT() const { return Var[iHistory].time; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca pouzdanost trenutne varijable.
    // Ovo ima smisla samo ak je varijabla prethodno postavljena.
    //________________________________________________________________________________________________________________________________________________
    float GetConf() const { return Var[iHistory].conf; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Vraca velicinu kruznog spremnika
    //________________________________________________________________________________________________________________________________________________
    int BufferSize() const { return HistorySize; }

    //________________________________________________________________________________________________________________________________________________
    //
    // Izmjenjuje vrijednosti (cijeli spremnik i indeks trenutne varijable) sa drugom varijablom istog tipa i iste velicine spremnika.
    //________________________________________________________________________________________________________________________________________________
    void SwapValues(CVariableHistoryEx<Type, HistorySize>* pOther)
    {
        assert(pOther);

        // TUDU*: kak provjeriti dal su tipovi i velicine spremnika isti?

        // Kopiraj vrijednosti iz this varijable u temp varijable
        int iHistoryTemp = iHistory;
        CVariableEx<Type> VarTemp[HistorySize];
        memcpy(VarTemp, Var, HistorySize * sizeof(CVariableEx<Type>));

        // Kopiraj vrijednosti iz druge varijable u this
        iHistory = pOther->iHistory;
        memcpy(Var, pOther->Var, HistorySize * sizeof(CVariableEx<Type>));

        // Kopiraj vrijednosti iz temp varijabli u drugu varijablu
        pOther->iHistory = iHistoryTemp;
        memcpy(pOther->Var, VarTemp, HistorySize * sizeof(CVariableEx<Type>));
    }

public:
    CVariableEx<Type> Var[HistorySize]; // kruzni spremnik varijabli

protected:
    int iHistory; // Indeks trenutne varijable u kruznom spremniku

    // Ak je ovo true, provjeram vrijeme kod postavljanja varijable (Set() funkcija) i ak je vece od prethodnog,
    // premjestam se na sljedecu varijablu u kruznom spremniku
    bool bAutoAdvanceOnNewTime;
};

#endif // _Variable_h_
