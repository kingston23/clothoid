* Source ROS environment
`source /opt/ros/melodic/setup.bash`

### CREATE CATKIN WORKSPACE ###

* Create and build a catkin workspace:

```
`mkdir catkin_ws`
`cd catkin_ws`
`mkdir src`
`cd src`
`catkin_init_workspace`
`git@gitlab.com:kingston23/clothoid.git`
`cd ..`
```

* Compiling:
`catkin_make`

* Set up the path:
`cd devel`
`source setup.bash`

### INSTALL LIBRARY FOR SMOOTHING ###

The library is installed in `/urs/lib` because we defined this in CMakeLists.txt.

* Go to folder `path_lib`:

`cd catkin_ws/src/clothoid/c++/path_lib`

* Make new `build` folder:

`mkdir build`

* Go to folder `build` and type:

`cd build`

* Compile the source by typing: 

```
cmake ..
make
```

### TWD Planner ###

* Put the whole twdsrc folder into your catkin workspace's src folder. The folder `twdplanner` is the TWD planner, while the folder `pathtwdlistener` is the simple subscriber to the output message of the TWD planner. 
* Compile the source by typing `catkin_make` in the root folder of your catkin workspace
* Open three terminals and start:

1. `roslaunch twdplanner ushapeamcltest.launch`

2. `roslaunch twdplanner starttwd.launch`

3. `roslaunch pathtwdlistener startpathlistener.launch`

Click on the rviz window on `2D Nav Goal` button and then click somewhere in the free space of the loaded environment. The robot should start to move to the selected goal point. The TWD path should be plotted on the rviz window.


# Add moving obstacle

Robot_1 is a moving obstacle.

### Potrebne izmjene prije pokretanja skripti

* In `ushapeamcltest.world` add new robot with pose [x y z thet] : 

```

pioneer2dx
(
name "zeleni"
  color "green"
 pose [2.4 3.1 0.000 90.0000] 
sicklaser()
  localization_origin [0 0 0 0]
velocity_bounds [-2 2 0 0 0 0 -200 200 ]
acceleration_bounds [-0.2 0.2 0 0 0 0 -20 20]
)

```

* In `starttwd.launch` are made changes for some parametars and values are set to `robot_0`. Changes are saved as `starttwd_multi.launch`:

```
    <node name="twd" pkg="twdplanner" type="twd" output="screen">
        <param name="base_frame" value="/robot_0/base_link" />
        <param name="scan_topic" value="/robot_0/base_scan" />
        <param name="cmd_vel_topic" value="/robot_0/cmd_vel" />
	...
        <param name="drive_flag" type="bool" value="false" />
    </node>

```

`drive_flag` is an on/off flag if the robot will move to the goal using the Dynamic Window approach, or will not move but instead one can move it by drag and drop in Stage simulator

* In `maintwd.cpp` set waitForTransform and lookupTransform to `/robot_0/base_link`:

```
tf_listener.waitForTransform("/robot_0/base_link",msg->header.frame_id, msg->header.stamp, ros::Duration(5));
tf_listener.lookupTransform("/robot_0/base_link",msg->header.frame_id, ros::Time(0), transform_laser);

```

* In `moj.h` set cmd_vel_topic_:

```
      cmd_vel_topic_ = "/robot_0/cmd_vel";

```

### RUN SMOOTHING ALGORITHM ###

Set goal, current position and path:
`roslaunch nodelet_talker test1.launch`

Run smoothing algoritm:
`roslaunch nodelet_talker talker.launch`

The output will be saved in `~/.ros` folder with name `smoothedPath.txt`. The smoothing algorithm gives a smoothed path as output states of the form [x y] without theta (theta isn't calculated in clothoids segments but it can be calculated with atan2(y,x) where x and y are output states).


### SMOOTHED PATH VISUALIZATION ###
* The package `smoothedpathlistener` is for visaulization of smoothed path in Rviz. 

* For the visualization of the smoothed path run:
`roslaunch smoothedpathlistener startpathlistener.launch`

### TWD* and smoothing for multi robot:

* Compile the source by typing `catkin_make` in the root folder of your catkin workspace

* Open six terminals and start:

1. roslaunch twdplanner ushapeamcltest_multi.launch

2. roslaunch twdplanner starttwd_multi.launch

3. roslaunch pathtwdlistener startpathlistener.launch

4. roslaunch nodelet_talker test1.launch

5. roslaunch nodelet_talker talker.launch

6. roslaunch smoothedpathlistener startpathlistener.launch

Click on the rviz window on `2D Nav Goal` button and then click somewhere in the free space of the loaded environment. The robot should start to move to the selected goal point. The TWD path should be plotted on the rviz window.



### GIT ###

* Cheack changes and add new folder/file on git:

```
git status (vidis sto se promijenilo -crveno)
git add file (ili vise datoteka odvojenih razmakom)
git status (sad su zeleni)
git commit -m "poruka"
git push origin master (i sad je to na serveru)
```

* To pull changes from others:
`git pull`
