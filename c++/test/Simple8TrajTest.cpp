 #define DBG_FILE_ // Opcionalno
#include "Traj/Simple8Traj.h"
#include "Traj/Smooth8Traj.h"
#include "Traj/SimpleCircleTraj.h"
#include <stdio.h>
#include "Traj/PathSmoothing.h"



int main()
{


    double T;

// primjer za osmice
    // CSimple8Traj T8;
    // CSimple8Traj::SParms1 T8Parms;
    // T8Parms.StartTime = 1;
    // T8Parms.StartDisp = 10;
    // T8Parms.xCenter1 = 1;
    // T8Parms.yCenter1 = 1;
    // T8Parms.xCenter2 = -1;
    // T8Parms.yCenter2 = -1;
    // T8Parms.RadiusRatio = 2; // omjer 2 kruga
    // T8Parms.Angle0 = -20;
    // T8Parms.TotalDisp = 19;
    // T8Parms.v = 1;
    // T8Parms.Accel = 1;
    // T8Parms.Deccel = 1;
    // T8Parms.bPositive = true;
    // T8.SetParms(&T8Parms);

    // C2DTrajectoryBase::S2DTrajPoint STrajPt;
    // STrajPt.Time = 2;
    // T8.GetPointByTime(&STrajPt);
    // T8.LogTrajectoryByTime("T8.txt", 0.01);


//primjer za  krug na 2 nacina
    //CSimpleCircleTraj Circ;

    // ovaj nacin ne radi dobro
    // typedef CSimpleCircleTraj::SParms1 SCircParms;
    // SCircParms Cp;
    // Cp.StartTime = 1;
    // Cp.StartDisp = 1;
    // Cp.xCenter = 2;
    // Cp.yCenter = 2;
    // Cp.TrajAngle0 = 0;
    // Cp.TotalAngle = 3.14f*6;
    // Cp.v = 1;
    // Cp.Accel = 1;
    // Cp.Deccel = 1;
    // Cp.bPositive = true;
    // Circ.SetParms(&Cp);

    // typedef C2DTrajectoryBase::S2DTrajPoint STrajPt;
    // STrajPt TrajPt;
    // TrajPt.Time = 1;
    // Circ.GetPointByTime(&TrajPt);
    // TrajPt.Disp = 2;
    // Circ.GetPointByDisp(&TrajPt);


    // typedef CSimpleCircleTraj::SParms2 SCircParms2;
    // SCircParms2 Cp2;
    // Cp2.StartTime = 1;
    // Cp2.StartDisp = 2;
    // Cp2.x0 = 3;
    // Cp2.y0 = 3;
    // Cp2.Radius = 1;
    // Cp2.TrajAngle0 = 3.14f/2;
    // Cp2.TotalAngle = 3.14f*2; // crta puni krug ako se tu stavi 2*pi
    // Cp2.v = 0.75;
    // Cp2.Accel = 0.5;
    // Cp2.Deccel = 0.5;
    // Cp2.bPositive = true;
    // Circ.SetParms(&Cp2);

    // Circ.LogTrajectoryByTime("krug2.txt", 0.01);

    CSimpleSmooth8Traj Smooth8Traj;

    typedef CSimpleSmooth8Traj::SParms1 SSmoothParams1;
    SSmoothParams1 Cp2;
    Cp2.AccelerationLength = 1;
    Cp2.StartTime = 1;
    Cp2.EightDuration = 100;
    Cp2.vMax = 0.75;
    Cp2.x0 = 15.0f;
    Cp2.y0 = 12.5f;
    Cp2.Width = 10.0f; 
    Cp2.Height = 5.0f;
    Cp2.bInitialAcceleration = true;
    Smooth8Traj.SetParms(&Cp2);

    Smooth8Traj.LogTrajectoryByTime("smooth8traj.txt", 0.01);

    return 0;
}

