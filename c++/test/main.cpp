#include "Traj/PathSmoothing.h"

#include <stdio.h>

int main(int argc, char** argv)
{
    printf("hello world\n");

    C2DTrajectoryBase::S2DTrajPoint currentState;
    currentState.SetAllZero();
    currentState.x = 1.0;
    currentState.y =  1.00000;
    currentState.Angle =  0.0;
    currentState.Curv =  0 ;
    C2DTrajectoryBase::S2DTrajPoint goalState;
    goalState.SetAllZero();
    goalState.x = 5.0;
    goalState.y = 5.0000;
    goalState.Angle = 1.57079632679 ;
    goalState.Curv = 0;

    std::vector<Geom::C2DPoint> PWLinearPath;
    PWLinearPath.emplace_back(currentState.x, currentState.y);
    PWLinearPath.emplace_back(5.0, 1.0);
    //PWLinearPath.emplace_back(5.00000, 4.950000);
    PWLinearPath.emplace_back(goalState.x, goalState.y);
    // std::vector<Geom::C2DPoint> PWLinearPath;
    // PWLinearPath.emplace_back(currentState.x, currentState.y);
    // PWLinearPath.emplace_back(6.397382, 2.950023);
    // PWLinearPath.emplace_back(6.292099, 2.995801);
    // PWLinearPath.emplace_back(6.088983, 3.339636);
    // PWLinearPath.emplace_back(3.350000, 5.450000);
    // PWLinearPath.emplace_back(2.650000, 5.450000);
    // PWLinearPath.emplace_back(2.450000, 5.250000);
    // PWLinearPath.emplace_back(2.450000, 4.850000);
    // PWLinearPath.emplace_back(2.550000, 4.150000);
    // PWLinearPath.emplace_back(3.250000, 1.050000 );
    // PWLinearPath.emplace_back(3.450000, 0.850000);
    // PWLinearPath.emplace_back(goalState.x, goalState.y);
    


// ########### MOJ PRIMJER ##########
    // goalState.x = 2;
    // goalState.y = 0.1;

    // std::vector<Geom::C2DPoint> PWLinearPath;
    // PWLinearPath.emplace_back(currentState.x, currentState.y);
    // PWLinearPath.emplace_back(0.5, 0);
    // PWLinearPath.emplace_back(0.5, -0.5);
    // PWLinearPath.emplace_back(1, -0.5);
    // PWLinearPath.emplace_back(1, -1);
    // PWLinearPath.emplace_back(1.5, -0.5);
    // PWLinearPath.emplace_back(2, 0);
    // PWLinearPath.emplace_back(goalState.x, goalState.y);

    if(!C2DClothoidApp::CreateLookupTable())
        return 0;

    CClothoidPathSmoothing pathSmoothing;
    pathSmoothing.SmoothPath(currentState, goalState, PWLinearPath);
    CTrajCompositePathProfile& SmoothedPath = pathSmoothing.SmoothedPath();

    SmoothedPath.LogTrajectoryByDist("SampledTraj.txt", 0.01);

    C2DClothoidApp::DestroyLookupTable();

    return 0;
}

