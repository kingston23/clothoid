
#ifdef TEST

#include "Traj/MaxSpeedCircleLineTimeProfile.h"

#include "Traj/LineCirclePathPlanner1.h"
#include "stdio.h"

void Test()
{
    FILE* f = fopen("Shit.txt", "wt");

    CMobileRobot::SRobotState BeginState, EndState;

    BeginState.x = 0.f;
    BeginState.y = 0.f;
    BeginState.Angle = 0.f;
    BeginState.Velocity = 1.6f;

    EndState.x = 0.1f;
    EndState.y = 0.1f;
    EndState.Angle = 0.f;
    EndState.Velocity = 0.8f;

    CLineCirclePathPlanner1 Pp;

    CLineCirclePathPlanner1::SParms PathParms;
    PathParms.DefaultRadius = 0.2f;
    PathParms.EndLineLength = 0.05f;
    Pp.SetParms(&PathParms);

    CTrajPathProfile* pPath = Pp.GetPath(&BeginState, &EndState, nullptr);

    CMaxSpeedCircleLineTimeProfile Tp;

    CMaxSpeedCircleLineTimeProfile::SParms Parms;
    Parms.MaxAccel = 2;
    Parms.MaxRobotSpeed = 2;
    Parms.WheelDistance = 0.1f;
    Tp.SetParms(&Parms);

    C2DTrajectoryBase* pTraj = Tp.GetTimeProfile((CTrajCompositePathProfile*)pPath, &BeginState, &EndState, nullptr);

    C2DTrajectoryBase::S2DTrajPoint TrajPt;

    fprintf(f, "%%{'x','y','ang','v','a','t','disp','dist','c'}\n");
    for(float t = pTraj->GetBeginTime(); t < pTraj->GetBeginTime() + pTraj->GetDeltaTime(); t += 0.02f)
    {
        TrajPt.Time = t;
        pTraj->GetPointByTime(&TrajPt);

        pPath->GetPointByDist(&TrajPt);

        fprintf(f, "%f %f %f %f %f %f %f %f %f\n", TrajPt.x, TrajPt.y, TrajPt.Angle, TrajPt.Velocity, TrajPt.Accel, TrajPt.Time,
                TrajPt.Disp, TrajPt.Dist, TrajPt.Curv);
    }

    fclose(f);
}

#endif
